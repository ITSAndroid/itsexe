
## Rules for Gson
# For using GSON @Expose annotation
-keep class com.google.gson.stream.** { *; }
-keep class com.google.code.gson.* { *; }
-keep class com.google.gson.Gson.* { *; }
-keepattributes *Annotation*, Signature, Exception
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}
-keepclassmembers class * {
    private <fields>;
}

-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

 -keep class com.infinityinfoway.itsexe.custom_dialog.Rotate3dAnimation {
     public <init>(...);
  }