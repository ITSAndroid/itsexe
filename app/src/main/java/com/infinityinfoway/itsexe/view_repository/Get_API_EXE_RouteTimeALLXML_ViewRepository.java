package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeALLXML_Request;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Get_API_EXE_RouteTimeALLXML_ViewRepository {
    public static Get_API_EXE_RouteTimeALLXML_ViewRepository repository;
    private final ApiInterface apiService;

    public static Get_API_EXE_RouteTimeALLXML_ViewRepository getInstance() {
        if (repository == null) {
            repository = new Get_API_EXE_RouteTimeALLXML_ViewRepository();
        }
        return repository;
    }

    public Get_API_EXE_RouteTimeALLXML_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface OnResponseHandler{
        void onSuccessResponse(Response<Get_API_EXE_RouteTimeALLXML_Response> response);
        void onFailureResponse();
    }

    public void Get_API_EXE_RouteTimeALLXML(Get_API_EXE_RouteTimeALLXML_Request request,OnResponseHandler onResponseHandler){
        Call<Get_API_EXE_RouteTimeALLXML_Response> call=apiService.Get_API_EXE_RouteTimeALLXML(request);
        call.enqueue(new Callback<Get_API_EXE_RouteTimeALLXML_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_API_EXE_RouteTimeALLXML_Response> call, @NotNull Response<Get_API_EXE_RouteTimeALLXML_Response> response) {
                if(response.isSuccessful() && response.body().getStatus()== APP_CONSTANTS.STATUS_SUCCESS){
                    onResponseHandler.onSuccessResponse(response);
                }else{
                    onResponseHandler.onFailureResponse();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_API_EXE_RouteTimeALLXML_Response> call, @NotNull Throwable t) {
                onResponseHandler.onFailureResponse();
            }
        });
    }

}
