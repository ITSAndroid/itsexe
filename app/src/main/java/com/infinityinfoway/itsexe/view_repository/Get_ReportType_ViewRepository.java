package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_ReportType_Request;
import com.infinityinfoway.itsexe.api.response.Get_ReportType_Response;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Get_ReportType_ViewRepository {
    private final ApiInterface apiService;
    public static Get_ReportType_ViewRepository repository;

    public static Get_ReportType_ViewRepository getInstance() {
        if (repository == null) {
            repository = new Get_ReportType_ViewRepository();
        }
        return repository;
    }

    public Get_ReportType_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface OnResponseHandler {
        void onSuccessResponse(List<Get_ReportType_Response.ReportType> list);

        void onFailureResponse();
    }

    public void Get_ReportType(Get_ReportType_Request request, OnResponseHandler onResponseHandler) {
        Call<Get_ReportType_Response> call = apiService.Get_ReportType(request);
        call.enqueue(new Callback<Get_ReportType_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_ReportType_Response> call, @NotNull Response<Get_ReportType_Response> response) {
                if (response.isSuccessful() && response.body().getStatus() == APP_CONSTANTS.STATUS_SUCCESS && response.body().getData().getReportType().size() > 0) {
                    onResponseHandler.onSuccessResponse(response.body().getData().getReportType());
                } else {
                    onResponseHandler.onFailureResponse();
                }
            }
            @Override
            public void onFailure(@NotNull Call<Get_ReportType_Response> call, @NotNull Throwable t) {
                onResponseHandler.onFailureResponse();
            }
        });
    }

}
