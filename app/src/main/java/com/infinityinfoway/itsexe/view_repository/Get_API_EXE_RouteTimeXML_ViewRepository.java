package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeXML_Request;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Get_API_EXE_RouteTimeXML_ViewRepository {
    private static Get_API_EXE_RouteTimeXML_ViewRepository repository;
    private final ApiInterface apiService;

    public Get_API_EXE_RouteTimeXML_ViewRepository getInstance() {
        if (repository == null) {
            repository = new Get_API_EXE_RouteTimeXML_ViewRepository();
        }
        return repository;
    }

    public Get_API_EXE_RouteTimeXML_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface OnResponseHandler {
        void onSuccessResponse(List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> list);

        void onFailureResponse();
    }

    public void Get_API_EXE_RouteTimeXML(Get_API_EXE_RouteTimeXML_Request request, OnResponseHandler onResponseHandler) {
        Call<Get_API_EXE_RouteTimeXML_Response> call = apiService.Get_API_EXE_RouteTimeXML(request);
        call.enqueue(new Callback<Get_API_EXE_RouteTimeXML_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_API_EXE_RouteTimeXML_Response> call, @NotNull Response<Get_API_EXE_RouteTimeXML_Response> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == APP_CONSTANTS.STATUS_SUCCESS && response.body().getData().getRouteTimeXML().size() > 0) {
                    onResponseHandler.onSuccessResponse(response.body().getData().getRouteTimeXML());
                } else {
                    onResponseHandler.onFailureResponse();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_API_EXE_RouteTimeXML_Response> call, @NotNull Throwable t) {
                onResponseHandler.onFailureResponse();
            }
        });
    }

}
