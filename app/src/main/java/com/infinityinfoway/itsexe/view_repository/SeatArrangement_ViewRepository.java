package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatArrangement_Request;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatArrangement_ViewRepository {
    private static SeatArrangement_ViewRepository viewRepository;
    private SeatArrangement_OnResponseHandler onResponseHandler;

    public static SeatArrangement_ViewRepository getInstance() {
        if (viewRepository == null) {
            viewRepository = new SeatArrangement_ViewRepository();
        }
        return viewRepository;
    }

    private ApiInterface apiService;

    public SeatArrangement_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    // Get_Report_AllDayMemo_BranchWise

    public interface SeatArrangement_OnResponseHandler {
        void onSucessHandler(List<SeatArrangement_Response.Datum> list, boolean status);

        //void onSucessHandler(LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> map, boolean status);
        void onFailedHandler();
    }


    public void SeatArrangement(SeatArrangement_Request request, final SeatArrangement_OnResponseHandler onResponseHandler) {
        this.onResponseHandler = onResponseHandler;
        apiService.SeatArrangement(request).enqueue(new Callback<SeatArrangement_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatArrangement_Response> call, @NotNull Response<SeatArrangement_Response> response) {
                if (response != null && response.isSuccessful() && response.body() != null && response.body().getData().getData() != null && response.body().getData().getData().size() > 0) {
                    onResponseHandler.onSucessHandler(response.body().getData().getData(),true);
                }else{
                    onResponseHandler.onFailedHandler();
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatArrangement_Response> call, @NotNull Throwable t) {
                onResponseHandler.onFailedHandler();
            }
        });

    }

}
