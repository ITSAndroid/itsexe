package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.PickupDrop_Request;
import com.infinityinfoway.itsexe.api.response.PickupDrop_Response;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickupDrop_ViewRepository {
    public final ApiInterface apiService;
    public static PickupDrop_ViewRepository repository;

    public static PickupDrop_ViewRepository getInstance() {
        if (repository == null) {
            repository = new PickupDrop_ViewRepository();
        }
        return repository;
    }

    private PickupDrop_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface OnResponseHandler {
        void onSuccessResponse(List<PickupDrop_Response.Datum> list);

        void onFailureResponse();
    }

    public void PickupDrop(PickupDrop_Request request, OnResponseHandler onResponseHandler) {
        Call<PickupDrop_Response> call = apiService.PickupDrop(request);
        call.enqueue(new Callback<PickupDrop_Response>() {
            @Override
            public void onResponse(@NotNull Call<PickupDrop_Response> call, @NotNull Response<PickupDrop_Response> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == APP_CONSTANTS.STATUS_SUCCESS) {
                    if(response.body().getData() != null && response.body().getData().size() > 0){
                        onResponseHandler.onSuccessResponse(response.body().getData());
                    }else{
                        onResponseHandler.onSuccessResponse(new ArrayList<>());
                    }
                } else {
                    onResponseHandler.onFailureResponse();
                }
            }

            @Override
            public void onFailure(@NotNull Call<PickupDrop_Response> call, @NotNull Throwable t) {
                onResponseHandler.onFailureResponse();
            }
        });
    }

}
