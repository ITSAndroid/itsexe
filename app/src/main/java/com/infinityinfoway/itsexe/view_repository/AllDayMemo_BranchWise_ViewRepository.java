package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_Report_AllDayMemo_BranchWise_Request;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_BranchWise_Response;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDayMemo_BranchWise_ViewRepository {
    private static AllDayMemo_BranchWise_ViewRepository viewRepository;
    private AllDayMemo_BranchWise_OnResponseHandler allDayMemoBranchWiseOnResponseHandler;

    public static AllDayMemo_BranchWise_ViewRepository getInstance() {
        if (viewRepository == null) {
            viewRepository = new AllDayMemo_BranchWise_ViewRepository();
        }
        return viewRepository;
    }

    private ApiInterface apiService;

    public AllDayMemo_BranchWise_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    // Get_Report_AllDayMemo_BranchWise

    public interface AllDayMemo_BranchWise_OnResponseHandler {
        void onSucessHandler(LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> data,boolean status);
        void onFailedHandler();
    }

    public void Get_Report_AllDayMemo_BranchWise(Get_Report_AllDayMemo_BranchWise_Request request, AllDayMemo_BranchWise_OnResponseHandler mCallBack) {
        this.allDayMemoBranchWiseOnResponseHandler = mCallBack;
        apiService.Get_Report_AllDayMemo_BranchWise(request).enqueue(new Callback<Get_Report_AllDayMemo_BranchWise_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Report_AllDayMemo_BranchWise_Response> call, @NotNull Response<Get_Report_AllDayMemo_BranchWise_Response> response) {

                if (response != null && response.isSuccessful() && response.body() != null && response.body().getData().getAllDayMemo() != null && response.body().getData().getAllDayMemo().size() > 0) {

                    LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> Hashmap_allDayMemo = new LinkedHashMap<>();
                    LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>> linkedHashMap = null;

                    for (Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo pojo : response.body().getData().getAllDayMemo()) {

                        if (!Hashmap_allDayMemo.containsKey(pojo.getRouteTime())) {
                            linkedHashMap = new LinkedHashMap<>();
                        }

                        if (!Hashmap_allDayMemo.containsKey(pojo.getRouteName())) {
                            List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> pickDropPassengerDetailPojoList = new ArrayList<>();
                            pickDropPassengerDetailPojoList.add(pojo);
                            linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                        } else {
                            List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getRouteName());
                            pickDropPassengerDetailPojoList.add(pojo);
                            linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                        }

                        Hashmap_allDayMemo.put(pojo.getRouteTime(), linkedHashMap);

                    }

                    allDayMemoBranchWiseOnResponseHandler.onSucessHandler(Hashmap_allDayMemo,true);
                } else {
                    allDayMemoBranchWiseOnResponseHandler.onFailedHandler();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Report_AllDayMemo_BranchWise_Response> call, @NotNull Throwable t) {
                allDayMemoBranchWiseOnResponseHandler.onFailedHandler();
            }
        });
    }


}
