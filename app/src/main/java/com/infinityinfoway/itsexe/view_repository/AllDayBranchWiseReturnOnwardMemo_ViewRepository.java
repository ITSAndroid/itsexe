package com.infinityinfoway.itsexe.view_repository;

import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.AllDayBranchWiseReturnOnwardMemo_Request;
import com.infinityinfoway.itsexe.api.response.AllDayBranchWiseReturnOnwardMemo_Response;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDayBranchWiseReturnOnwardMemo_ViewRepository {
    private static AllDayBranchWiseReturnOnwardMemo_ViewRepository viewRepository;
    private OnResponseHandler onResponseHandler;

    public static AllDayBranchWiseReturnOnwardMemo_ViewRepository getInstance() {
        if (viewRepository == null) {
            viewRepository = new AllDayBranchWiseReturnOnwardMemo_ViewRepository();
        }
        return viewRepository;
    }

    private ApiInterface apiService;

    public AllDayBranchWiseReturnOnwardMemo_ViewRepository() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    // Get_Report_AllDayMemo_BranchWise

    public interface OnResponseHandler {
        void onSucessHandler(LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>> data,boolean status);
        void onFailedHandler();
    }

    public void AllDayBranchWiseReturnOnwardMemo(AllDayBranchWiseReturnOnwardMemo_Request request, OnResponseHandler mCallBack) {
        this.onResponseHandler = mCallBack;
        apiService.AllDayBranchWiseReturnOnwardMemo(request).enqueue(new Callback<AllDayBranchWiseReturnOnwardMemo_Response>() {
            @Override
            public void onResponse(@NotNull Call<AllDayBranchWiseReturnOnwardMemo_Response> call, @NotNull Response<AllDayBranchWiseReturnOnwardMemo_Response> response) {

                if (response != null && response.isSuccessful() && response.body() != null && response.body().getData() != null && response.body().getData().size() > 0) {

                    LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>> Hashmap_allDayMemo = new LinkedHashMap<>();
                    LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>> linkedHashMap = null;

                    for (AllDayBranchWiseReturnOnwardMemo_Response.Datum pojo : response.body().getData()) {

                        if (!Hashmap_allDayMemo.containsKey(pojo.getRouteTime())) {
                            linkedHashMap = new LinkedHashMap<>();
                        }

                        if (!Hashmap_allDayMemo.containsKey(pojo.getRouteName())) {
                            List<AllDayBranchWiseReturnOnwardMemo_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                            pickDropPassengerDetailPojoList.add(pojo);
                            linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                        } else {
                            List<AllDayBranchWiseReturnOnwardMemo_Response.Datum> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getRouteName());
                            pickDropPassengerDetailPojoList.add(pojo);
                            linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                        }

                        Hashmap_allDayMemo.put(pojo.getRouteTime(), linkedHashMap);

                    }

                    onResponseHandler.onSucessHandler(Hashmap_allDayMemo,true);
                } else {
                    onResponseHandler.onFailedHandler();
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllDayBranchWiseReturnOnwardMemo_Response> call, @NotNull Throwable t) {
                onResponseHandler.onFailedHandler();
            }
        });
    }


}
