package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.bean.BookingTypeSeatCounterBean;

import java.util.List;

public class BookingTypeDialog_Adapter extends RecyclerView.Adapter<BookingTypeDialog_Adapter.MyViewHolder> {
    private Context context;
    private List<BookingTypeSeatCounterBean> listITSBusSchedule;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(BookingTypeSeatCounterBean item, int pos);
    }

    public BookingTypeDialog_Adapter(Context context,List<BookingTypeSeatCounterBean> listITSBusSchedule, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.listITSBusSchedule = listITSBusSchedule;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_booking_type, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txt_booking_type_name.setText(listITSBusSchedule.get(position).getDefaultName());

        holder.bind(listITSBusSchedule.get(position), onItemClickListener);

        if(listITSBusSchedule.get(position).getIsClick()==1){
            holder.txt_booking_type_name.setBackground(context.getResources().getDrawable(R.drawable.bg_btn_red));
            holder.txt_booking_type_name.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.txt_booking_type_name.setBackground(context.getResources().getDrawable(R.drawable.cancel_btn_bg_exe));
            holder.txt_booking_type_name.setTextColor(context.getResources().getColor(R.color.black));
        }

    }

    @Override
    public int getItemCount() {
        return listITSBusSchedule.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_booking_type_name;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_booking_type_name = rowView.findViewById(R.id.txt_booking_type_name);
        }

        void bind(final BookingTypeSeatCounterBean item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, getAdapterPosition());
                }
            });
        }
    }




}
