package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.BranchUserWiseCollection_RefundChart_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BranchUserWiseRefundSummaryAdapter extends RecyclerView.Adapter<BranchUserWiseRefundSummaryAdapter.MyViewHolder> {

    private LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund>> linkedHashMapRefund;
    private Context context;
    private List<String> key_list;
    private int IsPendingAmount=1;
    private LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending>> linkedHashMapRefundPending;


    public BranchUserWiseRefundSummaryAdapter(Context context, LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund>> linkedHashMapRefund) {
        this.context = context;
        this.linkedHashMapRefund = linkedHashMapRefund;
        key_list = new ArrayList<>(linkedHashMapRefund.keySet());
    }

    public BranchUserWiseRefundSummaryAdapter(Context context, LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending>> linkedHashMapRefundPending, int IsPendingAmount) {
        this.context = context;
        this.linkedHashMapRefundPending = linkedHashMapRefundPending;
        key_list = new ArrayList<>(linkedHashMapRefundPending.keySet());
        this.IsPendingAmount=IsPendingAmount;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch_user_wise_collection_refund_summary_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {

        if(IsPendingAmount==1){
            holder.txt_amount_type.setText(key_list.get(listPosition));

            ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, linkedHashMapRefund.get(key_list.get(listPosition)));
            holder.rv_sub_data.setAdapter(childLayoutAdapter);
        }else{
            holder.txt_amount_type.setText(key_list.get(listPosition));

            ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, linkedHashMapRefundPending.get(key_list.get(listPosition)),IsPendingAmount);
            holder.rv_sub_data.setAdapter(childLayoutAdapter);
        }


    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_amount_type;
        private RecyclerView rv_sub_data;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_amount_type = rowView.findViewById(R.id.txt_amount_type);
            rv_sub_data = rowView.findViewById(R.id.rv_sub_data);
        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {
        private Context context;
        private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund> list;
        private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending> listPendingRefund;

        public ChildLayoutAdapter(Context context, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund> list) {
            this.context = context;
            this.list = list;
        }

        public ChildLayoutAdapter(Context context, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending> listPendingRefund,int IsPendingAmount) {
            this.context = context;
            this.listPendingRefund = listPendingRefund;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch_user_wise_collection_refund_summary_child, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            if(IsPendingAmount==1){
                BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund data = list.get(position);

                holder.txt_user_code.setText(data.getUserName());
                holder.txt_branch_code.setText(data.getBranchCode1());
                holder.txt_pnr_tkt.setText(data.getPNRTicketNO());
                holder.txt_jdate.setText(data.getJourneyDate());
                holder.txt_jtime.setText(data.getJourneyStartTime());
                holder.txt_sub_route.setText(data.getFromCity());
                holder.txt_seats.setText(data.getTotalPassanger());
                holder.txt_ref_chg.setText(data.getRefChg());
                holder.txt_amount.setText(String.format("%.2f", data.getCancelAmount()));
            }else{
                BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending data = listPendingRefund.get(position);

                holder.txt_user_code.setText(data.getUserName());
                holder.txt_branch_code.setText(data.getBMBranchCode());
                holder.txt_pnr_tkt.setText(data.getJMPNRNO());
                holder.txt_jdate.setText(data.getJMJourneyStartDate());
                holder.txt_jtime.setText(data.getJMJourneyCityTime());
                holder.txt_sub_route.setText(data.getSubRouteName());
                holder.txt_seats.setText(String.valueOf(data.getTotalSeat()));
                holder.txt_ref_chg.setText(data.getRefundCharge());
                holder.txt_amount.setText(String.format("%.2f", data.getJMPayableAmount()));
            }

        }

        @Override
        public int getItemCount() {
            if(IsPendingAmount==1){
                return list.size();
            }else{
                return listPendingRefund.size();
            }

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_user_code, txt_branch_code, txt_pnr_tkt, txt_jdate, txt_jtime, txt_sub_route, txt_seats, txt_ref_chg, txt_amount;

            MyViewHolder(View rowView) {
                super(rowView);
                txt_user_code = rowView.findViewById(R.id.txt_user_code);
                txt_branch_code = rowView.findViewById(R.id.txt_branch_code);
                txt_pnr_tkt = rowView.findViewById(R.id.txt_pnr_tkt);
                txt_jdate = rowView.findViewById(R.id.txt_jdate);
                txt_jtime = rowView.findViewById(R.id.txt_jtime);
                txt_sub_route = rowView.findViewById(R.id.txt_sub_route);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_ref_chg = rowView.findViewById(R.id.txt_ref_chg);
                txt_amount = rowView.findViewById(R.id.txt_amount);
            }
        }
    }

}
