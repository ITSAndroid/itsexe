package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.ReservationDetailReportWithAmount_Response;
import com.infinityinfoway.itsexe.databinding.ItemReservationDetailReportWithAmountDroppingChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemReservationDetailReportWithAmountDroppingParentBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ReservationDetailReportWithAmountAdapter_DroppingPoints extends RecyclerView.Adapter<ReservationDetailReportWithAmountAdapter_DroppingPoints.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>>> linkedHashMap;
    private List<String> key_list;

    public ReservationDetailReportWithAmountAdapter_DroppingPoints(Context context, LinkedHashMap<String, LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemReservationDetailReportWithAmountDroppingParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtCity.setText(key_list.get(position));
        holder.parentBinding.rvData.setAdapter(new ChildLayoutAdapter(context, linkedHashMap.get(key_list.get(position))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemReservationDetailReportWithAmountDroppingParentBinding parentBinding;

        public MyViewHolder(ItemReservationDetailReportWithAmountDroppingParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {

        private Context context;
        private List<String> key_list_v2;
        private LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>> childHashMap;

        public ChildLayoutAdapter(Context context, LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>> childHashMap) {
            this.context = context;
            this.childHashMap = childHashMap;
            key_list_v2 = new ArrayList<>(childHashMap.keySet());
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemReservationDetailReportWithAmountDroppingChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            // ReservationDetailReportWithAmount_Response.Datum data=list.get(position);
            holder.childBinding.txtDropName.setText(key_list_v2.get(position));
            holder.childBinding.txtSeats.setText(calTotalSeat(childHashMap.get(key_list_v2.get(position))));

        }

        @Override
        public int getItemCount() {
            return key_list_v2.size();
        }

        private class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemReservationDetailReportWithAmountDroppingChildBinding childBinding;

            public MyViewHolder(ItemReservationDetailReportWithAmountDroppingChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }
    }

    private String calTotalSeat(List<ReservationDetailReportWithAmount_Response.Datum> list) {
        int totalSeat = 0;
        String seatList = "";
        for (ReservationDetailReportWithAmount_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getJMSeatList())) {
                if (TextUtils.isEmpty(seatList)) {
                    seatList += data.getJMSeatList();
                } else {
                    seatList += "," + data.getJMSeatList();
                }
            }
            if (!TextUtils.isEmpty(data.getJMTotalPassengers())) {
                totalSeat += Integer.parseInt(data.getJMTotalPassengers());
            }
        }
        return totalSeat + " ( " + seatList + " )";
    }

}
