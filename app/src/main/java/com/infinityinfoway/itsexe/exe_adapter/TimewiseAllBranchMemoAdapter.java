package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.TimewiseAllBranchMemo_Response;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class TimewiseAllBranchMemoAdapter extends RecyclerView.Adapter<TimewiseAllBranchMemoAdapter.MyViewHolder> {

    LinkedHashMap<String, List<TimewiseAllBranchMemo_Response.Datum>> hashMap_data;
    List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public TimewiseAllBranchMemoAdapter(Context mContext, LinkedHashMap<String, List<TimewiseAllBranchMemo_Response.Datum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title_branchname, txt_br_commission_total, txt_total_gst, txt_total_seats, txt_total_amt;
        RecyclerView recycler_data_branch;

        MyViewHolder(View rowView) {
            super(rowView);

            title_branchname = rowView.findViewById(R.id.title_branchname);
            recycler_data_branch = rowView.findViewById(R.id.rv_branch);
            txt_br_commission_total = rowView.findViewById(R.id.txt_br_comm);
            txt_total_gst = rowView.findViewById(R.id.txt_gst);
            txt_total_seats = rowView.findViewById(R.id.txt_total_seats);
            txt_total_amt = rowView.findViewById(R.id.txt_total_amt);
        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time_wise_all_branch_memo_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.title_branchname.setText(key_list.get(listPosition));

        List<String> list = getSubTotal(hashMap_data.get(key_list.get(listPosition)));

        holder.txt_total_seats.setText(list.get(0));
        holder.txt_br_commission_total.setText(list.get(1));
        holder.txt_total_gst.setText(list.get(2));
        holder.txt_total_amt.setText(list.get(3));

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition)));
        holder.recycler_data_branch.setAdapter(childLayoutAdapter);

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        List<TimewiseAllBranchMemo_Response.Datum> SeatDataList;
        private Context context;

        public ChildLayoutAdapter(Context mContext, List<TimewiseAllBranchMemo_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_total_amt, txt_seats, txt_net_amt, txt_gst, txt_amt_seat, txt_br_comm, txt_comm, txt_seat_name, txt_tocity, txt_from_city, txt_no;


            MyViewHolder(View rowView) {
                super(rowView);

                txt_total_amt = rowView.findViewById(R.id.txt_total_amt);
                txt_seats = rowView.findViewById(R.id.total_seat);
                txt_net_amt = rowView.findViewById(R.id.txt_net_amt);
                txt_gst = rowView.findViewById(R.id.txt_gst);
                txt_amt_seat = rowView.findViewById(R.id.txt_amt_seat);
                txt_br_comm = rowView.findViewById(R.id.txt_br_comm);
                txt_comm = rowView.findViewById(R.id.txt_comm);
                txt_seat_name = rowView.findViewById(R.id.txt_seat_names);
                txt_tocity = rowView.findViewById(R.id.txt_to_city);
                txt_from_city = rowView.findViewById(R.id.txt_from_city);
                txt_no = rowView.findViewById(R.id.txt_sr_no);
            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.timewise_branchmemo_childlayout, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            positionCounter = positionCounter + 1;

            holder.txt_no.setText(String.valueOf(positionCounter));
            holder.txt_from_city.setText(SeatDataList.get(listPosition).getFromCityName());
            holder.txt_tocity.setText(SeatDataList.get(listPosition).getToCityName());
            holder.txt_seat_name.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.txt_comm.setText(SeatDataList.get(listPosition).getCommission());
            holder.txt_br_comm.setText(String.format("%.2f", SeatDataList.get(listPosition).getBranchCommission()));
            holder.txt_amt_seat.setText(String.format("%.2f", SeatDataList.get(listPosition).getAmount()));
            holder.txt_gst.setText(String.format("%.2f", SeatDataList.get(listPosition).getServiceTax()));
            holder.txt_net_amt.setText(String.format("%.2f", SeatDataList.get(listPosition).getExSTaxAmount()));
            holder.txt_seats.setText(String.valueOf(SeatDataList.get(listPosition).getTotalSeat()));
            holder.txt_total_amt.setText(String.format("%.2f", SeatDataList.get(listPosition).getTotalAmount()));
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

    }

    private List<String> getSubTotal(List<TimewiseAllBranchMemo_Response.Datum> list) {
        double totalBranchComm = 0.0, totalGST = 0.0, totalAmount = 0.0;
        int totalSeat = 0;
        for (TimewiseAllBranchMemo_Response.Datum data : list) {
            totalSeat += data.getTotalSeat();
            totalBranchComm += data.getBranchCommission();
            totalGST += data.getServiceTax();
            totalAmount += data.getTotalAmount();
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.format("%.2f", totalBranchComm));
        arrayList.add(String.format("%.2f", totalGST));
        arrayList.add(String.format("%.2f", totalAmount));
        return arrayList;
    }

}

