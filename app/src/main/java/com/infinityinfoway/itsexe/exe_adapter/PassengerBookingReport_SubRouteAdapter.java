package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PassengerBookingReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemPassengerBookingReportSubrouteBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PassengerBookingReport_SubRouteAdapter extends RecyclerView.Adapter<PassengerBookingReport_SubRouteAdapter.MyViewHolder> {
    private LinkedHashMap<String, List<PassengerBookingReport_Response.Datum>> hashMap_data;
    List<String> key_list;

    public PassengerBookingReport_SubRouteAdapter(Context mContext, LinkedHashMap<String, List<PassengerBookingReport_Response.Datum>> hashMap_data) {
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPassengerBookingReportSubrouteBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int listPosition) {
        holder.binding.txtRoute.setText(key_list.get(listPosition));
        List<PassengerBookingReport_Response.Datum> SeatDataList = hashMap_data.get(key_list.get(listPosition));
        ArrayList<String> arrayList = getSeatDetails(SeatDataList);
        holder.binding.txtTotalRoute.setText(arrayList.get(0));
        holder.binding.txtRouteSeatsList.setText(arrayList.get(1));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemPassengerBookingReportSubrouteBinding binding;

        MyViewHolder(ItemPassengerBookingReportSubrouteBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private ArrayList getSeatDetails(List<PassengerBookingReport_Response.Datum> list) {
        String seatNo = "";
        int totalSeat = 0;
        for (PassengerBookingReport_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSeat += Integer.parseInt(data.getTotalSeat());
            }
            if (!TextUtils.isEmpty(data.getSeatNo())) {
                seatNo = seatNo.length() <= 0 ? data.getSeatNo() : seatNo + "," + data.getSeatNo();
            }
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(seatNo);
        return arrayList;
    }


}
