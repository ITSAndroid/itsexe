package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_BranchWise_Response;
import com.infinityinfoway.itsexe.databinding.ItemAlldaymemoBranchChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemAlldaymemoBranchNestedchildBinding;
import com.infinityinfoway.itsexe.databinding.ItemAlldaymemoBranchParentBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class AllDayMemoBranchWiseAdapter extends RecyclerView.Adapter<AllDayMemoBranchWiseAdapter.MyViewHolder> {

    private LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> Hashmap_allDayMemo;
    private List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public AllDayMemoBranchWiseAdapter(Context mContext, LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> Hashmap_allDayMemo) {
        context = mContext;
        this.Hashmap_allDayMemo = Hashmap_allDayMemo;
        if(!Hashmap_allDayMemo.keySet().isEmpty()){
            key_list = new ArrayList<>(this.Hashmap_allDayMemo.keySet());
        }else{
            key_list=new ArrayList<>();
        }

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemAlldaymemoBranchParentBinding parentBinding;

        MyViewHolder(ItemAlldaymemoBranchParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemAlldaymemoBranchParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.parentBinding.txtTime.setText(key_list.get(listPosition));
        holder.parentBinding.rvAllDayBranch.setAdapter(new ChildLayoutAdapter(context, Hashmap_allDayMemo.get(key_list.get(listPosition))));
        List<String> list = getRouteTotalSeatAmt(Hashmap_allDayMemo.get(key_list.get(listPosition)));
        holder.parentBinding.txtTotalSeat.setText(list.get(0));
        holder.parentBinding.txtTotalAmount.setText(list.get(1));

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public void refreshAdapter(LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> hashMapLinkedHashMap){
        this.Hashmap_allDayMemo=new LinkedHashMap<>();
        this.Hashmap_allDayMemo.putAll(hashMapLinkedHashMap);
        this.key_list = new ArrayList<>(this.Hashmap_allDayMemo.keySet());
        this.notifyDataSetChanged();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>> linkedHashMap;
        private Context context;
        private List<String> key_list_V2;


        public ChildLayoutAdapter(Context mContext, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>> linkedHashMap) {
            this.context = mContext;
            this.linkedHashMap = linkedHashMap;
            key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private ItemAlldaymemoBranchChildBinding childBinding;


            MyViewHolder(ItemAlldaymemoBranchChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemAlldaymemoBranchChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            holder.childBinding.txtRoute.setText(key_list_V2.get(listPosition));
            holder.childBinding.rvAllDayBranchChild.setAdapter(new NestedChildLayoutAdapter(context, linkedHashMap.get(key_list_V2.get(listPosition))));
        }

        @Override
        public int getItemCount() {
            return key_list_V2.size();
        }


    }

    public class NestedChildLayoutAdapter extends RecyclerView.Adapter<NestedChildLayoutAdapter.MyViewHolder> {

        List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> SeatDataList;
        private Context context;


        public NestedChildLayoutAdapter(Context mContext, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemAlldaymemoBranchNestedchildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
            positionCounter = positionCounter + 1;

            holder.nestedchildBinding.txtSrNo.setText(String.valueOf(positionCounter));
            holder.nestedchildBinding.txtFrom.setText(SeatDataList.get(listPosition).getFromCityName());
            holder.nestedchildBinding.txtTo.setText(SeatDataList.get(listPosition).getToCityName());
            holder.nestedchildBinding.txtSeatNames.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.nestedchildBinding.txtAmount.setText(SeatDataList.get(listPosition).getAmount());
            holder.nestedchildBinding.txtTotalSeat.setText(String.valueOf(SeatDataList.get(listPosition).getTotalSeat()));
            holder.nestedchildBinding.txtTotalAmount.setText(String.format("%.2f", SeatDataList.get(listPosition).getTotalAmount()));
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemAlldaymemoBranchNestedchildBinding nestedchildBinding;

            MyViewHolder(ItemAlldaymemoBranchNestedchildBinding nestedchildBinding) {
                super(nestedchildBinding.getRoot());
                this.nestedchildBinding = nestedchildBinding;
            }
        }
    }

    private List<String> getRouteTotalSeatAmt(LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>> linkedHashMap) {
        int totalRouteSeat = 0;
        double totalRouteAmt = 0.0;

        List<String> key_list_V2 = new ArrayList<>(linkedHashMap.keySet());

        for (int i = 0; i < linkedHashMap.size(); i++) {
            for (Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo data : linkedHashMap.get(key_list_V2.get(i))) {
                totalRouteSeat += data.getTotalSeat();
                totalRouteAmt += data.getTotalAmount();
            }
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.format("%.2f", totalRouteAmt));
        return arrayList;

    }

}

