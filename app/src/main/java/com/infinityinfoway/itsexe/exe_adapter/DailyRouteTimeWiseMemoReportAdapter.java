package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.DailyRoutetimeWiseMemo_Response;


import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DailyRouteTimeWiseMemoReportAdapter extends RecyclerView.Adapter<DailyRouteTimeWiseMemoReportAdapter.MyViewHolder> {

    private Context context;
    private List<DailyRoutetimeWiseMemo_Response.Datum> listBusAvailableRoute;


    public DailyRouteTimeWiseMemoReportAdapter(Context mContext, List<DailyRoutetimeWiseMemo_Response.Datum> listContactUs) {
        this.context = mContext;
        this.listBusAvailableRoute = listContactUs;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_Sr_no, txt_amount_second, txt_commission, txt_seat, txt_seat_no, txt_branch_agent_name, txt_amount;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_Sr_no = itemView.findViewById(R.id.txt_Sr_no);
            txt_amount_second = itemView.findViewById(R.id.txt_amount_second);
            txt_commission = itemView.findViewById(R.id.txt_commission);
            txt_seat = itemView.findViewById(R.id.txt_seat);
            txt_seat_no = itemView.findViewById(R.id.txt_seat_no);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_branch_agent_name = itemView.findViewById(R.id.txt_branch_agent_name);
        }
    }


    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView;
        iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dailyroutetime_wisemomo_report, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        DailyRoutetimeWiseMemo_Response.Datum contact_data = listBusAvailableRoute.get(listPosition);

        holder.txt_Sr_no.setText((listPosition + 1) + "");
        holder.txt_amount_second.setText(String.format("%.2f", contact_data.getTotalNetAmount()));
        holder.txt_amount.setText(String.format("%.2f", contact_data.getTotalAmount()));
        holder.txt_commission.setText(String.format("%.2f", contact_data.getTotalCommission()));
        holder.txt_seat.setText(String.valueOf(contact_data.getTotalPax()));
        holder.txt_seat_no.setText(contact_data.getSeatList());
        holder.txt_branch_agent_name.setText(contact_data.getBranchAgentName());
    }

    @Override
    public int getItemCount() {
        return listBusAvailableRoute.size();
    }

}






