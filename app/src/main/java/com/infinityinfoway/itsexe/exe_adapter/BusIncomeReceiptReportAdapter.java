package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.BusIncomeReceipt_Response;
import com.infinityinfoway.itsexe.databinding.ItemBusIncomeReceiptReportChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemBusIncomeReceiptReportParentBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BusIncomeReceiptReportAdapter extends RecyclerView.Adapter<BusIncomeReceiptReportAdapter.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<BusIncomeReceipt_Response.Datum>> linkedHashMap;
    private List<String> key_list;
    private int positionCounter=0;

    public BusIncomeReceiptReportAdapter(Context context, LinkedHashMap<String, List<BusIncomeReceipt_Response.Datum>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        this.key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemBusIncomeReceiptReportParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtSrNo.setText(key_list.get(position));
        holder.parentBinding.rvData.setAdapter(new ChildLayoutAdapter(context,linkedHashMap.get(key_list.get(position))));
        List<String> list=calGrandTotal(linkedHashMap.get(key_list.get(position)));

        holder.parentBinding.txtAmount.setText(list.get(0));
        holder.parentBinding.txtSeat.setText(list.get(1));
        holder.parentBinding.txtNetAmt.setText(list.get(2));
        holder.parentBinding.txtGst.setText(list.get(3));
        holder.parentBinding.txtAgentComm.setText(list.get(4));

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemBusIncomeReceiptReportParentBinding parentBinding;

        public MyViewHolder(ItemBusIncomeReceiptReportParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }


    class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {

        private Context context;
        private List<BusIncomeReceipt_Response.Datum> list;

        public ChildLayoutAdapter(Context context, List<BusIncomeReceipt_Response.Datum> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemBusIncomeReceiptReportChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            positionCounter++;

            BusIncomeReceipt_Response.Datum data = list.get(position);
            holder.childBinding.txtSrNo.setText(String.valueOf(positionCounter));
            holder.childBinding.txtFrom.setText(data.getFromcity());
            holder.childBinding.txtTo.setText(data.getTocity());

            holder.childBinding.txtAmount.setText(String.format("%.2f",data.getJPAmount()));
            holder.childBinding.txtSeat.setText(String.valueOf(data.getTotalSeat()));
            holder.childBinding.txtNetAmt.setText(String.format("%.2f",data.getJMPayableAmount()));
            holder.childBinding.txtGst.setText(String.format("%.2f",data.getJPServiceTax()));


        }

        @Override
        public int getItemCount() {
            return list.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemBusIncomeReceiptReportChildBinding childBinding;

            public MyViewHolder(ItemBusIncomeReceiptReportChildBinding binding) {
                super(binding.getRoot());
                this.childBinding = binding;
            }
        }
    }

    private List<String> calGrandTotal(List<BusIncomeReceipt_Response.Datum> list){
        int totalSeat=0;
        double totalAmount=0,totalNetAmount=0,totalGST=0,totalAgentComm=0;
        for(BusIncomeReceipt_Response.Datum data:list){
            totalSeat+=data.getTotalSeat();
            totalAmount+=data.getJMPayableAmount();
            totalNetAmount+=data.getJMPayableAmount();
            totalGST+=data.getJPServiceTax();
            totalAgentComm+=data.getAgentComm();
        }

        List<String> arrayList=new ArrayList<>();
        arrayList.add(String.format("%.2f",totalAmount));
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.format("%.2f",totalNetAmount));
        arrayList.add(String.format("%.2f",totalGST));
        arrayList.add(String.format("%.2f",totalAgentComm));
        return arrayList;
    }

}
