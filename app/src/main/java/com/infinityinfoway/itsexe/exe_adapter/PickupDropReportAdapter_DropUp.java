package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PickupDropReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemPickupDropReportChildDropwiseBinding;
import com.infinityinfoway.itsexe.databinding.ItemPickupDropReportParentDropwiseBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PickupDropReportAdapter_DropUp extends RecyclerView.Adapter<PickupDropReportAdapter_DropUp.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<PickupDropReport_Response.SubDropUpWiseChart>> linkedHashMap;
    private List<String> key_list;
    private int positionCounter = 0;

    public PickupDropReportAdapter_DropUp(Context context, LinkedHashMap<String, List<PickupDropReport_Response.SubDropUpWiseChart>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPickupDropReportParentDropwiseBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtDropName.setText(key_list.get(position));
        holder.parentBinding.rvDrop.setAdapter(new ChildLayoutAdapter(linkedHashMap.get(key_list.get(position)), context));
        holder.parentBinding.txtCityWiseTotalSeat.setText("City Wise Total Seats: "+calTotalSeat(linkedHashMap.get(key_list.get(position))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemPickupDropReportParentDropwiseBinding parentBinding;

        public MyViewHolder(ItemPickupDropReportParentDropwiseBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {

        private List<PickupDropReport_Response.SubDropUpWiseChart> list;
        private Context context;

        public ChildLayoutAdapter(List<PickupDropReport_Response.SubDropUpWiseChart> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemPickupDropReportChildDropwiseBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            positionCounter++;
            holder.childBinding.txtSrNo.setText(String.valueOf(positionCounter));
            holder.childBinding.txtCityDrop.setText(list.get(position).getDropName());
            holder.childBinding.txtSeatNames.setText(list.get(position).getSeatNo());
            holder.childBinding.txtTotalSeat.setText(list.get(position).getTotalSeat());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemPickupDropReportChildDropwiseBinding childBinding;

            public MyViewHolder(ItemPickupDropReportChildDropwiseBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }
    }

    private String calTotalSeat(List<PickupDropReport_Response.SubDropUpWiseChart> list) {
        int totalSeat = 0;
        for (PickupDropReport_Response.SubDropUpWiseChart data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSeat += Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSeat);

    }

}
