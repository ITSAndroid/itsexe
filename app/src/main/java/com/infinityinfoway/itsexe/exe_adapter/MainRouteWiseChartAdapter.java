package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.MainRouteWiseChart_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MainRouteWiseChartAdapter extends RecyclerView.Adapter<MainRouteWiseChartAdapter.MyViewHolder> {

    private LinkedHashMap<String, List<MainRouteWiseChart_Response.Datum>> hashMap_data;
    private List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public MainRouteWiseChartAdapter(Context context, LinkedHashMap<String, List<MainRouteWiseChart_Response.Datum>> hashMap_data) {
        this.context = context;
        this.hashMap_data = hashMap_data;
        this.key_list = new ArrayList<>(hashMap_data.keySet());
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_route_wise_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_branch_name.setText(key_list.get(listPosition));

        holder.txt_branch_name.setPaintFlags(holder.txt_branch_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        List<String> arrayList = getBranchWiseTotal(hashMap_data.get(key_list.get(listPosition)));
        holder.txt_total_seat.setText(arrayList.get(0));
        holder.txt_base_fare.setText(arrayList.get(1));
        holder.txt_gst.setText(arrayList.get(2));
        holder.txt_total_amt.setText(arrayList.get(3));

        holder.rv_branch.setAdapter(new MainRouteWiseChartAdapter.ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_branch_name, txt_total_seat, txt_base_fare, txt_gst, txt_total_amt;
        private RecyclerView rv_branch;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_branch_name = rowView.findViewById(R.id.txt_branch_name);
            rv_branch = rowView.findViewById(R.id.rv_branch);
            txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
            txt_base_fare = rowView.findViewById(R.id.txt_base_fare);
            txt_gst = rowView.findViewById(R.id.txt_gst);
            txt_total_amt = rowView.findViewById(R.id.txt_total_amt);

        }

    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<MainRouteWiseChartAdapter.ChildLayoutAdapter.MyViewHolder> {

        List<MainRouteWiseChart_Response.Datum> SeatDataList;
        private Context context;

        public ChildLayoutAdapter(Context mContext, List<MainRouteWiseChart_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_route_wise_child, parent, false);
            return new ChildLayoutAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            positionCounter = positionCounter + 1;

            MainRouteWiseChart_Response.Datum data = SeatDataList.get(position);

            holder.txt_sr_no.setText(String.valueOf(positionCounter));
            holder.txt_pnr_tkt.setText(data.getPNRNO());
            holder.txt_user.setText(data.getUserName());
            holder.txt_total_seat.setText(data.getTotalSeat());
            holder.txt_seats.setText(data.getSeatNo());
            holder.txt_customer.setText(data.getCustomerName());
            holder.txt_phone.setText(data.getPhone());
            holder.txt_subroute.setText(data.getFromCityToCity());
            holder.txt_pickup.setText(data.getPickUpName());
            holder.txt_booking_type.setText(data.getBookingType());
            holder.txt_base_fare.setText(String.valueOf(data.getBaseFare()));
            holder.txt_gst.setText(String.valueOf(data.getJMServiceTax()));
            holder.txt_amount.setText(String.valueOf(data.getJMPayableAmount()));


        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_pnr_tkt, txt_user, txt_total_seat, txt_seats, txt_customer,
                    txt_phone, txt_subroute, txt_booking_type, txt_pickup, txt_base_fare, txt_gst, txt_amount;

            MyViewHolder(View rowView) {
                super(rowView);

                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_pnr_tkt = rowView.findViewById(R.id.txt_pnr_tkt);
                txt_user = rowView.findViewById(R.id.txt_user);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_customer = rowView.findViewById(R.id.txt_customer);
                txt_phone = rowView.findViewById(R.id.txt_phone);
                txt_subroute = rowView.findViewById(R.id.txt_subroute);
                txt_pickup = rowView.findViewById(R.id.txt_pickup);
                txt_booking_type = rowView.findViewById(R.id.txt_booking_type);
                txt_base_fare = rowView.findViewById(R.id.txt_base_fare);
                txt_gst = rowView.findViewById(R.id.txt_gst);
                txt_amount = rowView.findViewById(R.id.txt_amount);

            }
        }

    }

    private List<String> getBranchWiseTotal(List<MainRouteWiseChart_Response.Datum> list) {
        int totalRouteSeat = 0;
        double totalBaseFare = 0.0, totalGST = 0.0, totalAmount = 0.0;
        for (MainRouteWiseChart_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalRouteSeat += Integer.parseInt(data.getTotalSeat());
            }
            totalBaseFare += data.getBaseFare();
            totalGST += data.getJMServiceTax();
            totalAmount += data.getJMPayableAmount();

        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.valueOf(totalBaseFare));
        arrayList.add(String.valueOf(totalGST));
        arrayList.add(String.valueOf(totalAmount));
        return arrayList;
    }
}
