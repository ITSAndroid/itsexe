package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.SubRouteWiseChart_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class SubRouteWiseChartAdapter extends RecyclerView.Adapter<SubRouteWiseChartAdapter.MyViewHolder> {
    LinkedHashMap<String, LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>>> hashMap_data;
    List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public SubRouteWiseChartAdapter(Context context, LinkedHashMap<String, LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>>> hashMap_data) {
        this.context = context;
        this.hashMap_data = hashMap_data;
        this.key_list = new ArrayList<>(hashMap_data.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_route_wise_chart_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_sub_route.setText(key_list.get(listPosition));
        List<String> list = getSubRouteWiseTotal(1, hashMap_data.get(key_list.get(listPosition)), listPosition);
        holder.txt_sub_route_total_seat.setText(list.get(0));
        holder.txt_sub_route_total_amt.setText(list.get(1));

        holder.rv_sub_route.setAdapter(new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_sub_route, txt_sub_route_total_seat, txt_sub_route_total_amt;
        RecyclerView rv_sub_route;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_sub_route = rowView.findViewById(R.id.txt_sub_route);
            rv_sub_route = rowView.findViewById(R.id.rv_sub_route);
            txt_sub_route_total_seat = rowView.findViewById(R.id.txt_sub_route_total_seat);
            txt_sub_route_total_amt = rowView.findViewById(R.id.txt_sub_route_total_amt);

        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<SubRouteWiseChartAdapter.ChildLayoutAdapter.MyViewHolder> {

        private LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>> linkedHashMap;
        private Context context;
        private List<String> key_list_V2;

        public ChildLayoutAdapter(Context mContext, LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>> linkedHashMap) {
            this.context = mContext;
            this.linkedHashMap = linkedHashMap;
            this.key_list_V2 = new ArrayList<>(this.linkedHashMap.keySet());
        }


        @NonNull
        @Override
        public ChildLayoutAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_route_wise_chart_child, parent, false);
            return new ChildLayoutAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ChildLayoutAdapter.MyViewHolder holder, int listPosition) {

            holder.txt_branch_name.setText(key_list_V2.get(listPosition));
            holder.txt_branch_name.setPaintFlags(holder.txt_branch_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            List<String> list = getSubRouteWiseTotal(0, linkedHashMap, listPosition);
            holder.txt_sub_branch_total_seat.setText(list.get(0));
            holder.txt_sub_branch_total_amt.setText(list.get(1));

            holder.rv_sub_route.setAdapter(new NestedChildLayoutAdapter(context, linkedHashMap.get(key_list_V2.get(listPosition))));

        }

        @Override
        public int getItemCount() {
            return key_list_V2.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private RecyclerView rv_sub_route;
            private TextView txt_branch_name, txt_sub_branch_total_seat, txt_sub_branch_total_amt;

            MyViewHolder(View rowView) {
                super(rowView);
                txt_branch_name = rowView.findViewById(R.id.txt_branch_name);
                rv_sub_route = rowView.findViewById(R.id.rv_sub_route);
                txt_sub_branch_total_seat = rowView.findViewById(R.id.txt_sub_branch_total_seat);
                txt_sub_branch_total_amt = rowView.findViewById(R.id.txt_sub_branch_total_amt);

            }

        }


    }


    public class NestedChildLayoutAdapter extends RecyclerView.Adapter<SubRouteWiseChartAdapter.NestedChildLayoutAdapter.MyViewHolder> {

        List<SubRouteWiseChart_Response.Datum> SeatDataList;
        private Context context;


        public NestedChildLayoutAdapter(Context mContext, List<SubRouteWiseChart_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_route_wise_chart_nested_child, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
            positionCounter = positionCounter + 1;
            holder.txt_sr_no.setText(String.valueOf(positionCounter));
            holder.txt_pnr_tkt.setText(SeatDataList.get(listPosition).getPNRNO());
            holder.txt_user_id.setText(String.valueOf(SeatDataList.get(listPosition).getUserID()));
            holder.txt_total_seat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.txt_seats.setText(SeatDataList.get(listPosition).getSeatNo());

            holder.txt_customer.setText(SeatDataList.get(listPosition).getCustomerName());
            holder.txt_phone.setText(SeatDataList.get(listPosition).getPhone());
            holder.txt_pickup_name.setText(SeatDataList.get(listPosition).getPickUpName());
            holder.txt_booking_type.setText(SeatDataList.get(listPosition).getBookingType());
            holder.txt_amount.setText(String.valueOf(SeatDataList.get(listPosition).getJMPayableAmount()));
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_pnr_tkt, txt_user_id, txt_total_seat, txt_seats, txt_customer, txt_phone, txt_pickup_name, txt_booking_type, txt_amount, txt_remarks;

            MyViewHolder(View rowView) {
                super(rowView);

                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_pnr_tkt = rowView.findViewById(R.id.txt_pnr_tkt);
                txt_user_id = rowView.findViewById(R.id.txt_user_id);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_customer = rowView.findViewById(R.id.txt_customer_name);
                txt_phone = rowView.findViewById(R.id.txt_phone);
                txt_pickup_name = rowView.findViewById(R.id.txt_pickup_name);
                txt_booking_type = rowView.findViewById(R.id.txt_booking_type);
                txt_amount = rowView.findViewById(R.id.txt_amount);

            }
        }
    }

    private List<String> getSubRouteWiseTotal(int IsSubRouteTotal, LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>> linkedHashMap, int position) {
        // IsSubRouteTotal=1 , BranchTotal=0
        int totalRouteSeat = 0;
        double totalRouteAmt = 0.0;
        List<String> key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        if (IsSubRouteTotal == 1) {
            for (int i = 0; i < linkedHashMap.size(); i++) {
                for (SubRouteWiseChart_Response.Datum data : linkedHashMap.get(key_list_V2.get(i))) {
                    if (!TextUtils.isEmpty(data.getTotalSeat())) {
                        totalRouteSeat += Integer.parseInt(data.getTotalSeat());
                    }
                    totalRouteAmt += data.getJMPayableAmount();
                }
            }
        } else {
            for (SubRouteWiseChart_Response.Datum data : linkedHashMap.get(key_list_V2.get(position))) {
                if (!TextUtils.isEmpty(data.getTotalSeat())) {
                    totalRouteSeat += Integer.parseInt(data.getTotalSeat());
                }
                totalRouteAmt += data.getJMPayableAmount();
            }
        }

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.valueOf(totalRouteAmt));
        return arrayList;

    }

}
