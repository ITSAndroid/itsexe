package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.SeatNumberWiseChart_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SeatNumberWiseChartAdapter extends RecyclerView.Adapter<SeatNumberWiseChartAdapter.MyViewHolder> {

    private Context context;
    private List<SeatNumberWiseChart_Response.Detail> listBusAvailableRoute;
    private int positionCounter = 0;

    public SeatNumberWiseChartAdapter(Context mContext, List<SeatNumberWiseChart_Response.Detail> listContactUs) {
        this.context = mContext;
        this.listBusAvailableRoute = listContactUs;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_sr_no, txt_pnr_no, txt_tkt_no, txt_bk_type, txt_seat_no, txt_pass_name, txt_phone_no, txt_sub_route, txt_pickup_name, txt_booking_by, txt_amount;

        public MyViewHolder(View itemView) {
            super(itemView);


            txt_sr_no = itemView.findViewById(R.id.txt_sr_no);
            txt_pnr_no = itemView.findViewById(R.id.txt_pnr_no);
            txt_tkt_no = itemView.findViewById(R.id.txt_tkt_no);
            txt_seat_no = itemView.findViewById(R.id.txt_seat_no);
            txt_pass_name = itemView.findViewById(R.id.txt_pass_name);
            txt_phone_no = itemView.findViewById(R.id.txt_phone_no);
            txt_sub_route = itemView.findViewById(R.id.txt_sub_route);
            txt_pickup_name = itemView.findViewById(R.id.txt_pickup_name);
            txt_booking_by = itemView.findViewById(R.id.txt_booking_by);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_bk_type = itemView.findViewById(R.id.txt_bk_type);


        }
    }


    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView;
        iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seatnumber_wisechart_report, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        positionCounter = positionCounter + 1;

        SeatNumberWiseChart_Response.Detail contact_data = listBusAvailableRoute.get(listPosition);


        holder.txt_sr_no.setText(String.valueOf(positionCounter));
        holder.txt_pnr_no.setText(contact_data.getPNRNO());
        holder.txt_tkt_no.setText(contact_data.getTicketNo());
        holder.txt_bk_type.setText(contact_data.getBookingTypeName());
        holder.txt_seat_no.setText(contact_data.getSeatNo());
        holder.txt_pass_name.setText(contact_data.getCustomerName());
        holder.txt_phone_no.setText(contact_data.getPhone());
        holder.txt_sub_route.setText(contact_data.getFromCity());
        holder.txt_pickup_name.setText(contact_data.getPickUpName());
        holder.txt_booking_by.setText(contact_data.getBookbyUser());
        holder.txt_amount.setText(contact_data.getAmount());


    }

    @Override
    public int getItemCount() {
        return listBusAvailableRoute.size();
    }

}



