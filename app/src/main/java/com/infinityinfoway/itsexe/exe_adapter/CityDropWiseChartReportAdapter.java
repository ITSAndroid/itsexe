package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.CityDropUpWiseChart_Response;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CityDropWiseChartReportAdapter extends RecyclerView.Adapter<CityDropWiseChartReportAdapter.MyViewHolder> {

    LinkedHashMap<String, List<CityDropUpWiseChart_Response.Datum>> hashMap_data;
    List<String> key_list;
    private Context context;
    private int positionCounter = 0;


    public CityDropWiseChartReportAdapter(Context mContext, LinkedHashMap<String, List<CityDropUpWiseChart_Response.Datum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_city_name, txt_city_wise_total_seat;
        RecyclerView rv_city_drop_item;

        MyViewHolder(View rowView) {
            super(rowView);

            txt_city_wise_total_seat = rowView.findViewById(R.id.txt_city_wise_total_seat);
            txt_city_name = rowView.findViewById(R.id.txt_city_name);
            rv_city_drop_item = rowView.findViewById(R.id.rv_city_drop_item);

        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView;
        iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_citywise_drop_report, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.txt_city_name.setText(key_list.get(listPosition));
        holder.txt_city_wise_total_seat.setText("City Wise Total Seats: " + getRouteTotalSeatAmt(hashMap_data.get(key_list.get(listPosition))).get(0));

        holder.rv_city_drop_item.setAdapter(new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition))));


    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        List<CityDropUpWiseChart_Response.Datum> SeatDataList;
        private Context context;


        public ChildLayoutAdapter(Context mContext, List<CityDropUpWiseChart_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_drop_name, txt_sr_no, txt_total_seat, txt_seat_no;


            MyViewHolder(View rowView) {
                super(rowView);
                txt_drop_name = rowView.findViewById(R.id.txt_drop_name);
                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seat_no = rowView.findViewById(R.id.txt_seat_no);
            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView;
            iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_dropwise_chart_childlayout, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


            positionCounter = positionCounter + 1;
            holder.txt_sr_no.setText(String.valueOf(positionCounter));
            holder.txt_drop_name.setText(SeatDataList.get(listPosition).getDropName());
            holder.txt_total_seat.setText(String.valueOf(SeatDataList.get(listPosition).getTotalSeat()));
            holder.txt_seat_no.setText(SeatDataList.get(listPosition).getSeatNo());


        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

    }

    private List<String> getRouteTotalSeatAmt(List<CityDropUpWiseChart_Response.Datum> list) {
        int totalRouteSeat = 0;
        for (CityDropUpWiseChart_Response.Datum data : list) {
            totalRouteSeat += data.getTotalSeat();
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        return arrayList;
    }

}
