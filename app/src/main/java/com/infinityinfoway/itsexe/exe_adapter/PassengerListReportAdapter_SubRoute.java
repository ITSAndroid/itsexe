package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PassengerList_Response;
import com.infinityinfoway.itsexe.databinding.ItemPassengerListReportSubrouteBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PassengerListReportAdapter_SubRoute extends RecyclerView.Adapter<PassengerListReportAdapter_SubRoute.MyViewHolder> {

    private LinkedHashMap<String, List<PassengerList_Response.Datum>> linkedHashMap;
    private List<String> key_list;
    private Context context;

    public PassengerListReportAdapter_SubRoute(Context context, LinkedHashMap<String, List<PassengerList_Response.Datum>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPassengerListReportSubrouteBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.txtRoute.setText(key_list.get(position));
        holder.binding.txtRouteCounter.setText(String.valueOf(linkedHashMap.get(key_list.get(position)).size()));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemPassengerListReportSubrouteBinding binding;

        public MyViewHolder(ItemPassengerListReportSubrouteBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
