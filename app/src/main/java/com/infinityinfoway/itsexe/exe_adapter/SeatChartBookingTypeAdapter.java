package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.bean.BookingTypeSeatCounterBean;
import com.infinityinfoway.itsexe.database.model.ITSFetchChartForeColor_Model;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import java.util.List;

public class SeatChartBookingTypeAdapter extends RecyclerView.Adapter<SeatChartBookingTypeAdapter.MyViewHolder> {
    private Context context;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML> bookingTypeList;
    private OnItemClickListener onItemClickListener;
    private List<ITSFetchChartForeColor_Model> chartForeColorModelList;
    private int borderColor = Color.parseColor("#000000");
    private List<BookingTypeSeatCounterBean> bookingTypeSeatCounterList;

    public SeatChartBookingTypeAdapter(Context context, List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML> bookingTypeList, List<ITSFetchChartForeColor_Model> chartForeColorModelList,
                                       List<BookingTypeSeatCounterBean> bookingTypeSeatCounterList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.bookingTypeList = bookingTypeList;
        this.chartForeColorModelList = chartForeColorModelList;
        this.bookingTypeSeatCounterList = bookingTypeSeatCounterList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seatchart_booking_type, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML data = bookingTypeList.get(position);

        int totalSeat = findBookingTypeTotalSeat(data.getBookingTypeId());

        holder.bind(bookingTypeList.get(position), onItemClickListener, totalSeat);


        holder.txt_booking_type_name.setText(bookingTypeList.get(position).getDefaultName() + ": " + totalSeat);

        /*if (bookingTypeList.get(position).getTotalSeatbook() != null) {
            holder.txt_booking_type_name.setText(bookingTypeList.get(position).getDefault_name() + ": " + bookingTypeList.get(position).getTotalSeatbook());
        } else {
            holder.txt_booking_type_name.setText(bookingTypeList.get(position).getDefault_name() + ": 0");
        }*/


        int startColor;
        if (!TextUtils.isEmpty(data.getColor()) && data.getColor().contains("#")) {
            startColor = Color.parseColor(data.getColor());
        } else {
            startColor = Color.parseColor("#" + data.getColor());
        }

        //int[] colors = {startColor, startColor};

        final GradientDrawable gradientDrawable = (GradientDrawable) holder.ll_seat_chart.getBackground();
        gradientDrawable.setColor(startColor);
        //GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        //gradientDrawable.setStroke(1, borderColor);

        holder.ll_seat_chart.setBackground(gradientDrawable);
        holder.txt_booking_type_name.setTextColor(getComplementaryColor(startColor));
        holder.ll_seat_chart.setTag(startColor);


    }

    @Override
    public int getItemCount() {
        return bookingTypeList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML item, int pos, int totalSeat);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_booking_type_name;
        private LinearLayout ll_seat_chart;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_booking_type_name = rowView.findViewById(R.id.txt_booking_type_name);
            ll_seat_chart = rowView.findViewById(R.id.ll_seat_chart);
        }

        void bind(final Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML item, final OnItemClickListener listener, int totalSeat) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (totalSeat > 0 && item.getBookingTypeId() != APP_CONSTANTS.EMPTY_SEAT_BOOKING) {
                        listener.onItemClick(item, getAdapterPosition(), totalSeat);
                    }
                }
            });
        }

    }

    private int getComplementaryColor(int color) {
        int R = color & 255;
        int G = (color >> 8) & 255;
        int B = (color >> 16) & 255;
        int A = (color >> 24) & 255;
        R = 255 - R;
        G = 255 - G;
        B = 255 - B;
        return R + (G << 8) + (B << 16) + (A << 24);
    }

    private int findBookingTypeTotalSeat(int bookingTypeId) {
        int totalSeat = 0;
        for (int i = 0; i < bookingTypeSeatCounterList.size(); i++) {
            if (bookingTypeSeatCounterList.get(i).getBookingTypeId() == bookingTypeId) {
                if (bookingTypeSeatCounterList.get(i).getJPGender().equalsIgnoreCase("MF")) {
                    totalSeat = 0;
                } else {
                    totalSeat = bookingTypeSeatCounterList.get(i).getTotalSeatbook();
                }

                break;
            }
        }
        return totalSeat;
    }

}
