package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.ReservationDetailReportWithAmount_Response;
import com.infinityinfoway.itsexe.databinding.ItemReservationDetailReportWithAmountChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemReservationDetailReportWithAmountParentBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ReservationDetailReportWithAmountAdapter extends RecyclerView.Adapter<ReservationDetailReportWithAmountAdapter.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>> linkedHashMap;
    private List<String> key_list;

    public ReservationDetailReportWithAmountAdapter(Context context, LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemReservationDetailReportWithAmountParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtPickup.setText(key_list.get(position));
        holder.parentBinding.rvData.setAdapter(new ChildLayoutAdapter(context, linkedHashMap.get(key_list.get(position))));
        holder.parentBinding.txtSeats.setText("Seats : " + calTotalSeats(linkedHashMap.get(key_list.get(position))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemReservationDetailReportWithAmountParentBinding parentBinding;

        public MyViewHolder(ItemReservationDetailReportWithAmountParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {

        private Context context;
        private List<ReservationDetailReportWithAmount_Response.Datum> list;

        public ChildLayoutAdapter(Context context, List<ReservationDetailReportWithAmount_Response.Datum> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemReservationDetailReportWithAmountChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            ReservationDetailReportWithAmount_Response.Datum data = list.get(position);

            holder.childBinding.txtPnr.setText(data.getJMPNRNO());
            holder.childBinding.txtSeats.setText(data.getJMSeatList());
            holder.childBinding.txtName.setText(data.getJMPassengerName());
            holder.childBinding.txtPhone.setText(data.getJMPhone1());
            holder.childBinding.txtOrigin.setText(data.getRouteOrigin());
            holder.childBinding.txtDest.setText(data.getRouteDestination());
            holder.childBinding.txtAmount.setText(String.format("%.2f", data.getJMPayableAmount()));
            holder.childBinding.txtIssueBy.setText(data.getBookedBy());
            holder.childBinding.txtRemarks.setText(data.getJMRemarks());

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemReservationDetailReportWithAmountChildBinding childBinding;

            public MyViewHolder(ItemReservationDetailReportWithAmountChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }
    }

    private String calTotalSeats(List<ReservationDetailReportWithAmount_Response.Datum> list) {
        int totalSeat = 0;
        for (ReservationDetailReportWithAmount_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getJMTotalPassengers())) {
                totalSeat += Integer.parseInt(data.getJMTotalPassengers());
            }
        }
        return String.valueOf(totalSeat);
    }

}
