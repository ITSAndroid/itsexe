package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.SeatWiseInquiryReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemSeatwiseInquiryReportBinding;

import java.util.List;

public class SeatWiseInquiryReportAdapter extends RecyclerView.Adapter<SeatWiseInquiryReportAdapter.MyViewHolder> {

    private Context context;
    private List<SeatWiseInquiryReport_Response.Datum> list;

    public SeatWiseInquiryReportAdapter(Context context, List<SeatWiseInquiryReport_Response.Datum> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemSeatwiseInquiryReportBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SeatWiseInquiryReport_Response.Datum data = list.get(position);

        holder.binding.txtReport.setText(data.getReported());
        holder.binding.txtPnr.setText(data.getJMTicketNo());
        holder.binding.txtUserId.setText(data.getBUMUserName());
        holder.binding.txtSeats.setText(data.getSeatNo());
        holder.binding.txtCustomer.setText(data.getJMPassengerName());
        holder.binding.txtCustPhone.setText(data.getJMPhone1());
        holder.binding.txtCity.setText(data.getFromCity() + "-" + data.getToCity());
        holder.binding.txtPickup.setText(data.getPickUpName());
        holder.binding.txtType.setText(data.getBookingType());
        holder.binding.txtFare.setText(String.format("%.2f", data.getFare()));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemSeatwiseInquiryReportBinding binding;

        public MyViewHolder(ItemSeatwiseInquiryReportBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
