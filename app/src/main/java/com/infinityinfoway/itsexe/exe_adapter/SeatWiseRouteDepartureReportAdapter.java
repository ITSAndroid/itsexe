package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.SeatWiseRouteDepartureReport_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SeatWiseRouteDepartureReportAdapter extends RecyclerView.Adapter<SeatWiseRouteDepartureReportAdapter.MyViewHolder> {

    private Context context;
    private List<SeatWiseRouteDepartureReport_Response.Datum> listBusAvailableRoute;


    public SeatWiseRouteDepartureReportAdapter(Context mContext, List<SeatWiseRouteDepartureReport_Response.Datum> listContactUs) {
        this.context = mContext;
        this.listBusAvailableRoute = listContactUs;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_name, txt_mobile, txt_to, txt_pnr, txt_agent_branch, txt_seat_no, txt_fare, txt_ag_tkt_no;

        public MyViewHolder(View itemView) {
            super(itemView);


            txt_name = itemView.findViewById(R.id.txt_name);
            txt_mobile = itemView.findViewById(R.id.txt_mobile);
            txt_to = itemView.findViewById(R.id.txt_to);
            txt_pnr = itemView.findViewById(R.id.txt_pnr);
            txt_agent_branch = itemView.findViewById(R.id.txt_agent_branch);
            txt_seat_no = itemView.findViewById(R.id.txt_seat_no);
            txt_fare = itemView.findViewById(R.id.txt_fare);
            txt_ag_tkt_no = itemView.findViewById(R.id.txt_ag_tkt_no);

        }
    }


    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_routedeparture_report, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        SeatWiseRouteDepartureReport_Response.Datum contact_data = listBusAvailableRoute.get(listPosition);

        holder.txt_name.setText(contact_data.getPassengerName());
        holder.txt_mobile.setText(contact_data.getJMPhone1());
        holder.txt_to.setText(contact_data.getToCity());
        holder.txt_pnr.setText(contact_data.getJmPnrno());
        holder.txt_agent_branch.setText(contact_data.getAMAgentName());
        holder.txt_seat_no.setText(contact_data.getSeatNo());
        holder.txt_fare.setText(contact_data.getFare());
        holder.txt_ag_tkt_no.setText(contact_data.getJMTicketNo());


    }

    @Override
    public int getItemCount() {
        return listBusAvailableRoute.size();
    }

}
