package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.SeatAllocationChart_Response;
import com.infinityinfoway.itsexe.bean.CargoReportBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class SeatAllocationChartAdapter extends RecyclerView.Adapter<SeatAllocationChartAdapter.MyViewHolder> {

    LinkedHashMap<String, List<SeatAllocationChart_Response.Datum>> hashMap_data;
    List<String> key_list;
    private Context context;
    private int positionCounter = 0;
    LinearLayoutManager linearLayoutManager;


    public interface OnItemClickListener {
        void onItemClick(CargoReportBean item);
    }

    public SeatAllocationChartAdapter(Context mContext, LinkedHashMap<String, List<SeatAllocationChart_Response.Datum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title_allocation, txt_total_seat, txt_base_fare, txt_gst, txt_total_amount;
        RecyclerView recycler_data_allocation;

        MyViewHolder(View rowView) {
            super(rowView);

            title_allocation = rowView.findViewById(R.id.title_allocation);
            txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
            txt_base_fare = rowView.findViewById(R.id.txt_base_fare);
            txt_gst = rowView.findViewById(R.id.txt_gst);
            txt_total_amount = rowView.findViewById(R.id.txt_total_amount);
            recycler_data_allocation = rowView.findViewById(R.id.recycler_data_allocation);

        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seat_allocation_wisechart_report, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.title_allocation.setText(key_list.get(listPosition));

        List<String> list = getTotalAmount(hashMap_data.get(key_list.get(listPosition)));
        holder.txt_total_seat.setText(list.get(0));
        holder.txt_base_fare.setText(list.get(1));
        holder.txt_gst.setText(list.get(2));
        holder.txt_total_amount.setText(list.get(3));

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition)));
        holder.recycler_data_allocation.setAdapter(childLayoutAdapter);

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        List<SeatAllocationChart_Response.Datum> SeatDataList;
        private Context context;


        public ChildLayoutAdapter(Context mContext, List<SeatAllocationChart_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_serial_no, txt_name, total_seat, txt_seat_no, txt_base_fare, txt_gst, txt_total_amt;


            MyViewHolder(View rowView) {
                super(rowView);
                txt_serial_no = rowView.findViewById(R.id.txt_serial_no);
                txt_name = rowView.findViewById(R.id.txt_name);
                total_seat = rowView.findViewById(R.id.total_seat);
                txt_seat_no = rowView.findViewById(R.id.txt_seat_no);
                txt_base_fare = rowView.findViewById(R.id.txt_base_fare);
                txt_gst = rowView.findViewById(R.id.txt_gst);
                txt_total_amt = rowView.findViewById(R.id.txt_total_amt);
            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.seat_allocation_wisechart_child_layout, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            positionCounter = positionCounter + 1;
            holder.txt_serial_no.setText(positionCounter + "");
            holder.txt_name.setText(SeatDataList.get(listPosition).getSubRouteName());
            holder.total_seat.setText(String.valueOf(SeatDataList.get(listPosition).getTotalSeat()));
            holder.txt_seat_no.setText(SeatDataList.get(listPosition).getSeatNo());

            holder.txt_base_fare.setText(String.format("%.2f",SeatDataList.get(listPosition).getBaseFare()));
            holder.txt_gst.setText(String.format("%.2f",SeatDataList.get(listPosition).getGST()));
            holder.txt_total_amt.setText(String.format("%.2f",SeatDataList.get(listPosition).getAmount()));

        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }


    }

    private List<String> getTotalAmount(List<SeatAllocationChart_Response.Datum> SeatDataList) {
        double totalBaseFare = 0.0, totalGST = 0.0, totalAmount = 0.0;
        int totalSeat = 0;
        for (SeatAllocationChart_Response.Datum data : SeatDataList) {
            totalSeat += data.getTotalSeat();
            totalBaseFare += data.getBaseFare();
            totalGST += data.getGST();
            totalAmount += data.getAmount();
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.format("%.2f", totalBaseFare));
        arrayList.add(String.format("%.2f", totalGST));
        arrayList.add(String.format("%.2f", totalAmount));
        return arrayList;
    }

}
