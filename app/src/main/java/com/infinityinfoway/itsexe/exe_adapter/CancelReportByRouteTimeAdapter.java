package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_Report_Booking_Cancel_ReportByRouteTime_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CancelReportByRouteTimeAdapter extends RecyclerView.Adapter<CancelReportByRouteTimeAdapter.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime>> linkedHashMap;
    private List<String> key_list;

    public CancelReportByRouteTimeAdapter(Context mContext, LinkedHashMap<String, List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime>> linkedHashMap) {
        this.context = mContext;
        this.linkedHashMap = linkedHashMap;
        this.key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cancel_report_byroute_time_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_grp_header.setText(key_list.get(listPosition));
        holder.txt_grp_header.setPaintFlags(holder.txt_grp_header.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, linkedHashMap.get(key_list.get(listPosition)));
        holder.rv_parent_cancel.setAdapter(childLayoutAdapter);
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_grp_header;
        RecyclerView rv_parent_cancel;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_grp_header = itemView.findViewById(R.id.txt_grp_header);
            rv_parent_cancel = itemView.findViewById(R.id.rv_parent_cancel);
        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {

        private Context context;
        private List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime> list;

        public ChildLayoutAdapter(Context context, List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cancel_report_byroute_time_child, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime contact_data = list.get(position);


            holder.txt_cancel_date.setText(contact_data.getJMCancelDate());
            holder.txt_cancel_branch.setText(contact_data.getCancelBranch());
            holder.txt_cancel_by.setText(contact_data.getCancelBy());
            holder.txt_type.setText(contact_data.getBookingType());
            holder.txt_booking_date.setText(contact_data.getJMBookingDateTime());
            holder.txt_branch_code.setText(contact_data.getBMBranchCode());
            holder.txt_user_code.setText(contact_data.getBUMUserName());
            holder.txt_pnr_tkt_no.setText(contact_data.getPNRTKT());
            holder.txt_below_7.setText(contact_data.getJourney());
            holder.txt_below_6.setText(contact_data.getJMJourneyCityTime());
            holder.txt_below_5.setText(contact_data.getJMCancelRemarks());
            holder.txt_below_4.setText(contact_data.getJMPayableAmount());
            holder.txt_below_3.setText(contact_data.getJMPhone1());
            holder.txt_below_2.setText(contact_data.getJMSeatList());
            holder.txt_below_1.setText(contact_data.getJMPassengerName());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_cancel_date, txt_cancel_branch, txt_cancel_by, txt_type, txt_booking_date, txt_branch_code, txt_user_code, txt_pnr_tkt_no;
            TextView txt_below_7, txt_below_6, txt_below_5, txt_below_4, txt_below_3, txt_below_2, txt_below_1;

            public MyViewHolder(View itemView) {
                super(itemView);
                txt_cancel_date = itemView.findViewById(R.id.txt_cancel_date);
                txt_cancel_branch = itemView.findViewById(R.id.txt_cancel_branch);
                txt_cancel_by = itemView.findViewById(R.id.txt_cancel_by);
                txt_type = itemView.findViewById(R.id.txt_type);
                txt_booking_date = itemView.findViewById(R.id.txt_booking_date);
                txt_branch_code = itemView.findViewById(R.id.txt_branch_code);
                txt_user_code = itemView.findViewById(R.id.txt_user_code);
                txt_pnr_tkt_no = itemView.findViewById(R.id.txt_pnr_tkt_no);
                txt_below_7 = itemView.findViewById(R.id.txt_below_7);
                txt_below_6 = itemView.findViewById(R.id.txt_below_6);
                txt_below_5 = itemView.findViewById(R.id.txt_below_5);
                txt_below_4 = itemView.findViewById(R.id.txt_below_4);
                txt_below_3 = itemView.findViewById(R.id.txt_below_3);
                txt_below_2 = itemView.findViewById(R.id.txt_below_2);
                txt_below_1 = itemView.findViewById(R.id.txt_below_1);
            }
        }

    }


}



