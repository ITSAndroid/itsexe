package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PassengerList_Response;
import com.infinityinfoway.itsexe.databinding.ItemPassengerListReportChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemPassengerListReportParentBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PassengerListReportAdapter extends RecyclerView.Adapter<PassengerListReportAdapter.MyViewHolder> {

    private HashMap<String, List<PassengerList_Response.Datum>> listHashMap;
    private List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public PassengerListReportAdapter(Context context, HashMap<String, List<PassengerList_Response.Datum>> listHashMap) {
        this.context = context;
        this.listHashMap = listHashMap;
        key_list = new ArrayList<>(listHashMap.keySet());
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPassengerListReportParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtPickupName.setText(key_list.get(position));
        holder.parentBinding.rvData.setAdapter(new ChildLayoutAdapter(context, listHashMap.get(key_list.get(position))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemPassengerListReportParentBinding parentBinding;

        public MyViewHolder(ItemPassengerListReportParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {
        private Context context;
        List<PassengerList_Response.Datum> list;

        public ChildLayoutAdapter(Context context, List<PassengerList_Response.Datum> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemPassengerListReportChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            positionCounter++;
            PassengerList_Response.Datum data = list.get(position);
            holder.childBinding.txtSrNo.setText(String.valueOf(positionCounter));
            holder.childBinding.txtSeatNo.setText(data.getSeatNo());
            holder.childBinding.txtPnr.setText(data.getPNRTicketNo());
            holder.childBinding.txtName.setText(data.getCustomerName());
            holder.childBinding.txtGender.setText(data.getJPGender());
            holder.childBinding.txtAge.setText(data.getJPAge());
            holder.childBinding.txtType.setText(data.getBookingType());
            holder.childBinding.txtRemarks.setText(data.getRemarks());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemPassengerListReportChildBinding childBinding;

            public MyViewHolder(ItemPassengerListReportChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }
    }

}
