package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.BranchUserWiseCollection_RefundChart_Response;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BranchUserWiseCollectionSummaryAdapter extends RecyclerView.Adapter<BranchUserWiseCollectionSummaryAdapter.MyViewHolder> {
    private LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection>> linkedHashMapCollection;
    List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public BranchUserWiseCollectionSummaryAdapter(LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection>> linkedHashMapCollection, Context context) {
        this.linkedHashMapCollection = linkedHashMapCollection;
        this.context = context;
        this.key_list = new ArrayList<>(linkedHashMapCollection.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch_user_wise_collection_summary_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_pickup_name.setText(key_list.get(listPosition));
        holder.txt_pickup_name.setPaintFlags(holder.txt_pickup_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, linkedHashMapCollection.get(key_list.get(listPosition)));
        holder.rv_parent_collection.setAdapter(childLayoutAdapter);
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_pickup_name, txt_route_name;
        RecyclerView rv_parent_collection;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_pickup_name = rowView.findViewById(R.id.txt_pickup_name);
            rv_parent_collection = rowView.findViewById(R.id.rv_parent_collection);
        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<BranchUserWiseCollectionSummaryAdapter.ChildLayoutAdapter.MyViewHolder> {


        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection> SeatDataList;
        private Context context;
        LinearLayoutManager linearLayoutManager;


        public ChildLayoutAdapter(Context mContext, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_pnr_no, txt_journey_date, txt_journey_date_time, txt_subroute_name;
            private TextView txt_total_pax, txt_seats, txt_amount;


            MyViewHolder(View rowView) {
                super(rowView);
                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_pnr_no = rowView.findViewById(R.id.txt_pnr_no);
                txt_journey_date = rowView.findViewById(R.id.txt_journey_date);
                txt_journey_date_time = rowView.findViewById(R.id.txt_journey_date_time);
                txt_subroute_name = rowView.findViewById(R.id.txt_subroute_name);
                txt_total_pax = rowView.findViewById(R.id.txt_total_pax);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_amount = rowView.findViewById(R.id.txt_amount);

            }
        }

        @NotNull
        @Override
        public ChildLayoutAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch_user_wise_collection_summary_child, parent, false);
            return new ChildLayoutAdapter.MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final ChildLayoutAdapter.MyViewHolder holder, final int listPosition) {

            BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection data = SeatDataList.get(listPosition);
            positionCounter += 1;
            holder.txt_sr_no.setText(positionCounter + "");
            if (!TextUtils.isEmpty(data.getSymbol())) {
                holder.txt_pnr_no.setText(data.getSymbol() + "  " + data.getPNRTicketNO() + "   ");
            } else {
                holder.txt_pnr_no.setText("    " + data.getPNRTicketNO() + "   ");
            }

            holder.txt_journey_date.setText(data.getJDate());
            holder.txt_journey_date_time.setText(data.getJMJourneyStartTime());
            if (TextUtils.isEmpty(data.getFromCity())) {
                if (!TextUtils.isEmpty(data.getDEMRemarks())) {
                    holder.txt_subroute_name.setText(data.getDEMRemarks());
                }
            } else {
                holder.txt_subroute_name.setText(data.getFromCity());
            }

            holder.txt_total_pax.setText(data.getJMTotalPassengers());
            holder.txt_seats.setText(data.getJMSeatList());
            holder.txt_amount.setText(String.format("%.2f", data.getJMPayableAmount()));

        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

    }


}
