package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.PassengerBookingReport_Response;
import com.infinityinfoway.itsexe.bean.CargoReportBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PassengerBookingReportAdapter extends RecyclerView.Adapter<PassengerBookingReportAdapter.MyViewHolder> {

    private LinkedHashMap<String, List<PassengerBookingReport_Response.Datum>> hashMap_data;
    List<String> key_list;
    private Context context;

    LinearLayoutManager linearLayoutManager;

    public interface OnItemClickListener {
        void onItemClick(CargoReportBean item);
    }

    public PassengerBookingReportAdapter(Context mContext, LinkedHashMap<String, List<PassengerBookingReport_Response.Datum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_pickup_name;
        RecyclerView recycler_passenger_data;

        MyViewHolder(View rowView) {
            super(rowView);

            txt_pickup_name = rowView.findViewById(R.id.txt_pickup_name);
            recycler_passenger_data = rowView.findViewById(R.id.recycler_passenger_data);

        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_passenger_booking_report, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.txt_pickup_name.setText(key_list.get(listPosition));

        linearLayoutManager = new LinearLayoutManager(context);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition)));
        holder.recycler_passenger_data.setLayoutManager(linearLayoutManager);
        holder.recycler_passenger_data.setAdapter(childLayoutAdapter);

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        List<PassengerBookingReport_Response.Datum> SeatDataList;
        private Context context;
        LinearLayoutManager linearLayoutManager;


        public ChildLayoutAdapter(Context mContext, List<PassengerBookingReport_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_pnr_tkt, txt_creation_date, txt_user_id, txt_total_seat, txt_seat_no, txt_customer, txt_phone, txt_city, txt_type, txt_status, txt_remarks;


            MyViewHolder(View rowView) {
                super(rowView);
                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_pnr_tkt = rowView.findViewById(R.id.txt_pnr_tkt);
                txt_creation_date = rowView.findViewById(R.id.txt_creation_date);
                txt_user_id = rowView.findViewById(R.id.txt_user_id);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seat_no = rowView.findViewById(R.id.txt_seat_no);
                txt_customer = rowView.findViewById(R.id.txt_customer);
                txt_phone = rowView.findViewById(R.id.txt_phone);
                txt_city = rowView.findViewById(R.id.txt_city);
                txt_type = rowView.findViewById(R.id.txt_type);
                txt_status = rowView.findViewById(R.id.txt_status);
                txt_remarks = rowView.findViewById(R.id.txt_remarks);

            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_passenger_bookingreport_childlayout, parent, false));
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


            holder.txt_sr_no.setText((listPosition + 1) + "");
            holder.txt_pnr_tkt.setText(SeatDataList.get(listPosition).getPNRTicketNo());
            holder.txt_creation_date.setText(SeatDataList.get(listPosition).getJourneyDate());
            holder.txt_user_id.setText(SeatDataList.get(listPosition).getUserName());
            holder.txt_total_seat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.txt_seat_no.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.txt_customer.setText(SeatDataList.get(listPosition).getCustomerName());
            holder.txt_phone.setText(SeatDataList.get(listPosition).getPhone());
            holder.txt_city.setText(SeatDataList.get(listPosition).getFromCity());
            holder.txt_type.setText(SeatDataList.get(listPosition).getBookingType());
            holder.txt_status.setText(SeatDataList.get(listPosition).getBookingType());
            holder.txt_remarks.setText(SeatDataList.get(listPosition).getRemarks());


        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }


    }

}