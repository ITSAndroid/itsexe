package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.RouteTimeWisePickupChart_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RouteTimeWisePickupChartAdapter extends RecyclerView.Adapter<RouteTimeWisePickupChartAdapter.MyViewHolder> {
    private LinkedHashMap<String, List<RouteTimeWisePickupChart_Response.Datum>> hashMap_data;
    private List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public RouteTimeWisePickupChartAdapter(Context context, LinkedHashMap<String, List<RouteTimeWisePickupChart_Response.Datum>> hashMap_data) {
        this.context = context;
        this.hashMap_data = hashMap_data;
        this.key_list = new ArrayList<>(hashMap_data.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_route_time_wise_pickup_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_pickup_name.setText(key_list.get(listPosition));
        holder.txt_pickup_total.setText("PickUp Wise Total Seats : " + getPickupWiseTotal(hashMap_data.get(key_list.get(listPosition))));
        holder.rv_pickup.setAdapter(new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_pickup_total, txt_pickup_name;
        RecyclerView rv_pickup;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_pickup_name = rowView.findViewById(R.id.txt_pickup_name);
            rv_pickup = rowView.findViewById(R.id.rv_pickup);
            txt_pickup_total = rowView.findViewById(R.id.txt_pickup_total);

        }
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<RouteTimeWisePickupChartAdapter.ChildLayoutAdapter.MyViewHolder> {

        List<RouteTimeWisePickupChart_Response.Datum> SeatDataList;
        private Context context;


        public ChildLayoutAdapter(Context mContext, List<RouteTimeWisePickupChart_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;
        }


        @NonNull
        @Override
        public ChildLayoutAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_route_time_wise_pickup_child, parent, false);
            return new ChildLayoutAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ChildLayoutAdapter.MyViewHolder holder, int listPosition) {
            positionCounter = positionCounter + 1;
            holder.txt_sr_no.setText(String.valueOf(positionCounter));
            holder.txt_tkt_no.setText(SeatDataList.get(listPosition).getTicketNo());
            holder.txt_user.setText(String.valueOf(SeatDataList.get(listPosition).getUserID()));
            holder.txt_total_seat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.txt_seats.setText(SeatDataList.get(listPosition).getSeatNo());

            holder.txt_customer.setText(SeatDataList.get(listPosition).getCustomerName());
            holder.txt_phone.setText(SeatDataList.get(listPosition).getPhone());
            holder.txt_route.setText(SeatDataList.get(listPosition).getFromCityToCity());
            holder.txt_booking_type.setText(SeatDataList.get(listPosition).getBookingType());
            holder.txt_remarks.setText(SeatDataList.get(listPosition).getRemarks());
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_tkt_no, txt_user, txt_total_seat, txt_seats, txt_customer, txt_phone, txt_route, txt_booking_type, txt_drop_off, txt_remarks;

            MyViewHolder(View rowView) {
                super(rowView);

                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_tkt_no = rowView.findViewById(R.id.txt_tkt_no);
                txt_user = rowView.findViewById(R.id.txt_user);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_customer = rowView.findViewById(R.id.txt_customer);
                txt_phone = rowView.findViewById(R.id.txt_phone);
                txt_route = rowView.findViewById(R.id.txt_route);
                txt_booking_type = rowView.findViewById(R.id.txt_booking_type);
                txt_drop_off = rowView.findViewById(R.id.txt_drop_off);
                txt_remarks = rowView.findViewById(R.id.txt_remarks);

            }
        }
    }


    private String getPickupWiseTotal(List<RouteTimeWisePickupChart_Response.Datum> list) {
        int totalRouteSeat = 0;
        for (RouteTimeWisePickupChart_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalRouteSeat += Integer.parseInt(data.getTotalSeat());
            }

        }
        return String.valueOf(totalRouteSeat);
    }

}
