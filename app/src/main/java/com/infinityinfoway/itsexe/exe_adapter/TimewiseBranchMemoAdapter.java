package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.TimewiseBranchMemo_Response;

import java.util.List;

public class TimewiseBranchMemoAdapter extends RecyclerView.Adapter<TimewiseBranchMemoAdapter.MyViewHolder> {

    private List<TimewiseBranchMemo_Response.Datum> list_allocation;
    private Context context;

    public TimewiseBranchMemoAdapter(Context context, List<TimewiseBranchMemo_Response.Datum> list_allocation) {
        this.list_allocation = list_allocation;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time_wise_branch_memo, parent, false);
        return new TimewiseBranchMemoAdapter.MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TimewiseBranchMemo_Response.Datum data = list_allocation.get(position);

        holder.txt_sr_no.setText(String.valueOf(position + 1));
        holder.txt_from_city.setText(data.getFromCityName());
        holder.txt_to_city.setText(data.getToCityName());
        holder.txt_seat_names.setText(data.getSeatNo());
        holder.txt_base_fare.setText(String.format("%.2f", data.getBaseFare()));
        holder.txt_gst.setText(String.format("%.2f", data.getServiceTax()));
        holder.txt_amt_seat.setText(String.format("%.2f", data.getAmount()));
        holder.txt_total_seats.setText(String.valueOf(data.getTotalSeat()));
        holder.txt_total_amt.setText(String.format("%.2f", data.getTotalAmount()));

    }

    @Override
    public int getItemCount() {
        return list_allocation.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_sr_no, txt_from_city, txt_to_city, txt_seat_names, txt_base_fare, txt_gst, txt_amt_seat, txt_total_seats, txt_total_amt;

        MyViewHolder(View rowView) {
            super(rowView);

            txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
            txt_from_city = rowView.findViewById(R.id.txt_from_city);
            txt_to_city = rowView.findViewById(R.id.txt_to_city);
            txt_seat_names = rowView.findViewById(R.id.txt_seat_names);
            txt_base_fare = rowView.findViewById(R.id.txt_base_fare);
            txt_gst = rowView.findViewById(R.id.txt_gst);
            txt_amt_seat = rowView.findViewById(R.id.txt_amt_seat);
            txt_total_seats = rowView.findViewById(R.id.txt_total_seats);
            txt_total_amt = rowView.findViewById(R.id.txt_total_amt);
        }
    }

}
