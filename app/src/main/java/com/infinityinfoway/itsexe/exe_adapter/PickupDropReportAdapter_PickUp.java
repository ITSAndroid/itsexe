package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PickupDropReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemPickupDropReportChildPickupwiseBinding;
import com.infinityinfoway.itsexe.databinding.ItemPickupDropReportParentPickupwiseBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PickupDropReportAdapter_PickUp extends RecyclerView.Adapter<PickupDropReportAdapter_PickUp.MyViewHolder> {

    LinkedHashMap<String, List<PickupDropReport_Response.PickUpDropWiseChart>> hashMap_data;
    List<String> key_list;
    private Context context;
    int positionCounter = 0;

    public PickupDropReportAdapter_PickUp(LinkedHashMap<String, List<PickupDropReport_Response.PickUpDropWiseChart>> hashMap_data, Context context) {
        this.hashMap_data = hashMap_data;
        this.context = context;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPickupDropReportParentPickupwiseBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtPickupName.setText(key_list.get(position));
        holder.parentBinding.rvPickup.setAdapter(new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(position))));
        holder.parentBinding.pickupWiseTotalSeat.setText("Pickup Wise Total Seats: "+calTotalSeat(hashMap_data.get(key_list.get(position))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemPickupDropReportParentPickupwiseBinding parentBinding;

        public MyViewHolder(ItemPickupDropReportParentPickupwiseBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewholder> {

        List<PickupDropReport_Response.PickUpDropWiseChart> SeatDataList;
        private Context context;

        public ChildLayoutAdapter(Context context, List<PickupDropReport_Response.PickUpDropWiseChart> seatDataList) {
            SeatDataList = seatDataList;
            this.context = context;
        }

        @NonNull
        @Override
        public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewholder(ItemPickupDropReportChildPickupwiseBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewholder holder, int listPosition) {

            positionCounter++;

            holder.childBinding.txtSrNo.setText(String.valueOf(positionCounter));
            holder.childBinding.txtTktNo.setText(SeatDataList.get(listPosition).getTicketNo());
            holder.childBinding.txtUser.setText(SeatDataList.get(listPosition).getUserID());
            holder.childBinding.txtSeat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.childBinding.txtSeats.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.childBinding.txtCustomer.setText(SeatDataList.get(listPosition).getCustomerName());
            holder.childBinding.txtPhone.setText(SeatDataList.get(listPosition).getPhone());
            holder.childBinding.txtRouteName.setText(SeatDataList.get(listPosition).getFromCity());
            holder.childBinding.txtBookingType.setText(SeatDataList.get(listPosition).getBookingType());
            holder.childBinding.txtRemarks.setText(SeatDataList.get(listPosition).getRemarks());
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewholder extends RecyclerView.ViewHolder {
            private ItemPickupDropReportChildPickupwiseBinding childBinding;

            public MyViewholder(ItemPickupDropReportChildPickupwiseBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }

    }

    private String calTotalSeat(List<PickupDropReport_Response.PickUpDropWiseChart> list) {
        int totalSeat = 0;
        for (PickupDropReport_Response.PickUpDropWiseChart data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSeat += Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSeat);

    }

}
