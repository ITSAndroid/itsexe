package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.CityWiseSeatChartReport_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CityWiseSeatTypeChartAdapter extends RecyclerView.Adapter<CityWiseSeatTypeChartAdapter.MyViewHolder> {

    private Context context;
    private List<String> key_list;
    private LinkedHashMap<String, LinkedHashMap<String, List<CityWiseSeatChartReport_Response.Datum>>> linkedHashMap1;

    public CityWiseSeatTypeChartAdapter(Context context, LinkedHashMap<String, LinkedHashMap<String, List<CityWiseSeatChartReport_Response.Datum>>> linkedHashMap1) {
        this.context = context;
        this.linkedHashMap1 = linkedHashMap1;
        this.key_list = new ArrayList<>(linkedHashMap1.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView;
        iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_wise_seat_type_chart_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_ser.setText(key_list.get(listPosition));
        holder.txt_ser.setPaintFlags(holder.txt_ser.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(linkedHashMap1.get(key_list.get(listPosition)), context);
        holder.rv_branch.setAdapter(childLayoutAdapter);
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_ser;
        RecyclerView rv_branch;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_ser = rowView.findViewById(R.id.txt_ser);
            rv_branch = rowView.findViewById(R.id.rv_branch);
        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {

        private LinkedHashMap<String, List<CityWiseSeatChartReport_Response.Datum>> linkedHashMap;
        private LinearLayoutManager linearLayoutManagerChild;
        private Context context;
        private List<String> key_list_V2;

        public ChildLayoutAdapter(LinkedHashMap<String, List<CityWiseSeatChartReport_Response.Datum>> linkedHashMap, Context context) {
            this.linkedHashMap = linkedHashMap;
            this.context = context;
            key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_wise_seat_type_chart_child, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
            linearLayoutManagerChild = new LinearLayoutManager(context);
            holder.txt_cmp_name.setText(key_list_V2.get(listPosition));
            holder.txt_cmp_name.setPaintFlags(holder.txt_cmp_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            holder.rv_seat_details.setLayoutManager(linearLayoutManagerChild);
            holder.rv_seat_details.setAdapter(new NestedChildLayoutAdapter(context, linkedHashMap.get(key_list_V2.get(listPosition))));

            List<String> arrayList = getSeatDetailsData(linkedHashMap.get(key_list_V2.get(listPosition)));

            holder.total_seat.setText(arrayList.get(0));
            holder.txt_total_slp.setText(arrayList.get(1));
            holder.txt_seat_amount.setText(arrayList.get(2));
            holder.txt_slp_amount.setText(arrayList.get(3));
            holder.txt_commission.setText(arrayList.get(4));
            holder.txt_net_amt.setText(arrayList.get(5));
        }

        @Override
        public int getItemCount() {
            return key_list_V2.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_cmp_name, total_seat, txt_total_slp, txt_seat_amount, txt_slp_amount, txt_commission, txt_net_amt;
            private RecyclerView rv_seat_details;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_cmp_name = itemView.findViewById(R.id.txt_cmp_name);
                rv_seat_details = itemView.findViewById(R.id.rv_seat_details);
                total_seat = itemView.findViewById(R.id.total_seat);
                txt_total_slp = itemView.findViewById(R.id.txt_total_slp);
                txt_seat_amount = itemView.findViewById(R.id.txt_seat_amount);
                txt_slp_amount = itemView.findViewById(R.id.txt_slp_amount);
                txt_commission = itemView.findViewById(R.id.txt_commission);
                txt_net_amt = itemView.findViewById(R.id.txt_net_amt);
            }
        }

    }

    public class NestedChildLayoutAdapter extends RecyclerView.Adapter<NestedChildLayoutAdapter.MyViewHolder> {
        private Context context;
        private List<CityWiseSeatChartReport_Response.Datum> SeatDataList;

        public NestedChildLayoutAdapter(Context context, List<CityWiseSeatChartReport_Response.Datum> seatDataList) {
            this.context = context;
            SeatDataList = seatDataList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_wise_seat_type_chart_nested_child, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            CityWiseSeatChartReport_Response.Datum data = SeatDataList.get(position);

            holder.txt_from.setText(data.getFromCity());
            holder.txt_to.setText(data.getTocity());
            holder.txt_total_seat.setText(String.valueOf(data.getSeat()));
            holder.txt_total_slp.setText(String.valueOf(data.getSLP()));
            holder.txt_seat_amount.setText(data.getSEATFARE());
            holder.txt_slp_amount.setText(data.getSLPFARE());
            holder.txt_commission.setText(data.getBBICommAmount());
            holder.txt_net_amt.setText(data.getBBINetAmount());

        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_from, txt_to, txt_total_seat, txt_total_slp, txt_seat_amount, txt_slp_amount, txt_commission, txt_net_amt;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_from = itemView.findViewById(R.id.txt_from);
                txt_to = itemView.findViewById(R.id.txt_to);
                txt_total_seat = itemView.findViewById(R.id.txt_total_seat);
                txt_total_slp = itemView.findViewById(R.id.txt_total_slp);
                txt_seat_amount = itemView.findViewById(R.id.txt_seat_amount);
                txt_slp_amount = itemView.findViewById(R.id.txt_slp_amount);
                txt_commission = itemView.findViewById(R.id.txt_commission);
                txt_net_amt = itemView.findViewById(R.id.txt_net_amt);

            }
        }
    }

    private List<String> getSeatDetailsData(List<CityWiseSeatChartReport_Response.Datum> SeatDataList) {
        double totalSeatAmt = 0.0, totalSlpAmt = 0.0, totalComm = 0.0, totalNetAmt = 0.0;
        int totalSeat = 0, totalSlp = 0;
        List<String> arrayList = new ArrayList<>();
        for (CityWiseSeatChartReport_Response.Datum data : SeatDataList) {
            totalSeat += data.getSeat();
            totalSlp += data.getSLP();
            totalSeatAmt += Double.parseDouble(data.getSEATFARE());
            totalSlpAmt += Double.parseDouble(data.getSLPFARE());
            totalComm += Double.parseDouble(data.getBBICommAmount());
            totalNetAmt += Double.parseDouble(data.getBBINetAmount());
        }
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.valueOf(totalSlp));
        arrayList.add(String.valueOf(totalSeatAmt));
        arrayList.add(String.valueOf(totalSlpAmt));
        arrayList.add(String.valueOf(totalComm));
        arrayList.add(String.valueOf(totalNetAmt));
        return arrayList;
    }

}
