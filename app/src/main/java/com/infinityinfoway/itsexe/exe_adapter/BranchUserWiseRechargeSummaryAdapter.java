package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.BranchUserWiseCollection_RefundChart_Response;

import java.util.List;

public class BranchUserWiseRechargeSummaryAdapter extends RecyclerView.Adapter<BranchUserWiseRechargeSummaryAdapter.MyViewHolder> {
    private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseAgentRecharge> list;
    private Context context;

    public BranchUserWiseRechargeSummaryAdapter(Context context,List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseAgentRecharge> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch_user_wise_collection_recharge_summary, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BranchUserWiseCollection_RefundChart_Response.BranchUserWiseAgentRecharge data = list.get(position);
        holder.txt_sr_no.setText(String.valueOf(position + 1));
        holder.txt_agent.setText(data.getCAMSetAgentName());
        holder.txt_amount.setText(String.format("%.2f", data.getCARMRecharge()));
        holder.txt_date.setText(data.getTransDate());
        holder.txt_remarks.setText(data.getCRAMRemarks());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_sr_no, txt_agent, txt_amount, txt_date, txt_remarks;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
            txt_agent = rowView.findViewById(R.id.txt_agent);
            txt_amount = rowView.findViewById(R.id.txt_amount);
            txt_date = rowView.findViewById(R.id.txt_date);
            txt_remarks = rowView.findViewById(R.id.txt_remarks);
        }
    }

}
