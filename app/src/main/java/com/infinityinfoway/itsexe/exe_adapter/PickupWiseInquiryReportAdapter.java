package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.PickupWiseInquiry_Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PickupWiseInquiryReportAdapter extends RecyclerView.Adapter<PickupWiseInquiryReportAdapter.MyViewHolder> {

    LinkedHashMap<String, List<PickupWiseInquiry_Response.MainDatum>> hashMap_data;
    List<String> key_list;
    private Context context;

    public PickupWiseInquiryReportAdapter(Context mContext, LinkedHashMap<String, List<PickupWiseInquiry_Response.MainDatum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pickup_wise_inquiry_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_pickup_name.setText(key_list.get(listPosition));
        holder.txt_pickup_total.setText("PickUp Wise Total Seats : " + getPickupWiseTotal(hashMap_data.get(key_list.get(listPosition))));
        holder.txt_pickup_name.setPaintFlags(holder.txt_pickup_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.rv_pickup.setAdapter(new PickupWiseInquiryReportAdapter.ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition))));

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_pickup_total, txt_pickup_name;
        RecyclerView rv_pickup;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_pickup_name = rowView.findViewById(R.id.txt_pickup_name);
            rv_pickup = rowView.findViewById(R.id.rv_pickup);
            txt_pickup_total = rowView.findViewById(R.id.txt_pickup_total);

        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<PickupWiseInquiryReportAdapter.ChildLayoutAdapter.MyViewHolder> {

        List<PickupWiseInquiry_Response.MainDatum> SeatDataList;
        private Context context;


        public ChildLayoutAdapter(Context mContext, List<PickupWiseInquiry_Response.MainDatum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;
        }


        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pickup_wise_inquiry_child, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
            holder.txt_pnr_no.setText(SeatDataList.get(listPosition).getPNRNO());
            holder.txt_tkt_no.setText(SeatDataList.get(listPosition).getTicketNo());
            holder.txt_user_name.setText(SeatDataList.get(listPosition).getUserName());
            holder.txt_total_seat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.txt_seats.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.txt_customer_name.setText(SeatDataList.get(listPosition).getCustomerName());
            holder.txt_phone.setText(SeatDataList.get(listPosition).getPhone());
            holder.txt_to.setText(SeatDataList.get(listPosition).getToCity());
            holder.txt_booking_type.setText(SeatDataList.get(listPosition).getBookingType());
            holder.txt_drop_off.setText(SeatDataList.get(listPosition).getDropName());
            holder.txt_remarks.setText(SeatDataList.get(listPosition).getRemarks());
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_pnr_no, txt_tkt_no, txt_user_name, txt_total_seat, txt_seats, txt_customer_name, txt_phone, txt_to, txt_booking_type, txt_drop_off, txt_remarks;

            MyViewHolder(View rowView) {
                super(rowView);

                txt_pnr_no = rowView.findViewById(R.id.txt_pnr_no);
                txt_tkt_no = rowView.findViewById(R.id.txt_tkt_no);
                txt_user_name = rowView.findViewById(R.id.txt_user_name);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_customer_name = rowView.findViewById(R.id.txt_customer_name);
                txt_phone = rowView.findViewById(R.id.txt_phone);
                txt_to = rowView.findViewById(R.id.txt_to);
                txt_booking_type = rowView.findViewById(R.id.txt_booking_type);
                txt_drop_off = rowView.findViewById(R.id.txt_drop_off);
                txt_remarks = rowView.findViewById(R.id.txt_remarks);

            }
        }
    }

    private String getPickupWiseTotal(List<PickupWiseInquiry_Response.MainDatum> list) {
        int totalRouteSeat = 0;
        for (PickupWiseInquiry_Response.MainDatum data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalRouteSeat += Integer.parseInt(data.getTotalSeat());
            }

        }
        return String.valueOf(totalRouteSeat);
    }


}
