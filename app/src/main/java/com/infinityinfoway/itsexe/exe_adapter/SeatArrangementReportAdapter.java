package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.databinding.ItemSeatArrangementReportBinding;

import java.util.List;

public class SeatArrangementReportAdapter extends RecyclerView.Adapter<SeatArrangementReportAdapter.MyViewHolder> {

    private Context context;
    private List<SeatArrangement_Response.Datum> list;
    private int max_column_length;

    public SeatArrangementReportAdapter(Context context, List<SeatArrangement_Response.Datum> list,int max_column_length) {
        this.context = context;
        this.list = list;
        this.max_column_length=max_column_length;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemSeatArrangementReportBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SeatArrangement_Response.Datum data=list.get(position);

        if(data.getCADBlockType()==3){
           // holder.itemView.setBackgroundResource(R.drawable.cancel_btn_bg_exe);
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            holder.itemView.setVisibility(View.INVISIBLE);
        }else{
            holder.itemView.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundResource(R.drawable.bg_fill_rect);
            holder.binding.txtSeat.setText(data.getBADSeatNo());
            holder.binding.txtSeatDetails.setText(data.getJMPassengerName());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemSeatArrangementReportBinding binding;

        public MyViewHolder(ItemSeatArrangementReportBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
