package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.bean.SelectedSeatDetailsBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class BookTicketPassengerListAdapter extends RecyclerView.Adapter<BookTicketPassengerListAdapter.MyViewHolder> {

    private Context context;
    private List<SelectedSeatDetailsBean> setList;
    private OnTextChangeValidator onTextChangeValidator;
    private List<String> genderList = new ArrayList<>();
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetIdProof> idProofList;
    private List<String> idProofSubList;
    private int OS_IsAskIDProof;
    private int waiting_booking_state;
    private boolean textChanged=false;
    private EditText txtSeatNo;

    public BookTicketPassengerListAdapter(Context context, List<SelectedSeatDetailsBean> setList, int OS_IsAskIDProof, List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetIdProof> idProofList, OnTextChangeValidator onTextChangeValidator) {
        this.context = context;
        this.setList = setList;
        this.OS_IsAskIDProof = OS_IsAskIDProof;
        this.idProofList = idProofList;
        idProofSubList = new ArrayList<>();
        if (idProofList != null && idProofList.size() > 0) {
            for (int i = 0; i < idProofList.size(); i++) {
                idProofSubList.add(idProofList.get(i).getPMProofName());
            }
        }
        this.onTextChangeValidator = onTextChangeValidator;
        waiting_booking_state=0;
        genderList.add("M");
        genderList.add("F");
    }

    public interface OnTextChangeValidator {
        void onTextSeatNoChange(List<SelectedSeatDetailsBean> list);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book_seat_selection_list, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final MyViewHolder myViewHolder = holder;

        myViewHolder.myCustomEditTextListener[0].updatePosition(1, position);
        myViewHolder.myCustomEditTextListener[1].updatePosition(2, position);
        myViewHolder.myCustomEditTextListener[2].updatePosition(3, position);
        myViewHolder.myCustomEditTextListener[4].updatePosition(5, position);

        myViewHolder.myFocusChangeListener[0].updatePosition(1, position);

        SelectedSeatDetailsBean data = setList.get(position);

        if (!TextUtils.isEmpty(data.getBADSeatNo())) {
            myViewHolder.et_seat_no.setText(data.getBADSeatNo());
        } else {
            myViewHolder.et_seat_no.setText("");
        }

        this.txtSeatNo=myViewHolder.et_seat_no;

        if (!TextUtils.isEmpty(data.getJMPassengerName())) {
            myViewHolder.et_pax_name.setText(data.getJMPassengerName());
        } else {
            myViewHolder.et_pax_name.setText("");
        }

        if (!TextUtils.isEmpty(data.getJMPhone1())) {
            myViewHolder.et_pax_phone.setText(data.getJMPhone1());
        } else {
            myViewHolder.et_pax_phone.setText("");
        }

        if (!TextUtils.isEmpty(data.getPaxAge())) {
            myViewHolder.et_pax_age.setText(data.getPaxAge());
        } else {
            myViewHolder.et_pax_age.setText("");
        }

     //   holder.bind(position,data, onTextChangeValidator);

        //  holder.bind(data);

        myViewHolder.spn_gender.setAdapter(new GenderArrayAdapter(context, genderList));

        myViewHolder.spn_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                if (setList != null && setList.size() > 0 && setList.size()>position) {
                    setList.get(position).setJPGender(genderList.get(pos));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (!TextUtils.isEmpty(data.getJPGender())) {
            myViewHolder.spn_gender.setSelection(genderList.indexOf(data.getJPGender()));
        } else {
            myViewHolder.spn_gender.setSelection(0);
        }

        if (OS_IsAskIDProof == 1) {
            myViewHolder.ll_id_proof.setVisibility(View.VISIBLE);

            myViewHolder.myCustomEditTextListener[3].updatePosition(4, position);

            if (!TextUtils.isEmpty(data.getIdProofNumber())) {
                myViewHolder.et_id_no.setText(data.getIdProofNumber());
            } else {
                myViewHolder.et_id_no.setText("");
            }

            myViewHolder.spn_id_proof.setAdapter(new IdProofArrayAdapter(context, idProofSubList));

            //myViewHolder.spn_id_proof.setAdapter(new IdProofArrayAdapter(context, idProofList));

            myViewHolder.spn_id_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    setList.get(position).setIdProofId(idProofList.get(pos).getPMID());
                    setList.get(position).setIdProofName(idProofList.get(pos).getPMProofName());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            if (!TextUtils.isEmpty(data.getIdProofName())) {
                myViewHolder.spn_id_proof.setSelection(idProofSubList.indexOf(data.getIdProofName()));
            } else {
                myViewHolder.spn_id_proof.setSelection(0);
            }
        } else {
            myViewHolder.ll_id_proof.setVisibility(View.GONE);
        }

        /*if (!TextUtils.isEmpty(data.getJPGender())) {
            holder.spn_gender.setSelection(genderList.indexOf(data.getJPGender()));
        }

        if (!TextUtils.isEmpty(data.getJMPassengerName())) {
            holder.et_pax_name.setText(data.getJMPassengerName());
        }

        if (!TextUtils.isEmpty(data.getJMPhone1())) {
            holder.et_pax_phone.setText(data.getJMPhone1());
        }*/


    }

    @Override
    public int getItemCount() {
        return setList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private EditText et_seat_no, et_pax_phone, et_pax_name, et_pax_age, et_id_no;
        private Spinner spn_gender, spn_id_proof;
        final MyCustomEditTextListener[] myCustomEditTextListener = new MyCustomEditTextListener[5];
        private LinearLayout ll_id_proof;
        final MyFocusChangeListener [] myFocusChangeListener=new MyFocusChangeListener[1];

        MyViewHolder(View rowView) {
            super(rowView);
            ll_id_proof = rowView.findViewById(R.id.ll_id_proof);
            et_seat_no = rowView.findViewById(R.id.et_seat_no);
            spn_gender = rowView.findViewById(R.id.spn_gender);
            spn_id_proof = rowView.findViewById(R.id.spn_id_proof);
            et_pax_phone = rowView.findViewById(R.id.et_pax_phone);
            et_pax_name = rowView.findViewById(R.id.et_pax_name);
            et_pax_age = rowView.findViewById(R.id.et_pax_age);
            et_id_no = rowView.findViewById(R.id.et_id_no);
            myCustomEditTextListener[0] = new MyCustomEditTextListener();
            myCustomEditTextListener[1] = new MyCustomEditTextListener();
            myCustomEditTextListener[2] = new MyCustomEditTextListener();
            myCustomEditTextListener[3] = new MyCustomEditTextListener();
            myCustomEditTextListener[4] = new MyCustomEditTextListener();

            myFocusChangeListener[0]=new MyFocusChangeListener();

            et_pax_phone.addTextChangedListener(myCustomEditTextListener[0]);
            et_pax_name.addTextChangedListener(myCustomEditTextListener[1]);
            et_pax_age.addTextChangedListener(myCustomEditTextListener[2]);
            et_id_no.addTextChangedListener(myCustomEditTextListener[3]);
            et_seat_no.addTextChangedListener(myCustomEditTextListener[4]);

            et_seat_no.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

            et_seat_no.setOnFocusChangeListener(myFocusChangeListener[0]);

        }



    }

    private class GenderArrayAdapter extends ArrayAdapter<String> {

        private Context context;
        private List<String> genderArray;

        public GenderArrayAdapter(Context context, List<String> genderArray) {
            super(context, R.layout.autocomplete_items, genderArray);
            this.context = context;
            this.genderArray = genderArray;
        }

        @Override
        public int getCount() {
            return genderArray.size();
        }

        @Override
        public String getItem(int position) {
            return genderArray.get(position);
        }

        @Override
        public View getDropDownView(int position, View view,
                                    @NotNull ViewGroup parent) {
            // TODO Auto-generated method stub
            if (view == null) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
            }

            TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

            tv_spinnerItems.setText(genderArray.get(position));
            return view;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.spinner_item, parent, false);


            TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


            tv_spinnerItems.setText(genderArray.get(position));

            return rowView;
        }

    }

    public static class IdProofArrayAdapter extends ArrayAdapter<String> {

        private Context context;
        private List<String> idProofArray;

        public IdProofArrayAdapter(Context context, List<String> idProofArray) {
            super(context, R.layout.autocomplete_items, idProofArray);
            this.context = context;
            this.idProofArray = idProofArray;
        }

        @Override
        public int getCount() {
            return idProofArray.size();
        }

        @Override
        public String getItem(int position) {
            return idProofArray.get(position);
        }

        @Override
        public View getDropDownView(int position, View view,
                                    @NotNull ViewGroup parent) {
            // TODO Auto-generated method stub
            if (view == null) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
            }

            TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

            tv_spinnerItems.setText(idProofArray.get(position));
            return view;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.spinner_item, parent, false);


            TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


            tv_spinnerItems.setText(idProofArray.get(position));

            return rowView;
        }

    }

    /*public static class IdProofArrayAdapter extends ArrayAdapter<ITSGetIdProof_Model> {

        private Context context;
        private List<ITSGetIdProof_Model> idProofArray;

        public IdProofArrayAdapter(Context context, List<ITSGetIdProof_Model> idProofArray) {
            super(context, R.layout.autocomplete_items, idProofArray);
            this.context = context;
            this.idProofArray = idProofArray;
        }

        @Override
        public int getCount() {
            return idProofArray.size();
        }



        @Override
        public View getDropDownView(int position, View view,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            if (view == null) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
            }

            TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

            tv_spinnerItems.setText(idProofArray.get(position).getPM_ProofName());
            return view;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.spinner_item, parent, false);


            TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


            tv_spinnerItems.setText(idProofArray.get(position).getPM_ProofName());

            return rowView;
        }

    }*/

    private class MyFocusChangeListener implements View.OnFocusChangeListener{

        private int position;
        int view_type = 0;

        void updatePosition(int view_type, int position) {
            this.position = position;
            this.view_type = view_type;
        }

        @Override
        public void onFocusChange(View view, boolean b) {
            if(!b){
                //if(waiting_booking_state==1){
                    onTextChangeValidator.onTextSeatNoChange(setList);
               // }
            }
        }
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;
        String str = "";
        int view_type = 0;


        void updatePosition(int view_type, int position) {
            this.position = position;
            this.view_type = view_type;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // contactList.get(position).setGrade(charSequence.toString());
            try {
                str = charSequence.toString();
            } catch (Exception ignored) {
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op

            switch (view_type) {
                case 1:
                    if (TextUtils.isEmpty(editable.toString())) {
                        setList.get(position).setJMPhone1("");
                    } else {
                        setList.get(position).setJMPhone1(editable.toString());
                    }
                    break;
                case 2:
                    if (TextUtils.isEmpty(editable.toString())) {
                        setList.get(position).setJMPassengerName("");
                    } else {
                        setList.get(position).setJMPassengerName(editable.toString());
                    }
                    break;
                case 3:
                    if (TextUtils.isEmpty(editable.toString())) {
                        setList.get(position).setPaxAge("");
                    } else {
                        setList.get(position).setPaxAge(editable.toString());
                    }
                    break;
                case 4:
                    if (TextUtils.isEmpty(editable.toString())) {
                        setList.get(position).setIdProofNumber("");
                    } else {
                        setList.get(position).setIdProofNumber(editable.toString());
                    }
                    break;
                case 5:
                    if (TextUtils.isEmpty(editable.toString())) {
                        setList.get(position).setBADSeatNo("");
                    } else {
                        setList.get(position).setBADSeatNo(editable.toString());
                    }

                    /*if(waiting_booking_state==1){
                        onTextChangeValidator.onTextSeatNoChange(setList.get(position), position);
                        if(txtSeatNo!=null){
                            if(!TextUtils.isEmpty(editable.toString())){
                                txtSeatNo.setSelection(txtSeatNo.getText().toString().length());
                            }
                            txtSeatNo.requestFocus();
                        }
                    }*/
                    break;
            }

        }
    }

    public void setWaitingBookingState(int state) {
        waiting_booking_state = state;
    }

    public int getWaitingBookingState() {
        return waiting_booking_state;
    }

}
