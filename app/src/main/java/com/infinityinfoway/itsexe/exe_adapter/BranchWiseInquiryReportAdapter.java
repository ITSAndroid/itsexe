package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.BranchWiseInquiryReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemBranchwiseInquiryReportChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemBranchwiseInquiryReportParentBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BranchWiseInquiryReportAdapter extends RecyclerView.Adapter<BranchWiseInquiryReportAdapter.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<BranchWiseInquiryReport_Response.Datum>> linkedHashMap;
    private List<String> key_list;

    public BranchWiseInquiryReportAdapter(Context context, LinkedHashMap<String, List<BranchWiseInquiryReport_Response.Datum>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        this.key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemBranchwiseInquiryReportParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.parentBinding.txtBranchName.setText(key_list.get(position));
        holder.parentBinding.rvBranchData.setAdapter(new ChildLayoutAdapter(context, linkedHashMap.get(key_list.get(position))));
        List<String> arrayList=calSummary(linkedHashMap.get(key_list.get(position)));
        holder.parentBinding.txtSeat.setText(arrayList.get(0));
        holder.parentBinding.txtFare.setText(arrayList.get(1));
        holder.parentBinding.txtStax.setText(arrayList.get(2));
        holder.parentBinding.txtBkgFare.setText(arrayList.get(3));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemBranchwiseInquiryReportParentBinding parentBinding;

        public MyViewHolder(ItemBranchwiseInquiryReportParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {
        private Context context;
        private List<BranchWiseInquiryReport_Response.Datum> list;

        public ChildLayoutAdapter(Context context, List<BranchWiseInquiryReport_Response.Datum> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemBranchwiseInquiryReportChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            BranchWiseInquiryReport_Response.Datum data=list.get(position);

            holder.childBinding.txtPnr.setText(data.getJMPNRNO()+"-"+data.getJMTicketNo());
            holder.childBinding.txtUser.setText(data.getBUMUserName());
            holder.childBinding.txtSeat.setText(data.getJMTotalPassengers());
            holder.childBinding.txtSeats.setText(data.getSeatNo());
            holder.childBinding.txtCustomer.setText(data.getJMPassengerName());
            holder.childBinding.txtCustPhone.setText(data.getJMPhone1());
            holder.childBinding.txtCity.setText(data.getFromCity()+"-"+data.getToCity());
            holder.childBinding.txtPickup.setText(data.getPickUpName());
            holder.childBinding.txtDropOff.setText(data.getDropName());
            holder.childBinding.txtFare.setText(String.format("%.2f",data.getFare()));
            holder.childBinding.txtStax.setText(String.format("%.2f",data.getSTAX()));
            holder.childBinding.txtBkgFare.setText(String.format("%.2f",data.getBKGFARE()));
            holder.childBinding.txtType.setText(data.getBookingType());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private class MyViewHolder extends RecyclerView.ViewHolder {
            private ItemBranchwiseInquiryReportChildBinding childBinding;

            public MyViewHolder(ItemBranchwiseInquiryReportChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }
    }

    private List<String> calSummary(List<BranchWiseInquiryReport_Response.Datum> list){
        int totalSeat=0;
        double totalFare=0,totalStax=0,totalBKGFare=0;
        for(BranchWiseInquiryReport_Response.Datum data:list){
            if(!TextUtils.isEmpty(data.getJMTotalPassengers())){
                totalSeat+=Integer.parseInt(data.getJMTotalPassengers());
            }
            totalFare+=data.getFare();
            totalStax+=data.getSTAX();
            totalBKGFare+=data.getBKGFARE();
        }

        List<String> arrayList=new ArrayList<>();
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.format("%.2f",totalFare));
        arrayList.add(String.format("%.2f",totalStax));
        arrayList.add(String.format("%.2f",totalBKGFare));
        return arrayList;
    }

}
