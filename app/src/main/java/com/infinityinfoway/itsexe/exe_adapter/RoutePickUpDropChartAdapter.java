package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.RoutePickupDropChart_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RoutePickUpDropChartAdapter extends RecyclerView.Adapter<RoutePickUpDropChartAdapter.MyViewHolder> {

    private Context context;
    private List<RoutePickupDropChart_Response.Datum> listBusAvailableRoute;


    public RoutePickUpDropChartAdapter(Context mContext, List<RoutePickupDropChart_Response.Datum> listContactUs) {
        this.context = mContext;
        this.listBusAvailableRoute = listContactUs;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_sr_no, txt_seat_no, txt_Name, txt_mobile, txt_from_city, txt_to_city, txt_pickup_name, txt_drop_name, txt_booked_by;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_sr_no = itemView.findViewById(R.id.txt_sr_no);
            txt_seat_no = itemView.findViewById(R.id.txt_seat_no);
            txt_Name = itemView.findViewById(R.id.txt_Name);
            txt_mobile = itemView.findViewById(R.id.txt_mobile);
            txt_from_city = itemView.findViewById(R.id.txt_from_city);
            txt_to_city = itemView.findViewById(R.id.txt_to_city);
            txt_pickup_name = itemView.findViewById(R.id.txt_pickup_name);
            txt_drop_name = itemView.findViewById(R.id.txt_drop_name);
            txt_booked_by = itemView.findViewById(R.id.txt_booked_by);
        }
    }


    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView;
        iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_routepickup_drop_chart, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        RoutePickupDropChart_Response.Datum contact_data = listBusAvailableRoute.get(listPosition);

        holder.txt_sr_no.setText((listPosition + 1) + "");
        holder.txt_seat_no.setText(contact_data.getSeatNo());
        holder.txt_Name.setText(contact_data.getPassengerName());
        holder.txt_mobile.setText(contact_data.getPhoneNo());
        holder.txt_from_city.setText(contact_data.getFromCity());
        holder.txt_to_city.setText(contact_data.getToCity());
        holder.txt_pickup_name.setText(contact_data.getPickUpName());
        holder.txt_drop_name.setText(contact_data.getDropName());
        holder.txt_booked_by.setText(contact_data.getBookedBy());
    }

    @Override
    public int getItemCount() {
        return listBusAvailableRoute.size();
    }

}





