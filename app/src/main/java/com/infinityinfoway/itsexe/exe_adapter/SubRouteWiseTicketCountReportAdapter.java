package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.SubRouteWiseTicketCount_Response;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class SubRouteWiseTicketCountReportAdapter extends RecyclerView.Adapter<SubRouteWiseTicketCountReportAdapter.MyViewHolder> {

    private LinkedHashMap<String, LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>>> hashMap_data;
    private List<String> key_list;
    private Context context;
    private int positionCounter = 0;

    public SubRouteWiseTicketCountReportAdapter(Context mContext, LinkedHashMap<String, LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_time, txt_time_wise_total_amt, txt_time_wise_total_seat;
        RecyclerView rv_parent;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_time = rowView.findViewById(R.id.txt_time);
            rv_parent = rowView.findViewById(R.id.rv_parent);
            txt_time_wise_total_amt = rowView.findViewById(R.id.txt_time_wise_total_amt);
            txt_time_wise_total_seat = rowView.findViewById(R.id.txt_time_wise_total_seat);
        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subroute_wiseticket_count_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.txt_time.setText(key_list.get(listPosition));

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition)));
        holder.rv_parent.setAdapter(new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition))));

        // 1 find time wise total
        List<String> list = getRouteTotalSeatAmt(1, hashMap_data.get(key_list.get(listPosition)), listPosition);

        holder.txt_time_wise_total_seat.setText(list.get(0));
        holder.txt_time_wise_total_amt.setText(list.get(1));

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        private Context context;
        private LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>> linkedHashMap;
        private List<String> key_listV2;


        public ChildLayoutAdapter(Context mContext, LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>> linkedHashMap) {
            this.context = mContext;
            this.linkedHashMap = linkedHashMap;
            key_listV2 = new ArrayList<>(linkedHashMap.keySet());
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_route, txt_route_wise_total_amt, txt_route_wise_total_seat;
            private RecyclerView rv_child;

            MyViewHolder(View rowView) {
                super(rowView);

                txt_route = rowView.findViewById(R.id.txt_route);
                rv_child = rowView.findViewById(R.id.rv_child);
                txt_route_wise_total_amt = rowView.findViewById(R.id.txt_route_wise_total_amt);
                txt_route_wise_total_seat = rowView.findViewById(R.id.txt_route_wise_total_seat);
            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subroute_wiseticket_count_child_layout, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

            holder.txt_route.setText(key_listV2.get(listPosition));
            holder.rv_child.setAdapter(new NestedChildLayoutAdapter(context, linkedHashMap.get(key_listV2.get(listPosition))));

            // 2 find route wise total
            List<String> list = getRouteTotalSeatAmt(2, linkedHashMap, listPosition);

            holder.txt_route_wise_total_seat.setText(list.get(0));
            holder.txt_route_wise_total_amt.setText(list.get(1));

        }

        @Override
        public int getItemCount() {
            return key_listV2.size();
        }

    }

    public class NestedChildLayoutAdapter extends RecyclerView.Adapter<NestedChildLayoutAdapter.MyViewHolder> {
        List<SubRouteWiseTicketCount_Response.Datum> listSubRoute;
        private Context context;

        public NestedChildLayoutAdapter(Context mContext, List<SubRouteWiseTicketCount_Response.Datum> listSubRoute) {
            this.context = mContext;
            this.listSubRoute = listSubRoute;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subroute_wiseticket_count_nestedchild, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
            positionCounter = positionCounter + 1;
            holder.txt_sr_no.setText(positionCounter + "");
            holder.txt_subroute_name.setText(listSubRoute.get(listPosition).getFromCityToCity());
            holder.txt_amount.setText(String.valueOf(listSubRoute.get(listPosition).getJMPayableAmount()));
            holder.txt_total_Seat.setText(String.valueOf(listSubRoute.get(listPosition).getTotalSeat()));
        }

        @Override
        public int getItemCount() {
            return listSubRoute.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_subroute_name, txt_amount, txt_total_Seat;


            MyViewHolder(View rowView) {
                super(rowView);
                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_subroute_name = rowView.findViewById(R.id.txt_subroute_name);
                txt_amount = rowView.findViewById(R.id.txt_amount);
                txt_total_Seat = rowView.findViewById(R.id.txt_total_Seat);
            }
        }

    }

    private List<String> getRouteTotalSeatAmt(int isTimeWiseTotal, LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>> linkedHashMap, int position) {
        int totalRouteSeat = 0;
        double totalRouteAmt = 0.0;
        List<String> key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        if (isTimeWiseTotal == 1) {
            for (int i = 0; i < linkedHashMap.size(); i++) {
                for (SubRouteWiseTicketCount_Response.Datum data : linkedHashMap.get(key_list_V2.get(i))) {
                    totalRouteSeat += data.getTotalSeat();
                    totalRouteAmt += data.getJMPayableAmount();
                }
            }
        } else {
            for (SubRouteWiseTicketCount_Response.Datum data : linkedHashMap.get(key_list_V2.get(position))) {
                totalRouteSeat += data.getTotalSeat();
                totalRouteAmt += data.getJMPayableAmount();
            }
        }


        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.valueOf(totalRouteAmt));
        return arrayList;

    }

}


