package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.ReservationDetailReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemReservationDetailReportChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemReservationDetailReportParentBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ReservationDetailReportAdapter extends RecyclerView.Adapter<ReservationDetailReportAdapter.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<ReservationDetailReport_Response.ReservationDetail>> linkedHashMap;
    private List<String> key_list;

    public ReservationDetailReportAdapter(Context context, LinkedHashMap<String, List<ReservationDetailReport_Response.ReservationDetail>> linkedHashMap) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        key_list=new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemReservationDetailReportParentBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
      holder.parentBinding.txtPickup.setText(key_list.get(position));
      holder.parentBinding.rvData.setAdapter(new ChildLayoutAdapter(context,linkedHashMap.get(key_list.get(position))));
        holder.parentBinding.txtSeats.setText("Seats : " + calTotalSeats(linkedHashMap.get(key_list.get(position))));
    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemReservationDetailReportParentBinding parentBinding;

        public MyViewHolder(ItemReservationDetailReportParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    private class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder>{

        private Context context;
        private List<ReservationDetailReport_Response.ReservationDetail> list;

        public ChildLayoutAdapter(Context context, List<ReservationDetailReport_Response.ReservationDetail> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemReservationDetailReportChildBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            ReservationDetailReport_Response.ReservationDetail data=list.get(position);
            holder.childBinding.txtPnr.setText(data.getJMPNRNO());
            holder.childBinding.txtSeats.setText(data.getJMSeatList());
            holder.childBinding.txtName.setText(data.getJMPassengerName());
            holder.childBinding.txtPhone.setText(data.getJMPhone1());
            holder.childBinding.txtDest.setText(data.getRouteDestination());
            holder.childBinding.txtDropOff.setText(data.getPMDropName());
            holder.childBinding.txtIssueBy.setText(data.getBookedBy());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private class MyViewHolder extends RecyclerView.ViewHolder{
            public ItemReservationDetailReportChildBinding childBinding;
            public MyViewHolder(ItemReservationDetailReportChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding=childBinding;
            }
        }
    }

    private String calTotalSeats(List<ReservationDetailReport_Response.ReservationDetail> list) {
        int totalSeat = 0;
        for (ReservationDetailReport_Response.ReservationDetail data : list) {
            if (!TextUtils.isEmpty(data.getJMTotalPassengers())) {
                totalSeat += Integer.parseInt(data.getJMTotalPassengers());
            }
        }
        return String.valueOf(totalSeat);
    }

}
