package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.MainRouteWisePassengerDetails_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MainRouteWisePassengerDetailAdapter extends RecyclerView.Adapter<MainRouteWisePassengerDetailAdapter.MyViewHolder> {

    private Context context;
    private List<MainRouteWisePassengerDetails_Response.Datum> list_passenger_details;


    public MainRouteWisePassengerDetailAdapter(Context mContext, List<MainRouteWisePassengerDetails_Response.Datum> list_passenger_details) {
        this.context = mContext;
        this.list_passenger_details = list_passenger_details;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_passenger_name, txt_sr_no, txt_phone_no, txt_email, txt_seat_no;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_passenger_name = itemView.findViewById(R.id.txt_passenger_name);
            txt_sr_no = itemView.findViewById(R.id.txt_sr_no);
            txt_phone_no = itemView.findViewById(R.id.txt_phone_no);
            txt_email = itemView.findViewById(R.id.txt_email);
            txt_seat_no = itemView.findViewById(R.id.txt_seat_no);
        }
    }


    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mainroutewise_passengerdetail, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        MainRouteWisePassengerDetails_Response.Datum contact_data = list_passenger_details.get(listPosition);
        holder.txt_sr_no.setText((listPosition + 1) + "");
        holder.txt_passenger_name.setText(contact_data.getJMPassengerName());
        holder.txt_phone_no.setText(contact_data.getJMPhone1());
        holder.txt_email.setText(contact_data.getJMEmailID());
        holder.txt_seat_no.setText(contact_data.getJMSeatList());
    }

    @Override
    public int getItemCount() {
        return list_passenger_details.size();
    }

}




