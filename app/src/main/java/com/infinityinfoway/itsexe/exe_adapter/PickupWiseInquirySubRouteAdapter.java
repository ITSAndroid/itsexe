package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.PickupWiseInquiry_Response;

import java.util.List;

public class PickupWiseInquirySubRouteAdapter extends RecyclerView.Adapter<PickupWiseInquirySubRouteAdapter.MyViewHolder> {
    List<PickupWiseInquiry_Response.SubRouteWiseDatum> table1List;
    private Context context;

    public PickupWiseInquirySubRouteAdapter(List<PickupWiseInquiry_Response.SubRouteWiseDatum> table1List, Context context) {
        this.table1List = table1List;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pickup_wise_inquiry_subroute, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.txt_subroute.setText(table1List.get(listPosition).getSubRouteName());
        holder.txt_subroute_total.setText(table1List.get(listPosition).getTotalSeat());
        holder.txt_subroute_seats.setText(table1List.get(listPosition).getSeatNo());
    }

    @Override
    public int getItemCount() {
        return table1List.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_subroute, txt_subroute_total, txt_subroute_seats;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_subroute = rowView.findViewById(R.id.txt_subroute);
            txt_subroute_total = rowView.findViewById(R.id.txt_subroute_total);
            txt_subroute_seats = rowView.findViewById(R.id.txt_subroute_seats);

        }
    }
}
