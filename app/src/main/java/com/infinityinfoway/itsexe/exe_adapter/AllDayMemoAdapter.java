package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_Response;
import com.infinityinfoway.itsexe.bean.CargoReportBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class AllDayMemoAdapter extends RecyclerView.Adapter<AllDayMemoAdapter.MyViewHolder> {

    private LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>>> Hashmap_allDayMemo;
    List<String> key_list;
    private Context context;
    private int positionCounter = 0;
    LinearLayoutManager linearLayoutManager;

    public interface OnItemClickListener {
        void onItemClick(CargoReportBean item);
    }

    public AllDayMemoAdapter(Context mContext, LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>>> Hashmap_allDayMemo) {
        context = mContext;
        this.Hashmap_allDayMemo = Hashmap_allDayMemo;
        key_list = new ArrayList<>(Hashmap_allDayMemo.keySet());


    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_time, time_wise_total_seat, time_wise_total_amt;
        RecyclerView recycler_seat_data;
        private LinearLayout ll_time_wise;

        MyViewHolder(View rowView) {
            super(rowView);

            time_wise_total_seat = rowView.findViewById(R.id.time_wise_total_seat);
            time_wise_total_amt = rowView.findViewById(R.id.time_wise_total_amt);
            recycler_seat_data = rowView.findViewById(R.id.recycler_seat_data);
            txt_time = rowView.findViewById(R.id.txt_time);
            ll_time_wise = rowView.findViewById(R.id.ll_time_wise);
        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alldaymemo_report_parent, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.ll_time_wise.setVisibility(View.VISIBLE);

        holder.txt_time.setText(key_list.get(listPosition));

        linearLayoutManager = new LinearLayoutManager(context);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, Hashmap_allDayMemo.get(key_list.get(listPosition)));
        holder.recycler_seat_data.setLayoutManager(linearLayoutManager);
        holder.recycler_seat_data.setAdapter(childLayoutAdapter);

        // 1 find time wise total
        List<String> list = getRouteTotalSeatAmt(1, Hashmap_allDayMemo.get(key_list.get(listPosition)), listPosition);

        holder.time_wise_total_seat.setText(list.get(0));
        holder.time_wise_total_amt.setText(list.get(1));

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {
        LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>> linkedHashMap;
        Context context;
        private List<String> key_list_V2;

        public ChildLayoutAdapter(Context context, LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>> linkedHashMap) {
            this.context = context;
            this.linkedHashMap = linkedHashMap;
            key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alldaymemo_seat_detail_childlayout, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
            holder.txt_route_name.setText(key_list_V2.get(listPosition));
            holder.rv_route_details.setAdapter(new NestedChildLayoutAdapter(context, linkedHashMap.get(key_list_V2.get(listPosition))));
            // 2 find route wise total
            List<String> list = getRouteTotalSeatAmt(2, linkedHashMap, listPosition);

            holder.txt_route_total_seat.setText(list.get(0));
            holder.txt_route_total_amt.setText(list.get(1));
        }

        @Override
        public int getItemCount() {
            return key_list_V2.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_route_name, txt_route_total_seat, txt_route_total_amt;
            RecyclerView rv_route_details;
            private LinearLayout ll_route_wise;

            MyViewHolder(View rowView) {
                super(rowView);
                txt_route_name = rowView.findViewById(R.id.txt_route_name);
                txt_route_total_seat = rowView.findViewById(R.id.txt_route_total_seat);
                txt_route_total_amt = rowView.findViewById(R.id.txt_route_total_amt);
                ll_route_wise = rowView.findViewById(R.id.ll_route_wise);
                rv_route_details = rowView.findViewById(R.id.rv_route_details);
            }
        }
    }


    public class NestedChildLayoutAdapter extends RecyclerView.Adapter<NestedChildLayoutAdapter.MyViewHolder> {


        List<Get_Report_AllDayMemo_Response.AllDayMemo> SeatDataList;
        private Context context;


        public NestedChildLayoutAdapter(Context mContext, List<Get_Report_AllDayMemo_Response.AllDayMemo> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_From_City, txt_to_city, txt_seat_names, txt_amt_per_Seat, txt_total_seat, txt_total_amt;


            MyViewHolder(View rowView) {
                super(rowView);


                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_From_City = rowView.findViewById(R.id.txt_From_City);
                txt_to_city = rowView.findViewById(R.id.txt_to_city);
                txt_seat_names = rowView.findViewById(R.id.txt_seat_names);
                txt_amt_per_Seat = rowView.findViewById(R.id.txt_amt_per_Seat);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_total_amt = rowView.findViewById(R.id.txt_total_amt);

            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView;
            iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alldaymemo_seat_detail_nested_child, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            positionCounter = positionCounter + 1;
            holder.txt_sr_no.setText(positionCounter + "");
            holder.txt_From_City.setText(SeatDataList.get(listPosition).getFromCityName());
            holder.txt_to_city.setText(SeatDataList.get(listPosition).getToCityName());
            holder.txt_seat_names.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.txt_amt_per_Seat.setText(SeatDataList.get(listPosition).getAmount());
            holder.txt_total_seat.setText(String.valueOf(SeatDataList.get(listPosition).getTotalSeat()));
            holder.txt_total_amt.setText(String.valueOf(SeatDataList.get(listPosition).getTotalAmount()));
        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

    }

    private List<String> getRouteTotalSeatAmt(int isTimeWiseTotal, LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>> linkedHashMap, int position) {
        int totalRouteSeat = 0;
        double totalRouteAmt = 0.0;
        List<String> key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        if (isTimeWiseTotal == 1) {
            for (int i = 0; i < linkedHashMap.size(); i++) {
                for (Get_Report_AllDayMemo_Response.AllDayMemo data : linkedHashMap.get(key_list_V2.get(i))) {
                    totalRouteSeat += data.getTotalSeat();
                    totalRouteAmt += data.getTotalAmount();
                }
            }
        } else {
            for (Get_Report_AllDayMemo_Response.AllDayMemo data : linkedHashMap.get(key_list_V2.get(position))) {
                totalRouteSeat += data.getTotalSeat();
                totalRouteAmt += data.getTotalAmount();
            }
        }


        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.valueOf(totalRouteAmt));
        return arrayList;

    }

}

