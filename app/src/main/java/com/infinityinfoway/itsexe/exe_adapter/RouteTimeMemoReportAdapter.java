package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.RouteTimeMemoReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemRouteTimeMemoReportChildBinding;
import com.infinityinfoway.itsexe.databinding.ItemRouteTimeMemoReportParentBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RouteTimeMemoReportAdapter extends RecyclerView.Adapter<RouteTimeMemoReportAdapter.MyViewHolder> {

    private Context context;
    private LinkedHashMap<String, List<RouteTimeMemoReport_Response.Datum>> linkedHashMap;
    private List<String> key_list;
    private int positionCounter = 0;

    public RouteTimeMemoReportAdapter(Context mContext, LinkedHashMap<String, List<RouteTimeMemoReport_Response.Datum>> linkedHashMap) {
        this.context = mContext;
        this.linkedHashMap = linkedHashMap;
        this.key_list = new ArrayList<>(linkedHashMap.keySet());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemRouteTimeMemoReportParentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        holder.parentBinding.txtGropHeader.setText(key_list.get(listPosition));

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, linkedHashMap.get(key_list.get(listPosition)));
        holder.parentBinding.rvData.setAdapter(childLayoutAdapter);

        List<String> list = groupSummary(linkedHashMap.get(key_list.get(listPosition)));

        holder.parentBinding.txtSeat.setText(list.get(0));
        holder.parentBinding.txtAmount.setText(list.get(1));
        holder.parentBinding.txtNetCollection.setText(list.get(1));
        holder.parentBinding.txtBranchCharge.setText(list.get(2));
        holder.parentBinding.txtNetAmt.setText(list.get(3));


    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemRouteTimeMemoReportParentBinding parentBinding;

        public MyViewHolder(ItemRouteTimeMemoReportParentBinding parentBinding) {
            super(parentBinding.getRoot());
            this.parentBinding = parentBinding;
        }
    }

    public class ChildLayoutAdapter extends RecyclerView.Adapter<RouteTimeMemoReportAdapter.ChildLayoutAdapter.MyViewHolder> {

        private Context context;
        private List<RouteTimeMemoReport_Response.Datum> list;

        public ChildLayoutAdapter(Context context, List<RouteTimeMemoReport_Response.Datum> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(ItemRouteTimeMemoReportChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            RouteTimeMemoReport_Response.Datum contact_data = list.get(position);

            positionCounter++;

            holder.childBinding.txtSrNo.setText(String.valueOf(positionCounter));
            holder.childBinding.txtFromCity.setText(contact_data.getFromCity());
            holder.childBinding.txtToCity.setText(contact_data.getToCity());
            holder.childBinding.txtRate.setText(String.format("%.2f", contact_data.getJPAmount()));
            holder.childBinding.txtSeatNames.setText(contact_data.getJMSeatList());
            holder.childBinding.txtSeat.setText(String.valueOf(contact_data.getJMTotalPassengers()));
            holder.childBinding.txtTotalAmt.setText(String.format("%.2f", contact_data.getJMPayableAmount()));

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private ItemRouteTimeMemoReportChildBinding childBinding;

            public MyViewHolder(ItemRouteTimeMemoReportChildBinding childBinding) {
                super(childBinding.getRoot());
                this.childBinding = childBinding;
            }
        }

    }

    private List<String> groupSummary(List<RouteTimeMemoReport_Response.Datum> list) {
        List<String> arrayList = new ArrayList<>();

        Double totalBranchCharge=0.0;
        String totalSeat="0",totalAmount="0",totalNetCollection="0";

        for(RouteTimeMemoReport_Response.Datum data: list){
            if(data.getGroupByID()==1){
                totalSeat=data.getTotalSeatSELF();
                totalAmount=data.getTotalAmountSELF();
                if(!TextUtils.isEmpty(data.getBranchChargesSELF())){
                    totalBranchCharge+=Double.parseDouble(data.getBranchChargesSELF());
                }
                totalNetCollection=data.getNetCollectionSELF();
            }else{
                totalSeat=data.getTotalSeatAgent();
                totalAmount=data.getTotalAmountAgent();
                if(!TextUtils.isEmpty(data.getBranchChargesAgent())){
                    totalBranchCharge+=Double.parseDouble(data.getBranchChargesAgent());
                }
                totalNetCollection=data.getNetCollectionAgent();
            }
        }

        arrayList.add(totalSeat);
        arrayList.add(totalAmount);
        arrayList.add(String.format("%.2f",totalBranchCharge));
        arrayList.add(totalNetCollection);

        return arrayList;
    }


}
