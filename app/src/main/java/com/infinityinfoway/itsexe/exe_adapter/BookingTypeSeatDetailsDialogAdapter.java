package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.bean.SelectedSeatDetailsBean;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BookingTypeSeatDetailsDialogAdapter extends RecyclerView.Adapter<BookingTypeSeatDetailsDialogAdapter.MyViewHolder> {

    private Context context;
    private OnItemClickListener onItemClickListener;
    private LinkedHashMap<String, SelectedSeatDetailsBean> linkedHashMap;
    private List<String> KeyList;

    public interface OnItemClickListener {
        void onItemClick(SelectedSeatDetailsBean item, int position, int flag);
    }

    public BookingTypeSeatDetailsDialogAdapter(Context context, LinkedHashMap<String, SelectedSeatDetailsBean> linkedHashMap, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.linkedHashMap = linkedHashMap;
        this.KeyList = new ArrayList<>(linkedHashMap.keySet());
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_type_wise_seat_details, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        SelectedSeatDetailsBean data = linkedHashMap.get(KeyList.get(position));

        holder.bind(data, onItemClickListener, position);

        holder.txt_sr_no.setText(String.valueOf(position + 1));
        holder.txt_pnr.setText(String.valueOf(data.getJMPNRNO()));
        holder.txt_sub_route.setText(data.getFromCity() + " - " + data.getToCity());
        holder.txt_name.setText(data.getJMPassengerName());
        holder.txt_mobile.setText(data.getJMPhone1());
        holder.txt_seats.setText(data.getBADSeatNo());
        holder.txt_remarks.setText(data.getRemarks());


        switch (data.getBookingTypeID()) {
            case APP_CONSTANTS.CONFIRM_BOOKING:
                holder.txt_cancel.setVisibility(View.VISIBLE);
                holder.txt_reprint.setVisibility(View.VISIBLE);
                holder.txt_modify.setVisibility(View.VISIBLE);
                break;

            case APP_CONSTANTS.PHONE_BOOKING:
                holder.txt_reprint.setVisibility(View.GONE);

                holder.txt_hold.setVisibility(View.VISIBLE);
                holder.txt_confirm.setVisibility(View.VISIBLE);
                holder.txt_cancel.setVisibility(View.VISIBLE);
                holder.txt_modify.setVisibility(View.VISIBLE);
                holder.txt_phone_time.setVisibility(View.VISIBLE);
                holder.txt_phone_edit.setVisibility(View.VISIBLE);
                holder.txt_phone_time.setText(data.getPhoneBookingTime());
                holder.txt_phone_edit.setText("Edit");
                break;

            case APP_CONSTANTS.AGENT_BOOKING:

            case APP_CONSTANTS.AGENT_QUOTA_BOOKING:

            case APP_CONSTANTS.BRANCH_BOOKING:

            case APP_CONSTANTS.WAITING_BOOKING:
                holder.txt_cancel.setVisibility(View.VISIBLE);
                holder.txt_modify.setVisibility(View.VISIBLE);
                break;

            case APP_CONSTANTS.GUEST_BOOKING:
                holder.txt_cancel.setVisibility(View.VISIBLE);
                holder.txt_reprint.setVisibility(View.VISIBLE);
                holder.txt_modify.setVisibility(View.VISIBLE);
                break;

            case APP_CONSTANTS.BANK_CARD_BOOKING:
                break;

            case APP_CONSTANTS.CMP_CARD_BOOKING:
                break;
        }

        holder.txt_cancel.setPaintFlags(holder.txt_cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.txt_reprint.setPaintFlags(holder.txt_reprint.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.txt_modify.setPaintFlags(holder.txt_modify.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    @Override
    public int getItemCount() {
        return KeyList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_hold, txt_confirm, txt_sr_no, txt_reprint, txt_cancel, txt_modify, txt_pnr, txt_sub_route, txt_name,
                txt_mobile, txt_seats, txt_remarks, txt_phone_time, txt_phone_edit;

        MyViewHolder(View rowView) {
            super(rowView);
            txt_hold = rowView.findViewById(R.id.txt_hold);
            txt_confirm = rowView.findViewById(R.id.txt_confirm);
            txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
            txt_reprint = rowView.findViewById(R.id.txt_reprint);
            txt_cancel = rowView.findViewById(R.id.txt_cancel);
            txt_modify = rowView.findViewById(R.id.txt_modify);
            txt_phone_time = rowView.findViewById(R.id.txt_phone_time);
            txt_phone_edit = rowView.findViewById(R.id.txt_phone_edit);
            txt_pnr = rowView.findViewById(R.id.txt_pnr);
            txt_sub_route = rowView.findViewById(R.id.txt_sub_route);
            txt_name = rowView.findViewById(R.id.txt_name);
            txt_mobile = rowView.findViewById(R.id.txt_mobile);
            txt_seats = rowView.findViewById(R.id.txt_seats);
            txt_remarks = rowView.findViewById(R.id.txt_remarks);
        }

        //Flag===> 0-> Reprint, 1-> Cancel , 2-> Modify , 3 -> Hold , 4-> Confirm , 5 -> PhoneTime

        void bind(final SelectedSeatDetailsBean item, final OnItemClickListener listener, final int position) {

            txt_reprint.setOnClickListener(v -> listener.onItemClick(item, position, 0));
            txt_cancel.setOnClickListener(v -> listener.onItemClick(item, position, 1));
            txt_modify.setOnClickListener(v -> listener.onItemClick(item, position, 2));
            txt_hold.setOnClickListener(v -> listener.onItemClick(item, position, 3));
            txt_confirm.setOnClickListener(v -> listener.onItemClick(item, position, 4));
            txt_phone_edit.setOnClickListener(v -> listener.onItemClick(item, position, 5));

        }
    }

    public void modifyAdapter(LinkedHashMap<String, SelectedSeatDetailsBean> linkedHashMapData) {
        linkedHashMap.clear();
        KeyList.clear();
        this.linkedHashMap.putAll(linkedHashMapData);
        KeyList = new ArrayList<>(linkedHashMap.keySet());
        notifyDataSetChanged();


    }

}
