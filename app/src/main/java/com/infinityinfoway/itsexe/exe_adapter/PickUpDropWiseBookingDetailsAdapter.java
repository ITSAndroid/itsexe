package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.PickupDropWiseBookingDetails_Response;
import com.infinityinfoway.itsexe.bean.CargoReportBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PickUpDropWiseBookingDetailsAdapter extends RecyclerView.Adapter<PickUpDropWiseBookingDetailsAdapter.MyViewHolder> {

    LinkedHashMap<String, List<PickupDropWiseBookingDetails_Response.Datum>> hashMap_data;
    List<String> key_list;
    private Context context;
    LinearLayoutManager linearLayoutManager;

    public interface OnItemClickListener {
        void onItemClick(CargoReportBean item);
    }

    public PickUpDropWiseBookingDetailsAdapter(Context mContext, LinkedHashMap<String, List<PickupDropWiseBookingDetails_Response.Datum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());


    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView pickup_wise_total_seat, txt_pickup_name;
        RecyclerView recycler_pickup_drop_pass_details;

        MyViewHolder(View rowView) {
            super(rowView);

            recycler_pickup_drop_pass_details = rowView.findViewById(R.id.recycler_pickup_drop_pass_details);
            pickup_wise_total_seat = rowView.findViewById(R.id.pickup_wise_total_seat);
            txt_pickup_name = rowView.findViewById(R.id.txt_pickup_name);
        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pickup_drop_wise_booking_details, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.txt_pickup_name.setText(key_list.get(listPosition));
        holder.pickup_wise_total_seat.setText("PickUp Wise Total Seats : " + calTotalSeat(hashMap_data.get(key_list.get(listPosition))));

        linearLayoutManager = new LinearLayoutManager(context);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition)));
        holder.recycler_pickup_drop_pass_details.setLayoutManager(linearLayoutManager);
        holder.recycler_pickup_drop_pass_details.setAdapter(childLayoutAdapter);

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        List<PickupDropWiseBookingDetails_Response.Datum> SeatDataList;
        private Context context;


        public ChildLayoutAdapter(Context mContext, List<PickupDropWiseBookingDetails_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_sr_no, txt_tkt_no, txt_user, txt_total_Seat, txt_seats, txt_customer_name, txt_phone, txt_route_name, txt_booking_type, txt_drop_off, txt_remarks;

            MyViewHolder(View rowView) {
                super(rowView);

                txt_sr_no = rowView.findViewById(R.id.txt_sr_no);
                txt_tkt_no = rowView.findViewById(R.id.txt_tkt_no);
                txt_user = rowView.findViewById(R.id.txt_user);
                txt_total_Seat = rowView.findViewById(R.id.txt_total_Seat);
                txt_seats = rowView.findViewById(R.id.txt_seats);
                txt_customer_name = rowView.findViewById(R.id.txt_customer_name);
                txt_phone = rowView.findViewById(R.id.txt_phone);
                txt_route_name = rowView.findViewById(R.id.txt_route_name);
                txt_booking_type = rowView.findViewById(R.id.txt_booking_type);
                txt_drop_off = rowView.findViewById(R.id.txt_drop_off);
                txt_remarks = rowView.findViewById(R.id.txt_remarks);

            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView;
            iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pickup_dropwise_passenger_detail_childlayout, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


            holder.txt_sr_no.setText((listPosition + 1) + "");
            holder.txt_tkt_no.setText(SeatDataList.get(listPosition).getTicketNo());
            holder.txt_user.setText(SeatDataList.get(listPosition).getUserID());
            holder.txt_total_Seat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.txt_seats.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.txt_customer_name.setText(SeatDataList.get(listPosition).getCustomerName());
            holder.txt_phone.setText(SeatDataList.get(listPosition).getPhone());
            holder.txt_route_name.setText(SeatDataList.get(listPosition).getFromCity());
            holder.txt_booking_type.setText(SeatDataList.get(listPosition).getBookingType());
            holder.txt_drop_off.setText(SeatDataList.get(listPosition).getDropName());
            holder.txt_remarks.setText(SeatDataList.get(listPosition).getRemarks());

        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }


    }

    private String calTotalSeat(List<PickupDropWiseBookingDetails_Response.Datum> SeatDataList){
        int totalSeat=0;
        for(PickupDropWiseBookingDetails_Response.Datum data:SeatDataList){
            if(!TextUtils.isEmpty(data.getTotalSeat())){
                totalSeat+=Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSeat);
    }

}


