package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.WaitingReport_Response;
import com.infinityinfoway.itsexe.databinding.ItemWaitingListReportBinding;

import java.util.List;

public class WaitingListReportAdapter extends RecyclerView.Adapter<WaitingListReportAdapter.MyViewHodler> {

    private Context context;
    private List<WaitingReport_Response.Datum> list;
    private OnItemClickListener onItemClickListener;

    public WaitingListReportAdapter(Context context, List<WaitingReport_Response.Datum> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int action_flag, WaitingReport_Response.Datum data);
    }

    @NonNull
    @Override
    public MyViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHodler(ItemWaitingListReportBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHodler holder, int position) {
        WaitingReport_Response.Datum data = list.get(position);

        holder.bind(onItemClickListener,data);

        holder.binding.txtSrNo.setText(String.valueOf(position + 1));
        holder.binding.txtPnr.setText(data.getPNRNO());
        holder.binding.txtCityTime.setText(data.getCityTime());
        holder.binding.txtFrom.setText(data.getFromCityName()+ " To "+data.getToCityName());
        holder.binding.txtPassengerName.setText(data.getPassengerName());
        holder.binding.txtPhone.setText(data.getPhoneNo());
        holder.binding.txtTotalSeat.setText(String.valueOf(data.getJMTotalPassengers()));
        holder.binding.txtBookingOn.setText(data.getBookingDate());
        holder.binding.txtJdate.setText(data.getJourneyDate());
        holder.binding.txtMainRouteName.setText(data.getRouteNameDisplay());


        holder.binding.txtCancel.setPaintFlags(holder.binding.txtCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtLoad.setPaintFlags(holder.binding.txtLoad.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHodler extends RecyclerView.ViewHolder {
        private ItemWaitingListReportBinding binding;

        public MyViewHodler(ItemWaitingListReportBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(OnItemClickListener listener,WaitingReport_Response.Datum data){
            binding.txtLoad.setOnClickListener(view -> listener.onItemClick(0,data));

            binding.txtCancel.setOnClickListener(view -> listener.onItemClick(1,data));
        }

    }
}
