package com.infinityinfoway.itsexe.exe_adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.RouteTimeBookingDetails_Response;
import com.infinityinfoway.itsexe.bean.CargoReportBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RouteTimeBookingMemoReportAdapter extends RecyclerView.Adapter<RouteTimeBookingMemoReportAdapter.MyViewHolder> {

    LinkedHashMap<String, List<RouteTimeBookingDetails_Response.Datum>> hashMap_data;
    List<String> key_list;
    private Context context;
    LinearLayoutManager linearLayoutManager;
    private int positionCounter = 0;

    public interface OnItemClickListener {
        void onItemClick(CargoReportBean item);
    }

    public RouteTimeBookingMemoReportAdapter(Context mContext, LinkedHashMap<String, List<RouteTimeBookingDetails_Response.Datum>> hashMap_data) {
        context = mContext;
        this.hashMap_data = hashMap_data;
        key_list = new ArrayList<>(hashMap_data.keySet());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_group_by, txt_total_Seat, txt_total_amount;
        RecyclerView recycler_view_route_booking_memo;

        MyViewHolder(View rowView) {
            super(rowView);

            txt_group_by = rowView.findViewById(R.id.txt_group_by);
            txt_total_Seat = rowView.findViewById(R.id.txt_total_Seat);
            txt_total_amount = rowView.findViewById(R.id.txt_total_amount);
            recycler_view_route_booking_memo = rowView.findViewById(R.id.recycler_view_route_booking_memo);

        }
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteamView;
        iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_routetime_booking_memo_report, parent, false);
        return new MyViewHolder(iteamView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.txt_group_by.setText(key_list.get(listPosition));
        List<String> arrayList=calGroupTotal(hashMap_data.get(key_list.get(listPosition)));

        holder.txt_total_Seat.setText(arrayList.get(0));
        holder.txt_total_amount.setText(arrayList.get(1));

        linearLayoutManager = new LinearLayoutManager(context);

        ChildLayoutAdapter childLayoutAdapter = new ChildLayoutAdapter(context, hashMap_data.get(key_list.get(listPosition)));
        holder.recycler_view_route_booking_memo.setLayoutManager(linearLayoutManager);
        holder.recycler_view_route_booking_memo.setAdapter(childLayoutAdapter);

        calGroupTotal(hashMap_data.get(key_list.get(listPosition)));

    }

    @Override
    public int getItemCount() {
        return key_list.size();
    }


    public class ChildLayoutAdapter extends RecyclerView.Adapter<ChildLayoutAdapter.MyViewHolder> {


        List<RouteTimeBookingDetails_Response.Datum> SeatDataList;
        private Context context;
        LinearLayoutManager linearLayoutManager;


        public ChildLayoutAdapter(Context mContext, List<RouteTimeBookingDetails_Response.Datum> SeatDataList) {
            this.context = mContext;
            this.SeatDataList = SeatDataList;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView txt_amount, txt_total_seat, txt_seat_no, txt_rate, txt_to_city_name, txt_from_city, txt_Sr_no;


            MyViewHolder(View rowView) {
                super(rowView);
                txt_amount = rowView.findViewById(R.id.txt_amount);
                txt_total_seat = rowView.findViewById(R.id.txt_total_seat);
                txt_seat_no = rowView.findViewById(R.id.txt_seat_no);
                txt_rate = rowView.findViewById(R.id.txt_rate);
                txt_to_city_name = rowView.findViewById(R.id.txt_to_city_name);
                txt_from_city = rowView.findViewById(R.id.txt_from_city);
                txt_Sr_no = rowView.findViewById(R.id.txt_Sr_no);
            }
        }

        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View iteamView;
            iteamView = LayoutInflater.from(parent.getContext()).inflate(R.layout.routetime_booking_memo_report_child_layout, parent, false);
            return new MyViewHolder(iteamView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

            positionCounter++;
            holder.txt_amount.setText(SeatDataList.get(listPosition).getTotalAmount());
            holder.txt_total_seat.setText(SeatDataList.get(listPosition).getTotalSeat());
            holder.txt_seat_no.setText(SeatDataList.get(listPosition).getSeatNo());
            holder.txt_rate.setText(SeatDataList.get(listPosition).getAmount());
            holder.txt_to_city_name.setText(SeatDataList.get(listPosition).getToCityName());
            holder.txt_from_city.setText(SeatDataList.get(listPosition).getFromCityName());
            holder.txt_Sr_no.setText(String.valueOf(positionCounter));


        }

        @Override
        public int getItemCount() {
            return SeatDataList.size();
        }

    }

    private List<String> calGroupTotal(List<RouteTimeBookingDetails_Response.Datum> SeatDataList) {
        List<String> arrayList=new ArrayList<>();
        double totalAmount=0.0;
        int totalSeat=0;
        for (RouteTimeBookingDetails_Response.Datum data : SeatDataList) {
            if(!TextUtils.isEmpty(data.getTotalAmount())){
                totalAmount+=Double.parseDouble(data.getTotalAmount());
            }
            if(!TextUtils.isEmpty(data.getTotalSeat())){
                totalSeat+=Integer.parseInt(data.getTotalSeat());
            }
        }

        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.format("%.2f",totalAmount));

        return arrayList;
    }

}
