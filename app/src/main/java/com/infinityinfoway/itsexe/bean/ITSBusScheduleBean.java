package com.infinityinfoway.itsexe.bean;

import java.io.Serializable;

public class ITSBusScheduleBean implements Serializable {

    public String RouteTimeID, RouteID, RouteTime, RouteName, BusNo, StartTime;
    public int RouteSelectPosition, RouteCompanyID;
    public String BusId, Kilometer,RT_ScheduleCode;
}
