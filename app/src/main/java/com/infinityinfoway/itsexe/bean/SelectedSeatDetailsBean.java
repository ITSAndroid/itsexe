package com.infinityinfoway.itsexe.bean;

import android.text.TextUtils;
import android.widget.Button;

import java.io.Serializable;

public class SelectedSeatDetailsBean implements Serializable {


    private String bADSeatNo;
    private String jPGender;
    private Integer seatStatus;
    private Integer cADSeatType;
    private Integer cADRow;
    private Integer cADCell;
    private Integer cADUDType;
    private Integer cADBlockType;
    private Integer cADRowSpan;
    private Integer cADColspan;
    private int jMPNRNO;
    private Integer jMJourneyFrom;
    private String fromCity;
    private Integer jMJourneyTo;
    private String toCity;
    private String jMPassengerName;
    private String jMPhone1;
    private Integer pMPickupID;
    private String pickUPTime;
    private Integer jPChartFinishStatus;
    private int isAllowProcess;
    private Integer cADBusType;
    private Integer cADFareType;
    private int seatRate;
    private int bookingTypeID;
    private String pSIDepartTime;
    private Integer orderBy;
    private Integer fromSrNo;
    private Integer pSIIsSameDay;
    private Integer totalSeat;
    private Integer totalBookedSeat;
    private String bookingTypeName;
    private String bookingUser;
    private Double totalAmount;
    private String pickupManName;
    private Integer isStopBooking;
    private String reported;
    private String nonReported;
    private String pending;
    private String isIncludeTax;
    private Double serviceTax;
    private Integer arrangementID;
    private Integer busID;
    private String remarks;
    private Integer jMPhoneOnHold;
    private Integer tHIsSeatBlockWeb;
    private String seatHoldBy;
    private String bookingDateTime;
    private Integer bMBranchID;
    private Integer uMUserID;
    private String routeName;
    private String routeTime;
    private String busNo;
    private Double currentKM;
    private Integer routeCompanyID;
    private String journeyDate;
    private String emailID;
    private String holdReleaseDateTime;
    private Integer cAMIsOnline;
    private String b2COrderNo;
    private Integer walletTypeID;
    private Integer cDHSocialDistanceTransactionType;
    private Double cDHSocialDistancePercentage;
    private String dMDriver1;
    private String PaxAge = "0", IdProofNumber = "", IdProofName;
    private String dMPhoneNo = "";
    private int IdProofId = 0, radioButton = 0, routeId = 0, routeTimeId = 0;
    private Double ServiceTaxRoundUp = 0.0;
    private Button seatButton;
    private String bookingTypeColor,phoneBookingTime;

    public String getPhoneBookingTime() {
        return phoneBookingTime;
    }

    public void setPhoneBookingTime(String phoneBookingTime) {
        this.phoneBookingTime = phoneBookingTime;
    }

    public String getBookingTypeColor() {
        return bookingTypeColor;
    }

    public void setBookingTypeColor(String bookingTypeColor) {
        this.bookingTypeColor = bookingTypeColor;
    }

    public Button getSeatButton() {
        return seatButton;
    }

    public void setSeatButton(Button seatButton) {
        this.seatButton = seatButton;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getRouteTimeId() {
        return routeTimeId;
    }

    public void setRouteTimeId(int routeTimeId) {
        this.routeTimeId = routeTimeId;
    }

    public int getRadioButton() {
        return radioButton;
    }

    public void setRadioButton(int radioButton) {
        this.radioButton = radioButton;
    }

    public String getIdProofName() {
        return TextUtils.isEmpty(IdProofName) ? "" : IdProofName;
    }

    public void setIdProofName(String idProofName) {
        IdProofName = idProofName;
    }

    public String getPaxAge() {
        return TextUtils.isEmpty(PaxAge) ? "" : PaxAge;
    }

    public void setPaxAge(String paxAge) {
        PaxAge = paxAge;
    }

    public String getIdProofNumber() {
        return TextUtils.isEmpty(IdProofNumber) ? " " : IdProofNumber;
    }

    public void setIdProofNumber(String idProofNumber) {
        IdProofNumber = idProofNumber;
    }

    public int getIdProofId() {
        return IdProofId;
    }

    public void setIdProofId(int idProofId) {
        IdProofId = idProofId;
    }

    public Double getServiceTaxRoundUp() {
        return ServiceTaxRoundUp;
    }

    public void setServiceTaxRoundUp(Double serviceTaxRoundUp) {
        ServiceTaxRoundUp = serviceTaxRoundUp;
    }

    public String getBADSeatNo() {
        return bADSeatNo;
    }

    public void setBADSeatNo(String bADSeatNo) {
        this.bADSeatNo = bADSeatNo;
    }

    public String getJPGender() {
        return jPGender;
    }

    public void setJPGender(String jPGender) {
        this.jPGender = jPGender;
    }

    public Integer getSeatStatus() {
        return seatStatus;
    }

    public void setSeatStatus(Integer seatStatus) {
        this.seatStatus = seatStatus;
    }

    public Integer getCADSeatType() {
        return cADSeatType;
    }

    public void setCADSeatType(Integer cADSeatType) {
        this.cADSeatType = cADSeatType;
    }

    public Integer getCADRow() {
        return cADRow;
    }

    public void setCADRow(Integer cADRow) {
        this.cADRow = cADRow;
    }

    public Integer getCADCell() {
        return cADCell;
    }

    public void setCADCell(Integer cADCell) {
        this.cADCell = cADCell;
    }

    public Integer getCADUDType() {
        return cADUDType;
    }

    public void setCADUDType(Integer cADUDType) {
        this.cADUDType = cADUDType;
    }

    public Integer getCADBlockType() {
        return cADBlockType;
    }

    public void setCADBlockType(Integer cADBlockType) {
        this.cADBlockType = cADBlockType;
    }

    public Integer getCADRowSpan() {
        return cADRowSpan;
    }

    public void setCADRowSpan(Integer cADRowSpan) {
        this.cADRowSpan = cADRowSpan;
    }

    public Integer getCADColspan() {
        return cADColspan;
    }

    public void setCADColspan(Integer cADColspan) {
        this.cADColspan = cADColspan;
    }

    public int getJMPNRNO() {
        return jMPNRNO;
    }

    public void setJMPNRNO(int jMPNRNO) {
        this.jMPNRNO = jMPNRNO;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getJMPassengerName() {
        return jMPassengerName;
    }

    public void setJMPassengerName(String jMPassengerName) {
        this.jMPassengerName = jMPassengerName;
    }

    public String getJMPhone1() {
        return jMPhone1;
    }

    public void setJMPhone1(String jMPhone1) {
        this.jMPhone1 = jMPhone1;
    }

    public Integer getPMPickupID() {
        return pMPickupID;
    }

    public void setPMPickupID(Integer pMPickupID) {
        this.pMPickupID = pMPickupID;
    }

    public String getPickUPTime() {
        return pickUPTime;
    }

    public void setPickUPTime(String pickUPTime) {
        this.pickUPTime = pickUPTime;
    }

    public Integer getJPChartFinishStatus() {
        return jPChartFinishStatus;
    }

    public void setJPChartFinishStatus(Integer jPChartFinishStatus) {
        this.jPChartFinishStatus = jPChartFinishStatus;
    }

    public int getIsAllowProcess() {
        return isAllowProcess;
    }

    public void setIsAllowProcess(int isAllowProcess) {
        this.isAllowProcess = isAllowProcess;
    }

    public Integer getCADBusType() {
        return cADBusType;
    }

    public void setCADBusType(Integer cADBusType) {
        this.cADBusType = cADBusType;
    }

    public Integer getCADFareType() {
        return cADFareType;
    }

    public void setCADFareType(Integer cADFareType) {
        this.cADFareType = cADFareType;
    }

    public int getSeatRate() {
        return seatRate;
    }

    public void setSeatRate(int seatRate) {
        this.seatRate = seatRate;
    }

    public int getBookingTypeID() {
        return bookingTypeID;
    }

    public void setBookingTypeID(int bookingTypeID) {
        this.bookingTypeID = bookingTypeID;
    }

    public String getPSIDepartTime() {
        return pSIDepartTime;
    }

    public void setPSIDepartTime(String pSIDepartTime) {
        this.pSIDepartTime = pSIDepartTime;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getFromSrNo() {
        return fromSrNo;
    }

    public void setFromSrNo(Integer fromSrNo) {
        this.fromSrNo = fromSrNo;
    }

    public Integer getPSIIsSameDay() {
        return pSIIsSameDay;
    }

    public void setPSIIsSameDay(Integer pSIIsSameDay) {
        this.pSIIsSameDay = pSIIsSameDay;
    }

    public Integer getTotalSeat() {
        return totalSeat;
    }

    public void setTotalSeat(Integer totalSeat) {
        this.totalSeat = totalSeat;
    }

    public Integer getTotalBookedSeat() {
        return totalBookedSeat;
    }

    public void setTotalBookedSeat(Integer totalBookedSeat) {
        this.totalBookedSeat = totalBookedSeat;
    }

    public String getBookingTypeName() {
        return bookingTypeName;
    }

    public void setBookingTypeName(String bookingTypeName) {
        this.bookingTypeName = bookingTypeName;
    }

    public String getBookingUser() {
        return bookingUser;
    }

    public void setBookingUser(String bookingUser) {
        this.bookingUser = bookingUser;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPickupManName() {
        return pickupManName;
    }

    public void setPickupManName(String pickupManName) {
        this.pickupManName = pickupManName;
    }

    public Integer getIsStopBooking() {
        return isStopBooking;
    }

    public void setIsStopBooking(Integer isStopBooking) {
        this.isStopBooking = isStopBooking;
    }

    public String getReported() {
        return reported;
    }

    public void setReported(String reported) {
        this.reported = reported;
    }

    public String getNonReported() {
        return nonReported;
    }

    public void setNonReported(String nonReported) {
        this.nonReported = nonReported;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getIsIncludeTax() {
        return isIncludeTax;
    }

    public void setIsIncludeTax(String isIncludeTax) {
        this.isIncludeTax = isIncludeTax;
    }

    public Double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(Double serviceTax) {
        this.serviceTax = serviceTax;
    }

    public Integer getArrangementID() {
        return arrangementID;
    }

    public void setArrangementID(Integer arrangementID) {
        this.arrangementID = arrangementID;
    }

    public Integer getBusID() {
        return busID;
    }

    public void setBusID(Integer busID) {
        this.busID = busID;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getJMPhoneOnHold() {
        return jMPhoneOnHold;
    }

    public void setJMPhoneOnHold(Integer jMPhoneOnHold) {
        this.jMPhoneOnHold = jMPhoneOnHold;
    }

    public Integer getTHIsSeatBlockWeb() {
        return tHIsSeatBlockWeb;
    }

    public void setTHIsSeatBlockWeb(Integer tHIsSeatBlockWeb) {
        this.tHIsSeatBlockWeb = tHIsSeatBlockWeb;
    }

    public String getSeatHoldBy() {
        return seatHoldBy;
    }

    public void setSeatHoldBy(String seatHoldBy) {
        this.seatHoldBy = seatHoldBy;
    }

    public String getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(String bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getRouteTime() {
        return routeTime;
    }

    public void setRouteTime(String routeTime) {
        this.routeTime = routeTime;
    }

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public Double getCurrentKM() {
        return currentKM;
    }

    public void setCurrentKM(Double currentKM) {
        this.currentKM = currentKM;
    }

    public Integer getRouteCompanyID() {
        return routeCompanyID;
    }

    public void setRouteCompanyID(Integer routeCompanyID) {
        this.routeCompanyID = routeCompanyID;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getHoldReleaseDateTime() {
        return holdReleaseDateTime;
    }

    public void setHoldReleaseDateTime(String holdReleaseDateTime) {
        this.holdReleaseDateTime = holdReleaseDateTime;
    }

    public Integer getCAMIsOnline() {
        return cAMIsOnline;
    }

    public void setCAMIsOnline(Integer cAMIsOnline) {
        this.cAMIsOnline = cAMIsOnline;
    }

    public String getB2COrderNo() {
        return b2COrderNo;
    }

    public void setB2COrderNo(String b2COrderNo) {
        this.b2COrderNo = b2COrderNo;
    }

    public Integer getWalletTypeID() {
        return walletTypeID;
    }

    public void setWalletTypeID(Integer walletTypeID) {
        this.walletTypeID = walletTypeID;
    }

    public Integer getCDHSocialDistanceTransactionType() {
        return cDHSocialDistanceTransactionType;
    }

    public void setCDHSocialDistanceTransactionType(Integer cDHSocialDistanceTransactionType) {
        this.cDHSocialDistanceTransactionType = cDHSocialDistanceTransactionType;
    }

    public Double getCDHSocialDistancePercentage() {
        return cDHSocialDistancePercentage;
    }

    public void setCDHSocialDistancePercentage(Double cDHSocialDistancePercentage) {
        this.cDHSocialDistancePercentage = cDHSocialDistancePercentage;
    }

    public String getDMDriver1() {
        return dMDriver1;
    }

    public void setDMDriver1(String dMDriver1) {
        this.dMDriver1 = dMDriver1;
    }

    public String getDMPhoneNo() {
        return dMPhoneNo;
    }

    public void setDMPhoneNo(String dMPhoneNo) {
        this.dMPhoneNo = dMPhoneNo;
    }

}
