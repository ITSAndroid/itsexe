package com.infinityinfoway.itsexe.bean;

import com.infinityinfoway.itsexe.utils.API_RESPONSE_STATUS;

public class Api_List_Status {

    private String apiName, apiResponseMsg;
    private API_RESPONSE_STATUS apiStatus;

    public String getApiResponseMsg() {
        return apiResponseMsg;
    }

    public void setApiResponseMsg(String apiResponseMsg) {
        this.apiResponseMsg = apiResponseMsg;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public API_RESPONSE_STATUS getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(API_RESPONSE_STATUS apiStatus) {
        this.apiStatus = apiStatus;
    }
}
