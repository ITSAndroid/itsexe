package com.infinityinfoway.itsexe.bean;

import com.infinityinfoway.itsexe.utils.NETWORK_STATUS;

public class NetworkModel {
    private NETWORK_STATUS isConnected;

    public NetworkModel(NETWORK_STATUS isConnected) {
        this.isConnected = isConnected;
    }

    public NETWORK_STATUS getIsConnected() {
        return isConnected;
    }
}
