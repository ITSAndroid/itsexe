package com.infinityinfoway.itsexe.bean;

import java.io.Serializable;



public class DragDropBean implements Serializable {
    public String sel_BusNo, sel_BranchCityName, sel_tocity, seatNo, JM_PNRNO, JM_PassengerName, PM_PickupName, JM_Phone1, sel_mainRouteName,
        sel_routeTime, tableSeatNo, BookingTypeName, SeatDeatailsData, PickupData, BookingUser, COMPANY_NAME, BRANCH_NAME, BRANCH_CODE,
        RouteName, sel_JourneyDate, BookingTypeID;

    public int sel_CompanyID, sel_BranchID, sel_BranchUserID, sel_FromCityID, sel_ToCityID, sel_RouteID, sel_TimeID, PM_PickupID, IsAllowProcess,
        PICKUP_ID, ITSBusSchedulePostion, SP_BOARDINGPOINT_POSITION;
    public double TotalAmount;

}
