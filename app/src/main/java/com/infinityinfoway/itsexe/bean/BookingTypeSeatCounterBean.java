package com.infinityinfoway.itsexe.bean;

public class BookingTypeSeatCounterBean {
    private int BookingTypeId, TotalSeatbook,IsClick,TextColor;
    private String DefaultName, jPGender;

    public int getTextColor() {
        return TextColor;
    }

    public void setTextColor(int textColor) {
        TextColor = textColor;
    }

    public int getIsClick() {
        return IsClick;
    }

    public void setIsClick(int IsClick) {
        this.IsClick = IsClick;
    }

    public String getJPGender() {
        return jPGender;
    }

    public void setJPGender(String jPGender) {
        this.jPGender = jPGender;
    }

    public int getBookingTypeId() {
        return BookingTypeId;
    }

    public void setBookingTypeId(int bookingTypeId) {
        BookingTypeId = bookingTypeId;
    }

    public int getTotalSeatbook() {
        return TotalSeatbook;
    }

    public void setTotalSeatbook(int totalSeatbook) {
        TotalSeatbook = totalSeatbook;
    }

    public String getDefaultName() {
        return DefaultName;
    }

    public void setDefaultName(String defaultName) {
        DefaultName = defaultName;
    }
}
