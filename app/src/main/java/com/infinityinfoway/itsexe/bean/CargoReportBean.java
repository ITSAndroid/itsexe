package com.infinityinfoway.itsexe.bean;

import java.io.Serializable;

public class CargoReportBean implements Serializable {

    private String GRN_No;
    private String SenderName;
    private String SenderNo;
    private String ReceiverName;
    private String ReceiverNo;
    private String TotalQuentity;
    private String TotalCharge;
    private String BookingTypeName;


    public String getGRN_No() {
        return GRN_No;
    }

    public void setGRN_No(String GRN_No) {
        this.GRN_No = GRN_No;
    }

    public String getSenderName() {
        return SenderName;
    }

    public void setSenderName(String senderName) {
        SenderName = senderName;
    }

    public String getSenderNo() {
        return SenderNo;
    }

    public void setSenderNo(String senderNo) {
        SenderNo = senderNo;
    }

    public String getReceiverName() {
        return ReceiverName;
    }

    public void setReceiverName(String receiverName) {
        ReceiverName = receiverName;
    }

    public String getReceiverNo() {
        return ReceiverNo;
    }

    public void setReceiverNo(String receiverNo) {
        ReceiverNo = receiverNo;
    }

    public String getTotalQuentity() {
        return TotalQuentity;
    }

    public void setTotalQuentity(String totalQuentity) {
        TotalQuentity = totalQuentity;
    }

    public String getTotalCharge() {
        return TotalCharge;
    }

    public void setTotalCharge(String totalCharge) {
        TotalCharge = totalCharge;
    }

    public String getBookingTypeName() {
        return BookingTypeName;
    }

    public void setBookingTypeName(String bookingTypeName) {
        BookingTypeName = bookingTypeName;
    }
}
