package com.infinityinfoway.itsexe.utils;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;

public class CustomSnackBar {
    public static Snackbar showSnackbar(Context context, LinearLayout llMain, String title, String content, int duration,int is_top_disp) { // Create the Snackbar
        Snackbar snackbar = Snackbar.make(llMain, "", 3000);
        // 15 is margin from all the sides for snackbar
        int marginFromSides = 30;

        float height = 60;

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        //inflate view
        View snackView = inflater.inflate(R.layout.snackbar_layout, null);

        TextView txt_title = snackView.findViewById(R.id.txt_title);
        TextView txt_content = snackView.findViewById(R.id.txt_content);

        txt_title.setText(title);
        txt_content.setText(content);

        //snackbar.getView().setBackgroundColor(Color.WHITE);
        snackbar.getView().setBackground(context.getResources().getDrawable(R.drawable.round_edges));

        Snackbar.SnackbarLayout snackBarView = (Snackbar.SnackbarLayout) snackbar.getView();
        FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
        parentParams.setMargins(marginFromSides, marginFromSides, marginFromSides, marginFromSides);
//        parentParams.height = (int) height;
        parentParams.height = FrameLayout.LayoutParams.WRAP_CONTENT;
        parentParams.width = FrameLayout.LayoutParams.MATCH_PARENT;
        if(is_top_disp==1){
            parentParams.gravity=Gravity.TOP|Gravity.CENTER_HORIZONTAL;
        }
        snackBarView.setLayoutParams(parentParams);
        snackBarView.addView(snackView, 0);

        return snackbar;
    }
}
