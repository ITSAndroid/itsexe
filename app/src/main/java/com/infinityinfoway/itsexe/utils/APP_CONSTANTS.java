package com.infinityinfoway.itsexe.utils;

import com.infinityinfoway.itsexe.R;

public class APP_CONSTANTS {

    public static String SELECTED_SEAT_COLOR = "#330033";

    public static int COMPANY_ID = 1;
    public static int BRANCH_ID = 9;
    public static int BRANCH_USER_ID = 5927;
    public static int ROUTE_ID = 16647;
    public static int ROUTE_TIME_ID = 44757;
    public static int DIALOG_SLEEP_TIME = 1000;

    // BOOKING AND UPDATE STATUS
    public static final int INSERT = 0;
    public static final int MODIFY = 1;
    public static final int CANCEL = 2;

    // BOOKING AND UPDATE STATUS
    public static final int OWN_ROUTE = 0;
    public static final int OTHER_BRANCH_ROUTE = 1;
    public static final int FRANCHISE_ROUTE = 2;

    // Agent Types

    public static final int GENERAL_AGENT = 0;
    public static final int ONLINE_AGENT = 1;
    public static final int API_AGENT = 2;
    public static final int B2C_AGENT = 3;

    // TICKET - INVOICE PRINT STATUS
    public static final int SEND_TICKET = 0;
    public static final int SEND_INVOICE = 1;
    public static final int DOWNLOAD_TICKET = 2;
    public static final int DOWNLOAD_INVOICE = 3;

    // Righs ON- OFF
    public static final int RIGHTS_ALLOW = 1;
    public static final int RIGHTS_DENY = 0;

    // Status
    public static final int STATUS_FAILED = 0;
    public static final int STATUS_SUCCESS = 1;

    // COMMON
    public static final int TIME_PREVENT_DOUBLE_CLICK = 1000;

    // COMMON WARNING DIALOG TRANSACTION TYPE
    public static final int SEAT_BLOCK_UNBLOCK = 1;
    public static final int NON_REPORTED = 2;
    public static final int OPEN_DOWNLOAD_FILE = 3;

    // FILE VIEW
    public static final int FILE_VIEW = 1;
    public static final int FILE_SHARE = 2;

    //DIALOG TYPE
    public static final int DIALOG_TYPE_CUSTOM_IMAGE = -1;
    public static final int DIALOG_TYPE_ERROR = 0;
    public static final int DIALOG_TYPE_SUCCESS = 1;
    public static final int DIALOG_TYPE_WARNING = 2;


    // MESSAGE

    public static String NO_DATA = "Oops No Data Available";
    public static String NO_DATA_CONTENT = "Currently there is no data , try again";
    public static String CHART_DATA_CONTENT = "Seat chart data unavaialble , try next route or date";


    // BOOKING TYPE

    public static final int CONFIRM_BOOKING = 1;
    public static final int PHONE_BOOKING = 2;
    public static final int AGENT_BOOKING = 3;
    public static final int BRANCH_BOOKING = 4;
    public static final int GUEST_BOOKING = 5;
    public static final int BANK_CARD_BOOKING = 6;
    public static final int CMP_CARD_BOOKING = 7;
    public static final int WAITING_BOOKING = 8;
    public static final int AGENT_QUOTA_BOOKING = 9;
    public static final int OPEN_BOOKING = 11;
    public static final int EMPTY_SEAT_BOOKING = 12;
    public static final int RESERVE_SEAT_BOOKING = 13;
    public static final int STOP_BOOKING = 14;
    public static final int AGENT_PHONE_BOOKING = 15;
    public static final int API_PHONE_BOOKING = 16;
    public static final int AGENT_CC_CARD_BOOKING = 17;
    public static final int ITSPL_WALLET_BOOKING = 18;
    public static final int REMOTE_PAYMENT_BOOKING = 19;
    public static final int CHART_SHARING_BOOKING = 20;
    public static final int OFFLINE_AGENT_BOOKING = 21;
    public static final int LOGIN_AGENT_BOOKING = 22;
    public static final int API_BOOKING = 23;
    public static final int B2C_BOOKING = 24;
    public static final int HOLD_SEAT_BOOKING = 25;


    // SEARCH TITLE

    public static final int SEARCHING_PNR = 0;
    public static final int SEARCHING_SEAT = 1;
    public static final int SEARCHING_MOBILE = 2;
    public static final int SEARCHING_PHONE_TKT_COUNT = 3;
    public static final int SEARCHING_PRINT_CANCEL_TKT = 4;
    public static final int SEARCHING_GET_SCHEDULE_BUSNO = 5;
    public static final int SEARCHING_PAST_BOOK_MOBILE = 6;
    public static final int SEARCHING_GPS_LOCATION_BUS_NO = 7;
    public static final int SEARCHING_CHANGE_PNR_GST = 8;
    public static final int SEARCHING_PRINT_INVOICE = 9;
    public static final int SEARCHING_ESEND_INVOICE = 10;
    public static final int SEARCHING_PNR_LOG = 11;
    public static final int SEARCHING_PNR_FEEDBACK = 12;
    public static final int SEARCHING_INVOICE_PDF = 13;


    // API NAME
    // Login Screen
    public static final String Get_CompanyList="Get_CompanyList";
    public static final String Get_BranchList="Get_BranchList";
    public static final String Get_TabVerifyLogin="Get_TabVerifyLogin";
    public static final String AdminLogin="AdminLogin";



    public static final String Get_ReportType="Get_ReportType";
    public static final String Get_API_EXE_RouteTimeXML="Get_API_EXE_RouteTimeXML";
    public static final String Get_API_EXE_RouteTimeALLXML="Get_API_EXE_RouteTimeALLXML";
    public static final String PickupDrop="PickupDrop";
    public static final String SeatArrangement="SeatArrangement";


}
