package com.infinityinfoway.itsexe.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.lifecycle.LiveData;

import com.infinityinfoway.itsexe.bean.NetworkModel;

public class NetworkLiveData extends LiveData<NetworkModel> {
    private Context context;

    public NetworkLiveData(Context context) {
        this.context = context;
    }

    @Override
    protected void onActive() {
        super.onActive();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(networkReceiver, filter);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        context.unregisterReceiver(networkReceiver);
    }

    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {

                isConnectingToInternet();

               /* NetworkInfo activeNetwork = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if (isConnected) {
                    switch (activeNetwork.getType()) {
                        case ConnectivityManager.TYPE_WIFI:
                            postValue(new NetworkModel(true));
                            break;
                        case ConnectivityManager.TYPE_MOBILE:
                            postValue(new NetworkModel(true));
                            break;
                    }
                } else {
                    postValue(new NetworkModel(false));
                }*/
            }
        }
    };

    public void isConnectingToInternet() {
        NETWORK_STATUS flag = null;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo networkInfo : info) {

                    if(networkInfo.getType()==ConnectivityManager.TYPE_MOBILE){
                        if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                            flag = NETWORK_STATUS.CONNECTED;
                            break;
                        }else if (networkInfo.getState() == NetworkInfo.State.CONNECTING) {
                            flag = NETWORK_STATUS.CONNECTING;
                            break;
                        }else if (networkInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                            flag = NETWORK_STATUS.DISCONNECTED;
                        }else if (networkInfo.getState() == NetworkInfo.State.DISCONNECTING) {
                            flag = NETWORK_STATUS.DISCONNECTING;
                        }else if (networkInfo.getState() == NetworkInfo.State.SUSPENDED) {
                            flag = NETWORK_STATUS.SUSPENDED;
                        }else if (networkInfo.getState() == NetworkInfo.State.UNKNOWN) {
                            flag = NETWORK_STATUS.UNKNOWN;
                        }
                    }else if(networkInfo.getType()==ConnectivityManager.TYPE_WIFI){
                        if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                            flag = NETWORK_STATUS.CONNECTED;
                            break;
                        }else if (networkInfo.getState() == NetworkInfo.State.CONNECTING) {
                            flag = NETWORK_STATUS.CONNECTING;
                            break;
                        }else if (networkInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                            flag = NETWORK_STATUS.DISCONNECTED;
                            /*try{
                                Thread.sleep(1000);
                            }catch (Exception ignored){}*/
                        }else if (networkInfo.getState() == NetworkInfo.State.DISCONNECTING) {
                            flag = NETWORK_STATUS.DISCONNECTING;
                        }else if (networkInfo.getState() == NetworkInfo.State.SUSPENDED) {
                            flag = NETWORK_STATUS.SUSPENDED;
                        }else if (networkInfo.getState() == NetworkInfo.State.UNKNOWN) {
                            flag = NETWORK_STATUS.UNKNOWN;
                        }
                    }


                }
            }
        }

        if(flag!=null){
            postValue(new NetworkModel(flag));
        }


        /*if (flag) {
            postValue(new NetworkModel(flag));
        } else {
            postValue(new NetworkModel(flag));
        }*/

    }

   /* public void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
                NetworkRequest.Builder builder = new NetworkRequest.Builder();
                connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                                                                       @Override
                                                                       public void onAvailable(Network network) {
                                                                           Variables.isNetworkConnected = true; // Global Static Variable
                                                                       }

                                                                       @Override
                                                                       public void onLost(Network network) {
                                                                           Variables.isNetworkConnected = false; // Global Static Variable
                                                                       }


                                                                   }

                );
                Variables.isNetworkConnected = false;
            } else {

            }


        } catch (Exception e) {

        }
    }*/

}
