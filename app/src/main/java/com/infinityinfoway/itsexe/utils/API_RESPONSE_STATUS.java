package com.infinityinfoway.itsexe.utils;

public enum API_RESPONSE_STATUS {
    SUCCESS,
    FAILED,
    NETWORK_ERROR,
    RUNNING,
    PROGRESS_SHOWN,
    NO_DATA
}
