package com.infinityinfoway.itsexe.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.infinityinfoway.itsexe.R;

public class CustomToast {
    public static void showToastMessage(Context context, String message) {

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout= inflater.inflate(R.layout.custom_toast, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));
        TextView tv_toastMsg = layout.findViewById(R.id.tv_toastMsg);
        tv_toastMsg.setText(message);

        Toast customtoast = new Toast(context);
        customtoast.setView(layout);
        customtoast.setDuration(Toast.LENGTH_LONG);
        customtoast.show();
    }

    public static void showToastMessage(Context context, String message,int duration) {

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout;
        layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));
        TextView tv_toastMsg = layout.findViewById(R.id.tv_toastMsg);
        tv_toastMsg.setText(message);

        Toast customtoast = new Toast(context);
        customtoast.setView(layout);
        customtoast.setDuration(duration);
        customtoast.show();
    }
}
