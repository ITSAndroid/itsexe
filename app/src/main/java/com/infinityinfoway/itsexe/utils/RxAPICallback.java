package com.infinityinfoway.itsexe.utils;

public interface RxAPICallback<T> {
    void onSuccess(T t);

    void onFailed(Throwable throwable);
}
