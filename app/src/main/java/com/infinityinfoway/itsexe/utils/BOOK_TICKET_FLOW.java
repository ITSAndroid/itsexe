package com.infinityinfoway.itsexe.utils;

public enum BOOK_TICKET_FLOW {
    BOOK_TICKET_API_CALL,
    OPEN_GST_DIALOG,
    PHONE_BOOKING_TIME_POPUP
}
