package com.infinityinfoway.itsexe.utils;

public enum NETWORK_STATUS {
    CONNECTED,
    CONNECTING,
    DISCONNECTED,
    DISCONNECTING,
    SUSPENDED,
    UNKNOWN
}
