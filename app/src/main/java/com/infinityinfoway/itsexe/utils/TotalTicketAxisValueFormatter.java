package com.infinityinfoway.itsexe.utils;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;


public class TotalTicketAxisValueFormatter implements IAxisValueFormatter {

    private final String[] mMonths;
    //private BarLineChartBase<?> chart;

    public TotalTicketAxisValueFormatter(BarLineChartBase<?> chart, String[] Rangename) {
        this.mMonths = Rangename;
        //this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if(mMonths!=null && mMonths.length>(int) value){
            return mMonths[(int) value];
        }else{
            return "";
        }
    }
}
