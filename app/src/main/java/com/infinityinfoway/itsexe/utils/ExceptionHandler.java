package com.infinityinfoway.itsexe.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Process;

import com.infinityinfoway.itsexe.activity.ExceptionHandlerActivity;

import org.jetbrains.annotations.NotNull;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {

    private Context myContext;
    private String versionName;
    private String phoneModel;
    private String androidVersion;

    private String myactivityname;
    private String versionname_report;

    private static String TAG = "Crash Analytics";

    public ExceptionHandler(Context context, String activityname) {
        myContext = context;
        myactivityname = activityname;
    }


    void recoltInformations(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi;
            // Version
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionCode + "(" + pi.versionName + ")";

            // Package name
            phoneModel = android.os.Build.MODEL;

            // Android version
            androidVersion = android.os.Build.VERSION.RELEASE;

        } catch (NameNotFoundException ignored) {
        }
    }

    public void uncaughtException(@NotNull Thread thread, Throwable exception) {
        recoltInformations(myContext);

        String DnT_report;
        String stacktrace_report;
        Date curDate = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", java.util.Locale.getDefault());


        DnT_report = df.format(curDate);
        String deviceinfo_report = androidVersion + " :: " + phoneModel;

        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);

        stacktrace_report = "" + stackTrace.toString();
        versionname_report = versionName;

        Intent intent = new Intent(myContext, ExceptionHandlerActivity.class);
        intent.putExtra("caller", "error");
        intent.putExtra("DNT_REPORT", DnT_report);
        intent.putExtra("DEVICEINFO_REPORT", deviceinfo_report);
        intent.putExtra("STACKTRACE_REPORT", stacktrace_report);
        intent.putExtra("VERSIONNAME_REPORT", versionname_report);
        intent.putExtra("ACTIVITY_NAME", myactivityname);
        myContext.startActivity(intent);

        Process.killProcess(Process.myPid());
        System.exit(10);
    }
}
