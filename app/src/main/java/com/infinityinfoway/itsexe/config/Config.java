package com.infinityinfoway.itsexe.config;


import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import java.text.DecimalFormat;

public class Config {
    // json endpoint url
    //https://apiitab.itspl.net/
    public static final String PACKAGE_NAME = "com.infinityinfoway.itsexe";
    public static final String db_name = "BusConnect";
    public static final int db_version = 2;
    public static final String APP_NAME = "itsexe";
    public static final String VERIFY_CALL = "IqfDFFWat7w9wPfkYkZQijwu2s8GrRUV2AeciqLDWZ9y24iQv9I3";
    public static final String api_url = "http://apibusops.itspl.net/";
    public static final String str_URL = "https://apibusops.itspl.net/ITSGateway.asmx";
    public static final String str_URL_EXE = "http://apiexebooking.itspl.net/itsgateway.asmx";
    public static final String str_SOAPActURL = "http://apibusops.itspl.net/";
    public static final boolean AEH_ENABLE = false;
    public static final String AEH_URL = "http://aeh.infinity-travel-solutions.com/app-api/service/common-api.php";
    public static final String AEH_ActURL = "http://aeh.infinity-travel-solutions.com/app-api/service/";
    public static final String JSON_API_URL = "https://apiitab.itspl.net/";


    public static double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    public static Spanned fromHtml(String text)
    {
        Spanned result;
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(text);
        }
        return result;
    }

}