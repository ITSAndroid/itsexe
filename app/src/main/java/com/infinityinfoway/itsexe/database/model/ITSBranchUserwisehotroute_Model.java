package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSBranchUserwisehotroute")
public class ITSBranchUserwisehotroute_Model {

    @ColumnInfo(name = "srno")
    private Integer srno;

    @ColumnInfo(name = "BUHR_ID")
    private Integer BUHR_ID;

    @ColumnInfo(name = "CM_FromCityID")
    private Integer CM_FromCityID;

    @ColumnInfo(name = "CM_ToCityID")
    private Integer CM_ToCityID;

    @ColumnInfo(name = "HotRoute")
    private String HotRoute;

    public Integer getSrno() {
        return srno;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public Integer getBUHR_ID() {
        return BUHR_ID;
    }

    public void setBUHR_ID(Integer BUHR_ID) {
        this.BUHR_ID = BUHR_ID;
    }

    public Integer getCM_FromCityID() {
        return CM_FromCityID;
    }

    public void setCM_FromCityID(Integer CM_FromCityID) {
        this.CM_FromCityID = CM_FromCityID;
    }

    public Integer getCM_ToCityID() {
        return CM_ToCityID;
    }

    public void setCM_ToCityID(Integer CM_ToCityID) {
        this.CM_ToCityID = CM_ToCityID;
    }

    public String getHotRoute() {
        return HotRoute;
    }

    public void setHotRoute(String hotRoute) {
        HotRoute = hotRoute;
    }
}
