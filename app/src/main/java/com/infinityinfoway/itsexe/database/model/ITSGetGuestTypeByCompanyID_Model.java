package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSGetGuestTypeByCompanyID")
public class ITSGetGuestTypeByCompanyID_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "GTM_GuestTypeID")
    private int GTM_GuestTypeID;

    @ColumnInfo(name = "GTM_GuestName")
    private String GTM_GuestName;

    @ColumnInfo(name = "CM_CompanyID")
    private int CM_CompanyID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getGTM_GuestTypeID() {
        return GTM_GuestTypeID;
    }

    public void setGTM_GuestTypeID(int GTM_GuestTypeID) {
        this.GTM_GuestTypeID = GTM_GuestTypeID;
    }

    public String getGTM_GuestName() {
        return GTM_GuestName;
    }

    public void setGTM_GuestName(String GTM_GuestName) {
        this.GTM_GuestName = GTM_GuestName;
    }

    public int getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(int CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }
}
