package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.infinityinfoway.itsexe.utils.TimeStampConverter;

import java.util.Date;

@Entity(tableName = "ITSGetCompanyMarquee")
public class ITSGetCompanyMarquee_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "mm_message")
    private String mm_message;

    @ColumnInfo(name = "RM_RouteID")
    private int RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private int RT_RouteTimeID;

    @ColumnInfo(name = "JM_FromDateint")
    @TypeConverters({TimeStampConverter.class})
    private Date JM_FromDateint;

    @ColumnInfo(name = "JM_ToDateint")
    @TypeConverters({TimeStampConverter.class})
    private Date JM_ToDateint;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMm_message() {
        return mm_message;
    }

    public void setMm_message(String mm_message) {
        this.mm_message = mm_message;
    }

    public int getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(int RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public int getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(int RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public Date getJM_FromDateint() {
        return JM_FromDateint;
    }

    public void setJM_FromDateint(Date JM_FromDateint) {
        this.JM_FromDateint = JM_FromDateint;
    }

    public Date getJM_ToDateint() {
        return JM_ToDateint;
    }

    public void setJM_ToDateint(Date JM_ToDateint) {
        this.JM_ToDateint = JM_ToDateint;
    }
}
