package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSDoNotAllowFareChangeInBelowRouteTime")
public class ITSDoNotAllowFareChangeInBelowRouteTime_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "CM_CompanyID")
    private int CM_CompanyID;

    @ColumnInfo(name = "RM_RouteID")
    private int RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private int RT_RouteTimeID;

    public int getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(int CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public int getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(int RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public int getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(int RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
