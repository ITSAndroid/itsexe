package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSGetAllAgentByCompanyID")
public class ITSGetAllAgentByCompanyID_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "AM_AgentID")
    private Integer AM_AgentID;

    @ColumnInfo(name = "AM_AgentName")
    private String AM_AgentName;

    @ColumnInfo(name = "AM_PhoneNo1")
    private String AM_PhoneNo1;

    @ColumnInfo(name = "CM_CityName")
    private String CM_CityName;

    @ColumnInfo(name = "CM_CityId")
    private Integer CM_CityId;

    @ColumnInfo(name = "CAM_IsOnline")
    private Integer CAM_IsOnline;

    @ColumnInfo(name = "CAM_AgentCode")
    private String CAM_AgentCode;

    @ColumnInfo(name = "IsAgentCredit")
    private Integer IsAgentCredit;

    @ColumnInfo(name = "IS_DisplayOnAgentList")
    private Integer IS_DisplayOnAgentList;

    @ColumnInfo(name = "CAM_OtherCharge")
    private Double CAM_OtherCharge;

    @ColumnInfo(name = "CAM_OtherChargeType")
    private Integer CAM_OtherChargeType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAM_AgentID() {
        return AM_AgentID;
    }

    public void setAM_AgentID(Integer AM_AgentID) {
        this.AM_AgentID = AM_AgentID;
    }

    public String getAM_AgentName() {
        return AM_AgentName;
    }

    public void setAM_AgentName(String AM_AgentName) {
        this.AM_AgentName = AM_AgentName;
    }

    public String getAM_PhoneNo1() {
        return AM_PhoneNo1;
    }

    public void setAM_PhoneNo1(String AM_PhoneNo1) {
        this.AM_PhoneNo1 = AM_PhoneNo1;
    }

    public String getCM_CityName() {
        return CM_CityName;
    }

    public void setCM_CityName(String CM_CityName) {
        this.CM_CityName = CM_CityName;
    }

    public Integer getCM_CityId() {
        return CM_CityId;
    }

    public void setCM_CityId(Integer CM_CityId) {
        this.CM_CityId = CM_CityId;
    }

    public Integer getCAM_IsOnline() {
        return CAM_IsOnline;
    }

    public void setCAM_IsOnline(Integer CAM_IsOnline) {
        this.CAM_IsOnline = CAM_IsOnline;
    }

    public String getCAM_AgentCode() {
        return CAM_AgentCode;
    }

    public void setCAM_AgentCode(String CAM_AgentCode) {
        this.CAM_AgentCode = CAM_AgentCode;
    }

    public Integer getIsAgentCredit() {
        return IsAgentCredit;
    }

    public void setIsAgentCredit(Integer isAgentCredit) {
        IsAgentCredit = isAgentCredit;
    }

    public Integer getIS_DisplayOnAgentList() {
        return IS_DisplayOnAgentList;
    }

    public void setIS_DisplayOnAgentList(Integer IS_DisplayOnAgentList) {
        this.IS_DisplayOnAgentList = IS_DisplayOnAgentList;
    }

    public Double getCAM_OtherCharge() {
        return CAM_OtherCharge;
    }

    public void setCAM_OtherCharge(Double CAM_OtherCharge) {
        this.CAM_OtherCharge = CAM_OtherCharge;
    }

    public Integer getCAM_OtherChargeType() {
        return CAM_OtherChargeType;
    }

    public void setCAM_OtherChargeType(Integer CAM_OtherChargeType) {
        this.CAM_OtherChargeType = CAM_OtherChargeType;
    }
}
