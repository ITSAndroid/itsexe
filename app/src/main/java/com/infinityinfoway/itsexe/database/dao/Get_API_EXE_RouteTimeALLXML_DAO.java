package com.infinityinfoway.itsexe.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.database.model.ITSBranchMasterSelectXML_Model;
import com.infinityinfoway.itsexe.database.model.ITSDoNotAllowFareChangeInBelowRouteTime_Model;
import com.infinityinfoway.itsexe.database.model.ITSFetchChartForeColor_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetAllAgentByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetBranchUserByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetCompanyMarquee_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetGuestTypeByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSOnlineWallet_Model;

import java.util.Date;
import java.util.List;

@Dao
public interface Get_API_EXE_RouteTimeALLXML_DAO {

    //================================================================================
    // 1) Server Date
    //================================================================================

   /* @Query("SELECT * FROM ServerDate")
    List<ServerDate_Model> getServerDate();

    @Insert
    void insertServerDate(List<ServerDate_Model> list);

    @Query("DELETE  FROM ServerDate")
    void deleteServerDate();*/

    //================================================================================
    // 2) ITSFetchBusNumber
    //================================================================================

 /*   @Query("SELECT * FROM ITSFetchBusNumber")
    List<ITSFetchBusNumber_Model> getITSFetchBusNumber();

    @Insert
    void insertITSFetchBusNumber(List<ITSFetchBusNumber_Model> list);

    @Query("DELETE  FROM ITSFetchBusNumber")
    void deleteITSFetchBusNumber();*/

    //================================================================================
    // 3) ITSSelectExeSetting
    //================================================================================

   /* @Query("SELECT * FROM ITSSelectExeSetting")
    List<ITSSelectExeSetting_Model> getITSSelectExeSetting();

    @Insert
    void insertITSSelectExeSetting(List<ITSSelectExeSetting_Model> list);

    @Query("DELETE  FROM ITSSelectExeSetting")
    void deleteITSSelectExeSetting();*/

    //================================================================================
    // 4) ITSGetBookingRightsByUser
    //================================================================================

    @Query("SELECT * FROM ITSGetBookingRightsByUser")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingRightsByUser> getITSGetBookingRightsByUser();

    @Insert
    void insertITSGetBookingRightsByUser(List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingRightsByUser> list);

    @Query("DELETE  FROM ITSGetBookingRightsByUser")
    void deleteITSGetBookingRightsByUser();

    //================================================================================
    // 5) ITSGetAllAgentByCompanyID
    //================================================================================

    /*@Query("SELECT DISTINCT CM_CityName,CM_CityId FROM ITSGetAllAgentByCompanyID where IS_DisplayOnAgentList=1 order by CM_CityName")
    List<ITSGetAllAgentByCompanyID_Model> getITSAllAgentCityNameByCompanyId();*/

    @Query("SELECT DISTINCT CM_CityName,CM_CityId FROM ITSGetAllAgentByCompanyID  order by CM_CityName")
    List<ITSGetAllAgentByCompanyID_Model> getITSAllAgentCityNameByCompanyId();

   /* @Query("SELECT AM_AgentName,AM_AgentID FROM ITSGetAllAgentByCompanyID where IS_DisplayOnAgentList=1 and CM_CityId = :cityId order by AM_AgentName")
    List<ITSGetAllAgentByCompanyID_Model> getITSAllAgentByCompanyId(Integer cityId);*/

    @Query("SELECT AM_AgentName,AM_AgentID FROM ITSGetAllAgentByCompanyID where CM_CityId = :cityId order by AM_AgentName")
    List<ITSGetAllAgentByCompanyID_Model> getITSAllAgentByCompanyId(Integer cityId);

    @Query("SELECT * FROM ITSGetAllAgentByCompanyID")
    List<ITSGetAllAgentByCompanyID_Model> getITSGetAllAgentByCompanyID();

    @Insert
    void insertITSGetAllAgentByCompanyID(List<ITSGetAllAgentByCompanyID_Model> list);

    @Query("DELETE  FROM ITSGetAllAgentByCompanyID")
    void deleteITSGetAllAgentByCompanyID();

    //================================================================================
    // 6) ITSGetCompanyMarquee
    //================================================================================

    @Query("SELECT * FROM ITSGetCompanyMarquee where (JM_FromDateint <= :reqDate AND JM_ToDateint >= :reqDate) AND (RM_RouteID = :routeID AND RT_RouteTimeID = :routeTimeID) ")
    List<ITSGetCompanyMarquee_Model> getITSGetCompanyMarquee(Date reqDate, int routeID, int routeTimeID);

    @Query("SELECT * FROM ITSGetCompanyMarquee where (JM_FromDateint <= :reqDate AND JM_ToDateint >= :reqDate) ORDER BY id ASC LIMIT 1 ")
    List<ITSGetCompanyMarquee_Model> getITSGetCompanyMarqueeTop(Date reqDate);

    @Insert
    void insertITSGetCompanyMarquee(List<ITSGetCompanyMarquee_Model> list);

    @Query("DELETE  FROM ITSGetCompanyMarquee")
    void deleteITSGetCompanyMarquee();

    //================================================================================
    // 7) ITSGetITSMarquee
    //================================================================================

    @Query("SELECT * FROM ITSGetITSMarquee")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetITSMarquee> getITSGetITSMarquee();

    @Insert
    void insertITSGetITSMarquee(List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetITSMarquee> list);

    @Query("DELETE  FROM ITSGetITSMarquee")
    void deleteITSGetITSMarquee();

    //================================================================================
    // 8) ITSBranchMasterSelectXML
    //================================================================================

    @Query("SELECT * FROM ITSBranchMasterSelectXML order by BM_BranchName")
    List<ITSBranchMasterSelectXML_Model> getITSBranchMasterSelectXML();

    @Insert
    void insertITSBranchMasterSelectXML(List<ITSBranchMasterSelectXML_Model> list);

    @Query("DELETE  FROM ITSBranchMasterSelectXML")
    void deleteITSBranchMasterSelectXML();

    //================================================================================
    // 9) ITSGetGuestTypeByCompanyID
    //================================================================================

    @Query("SELECT * FROM ITSGetGuestTypeByCompanyID order by GTM_GuestName")
    List<ITSGetGuestTypeByCompanyID_Model> getITSGetGuestTypeByCompanyID();

    @Insert
    void insertITSGetGuestTypeByCompanyID(List<ITSGetGuestTypeByCompanyID_Model> list);

    @Query("DELETE  FROM ITSGetGuestTypeByCompanyID")
    void deleteITSGetGuestTypeByCompanyID();

    //================================================================================
    // 10) ITSBusChartArrangement
    //================================================================================

   /* @Query("SELECT * FROM ITSBusChartArrangement")
    List<ITSBusChartArrangement_Model> getITSBusChartArrangement();

    @Insert
    void insertITSBusChartArrangement(List<ITSBusChartArrangement_Model> list);

    @Query("DELETE  FROM ITSBusChartArrangement")
    void deleteITSBusChartArrangement();*/

    //================================================================================
    // 11) ITSBusTypeColorCode
    //================================================================================

    /*@Query("SELECT * FROM ITSBusTypeColorCode")
    List<ITSBusTypeColorCode_Model> getITSBusTypeColorCode();

    @Insert
    void insertITSBusTypeColorCode(List<ITSBusTypeColorCode_Model> list);

    @Query("DELETE  FROM ITSBusTypeColorCode")
    void deleteITSBusTypeColorCode();*/

    //================================================================================
    // 12) ITSCancelremark
    //================================================================================

    @Query("SELECT * FROM ITSCancelremark")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSCancelremark> getITSCancelremark();

    @Insert
    void insertITSCancelremark(List<Get_API_EXE_RouteTimeALLXML_Response.ITSCancelremark> list);

    @Query("DELETE  FROM ITSCancelremark")
    void deleteITSCancelremark();

    //================================================================================
    // 13) ITSGetBookingTypeByCompanyIDXML
    //================================================================================

    @Query("SELECT * FROM ITSGetBookingTypeByCompanyIDXML")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML> getITSGetBookingTypeByCompanyIDXML();

    @Insert
    void insertITSGetBookingTypeByCompanyIDXML(List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML> list);

    @Query("DELETE  FROM ITSGetBookingTypeByCompanyIDXML")
    void deleteITSGetBookingTypeByCompanyIDXML();

    //================================================================================
    // 14) ITSGetOtherCompanyBookingRightsExe
    //================================================================================

    @Query("SELECT * FROM ITSGetOtherCompanyBookingRightsExe where fromCMCompanyID==:loginCmpId and toCMCompanyID==:bookingCmpId")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetOtherCompanyBookingRightsExe> getITSGetOtherCompanyBookingRightsExe(int loginCmpId,int bookingCmpId);

    @Insert
    void insertITSGetOtherCompanyBookingRightsExe(List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetOtherCompanyBookingRightsExe> list);

    @Query("DELETE  FROM ITSGetOtherCompanyBookingRightsExe")
    void deleteITSGetOtherCompanyBookingRightsExe();

    //================================================================================
    // 15) ITSOnlineWallet
    //================================================================================

    @Query("SELECT * FROM ITSOnlineWallet")
    List<ITSOnlineWallet_Model> getITSOnlineWallet();

    @Insert
    void insertITSOnlineWallet(List<ITSOnlineWallet_Model> list);

    @Query("DELETE  FROM ITSOnlineWallet")
    void deleteITSOnlineWallet();

    //================================================================================
    // 16) ITSRemarkModify
    //================================================================================

    @Query("SELECT * FROM ITSRemarkModify")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkModify> getITSRemarkModify();

    @Insert
    void insertITSRemarkModify(List<Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkModify> list);

    @Query("DELETE  FROM ITSRemarkModify")
    void deleteITSRemarkModify();

    //================================================================================
    // 17) ITSGetBranchUserByCompanyID
    //================================================================================

    @Query("SELECT * FROM ITSGetBranchUserByCompanyID where BM_BranchID= :BranchId order by BUM_UserName")
    List<ITSGetBranchUserByCompanyID_Model> getITSGetBranchUserByCompanyID(Integer BranchId);

    @Insert
    void insertITSGetBranchUserByCompanyID(List<ITSGetBranchUserByCompanyID_Model> list);

    @Query("DELETE  FROM ITSGetBranchUserByCompanyID")
    void deleteITSGetBranchUserByCompanyID();

    //================================================================================
    // 18) ITSGetDisplayRouteBeforeMin
    //================================================================================

    @Query("SELECT * FROM ITSGetDisplayRouteBeforeMin")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetDisplayRouteBeforeMin> getITSGetDisplayRouteBeforeMin();

    @Insert
    void insertITSGetDisplayRouteBeforeMin(List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetDisplayRouteBeforeMin> list);

    @Query("DELETE  FROM ITSGetDisplayRouteBeforeMin")
    void deleteITSGetDisplayRouteBeforeMin();

    //================================================================================
    // 19) XMLKey
    //================================================================================

    /*@Query("SELECT * FROM XMLKey")
    List<XMLKey_Model> getXMLKey();

    @Insert
    void insertXMLKey(List<XMLKey_Model> list);

    @Query("DELETE  FROM XMLKey")
    void deleteXMLKey();*/

    //================================================================================
    // 20) ITSGetExtraRouteTime
    //================================================================================

    /*@Query("SELECT * FROM ITSGetExtraRouteTime")
    List<ITSGetExtraRouteTime_Model> getITSGetExtraRouteTime();

    @Insert
    void insertITSGetExtraRouteTime(List<ITSGetExtraRouteTime_Model> list);

    @Query("DELETE  FROM ITSGetExtraRouteTime")
    void deleteITSGetExtraRouteTime();*/

    //================================================================================
    // 21) ITSGetFareCaptionXML
    //================================================================================

   /* @Query("SELECT * FROM ITSGetFareCaptionXML")
    List<ITSGetFareCaptionXML_Model> getITSGetFareCaptionXML();

    @Insert
    void insertITSGetFareCaptionXML(List<ITSGetFareCaptionXML_Model> list);

    @Query("DELETE  FROM ITSGetFareCaptionXML")
    void deleteITSGetFareCaptionXML();*/

    //================================================================================
    // 22) ITSFetchChartForeColor
    //================================================================================

    @Query("SELECT * FROM ITSFetchChartForeColor")
    List<ITSFetchChartForeColor_Model> getITSFetchChartForeColor();

    @Insert
    void insertITSFetchChartForeColor(List<ITSFetchChartForeColor_Model> list);

    @Query("DELETE  FROM ITSFetchChartForeColor")
    void deleteITSFetchChartForeColor();

    //================================================================================
    // 23) ITSAutoDeletePhoneBookingAfterBookingTime
    //================================================================================

/*
    @Query("SELECT * FROM ITSAutoDeletePhoneBookingAfterBookingTime")
    List<ITSAutoDeletePhoneBookingAfterBookingTime_Model> getITSAutoDeletePhoneBookingAfterBookingTime();

    @Insert
    void insertITSAutoDeletePhoneBookingAfterBookingTime(List<ITSAutoDeletePhoneBookingAfterBookingTime_Model> list);

    @Query("DELETE  FROM ITSAutoDeletePhoneBookingAfterBookingTime")
    void deleteITSAutoDeletePhoneBookingAfterBookingTime();
*/

    //================================================================================
    // 24) ITSBranchUserwisehotroute
    //================================================================================

   /* @Query("SELECT * FROM ITSBranchUserwisehotroute")
    List<ITSBranchUserwisehotroute_Model> getITSBranchUserwisehotroute();

    @Insert
    void insertITSBranchUserwisehotroute(List<ITSBranchUserwisehotroute_Model> list);

    @Query("DELETE  FROM ITSBranchUserwisehotroute")
    void deleteITSBranchUserwisehotroute();*/

    //================================================================================
    // 25) ITSDoNotAllowFareChangeInBelowRouteTime
    //================================================================================

    @Query("SELECT * FROM ITSDoNotAllowFareChangeInBelowRouteTime")
    List<ITSDoNotAllowFareChangeInBelowRouteTime_Model> getITSDoNotAllowFareChangeInBelowRouteTime();

    @Insert
    void insertITSDoNotAllowFareChangeInBelowRouteTime(List<ITSDoNotAllowFareChangeInBelowRouteTime_Model> list);

    @Query("DELETE  FROM ITSDoNotAllowFareChangeInBelowRouteTime")
    void deleteITSDoNotAllowFareChangeInBelowRouteTime();

    //================================================================================
    // 26) ITSRouteWiseChargedAmenity
    //================================================================================

    /*@Query("SELECT * FROM ITSRouteWiseChargedAmenity")
    List<ITSRouteWiseChargedAmenity_Model> getITSRouteWiseChargedAmenity();

    @Insert
    void insertITSRouteWiseChargedAmenity(List<ITSRouteWiseChargedAmenity_Model> list);

    @Query("DELETE  FROM ITSRouteWiseChargedAmenity")
    void deleteITSRouteWiseChargedAmenity();*/

    //================================================================================
    // 27) ITSRemarkRePrint
    //================================================================================

    @Query("SELECT * FROM ITSRemarkRePrint")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkRePrint> getITSRemarkRePrint();

    @Insert
    void insertITSRemarkRePrint(List<Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkRePrint> list);

    @Query("DELETE  FROM ITSRemarkRePrint")
    void deleteITSRemarkRePrint();

    //================================================================================
    // 28) ITSState
    //================================================================================

    @Query("SELECT * FROM ITSState")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSState> getITSState();

    @Insert
    void insertITSState(List<Get_API_EXE_RouteTimeALLXML_Response.ITSState> list);

    @Query("DELETE  FROM ITSState")
    void deleteITSState();

    //================================================================================
    // 29) ITSRouteTimeExtraFareGet
    //================================================================================

    @Query("SELECT * FROM ITSRouteTimeExtraFareGet")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSRouteTimeExtraFareGet> getITSRouteTimeExtraFareGet();

    @Insert
    void insertITSRouteTimeExtraFareGet(List<Get_API_EXE_RouteTimeALLXML_Response.ITSRouteTimeExtraFareGet> list);

    @Query("DELETE  FROM ITSRouteTimeExtraFareGet")
    void deleteITSRouteTimeExtraFareGet();

    //================================================================================
    // 30) ITSRouteTimeInsuranceCharge
    //================================================================================

   /* @Query("SELECT * FROM ITSRouteTimeInsuranceCharge")
    List<ITSRouteTimeInsuranceCharge_Model> getITSRouteTimeInsuranceCharge();

    @Insert
    void insertITSRouteTimeInsuranceCharge(List<ITSRouteTimeInsuranceCharge_Model> list);

    @Query("DELETE  FROM ITSRouteTimeInsuranceCharge")
    void deleteITSRouteTimeInsuranceCharge();*/

    //================================================================================
    // 31) ITSRouteTimeWiseAgent
    //================================================================================

    /*@Query("SELECT * FROM ITSRouteTimeWiseAgent")
    List<ITSRouteTimeWiseAgent_Model> getITSRouteTimeWiseAgent();

    @Insert
    void insertITSRouteTimeWiseAgent(List<ITSRouteTimeWiseAgent_Model> list);

    @Query("DELETE  FROM ITSRouteTimeWiseAgent")
    void deleteITSRouteTimeWiseAgent();*/

    //================================================================================
    // 32) ITSGetIdProof
    //================================================================================

    @Query("SELECT * FROM ITSGetIdProof")
    List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetIdProof> getITSGetIdProof();

    @Insert
    void insertITSGetIdProof(List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetIdProof> list);

    @Query("DELETE  FROM ITSGetIdProof")
    void deleteITSGetIdProof();


}
