package com.infinityinfoway.itsexe.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.database.dao.Get_API_EXE_RouteTimeALLXML_DAO;
import com.infinityinfoway.itsexe.database.dao.Get_API_EXE_RouteTimeXML_DAO;
import com.infinityinfoway.itsexe.database.model.ITSBranchMasterSelectXML_Model;
import com.infinityinfoway.itsexe.database.model.ITSDoNotAllowFareChangeInBelowRouteTime_Model;
import com.infinityinfoway.itsexe.database.model.ITSFetchChartForeColor_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetAllAgentByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetBranchUserByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetCompanyMarquee_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetGuestTypeByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSOnlineWallet_Model;
import com.infinityinfoway.itsexe.utils.TimeStampConverter;

@Database(entities = {Get_API_EXE_RouteTimeXML_Response.RouteTimeXML.class, Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML.class, ITSGetAllAgentByCompanyID_Model.class, ITSBranchMasterSelectXML_Model.class, ITSGetGuestTypeByCompanyID_Model.class,
        ITSGetBranchUserByCompanyID_Model.class, ITSOnlineWallet_Model.class, Get_API_EXE_RouteTimeALLXML_Response.ITSGetDisplayRouteBeforeMin.class, ITSFetchChartForeColor_Model.class, ITSGetCompanyMarquee_Model.class, Get_API_EXE_RouteTimeALLXML_Response.ITSGetITSMarquee.class,
        Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingRightsByUser.class, ITSDoNotAllowFareChangeInBelowRouteTime_Model.class, Get_API_EXE_RouteTimeALLXML_Response.ITSGetOtherCompanyBookingRightsExe.class,
        Get_API_EXE_RouteTimeALLXML_Response.ITSRouteTimeExtraFareGet.class, Get_API_EXE_RouteTimeALLXML_Response.ITSGetIdProof.class, Get_API_EXE_RouteTimeALLXML_Response.ITSState.class,
        Get_API_EXE_RouteTimeALLXML_Response.ITSCancelremark.class, Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkModify.class, Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkRePrint.class}, version = Config.db_version, exportSchema = false)
@TypeConverters({TimeStampConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract Get_API_EXE_RouteTimeXML_DAO getApiExeRouteTimeXMLDao();

    public abstract Get_API_EXE_RouteTimeALLXML_DAO getApiExeRouteTimeALLXMLDao();

}
