package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSRouteTimeInsuranceCharge")
public class ITSRouteTimeInsuranceCharge_Model {


    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    @ColumnInfo(name = "RTIM_IsBranch")
    private Integer RTIM_IsBranch;

    @ColumnInfo(name = "RTIM_BranchAmount")
    private Integer RTIM_BranchAmount;

    @ColumnInfo(name = "RTIM_IsGeneralAgent")
    private Integer RTIM_IsGeneralAgent;

    @ColumnInfo(name = "RTIM_GeneralAgentAmount")
    private Integer RTIM_GeneralAgentAmount;

    @ColumnInfo(name = "RTIM_IsLoginAgent")
    private Integer RTIM_IsLoginAgent;

    @ColumnInfo(name = "RTIM_LoginAgentAmount")
    private Integer RTIM_LoginAgentAmount;

    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public Integer getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(Integer RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public Integer getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(Integer RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public Integer getRTIM_IsBranch() {
        return RTIM_IsBranch;
    }

    public void setRTIM_IsBranch(Integer RTIM_IsBranch) {
        this.RTIM_IsBranch = RTIM_IsBranch;
    }

    public Integer getRTIM_BranchAmount() {
        return RTIM_BranchAmount;
    }

    public void setRTIM_BranchAmount(Integer RTIM_BranchAmount) {
        this.RTIM_BranchAmount = RTIM_BranchAmount;
    }

    public Integer getRTIM_IsGeneralAgent() {
        return RTIM_IsGeneralAgent;
    }

    public void setRTIM_IsGeneralAgent(Integer RTIM_IsGeneralAgent) {
        this.RTIM_IsGeneralAgent = RTIM_IsGeneralAgent;
    }

    public Integer getRTIM_GeneralAgentAmount() {
        return RTIM_GeneralAgentAmount;
    }

    public void setRTIM_GeneralAgentAmount(Integer RTIM_GeneralAgentAmount) {
        this.RTIM_GeneralAgentAmount = RTIM_GeneralAgentAmount;
    }

    public Integer getRTIM_IsLoginAgent() {
        return RTIM_IsLoginAgent;
    }

    public void setRTIM_IsLoginAgent(Integer RTIM_IsLoginAgent) {
        this.RTIM_IsLoginAgent = RTIM_IsLoginAgent;
    }

    public Integer getRTIM_LoginAgentAmount() {
        return RTIM_LoginAgentAmount;
    }

    public void setRTIM_LoginAgentAmount(Integer RTIM_LoginAgentAmount) {
        this.RTIM_LoginAgentAmount = RTIM_LoginAgentAmount;
    }
}
