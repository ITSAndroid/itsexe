package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSAutoDeletePhoneBookingAfterBookingTime")
public class ITSAutoDeletePhoneBookingAfterBookingTime_Model {

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    @ColumnInfo(name = "APB_AutoDeletePhoneBookMinutes")
    private Integer APB_AutoDeletePhoneBookMinutes;


}
