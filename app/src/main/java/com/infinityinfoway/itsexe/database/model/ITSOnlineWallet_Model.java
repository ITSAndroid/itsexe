package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSOnlineWallet")
public class ITSOnlineWallet_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "WCM_ID")
    private Integer WCM_ID;

    @ColumnInfo(name = "WCM_CompanyName")
    private String WCM_CompanyName;

    @ColumnInfo(name = "WCD_WalletKey")
    private String WCD_WalletKey;

    @ColumnInfo(name = "WCD_TDRCharges")
    private Double WCD_TDRCharges;

    @ColumnInfo(name = "WCM_IsOTP")
    private Integer WCM_IsOTP;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWCM_ID() {
        return WCM_ID;
    }

    public void setWCM_ID(Integer WCM_ID) {
        this.WCM_ID = WCM_ID;
    }

    public String getWCM_CompanyName() {
        return WCM_CompanyName;
    }

    public void setWCM_CompanyName(String WCM_CompanyName) {
        this.WCM_CompanyName = WCM_CompanyName;
    }

    public String getWCD_WalletKey() {
        return WCD_WalletKey;
    }

    public void setWCD_WalletKey(String WCD_WalletKey) {
        this.WCD_WalletKey = WCD_WalletKey;
    }

    public Double getWCD_TDRCharges() {
        return WCD_TDRCharges;
    }

    public void setWCD_TDRCharges(Double WCD_TDRCharges) {
        this.WCD_TDRCharges = WCD_TDRCharges;
    }

    public Integer getWCM_IsOTP() {
        return WCM_IsOTP;
    }

    public void setWCM_IsOTP(Integer WCM_IsOTP) {
        this.WCM_IsOTP = WCM_IsOTP;
    }
}
