package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSCancelremark")
public class ITSCancelremark_Model {

    @ColumnInfo(name = "RM_RemarkID")
    private Integer RM_RemarkID;

    @ColumnInfo(name = "RM_RemarkType")
    private String RM_RemarkType;

    @ColumnInfo(name = "RM_RemarkName")
    private String RM_RemarkName;

    public Integer getRM_RemarkID() {
        return RM_RemarkID;
    }

    public void setRM_RemarkID(Integer RM_RemarkID) {
        this.RM_RemarkID = RM_RemarkID;
    }

    public String getRM_RemarkType() {
        return RM_RemarkType;
    }

    public void setRM_RemarkType(String RM_RemarkType) {
        this.RM_RemarkType = RM_RemarkType;
    }

    public String getRM_RemarkName() {
        return RM_RemarkName;
    }

    public void setRM_RemarkName(String RM_RemarkName) {
        this.RM_RemarkName = RM_RemarkName;
    }
}
