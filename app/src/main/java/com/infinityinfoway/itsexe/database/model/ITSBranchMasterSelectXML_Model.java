package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSBranchMasterSelectXML")
public class ITSBranchMasterSelectXML_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "BM_BranchID")
    private Integer BM_BranchID;

    @ColumnInfo(name = "BM_BranchName")
    private String BM_BranchName;

    @ColumnInfo(name = "BM_BranchCode")
    private String BM_BranchCode;

    @ColumnInfo(name = "BM_UserName")
    private String BM_UserName;

    @ColumnInfo(name = "BM_Password")
    private String BM_Password;

    @ColumnInfo(name = "BM_BranchManager")
    private String BM_BranchManager;

    @ColumnInfo(name = "BM_ContactNo")
    private String BM_ContactNo;

    @ColumnInfo(name = "BM_ContactNo1")
    private String BM_ContactNo1;

    @ColumnInfo(name = "CM_CountryID")
    private Integer CM_CountryID;

    @ColumnInfo(name = "SM_StateID")
    private Integer SM_StateID;

    @ColumnInfo(name = "CM_CityID")
    private Integer CM_CityID;

    @ColumnInfo(name = "BM_Address")
    private String BM_Address;

    @ColumnInfo(name = "PM_PickupID")
    private Integer PM_PickupID;

    @ColumnInfo(name = "PM_PickupName")
    private String PM_PickupName;

    @ColumnInfo(name = "BM_PhoneNo")
    private String BM_PhoneNo;

    @ColumnInfo(name = "BM_CommissionAmt")
    private String BM_CommissionAmt;

    @ColumnInfo(name = "BM_CommissionPer")
    private Double BM_CommissionPer;

    @ColumnInfo(name = "BM_NoOfUsers")
    private Integer BM_NoOfUsers;

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "BM_CreatedDate")
    private String BM_CreatedDate;

    @ColumnInfo(name = "BM_IsActive")
    private Integer BM_IsActive;

    @ColumnInfo(name = "BM_AutoDeletePhoneHour")
    private Integer BM_AutoDeletePhoneHour;

    @ColumnInfo(name = "BM_AutoDeletePhoneMin")
    private Integer BM_AutoDeletePhoneMin;

    @ColumnInfo(name = "BM_IsPrepaid")
    private Integer BM_IsPrepaid;

    @ColumnInfo(name = "BM_AutoAvailability")
    private Integer BM_AutoAvailability;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBM_BranchID() {
        return BM_BranchID;
    }

    public void setBM_BranchID(Integer BM_BranchID) {
        this.BM_BranchID = BM_BranchID;
    }

    public String getBM_BranchName() {
        return BM_BranchName;
    }

    public void setBM_BranchName(String BM_BranchName) {
        this.BM_BranchName = BM_BranchName;
    }

    public String getBM_BranchCode() {
        return BM_BranchCode;
    }

    public void setBM_BranchCode(String BM_BranchCode) {
        this.BM_BranchCode = BM_BranchCode;
    }

    public String getBM_UserName() {
        return BM_UserName;
    }

    public void setBM_UserName(String BM_UserName) {
        this.BM_UserName = BM_UserName;
    }

    public String getBM_Password() {
        return BM_Password;
    }

    public void setBM_Password(String BM_Password) {
        this.BM_Password = BM_Password;
    }

    public String getBM_BranchManager() {
        return BM_BranchManager;
    }

    public void setBM_BranchManager(String BM_BranchManager) {
        this.BM_BranchManager = BM_BranchManager;
    }

    public String getBM_ContactNo() {
        return BM_ContactNo;
    }

    public void setBM_ContactNo(String BM_ContactNo) {
        this.BM_ContactNo = BM_ContactNo;
    }

    public String getBM_ContactNo1() {
        return BM_ContactNo1;
    }

    public void setBM_ContactNo1(String BM_ContactNo1) {
        this.BM_ContactNo1 = BM_ContactNo1;
    }

    public Integer getCM_CountryID() {
        return CM_CountryID;
    }

    public void setCM_CountryID(Integer CM_CountryID) {
        this.CM_CountryID = CM_CountryID;
    }

    public Integer getSM_StateID() {
        return SM_StateID;
    }

    public void setSM_StateID(Integer SM_StateID) {
        this.SM_StateID = SM_StateID;
    }

    public Integer getCM_CityID() {
        return CM_CityID;
    }

    public void setCM_CityID(Integer CM_CityID) {
        this.CM_CityID = CM_CityID;
    }

    public String getBM_Address() {
        return BM_Address;
    }

    public void setBM_Address(String BM_Address) {
        this.BM_Address = BM_Address;
    }

    public Integer getPM_PickupID() {
        return PM_PickupID;
    }

    public void setPM_PickupID(Integer PM_PickupID) {
        this.PM_PickupID = PM_PickupID;
    }

    public String getPM_PickupName() {
        return PM_PickupName;
    }

    public void setPM_PickupName(String PM_PickupName) {
        this.PM_PickupName = PM_PickupName;
    }

    public String getBM_PhoneNo() {
        return BM_PhoneNo;
    }

    public void setBM_PhoneNo(String BM_PhoneNo) {
        this.BM_PhoneNo = BM_PhoneNo;
    }

    public String getBM_CommissionAmt() {
        return BM_CommissionAmt;
    }

    public void setBM_CommissionAmt(String BM_CommissionAmt) {
        this.BM_CommissionAmt = BM_CommissionAmt;
    }

    public Double getBM_CommissionPer() {
        return BM_CommissionPer;
    }

    public void setBM_CommissionPer(Double BM_CommissionPer) {
        this.BM_CommissionPer = BM_CommissionPer;
    }

    public Integer getBM_NoOfUsers() {
        return BM_NoOfUsers;
    }

    public void setBM_NoOfUsers(Integer BM_NoOfUsers) {
        this.BM_NoOfUsers = BM_NoOfUsers;
    }

    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public String getBM_CreatedDate() {
        return BM_CreatedDate;
    }

    public void setBM_CreatedDate(String BM_CreatedDate) {
        this.BM_CreatedDate = BM_CreatedDate;
    }

    public Integer getBM_IsActive() {
        return BM_IsActive;
    }

    public void setBM_IsActive(Integer BM_IsActive) {
        this.BM_IsActive = BM_IsActive;
    }

    public Integer getBM_AutoDeletePhoneHour() {
        return BM_AutoDeletePhoneHour;
    }

    public void setBM_AutoDeletePhoneHour(Integer BM_AutoDeletePhoneHour) {
        this.BM_AutoDeletePhoneHour = BM_AutoDeletePhoneHour;
    }

    public Integer getBM_AutoDeletePhoneMin() {
        return BM_AutoDeletePhoneMin;
    }

    public void setBM_AutoDeletePhoneMin(Integer BM_AutoDeletePhoneMin) {
        this.BM_AutoDeletePhoneMin = BM_AutoDeletePhoneMin;
    }

    public Integer getBM_IsPrepaid() {
        return BM_IsPrepaid;
    }

    public void setBM_IsPrepaid(Integer BM_IsPrepaid) {
        this.BM_IsPrepaid = BM_IsPrepaid;
    }

    public Integer getBM_AutoAvailability() {
        return BM_AutoAvailability;
    }

    public void setBM_AutoAvailability(Integer BM_AutoAvailability) {
        this.BM_AutoAvailability = BM_AutoAvailability;
    }
}
