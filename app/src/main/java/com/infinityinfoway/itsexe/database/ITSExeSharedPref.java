package com.infinityinfoway.itsexe.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.infinityinfoway.itsexe.api.response.Get_TabVerifyLogin_Response;

public class ITSExeSharedPref {

    private Context context;

    public ITSExeSharedPref(Context mContext) {
        context = mContext;
    }

    public void setLoginCompanyName(String LoginCompanyName) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("LoginCompanyName", LoginCompanyName);
        editPref.commit();
    }

    public String getLoginCompanyName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("LoginCompanyName", "");
    }

    //======================================================================================
    //	Is Registered
    //======================================================================================

    public void setStartTime(String startTime) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("StartTime", startTime);
        editPref.commit();
    }

    public String getStartTime() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("StartTime", "");
    }


    public int getAdavanceDateShow() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("AdvanceDateShow", 0);
    }

    public void setBackDays(int AdvanceDateShow) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("BackDays", AdvanceDateShow);
        editPref.commit();
    }

    public int getBackDays() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BackDays", 0);
    }

    public void setIsAllowStopBooking(int IsAllowStopBooking) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("IsAllowStopBooking", IsAllowStopBooking);
        editPref.commit();
    }

    public int getIsAllowStopBooking() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("IsAllowStopBooking", 0);
    }

    public void setIsAllowFareChange(int IsAllowFareChange) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("IsAllowFareChange", IsAllowFareChange);
        editPref.commit();
    }

    public int getIsAllowFareChange() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("IsAllowFareChange", 0);
    }

    public void setITSBookingType(String strBookingTypeId, String strBookingTypeColor, String strBookingTypeName) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putString("ITSBookingTypeID", strBookingTypeId);
        editPref.putString("ITSBookingTypeColor", strBookingTypeColor);
        editPref.putString("ITSBookingTypeName", strBookingTypeName);
        editPref.commit();
    }

    public String getITSBookingTypeID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("ITSBookingTypeID", "");
    }

    public String getITSBookingTypeColor() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("ITSBookingTypeColor", "");
    }

    public String getITSBookingTypeName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("ITSBookingTypeName", "");
    }

    public void setBranchCityName(String BranchCityName) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putString("BranchCityName", BranchCityName);
        editPref.commit();
    }


    public void setIsLogin(Boolean isLogin) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putBoolean("IS_LOGIN", isLogin);
        editPref.commit();
    }

    public boolean IsLogin() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getBoolean("IS_LOGIN", false);
    }


    public void setEmptySeatColorCode(String EmptySeatColor) {
        // TODO Auto-generated method stub
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putString("EmptySeatColor", EmptySeatColor);
        editPref.commit();
    }

    public String getEmptySeatColorCode() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("EmptySeatColor", "#D6D6D6");
    }

    public void setChkRememberMe(int IsSavePassword) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("IsSavePassword", IsSavePassword);
        editPref.commit();
    }

    public int getIsChkRememberMe() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("IsSavePassword", 0);
    }

    public void setITSBusSchedulePostion(int position) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("ITSBusSchedulePostion", position);
        editPref.commit();
    }

    public int getITSBusSchedulePostion() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("ITSBusSchedulePostion", 0);
    }

    //=====================================================================================
    //  Login Response Rights
    //=====================================================================================

    public void setLoginDetails(Get_TabVerifyLogin_Response.LoginDetail data) {

        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putString("BUM_UserName", data.getBUMUserName());
        editPref.putString("BUM_Password", data.getBUMPassword());
        editPref.putString("BUM_FullName", data.getBUMFullName());
        editPref.putInt("BUM_BranchUserID", data.getBUMBranchUserID());
        editPref.putString("BM_BranchCode", data.getBMBranchCode());
        editPref.putString("CM_Email", data.getCMEmail());
        editPref.putInt("BM_RequiredPhone", data.getBMRequiredPhone());
        editPref.putInt("BM_IsPrepaid", data.getBMIsPrepaid());
        editPref.putInt("CM_CompanyID", data.getCMCompanyID());
        editPref.putInt("BM_BranchID", data.getBMBranchID());
        editPref.putInt("BM_IsBranchTicketPrint", data.getBMIsBranchTicketPrint());
        editPref.putInt("GA_XmlSourceMemory", data.getGAXmlSourceMemory());
        editPref.putInt("IsStoreOnLocalPC", data.getIsStoreOnLocalPC());
        editPref.putInt("BM_IsShowParcel", data.getBMIsShowParcel());
        editPref.putInt("CargoRedirect", data.getCargoRedirect());
        editPref.putInt("ISAppOpen", data.getISAppOpen());
        editPref.putInt("BM_IsAskReprintReason", data.getBMIsAskReprintReason());
        editPref.putInt("BM_IsAskModifyReason", data.getBMIsAskModifyReason());
        editPref.putInt("BM_IsAskCancelReason", data.getBMIsAskCancelReason());
        editPref.putInt("CM_CityID", data.getCMCityID());
        editPref.putString("BM_BranchName", data.getBMBranchName());
        editPref.putString("OTP", data.getOTP());
        editPref.putString("SMSString", data.getSMSString());
        editPref.putInt("BM_AskOTPonPhoneBooking", data.getBMAskOTPonPhoneBooking());
        editPref.putString("OS_SetDefaultBrowser", data.getOSSetDefaultBrowser());
        editPref.putInt("BM_IsAskNonReportedReason", data.getBMIsAskNonReportedReason());
        editPref.commit();
    }

    public String getBUM_UserName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("BUM_UserName", "");
    }

    public String getBUM_Password() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("BUM_Password", "");
    }

    public String getBUM_FullName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("BUM_FullName", "");
    }

    public int getBUM_BranchUserID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BUM_BranchUserID", 0);
    }

    public String getBM_BranchCode() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("BM_BranchCode", "");
    }

    public String getCM_Email() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("CM_Email", "");
    }

    public int getBM_RequiredPhone() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_RequiredPhone", 0);
    }

    public int getBM_IsPrepaid() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsPrepaid", 0);
    }

    public int getCM_CompanyID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("CM_CompanyID", 0);
    }

    public int getBM_BranchID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_BranchID", 0);
    }

    public int getBM_IsBranchTicketPrint() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsBranchTicketPrint", 0);
    }

    public int getGA_XmlSourceMemory() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("GA_XmlSourceMemory", 0);
    }

    public int getIsStoreOnLocalPC() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("IsStoreOnLocalPC", 0);
    }

    public int getBM_IsShowParcel() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsShowParcel", 0);
    }

    public int getCargoRedirect() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("CargoRedirect", 0);
    }

    public int getISAppOpen() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("ISAppOpen", 0);
    }

    public int getBM_IsAskReprintReason() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsAskReprintReason", 0);
    }

    public int getBM_IsAskModifyReason() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsAskModifyReason", 0);
    }

    public int getBM_IsAskCancelReason() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsAskCancelReason", 0);
    }

    public int getCM_CityID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("CM_CityID", 0);
    }

    public String getBM_BranchName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("BM_BranchName(", "");
    }

    public String getOTP() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("OTP", "");
    }

    public String getSMSString() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("SMSString", "");
    }

    public int getBM_AskOTPonPhoneBooking() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_AskOTPonPhoneBooking", 0);
    }

    public String getOS_SetDefaultBrowser() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("OS_SetDefaultBrowser", "");
    }

    public int getBM_IsAskNonReportedReason() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("BM_IsAskNonReportedReason", 0);
    }

    //=====================================================================================
    //  Login Response Rights
    //=====================================================================================


    //======================================================================================
    //saveVersionInfo
    //======================================================================================
    public void saveVersionInfo(int versionCode, String versionName) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("VERSIONCODE", versionCode);
        editPref.putString("VERSIONNAME", versionName);
        editPref.apply();
    }

    public String getVersionName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("VERSIONNAME", "");
    }

    public int getVersionCode() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("VERSIONCODE", 0);

    }

    //======================================================================================
    //	OS_DeviceID
    //======================================================================================
    public void setTBRM_ID(String TBRM_ID) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("TBRM_ID", TBRM_ID);
        editPref.apply();
    }

    public String getTBRM_ID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("TBRM_ID", "");
    }


    public void setOSDeviceID(String osDeviceID) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("OS_DEVICEID", osDeviceID);
        editPref.apply();
    }

    public String getOSDeviceID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("OS_DEVICEID", "");
    }

    //======================================================================================
    //Save Device Details
    //======================================================================================
    public void savePref(String deviceUser, String deviceID, int osVersion, String imei, String deviceModel, String deviceMake, String deviceProduct) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putString("REG_DEVICEUSER", deviceUser);
        editPref.putString("REG_DEVICEID", deviceID);
        editPref.putInt("REG_OSVERSION", osVersion);
        editPref.putString("REG_IMEI", imei);
        editPref.putString("REG_DEVICEMODEL", deviceModel);
        editPref.putString("REG_DEVICEMAKE", deviceMake);
        editPref.putString("REG_DEVICEPRODUCT", deviceProduct);
        editPref.apply();
    }

    public String getdeviceUser() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_DEVICEUSER", "");
    }

    public String getdeviceID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_DEVICEID", "");
    }

    public int getosVersion() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("REG_OSVERSION", 0);
    }

    public String getimei() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_IMEI", "");
    }

    public String getdeviceModel() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_DEVICEMODEL", "");
    }

    public String getdeviceMake() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_DEVICEMAKE", "");
    }

    public String getdeviceProduct() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_DEVICEPRODUCT", "");
    }

    public void setLoginStateId(int StateId) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("StateId", StateId);
        editPref.commit();
    }

    public int getLoginStateId() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("StateId", 0);
    }

    public void setAdminUserId(int AdminUserId) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("AdminUserId", AdminUserId);
        editPref.apply();
    }

    public int getAdminUserId() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("AdminUserId", 0);
    }


    public void setAdminUserNamePassword(String AdminUserName, String AdminUserPassword) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putString("AdminUserName", AdminUserName);
        editPref.putString("AdminUserPassword", AdminUserPassword);
        editPref.apply();
    }

    public String getAdminUserPassword() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("AdminUserPassword", "");
    }

    public String getAdminUserName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("AdminUserName", "");
    }

    public void setAdminRememberMe(boolean AdminRememberMe) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putBoolean("AdminRememberMe", AdminRememberMe);
        editPref.apply();
    }

    public boolean getAdminRememberMe() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getBoolean("AdminRememberMe", false);
    }


}
