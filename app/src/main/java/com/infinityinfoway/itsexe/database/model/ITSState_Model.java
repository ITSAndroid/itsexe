package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSState")
public class ITSState_Model {

    @ColumnInfo(name = "SM_StateID")
    private Integer SM_StateID;

    @ColumnInfo(name = "SM_StateName")
    private String SM_StateName;


}
