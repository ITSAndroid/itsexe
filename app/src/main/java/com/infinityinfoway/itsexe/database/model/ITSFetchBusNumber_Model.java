package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSFetchBusNumber")
public class ITSFetchBusNumber_Model {

    @ColumnInfo(name = "CM_CompanyID")
    private String CM_CompanyID;

    @ColumnInfo(name = "BM_BusID")
    private String BM_BusID;

    @ColumnInfo(name = "BM_BusNo")
    private String BM_BusNo;

    @ColumnInfo(name = "BTM_BusTypeID")
    private String BTM_BusTypeID;

    @ColumnInfo(name = "BM_seating")
    private String BM_seating;

    @ColumnInfo(name = "BM_Remarks")
    private String BM_Remarks;

    @ColumnInfo(name = "BM_IsActive")
    private String BM_IsActive;

    @ColumnInfo(name = "BM_No_Of_Tyres")
    private String BM_No_Of_Tyres;

    public String getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(String CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public String getBM_BusID() {
        return BM_BusID;
    }

    public void setBM_BusID(String BM_BusID) {
        this.BM_BusID = BM_BusID;
    }

    public String getBM_BusNo() {
        return BM_BusNo;
    }

    public void setBM_BusNo(String BM_BusNo) {
        this.BM_BusNo = BM_BusNo;
    }

    public String getBTM_BusTypeID() {
        return BTM_BusTypeID;
    }

    public void setBTM_BusTypeID(String BTM_BusTypeID) {
        this.BTM_BusTypeID = BTM_BusTypeID;
    }

    public String getBM_seating() {
        return BM_seating;
    }

    public void setBM_seating(String BM_seating) {
        this.BM_seating = BM_seating;
    }

    public String getBM_Remarks() {
        return BM_Remarks;
    }

    public void setBM_Remarks(String BM_Remarks) {
        this.BM_Remarks = BM_Remarks;
    }

    public String getBM_IsActive() {
        return BM_IsActive;
    }

    public void setBM_IsActive(String BM_IsActive) {
        this.BM_IsActive = BM_IsActive;
    }

    public String getBM_No_Of_Tyres() {
        return BM_No_Of_Tyres;
    }

    public void setBM_No_Of_Tyres(String BM_No_Of_Tyres) {
        this.BM_No_Of_Tyres = BM_No_Of_Tyres;
    }
}
