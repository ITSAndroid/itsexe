package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSBusChartArrangement")
public class ITSBusChartArrangement_Model {

    @ColumnInfo(name = "CAM_ID")
    private Integer CAM_ID;

    @ColumnInfo(name = "BAM_ID")
    private Integer BAM_ID;

    @ColumnInfo(name = "BAM_Rows")
    private Integer BAM_Rows;

    @ColumnInfo(name = "BAM_Columns")
    private Integer BAM_Columns;

    @ColumnInfo(name = "BAM_Arrangement")
    private String BAM_Arrangement;

    @ColumnInfo(name = "BAM_IsActive")
    private Integer BAM_IsActive;

    @ColumnInfo(name = "Detail_ID")
    private Integer Detail_ID;

    @ColumnInfo(name = "BAD_Row")
    private Integer BAD_Row;

    @ColumnInfo(name = "BAD_Column")
    private Integer BAD_Column;

    @ColumnInfo(name = "BAD_Cell")
    private Integer BAD_Cell;

    @ColumnInfo(name = "BAD_SeatNo")
    private String BAD_SeatNo;

    @ColumnInfo(name = "BAD_BusType")
    private Integer BAD_BusType;

    @ColumnInfo(name = "BAD_SeatType")
    private Integer BAD_SeatType;

    @ColumnInfo(name = "BAD_BlockType")
    private Integer BAD_BlockType;

    @ColumnInfo(name = "BAD_UDType")
    private Integer BAD_UDType;

    @ColumnInfo(name = "BAD_Rowspan")
    private Integer BAD_Rowspan;

    @ColumnInfo(name = "BAD_Colspan")
    private Integer BAD_Colspan;

    @ColumnInfo(name = "BAD_Display")
    private String BAD_Display;

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "CAD_FareType")
    private String CAD_FareType;

    @ColumnInfo(name = "OS_IsSeatWiseFare")
    private Integer OS_IsSeatWiseFare;

    public Integer getCAM_ID() {
        return CAM_ID;
    }

    public void setCAM_ID(Integer CAM_ID) {
        this.CAM_ID = CAM_ID;
    }

    public Integer getBAM_ID() {
        return BAM_ID;
    }

    public void setBAM_ID(Integer BAM_ID) {
        this.BAM_ID = BAM_ID;
    }

    public Integer getBAM_Rows() {
        return BAM_Rows;
    }

    public void setBAM_Rows(Integer BAM_Rows) {
        this.BAM_Rows = BAM_Rows;
    }

    public Integer getBAM_Columns() {
        return BAM_Columns;
    }

    public void setBAM_Columns(Integer BAM_Columns) {
        this.BAM_Columns = BAM_Columns;
    }

    public String getBAM_Arrangement() {
        return BAM_Arrangement;
    }

    public void setBAM_Arrangement(String BAM_Arrangement) {
        this.BAM_Arrangement = BAM_Arrangement;
    }

    public Integer getBAM_IsActive() {
        return BAM_IsActive;
    }

    public void setBAM_IsActive(Integer BAM_IsActive) {
        this.BAM_IsActive = BAM_IsActive;
    }

    public Integer getDetail_ID() {
        return Detail_ID;
    }

    public void setDetail_ID(Integer detail_ID) {
        Detail_ID = detail_ID;
    }

    public Integer getBAD_Row() {
        return BAD_Row;
    }

    public void setBAD_Row(Integer BAD_Row) {
        this.BAD_Row = BAD_Row;
    }

    public Integer getBAD_Column() {
        return BAD_Column;
    }

    public void setBAD_Column(Integer BAD_Column) {
        this.BAD_Column = BAD_Column;
    }

    public Integer getBAD_Cell() {
        return BAD_Cell;
    }

    public void setBAD_Cell(Integer BAD_Cell) {
        this.BAD_Cell = BAD_Cell;
    }

    public String getBAD_SeatNo() {
        return BAD_SeatNo;
    }

    public void setBAD_SeatNo(String BAD_SeatNo) {
        this.BAD_SeatNo = BAD_SeatNo;
    }

    public Integer getBAD_BusType() {
        return BAD_BusType;
    }

    public void setBAD_BusType(Integer BAD_BusType) {
        this.BAD_BusType = BAD_BusType;
    }

    public Integer getBAD_SeatType() {
        return BAD_SeatType;
    }

    public void setBAD_SeatType(Integer BAD_SeatType) {
        this.BAD_SeatType = BAD_SeatType;
    }

    public Integer getBAD_BlockType() {
        return BAD_BlockType;
    }

    public void setBAD_BlockType(Integer BAD_BlockType) {
        this.BAD_BlockType = BAD_BlockType;
    }

    public Integer getBAD_UDType() {
        return BAD_UDType;
    }

    public void setBAD_UDType(Integer BAD_UDType) {
        this.BAD_UDType = BAD_UDType;
    }

    public Integer getBAD_Rowspan() {
        return BAD_Rowspan;
    }

    public void setBAD_Rowspan(Integer BAD_Rowspan) {
        this.BAD_Rowspan = BAD_Rowspan;
    }

    public Integer getBAD_Colspan() {
        return BAD_Colspan;
    }

    public void setBAD_Colspan(Integer BAD_Colspan) {
        this.BAD_Colspan = BAD_Colspan;
    }

    public String getBAD_Display() {
        return BAD_Display;
    }

    public void setBAD_Display(String BAD_Display) {
        this.BAD_Display = BAD_Display;
    }

    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public String getCAD_FareType() {
        return CAD_FareType;
    }

    public void setCAD_FareType(String CAD_FareType) {
        this.CAD_FareType = CAD_FareType;
    }

    public Integer getOS_IsSeatWiseFare() {
        return OS_IsSeatWiseFare;
    }

    public void setOS_IsSeatWiseFare(Integer OS_IsSeatWiseFare) {
        this.OS_IsSeatWiseFare = OS_IsSeatWiseFare;
    }
}
