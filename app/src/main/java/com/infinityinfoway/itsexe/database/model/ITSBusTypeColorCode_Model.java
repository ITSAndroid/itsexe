package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSBusTypeColorCode")
public class ITSBusTypeColorCode_Model {

    @ColumnInfo(name = "BTM_BusTypeID")
    private Integer BTM_BusTypeID;

    @ColumnInfo(name = "BTM_RouteNameDisplayColor")
    private String BTM_RouteNameDisplayColor;

    public Integer getBTM_BusTypeID() {
        return BTM_BusTypeID;
    }

    public void setBTM_BusTypeID(Integer BTM_BusTypeID) {
        this.BTM_BusTypeID = BTM_BusTypeID;
    }

    public String getBTM_RouteNameDisplayColor() {
        return BTM_RouteNameDisplayColor;
    }

    public void setBTM_RouteNameDisplayColor(String BTM_RouteNameDisplayColor) {
        this.BTM_RouteNameDisplayColor = BTM_RouteNameDisplayColor;
    }
}
