package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSRouteTimeWiseAgent")
public class ITSRouteTimeWiseAgent_Model {

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "AM_AgentID")
    private Integer AM_AgentID;

    @ColumnInfo(name = "CAM_CompanyAgentID")
    private Integer CAM_CompanyAgentID;

    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public Integer getAM_AgentID() {
        return AM_AgentID;
    }

    public void setAM_AgentID(Integer AM_AgentID) {
        this.AM_AgentID = AM_AgentID;
    }

    public Integer getCAM_CompanyAgentID() {
        return CAM_CompanyAgentID;
    }

    public void setCAM_CompanyAgentID(Integer CAM_CompanyAgentID) {
        this.CAM_CompanyAgentID = CAM_CompanyAgentID;
    }

    public Integer getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(Integer RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public Integer getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(Integer RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }
}
