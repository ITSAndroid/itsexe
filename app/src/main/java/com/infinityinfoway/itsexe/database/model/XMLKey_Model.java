package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "XMLKey")
public class XMLKey_Model {

    @ColumnInfo(name = "XK_id")
    private Integer XK_id;

    @ColumnInfo(name = "Xk_Remarks")
    private String Xk_Remarks;

    @ColumnInfo(name = "XK_key")
    private String XK_key;


    @ColumnInfo(name = "XK_exeversion")
    private String XK_exeversion;


    @ColumnInfo(name = "XK_AssemblyBuild")
    private Integer XK_AssemblyBuild;


    @ColumnInfo(name = "XK_AssemblyRevision")
    private Integer XK_AssemblyRevision;


    @ColumnInfo(name = "XK_ShowError")
    private Integer XK_ShowError;


    @ColumnInfo(name = "XK_Test")
    private Integer XK_Test;


    @ColumnInfo(name = "Xk_TestConn")
    private String Xk_TestConn;


    @ColumnInfo(name = "Xk_Tracert")
    private String Xk_Tracert;


    @ColumnInfo(name = "XK_HelpLineNo")
    private String XK_HelpLineNo;


    @ColumnInfo(name = "XK_EXE_Domain")
    private String XK_EXE_Domain;


    @ColumnInfo(name = "XK_SMS_Domain")
    private String XK_SMS_Domain;


    @ColumnInfo(name = "XK_Email_Domain")
    private String XK_Email_Domain;


    @ColumnInfo(name = "XK_VersionRemarks")
    private String XK_VersionRemarks;


    @ColumnInfo(name = "XK_CopyRightYear")
    private String XK_CopyRightYear;

    @ColumnInfo(name = "XK_InfinityQSVersion")
    private String XK_InfinityQSVersion;

    @ColumnInfo(name = "XK_PrinterVersion")
    private String XK_PrinterVersion;

    @ColumnInfo(name = "WCM_ID")
    private Integer WCM_ID;

    @ColumnInfo(name = "WCM_CompanyName")
    private String WCM_CompanyName;

    @ColumnInfo(name = "WCD_WalletKey")
    private String WCD_WalletKey;

    @ColumnInfo(name = "WCD_TDRCharges")
    private Double WCD_TDRCharges;

    public Integer getXK_id() {
        return XK_id;
    }

    public void setXK_id(Integer XK_id) {
        this.XK_id = XK_id;
    }

    public String getXk_Remarks() {
        return Xk_Remarks;
    }

    public void setXk_Remarks(String xk_Remarks) {
        Xk_Remarks = xk_Remarks;
    }

    public String getXK_key() {
        return XK_key;
    }

    public void setXK_key(String XK_key) {
        this.XK_key = XK_key;
    }

    public String getXK_exeversion() {
        return XK_exeversion;
    }

    public void setXK_exeversion(String XK_exeversion) {
        this.XK_exeversion = XK_exeversion;
    }

    public Integer getXK_AssemblyBuild() {
        return XK_AssemblyBuild;
    }

    public void setXK_AssemblyBuild(Integer XK_AssemblyBuild) {
        this.XK_AssemblyBuild = XK_AssemblyBuild;
    }

    public Integer getXK_AssemblyRevision() {
        return XK_AssemblyRevision;
    }

    public void setXK_AssemblyRevision(Integer XK_AssemblyRevision) {
        this.XK_AssemblyRevision = XK_AssemblyRevision;
    }

    public Integer getXK_ShowError() {
        return XK_ShowError;
    }

    public void setXK_ShowError(Integer XK_ShowError) {
        this.XK_ShowError = XK_ShowError;
    }

    public Integer getXK_Test() {
        return XK_Test;
    }

    public void setXK_Test(Integer XK_Test) {
        this.XK_Test = XK_Test;
    }

    public String getXk_TestConn() {
        return Xk_TestConn;
    }

    public void setXk_TestConn(String xk_TestConn) {
        Xk_TestConn = xk_TestConn;
    }

    public String getXk_Tracert() {
        return Xk_Tracert;
    }

    public void setXk_Tracert(String xk_Tracert) {
        Xk_Tracert = xk_Tracert;
    }

    public String getXK_HelpLineNo() {
        return XK_HelpLineNo;
    }

    public void setXK_HelpLineNo(String XK_HelpLineNo) {
        this.XK_HelpLineNo = XK_HelpLineNo;
    }

    public String getXK_EXE_Domain() {
        return XK_EXE_Domain;
    }

    public void setXK_EXE_Domain(String XK_EXE_Domain) {
        this.XK_EXE_Domain = XK_EXE_Domain;
    }

    public String getXK_SMS_Domain() {
        return XK_SMS_Domain;
    }

    public void setXK_SMS_Domain(String XK_SMS_Domain) {
        this.XK_SMS_Domain = XK_SMS_Domain;
    }

    public String getXK_Email_Domain() {
        return XK_Email_Domain;
    }

    public void setXK_Email_Domain(String XK_Email_Domain) {
        this.XK_Email_Domain = XK_Email_Domain;
    }

    public String getXK_VersionRemarks() {
        return XK_VersionRemarks;
    }

    public void setXK_VersionRemarks(String XK_VersionRemarks) {
        this.XK_VersionRemarks = XK_VersionRemarks;
    }

    public String getXK_CopyRightYear() {
        return XK_CopyRightYear;
    }

    public void setXK_CopyRightYear(String XK_CopyRightYear) {
        this.XK_CopyRightYear = XK_CopyRightYear;
    }

    public String getXK_InfinityQSVersion() {
        return XK_InfinityQSVersion;
    }

    public void setXK_InfinityQSVersion(String XK_InfinityQSVersion) {
        this.XK_InfinityQSVersion = XK_InfinityQSVersion;
    }

    public String getXK_PrinterVersion() {
        return XK_PrinterVersion;
    }

    public void setXK_PrinterVersion(String XK_PrinterVersion) {
        this.XK_PrinterVersion = XK_PrinterVersion;
    }

    public Integer getWCM_ID() {
        return WCM_ID;
    }

    public void setWCM_ID(Integer WCM_ID) {
        this.WCM_ID = WCM_ID;
    }

    public String getWCM_CompanyName() {
        return WCM_CompanyName;
    }

    public void setWCM_CompanyName(String WCM_CompanyName) {
        this.WCM_CompanyName = WCM_CompanyName;
    }

    public String getWCD_WalletKey() {
        return WCD_WalletKey;
    }

    public void setWCD_WalletKey(String WCD_WalletKey) {
        this.WCD_WalletKey = WCD_WalletKey;
    }

    public Double getWCD_TDRCharges() {
        return WCD_TDRCharges;
    }

    public void setWCD_TDRCharges(Double WCD_TDRCharges) {
        this.WCD_TDRCharges = WCD_TDRCharges;
    }
}
