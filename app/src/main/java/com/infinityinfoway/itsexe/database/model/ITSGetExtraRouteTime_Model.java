package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSGetExtraRouteTime")
public class ITSGetExtraRouteTime_Model {

    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    @ColumnInfo(name = "DATE")
    private String DATE;

    @ColumnInfo(name = "RTM_CategoryType")
    private Integer RTM_CategoryType;

    public Integer getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(Integer RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public Integer getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(Integer RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public Integer getRTM_CategoryType() {
        return RTM_CategoryType;
    }

    public void setRTM_CategoryType(Integer RTM_CategoryType) {
        this.RTM_CategoryType = RTM_CategoryType;
    }
}
