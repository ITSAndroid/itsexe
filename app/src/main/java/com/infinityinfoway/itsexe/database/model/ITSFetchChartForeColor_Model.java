package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSFetchChartForeColor")
public class ITSFetchChartForeColor_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "ES_ConfColor")
    private String ES_ConfColor;

    @ColumnInfo(name = "ES_PhColor")
    private String ES_PhColor;

    @ColumnInfo(name = "ES_AgColor")
    private String ES_AgColor;

    @ColumnInfo(name = "ES_AgQuotaColor")
    private String ES_AgQuotaColor;

    @ColumnInfo(name = "ES_GuestColor")
    private String ES_GuestColor;

    @ColumnInfo(name = "ES_BankC")
    private String ES_BankC;

    @ColumnInfo(name = "ES_CompC")
    private String ES_CompC;

    @ColumnInfo(name = "ES_BranchColor")
    private String ES_BranchColor;

    @ColumnInfo(name = "ES_OnlineAgentColor")
    private String ES_OnlineAgentColor;

    @ColumnInfo(name = "ES_B2CColor")
    private String ES_B2CColor;

    @ColumnInfo(name = "ES_APiColor")
    private String ES_APiColor;

    @ColumnInfo(name = "ES_OnlineAgentPhoneColor")
    private String ES_OnlineAgentPhoneColor;

    @ColumnInfo(name = "ES_OnlineAgentCardColor")
    private String ES_OnlineAgentCardColor;

    @ColumnInfo(name = "ES_OnlineWallet")
    private String ES_OnlineWallet;

    @ColumnInfo(name = "ES_RemotePayment")
    private String ES_RemotePayment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getES_ConfColor() {
        return ES_ConfColor;
    }

    public void setES_ConfColor(String ES_ConfColor) {
        this.ES_ConfColor = ES_ConfColor;
    }

    public String getES_PhColor() {
        return ES_PhColor;
    }

    public void setES_PhColor(String ES_PhColor) {
        this.ES_PhColor = ES_PhColor;
    }

    public String getES_AgColor() {
        return ES_AgColor;
    }

    public void setES_AgColor(String ES_AgColor) {
        this.ES_AgColor = ES_AgColor;
    }

    public String getES_AgQuotaColor() {
        return ES_AgQuotaColor;
    }

    public void setES_AgQuotaColor(String ES_AgQuotaColor) {
        this.ES_AgQuotaColor = ES_AgQuotaColor;
    }

    public String getES_GuestColor() {
        return ES_GuestColor;
    }

    public void setES_GuestColor(String ES_GuestColor) {
        this.ES_GuestColor = ES_GuestColor;
    }

    public String getES_BankC() {
        return ES_BankC;
    }

    public void setES_BankC(String ES_BankC) {
        this.ES_BankC = ES_BankC;
    }

    public String getES_CompC() {
        return ES_CompC;
    }

    public void setES_CompC(String ES_CompC) {
        this.ES_CompC = ES_CompC;
    }

    public String getES_BranchColor() {
        return ES_BranchColor;
    }

    public void setES_BranchColor(String ES_BranchColor) {
        this.ES_BranchColor = ES_BranchColor;
    }

    public String getES_OnlineAgentColor() {
        return ES_OnlineAgentColor;
    }

    public void setES_OnlineAgentColor(String ES_OnlineAgentColor) {
        this.ES_OnlineAgentColor = ES_OnlineAgentColor;
    }

    public String getES_B2CColor() {
        return ES_B2CColor;
    }

    public void setES_B2CColor(String ES_B2CColor) {
        this.ES_B2CColor = ES_B2CColor;
    }

    public String getES_APiColor() {
        return ES_APiColor;
    }

    public void setES_APiColor(String ES_APiColor) {
        this.ES_APiColor = ES_APiColor;
    }

    public String getES_OnlineAgentPhoneColor() {
        return ES_OnlineAgentPhoneColor;
    }

    public void setES_OnlineAgentPhoneColor(String ES_OnlineAgentPhoneColor) {
        this.ES_OnlineAgentPhoneColor = ES_OnlineAgentPhoneColor;
    }

    public String getES_OnlineAgentCardColor() {
        return ES_OnlineAgentCardColor;
    }

    public void setES_OnlineAgentCardColor(String ES_OnlineAgentCardColor) {
        this.ES_OnlineAgentCardColor = ES_OnlineAgentCardColor;
    }

    public String getES_OnlineWallet() {
        return ES_OnlineWallet;
    }

    public void setES_OnlineWallet(String ES_OnlineWallet) {
        this.ES_OnlineWallet = ES_OnlineWallet;
    }

    public String getES_RemotePayment() {
        return ES_RemotePayment;
    }

    public void setES_RemotePayment(String ES_RemotePayment) {
        this.ES_RemotePayment = ES_RemotePayment;
    }
}
