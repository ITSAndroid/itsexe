package com.infinityinfoway.itsexe.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;

import java.util.Date;
import java.util.List;

@Dao
public interface Get_API_EXE_RouteTimeXML_DAO {

    @Query("SELECT FromCityName FROM Get_API_EXE_RouteTimeXML WHERE RM_FromCityID = :cityId  ")
    String getCityName(String cityId);

    // Fetch From City And To City

    @Query("SELECT DISTINCT FromCityName FROM Get_API_EXE_RouteTimeXML ORDER BY FromCityName")
    List<String> getFromCity();

    @Query("SELECT DISTINCT ToCityName FROM Get_API_EXE_RouteTimeXML where FromCityName= :fromCity ORDER BY ToCityName")
    List<String> getToCity(String fromCity);

    @Query("SELECT  RTM_FromCityID FROM Get_API_EXE_RouteTimeXML where FromCityName= :fromCity  LIMIT 1")
    String getFromCityId(String fromCity);

    @Query("SELECT  RTM_ToCityID FROM Get_API_EXE_RouteTimeXML where ToCityName= :toCity LIMIT 1")
    String getToCityId(String toCity);

    // Route List

    @Query("SELECT * FROM Get_API_EXE_RouteTimeXML WHERE (RT_FromDate <= :jDate AND RT_ToDate >= :jDate) AND   (RTM_FromCityID = :fromCity AND RTM_ToCityID = :toCity)   ")
    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> fetchRouteList(Date jDate, String fromCity, String toCity);

//    @Query("SELECT * FROM Get_API_EXE_RouteTimeXML WHERE (RT_FromDate <= :jDate AND RT_ToDate >= :jDate) AND (RM_FromCityID = :fromCity AND RM_ToCityID = :toCity) OR (RTM_FromCityID = :fromCity AND RTM_ToCityID = :toCity)   GROUP BY RT_RouteTimeID ORDER BY time(StartTime) ")
//    List<Get_API_EXE_RouteTimeXML_Model> fetchRouteList(Date jDate, String fromCity, String toCity);

    @Insert
    void insertAllGet_API_EXE_RouteTimeXML(List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> list);

    @Query("DELETE  FROM Get_API_EXE_RouteTimeXML")
    void deleteAllGet_API_EXE_RouteTimeXML();

}
