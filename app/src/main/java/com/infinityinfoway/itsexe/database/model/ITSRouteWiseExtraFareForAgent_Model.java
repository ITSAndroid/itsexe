package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSRouteWiseExtraFareForAgent")
public class ITSRouteWiseExtraFareForAgent_Model {


    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    @ColumnInfo(name = "RTM_FromCityID")
    private Integer RTM_FromCityID;

    @ColumnInfo(name = "RTM_ToCityID")
    private Integer RTM_ToCityID;

    @ColumnInfo(name = "OABC_ACSeatRate")
    private Double OABC_ACSeatRate;

    @ColumnInfo(name = "OABC_ACSleeperRate")
    private Double OABC_ACSleeperRate;

    @ColumnInfo(name = "OABC_ACSlumberRate")
    private Double OABC_ACSlumberRate;

    @ColumnInfo(name = "OABC_NonACSeatRate")
    private Double OABC_NonACSeatRate;

    @ColumnInfo(name = "OABC_NonACSleeperRate")
    private Double OABC_NonACSleeperRate;

    @ColumnInfo(name = "OABC_NonACSlumberRate")
    private Double OABC_NonACSlumberRate;

    @ColumnInfo(name = "OABC_OnlineAgent")
    private Integer OABC_OnlineAgent;

    @ColumnInfo(name = "OABC_OfflineAgent")
    private Integer OABC_OfflineAgent;

    public Integer getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(Integer RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public Integer getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(Integer RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public Integer getRTM_FromCityID() {
        return RTM_FromCityID;
    }

    public void setRTM_FromCityID(Integer RTM_FromCityID) {
        this.RTM_FromCityID = RTM_FromCityID;
    }

    public Integer getRTM_ToCityID() {
        return RTM_ToCityID;
    }

    public void setRTM_ToCityID(Integer RTM_ToCityID) {
        this.RTM_ToCityID = RTM_ToCityID;
    }

    public Double getOABC_ACSeatRate() {
        return OABC_ACSeatRate;
    }

    public void setOABC_ACSeatRate(Double OABC_ACSeatRate) {
        this.OABC_ACSeatRate = OABC_ACSeatRate;
    }

    public Double getOABC_ACSleeperRate() {
        return OABC_ACSleeperRate;
    }

    public void setOABC_ACSleeperRate(Double OABC_ACSleeperRate) {
        this.OABC_ACSleeperRate = OABC_ACSleeperRate;
    }

    public Double getOABC_ACSlumberRate() {
        return OABC_ACSlumberRate;
    }

    public void setOABC_ACSlumberRate(Double OABC_ACSlumberRate) {
        this.OABC_ACSlumberRate = OABC_ACSlumberRate;
    }

    public Double getOABC_NonACSeatRate() {
        return OABC_NonACSeatRate;
    }

    public void setOABC_NonACSeatRate(Double OABC_NonACSeatRate) {
        this.OABC_NonACSeatRate = OABC_NonACSeatRate;
    }

    public Double getOABC_NonACSleeperRate() {
        return OABC_NonACSleeperRate;
    }

    public void setOABC_NonACSleeperRate(Double OABC_NonACSleeperRate) {
        this.OABC_NonACSleeperRate = OABC_NonACSleeperRate;
    }

    public Double getOABC_NonACSlumberRate() {
        return OABC_NonACSlumberRate;
    }

    public void setOABC_NonACSlumberRate(Double OABC_NonACSlumberRate) {
        this.OABC_NonACSlumberRate = OABC_NonACSlumberRate;
    }

    public Integer getOABC_OnlineAgent() {
        return OABC_OnlineAgent;
    }

    public void setOABC_OnlineAgent(Integer OABC_OnlineAgent) {
        this.OABC_OnlineAgent = OABC_OnlineAgent;
    }

    public Integer getOABC_OfflineAgent() {
        return OABC_OfflineAgent;
    }

    public void setOABC_OfflineAgent(Integer OABC_OfflineAgent) {
        this.OABC_OfflineAgent = OABC_OfflineAgent;
    }
}
