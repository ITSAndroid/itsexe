package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITSGetBranchUserByCompanyID")
public class ITSGetBranchUserByCompanyID_Model {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "BUM_BranchUserID")
    private Integer BUM_BranchUserID;

    @ColumnInfo(name = "BM_BranchID")
    private Integer BM_BranchID;

    @ColumnInfo(name = "BUM_UserName")
    private String BUM_UserName;

    @ColumnInfo(name = "BUM_Password")
    private String BUM_Password;

    @ColumnInfo(name = "BUM_Position")
    private String BUM_Position;

    @ColumnInfo(name = "BUM_FullName")
    private String BUM_FullName;

    @ColumnInfo(name = "BUM_Address")
    private String BUM_Address;

    @ColumnInfo(name = "BUM_ContactNo")
    private String BUM_ContactNo;

    @ColumnInfo(name = "BUM_Photo")
    private String BUM_Photo;

    @ColumnInfo(name = "SM_StateID")
    private Integer SM_StateID;

    @ColumnInfo(name = "CM_CityID")
    private Integer CM_CityID;

    @ColumnInfo(name = "BUM_ReferenceBy")
    private String BUM_ReferenceBy;

    @ColumnInfo(name = "BUM_ContactNoOfReference")
    private String BUM_ContactNoOfReference;

    @ColumnInfo(name = "BUM_Remarks")
    private String BUM_Remarks;

    @ColumnInfo(name = "BUM_BirthDate")
    private String BUM_BirthDate;

    @ColumnInfo(name = "BUM_BranchCode")
    private String BUM_BranchCode;

    @ColumnInfo(name = "BUM_CreatedDate")
    private String BUM_CreatedDate;

    @ColumnInfo(name = "BUM_IsActive")
    private Integer BUM_IsActive;

    public Integer getBUM_BranchUserID() {
        return BUM_BranchUserID;
    }

    public void setBUM_BranchUserID(Integer BUM_BranchUserID) {
        this.BUM_BranchUserID = BUM_BranchUserID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBM_BranchID() {
        return BM_BranchID;
    }

    public void setBM_BranchID(Integer BM_BranchID) {
        this.BM_BranchID = BM_BranchID;
    }

    public String getBUM_UserName() {
        return BUM_UserName;
    }

    public void setBUM_UserName(String BUM_UserName) {
        this.BUM_UserName = BUM_UserName;
    }

    public String getBUM_Password() {
        return BUM_Password;
    }

    public void setBUM_Password(String BUM_Password) {
        this.BUM_Password = BUM_Password;
    }

    public String getBUM_Position() {
        return BUM_Position;
    }

    public void setBUM_Position(String BUM_Position) {
        this.BUM_Position = BUM_Position;
    }

    public String getBUM_FullName() {
        return BUM_FullName;
    }

    public void setBUM_FullName(String BUM_FullName) {
        this.BUM_FullName = BUM_FullName;
    }

    public String getBUM_Address() {
        return BUM_Address;
    }

    public void setBUM_Address(String BUM_Address) {
        this.BUM_Address = BUM_Address;
    }

    public String getBUM_ContactNo() {
        return BUM_ContactNo;
    }

    public void setBUM_ContactNo(String BUM_ContactNo) {
        this.BUM_ContactNo = BUM_ContactNo;
    }

    public String getBUM_Photo() {
        return BUM_Photo;
    }

    public void setBUM_Photo(String BUM_Photo) {
        this.BUM_Photo = BUM_Photo;
    }

    public Integer getSM_StateID() {
        return SM_StateID;
    }

    public void setSM_StateID(Integer SM_StateID) {
        this.SM_StateID = SM_StateID;
    }

    public Integer getCM_CityID() {
        return CM_CityID;
    }

    public void setCM_CityID(Integer CM_CityID) {
        this.CM_CityID = CM_CityID;
    }

    public String getBUM_ReferenceBy() {
        return BUM_ReferenceBy;
    }

    public void setBUM_ReferenceBy(String BUM_ReferenceBy) {
        this.BUM_ReferenceBy = BUM_ReferenceBy;
    }

    public String getBUM_ContactNoOfReference() {
        return BUM_ContactNoOfReference;
    }

    public void setBUM_ContactNoOfReference(String BUM_ContactNoOfReference) {
        this.BUM_ContactNoOfReference = BUM_ContactNoOfReference;
    }

    public String getBUM_Remarks() {
        return BUM_Remarks;
    }

    public void setBUM_Remarks(String BUM_Remarks) {
        this.BUM_Remarks = BUM_Remarks;
    }

    public String getBUM_BirthDate() {
        return BUM_BirthDate;
    }

    public void setBUM_BirthDate(String BUM_BirthDate) {
        this.BUM_BirthDate = BUM_BirthDate;
    }

    public String getBUM_BranchCode() {
        return BUM_BranchCode;
    }

    public void setBUM_BranchCode(String BUM_BranchCode) {
        this.BUM_BranchCode = BUM_BranchCode;
    }

    public String getBUM_CreatedDate() {
        return BUM_CreatedDate;
    }

    public void setBUM_CreatedDate(String BUM_CreatedDate) {
        this.BUM_CreatedDate = BUM_CreatedDate;
    }

    public Integer getBUM_IsActive() {
        return BUM_IsActive;
    }

    public void setBUM_IsActive(Integer BUM_IsActive) {
        this.BUM_IsActive = BUM_IsActive;
    }

    public String getBUM_Agent_Number() {
        return BUM_Agent_Number;
    }

    public void setBUM_Agent_Number(String BUM_Agent_Number) {
        this.BUM_Agent_Number = BUM_Agent_Number;
    }

    @ColumnInfo(name = "BUM_Agent_Number")
    private String BUM_Agent_Number;


}
