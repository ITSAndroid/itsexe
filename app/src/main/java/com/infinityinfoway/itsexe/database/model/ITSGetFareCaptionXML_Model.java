package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSGetFareCaptionXML")
public class ITSGetFareCaptionXML_Model {

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    @ColumnInfo(name = "FC_ACFare")
    private String FC_ACFare;

    @ColumnInfo(name = "FC_NonACFare")
    private String FC_NonACFare;

    @ColumnInfo(name = "FC_ACSeat")
    private String FC_ACSeat;

    @ColumnInfo(name = "FC_ACSLP")
    private String FC_ACSLP;

    @ColumnInfo(name = "FC_ACSlmb")
    private String FC_ACSlmb;

    @ColumnInfo(name = "FC_NonACSeat")
    private String FC_NonACSeat;

    @ColumnInfo(name = "FC_NonACSLP")
    private String FC_NonACSLP;

    @ColumnInfo(name = "FC_NonACSlmb")
    private String FC_NonACSlmb;

    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public Integer getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(Integer RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public Integer getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(Integer RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public String getFC_ACFare() {
        return FC_ACFare;
    }

    public void setFC_ACFare(String FC_ACFare) {
        this.FC_ACFare = FC_ACFare;
    }

    public String getFC_NonACFare() {
        return FC_NonACFare;
    }

    public void setFC_NonACFare(String FC_NonACFare) {
        this.FC_NonACFare = FC_NonACFare;
    }

    public String getFC_ACSeat() {
        return FC_ACSeat;
    }

    public void setFC_ACSeat(String FC_ACSeat) {
        this.FC_ACSeat = FC_ACSeat;
    }

    public String getFC_ACSLP() {
        return FC_ACSLP;
    }

    public void setFC_ACSLP(String FC_ACSLP) {
        this.FC_ACSLP = FC_ACSLP;
    }

    public String getFC_ACSlmb() {
        return FC_ACSlmb;
    }

    public void setFC_ACSlmb(String FC_ACSlmb) {
        this.FC_ACSlmb = FC_ACSlmb;
    }

    public String getFC_NonACSeat() {
        return FC_NonACSeat;
    }

    public void setFC_NonACSeat(String FC_NonACSeat) {
        this.FC_NonACSeat = FC_NonACSeat;
    }

    public String getFC_NonACSLP() {
        return FC_NonACSLP;
    }

    public void setFC_NonACSLP(String FC_NonACSLP) {
        this.FC_NonACSLP = FC_NonACSLP;
    }

    public String getFC_NonACSlmb() {
        return FC_NonACSlmb;
    }

    public void setFC_NonACSlmb(String FC_NonACSlmb) {
        this.FC_NonACSlmb = FC_NonACSlmb;
    }
}
