package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSRouteWiseChargedAmenity")
public class ITSRouteWiseChargedAmenity_Model {

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "RM_RouteID")
    private Integer RM_RouteID;

    @ColumnInfo(name = "RT_RouteTimeID")
    private Integer RT_RouteTimeID;

    @ColumnInfo(name = "AM_ID")
    private Integer AM_ID;

    @ColumnInfo(name = "AM_AmenityName")
    private String AM_AmenityName;

    @ColumnInfo(name = "RAM_Rate")
    private Double RAM_Rate;

    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public Integer getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(Integer RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public Integer getRT_RouteTimeID() {
        return RT_RouteTimeID;
    }

    public void setRT_RouteTimeID(Integer RT_RouteTimeID) {
        this.RT_RouteTimeID = RT_RouteTimeID;
    }

    public Integer getAM_ID() {
        return AM_ID;
    }

    public void setAM_ID(Integer AM_ID) {
        this.AM_ID = AM_ID;
    }

    public String getAM_AmenityName() {
        return AM_AmenityName;
    }

    public void setAM_AmenityName(String AM_AmenityName) {
        this.AM_AmenityName = AM_AmenityName;
    }

    public Double getRAM_Rate() {
        return RAM_Rate;
    }

    public void setRAM_Rate(Double RAM_Rate) {
        this.RAM_Rate = RAM_Rate;
    }
}
