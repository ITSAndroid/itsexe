package com.infinityinfoway.itsexe.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "ITSSelectExeSetting")
public class ITSSelectExeSetting_Model {

    @ColumnInfo(name = "CM_CompanyID")
    private Integer CM_CompanyID;

    @ColumnInfo(name = "BM_BranchID")
    private Integer BM_BranchID;

    @ColumnInfo(name = "UM_UserID")
    private Integer UM_UserID;

    @ColumnInfo(name = "ES_ThemeID")
    private Integer ES_ThemeID;

    @ColumnInfo(name = "ES_DefaultBookingTypeID")
    private Integer ES_DefaultBookingTypeID;

    @ColumnInfo(name = "ES_BookingDateSetting")
    private Integer ES_BookingDateSetting;

    @ColumnInfo(name = "ES_TicketReport")
    private Integer ES_TicketReport;

    @ColumnInfo(name = "ES_OnlineAgentNeed")
    private Integer ES_OnlineAgentNeed;

    @ColumnInfo(name = "ES_OpenRoutePopUp")
    private Integer ES_OpenRoutePopUp;

    @ColumnInfo(name = "ES_PhoneValidationNeed")
    private Integer ES_PhoneValidationNeed;

    @ColumnInfo(name = "ES_DefaultToolTip")
    private Integer ES_DefaultToolTip;

    @ColumnInfo(name = "ES_IsShowCalender")
    private Integer ES_IsShowCalender;

    @ColumnInfo(name = "PhoneStartCharacter")
    private String PhoneStartCharacter;

    @ColumnInfo(name = "ES_IsShowBusTypeFilter")
    private Integer ES_IsShowBusTypeFilter;

    @ColumnInfo(name = "ES_IsHTTPS")
    private Integer ES_IsHTTPS;

    @ColumnInfo(name = "ES_ClearSeatsOnNewRoute")
    private Integer ES_ClearSeatsOnNewRoute;


    public Integer getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(Integer CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public Integer getBM_BranchID() {
        return BM_BranchID;
    }

    public void setBM_BranchID(Integer BM_BranchID) {
        this.BM_BranchID = BM_BranchID;
    }

    public Integer getUM_UserID() {
        return UM_UserID;
    }

    public void setUM_UserID(Integer UM_UserID) {
        this.UM_UserID = UM_UserID;
    }

    public Integer getES_ThemeID() {
        return ES_ThemeID;
    }

    public void setES_ThemeID(Integer ES_ThemeID) {
        this.ES_ThemeID = ES_ThemeID;
    }

    public Integer getES_DefaultBookingTypeID() {
        return ES_DefaultBookingTypeID;
    }

    public void setES_DefaultBookingTypeID(Integer ES_DefaultBookingTypeID) {
        this.ES_DefaultBookingTypeID = ES_DefaultBookingTypeID;
    }

    public Integer getES_BookingDateSetting() {
        return ES_BookingDateSetting;
    }

    public void setES_BookingDateSetting(Integer ES_BookingDateSetting) {
        this.ES_BookingDateSetting = ES_BookingDateSetting;
    }

    public Integer getES_TicketReport() {
        return ES_TicketReport;
    }

    public void setES_TicketReport(Integer ES_TicketReport) {
        this.ES_TicketReport = ES_TicketReport;
    }

    public Integer getES_OnlineAgentNeed() {
        return ES_OnlineAgentNeed;
    }

    public void setES_OnlineAgentNeed(Integer ES_OnlineAgentNeed) {
        this.ES_OnlineAgentNeed = ES_OnlineAgentNeed;
    }

    public Integer getES_OpenRoutePopUp() {
        return ES_OpenRoutePopUp;
    }

    public void setES_OpenRoutePopUp(Integer ES_OpenRoutePopUp) {
        this.ES_OpenRoutePopUp = ES_OpenRoutePopUp;
    }

    public Integer getES_PhoneValidationNeed() {
        return ES_PhoneValidationNeed;
    }

    public void setES_PhoneValidationNeed(Integer ES_PhoneValidationNeed) {
        this.ES_PhoneValidationNeed = ES_PhoneValidationNeed;
    }

    public Integer getES_DefaultToolTip() {
        return ES_DefaultToolTip;
    }

    public void setES_DefaultToolTip(Integer ES_DefaultToolTip) {
        this.ES_DefaultToolTip = ES_DefaultToolTip;
    }

    public Integer getES_IsShowCalender() {
        return ES_IsShowCalender;
    }

    public void setES_IsShowCalender(Integer ES_IsShowCalender) {
        this.ES_IsShowCalender = ES_IsShowCalender;
    }

    public String getPhoneStartCharacter() {
        return PhoneStartCharacter;
    }

    public void setPhoneStartCharacter(String phoneStartCharacter) {
        PhoneStartCharacter = phoneStartCharacter;
    }

    public Integer getES_IsShowBusTypeFilter() {
        return ES_IsShowBusTypeFilter;
    }

    public void setES_IsShowBusTypeFilter(Integer ES_IsShowBusTypeFilter) {
        this.ES_IsShowBusTypeFilter = ES_IsShowBusTypeFilter;
    }

    public Integer getES_IsHTTPS() {
        return ES_IsHTTPS;
    }

    public void setES_IsHTTPS(Integer ES_IsHTTPS) {
        this.ES_IsHTTPS = ES_IsHTTPS;
    }

    public Integer getES_ClearSeatsOnNewRoute() {
        return ES_ClearSeatsOnNewRoute;
    }

    public void setES_ClearSeatsOnNewRoute(Integer ES_ClearSeatsOnNewRoute) {
        this.ES_ClearSeatsOnNewRoute = ES_ClearSeatsOnNewRoute;
    }
}
