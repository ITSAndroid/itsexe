package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Report_CityPickUpWise_Request {
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("FromCityID")
    @Expose
    private Integer fromCityID;
    @SerializedName("ToCityID")
    @Expose
    private Integer toCityID;
    @SerializedName("RouteID")
    @Expose
    private Integer routeID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("JourneyStartTime")
    @Expose
    private String journeyStartTime;
    @SerializedName("JourneyCityTime")
    @Expose
    private String journeyCityTime;
    @SerializedName("IsSubRoute")
    @Expose
    private Integer isSubRoute;
    @SerializedName("ArrangementID")
    @Expose
    private Integer arrangementID;
    @SerializedName("CityID")
    @Expose
    private Integer cityID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("BookedBy_CM_CompanyID")
    @Expose
    private Integer bookedByCMCompanyID;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getFromCityID() {
        return fromCityID;
    }

    public void setFromCityID(Integer fromCityID) {
        this.fromCityID = fromCityID;
    }

    public Integer getToCityID() {
        return toCityID;
    }

    public void setToCityID(Integer toCityID) {
        this.toCityID = toCityID;
    }

    public Integer getRouteID() {
        return routeID;
    }

    public void setRouteID(Integer routeID) {
        this.routeID = routeID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getJourneyStartTime() {
        return journeyStartTime;
    }

    public void setJourneyStartTime(String journeyStartTime) {
        this.journeyStartTime = journeyStartTime;
    }

    public String getJourneyCityTime() {
        return journeyCityTime;
    }

    public void setJourneyCityTime(String journeyCityTime) {
        this.journeyCityTime = journeyCityTime;
    }

    public Integer getIsSubRoute() {
        return isSubRoute;
    }

    public void setIsSubRoute(Integer isSubRoute) {
        this.isSubRoute = isSubRoute;
    }

    public Integer getArrangementID() {
        return arrangementID;
    }

    public void setArrangementID(Integer arrangementID) {
        this.arrangementID = arrangementID;
    }

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getBookedByCMCompanyID() {
        return bookedByCMCompanyID;
    }

    public void setBookedByCMCompanyID(Integer bookedByCMCompanyID) {
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }

    public Get_Report_CityPickUpWise_Request(Integer companyID, String fromDate, Integer fromCityID, Integer toCityID, Integer routeID, Integer rTTime, String journeyStartTime, String journeyCityTime, Integer isSubRoute, Integer arrangementID, Integer cityID, Integer uMUserID, Integer bookedByCMCompanyID) {
        this.companyID = companyID;
        this.fromDate = fromDate;
        this.fromCityID = fromCityID;
        this.toCityID = toCityID;
        this.routeID = routeID;
        this.rTTime = rTTime;
        this.journeyStartTime = journeyStartTime;
        this.journeyCityTime = journeyCityTime;
        this.isSubRoute = isSubRoute;
        this.arrangementID = arrangementID;
        this.cityID = cityID;
        this.uMUserID = uMUserID;
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }
}
