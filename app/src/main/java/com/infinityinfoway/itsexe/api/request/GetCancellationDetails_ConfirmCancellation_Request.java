package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCancellationDetails_ConfirmCancellation_Request {

    @SerializedName("JM_PNRNO")
    @Expose
    private Integer jMPNRNO;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BUM_BranchUserID")
    @Expose
    private Integer bUMBranchUserID;
    @SerializedName("JM_CancleAmount")
    @Expose
    private String jMCancleAmount;
    @SerializedName("JM_SeatList")
    @Expose
    private String jMSeatList;
    @SerializedName("BTM_BookingTypeID")
    @Expose
    private Integer bTMBookingTypeID;
    @SerializedName("JM_CancelBy")
    @Expose
    private Integer jMCancelBy;
    @SerializedName("JM_Cancel_Amount")
    @Expose
    private String jMCancelAmount;
    @SerializedName("JM_CancelBy_BranchID")
    @Expose
    private Integer jMCancelByBranchID;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_RefundCharges")
    @Expose
    private Double jMRefundCharges;
    @SerializedName("JM_Cancel_Remarks")
    @Expose
    private String jMCancelRemarks;
    @SerializedName("JM_IsBlackList")
    @Expose
    private Integer jMIsBlackList;
    @SerializedName("JM_PCRegistrationID")
    @Expose
    private Integer jMPCRegistrationID;
    @SerializedName("AgentRefundType")
    @Expose
    private Integer agentRefundType;
    @SerializedName("IsWalletCancellation")
    @Expose
    private Integer isWalletCancellation;
    @SerializedName("SeatNo")
    @Expose
    private String seatNo;
    @SerializedName("JM_ITSAdminID")
    @Expose
    private Integer jMITSAdminID;
    @SerializedName("Cancel_RemarkID")
    @Expose
    private Integer cancelRemarkID;
    @SerializedName("JM_CancelFromUserID")
    @Expose
    private Integer jMCancelFromUserID;
    @SerializedName("Walletkey")
    @Expose
    private String walletkey;
    @SerializedName("WalletType")
    @Expose
    private String walletType;
    @SerializedName("JM_Phone")
    @Expose
    private String jMPhone;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("CancelAmount")
    @Expose
    private String cancelAmount;

    public Integer getJMPNRNO() {
        return jMPNRNO;
    }

    public void setJMPNRNO(Integer jMPNRNO) {
        this.jMPNRNO = jMPNRNO;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBUMBranchUserID() {
        return bUMBranchUserID;
    }

    public void setBUMBranchUserID(Integer bUMBranchUserID) {
        this.bUMBranchUserID = bUMBranchUserID;
    }

    public String getJMCancleAmount() {
        return jMCancleAmount;
    }

    public void setJMCancleAmount(String jMCancleAmount) {
        this.jMCancleAmount = jMCancleAmount;
    }

    public String getJMSeatList() {
        return jMSeatList;
    }

    public void setJMSeatList(String jMSeatList) {
        this.jMSeatList = jMSeatList;
    }

    public Integer getBTMBookingTypeID() {
        return bTMBookingTypeID;
    }

    public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
        this.bTMBookingTypeID = bTMBookingTypeID;
    }

    public Integer getJMCancelBy() {
        return jMCancelBy;
    }

    public void setJMCancelBy(Integer jMCancelBy) {
        this.jMCancelBy = jMCancelBy;
    }

    public String getJMCancelAmount() {
        return jMCancelAmount;
    }

    public void setJMCancelAmount(String jMCancelAmount) {
        this.jMCancelAmount = jMCancelAmount;
    }

    public Integer getJMCancelByBranchID() {
        return jMCancelByBranchID;
    }

    public void setJMCancelByBranchID(Integer jMCancelByBranchID) {
        this.jMCancelByBranchID = jMCancelByBranchID;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Double getJMRefundCharges() {
        return jMRefundCharges;
    }

    public void setJMRefundCharges(Double jMRefundCharges) {
        this.jMRefundCharges = jMRefundCharges;
    }

    public String getJMCancelRemarks() {
        return jMCancelRemarks;
    }

    public void setJMCancelRemarks(String jMCancelRemarks) {
        this.jMCancelRemarks = jMCancelRemarks;
    }

    public Integer getJMIsBlackList() {
        return jMIsBlackList;
    }

    public void setJMIsBlackList(Integer jMIsBlackList) {
        this.jMIsBlackList = jMIsBlackList;
    }

    public Integer getJMPCRegistrationID() {
        return jMPCRegistrationID;
    }

    public void setJMPCRegistrationID(Integer jMPCRegistrationID) {
        this.jMPCRegistrationID = jMPCRegistrationID;
    }

    public Integer getAgentRefundType() {
        return agentRefundType;
    }

    public void setAgentRefundType(Integer agentRefundType) {
        this.agentRefundType = agentRefundType;
    }

    public Integer getIsWalletCancellation() {
        return isWalletCancellation;
    }

    public void setIsWalletCancellation(Integer isWalletCancellation) {
        this.isWalletCancellation = isWalletCancellation;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public Integer getJMITSAdminID() {
        return jMITSAdminID;
    }

    public void setJMITSAdminID(Integer jMITSAdminID) {
        this.jMITSAdminID = jMITSAdminID;
    }

    public Integer getCancelRemarkID() {
        return cancelRemarkID;
    }

    public void setCancelRemarkID(Integer cancelRemarkID) {
        this.cancelRemarkID = cancelRemarkID;
    }

    public Integer getJMCancelFromUserID() {
        return jMCancelFromUserID;
    }

    public void setJMCancelFromUserID(Integer jMCancelFromUserID) {
        this.jMCancelFromUserID = jMCancelFromUserID;
    }

    public String getWalletkey() {
        return walletkey;
    }

    public void setWalletkey(String walletkey) {
        this.walletkey = walletkey;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getJMPhone() {
        return jMPhone;
    }

    public void setJMPhone(String jMPhone) {
        this.jMPhone = jMPhone;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCancelAmount() {
        return cancelAmount;
    }

    public void setCancelAmount(String cancelAmount) {
        this.cancelAmount = cancelAmount;
    }

    public GetCancellationDetails_ConfirmCancellation_Request(Integer jMPNRNO, Integer jMBookedByCMCompanyID, Integer bMBranchID, Integer bUMBranchUserID, String jMCancleAmount, String jMSeatList, Integer bTMBookingTypeID, Integer jMCancelBy, String jMCancelAmount, Integer jMCancelByBranchID, Integer cMCompanyID, Double jMRefundCharges, String jMCancelRemarks, Integer jMIsBlackList, Integer jMPCRegistrationID, Integer agentRefundType, Integer isWalletCancellation, String seatNo, Integer jMITSAdminID, Integer cancelRemarkID, Integer jMCancelFromUserID
    ,String walletkey,String walletType,String jMPhone,String orderNo,String cancelAmount) {
        this.jMPNRNO = jMPNRNO;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchID = bMBranchID;
        this.bUMBranchUserID = bUMBranchUserID;
        this.jMCancleAmount = jMCancleAmount;
        this.jMSeatList = jMSeatList;
        this.bTMBookingTypeID = bTMBookingTypeID;
        this.jMCancelBy = jMCancelBy;
        this.jMCancelAmount = jMCancelAmount;
        this.jMCancelByBranchID = jMCancelByBranchID;
        this.cMCompanyID = cMCompanyID;
        this.jMRefundCharges = jMRefundCharges;
        this.jMCancelRemarks = jMCancelRemarks;
        this.jMIsBlackList = jMIsBlackList;
        this.jMPCRegistrationID = jMPCRegistrationID;
        this.agentRefundType = agentRefundType;
        this.isWalletCancellation = isWalletCancellation;
        this.seatNo = seatNo;
        this.jMITSAdminID = jMITSAdminID;
        this.cancelRemarkID = cancelRemarkID;
        this.jMCancelFromUserID = jMCancelFromUserID;
        this.walletkey=walletkey;
        this.walletType=walletType;
        this.jMPhone=jMPhone;
        this.orderNo=orderNo;
        this.cancelAmount=cancelAmount;
    }
}
