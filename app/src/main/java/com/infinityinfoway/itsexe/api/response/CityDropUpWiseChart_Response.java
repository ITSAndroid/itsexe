package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityDropUpWiseChart_Response {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("DropName")
        @Expose
        private String dropName;
        @SerializedName("CityDropTime")
        @Expose
        private String cityDropTime;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("RM_RouteNameDisplay")
        @Expose
        private String rMRouteNameDisplay;
        @SerializedName("RMC_SrNo")
        @Expose
        private Integer rMCSrNo;
        @SerializedName("JM_JourneyTo")
        @Expose
        private Integer jMJourneyTo;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("StartTime")
        @Expose
        private String startTime;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_CompanyAddress")
        @Expose
        private String cMCompanyAddress;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("BusNo")
        @Expose
        private String busNo;

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getDropName() {
            return dropName;
        }

        public void setDropName(String dropName) {
            this.dropName = dropName;
        }

        public String getCityDropTime() {
            return cityDropTime;
        }

        public void setCityDropTime(String cityDropTime) {
            this.cityDropTime = cityDropTime;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getRMRouteNameDisplay() {
            return rMRouteNameDisplay;
        }

        public void setRMRouteNameDisplay(String rMRouteNameDisplay) {
            this.rMRouteNameDisplay = rMRouteNameDisplay;
        }

        public Integer getRMCSrNo() {
            return rMCSrNo;
        }

        public void setRMCSrNo(Integer rMCSrNo) {
            this.rMCSrNo = rMCSrNo;
        }

        public Integer getJMJourneyTo() {
            return jMJourneyTo;
        }

        public void setJMJourneyTo(Integer jMJourneyTo) {
            this.jMJourneyTo = jMJourneyTo;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMCompanyAddress() {
            return cMCompanyAddress;
        }

        public void setCMCompanyAddress(String cMCompanyAddress) {
            this.cMCompanyAddress = cMCompanyAddress;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

    }

}
