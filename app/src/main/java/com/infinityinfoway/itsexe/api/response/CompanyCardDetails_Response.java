package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyCardDetails_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("CCM_CardNo")
        @Expose
        private String cCMCardNo;
        @SerializedName("CCM_Name")
        @Expose
        private String cCMName;
        @SerializedName("CCM_PhoneNo1")
        @Expose
        private String cCMPhoneNo1;
        @SerializedName("CCM_Address")
        @Expose
        private String cCMAddress;
        @SerializedName("CNM_CardNameID")
        @Expose
        private String cNMCardNameID;
        @SerializedName("CNM_Name")
        @Expose
        private String cNMName;
        @SerializedName("CPM_CardPolicyID")
        @Expose
        private Integer cPMCardPolicyID;
        @SerializedName("CPR_MinimumAmount")
        @Expose
        private String cPRMinimumAmount;
        @SerializedName("CPM_Amount")
        @Expose
        private String cPMAmount;
        @SerializedName("CPM_Percentage")
        @Expose
        private String cPMPercentage;
        @SerializedName("CPM_DicountRechargeWise")
        @Expose
        private String cPMDicountRechargeWise;
        @SerializedName("CPR_FromAmount")
        @Expose
        private String cPRFromAmount;
        @SerializedName("CPR_ToAmount")
        @Expose
        private String cPRToAmount;
        @SerializedName("CPR_Percentage")
        @Expose
        private String cPRPercentage;
        @SerializedName("CPR_DiscountRechrgeWise")
        @Expose
        private String cPRDiscountRechrgeWise;
        @SerializedName("CCM_IsPrepaidCard")
        @Expose
        private Integer cCMIsPrepaidCard;
        @SerializedName("CardType")
        @Expose
        private String cardType;
        @SerializedName("CCM_Balance")
        @Expose
        private String cCMBalance;
        @SerializedName("CNM_ISWallet")
        @Expose
        private Integer cNMISWallet;
        @SerializedName("CNM_MinRedemption")
        @Expose
        private String cNMMinRedemption;
        @SerializedName("CCM_OTP")
        @Expose
        private String cCMOTP;
        @SerializedName("SMS_URL")
        @Expose
        private String sMSURL;
        @SerializedName("CNM_CashDiscount")
        @Expose
        private Integer cNMCashDiscount;
        @SerializedName("CNM_WalletCredit")
        @Expose
        private Integer cNMWalletCredit;
        @SerializedName("OS_IsCompanyCardOTP")
        @Expose
        private Integer oSIsCompanyCardOTP;
        @SerializedName("CCM_EmailID")
        @Expose
        private String cCMEmailID;
        @SerializedName("CNM_MaxRedemption")
        @Expose
        private String cNMMaxRedemption;
        @SerializedName("CNM_MaxRedemptionOnTotalAmount")
        @Expose
        private String cNMMaxRedemptionOnTotalAmount;

        public String getCCMCardNo() {
            return cCMCardNo;
        }

        public void setCCMCardNo(String cCMCardNo) {
            this.cCMCardNo = cCMCardNo;
        }

        public String getCCMName() {
            return cCMName;
        }

        public void setCCMName(String cCMName) {
            this.cCMName = cCMName;
        }

        public String getCCMPhoneNo1() {
            return cCMPhoneNo1;
        }

        public void setCCMPhoneNo1(String cCMPhoneNo1) {
            this.cCMPhoneNo1 = cCMPhoneNo1;
        }

        public String getCCMAddress() {
            return cCMAddress;
        }

        public void setCCMAddress(String cCMAddress) {
            this.cCMAddress = cCMAddress;
        }

        public String getCNMCardNameID() {
            return cNMCardNameID;
        }

        public void setCNMCardNameID(String cNMCardNameID) {
            this.cNMCardNameID = cNMCardNameID;
        }

        public String getCNMName() {
            return cNMName;
        }

        public void setCNMName(String cNMName) {
            this.cNMName = cNMName;
        }

        public Integer getCPMCardPolicyID() {
            return cPMCardPolicyID;
        }

        public void setCPMCardPolicyID(Integer cPMCardPolicyID) {
            this.cPMCardPolicyID = cPMCardPolicyID;
        }

        public String getCPRMinimumAmount() {
            return cPRMinimumAmount;
        }

        public void setCPRMinimumAmount(String cPRMinimumAmount) {
            this.cPRMinimumAmount = cPRMinimumAmount;
        }

        public String getCPMAmount() {
            return cPMAmount;
        }

        public void setCPMAmount(String cPMAmount) {
            this.cPMAmount = cPMAmount;
        }

        public String getCPMPercentage() {
            return cPMPercentage;
        }

        public void setCPMPercentage(String cPMPercentage) {
            this.cPMPercentage = cPMPercentage;
        }

        public String getCPMDicountRechargeWise() {
            return cPMDicountRechargeWise;
        }

        public void setCPMDicountRechargeWise(String cPMDicountRechargeWise) {
            this.cPMDicountRechargeWise = cPMDicountRechargeWise;
        }

        public String getCPRFromAmount() {
            return cPRFromAmount;
        }

        public void setCPRFromAmount(String cPRFromAmount) {
            this.cPRFromAmount = cPRFromAmount;
        }

        public String getCPRToAmount() {
            return cPRToAmount;
        }

        public void setCPRToAmount(String cPRToAmount) {
            this.cPRToAmount = cPRToAmount;
        }

        public String getCPRPercentage() {
            return cPRPercentage;
        }

        public void setCPRPercentage(String cPRPercentage) {
            this.cPRPercentage = cPRPercentage;
        }

        public String getCPRDiscountRechrgeWise() {
            return cPRDiscountRechrgeWise;
        }

        public void setCPRDiscountRechrgeWise(String cPRDiscountRechrgeWise) {
            this.cPRDiscountRechrgeWise = cPRDiscountRechrgeWise;
        }

        public Integer getCCMIsPrepaidCard() {
            return cCMIsPrepaidCard;
        }

        public void setCCMIsPrepaidCard(Integer cCMIsPrepaidCard) {
            this.cCMIsPrepaidCard = cCMIsPrepaidCard;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getCCMBalance() {
            return cCMBalance;
        }

        public void setCCMBalance(String cCMBalance) {
            this.cCMBalance = cCMBalance;
        }

        public Integer getCNMISWallet() {
            return cNMISWallet;
        }

        public void setCNMISWallet(Integer cNMISWallet) {
            this.cNMISWallet = cNMISWallet;
        }

        public String getCNMMinRedemption() {
            return cNMMinRedemption;
        }

        public void setCNMMinRedemption(String cNMMinRedemption) {
            this.cNMMinRedemption = cNMMinRedemption;
        }

        public String getCCMOTP() {
            return cCMOTP;
        }

        public void setCCMOTP(String cCMOTP) {
            this.cCMOTP = cCMOTP;
        }

        public String getSMSURL() {
            return sMSURL;
        }

        public void setSMSURL(String sMSURL) {
            this.sMSURL = sMSURL;
        }

        public Integer getCNMCashDiscount() {
            return cNMCashDiscount;
        }

        public void setCNMCashDiscount(Integer cNMCashDiscount) {
            this.cNMCashDiscount = cNMCashDiscount;
        }

        public Integer getCNMWalletCredit() {
            return cNMWalletCredit;
        }

        public void setCNMWalletCredit(Integer cNMWalletCredit) {
            this.cNMWalletCredit = cNMWalletCredit;
        }

        public Integer getOSIsCompanyCardOTP() {
            return oSIsCompanyCardOTP;
        }

        public void setOSIsCompanyCardOTP(Integer oSIsCompanyCardOTP) {
            this.oSIsCompanyCardOTP = oSIsCompanyCardOTP;
        }

        public String getCCMEmailID() {
            return cCMEmailID;
        }

        public void setCCMEmailID(String cCMEmailID) {
            this.cCMEmailID = cCMEmailID;
        }

        public String getCNMMaxRedemption() {
            return cNMMaxRedemption;
        }

        public void setCNMMaxRedemption(String cNMMaxRedemption) {
            this.cNMMaxRedemption = cNMMaxRedemption;
        }

        public String getCNMMaxRedemptionOnTotalAmount() {
            return cNMMaxRedemptionOnTotalAmount;
        }

        public void setCNMMaxRedemptionOnTotalAmount(String cNMMaxRedemptionOnTotalAmount) {
            this.cNMMaxRedemptionOnTotalAmount = cNMMaxRedemptionOnTotalAmount;
        }

    }

}
