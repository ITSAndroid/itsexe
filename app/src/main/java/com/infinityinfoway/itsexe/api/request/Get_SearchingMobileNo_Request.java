package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_SearchingMobileNo_Request {
    @SerializedName("Phone1")
    @Expose
    private String phone1;
    @SerializedName("BookedBy_CM_CompanyID")
    @Expose
    private Integer bookedByCMCompanyID;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public Integer getBookedByCMCompanyID() {
        return bookedByCMCompanyID;
    }

    public void setBookedByCMCompanyID(Integer bookedByCMCompanyID) {
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Get_SearchingMobileNo_Request(String phone1, Integer bookedByCMCompanyID, Integer branchID) {
        this.phone1 = phone1;
        this.bookedByCMCompanyID = bookedByCMCompanyID;
        this.branchID = branchID;
    }
}
