package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainRouteWisePassengerDetails_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_Address")
        @Expose
        private String cMAddress;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("PM_PickupMan")
        @Expose
        private String pMPickupMan;
        @SerializedName("PickupManName")
        @Expose
        private String pickupManName;
        @SerializedName("JM_EmailID")
        @Expose
        private String jMEmailID;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMAddress() {
            return cMAddress;
        }

        public void setCMAddress(String cMAddress) {
            this.cMAddress = cMAddress;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getPMPickupMan() {
            return pMPickupMan;
        }

        public void setPMPickupMan(String pMPickupMan) {
            this.pMPickupMan = pMPickupMan;
        }

        public String getPickupManName() {
            return pickupManName;
        }

        public void setPickupManName(String pickupManName) {
            this.pickupManName = pickupManName;
        }

        public String getJMEmailID() {
            return jMEmailID;
        }

        public void setJMEmailID(String jMEmailID) {
            this.jMEmailID = jMEmailID;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
