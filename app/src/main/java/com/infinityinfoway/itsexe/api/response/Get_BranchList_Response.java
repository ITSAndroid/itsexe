package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_BranchList_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("BranchDetails")
        @Expose
        private List<BranchDetail> branchDetails = null;

        public List<BranchDetail> getBranchDetails() {
            return branchDetails;
        }

        public void setBranchDetails(List<BranchDetail> branchDetails) {
            this.branchDetails = branchDetails;
        }

    }

    public class BranchDetail {

        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

    }
}
