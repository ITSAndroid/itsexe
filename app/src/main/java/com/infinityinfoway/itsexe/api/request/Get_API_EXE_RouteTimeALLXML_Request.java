package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_API_EXE_RouteTimeALLXML_Request {
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BUM_BranchUserID")
    @Expose
    private Integer bUMBranchUserID;
    @SerializedName("CM_CityID")
    @Expose
    private Integer cMCityID;
    @SerializedName("PCRM_AppVersion")
    @Expose
    private String pCRMAppVersion;
    @SerializedName("TBRM_ID")
    @Expose
    private String tBRMID;
    @SerializedName("PCRM_CurrentAssembly")
    @Expose
    private String pCRMCurrentAssembly;
    @SerializedName("IsLoginFromOtherBranch")
    @Expose
    private Integer isLoginFromOtherBranch;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBUMBranchUserID() {
        return bUMBranchUserID;
    }

    public void setBUMBranchUserID(Integer bUMBranchUserID) {
        this.bUMBranchUserID = bUMBranchUserID;
    }

    public Integer getCMCityID() {
        return cMCityID;
    }

    public void setCMCityID(Integer cMCityID) {
        this.cMCityID = cMCityID;
    }

    public String getPCRMAppVersion() {
        return pCRMAppVersion;
    }

    public void setPCRMAppVersion(String pCRMAppVersion) {
        this.pCRMAppVersion = pCRMAppVersion;
    }

    public String getTBRMID() {
        return tBRMID;
    }

    public void setTBRMID(String tBRMID) {
        this.tBRMID = tBRMID;
    }

    public String getPCRMCurrentAssembly() {
        return pCRMCurrentAssembly;
    }

    public void setPCRMCurrentAssembly(String pCRMCurrentAssembly) {
        this.pCRMCurrentAssembly = pCRMCurrentAssembly;
    }

    public Integer getIsLoginFromOtherBranch() {
        return isLoginFromOtherBranch;
    }

    public void setIsLoginFromOtherBranch(Integer isLoginFromOtherBranch) {
        this.isLoginFromOtherBranch = isLoginFromOtherBranch;
    }

    public Get_API_EXE_RouteTimeALLXML_Request(Integer cMCompanyID, Integer bMBranchID, Integer bUMBranchUserID, Integer cMCityID, String pCRMAppVersion, String tBRMID, String pCRMCurrentAssembly, Integer isLoginFromOtherBranch) {
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
        this.bUMBranchUserID = bUMBranchUserID;
        this.cMCityID = cMCityID;
        this.pCRMAppVersion = pCRMAppVersion;
        this.tBRMID = tBRMID;
        this.pCRMCurrentAssembly = pCRMCurrentAssembly;
        this.isLoginFromOtherBranch = isLoginFromOtherBranch;
    }
}
