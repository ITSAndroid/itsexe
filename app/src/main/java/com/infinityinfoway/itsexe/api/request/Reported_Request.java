package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reported_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("JM_PNRNO")
    @Expose
    private String jmPnrno;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("JM_Seats")
    @Expose
    private String jMSeats;
    @SerializedName("JM_PCRegistrationID")
    @Expose
    private Integer jMPCRegistrationID;
    @SerializedName("JM_ITSAdminID")
    @Expose
    private Integer jMITSAdminID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public String getJmPnrno() {
        return jmPnrno;
    }

    public void setJmPnrno(String jmPnrno) {
        this.jmPnrno = jmPnrno;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public String getJMSeats() {
        return jMSeats;
    }

    public void setJMSeats(String jMSeats) {
        this.jMSeats = jMSeats;
    }

    public Integer getJMPCRegistrationID() {
        return jMPCRegistrationID;
    }

    public void setJMPCRegistrationID(Integer jMPCRegistrationID) {
        this.jMPCRegistrationID = jMPCRegistrationID;
    }

    public Integer getJMITSAdminID() {
        return jMITSAdminID;
    }

    public void setJMITSAdminID(Integer jMITSAdminID) {
        this.jMITSAdminID = jMITSAdminID;
    }

    public Reported_Request(Integer cMCompanyID, Integer jMBookedByCMCompanyID, Integer bMBranchID, Integer uMUserID, String jmPnrno, String jMJourneyStartDate, Integer rMRouteID, Integer rTRouteTimeID, String jMSeats, Integer jMPCRegistrationID, Integer jMITSAdminID) {
        this.cMCompanyID = cMCompanyID;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchID = bMBranchID;
        this.uMUserID = uMUserID;
        this.jmPnrno = jmPnrno;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.jMSeats = jMSeats;
        this.jMPCRegistrationID = jMPCRegistrationID;
        this.jMITSAdminID = jMITSAdminID;
    }
}
