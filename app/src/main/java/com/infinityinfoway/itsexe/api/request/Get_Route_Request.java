package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Route_Request {
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BUM_UserID")
    @Expose
    private Integer bUMUserID;
    @SerializedName("FromCityID")
    @Expose
    private Integer fromCityID;
    @SerializedName("ToCityID")
    @Expose
    private Integer toCityID;
    @SerializedName("JourneyDate")
    @Expose
    private String journeyDate;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBUMUserID() {
        return bUMUserID;
    }

    public void setBUMUserID(Integer bUMUserID) {
        this.bUMUserID = bUMUserID;
    }

    public Integer getFromCityID() {
        return fromCityID;
    }

    public void setFromCityID(Integer fromCityID) {
        this.fromCityID = fromCityID;
    }

    public Integer getToCityID() {
        return toCityID;
    }

    public void setToCityID(Integer toCityID) {
        this.toCityID = toCityID;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public Get_Route_Request(Integer companyID, Integer bMBranchID, Integer bUMUserID, Integer fromCityID, Integer toCityID, String journeyDate) {
        this.companyID = companyID;
        this.bMBranchID = bMBranchID;
        this.bUMUserID = bUMUserID;
        this.fromCityID = fromCityID;
        this.toCityID = toCityID;
        this.journeyDate = journeyDate;
    }
}
