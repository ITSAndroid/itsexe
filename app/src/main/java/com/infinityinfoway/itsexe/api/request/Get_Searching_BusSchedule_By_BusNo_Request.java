package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Searching_BusSchedule_By_BusNo_Request {
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;
    @SerializedName("BusNo")
    @Expose
    private String busNo;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public Get_Searching_BusSchedule_By_BusNo_Request(Integer companyID, String journeyStartDate, String busNo) {
        this.companyID = companyID;
        this.journeyStartDate = journeyStartDate;
        this.busNo = busNo;
    }

}
