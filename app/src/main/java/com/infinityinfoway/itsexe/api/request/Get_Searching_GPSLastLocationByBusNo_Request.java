package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Searching_GPSLastLocationByBusNo_Request {
    @SerializedName("BusNo")
    @Expose
    private String busNo;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Get_Searching_GPSLastLocationByBusNo_Request(String busNo, Integer companyID) {
        this.busNo = busNo;
        this.companyID = companyID;
    }
}
