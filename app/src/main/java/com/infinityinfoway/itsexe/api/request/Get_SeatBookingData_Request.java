package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_SeatBookingData_Request {

    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("RouteID")
    @Expose
    private Integer routeID;
    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Integer getRouteID() {
        return routeID;
    }

    public void setRouteID(Integer routeID) {
        this.routeID = routeID;
    }

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public Get_SeatBookingData_Request(Integer companyID, Integer routeID, String journeyStartDate) {
        this.companyID = companyID;
        this.routeID = routeID;
        this.journeyStartDate = journeyStartDate;
    }
}
