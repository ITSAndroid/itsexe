package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ESendGSTInvoice_Request {

    @SerializedName("JM_PNRNO")
    @Expose
    private String jmPnrno;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("CustomerEmail")
    @Expose
    private String customerEmail;

    public String getJmPnrno() {
        return jmPnrno;
    }

    public void setJmPnrno(String jmPnrno) {
        this.jmPnrno = jmPnrno;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public ESendGSTInvoice_Request(String jmPnrno, Integer cMCompanyID, String companyName, String customerEmail) {
        this.jmPnrno = jmPnrno;
        this.cMCompanyID = cMCompanyID;
        this.companyName = companyName;
        this.customerEmail = customerEmail;
    }
}
