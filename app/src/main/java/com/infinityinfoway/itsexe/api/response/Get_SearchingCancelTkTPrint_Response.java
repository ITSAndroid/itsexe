package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_SearchingCancelTkTPrint_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("SearchingCancelTkTPrint")
        @Expose
        private List<SearchingCancelTkTPrint> searchingCancelTkTPrint = null;

        public List<SearchingCancelTkTPrint> getSearchingCancelTkTPrint() {
            return searchingCancelTkTPrint;
        }

        public void setSearchingCancelTkTPrint(List<SearchingCancelTkTPrint> searchingCancelTkTPrint) {
            this.searchingCancelTkTPrint = searchingCancelTkTPrint;
        }

    }

    public class SearchingCancelTkTPrint {

        @SerializedName("PNRNO")
        @Expose
        private Integer pNRNO;
        @SerializedName("TicketNo")
        @Expose
        private String ticketNo;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("BookingDate")
        @Expose
        private String bookingDate;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("TripName")
        @Expose
        private String tripName;
        @SerializedName("PassangerName")
        @Expose
        private String passangerName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("SleeperNo")
        @Expose
        private String sleeperNo;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("TotalSleeper")
        @Expose
        private Integer totalSleeper;
        @SerializedName("TotalSeatAmount")
        @Expose
        private Integer totalSeatAmount;
        @SerializedName("TotalSleeperAmount")
        @Expose
        private String totalSleeperAmount;
        @SerializedName("TotalAmount")
        @Expose
        private Integer totalAmount;
        @SerializedName("ReportingTime")
        @Expose
        private String reportingTime;
        @SerializedName("CoachNo")
        @Expose
        private String coachNo;
        @SerializedName("DepartureTime")
        @Expose
        private String departureTime;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("UserCode")
        @Expose
        private String userCode;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("DayName")
        @Expose
        private String dayName;
        @SerializedName("MainTime")
        @Expose
        private String mainTime;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("AgentName")
        @Expose
        private String agentName;
        @SerializedName("CancelDateTime")
        @Expose
        private String cancelDateTime;
        @SerializedName("RefundCharges")
        @Expose
        private String refundCharges;
        @SerializedName("RefundPer")
        @Expose
        private String refundPer;
        @SerializedName("BaseFare")
        @Expose
        private Double baseFare;
        @SerializedName("JM_ServiceTax")
        @Expose
        private Double jMServiceTax;
        @SerializedName("CancellationServiceTax")
        @Expose
        private Double cancellationServiceTax;
        @SerializedName("OriginalRefundCharge")
        @Expose
        private String originalRefundCharge;
        @SerializedName("TotalCancellationCharges")
        @Expose
        private Double totalCancellationCharges;
        @SerializedName("TotalRefundAmount")
        @Expose
        private String totalRefundAmount;
        @SerializedName("SC_GSTNo")
        @Expose
        private String sCGSTNo;
        @SerializedName("OperatorGSTState")
        @Expose
        private String operatorGSTState;
        @SerializedName("PassengerGSTState")
        @Expose
        private String passengerGSTState;
        @SerializedName("JM_GSTCompanyName")
        @Expose
        private String jMGSTCompanyName;
        @SerializedName("JM_GSTRegNo")
        @Expose
        private String jMGSTRegNo;
        @SerializedName("CM_CancellationIGST")
        @Expose
        private String cMCancellationIGST;
        @SerializedName("CM_CancellationCGST")
        @Expose
        private String cMCancellationCGST;
        @SerializedName("CM_CancellationSGST")
        @Expose
        private String cMCancellationSGST;
        @SerializedName("JM_CancelIGST")
        @Expose
        private Double jMCancelIGST;
        @SerializedName("JM_CancelCGST")
        @Expose
        private Double jMCancelCGST;
        @SerializedName("JM_CancelSGST")
        @Expose
        private Double jMCancelSGST;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("RT_RouteName")
        @Expose
        private String rTRouteName;
        @SerializedName("BookedBy")
        @Expose
        private String bookedBy;
        @SerializedName("BookedTicketNo")
        @Expose
        private String bookedTicketNo;
        @SerializedName("BaseAmt")
        @Expose
        private Double baseAmt;
        @SerializedName("JM_Discount")
        @Expose
        private Double jMDiscount;
        @SerializedName("CM_BookingIGST")
        @Expose
        private String cMBookingIGST;
        @SerializedName("CM_BookingCGST")
        @Expose
        private String cMBookingCGST;
        @SerializedName("CM_BookingSGST")
        @Expose
        private String cMBookingSGST;
        @SerializedName("IGST")
        @Expose
        private Double iGST;
        @SerializedName("CGST")
        @Expose
        private Double cGST;
        @SerializedName("SGST")
        @Expose
        private Double sGST;
        @SerializedName("Status1")
        @Expose
        private Integer status1;

        public Integer getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(Integer pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getTicketNo() {
            return ticketNo;
        }

        public void setTicketNo(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getTripName() {
            return tripName;
        }

        public void setTripName(String tripName) {
            this.tripName = tripName;
        }

        public String getPassangerName() {
            return passangerName;
        }

        public void setPassangerName(String passangerName) {
            this.passangerName = passangerName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getSleeperNo() {
            return sleeperNo;
        }

        public void setSleeperNo(String sleeperNo) {
            this.sleeperNo = sleeperNo;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Integer getTotalSleeper() {
            return totalSleeper;
        }

        public void setTotalSleeper(Integer totalSleeper) {
            this.totalSleeper = totalSleeper;
        }

        public Integer getTotalSeatAmount() {
            return totalSeatAmount;
        }

        public void setTotalSeatAmount(Integer totalSeatAmount) {
            this.totalSeatAmount = totalSeatAmount;
        }

        public String getTotalSleeperAmount() {
            return totalSleeperAmount;
        }

        public void setTotalSleeperAmount(String totalSleeperAmount) {
            this.totalSleeperAmount = totalSleeperAmount;
        }

        public Integer getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Integer totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getReportingTime() {
            return reportingTime;
        }

        public void setReportingTime(String reportingTime) {
            this.reportingTime = reportingTime;
        }

        public String getCoachNo() {
            return coachNo;
        }

        public void setCoachNo(String coachNo) {
            this.coachNo = coachNo;
        }

        public String getDepartureTime() {
            return departureTime;
        }

        public void setDepartureTime(String departureTime) {
            this.departureTime = departureTime;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public String getUserCode() {
            return userCode;
        }

        public void setUserCode(String userCode) {
            this.userCode = userCode;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getDayName() {
            return dayName;
        }

        public void setDayName(String dayName) {
            this.dayName = dayName;
        }

        public String getMainTime() {
            return mainTime;
        }

        public void setMainTime(String mainTime) {
            this.mainTime = mainTime;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getAgentName() {
            return agentName;
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public String getCancelDateTime() {
            return cancelDateTime;
        }

        public void setCancelDateTime(String cancelDateTime) {
            this.cancelDateTime = cancelDateTime;
        }

        public String getRefundCharges() {
            return refundCharges;
        }

        public void setRefundCharges(String refundCharges) {
            this.refundCharges = refundCharges;
        }

        public String getRefundPer() {
            return refundPer;
        }

        public void setRefundPer(String refundPer) {
            this.refundPer = refundPer;
        }

        public Double getBaseFare() {
            return baseFare;
        }

        public void setBaseFare(Double baseFare) {
            this.baseFare = baseFare;
        }

        public Double getJMServiceTax() {
            return jMServiceTax;
        }

        public void setJMServiceTax(Double jMServiceTax) {
            this.jMServiceTax = jMServiceTax;
        }

        public Double getCancellationServiceTax() {
            return cancellationServiceTax;
        }

        public void setCancellationServiceTax(Double cancellationServiceTax) {
            this.cancellationServiceTax = cancellationServiceTax;
        }

        public String getOriginalRefundCharge() {
            return originalRefundCharge;
        }

        public void setOriginalRefundCharge(String originalRefundCharge) {
            this.originalRefundCharge = originalRefundCharge;
        }

        public Double getTotalCancellationCharges() {
            return totalCancellationCharges;
        }

        public void setTotalCancellationCharges(Double totalCancellationCharges) {
            this.totalCancellationCharges = totalCancellationCharges;
        }

        public String getTotalRefundAmount() {
            return totalRefundAmount;
        }

        public void setTotalRefundAmount(String totalRefundAmount) {
            this.totalRefundAmount = totalRefundAmount;
        }

        public String getSCGSTNo() {
            return sCGSTNo;
        }

        public void setSCGSTNo(String sCGSTNo) {
            this.sCGSTNo = sCGSTNo;
        }

        public String getOperatorGSTState() {
            return operatorGSTState;
        }

        public void setOperatorGSTState(String operatorGSTState) {
            this.operatorGSTState = operatorGSTState;
        }

        public String getPassengerGSTState() {
            return passengerGSTState;
        }

        public void setPassengerGSTState(String passengerGSTState) {
            this.passengerGSTState = passengerGSTState;
        }

        public String getJMGSTCompanyName() {
            return jMGSTCompanyName;
        }

        public void setJMGSTCompanyName(String jMGSTCompanyName) {
            this.jMGSTCompanyName = jMGSTCompanyName;
        }

        public String getJMGSTRegNo() {
            return jMGSTRegNo;
        }

        public void setJMGSTRegNo(String jMGSTRegNo) {
            this.jMGSTRegNo = jMGSTRegNo;
        }

        public String getCMCancellationIGST() {
            return cMCancellationIGST;
        }

        public void setCMCancellationIGST(String cMCancellationIGST) {
            this.cMCancellationIGST = cMCancellationIGST;
        }

        public String getCMCancellationCGST() {
            return cMCancellationCGST;
        }

        public void setCMCancellationCGST(String cMCancellationCGST) {
            this.cMCancellationCGST = cMCancellationCGST;
        }

        public String getCMCancellationSGST() {
            return cMCancellationSGST;
        }

        public void setCMCancellationSGST(String cMCancellationSGST) {
            this.cMCancellationSGST = cMCancellationSGST;
        }

        public Double getJMCancelIGST() {
            return jMCancelIGST;
        }

        public void setJMCancelIGST(Double jMCancelIGST) {
            this.jMCancelIGST = jMCancelIGST;
        }

        public Double getJMCancelCGST() {
            return jMCancelCGST;
        }

        public void setJMCancelCGST(Double jMCancelCGST) {
            this.jMCancelCGST = jMCancelCGST;
        }

        public Double getJMCancelSGST() {
            return jMCancelSGST;
        }

        public void setJMCancelSGST(Double jMCancelSGST) {
            this.jMCancelSGST = jMCancelSGST;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getRTRouteName() {
            return rTRouteName;
        }

        public void setRTRouteName(String rTRouteName) {
            this.rTRouteName = rTRouteName;
        }

        public String getBookedBy() {
            return bookedBy;
        }

        public void setBookedBy(String bookedBy) {
            this.bookedBy = bookedBy;
        }

        public String getBookedTicketNo() {
            return bookedTicketNo;
        }

        public void setBookedTicketNo(String bookedTicketNo) {
            this.bookedTicketNo = bookedTicketNo;
        }

        public Double getBaseAmt() {
            return baseAmt;
        }

        public void setBaseAmt(Double baseAmt) {
            this.baseAmt = baseAmt;
        }

        public Double getJMDiscount() {
            return jMDiscount;
        }

        public void setJMDiscount(Double jMDiscount) {
            this.jMDiscount = jMDiscount;
        }

        public String getCMBookingIGST() {
            return cMBookingIGST;
        }

        public void setCMBookingIGST(String cMBookingIGST) {
            this.cMBookingIGST = cMBookingIGST;
        }

        public String getCMBookingCGST() {
            return cMBookingCGST;
        }

        public void setCMBookingCGST(String cMBookingCGST) {
            this.cMBookingCGST = cMBookingCGST;
        }

        public String getCMBookingSGST() {
            return cMBookingSGST;
        }

        public void setCMBookingSGST(String cMBookingSGST) {
            this.cMBookingSGST = cMBookingSGST;
        }

        public Double getIGST() {
            return iGST;
        }

        public void setIGST(Double iGST) {
            this.iGST = iGST;
        }

        public Double getCGST() {
            return cGST;
        }

        public void setCGST(Double cGST) {
            this.cGST = cGST;
        }

        public Double getSGST() {
            return sGST;
        }

        public void setSGST(Double sGST) {
            this.sGST = sGST;
        }

        public Integer getStatus1() {
            return status1;
        }

        public void setStatus1(Integer status1) {
            this.status1 = status1;
        }

    }

}
