package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReservationDetailReportWithAmount_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("PM_PickupName")
        @Expose
        private String pMPickupName;
        @SerializedName("JM_PNRNO")
        @Expose
        private String jMPNRNO;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("PM_DropName")
        @Expose
        private String pMDropName;
        @SerializedName("BookedBy")
        @Expose
        private String bookedBy;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private String jMTotalPassengers;
        @SerializedName("ServiceType")
        @Expose
        private String serviceType;
        @SerializedName("ReprotTitle")
        @Expose
        private String reprotTitle;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("PrintDate")
        @Expose
        private String printDate;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("BusType")
        @Expose
        private String busType;
        @SerializedName("Driver1")
        @Expose
        private String driver1;
        @SerializedName("Driver2")
        @Expose
        private String driver2;
        @SerializedName("TotalSeats")
        @Expose
        private String totalSeats;
        @SerializedName("BookedSeats")
        @Expose
        private String bookedSeats;
        @SerializedName("EmptySeats")
        @Expose
        private String emptySeats;
        @SerializedName("RouteOrigin")
        @Expose
        private String routeOrigin;
        @SerializedName("RouteDestination")
        @Expose
        private String routeDestination;
        @SerializedName("RouteDepartureTime")
        @Expose
        private String routeDepartureTime;
        @SerializedName("FinalJourneyTime")
        @Expose
        private String finalJourneyTime;
        @SerializedName("BlockedSeats")
        @Expose
        private String blockedSeats;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("JM_Remarks")
        @Expose
        private String jMRemarks;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("AgentQuota")
        @Expose
        private String agentQuota;

        public String getPMPickupName() {
            return pMPickupName;
        }

        public void setPMPickupName(String pMPickupName) {
            this.pMPickupName = pMPickupName;
        }

        public String getJMPNRNO() {
            return jMPNRNO;
        }

        public void setJMPNRNO(String jMPNRNO) {
            this.jMPNRNO = jMPNRNO;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getPMDropName() {
            return pMDropName;
        }

        public void setPMDropName(String pMDropName) {
            this.pMDropName = pMDropName;
        }

        public String getBookedBy() {
            return bookedBy;
        }

        public void setBookedBy(String bookedBy) {
            this.bookedBy = bookedBy;
        }

        public String getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(String jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getReprotTitle() {
            return reprotTitle;
        }

        public void setReprotTitle(String reprotTitle) {
            this.reprotTitle = reprotTitle;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getPrintDate() {
            return printDate;
        }

        public void setPrintDate(String printDate) {
            this.printDate = printDate;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getBusType() {
            return busType;
        }

        public void setBusType(String busType) {
            this.busType = busType;
        }

        public String getDriver1() {
            return driver1;
        }

        public void setDriver1(String driver1) {
            this.driver1 = driver1;
        }

        public String getDriver2() {
            return driver2;
        }

        public void setDriver2(String driver2) {
            this.driver2 = driver2;
        }

        public String getTotalSeats() {
            return totalSeats;
        }

        public void setTotalSeats(String totalSeats) {
            this.totalSeats = totalSeats;
        }

        public String getBookedSeats() {
            return bookedSeats;
        }

        public void setBookedSeats(String bookedSeats) {
            this.bookedSeats = bookedSeats;
        }

        public String getEmptySeats() {
            return emptySeats;
        }

        public void setEmptySeats(String emptySeats) {
            this.emptySeats = emptySeats;
        }

        public String getRouteOrigin() {
            return routeOrigin;
        }

        public void setRouteOrigin(String routeOrigin) {
            this.routeOrigin = routeOrigin;
        }

        public String getRouteDestination() {
            return routeDestination;
        }

        public void setRouteDestination(String routeDestination) {
            this.routeDestination = routeDestination;
        }

        public String getRouteDepartureTime() {
            return routeDepartureTime;
        }

        public void setRouteDepartureTime(String routeDepartureTime) {
            this.routeDepartureTime = routeDepartureTime;
        }

        public String getFinalJourneyTime() {
            return finalJourneyTime;
        }

        public void setFinalJourneyTime(String finalJourneyTime) {
            this.finalJourneyTime = finalJourneyTime;
        }

        public String getBlockedSeats() {
            return blockedSeats;
        }

        public void setBlockedSeats(String blockedSeats) {
            this.blockedSeats = blockedSeats;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public String getJMRemarks() {
            return jMRemarks;
        }

        public void setJMRemarks(String jMRemarks) {
            this.jMRemarks = jMRemarks;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public String getAgentQuota() {
            return agentQuota;
        }

        public void setAgentQuota(String agentQuota) {
            this.agentQuota = agentQuota;
        }

    }

}
