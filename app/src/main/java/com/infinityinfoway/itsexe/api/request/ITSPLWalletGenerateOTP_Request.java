package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ITSPLWalletGenerateOTP_Request {

    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;
    @SerializedName("BranchUserID")
    @Expose
    private Integer branchUserID;
    @SerializedName("WalletType")
    @Expose
    private Integer walletType;
    @SerializedName("Walletkey")
    @Expose
    private String walletkey;
    @SerializedName("PaxName")
    @Expose
    private String paxName;
    @SerializedName("PaxMobileNo")
    @Expose
    private String paxMobileNo;
    @SerializedName("TotalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("PromoCode")
    @Expose
    private String promoCode;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("JourneyDate")
    @Expose
    private String journeyDate;
    @SerializedName("FromCityID")
    @Expose
    private Integer fromCityID;
    @SerializedName("ToCityID")
    @Expose
    private Integer toCityID;
    @SerializedName("FromCityName")
    @Expose
    private String fromCityName;
    @SerializedName("ToCityName")
    @Expose
    private String toCityName;
    @SerializedName("RouteCompanyID")
    @Expose
    private Integer routeCompanyID;
    @SerializedName("ArrangementID")
    @Expose
    private Integer arrangementID;
    @SerializedName("SeatList")
    @Expose
    private String seatList;
    @SerializedName("RouteID")
    @Expose
    private Integer routeID;
    @SerializedName("RouteTimeID")
    @Expose
    private Integer routeTimeID;
    @SerializedName("CityTime")
    @Expose
    private String cityTime;
    @SerializedName("RouteTime")
    @Expose
    private String routeTime;
    @SerializedName("PickupID")
    @Expose
    private Integer pickupID;
    @SerializedName("PickupName")
    @Expose
    private String pickupName;
    @SerializedName("PickupTime")
    @Expose
    private String pickupTime;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Integer getBranchUserID() {
        return branchUserID;
    }

    public void setBranchUserID(Integer branchUserID) {
        this.branchUserID = branchUserID;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getWalletkey() {
        return walletkey;
    }

    public void setWalletkey(String walletkey) {
        this.walletkey = walletkey;
    }

    public String getPaxName() {
        return paxName;
    }

    public void setPaxName(String paxName) {
        this.paxName = paxName;
    }

    public String getPaxMobileNo() {
        return paxMobileNo;
    }

    public void setPaxMobileNo(String paxMobileNo) {
        this.paxMobileNo = paxMobileNo;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public Integer getFromCityID() {
        return fromCityID;
    }

    public void setFromCityID(Integer fromCityID) {
        this.fromCityID = fromCityID;
    }

    public Integer getToCityID() {
        return toCityID;
    }

    public void setToCityID(Integer toCityID) {
        this.toCityID = toCityID;
    }

    public String getFromCityName() {
        return fromCityName;
    }

    public void setFromCityName(String fromCityName) {
        this.fromCityName = fromCityName;
    }

    public String getToCityName() {
        return toCityName;
    }

    public void setToCityName(String toCityName) {
        this.toCityName = toCityName;
    }

    public Integer getRouteCompanyID() {
        return routeCompanyID;
    }

    public void setRouteCompanyID(Integer routeCompanyID) {
        this.routeCompanyID = routeCompanyID;
    }

    public Integer getArrangementID() {
        return arrangementID;
    }

    public void setArrangementID(Integer arrangementID) {
        this.arrangementID = arrangementID;
    }

    public String getSeatList() {
        return seatList;
    }

    public void setSeatList(String seatList) {
        this.seatList = seatList;
    }

    public Integer getRouteID() {
        return routeID;
    }

    public void setRouteID(Integer routeID) {
        this.routeID = routeID;
    }

    public Integer getRouteTimeID() {
        return routeTimeID;
    }

    public void setRouteTimeID(Integer routeTimeID) {
        this.routeTimeID = routeTimeID;
    }

    public String getCityTime() {
        return cityTime;
    }

    public void setCityTime(String cityTime) {
        this.cityTime = cityTime;
    }

    public String getRouteTime() {
        return routeTime;
    }

    public void setRouteTime(String routeTime) {
        this.routeTime = routeTime;
    }

    public Integer getPickupID() {
        return pickupID;
    }

    public void setPickupID(Integer pickupID) {
        this.pickupID = pickupID;
    }

    public String getPickupName() {
        return pickupName;
    }

    public void setPickupName(String pickupName) {
        this.pickupName = pickupName;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public ITSPLWalletGenerateOTP_Request(Integer companyID, Integer branchID, Integer branchUserID, Integer walletType, String walletkey, String paxName, String paxMobileNo, Double totalAmount, String promoCode, String comment, String emailID, String journeyDate, Integer fromCityID, Integer toCityID, String fromCityName, String toCityName, Integer routeCompanyID, Integer arrangementID, String seatList, Integer routeID, Integer routeTimeID, String cityTime, String routeTime, Integer pickupID, String pickupName, String pickupTime) {
        this.companyID = companyID;
        this.branchID = branchID;
        this.branchUserID = branchUserID;
        this.walletType = walletType;
        this.walletkey = walletkey;
        this.paxName = paxName;
        this.paxMobileNo = paxMobileNo;
        this.totalAmount = totalAmount;
        this.promoCode = promoCode;
        this.comment = comment;
        this.emailID = emailID;
        this.journeyDate = journeyDate;
        this.fromCityID = fromCityID;
        this.toCityID = toCityID;
        this.fromCityName = fromCityName;
        this.toCityName = toCityName;
        this.routeCompanyID = routeCompanyID;
        this.arrangementID = arrangementID;
        this.seatList = seatList;
        this.routeID = routeID;
        this.routeTimeID = routeTimeID;
        this.cityTime = cityTime;
        this.routeTime = routeTime;
        this.pickupID = pickupID;
        this.pickupName = pickupName;
        this.pickupTime = pickupTime;
    }
}
