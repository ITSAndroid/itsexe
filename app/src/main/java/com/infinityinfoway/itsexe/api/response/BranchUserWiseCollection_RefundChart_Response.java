package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BranchUserWiseCollection_RefundChart_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("BranchUserWiseCollection")
        @Expose
        private List<BranchUserWiseCollection> branchUserWiseCollection = null;
        @SerializedName("BranchUserWiseAgentRecharge")
        @Expose
        private List<BranchUserWiseAgentRecharge> branchUserWiseAgentRecharge = null;
        @SerializedName("BranchUserWiseRefund")
        @Expose
        private List<BranchUserWiseRefund> branchUserWiseRefund = null;
        @SerializedName("BranchUserWiseCollectionRefundPending")
        @Expose
        private List<BranchUserWiseCollectionRefundPending> branchUserWiseCollectionRefundPending = null;

        public List<BranchUserWiseCollection> getBranchUserWiseCollection() {
            return branchUserWiseCollection;
        }

        public void setBranchUserWiseCollection(List<BranchUserWiseCollection> branchUserWiseCollection) {
            this.branchUserWiseCollection = branchUserWiseCollection;
        }

        public List<BranchUserWiseAgentRecharge> getBranchUserWiseAgentRecharge() {
            return branchUserWiseAgentRecharge;
        }

        public void setBranchUserWiseAgentRecharge(List<BranchUserWiseAgentRecharge> branchUserWiseAgentRecharge) {
            this.branchUserWiseAgentRecharge = branchUserWiseAgentRecharge;
        }

        public List<BranchUserWiseRefund> getBranchUserWiseRefund() {
            return branchUserWiseRefund;
        }

        public void setBranchUserWiseRefund(List<BranchUserWiseRefund> branchUserWiseRefund) {
            this.branchUserWiseRefund = branchUserWiseRefund;
        }


        public List<BranchUserWiseCollectionRefundPending> getBranchUserWiseCollectionRefundPending() {
            return branchUserWiseCollectionRefundPending;
        }

        public void setBranchUserWiseCollectionRefundPending(List<BranchUserWiseCollectionRefundPending> branchUserWiseCollectionRefundPending) {
            this.branchUserWiseCollectionRefundPending = branchUserWiseCollectionRefundPending;
        }

    }

    public class BranchUserWiseAgentRecharge {

        @SerializedName("BUM_UserName")
        @Expose
        private String bUMUserName;
        @SerializedName("CAM_CompanyAgentID")
        @Expose
        private Integer cAMCompanyAgentID;
        @SerializedName("CAM_SetAgentName")
        @Expose
        private String cAMSetAgentName;
        @SerializedName("CARM_Recharge")
        @Expose
        private Double cARMRecharge;
        @SerializedName("CARM_PaymentMode")
        @Expose
        private String cARMPaymentMode;
        @SerializedName("CARM_Bank")
        @Expose
        private String cARMBank;
        @SerializedName("CRAM_ChequeDraftCard")
        @Expose
        private String cRAMChequeDraftCard;
        @SerializedName("TransDate")
        @Expose
        private String transDate;
        @SerializedName("CRAM_Remarks")
        @Expose
        private String cRAMRemarks;

        public String getBUMUserName() {
            return bUMUserName;
        }

        public void setBUMUserName(String bUMUserName) {
            this.bUMUserName = bUMUserName;
        }

        public Integer getCAMCompanyAgentID() {
            return cAMCompanyAgentID;
        }

        public void setCAMCompanyAgentID(Integer cAMCompanyAgentID) {
            this.cAMCompanyAgentID = cAMCompanyAgentID;
        }

        public String getCAMSetAgentName() {
            return cAMSetAgentName;
        }

        public void setCAMSetAgentName(String cAMSetAgentName) {
            this.cAMSetAgentName = cAMSetAgentName;
        }

        public Double getCARMRecharge() {
            return cARMRecharge;
        }

        public void setCARMRecharge(Double cARMRecharge) {
            this.cARMRecharge = cARMRecharge;
        }

        public String getCARMPaymentMode() {
            return cARMPaymentMode;
        }

        public void setCARMPaymentMode(String cARMPaymentMode) {
            this.cARMPaymentMode = cARMPaymentMode;
        }

        public String getCARMBank() {
            return cARMBank;
        }

        public void setCARMBank(String cARMBank) {
            this.cARMBank = cARMBank;
        }

        public String getCRAMChequeDraftCard() {
            return cRAMChequeDraftCard;
        }

        public void setCRAMChequeDraftCard(String cRAMChequeDraftCard) {
            this.cRAMChequeDraftCard = cRAMChequeDraftCard;
        }

        public String getTransDate() {
            return transDate;
        }

        public void setTransDate(String transDate) {
            this.transDate = transDate;
        }

        public String getCRAMRemarks() {
            return cRAMRemarks;
        }

        public void setCRAMRemarks(String cRAMRemarks) {
            this.cRAMRemarks = cRAMRemarks;
        }

    }

    public class BranchUserWiseCollection {

        @SerializedName("BUM_UserName")
        @Expose
        private String bUMUserName;
        @SerializedName("PNR_Ticket_NO")
        @Expose
        private String pNRTicketNO;
        @SerializedName("Symbol")
        @Expose
        private String symbol;
        @SerializedName("J_Date")
        @Expose
        private String jDate;
        @SerializedName("JM_JourneyStartTime")
        @Expose
        private String jMJourneyStartTime;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private String jMTotalPassengers;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("BranchNameCode")
        @Expose
        private String branchNameCode;
        @SerializedName("BM_Address")
        @Expose
        private String bMAddress;
        @SerializedName("ReportDate")
        @Expose
        private String reportDate;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("DEM_Remarks")
        @Expose
        private String dEMRemarks;
        @SerializedName("JM_TicketNo")
        @Expose
        private Integer jMTicketNo;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getBUMUserName() {
            return bUMUserName;
        }

        public void setBUMUserName(String bUMUserName) {
            this.bUMUserName = bUMUserName;
        }

        public String getPNRTicketNO() {
            return pNRTicketNO;
        }

        public void setPNRTicketNO(String pNRTicketNO) {
            this.pNRTicketNO = pNRTicketNO;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getJMJourneyStartTime() {
            return jMJourneyStartTime;
        }

        public void setJMJourneyStartTime(String jMJourneyStartTime) {
            this.jMJourneyStartTime = jMJourneyStartTime;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(String jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public String getBranchNameCode() {
            return branchNameCode;
        }

        public void setBranchNameCode(String branchNameCode) {
            this.branchNameCode = branchNameCode;
        }

        public String getBMAddress() {
            return bMAddress;
        }

        public void setBMAddress(String bMAddress) {
            this.bMAddress = bMAddress;
        }

        public String getReportDate() {
            return reportDate;
        }

        public void setReportDate(String reportDate) {
            this.reportDate = reportDate;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getDEMRemarks() {
            return dEMRemarks;
        }

        public void setDEMRemarks(String dEMRemarks) {
            this.dEMRemarks = dEMRemarks;
        }

        public Integer getJMTicketNo() {
            return jMTicketNo;
        }

        public void setJMTicketNo(Integer jMTicketNo) {
            this.jMTicketNo = jMTicketNo;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

    public class BranchUserWiseRefund {

        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("BranchCode1")
        @Expose
        private String branchCode1;
        @SerializedName("PNR_TicketNO")
        @Expose
        private String pNRTicketNO;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("JourneyStartTime")
        @Expose
        private String journeyStartTime;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("TotalPassanger")
        @Expose
        private String totalPassanger;
        @SerializedName("Ref_Chg")
        @Expose
        private String refChg;
        @SerializedName("CancelAmount")
        @Expose
        private Double cancelAmount;
        @SerializedName("BranchCode")
        @Expose
        private String branchCode;
        @SerializedName("BranchAddress")
        @Expose
        private String branchAddress;
        @SerializedName("CashCollection")
        @Expose
        private String cashCollection;
        @SerializedName("BankCollection")
        @Expose
        private String bankCollection;
        @SerializedName("RefundCharge")
        @Expose
        private String refundCharge;
        @SerializedName("Total")
        @Expose
        private Double total;
        @SerializedName("Refund")
        @Expose
        private String refund;
        @SerializedName("NetTotal")
        @Expose
        private String netTotal;
        @SerializedName("OnlineCollection")
        @Expose
        private Double onlineCollection;
        @SerializedName("OnlineRefund")
        @Expose
        private Double onlineRefund;
        @SerializedName("OfflineCollection")
        @Expose
        private Double offlineCollection;
        @SerializedName("OfflineCommission")
        @Expose
        private Double offlineCommission;
        @SerializedName("OfflineRefund")
        @Expose
        private Double offlineRefund;
        @SerializedName("ReportDate")
        @Expose
        private String reportDate;
        @SerializedName("BranchBooking")
        @Expose
        private String branchBooking;
        @SerializedName("BranchBookingRefundCharge")
        @Expose
        private String branchBookingRefundCharge;
        @SerializedName("FinalTotal")
        @Expose
        private String finalTotal;
        @SerializedName("TotalAgentRecharge")
        @Expose
        private String totalAgentRecharge;
        @SerializedName("AmenitiesTotalCollection")
        @Expose
        private Double amenitiesTotalCollection;
        @SerializedName("AmenitiesTotalCancel")
        @Expose
        private Double amenitiesTotalCancel;
        @SerializedName("AmountType")
        @Expose
        private String amountType;
        @SerializedName("DEM_Remarks")
        @Expose
        private String dEMRemarks;
        @SerializedName("OtherIncome")
        @Expose
        private String otherIncome;
        @SerializedName("OtherExpense")
        @Expose
        private String otherExpense;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getBranchCode1() {
            return branchCode1;
        }

        public void setBranchCode1(String branchCode1) {
            this.branchCode1 = branchCode1;
        }

        public String getPNRTicketNO() {
            return pNRTicketNO;
        }

        public void setPNRTicketNO(String pNRTicketNO) {
            this.pNRTicketNO = pNRTicketNO;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getJourneyStartTime() {
            return journeyStartTime;
        }

        public void setJourneyStartTime(String journeyStartTime) {
            this.journeyStartTime = journeyStartTime;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getTotalPassanger() {
            return totalPassanger;
        }

        public void setTotalPassanger(String totalPassanger) {
            this.totalPassanger = totalPassanger;
        }

        public String getRefChg() {
            return refChg;
        }

        public void setRefChg(String refChg) {
            this.refChg = refChg;
        }

        public Double getCancelAmount() {
            return cancelAmount;
        }

        public void setCancelAmount(Double cancelAmount) {
            this.cancelAmount = cancelAmount;
        }

        public String getBranchCode() {
            return branchCode;
        }

        public void setBranchCode(String branchCode) {
            this.branchCode = branchCode;
        }

        public String getBranchAddress() {
            return branchAddress;
        }

        public void setBranchAddress(String branchAddress) {
            this.branchAddress = branchAddress;
        }

        public String getCashCollection() {
            return cashCollection;
        }

        public void setCashCollection(String cashCollection) {
            this.cashCollection = cashCollection;
        }

        public String getBankCollection() {
            return bankCollection;
        }

        public void setBankCollection(String bankCollection) {
            this.bankCollection = bankCollection;
        }

        public String getRefundCharge() {
            return refundCharge;
        }

        public void setRefundCharge(String refundCharge) {
            this.refundCharge = refundCharge;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getRefund() {
            return refund;
        }

        public void setRefund(String refund) {
            this.refund = refund;
        }

        public String getNetTotal() {
            return netTotal;
        }

        public void setNetTotal(String netTotal) {
            this.netTotal = netTotal;
        }

        public Double getOnlineCollection() {
            return onlineCollection;
        }

        public void setOnlineCollection(Double onlineCollection) {
            this.onlineCollection = onlineCollection;
        }

        public Double getOnlineRefund() {
            return onlineRefund;
        }

        public void setOnlineRefund(Double onlineRefund) {
            this.onlineRefund = onlineRefund;
        }

        public Double getOfflineCollection() {
            return offlineCollection;
        }

        public void setOfflineCollection(Double offlineCollection) {
            this.offlineCollection = offlineCollection;
        }

        public Double getOfflineCommission() {
            return offlineCommission;
        }

        public void setOfflineCommission(Double offlineCommission) {
            this.offlineCommission = offlineCommission;
        }

        public Double getOfflineRefund() {
            return offlineRefund;
        }

        public void setOfflineRefund(Double offlineRefund) {
            this.offlineRefund = offlineRefund;
        }

        public String getReportDate() {
            return reportDate;
        }

        public void setReportDate(String reportDate) {
            this.reportDate = reportDate;
        }

        public String getBranchBooking() {
            return branchBooking;
        }

        public void setBranchBooking(String branchBooking) {
            this.branchBooking = branchBooking;
        }

        public String getBranchBookingRefundCharge() {
            return branchBookingRefundCharge;
        }

        public void setBranchBookingRefundCharge(String branchBookingRefundCharge) {
            this.branchBookingRefundCharge = branchBookingRefundCharge;
        }

        public String getFinalTotal() {
            return finalTotal;
        }

        public void setFinalTotal(String finalTotal) {
            this.finalTotal = finalTotal;
        }

        public String getTotalAgentRecharge() {
            return totalAgentRecharge;
        }

        public void setTotalAgentRecharge(String totalAgentRecharge) {
            this.totalAgentRecharge = totalAgentRecharge;
        }

        public Double getAmenitiesTotalCollection() {
            return amenitiesTotalCollection;
        }

        public void setAmenitiesTotalCollection(Double amenitiesTotalCollection) {
            this.amenitiesTotalCollection = amenitiesTotalCollection;
        }

        public Double getAmenitiesTotalCancel() {
            return amenitiesTotalCancel;
        }

        public void setAmenitiesTotalCancel(Double amenitiesTotalCancel) {
            this.amenitiesTotalCancel = amenitiesTotalCancel;
        }

        public String getAmountType() {
            return amountType;
        }

        public void setAmountType(String amountType) {
            this.amountType = amountType;
        }

        public String getDEMRemarks() {
            return dEMRemarks;
        }

        public void setDEMRemarks(String dEMRemarks) {
            this.dEMRemarks = dEMRemarks;
        }

        public String getOtherIncome() {
            return otherIncome;
        }

        public void setOtherIncome(String otherIncome) {
            this.otherIncome = otherIncome;
        }

        public String getOtherExpense() {
            return otherExpense;
        }

        public void setOtherExpense(String otherExpense) {
            this.otherExpense = otherExpense;
        }

    }

    public class BranchUserWiseCollectionRefundPending {

        @SerializedName("BM_BranchCode")
        @Expose
        private String bMBranchCode;
        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("JM_PNRNO")
        @Expose
        private String jMPNRNO;
        @SerializedName("JM_TicketNo")
        @Expose
        private Integer jMTicketNo;
        @SerializedName("Symbol")
        @Expose
        private String symbol;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_JourneyCityTime")
        @Expose
        private String jMJourneyCityTime;
        @SerializedName("SubRouteName")
        @Expose
        private String subRouteName;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("Discount")
        @Expose
        private Double discount;
        @SerializedName("RefundCharge")
        @Expose
        private String refundCharge;
        @SerializedName("TotalCashCollection")
        @Expose
        private Double totalCashCollection;
        @SerializedName("TotalBankCollection")
        @Expose
        private Double totalBankCollection;
        @SerializedName("TotalRefundCharge")
        @Expose
        private Double totalRefundCharge;
        @SerializedName("NetCash")
        @Expose
        private Double netCash;
        @SerializedName("TotalRefund")
        @Expose
        private Double totalRefund;
        @SerializedName("PendingAmount")
        @Expose
        private Double pendingAmount;
        @SerializedName("ReceiveAmount")
        @Expose
        private Double receiveAmount;
        @SerializedName("TotalPayment")
        @Expose
        private Double totalPayment;
        @SerializedName("IncomeAmount")
        @Expose
        private Double incomeAmount;
        @SerializedName("ExpenseAmount")
        @Expose
        private Double expenseAmount;
        @SerializedName("NetBalance")
        @Expose
        private Double netBalance;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;
        @SerializedName("BM_Address")
        @Expose
        private String bMAddress;
        @SerializedName("ReportDate")
        @Expose
        private String reportDate;
        @SerializedName("ReportHeader")
        @Expose
        private String reportHeader;
        @SerializedName("GroupBy")
        @Expose
        private String groupBy;
        @SerializedName("ModificationCharges")
        @Expose
        private Double modificationCharges;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getBMBranchCode() {
            return bMBranchCode;
        }

        public void setBMBranchCode(String bMBranchCode) {
            this.bMBranchCode = bMBranchCode;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getJMPNRNO() {
            return jMPNRNO;
        }

        public void setJMPNRNO(String jMPNRNO) {
            this.jMPNRNO = jMPNRNO;
        }

        public Integer getJMTicketNo() {
            return jMTicketNo;
        }

        public void setJMTicketNo(Integer jMTicketNo) {
            this.jMTicketNo = jMTicketNo;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMJourneyCityTime() {
            return jMJourneyCityTime;
        }

        public void setJMJourneyCityTime(String jMJourneyCityTime) {
            this.jMJourneyCityTime = jMJourneyCityTime;
        }

        public String getSubRouteName() {
            return subRouteName;
        }

        public void setSubRouteName(String subRouteName) {
            this.subRouteName = subRouteName;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public String getRefundCharge() {
            return refundCharge;
        }

        public void setRefundCharge(String refundCharge) {
            this.refundCharge = refundCharge;
        }

        public Double getTotalCashCollection() {
            return totalCashCollection;
        }

        public void setTotalCashCollection(Double totalCashCollection) {
            this.totalCashCollection = totalCashCollection;
        }

        public Double getTotalBankCollection() {
            return totalBankCollection;
        }

        public void setTotalBankCollection(Double totalBankCollection) {
            this.totalBankCollection = totalBankCollection;
        }

        public Double getTotalRefundCharge() {
            return totalRefundCharge;
        }

        public void setTotalRefundCharge(Double totalRefundCharge) {
            this.totalRefundCharge = totalRefundCharge;
        }

        public Double getNetCash() {
            return netCash;
        }

        public void setNetCash(Double netCash) {
            this.netCash = netCash;
        }

        public Double getTotalRefund() {
            return totalRefund;
        }

        public void setTotalRefund(Double totalRefund) {
            this.totalRefund = totalRefund;
        }

        public Double getPendingAmount() {
            return pendingAmount;
        }

        public void setPendingAmount(Double pendingAmount) {
            this.pendingAmount = pendingAmount;
        }

        public Double getReceiveAmount() {
            return receiveAmount;
        }

        public void setReceiveAmount(Double receiveAmount) {
            this.receiveAmount = receiveAmount;
        }

        public Double getTotalPayment() {
            return totalPayment;
        }

        public void setTotalPayment(Double totalPayment) {
            this.totalPayment = totalPayment;
        }

        public Double getIncomeAmount() {
            return incomeAmount;
        }

        public void setIncomeAmount(Double incomeAmount) {
            this.incomeAmount = incomeAmount;
        }

        public Double getExpenseAmount() {
            return expenseAmount;
        }

        public void setExpenseAmount(Double expenseAmount) {
            this.expenseAmount = expenseAmount;
        }

        public Double getNetBalance() {
            return netBalance;
        }

        public void setNetBalance(Double netBalance) {
            this.netBalance = netBalance;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

        public String getBMAddress() {
            return bMAddress;
        }

        public void setBMAddress(String bMAddress) {
            this.bMAddress = bMAddress;
        }

        public String getReportDate() {
            return reportDate;
        }

        public void setReportDate(String reportDate) {
            this.reportDate = reportDate;
        }

        public String getReportHeader() {
            return reportHeader;
        }

        public void setReportHeader(String reportHeader) {
            this.reportHeader = reportHeader;
        }

        public String getGroupBy() {
            return groupBy;
        }

        public void setGroupBy(String groupBy) {
            this.groupBy = groupBy;
        }

        public Double getModificationCharges() {
            return modificationCharges;
        }

        public void setModificationCharges(Double modificationCharges) {
            this.modificationCharges = modificationCharges;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }


}
