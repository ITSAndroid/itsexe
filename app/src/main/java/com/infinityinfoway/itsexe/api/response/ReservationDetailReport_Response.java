package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReservationDetailReport_Response {

    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("ReservationDetails")
        @Expose
        private List<ReservationDetail> reservationDetails = null;
        @SerializedName("ReservationDetailsEmpty")
        @Expose
        private List<Object> reservationDetailsEmpty = null;

        public List<ReservationDetail> getReservationDetails() {
            return reservationDetails;
        }

        public void setReservationDetails(List<ReservationDetail> reservationDetails) {
            this.reservationDetails = reservationDetails;
        }

        public List<Object> getReservationDetailsEmpty() {
            return reservationDetailsEmpty;
        }

        public void setReservationDetailsEmpty(List<Object> reservationDetailsEmpty) {
            this.reservationDetailsEmpty = reservationDetailsEmpty;
        }

    }

    public class ReservationDetail {

        @SerializedName("PM_PickupName")
        @Expose
        private String pMPickupName;
        @SerializedName("JM_PNRNO")
        @Expose
        private String jMPNRNO;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("PM_DropName")
        @Expose
        private String pMDropName;
        @SerializedName("BookedBy")
        @Expose
        private String bookedBy;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private String jMTotalPassengers;
        @SerializedName("ServiceType")
        @Expose
        private String serviceType;
        @SerializedName("ReprotTitle")
        @Expose
        private String reprotTitle;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("PrintDate")
        @Expose
        private String printDate;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("BusType")
        @Expose
        private String busType;
        @SerializedName("Driver1")
        @Expose
        private String driver1;
        @SerializedName("Driver2")
        @Expose
        private String driver2;
        @SerializedName("TotalSeats")
        @Expose
        private Integer totalSeats;
        @SerializedName("BookedSeats")
        @Expose
        private Integer bookedSeats;
        @SerializedName("EmptySeats")
        @Expose
        private Integer emptySeats;
        @SerializedName("BoardingInfo")
        @Expose
        private String boardingInfo;
        @SerializedName("DroppingInfo")
        @Expose
        private String droppingInfo;
        @SerializedName("RouteOrigin")
        @Expose
        private String routeOrigin;
        @SerializedName("RouteDestination")
        @Expose
        private String routeDestination;
        @SerializedName("RouteDepartureTime")
        @Expose
        private String routeDepartureTime;
        @SerializedName("FinalJourneyTime")
        @Expose
        private String finalJourneyTime;
        @SerializedName("BlockedSeats")
        @Expose
        private Integer blockedSeats;

        public String getPMPickupName() {
            return pMPickupName;
        }

        public void setPMPickupName(String pMPickupName) {
            this.pMPickupName = pMPickupName;
        }

        public String getJMPNRNO() {
            return jMPNRNO;
        }

        public void setJMPNRNO(String jMPNRNO) {
            this.jMPNRNO = jMPNRNO;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getPMDropName() {
            return pMDropName;
        }

        public void setPMDropName(String pMDropName) {
            this.pMDropName = pMDropName;
        }

        public String getBookedBy() {
            return bookedBy;
        }

        public void setBookedBy(String bookedBy) {
            this.bookedBy = bookedBy;
        }

        public String getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(String jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getReprotTitle() {
            return reprotTitle;
        }

        public void setReprotTitle(String reprotTitle) {
            this.reprotTitle = reprotTitle;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getPrintDate() {
            return printDate;
        }

        public void setPrintDate(String printDate) {
            this.printDate = printDate;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getBusType() {
            return busType;
        }

        public void setBusType(String busType) {
            this.busType = busType;
        }

        public String getDriver1() {
            return driver1;
        }

        public void setDriver1(String driver1) {
            this.driver1 = driver1;
        }

        public String getDriver2() {
            return driver2;
        }

        public void setDriver2(String driver2) {
            this.driver2 = driver2;
        }

        public Integer getTotalSeats() {
            return totalSeats;
        }

        public void setTotalSeats(Integer totalSeats) {
            this.totalSeats = totalSeats;
        }

        public Integer getBookedSeats() {
            return bookedSeats;
        }

        public void setBookedSeats(Integer bookedSeats) {
            this.bookedSeats = bookedSeats;
        }

        public Integer getEmptySeats() {
            return emptySeats;
        }

        public void setEmptySeats(Integer emptySeats) {
            this.emptySeats = emptySeats;
        }

        public String getBoardingInfo() {
            return boardingInfo;
        }

        public void setBoardingInfo(String boardingInfo) {
            this.boardingInfo = boardingInfo;
        }

        public String getDroppingInfo() {
            return droppingInfo;
        }

        public void setDroppingInfo(String droppingInfo) {
            this.droppingInfo = droppingInfo;
        }

        public String getRouteOrigin() {
            return routeOrigin;
        }

        public void setRouteOrigin(String routeOrigin) {
            this.routeOrigin = routeOrigin;
        }

        public String getRouteDestination() {
            return routeDestination;
        }

        public void setRouteDestination(String routeDestination) {
            this.routeDestination = routeDestination;
        }

        public String getRouteDepartureTime() {
            return routeDepartureTime;
        }

        public void setRouteDepartureTime(String routeDepartureTime) {
            this.routeDepartureTime = routeDepartureTime;
        }

        public String getFinalJourneyTime() {
            return finalJourneyTime;
        }

        public void setFinalJourneyTime(String finalJourneyTime) {
            this.finalJourneyTime = finalJourneyTime;
        }

        public Integer getBlockedSeats() {
            return blockedSeats;
        }

        public void setBlockedSeats(Integer blockedSeats) {
            this.blockedSeats = blockedSeats;
        }

    }

}
