package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponCodeDetails_Request {
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BUM_BranchUserID")
    @Expose
    private Integer bUMBranchUserID;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("FromCityID")
    @Expose
    private Integer fromCityID;
    @SerializedName("ToCityID")
    @Expose
    private Integer toCityID;
    @SerializedName("BTM_BookingTypeID")
    @Expose
    private Integer bTMBookingTypeID;
    @SerializedName("JourneyDate")
    @Expose
    private String journeyDate;
    @SerializedName("CC_CouponCode")
    @Expose
    private String cCCouponCode;
    @SerializedName("CC_IsActive")
    @Expose
    private Integer cCIsActive;
    @SerializedName("JM_SeatString")
    @Expose
    private String jMSeatString;
    @SerializedName("multiBook")
    @Expose
    private Integer multiBook;
    @SerializedName("BAM_ArrangementID")
    @Expose
    private Integer bAMArrangementID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBUMBranchUserID() {
        return bUMBranchUserID;
    }

    public void setBUMBranchUserID(Integer bUMBranchUserID) {
        this.bUMBranchUserID = bUMBranchUserID;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public Integer getFromCityID() {
        return fromCityID;
    }

    public void setFromCityID(Integer fromCityID) {
        this.fromCityID = fromCityID;
    }

    public Integer getToCityID() {
        return toCityID;
    }

    public void setToCityID(Integer toCityID) {
        this.toCityID = toCityID;
    }

    public Integer getBTMBookingTypeID() {
        return bTMBookingTypeID;
    }

    public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
        this.bTMBookingTypeID = bTMBookingTypeID;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public String getCCCouponCode() {
        return cCCouponCode;
    }

    public void setCCCouponCode(String cCCouponCode) {
        this.cCCouponCode = cCCouponCode;
    }

    public Integer getCCIsActive() {
        return cCIsActive;
    }

    public void setCCIsActive(Integer cCIsActive) {
        this.cCIsActive = cCIsActive;
    }

    public String getJMSeatString() {
        return jMSeatString;
    }

    public void setJMSeatString(String jMSeatString) {
        this.jMSeatString = jMSeatString;
    }

    public Integer getMultiBook() {
        return multiBook;
    }

    public void setMultiBook(Integer multiBook) {
        this.multiBook = multiBook;
    }

    public Integer getBAMArrangementID() {
        return bAMArrangementID;
    }

    public void setBAMArrangementID(Integer bAMArrangementID) {
        this.bAMArrangementID = bAMArrangementID;
    }

    public CouponCodeDetails_Request(Integer cMCompanyID, Integer bMBranchID, Integer bUMBranchUserID, Integer rMRouteID, Integer rTRouteTimeID, Integer fromCityID, Integer toCityID, Integer bTMBookingTypeID, String journeyDate, String cCCouponCode, Integer cCIsActive, String jMSeatString, Integer multiBook, Integer bAMArrangementID) {
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
        this.bUMBranchUserID = bUMBranchUserID;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.fromCityID = fromCityID;
        this.toCityID = toCityID;
        this.bTMBookingTypeID = bTMBookingTypeID;
        this.journeyDate = journeyDate;
        this.cCCouponCode = cCCouponCode;
        this.cCIsActive = cCIsActive;
        this.jMSeatString = jMSeatString;
        this.multiBook = multiBook;
        this.bAMArrangementID = bAMArrangementID;
    }
}
