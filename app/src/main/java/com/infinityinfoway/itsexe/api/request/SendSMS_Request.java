package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendSMS_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BUM_UserID")
    @Expose
    private Integer bUMUserID;
    @SerializedName("JM_PNRNO")
    @Expose
    private String jmPnrno;
    @SerializedName("SMSType")
    @Expose
    private Integer sMSType;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBUMUserID() {
        return bUMUserID;
    }

    public void setBUMUserID(Integer bUMUserID) {
        this.bUMUserID = bUMUserID;
    }

    public String getJmPnrno() {
        return jmPnrno;
    }

    public void setJmPnrno(String jmPnrno) {
        this.jmPnrno = jmPnrno;
    }

    public Integer getSMSType() {
        return sMSType;
    }

    public void setSMSType(Integer sMSType) {
        this.sMSType = sMSType;
    }

    public SendSMS_Request(Integer cMCompanyID, Integer bMBranchID, Integer bUMUserID, String jmPnrno, Integer sMSType) {
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
        this.bUMUserID = bUMUserID;
        this.jmPnrno = jmPnrno;
        this.sMSType = sMSType;
    }
}


