package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusIncomeReceipt_Request {
    @Expose
    @SerializedName("BM_BranchUserID")
    private int BM_BranchUserID;
    @Expose
    @SerializedName("RT_Time")
    private int RT_Time;
    @Expose
    @SerializedName("RM_RouteID")
    private int RM_RouteID;
    @Expose
    @SerializedName("CM_CompanyID")
    private int CM_CompanyID;
    @Expose
    @SerializedName("JM_JourneyStartDate")
    private String JM_JourneyStartDate;

    public int getBM_BranchUserID() {
        return BM_BranchUserID;
    }

    public void setBM_BranchUserID(int BM_BranchUserID) {
        this.BM_BranchUserID = BM_BranchUserID;
    }

    public int getRT_Time() {
        return RT_Time;
    }

    public void setRT_Time(int RT_Time) {
        this.RT_Time = RT_Time;
    }

    public int getRM_RouteID() {
        return RM_RouteID;
    }

    public void setRM_RouteID(int RM_RouteID) {
        this.RM_RouteID = RM_RouteID;
    }

    public int getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(int CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public String getJM_JourneyStartDate() {
        return JM_JourneyStartDate;
    }

    public void setJM_JourneyStartDate(String JM_JourneyStartDate) {
        this.JM_JourneyStartDate = JM_JourneyStartDate;
    }

    public BusIncomeReceipt_Request(int BM_BranchUserID, int RT_Time, int RM_RouteID, int CM_CompanyID, String JM_JourneyStartDate) {
        this.BM_BranchUserID = BM_BranchUserID;
        this.RT_Time = RT_Time;
        this.RM_RouteID = RM_RouteID;
        this.CM_CompanyID = CM_CompanyID;
        this.JM_JourneyStartDate = JM_JourneyStartDate;
    }
}
