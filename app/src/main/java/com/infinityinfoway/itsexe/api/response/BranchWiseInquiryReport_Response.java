package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BranchWiseInquiryReport_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("JM_PNRNO")
        @Expose
        private String jMPNRNO;
        @SerializedName("JM_TicketNo")
        @Expose
        private String jMTicketNo;
        @SerializedName("BUM_UserName")
        @Expose
        private String bUMUserName;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private String jMTotalPassengers;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("DropName")
        @Expose
        private String dropName;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("Fare")
        @Expose
        private Double fare;
        @SerializedName("STAX")
        @Expose
        private Double sTAX;
        @SerializedName("BKGFARE")
        @Expose
        private Double bKGFARE;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("AgentQuota")
        @Expose
        private String agentQuota;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_CompanyAddress")
        @Expose
        private String cMCompanyAddress;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("DM_DriverName")
        @Expose
        private String dMDriverName;
        @SerializedName("DM_PhoneNo")
        @Expose
        private String dMPhoneNo;
        @SerializedName("PickupManName")
        @Expose
        private String pickupManName;
        @SerializedName("PM_PhoneNo")
        @Expose
        private String pMPhoneNo;
        @SerializedName("DM_PhoneNo1")
        @Expose
        private String dMPhoneNo1;
        @SerializedName("DM_PhoneNo2")
        @Expose
        private String dMPhoneNo2;
        @SerializedName("ReportUserName")
        @Expose
        private String reportUserName;
        @SerializedName("DM_DriverName1")
        @Expose
        private String dMDriverName1;
        @SerializedName("DM_Driver1")
        @Expose
        private String dMDriver1;
        @SerializedName("DM_Driver2")
        @Expose
        private String dMDriver2;
        @SerializedName("DM_Conductor1")
        @Expose
        private String dMConductor1;
        @SerializedName("DM_ConductorPhone")
        @Expose
        private String dMConductorPhone;
        @SerializedName("GroupBy")
        @Expose
        private String groupBy;

        public String getJMPNRNO() {
            return jMPNRNO;
        }

        public void setJMPNRNO(String jMPNRNO) {
            this.jMPNRNO = jMPNRNO;
        }

        public String getJMTicketNo() {
            return jMTicketNo;
        }

        public void setJMTicketNo(String jMTicketNo) {
            this.jMTicketNo = jMTicketNo;
        }

        public String getBUMUserName() {
            return bUMUserName;
        }

        public void setBUMUserName(String bUMUserName) {
            this.bUMUserName = bUMUserName;
        }

        public String getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(String jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public String getDropName() {
            return dropName;
        }

        public void setDropName(String dropName) {
            this.dropName = dropName;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public Double getFare() {
            return fare;
        }

        public void setFare(Double fare) {
            this.fare = fare;
        }

        public Double getSTAX() {
            return sTAX;
        }

        public void setSTAX(Double sTAX) {
            this.sTAX = sTAX;
        }

        public Double getBKGFARE() {
            return bKGFARE;
        }

        public void setBKGFARE(Double bKGFARE) {
            this.bKGFARE = bKGFARE;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getAgentQuota() {
            return agentQuota;
        }

        public void setAgentQuota(String agentQuota) {
            this.agentQuota = agentQuota;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMCompanyAddress() {
            return cMCompanyAddress;
        }

        public void setCMCompanyAddress(String cMCompanyAddress) {
            this.cMCompanyAddress = cMCompanyAddress;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getDMDriverName() {
            return dMDriverName;
        }

        public void setDMDriverName(String dMDriverName) {
            this.dMDriverName = dMDriverName;
        }

        public String getDMPhoneNo() {
            return dMPhoneNo;
        }

        public void setDMPhoneNo(String dMPhoneNo) {
            this.dMPhoneNo = dMPhoneNo;
        }

        public String getPickupManName() {
            return pickupManName;
        }

        public void setPickupManName(String pickupManName) {
            this.pickupManName = pickupManName;
        }

        public String getPMPhoneNo() {
            return pMPhoneNo;
        }

        public void setPMPhoneNo(String pMPhoneNo) {
            this.pMPhoneNo = pMPhoneNo;
        }

        public String getDMPhoneNo1() {
            return dMPhoneNo1;
        }

        public void setDMPhoneNo1(String dMPhoneNo1) {
            this.dMPhoneNo1 = dMPhoneNo1;
        }

        public String getDMPhoneNo2() {
            return dMPhoneNo2;
        }

        public void setDMPhoneNo2(String dMPhoneNo2) {
            this.dMPhoneNo2 = dMPhoneNo2;
        }

        public String getReportUserName() {
            return reportUserName;
        }

        public void setReportUserName(String reportUserName) {
            this.reportUserName = reportUserName;
        }

        public String getDMDriverName1() {
            return dMDriverName1;
        }

        public void setDMDriverName1(String dMDriverName1) {
            this.dMDriverName1 = dMDriverName1;
        }

        public String getDMDriver1() {
            return dMDriver1;
        }

        public void setDMDriver1(String dMDriver1) {
            this.dMDriver1 = dMDriver1;
        }

        public String getDMDriver2() {
            return dMDriver2;
        }

        public void setDMDriver2(String dMDriver2) {
            this.dMDriver2 = dMDriver2;
        }

        public String getDMConductor1() {
            return dMConductor1;
        }

        public void setDMConductor1(String dMConductor1) {
            this.dMConductor1 = dMConductor1;
        }

        public String getDMConductorPhone() {
            return dMConductorPhone;
        }

        public void setDMConductorPhone(String dMConductorPhone) {
            this.dMConductorPhone = dMConductorPhone;
        }

        public String getGroupBy() {
            return groupBy;
        }

        public void setGroupBy(String groupBy) {
            this.groupBy = groupBy;
        }

    }

}
