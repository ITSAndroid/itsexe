package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Searching_PrintGSTInvoice_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Searching_PrintGSTInvoice")
        @Expose
        private List<SearchingPrintGSTInvoice> searchingPrintGSTInvoice = null;
        @SerializedName("Table1")
        @Expose
        private List<Table1> table1 = null;
        @SerializedName("PDFUrl")
        @Expose
        private List<PDFUrl> pDFUrl = null;

        public List<SearchingPrintGSTInvoice> getSearchingPrintGSTInvoice() {
            return searchingPrintGSTInvoice;
        }

        public void setSearchingPrintGSTInvoice(List<SearchingPrintGSTInvoice> searchingPrintGSTInvoice) {
            this.searchingPrintGSTInvoice = searchingPrintGSTInvoice;
        }

        public List<Table1> getTable1() {
            return table1;
        }

        public void setTable1(List<Table1> table1) {
            this.table1 = table1;
        }

        public List<PDFUrl> getPDFUrl() {
            return pDFUrl;
        }

        public void setPDFUrl(List<PDFUrl> pDFUrl) {
            this.pDFUrl = pDFUrl;
        }

    }

    public class PDFUrl {

        @SerializedName("URL")
        @Expose
        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

    public class SearchingPrintGSTInvoice {
        @SerializedName("JM_PNRNO")
        @Expose
        private Integer jmPnrno;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_Address")
        @Expose
        private String cMAddress;
        @SerializedName("CM_Email")
        @Expose
        private String cMEmail;
        @SerializedName("CM_Website")
        @Expose
        private String cMWebsite;
        @SerializedName("CBD_GSTNo")
        @Expose
        private String cBDGSTNo;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_BookingDateTime")
        @Expose
        private String jMBookingDateTime;
        @SerializedName("SeatList")
        @Expose
        private String seatList;
        @SerializedName("NoOfPax")
        @Expose
        private Integer noOfPax;
        @SerializedName("SeatRate")
        @Expose
        private Double seatRate;
        @SerializedName("JM_CGST")
        @Expose
        private Double jmCgst;
        @SerializedName("JM_SGST")
        @Expose
        private Double jmSgst;
        @SerializedName("IGST")
        @Expose
        private Double igst;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("SAC")
        @Expose
        private String sac;
        @SerializedName("Satus")
        @Expose
        private Integer satus;
        @SerializedName("JM_BookedBy_CM_CompanyID")
        @Expose
        private Integer jMBookedByCMCompanyID;

        public Integer getJmPnrno() {
            return jmPnrno;
        }

        public void setJmPnrno(Integer jmPnrno) {
            this.jmPnrno = jmPnrno;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMAddress() {
            return cMAddress;
        }

        public void setCMAddress(String cMAddress) {
            this.cMAddress = cMAddress;
        }

        public String getCMEmail() {
            return cMEmail;
        }

        public void setCMEmail(String cMEmail) {
            this.cMEmail = cMEmail;
        }

        public String getCMWebsite() {
            return cMWebsite;
        }

        public void setCMWebsite(String cMWebsite) {
            this.cMWebsite = cMWebsite;
        }

        public String getCBDGSTNo() {
            return cBDGSTNo;
        }

        public void setCBDGSTNo(String cBDGSTNo) {
            this.cBDGSTNo = cBDGSTNo;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMBookingDateTime() {
            return jMBookingDateTime;
        }

        public void setJMBookingDateTime(String jMBookingDateTime) {
            this.jMBookingDateTime = jMBookingDateTime;
        }

        public String getSeatList() {
            return seatList;
        }

        public void setSeatList(String seatList) {
            this.seatList = seatList;
        }

        public Integer getNoOfPax() {
            return noOfPax;
        }

        public void setNoOfPax(Integer noOfPax) {
            this.noOfPax = noOfPax;
        }

        public Double getSeatRate() {
            return seatRate;
        }

        public void setSeatRate(Double seatRate) {
            this.seatRate = seatRate;
        }

        public Double getJmCgst() {
            return jmCgst;
        }

        public void setJmCgst(Double jmCgst) {
            this.jmCgst = jmCgst;
        }

        public Double getJmSgst() {
            return jmSgst;
        }

        public void setJmSgst(Double jmSgst) {
            this.jmSgst = jmSgst;
        }

        public Double getIgst() {
            return igst;
        }

        public void setIgst(Double igst) {
            this.igst = igst;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public String getSac() {
            return sac;
        }

        public void setSac(String sac) {
            this.sac = sac;
        }

        public Integer getSatus() {
            return satus;
        }

        public void setSatus(Integer satus) {
            this.satus = satus;
        }

        public Integer getJMBookedByCMCompanyID() {
            return jMBookedByCMCompanyID;
        }

        public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
            this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        }
    }

    public class Table1 {
        @SerializedName("JM_PNRNO")
        @Expose
        private Integer jmPnrno;
        @SerializedName("JM_BookingDateTime")
        @Expose
        private String jMBookingDateTime;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_JourneyStartTime")
        @Expose
        private String jMJourneyStartTime;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("JM_JourneyEndDate")
        @Expose
        private String jMJourneyEndDate;
        @SerializedName("JM_JourneyEndTime")
        @Expose
        private String jMJourneyEndTime;
        @SerializedName("JM_JourneyFrom")
        @Expose
        private Integer jMJourneyFrom;
        @SerializedName("JM_JourneyTo")
        @Expose
        private Integer jMJourneyTo;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteFrequency")
        @Expose
        private Integer rTRouteFrequency;
        @SerializedName("JM_ReportingDate")
        @Expose
        private String jMReportingDate;
        @SerializedName("JM_ReportingTime")
        @Expose
        private String jMReportingTime;
        @SerializedName("PM_PickupID")
        @Expose
        private Integer pMPickupID;
        @SerializedName("DM_DropID")
        @Expose
        private Integer dMDropID;
        @SerializedName("BTM_BookingTypeID")
        @Expose
        private Integer bTMBookingTypeID;
        @SerializedName("JM_TicketNo")
        @Expose
        private Integer jMTicketNo;
        @SerializedName("JM_IsTicketPrint")
        @Expose
        private Integer jMIsTicketPrint;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("UM_UserID")
        @Expose
        private Integer uMUserID;
        @SerializedName("JM_TicketStatus")
        @Expose
        private Integer jMTicketStatus;
        @SerializedName("JM_JourneyStatus")
        @Expose
        private Integer jMJourneyStatus;
        @SerializedName("PM_PaymentMode")
        @Expose
        private Integer pMPaymentMode;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private Integer jMTotalPassengers;
        @SerializedName("BTM_BusTypeID")
        @Expose
        private String bTMBusTypeID;
        @SerializedName("BAM_ArrangementID")
        @Expose
        private Integer bAMArrangementID;
        @SerializedName("RM_ACSeatRate")
        @Expose
        private Integer rMACSeatRate;
        @SerializedName("RM_ACSleeperRate")
        @Expose
        private Integer rMACSleeperRate;
        @SerializedName("RM_ACSlumberRate")
        @Expose
        private Integer rMACSlumberRate;
        @SerializedName("RM_NonACSeatRate")
        @Expose
        private Integer rMNonACSeatRate;
        @SerializedName("RM_NonACSleeperRate")
        @Expose
        private Integer rMNonACSleeperRate;
        @SerializedName("RM_NonAcSlumberRate")
        @Expose
        private Integer rMNonAcSlumberRate;
        @SerializedName("RM_ACSeatQuantity")
        @Expose
        private Integer rMACSeatQuantity;
        @SerializedName("RM_ACSleeperQuantity")
        @Expose
        private Integer rMACSleeperQuantity;
        @SerializedName("RM_ACSlumberQuantity")
        @Expose
        private Integer rMACSlumberQuantity;
        @SerializedName("RM_NonACSeatQuantity")
        @Expose
        private Integer rMNonACSeatQuantity;
        @SerializedName("RM_NonACSleeperQuantity")
        @Expose
        private Integer rMNonACSleeperQuantity;
        @SerializedName("RM_NonACSlumberQuantity")
        @Expose
        private Integer rMNonACSlumberQuantity;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("JM_Phone2")
        @Expose
        private String jMPhone2;
        @SerializedName("JM_EmailID")
        @Expose
        private String jMEmailID;
        @SerializedName("JM_Remarks")
        @Expose
        private String jMRemarks;
        @SerializedName("JM_JourneyCityTime")
        @Expose
        private String jMJourneyCityTime;
        @SerializedName("RT_Time")
        @Expose
        private Integer rTTime;
        @SerializedName("JM_CancelBy")
        @Expose
        private String jMCancelBy;
        @SerializedName("JM_CancelBy_BranchID")
        @Expose
        private String jMCancelByBranchID;
        @SerializedName("JM_CancelDate")
        @Expose
        private String jMCancelDate;
        @SerializedName("JM_Cancel_Amount")
        @Expose
        private String jMCancelAmount;
        @SerializedName("JM_Cancel_Remarks")
        @Expose
        private String jMCancelRemarks;
        @SerializedName("JM_ModifyBy")
        @Expose
        private Integer jMModifyBy;
        @SerializedName("JM_ModifyDate")
        @Expose
        private String jMModifyDate;
        @SerializedName("JM_PhoneOnHold")
        @Expose
        private Integer jMPhoneOnHold;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("JM_ChartFinish")
        @Expose
        private String jMChartFinish;
        @SerializedName("JM_ChartFinishAmount")
        @Expose
        private String jMChartFinishAmount;
        @SerializedName("JM_PhoneTime")
        @Expose
        private String jMPhoneTime;
        @SerializedName("JM_CancelFromUserID")
        @Expose
        private String jMCancelFromUserID;
        @SerializedName("JM_PhoneHourMin")
        @Expose
        private String jMPhoneHourMin;
        @SerializedName("JM_IsBlackList")
        @Expose
        private String jMIsBlackList;
        @SerializedName("JM_RefundCharges")
        @Expose
        private String jMRefundCharges;
        @SerializedName("JM_RefundPer")
        @Expose
        private String jMRefundPer;
        @SerializedName("JM_DiffMin")
        @Expose
        private String jMDiffMin;
        @SerializedName("JM_BookedBy_CM_CompanyID")
        @Expose
        private Integer jMBookedByCMCompanyID;
        @SerializedName("JM_B2B_BranchID")
        @Expose
        private Integer jMB2BBranchID;
        @SerializedName("JM_B2B_UserID")
        @Expose
        private Integer jMB2BUserID;
        @SerializedName("Agent_ID")
        @Expose
        private Integer agentID;
        @SerializedName("JM_OriginalTotalPayable")
        @Expose
        private String jMOriginalTotalPayable;
        @SerializedName("JM_PCRegistrationID")
        @Expose
        private Integer jMPCRegistrationID;
        @SerializedName("JM_IsPrepaidCard")
        @Expose
        private String jMIsPrepaidCard;
        @SerializedName("JM_ServiceTax")
        @Expose
        private Double jMServiceTax;
        @SerializedName("JM_EduCess")
        @Expose
        private Double jMEduCess;
        @SerializedName("JM_HigherEduCess")
        @Expose
        private Double jMHigherEduCess;
        @SerializedName("JM_RoundUP")
        @Expose
        private Double jMRoundUP;
        @SerializedName("JM_IsIncludeTax")
        @Expose
        private Integer jMIsIncludeTax;
        @SerializedName("JM_RefundServiceTax")
        @Expose
        private String jMRefundServiceTax;
        @SerializedName("ABPM_ID")
        @Expose
        private String abpmId;
        @SerializedName("JM_APIB2CPaymentJDate")
        @Expose
        private String jMAPIB2CPaymentJDate;
        @SerializedName("JM_B2C_OrderNo")
        @Expose
        private String jMB2COrderNo;
        @SerializedName("JM_CouponCode")
        @Expose
        private String jMCouponCode;
        @SerializedName("JM_CollectionAmount")
        @Expose
        private String jMCollectionAmount;
        @SerializedName("JM_PGType")
        @Expose
        private String jMPGType;
        @SerializedName("JM_PGPaymentType")
        @Expose
        private String jMPGPaymentType;
        @SerializedName("JM_TDRCharge")
        @Expose
        private String jMTDRCharge;
        @SerializedName("JM_IsTDRChargesInclude")
        @Expose
        private String jMIsTDRChargesInclude;
        @SerializedName("JM_MagicBoxCharges")
        @Expose
        private String jMMagicBoxCharges;
        @SerializedName("JM_CGST")
        @Expose
        private Double jmCgst;
        @SerializedName("JM_SGST")
        @Expose
        private Double jmSgst;
        @SerializedName("JM_GSTState")
        @Expose
        private Integer jMGSTState;
        @SerializedName("JM_GSTCompanyName")
        @Expose
        private String jMGSTCompanyName;
        @SerializedName("JM_GSTRegNo")
        @Expose
        private String jMGSTRegNo;
        @SerializedName("JM_CancelIGST")
        @Expose
        private String jMCancelIGST;
        @SerializedName("JM_CancelCGST")
        @Expose
        private String jMCancelCGST;
        @SerializedName("JM_CancelSGST")
        @Expose
        private String jMCancelSGST;
        @SerializedName("JM_PolicyID")
        @Expose
        private Integer jMPolicyID;
        @SerializedName("JM_ITSAdminID")
        @Expose
        private String jMITSAdminID;
        @SerializedName("JM_Discount")
        @Expose
        private String jMDiscount;
        @SerializedName("JM_ModifyCharges")
        @Expose
        private String jMModifyCharges;
        @SerializedName("JM_BirthDate")
        @Expose
        private String jMBirthDate;
        @SerializedName("JM_GSTSerialNo")
        @Expose
        private String jMGSTSerialNo;
        @SerializedName("JM_CreditSerialNo")
        @Expose
        private String jMCreditSerialNo;
        @SerializedName("JM_TicketSerialNo")
        @Expose
        private String jMTicketSerialNo;
        @SerializedName("JM_APIPNRNO")
        @Expose
        private String jmApipnrno;
        @SerializedName("JM_WalletRefundCharges")
        @Expose
        private String jMWalletRefundCharges;
        @SerializedName("JM_SurCharges")
        @Expose
        private String jMSurCharges;
        @SerializedName("JM_IPAddress")
        @Expose
        private String jMIPAddress;
        @SerializedName("JM_InsuranceCharges")
        @Expose
        private String jMInsuranceCharges;
        @SerializedName("JM_GoldSeatCharges")
        @Expose
        private String jMGoldSeatCharges;
        @SerializedName("JM_IncludeRefundInCollection")
        @Expose
        private String jMIncludeRefundInCollection;
        @SerializedName("JM_IsITSPG")
        @Expose
        private String jMIsITSPG;

        public Integer getJmPnrno() {
            return jmPnrno;
        }

        public void setJmPnrno(Integer jmPnrno) {
            this.jmPnrno = jmPnrno;
        }

        public String getJMBookingDateTime() {
            return jMBookingDateTime;
        }

        public void setJMBookingDateTime(String jMBookingDateTime) {
            this.jMBookingDateTime = jMBookingDateTime;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMJourneyStartTime() {
            return jMJourneyStartTime;
        }

        public void setJMJourneyStartTime(String jMJourneyStartTime) {
            this.jMJourneyStartTime = jMJourneyStartTime;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getJMJourneyEndDate() {
            return jMJourneyEndDate;
        }

        public void setJMJourneyEndDate(String jMJourneyEndDate) {
            this.jMJourneyEndDate = jMJourneyEndDate;
        }

        public String getJMJourneyEndTime() {
            return jMJourneyEndTime;
        }

        public void setJMJourneyEndTime(String jMJourneyEndTime) {
            this.jMJourneyEndTime = jMJourneyEndTime;
        }

        public Integer getJMJourneyFrom() {
            return jMJourneyFrom;
        }

        public void setJMJourneyFrom(Integer jMJourneyFrom) {
            this.jMJourneyFrom = jMJourneyFrom;
        }

        public Integer getJMJourneyTo() {
            return jMJourneyTo;
        }

        public void setJMJourneyTo(Integer jMJourneyTo) {
            this.jMJourneyTo = jMJourneyTo;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteFrequency() {
            return rTRouteFrequency;
        }

        public void setRTRouteFrequency(Integer rTRouteFrequency) {
            this.rTRouteFrequency = rTRouteFrequency;
        }

        public String getJMReportingDate() {
            return jMReportingDate;
        }

        public void setJMReportingDate(String jMReportingDate) {
            this.jMReportingDate = jMReportingDate;
        }

        public String getJMReportingTime() {
            return jMReportingTime;
        }

        public void setJMReportingTime(String jMReportingTime) {
            this.jMReportingTime = jMReportingTime;
        }

        public Integer getPMPickupID() {
            return pMPickupID;
        }

        public void setPMPickupID(Integer pMPickupID) {
            this.pMPickupID = pMPickupID;
        }

        public Integer getDMDropID() {
            return dMDropID;
        }

        public void setDMDropID(Integer dMDropID) {
            this.dMDropID = dMDropID;
        }

        public Integer getBTMBookingTypeID() {
            return bTMBookingTypeID;
        }

        public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
            this.bTMBookingTypeID = bTMBookingTypeID;
        }

        public Integer getJMTicketNo() {
            return jMTicketNo;
        }

        public void setJMTicketNo(Integer jMTicketNo) {
            this.jMTicketNo = jMTicketNo;
        }

        public Integer getJMIsTicketPrint() {
            return jMIsTicketPrint;
        }

        public void setJMIsTicketPrint(Integer jMIsTicketPrint) {
            this.jMIsTicketPrint = jMIsTicketPrint;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public Integer getUMUserID() {
            return uMUserID;
        }

        public void setUMUserID(Integer uMUserID) {
            this.uMUserID = uMUserID;
        }

        public Integer getJMTicketStatus() {
            return jMTicketStatus;
        }

        public void setJMTicketStatus(Integer jMTicketStatus) {
            this.jMTicketStatus = jMTicketStatus;
        }

        public Integer getJMJourneyStatus() {
            return jMJourneyStatus;
        }

        public void setJMJourneyStatus(Integer jMJourneyStatus) {
            this.jMJourneyStatus = jMJourneyStatus;
        }

        public Integer getPMPaymentMode() {
            return pMPaymentMode;
        }

        public void setPMPaymentMode(Integer pMPaymentMode) {
            this.pMPaymentMode = pMPaymentMode;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public Integer getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(Integer jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public String getBTMBusTypeID() {
            return bTMBusTypeID;
        }

        public void setBTMBusTypeID(String bTMBusTypeID) {
            this.bTMBusTypeID = bTMBusTypeID;
        }

        public Integer getBAMArrangementID() {
            return bAMArrangementID;
        }

        public void setBAMArrangementID(Integer bAMArrangementID) {
            this.bAMArrangementID = bAMArrangementID;
        }

        public Integer getRMACSeatRate() {
            return rMACSeatRate;
        }

        public void setRMACSeatRate(Integer rMACSeatRate) {
            this.rMACSeatRate = rMACSeatRate;
        }

        public Integer getRMACSleeperRate() {
            return rMACSleeperRate;
        }

        public void setRMACSleeperRate(Integer rMACSleeperRate) {
            this.rMACSleeperRate = rMACSleeperRate;
        }

        public Integer getRMACSlumberRate() {
            return rMACSlumberRate;
        }

        public void setRMACSlumberRate(Integer rMACSlumberRate) {
            this.rMACSlumberRate = rMACSlumberRate;
        }

        public Integer getRMNonACSeatRate() {
            return rMNonACSeatRate;
        }

        public void setRMNonACSeatRate(Integer rMNonACSeatRate) {
            this.rMNonACSeatRate = rMNonACSeatRate;
        }

        public Integer getRMNonACSleeperRate() {
            return rMNonACSleeperRate;
        }

        public void setRMNonACSleeperRate(Integer rMNonACSleeperRate) {
            this.rMNonACSleeperRate = rMNonACSleeperRate;
        }

        public Integer getRMNonAcSlumberRate() {
            return rMNonAcSlumberRate;
        }

        public void setRMNonAcSlumberRate(Integer rMNonAcSlumberRate) {
            this.rMNonAcSlumberRate = rMNonAcSlumberRate;
        }

        public Integer getRMACSeatQuantity() {
            return rMACSeatQuantity;
        }

        public void setRMACSeatQuantity(Integer rMACSeatQuantity) {
            this.rMACSeatQuantity = rMACSeatQuantity;
        }

        public Integer getRMACSleeperQuantity() {
            return rMACSleeperQuantity;
        }

        public void setRMACSleeperQuantity(Integer rMACSleeperQuantity) {
            this.rMACSleeperQuantity = rMACSleeperQuantity;
        }

        public Integer getRMACSlumberQuantity() {
            return rMACSlumberQuantity;
        }

        public void setRMACSlumberQuantity(Integer rMACSlumberQuantity) {
            this.rMACSlumberQuantity = rMACSlumberQuantity;
        }

        public Integer getRMNonACSeatQuantity() {
            return rMNonACSeatQuantity;
        }

        public void setRMNonACSeatQuantity(Integer rMNonACSeatQuantity) {
            this.rMNonACSeatQuantity = rMNonACSeatQuantity;
        }

        public Integer getRMNonACSleeperQuantity() {
            return rMNonACSleeperQuantity;
        }

        public void setRMNonACSleeperQuantity(Integer rMNonACSleeperQuantity) {
            this.rMNonACSleeperQuantity = rMNonACSleeperQuantity;
        }

        public Integer getRMNonACSlumberQuantity() {
            return rMNonACSlumberQuantity;
        }

        public void setRMNonACSlumberQuantity(Integer rMNonACSlumberQuantity) {
            this.rMNonACSlumberQuantity = rMNonACSlumberQuantity;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getJMPhone2() {
            return jMPhone2;
        }

        public void setJMPhone2(String jMPhone2) {
            this.jMPhone2 = jMPhone2;
        }

        public String getJMEmailID() {
            return jMEmailID;
        }

        public void setJMEmailID(String jMEmailID) {
            this.jMEmailID = jMEmailID;
        }

        public String getJMRemarks() {
            return jMRemarks;
        }

        public void setJMRemarks(String jMRemarks) {
            this.jMRemarks = jMRemarks;
        }

        public String getJMJourneyCityTime() {
            return jMJourneyCityTime;
        }

        public void setJMJourneyCityTime(String jMJourneyCityTime) {
            this.jMJourneyCityTime = jMJourneyCityTime;
        }

        public Integer getRTTime() {
            return rTTime;
        }

        public void setRTTime(Integer rTTime) {
            this.rTTime = rTTime;
        }

        public String getJMCancelBy() {
            return jMCancelBy;
        }

        public void setJMCancelBy(String jMCancelBy) {
            this.jMCancelBy = jMCancelBy;
        }

        public String getJMCancelByBranchID() {
            return jMCancelByBranchID;
        }

        public void setJMCancelByBranchID(String jMCancelByBranchID) {
            this.jMCancelByBranchID = jMCancelByBranchID;
        }

        public String getJMCancelDate() {
            return jMCancelDate;
        }

        public void setJMCancelDate(String jMCancelDate) {
            this.jMCancelDate = jMCancelDate;
        }

        public String getJMCancelAmount() {
            return jMCancelAmount;
        }

        public void setJMCancelAmount(String jMCancelAmount) {
            this.jMCancelAmount = jMCancelAmount;
        }

        public String getJMCancelRemarks() {
            return jMCancelRemarks;
        }

        public void setJMCancelRemarks(String jMCancelRemarks) {
            this.jMCancelRemarks = jMCancelRemarks;
        }

        public Integer getJMModifyBy() {
            return jMModifyBy;
        }

        public void setJMModifyBy(Integer jMModifyBy) {
            this.jMModifyBy = jMModifyBy;
        }

        public String getJMModifyDate() {
            return jMModifyDate;
        }

        public void setJMModifyDate(String jMModifyDate) {
            this.jMModifyDate = jMModifyDate;
        }

        public Integer getJMPhoneOnHold() {
            return jMPhoneOnHold;
        }

        public void setJMPhoneOnHold(Integer jMPhoneOnHold) {
            this.jMPhoneOnHold = jMPhoneOnHold;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getJMChartFinish() {
            return jMChartFinish;
        }

        public void setJMChartFinish(String jMChartFinish) {
            this.jMChartFinish = jMChartFinish;
        }

        public String getJMChartFinishAmount() {
            return jMChartFinishAmount;
        }

        public void setJMChartFinishAmount(String jMChartFinishAmount) {
            this.jMChartFinishAmount = jMChartFinishAmount;
        }

        public String getJMPhoneTime() {
            return jMPhoneTime;
        }

        public void setJMPhoneTime(String jMPhoneTime) {
            this.jMPhoneTime = jMPhoneTime;
        }

        public String getJMCancelFromUserID() {
            return jMCancelFromUserID;
        }

        public void setJMCancelFromUserID(String jMCancelFromUserID) {
            this.jMCancelFromUserID = jMCancelFromUserID;
        }

        public String getJMPhoneHourMin() {
            return jMPhoneHourMin;
        }

        public void setJMPhoneHourMin(String jMPhoneHourMin) {
            this.jMPhoneHourMin = jMPhoneHourMin;
        }

        public String getJMIsBlackList() {
            return jMIsBlackList;
        }

        public void setJMIsBlackList(String jMIsBlackList) {
            this.jMIsBlackList = jMIsBlackList;
        }

        public String getJMRefundCharges() {
            return jMRefundCharges;
        }

        public void setJMRefundCharges(String jMRefundCharges) {
            this.jMRefundCharges = jMRefundCharges;
        }

        public String getJMRefundPer() {
            return jMRefundPer;
        }

        public void setJMRefundPer(String jMRefundPer) {
            this.jMRefundPer = jMRefundPer;
        }

        public String getJMDiffMin() {
            return jMDiffMin;
        }

        public void setJMDiffMin(String jMDiffMin) {
            this.jMDiffMin = jMDiffMin;
        }

        public Integer getJMBookedByCMCompanyID() {
            return jMBookedByCMCompanyID;
        }

        public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
            this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        }

        public Integer getJMB2BBranchID() {
            return jMB2BBranchID;
        }

        public void setJMB2BBranchID(Integer jMB2BBranchID) {
            this.jMB2BBranchID = jMB2BBranchID;
        }

        public Integer getJMB2BUserID() {
            return jMB2BUserID;
        }

        public void setJMB2BUserID(Integer jMB2BUserID) {
            this.jMB2BUserID = jMB2BUserID;
        }

        public Integer getAgentID() {
            return agentID;
        }

        public void setAgentID(Integer agentID) {
            this.agentID = agentID;
        }

        public String getJMOriginalTotalPayable() {
            return jMOriginalTotalPayable;
        }

        public void setJMOriginalTotalPayable(String jMOriginalTotalPayable) {
            this.jMOriginalTotalPayable = jMOriginalTotalPayable;
        }

        public Integer getJMPCRegistrationID() {
            return jMPCRegistrationID;
        }

        public void setJMPCRegistrationID(Integer jMPCRegistrationID) {
            this.jMPCRegistrationID = jMPCRegistrationID;
        }

        public String getJMIsPrepaidCard() {
            return jMIsPrepaidCard;
        }

        public void setJMIsPrepaidCard(String jMIsPrepaidCard) {
            this.jMIsPrepaidCard = jMIsPrepaidCard;
        }

        public Double getJMServiceTax() {
            return jMServiceTax;
        }

        public void setJMServiceTax(Double jMServiceTax) {
            this.jMServiceTax = jMServiceTax;
        }

        public Double getJMEduCess() {
            return jMEduCess;
        }

        public void setJMEduCess(Double jMEduCess) {
            this.jMEduCess = jMEduCess;
        }

        public Double getJMHigherEduCess() {
            return jMHigherEduCess;
        }

        public void setJMHigherEduCess(Double jMHigherEduCess) {
            this.jMHigherEduCess = jMHigherEduCess;
        }

        public Double getJMRoundUP() {
            return jMRoundUP;
        }

        public void setJMRoundUP(Double jMRoundUP) {
            this.jMRoundUP = jMRoundUP;
        }

        public Integer getJMIsIncludeTax() {
            return jMIsIncludeTax;
        }

        public void setJMIsIncludeTax(Integer jMIsIncludeTax) {
            this.jMIsIncludeTax = jMIsIncludeTax;
        }

        public String getJMRefundServiceTax() {
            return jMRefundServiceTax;
        }

        public void setJMRefundServiceTax(String jMRefundServiceTax) {
            this.jMRefundServiceTax = jMRefundServiceTax;
        }

        public String getAbpmId() {
            return abpmId;
        }

        public void setAbpmId(String abpmId) {
            this.abpmId = abpmId;
        }

        public String getJMAPIB2CPaymentJDate() {
            return jMAPIB2CPaymentJDate;
        }

        public void setJMAPIB2CPaymentJDate(String jMAPIB2CPaymentJDate) {
            this.jMAPIB2CPaymentJDate = jMAPIB2CPaymentJDate;
        }

        public String getJMB2COrderNo() {
            return jMB2COrderNo;
        }

        public void setJMB2COrderNo(String jMB2COrderNo) {
            this.jMB2COrderNo = jMB2COrderNo;
        }

        public String getJMCouponCode() {
            return jMCouponCode;
        }

        public void setJMCouponCode(String jMCouponCode) {
            this.jMCouponCode = jMCouponCode;
        }

        public String getJMCollectionAmount() {
            return jMCollectionAmount;
        }

        public void setJMCollectionAmount(String jMCollectionAmount) {
            this.jMCollectionAmount = jMCollectionAmount;
        }

        public String getJMPGType() {
            return jMPGType;
        }

        public void setJMPGType(String jMPGType) {
            this.jMPGType = jMPGType;
        }

        public String getJMPGPaymentType() {
            return jMPGPaymentType;
        }

        public void setJMPGPaymentType(String jMPGPaymentType) {
            this.jMPGPaymentType = jMPGPaymentType;
        }

        public String getJMTDRCharge() {
            return jMTDRCharge;
        }

        public void setJMTDRCharge(String jMTDRCharge) {
            this.jMTDRCharge = jMTDRCharge;
        }

        public String getJMIsTDRChargesInclude() {
            return jMIsTDRChargesInclude;
        }

        public void setJMIsTDRChargesInclude(String jMIsTDRChargesInclude) {
            this.jMIsTDRChargesInclude = jMIsTDRChargesInclude;
        }

        public String getJMMagicBoxCharges() {
            return jMMagicBoxCharges;
        }

        public void setJMMagicBoxCharges(String jMMagicBoxCharges) {
            this.jMMagicBoxCharges = jMMagicBoxCharges;
        }

        public Double getJmCgst() {
            return jmCgst;
        }

        public void setJmCgst(Double jmCgst) {
            this.jmCgst = jmCgst;
        }

        public Double getJmSgst() {
            return jmSgst;
        }

        public void setJmSgst(Double jmSgst) {
            this.jmSgst = jmSgst;
        }

        public Integer getJMGSTState() {
            return jMGSTState;
        }

        public void setJMGSTState(Integer jMGSTState) {
            this.jMGSTState = jMGSTState;
        }

        public String getJMGSTCompanyName() {
            return jMGSTCompanyName;
        }

        public void setJMGSTCompanyName(String jMGSTCompanyName) {
            this.jMGSTCompanyName = jMGSTCompanyName;
        }

        public String getJMGSTRegNo() {
            return jMGSTRegNo;
        }

        public void setJMGSTRegNo(String jMGSTRegNo) {
            this.jMGSTRegNo = jMGSTRegNo;
        }

        public String getJMCancelIGST() {
            return jMCancelIGST;
        }

        public void setJMCancelIGST(String jMCancelIGST) {
            this.jMCancelIGST = jMCancelIGST;
        }

        public String getJMCancelCGST() {
            return jMCancelCGST;
        }

        public void setJMCancelCGST(String jMCancelCGST) {
            this.jMCancelCGST = jMCancelCGST;
        }

        public String getJMCancelSGST() {
            return jMCancelSGST;
        }

        public void setJMCancelSGST(String jMCancelSGST) {
            this.jMCancelSGST = jMCancelSGST;
        }

        public Integer getJMPolicyID() {
            return jMPolicyID;
        }

        public void setJMPolicyID(Integer jMPolicyID) {
            this.jMPolicyID = jMPolicyID;
        }

        public String getJMITSAdminID() {
            return jMITSAdminID;
        }

        public void setJMITSAdminID(String jMITSAdminID) {
            this.jMITSAdminID = jMITSAdminID;
        }

        public String getJMDiscount() {
            return jMDiscount;
        }

        public void setJMDiscount(String jMDiscount) {
            this.jMDiscount = jMDiscount;
        }

        public String getJMModifyCharges() {
            return jMModifyCharges;
        }

        public void setJMModifyCharges(String jMModifyCharges) {
            this.jMModifyCharges = jMModifyCharges;
        }

        public String getJMBirthDate() {
            return jMBirthDate;
        }

        public void setJMBirthDate(String jMBirthDate) {
            this.jMBirthDate = jMBirthDate;
        }

        public String getJMGSTSerialNo() {
            return jMGSTSerialNo;
        }

        public void setJMGSTSerialNo(String jMGSTSerialNo) {
            this.jMGSTSerialNo = jMGSTSerialNo;
        }

        public String getJMCreditSerialNo() {
            return jMCreditSerialNo;
        }

        public void setJMCreditSerialNo(String jMCreditSerialNo) {
            this.jMCreditSerialNo = jMCreditSerialNo;
        }

        public String getJMTicketSerialNo() {
            return jMTicketSerialNo;
        }

        public void setJMTicketSerialNo(String jMTicketSerialNo) {
            this.jMTicketSerialNo = jMTicketSerialNo;
        }

        public String getJmApipnrno() {
            return jmApipnrno;
        }

        public void setJmApipnrno(String jmApipnrno) {
            this.jmApipnrno = jmApipnrno;
        }

        public String getJMWalletRefundCharges() {
            return jMWalletRefundCharges;
        }

        public void setJMWalletRefundCharges(String jMWalletRefundCharges) {
            this.jMWalletRefundCharges = jMWalletRefundCharges;
        }

        public String getJMSurCharges() {
            return jMSurCharges;
        }

        public void setJMSurCharges(String jMSurCharges) {
            this.jMSurCharges = jMSurCharges;
        }

        public String getJMIPAddress() {
            return jMIPAddress;
        }

        public void setJMIPAddress(String jMIPAddress) {
            this.jMIPAddress = jMIPAddress;
        }

        public String getJMInsuranceCharges() {
            return jMInsuranceCharges;
        }

        public void setJMInsuranceCharges(String jMInsuranceCharges) {
            this.jMInsuranceCharges = jMInsuranceCharges;
        }

        public String getJMGoldSeatCharges() {
            return jMGoldSeatCharges;
        }

        public void setJMGoldSeatCharges(String jMGoldSeatCharges) {
            this.jMGoldSeatCharges = jMGoldSeatCharges;
        }

        public String getJMIncludeRefundInCollection() {
            return jMIncludeRefundInCollection;
        }

        public void setJMIncludeRefundInCollection(String jMIncludeRefundInCollection) {
            this.jMIncludeRefundInCollection = jMIncludeRefundInCollection;
        }

        public String getJMIsITSPG() {
            return jMIsITSPG;
        }

        public void setJMIsITSPG(String jMIsITSPG) {
            this.jMIsITSPG = jMIsITSPG;
        }
    }

}
