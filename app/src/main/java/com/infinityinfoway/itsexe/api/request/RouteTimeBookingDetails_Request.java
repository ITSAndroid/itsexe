package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteTimeBookingDetails_Request {
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("BM_BranchUserID")
    @Expose
    private Integer bMBranchUserID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public Integer getBMBranchUserID() {
        return bMBranchUserID;
    }

    public void setBMBranchUserID(Integer bMBranchUserID) {
        this.bMBranchUserID = bMBranchUserID;
    }

    public RouteTimeBookingDetails_Request(Integer cMCompanyID, String jMJourneyStartDate, Integer rMRouteID, Integer rTRouteTimeID, Integer bMBranchUserID) {
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.bMBranchUserID = bMBranchUserID;
    }
}
