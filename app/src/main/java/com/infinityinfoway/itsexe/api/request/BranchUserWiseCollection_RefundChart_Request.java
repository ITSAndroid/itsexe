package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchUserWiseCollection_RefundChart_Request {
    @SerializedName("JM_BookingDateTime")
    @Expose
    private String jMBookingDateTime;
    @SerializedName("BUM_BranchUserID")
    @Expose
    private Integer bUMBranchUserID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("IsPendingAmount")
    @Expose
    private Integer isPendingAmount;

    public String getJMBookingDateTime() {
        return jMBookingDateTime;
    }

    public void setJMBookingDateTime(String jMBookingDateTime) {
        this.jMBookingDateTime = jMBookingDateTime;
    }

    public Integer getBUMBranchUserID() {
        return bUMBranchUserID;
    }

    public void setBUMBranchUserID(Integer bUMBranchUserID) {
        this.bUMBranchUserID = bUMBranchUserID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getIsPendingAmount() {
        return isPendingAmount;
    }

    public void setIsPendingAmount(Integer isPendingAmount) {
        this.isPendingAmount = isPendingAmount;
    }

    public BranchUserWiseCollection_RefundChart_Request(String jMBookingDateTime, Integer bUMBranchUserID, Integer bMBranchID, Integer cMCompanyID, Integer isPendingAmount) {
        this.jMBookingDateTime = jMBookingDateTime;
        this.bUMBranchUserID = bUMBranchUserID;
        this.bMBranchID = bMBranchID;
        this.cMCompanyID = cMCompanyID;
        this.isPendingAmount = isPendingAmount;
    }
}
