package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReleaseStopBooking_Request {

    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("RSBR_RouteStopBookingRemarkID")
    @Expose
    private Integer rSBRRouteStopBookingRemarkID;

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getRSBRRouteStopBookingRemarkID() {
        return rSBRRouteStopBookingRemarkID;
    }

    public void setRSBRRouteStopBookingRemarkID(Integer rSBRRouteStopBookingRemarkID) {
        this.rSBRRouteStopBookingRemarkID = rSBRRouteStopBookingRemarkID;
    }

    public ReleaseStopBooking_Request(String jMJourneyStartDate, Integer cMCompanyID, Integer rMRouteID, Integer rTRouteTimeID, Integer bMBranchID, Integer uMUserID, Integer rSBRRouteStopBookingRemarkID) {
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.cMCompanyID = cMCompanyID;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.bMBranchID = bMBranchID;
        this.uMUserID = uMUserID;
        this.rSBRRouteStopBookingRemarkID = rSBRRouteStopBookingRemarkID;
    }


}
