package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RouteDepartureReport_Response {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("JM_PNRNO")
        @Expose
        private String jMPNRNO;
        @SerializedName("JM_TicketNo")
        @Expose
        private String jMTicketNo;
        @SerializedName("PassengerName")
        @Expose
        private String passengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("AM_AgentName")
        @Expose
        private String aMAgentName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Fare")
        @Expose
        private String fare;
        @SerializedName("BM_BusNo")
        @Expose
        private String bMBusNo;
        @SerializedName("DropOffice")
        @Expose
        private String dropOffice;
        @SerializedName("BoardingAt")
        @Expose
        private String boardingAt;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_Address")
        @Expose
        private String cMAddress;
        @SerializedName("CM_CompanyShortName")
        @Expose
        private String cMCompanyShortName;
        @SerializedName("BusType")
        @Expose
        private String busType;
        @SerializedName("TotalSeat")
        @Expose
        private String totalSeat;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("JourneyStartDate")
        @Expose
        private String journeyStartDate;
        @SerializedName("TotalBookedSeat")
        @Expose
        private String totalBookedSeat;
        @SerializedName("JM_Remarks")
        @Expose
        private String jMRemarks;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getJMPNRNO() {
            return jMPNRNO;
        }

        public void setJMPNRNO(String jMPNRNO) {
            this.jMPNRNO = jMPNRNO;
        }

        public String getJMTicketNo() {
            return jMTicketNo;
        }

        public void setJMTicketNo(String jMTicketNo) {
            this.jMTicketNo = jMTicketNo;
        }

        public String getPassengerName() {
            return passengerName;
        }

        public void setPassengerName(String passengerName) {
            this.passengerName = passengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getAMAgentName() {
            return aMAgentName;
        }

        public void setAMAgentName(String aMAgentName) {
            this.aMAgentName = aMAgentName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getFare() {
            return fare;
        }

        public void setFare(String fare) {
            this.fare = fare;
        }

        public String getBMBusNo() {
            return bMBusNo;
        }

        public void setBMBusNo(String bMBusNo) {
            this.bMBusNo = bMBusNo;
        }

        public String getDropOffice() {
            return dropOffice;
        }

        public void setDropOffice(String dropOffice) {
            this.dropOffice = dropOffice;
        }

        public String getBoardingAt() {
            return boardingAt;
        }

        public void setBoardingAt(String boardingAt) {
            this.boardingAt = boardingAt;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMAddress() {
            return cMAddress;
        }

        public void setCMAddress(String cMAddress) {
            this.cMAddress = cMAddress;
        }

        public String getCMCompanyShortName() {
            return cMCompanyShortName;
        }

        public void setCMCompanyShortName(String cMCompanyShortName) {
            this.cMCompanyShortName = cMCompanyShortName;
        }

        public String getBusType() {
            return busType;
        }

        public void setBusType(String busType) {
            this.busType = busType;
        }

        public String getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(String totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getJourneyStartDate() {
            return journeyStartDate;
        }

        public void setJourneyStartDate(String journeyStartDate) {
            this.journeyStartDate = journeyStartDate;
        }

        public String getTotalBookedSeat() {
            return totalBookedSeat;
        }

        public void setTotalBookedSeat(String totalBookedSeat) {
            this.totalBookedSeat = totalBookedSeat;
        }

        public String getJMRemarks() {
            return jMRemarks;
        }

        public void setJMRemarks(String jMRemarks) {
            this.jMRemarks = jMRemarks;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
