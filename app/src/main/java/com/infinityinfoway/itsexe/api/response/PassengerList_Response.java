package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PassengerList_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("BookingDate")
        @Expose
        private String bookingDate;
        @SerializedName("JourneyStartDate")
        @Expose
        private String journeyStartDate;
        @SerializedName("PNRTicketNo")
        @Expose
        private String pNRTicketNo;
        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("CustomerName")
        @Expose
        private String customerName;
        @SerializedName("Phone")
        @Expose
        private String phone;
        @SerializedName("TotalBookedSeat")
        @Expose
        private String totalBookedSeat;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("TotalSeat")
        @Expose
        private String totalSeat;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("FromToCity")
        @Expose
        private String fromToCity;
        @SerializedName("BookedBy")
        @Expose
        private String bookedBy;
        @SerializedName("BookingTypeID")
        @Expose
        private Integer bookingTypeID;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("CAM_IsOnline")
        @Expose
        private String cAMIsOnline;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("DropName")
        @Expose
        private String dropName;
        @SerializedName("PickupTime")
        @Expose
        private String pickupTime;
        @SerializedName("PSI_IsSameDay")
        @Expose
        private Integer pSIIsSameDay;
        @SerializedName("PickupDate")
        @Expose
        private String pickupDate;
        @SerializedName("JourneyStartTime")
        @Expose
        private String journeyStartTime;
        @SerializedName("JourneyCityTime")
        @Expose
        private String journeyCityTime;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("CitySrNo")
        @Expose
        private Integer citySrNo;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("TotalEmptySeat")
        @Expose
        private String totalEmptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("TotalEmptySleeper")
        @Expose
        private String totalEmptySleeper;
        @SerializedName("SubRouteName")
        @Expose
        private String subRouteName;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CM_CompanyShortName")
        @Expose
        private String cMCompanyShortName;
        @SerializedName("CM_CompanyAddress")
        @Expose
        private String cMCompanyAddress;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("DM_DriverName")
        @Expose
        private String dMDriverName;
        @SerializedName("DM_PhoneNo")
        @Expose
        private String dMPhoneNo;
        @SerializedName("ReportUserName")
        @Expose
        private String reportUserName;
        @SerializedName("DM_DriverName1")
        @Expose
        private String dMDriverName1;
        @SerializedName("DM_Conductor1")
        @Expose
        private String dMConductor1;
        @SerializedName("Halt")
        @Expose
        private String halt;
        @SerializedName("Fare")
        @Expose
        private Double fare;
        @SerializedName("GST")
        @Expose
        private Double gST;
        @SerializedName("AgentCmsn")
        @Expose
        private Double agentCmsn;
        @SerializedName("BranchCmsn")
        @Expose
        private Double branchCmsn;
        @SerializedName("Amt")
        @Expose
        private Double amt;
        @SerializedName("PM_PickupID")
        @Expose
        private Integer pMPickupID;
        @SerializedName("Origin")
        @Expose
        private String origin;
        @SerializedName("Destination")
        @Expose
        private String destination;
        @SerializedName("JM_PNRNo")
        @Expose
        private Integer jMPNRNo;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("TicketBookedByUser")
        @Expose
        private String ticketBookedByUser;
        @SerializedName("JP_Gender")
        @Expose
        private String jPGender;
        @SerializedName("JP_Age")
        @Expose
        private String jPAge;
        @SerializedName("PickUpNameList")
        @Expose
        private String pickUpNameList;
        @SerializedName("DropNameList")
        @Expose
        private String dropNameList;

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getJourneyStartDate() {
            return journeyStartDate;
        }

        public void setJourneyStartDate(String journeyStartDate) {
            this.journeyStartDate = journeyStartDate;
        }

        public String getPNRTicketNo() {
            return pNRTicketNo;
        }

        public void setPNRTicketNo(String pNRTicketNo) {
            this.pNRTicketNo = pNRTicketNo;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getTotalBookedSeat() {
            return totalBookedSeat;
        }

        public void setTotalBookedSeat(String totalBookedSeat) {
            this.totalBookedSeat = totalBookedSeat;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(String totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getFromToCity() {
            return fromToCity;
        }

        public void setFromToCity(String fromToCity) {
            this.fromToCity = fromToCity;
        }

        public String getBookedBy() {
            return bookedBy;
        }

        public void setBookedBy(String bookedBy) {
            this.bookedBy = bookedBy;
        }

        public Integer getBookingTypeID() {
            return bookingTypeID;
        }

        public void setBookingTypeID(Integer bookingTypeID) {
            this.bookingTypeID = bookingTypeID;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getCAMIsOnline() {
            return cAMIsOnline;
        }

        public void setCAMIsOnline(String cAMIsOnline) {
            this.cAMIsOnline = cAMIsOnline;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public String getDropName() {
            return dropName;
        }

        public void setDropName(String dropName) {
            this.dropName = dropName;
        }

        public String getPickupTime() {
            return pickupTime;
        }

        public void setPickupTime(String pickupTime) {
            this.pickupTime = pickupTime;
        }

        public Integer getPSIIsSameDay() {
            return pSIIsSameDay;
        }

        public void setPSIIsSameDay(Integer pSIIsSameDay) {
            this.pSIIsSameDay = pSIIsSameDay;
        }

        public String getPickupDate() {
            return pickupDate;
        }

        public void setPickupDate(String pickupDate) {
            this.pickupDate = pickupDate;
        }

        public String getJourneyStartTime() {
            return journeyStartTime;
        }

        public void setJourneyStartTime(String journeyStartTime) {
            this.journeyStartTime = journeyStartTime;
        }

        public String getJourneyCityTime() {
            return journeyCityTime;
        }

        public void setJourneyCityTime(String journeyCityTime) {
            this.journeyCityTime = journeyCityTime;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public Integer getCitySrNo() {
            return citySrNo;
        }

        public void setCitySrNo(Integer citySrNo) {
            this.citySrNo = citySrNo;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public String getTotalEmptySeat() {
            return totalEmptySeat;
        }

        public void setTotalEmptySeat(String totalEmptySeat) {
            this.totalEmptySeat = totalEmptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public String getTotalEmptySleeper() {
            return totalEmptySleeper;
        }

        public void setTotalEmptySleeper(String totalEmptySleeper) {
            this.totalEmptySleeper = totalEmptySleeper;
        }

        public String getSubRouteName() {
            return subRouteName;
        }

        public void setSubRouteName(String subRouteName) {
            this.subRouteName = subRouteName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCMCompanyShortName() {
            return cMCompanyShortName;
        }

        public void setCMCompanyShortName(String cMCompanyShortName) {
            this.cMCompanyShortName = cMCompanyShortName;
        }

        public String getCMCompanyAddress() {
            return cMCompanyAddress;
        }

        public void setCMCompanyAddress(String cMCompanyAddress) {
            this.cMCompanyAddress = cMCompanyAddress;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getDMDriverName() {
            return dMDriverName;
        }

        public void setDMDriverName(String dMDriverName) {
            this.dMDriverName = dMDriverName;
        }

        public String getDMPhoneNo() {
            return dMPhoneNo;
        }

        public void setDMPhoneNo(String dMPhoneNo) {
            this.dMPhoneNo = dMPhoneNo;
        }

        public String getReportUserName() {
            return reportUserName;
        }

        public void setReportUserName(String reportUserName) {
            this.reportUserName = reportUserName;
        }

        public String getDMDriverName1() {
            return dMDriverName1;
        }

        public void setDMDriverName1(String dMDriverName1) {
            this.dMDriverName1 = dMDriverName1;
        }

        public String getDMConductor1() {
            return dMConductor1;
        }

        public void setDMConductor1(String dMConductor1) {
            this.dMConductor1 = dMConductor1;
        }

        public String getHalt() {
            return halt;
        }

        public void setHalt(String halt) {
            this.halt = halt;
        }

        public Double getFare() {
            return fare;
        }

        public void setFare(Double fare) {
            this.fare = fare;
        }

        public Double getGST() {
            return gST;
        }

        public void setGST(Double gST) {
            this.gST = gST;
        }

        public Double getAgentCmsn() {
            return agentCmsn;
        }

        public void setAgentCmsn(Double agentCmsn) {
            this.agentCmsn = agentCmsn;
        }

        public Double getBranchCmsn() {
            return branchCmsn;
        }

        public void setBranchCmsn(Double branchCmsn) {
            this.branchCmsn = branchCmsn;
        }

        public Double getAmt() {
            return amt;
        }

        public void setAmt(Double amt) {
            this.amt = amt;
        }

        public Integer getPMPickupID() {
            return pMPickupID;
        }

        public void setPMPickupID(Integer pMPickupID) {
            this.pMPickupID = pMPickupID;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public Integer getJMPNRNo() {
            return jMPNRNo;
        }

        public void setJMPNRNo(Integer jMPNRNo) {
            this.jMPNRNo = jMPNRNo;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getTicketBookedByUser() {
            return ticketBookedByUser;
        }

        public void setTicketBookedByUser(String ticketBookedByUser) {
            this.ticketBookedByUser = ticketBookedByUser;
        }

        public String getJPGender() {
            return jPGender;
        }

        public void setJPGender(String jPGender) {
            this.jPGender = jPGender;
        }

        public String getJPAge() {
            return jPAge;
        }

        public void setJPAge(String jPAge) {
            this.jPAge = jPAge;
        }

        public String getPickUpNameList() {
            return pickUpNameList;
        }

        public void setPickUpNameList(String pickUpNameList) {
            this.pickUpNameList = pickUpNameList;
        }

        public String getDropNameList() {
            return dropNameList;
        }

        public void setDropNameList(String dropNameList) {
            this.dropNameList = dropNameList;
        }

    }

}
