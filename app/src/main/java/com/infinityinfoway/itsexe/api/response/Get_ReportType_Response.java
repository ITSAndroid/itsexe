package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_ReportType_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("ReportType")
        @Expose
        private List<ReportType> reportType = null;

        public List<ReportType> getReportType() {
            return reportType;
        }

        public void setReportType(List<ReportType> reportType) {
            this.reportType = reportType;
        }

    }

    public class ReportType {

        @SerializedName("ReportID")
        @Expose
        private Integer reportID;
        @SerializedName("ReportName")
        @Expose
        private String reportName;

        public Integer getReportID() {
            return reportID;
        }

        public void setReportID(Integer reportID) {
            this.reportID = reportID;
        }

        public String getReportName() {
            return reportName;
        }

        public void setReportName(String reportName) {
            this.reportName = reportName;
        }

    }
}
