package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Report_AllDayMemo_Request {

    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BookedBy_CM_CompanyID")
    @Expose
    private Integer bookedByCMCompanyID;
    @SerializedName("BranchUserID")
    @Expose
    private Integer branchUserID;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBookedByCMCompanyID() {
        return bookedByCMCompanyID;
    }

    public void setBookedByCMCompanyID(Integer bookedByCMCompanyID) {
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }

    public Integer getBranchUserID() {
        return branchUserID;
    }

    public void setBranchUserID(Integer branchUserID) {
        this.branchUserID = branchUserID;
    }

    public Get_Report_AllDayMemo_Request(Integer companyID, String journeyStartDate, Integer bMBranchID, Integer bookedByCMCompanyID, Integer branchUserID) {
        this.companyID = companyID;
        this.journeyStartDate = journeyStartDate;
        this.bMBranchID = bMBranchID;
        this.bookedByCMCompanyID = bookedByCMCompanyID;
        this.branchUserID = branchUserID;
    }
}
