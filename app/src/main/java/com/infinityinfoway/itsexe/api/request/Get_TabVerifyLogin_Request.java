package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_TabVerifyLogin_Request {

    @SerializedName("Username")
    @Expose
    private String username;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("TBRM_ID")
    @Expose
    private String tBRMID;
    @SerializedName("DeviceID")
    @Expose
    private String deviceID;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getTBRMID() {
        return tBRMID;
    }

    public void setTBRMID(String tBRMID) {
        this.tBRMID = tBRMID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Get_TabVerifyLogin_Request(String username, String password, Integer branchID, Integer companyID, String tBRMID, String deviceID) {
        this.username = username;
        this.password = password;
        this.branchID = branchID;
        this.companyID = companyID;
        this.tBRMID = tBRMID;
        this.deviceID = deviceID;
    }
}
