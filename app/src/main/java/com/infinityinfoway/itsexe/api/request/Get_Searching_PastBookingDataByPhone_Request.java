package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Searching_PastBookingDataByPhone_Request {

    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("Phone1")
    @Expose
    private String phone1;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public Get_Searching_PastBookingDataByPhone_Request(Integer companyID, String phone1) {
        this.companyID = companyID;
        this.phone1 = phone1;
    }
}
