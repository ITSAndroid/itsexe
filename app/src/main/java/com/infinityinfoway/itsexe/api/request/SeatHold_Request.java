package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatHold_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("JM_JourneyStartTime")
    @Expose
    private String jMJourneyStartTime;
    @SerializedName("JM_JourneyCityTime")
    @Expose
    private String jMJourneyCityTime;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("SeatBlockType")
    @Expose
    private String seatBlockType;
    @SerializedName("BlockFromDate")
    @Expose
    private String blockFromDate;
    @SerializedName("BlockToDate")
    @Expose
    private String blockToDate;
    @SerializedName("DayName")
    @Expose
    private String dayName;
    @SerializedName("JM_SeatList")
    @Expose
    private String jMSeatList;
    @SerializedName("TransStatus")
    @Expose
    private String transStatus;
    @SerializedName("TH_Remarks")
    @Expose
    private String tHRemarks;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public String getJMJourneyStartTime() {
        return jMJourneyStartTime;
    }

    public void setJMJourneyStartTime(String jMJourneyStartTime) {
        this.jMJourneyStartTime = jMJourneyStartTime;
    }

    public String getJMJourneyCityTime() {
        return jMJourneyCityTime;
    }

    public void setJMJourneyCityTime(String jMJourneyCityTime) {
        this.jMJourneyCityTime = jMJourneyCityTime;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public String getSeatBlockType() {
        return seatBlockType;
    }

    public void setSeatBlockType(String seatBlockType) {
        this.seatBlockType = seatBlockType;
    }

    public String getBlockFromDate() {
        return blockFromDate;
    }

    public void setBlockFromDate(String blockFromDate) {
        this.blockFromDate = blockFromDate;
    }

    public String getBlockToDate() {
        return blockToDate;
    }

    public void setBlockToDate(String blockToDate) {
        this.blockToDate = blockToDate;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getJMSeatList() {
        return jMSeatList;
    }

    public void setJMSeatList(String jMSeatList) {
        this.jMSeatList = jMSeatList;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getTHRemarks() {
        return tHRemarks;
    }

    public void setTHRemarks(String tHRemarks) {
        this.tHRemarks = tHRemarks;
    }

    public SeatHold_Request(Integer cMCompanyID, String jMJourneyStartDate, Integer rMRouteID, Integer rTRouteTimeID, Integer jMJourneyFrom, Integer jMJourneyTo, String jMJourneyStartTime, String jMJourneyCityTime, Integer bMBranchID, Integer uMUserID, String seatBlockType, String blockFromDate, String blockToDate, String dayName, String jMSeatList, String transStatus, String tHRemarks) {
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.jMJourneyStartTime = jMJourneyStartTime;
        this.jMJourneyCityTime = jMJourneyCityTime;
        this.bMBranchID = bMBranchID;
        this.uMUserID = uMUserID;
        this.seatBlockType = seatBlockType;
        this.blockFromDate = blockFromDate;
        this.blockToDate = blockToDate;
        this.dayName = dayName;
        this.jMSeatList = jMSeatList;
        this.transStatus = transStatus;
        this.tHRemarks = tHRemarks;
    }
}
