package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_InsertTabletBookingRegistration_Request {
    @SerializedName("TBRM_ID")
    @Expose
    private String tBRMID;
    @SerializedName("OSDeviceID")
    @Expose
    private String oSDeviceID;
    @SerializedName("OSVersion")
    @Expose
    private String oSVersion;
    @SerializedName("OSIMEI")
    @Expose
    private String oSIMEI;
    @SerializedName("OSDeviceModel")
    @Expose
    private String oSDeviceModel;
    @SerializedName("OSDeviceMake")
    @Expose
    private String oSDeviceMake;
    @SerializedName("OSDeviceProduct")
    @Expose
    private String oSDeviceProduct;
    @SerializedName("OSDeviceUserName")
    @Expose
    private String oSDeviceUserName;
    @SerializedName("OSMobileNo")
    @Expose
    private String oSMobileNo;
    @SerializedName("OSCompanyName")
    @Expose
    private String oSCompanyName;
    @SerializedName("OSVerifyCall")
    @Expose
    private String oSVerifyCall;
    @SerializedName("VerificationCode")
    @Expose
    private String verificationCode;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("IsActive")
    @Expose
    private Integer isActive;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("IsDemo")
    @Expose
    private Integer isDemo;
    @SerializedName("Request_CM_CompanyID")
    @Expose
    private Integer requestCMCompanyID;
    @SerializedName("VersionCode")
    @Expose
    private Integer versionCode;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("FCMID")
    @Expose
    private String fCMID;
    @SerializedName("VersionName")
    @Expose
    private String versionName;
    @SerializedName("TBRM_BranchName")
    @Expose
    private String tBRMBranchName;

    public String getTBRMID() {
        return tBRMID;
    }

    public void setTBRMID(String tBRMID) {
        this.tBRMID = tBRMID;
    }

    public String getOSDeviceID() {
        return oSDeviceID;
    }

    public void setOSDeviceID(String oSDeviceID) {
        this.oSDeviceID = oSDeviceID;
    }

    public String getOSVersion() {
        return oSVersion;
    }

    public void setOSVersion(String oSVersion) {
        this.oSVersion = oSVersion;
    }

    public String getOSIMEI() {
        return oSIMEI;
    }

    public void setOSIMEI(String oSIMEI) {
        this.oSIMEI = oSIMEI;
    }

    public String getOSDeviceModel() {
        return oSDeviceModel;
    }

    public void setOSDeviceModel(String oSDeviceModel) {
        this.oSDeviceModel = oSDeviceModel;
    }

    public String getOSDeviceMake() {
        return oSDeviceMake;
    }

    public void setOSDeviceMake(String oSDeviceMake) {
        this.oSDeviceMake = oSDeviceMake;
    }

    public String getOSDeviceProduct() {
        return oSDeviceProduct;
    }

    public void setOSDeviceProduct(String oSDeviceProduct) {
        this.oSDeviceProduct = oSDeviceProduct;
    }

    public String getOSDeviceUserName() {
        return oSDeviceUserName;
    }

    public void setOSDeviceUserName(String oSDeviceUserName) {
        this.oSDeviceUserName = oSDeviceUserName;
    }

    public String getOSMobileNo() {
        return oSMobileNo;
    }

    public void setOSMobileNo(String oSMobileNo) {
        this.oSMobileNo = oSMobileNo;
    }

    public String getOSCompanyName() {
        return oSCompanyName;
    }

    public void setOSCompanyName(String oSCompanyName) {
        this.oSCompanyName = oSCompanyName;
    }

    public String getOSVerifyCall() {
        return oSVerifyCall;
    }

    public void setOSVerifyCall(String oSVerifyCall) {
        this.oSVerifyCall = oSVerifyCall;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getIsDemo() {
        return isDemo;
    }

    public void setIsDemo(Integer isDemo) {
        this.isDemo = isDemo;
    }

    public Integer getRequestCMCompanyID() {
        return requestCMCompanyID;
    }

    public void setRequestCMCompanyID(Integer requestCMCompanyID) {
        this.requestCMCompanyID = requestCMCompanyID;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFCMID() {
        return fCMID;
    }

    public void setFCMID(String fCMID) {
        this.fCMID = fCMID;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getTBRMBranchName() {
        return tBRMBranchName;
    }

    public void setTBRMBranchName(String tBRMBranchName) {
        this.tBRMBranchName = tBRMBranchName;
    }

    public Get_InsertTabletBookingRegistration_Request(String tBRMID, String oSDeviceID, String oSVersion, String oSIMEI, String oSDeviceModel, String oSDeviceMake, String oSDeviceProduct, String oSDeviceUserName, String oSMobileNo, String oSCompanyName, String oSVerifyCall, String verificationCode, String userName, Integer isActive, Integer userID, Integer isDemo, Integer requestCMCompanyID, Integer versionCode, String endDate, String fCMID, String versionName,String tBRMBranchName) {
        this.tBRMID = tBRMID;
        this.oSDeviceID = oSDeviceID;
        this.oSVersion = oSVersion;
        this.oSIMEI = oSIMEI;
        this.oSDeviceModel = oSDeviceModel;
        this.oSDeviceMake = oSDeviceMake;
        this.oSDeviceProduct = oSDeviceProduct;
        this.oSDeviceUserName = oSDeviceUserName;
        this.oSMobileNo = oSMobileNo;
        this.oSCompanyName = oSCompanyName;
        this.oSVerifyCall = oSVerifyCall;
        this.verificationCode = verificationCode;
        this.userName = userName;
        this.isActive = isActive;
        this.userID = userID;
        this.isDemo = isDemo;
        this.requestCMCompanyID = requestCMCompanyID;
        this.versionCode = versionCode;
        this.endDate = endDate;
        this.fCMID = fCMID;
        this.versionName = versionName;
        this.tBRMBranchName=tBRMBranchName;
    }
}
