package com.infinityinfoway.itsexe.api.response;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Get_API_EXE_RouteTimeXML_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("RouteTimeXML")
        @Expose
        private List<RouteTimeXML> routeTimeXML = null;

        public List<RouteTimeXML> getRouteTimeXML() {
            return routeTimeXML;
        }

        public void setRouteTimeXML(List<RouteTimeXML> routeTimeXML) {
            this.routeTimeXML = routeTimeXML;
        }

    }

    @Entity(tableName = "Get_API_EXE_RouteTimeXML")
    public static class RouteTimeXML {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        @ColumnInfo(name = "RM_RouteID")
        @SerializedName("RM_RouteID")
        @Expose
        private String rMRouteID;

        @ColumnInfo(name = "CM_CompanyID")
        @SerializedName("CM_CompanyID")
        @Expose
        private String cMCompanyID;

        @ColumnInfo(name = "RM_FromCityID")
        @SerializedName("RM_FromCityID")
        @Expose
        private String rMFromCityID;

        @ColumnInfo(name = "RM_ToCityID")
        @SerializedName("RM_ToCityID")
        @Expose
        private String rMToCityID;

        @ColumnInfo(name = "RM_RouteNameDisplay")
        @SerializedName("RM_RouteNameDisplay")
        @Expose
        private String rMRouteNameDisplay;

        @ColumnInfo(name = "CSAM_CompanyArrangementID")
        @SerializedName("CSAM_CompanyArrangementID")
        @Expose
        private String cSAMCompanyArrangementID;

        @ColumnInfo(name = "RM_RouteCat")
        @SerializedName("RM_RouteCat")
        @Expose
        private String rMRouteCat;

        @ColumnInfo(name = "RTM_RouteTimeMatrixID")
        @SerializedName("RTM_RouteTimeMatrixID")
        @Expose
        private String rTMRouteTimeMatrixID;

        @ColumnInfo(name = "RT_ScheduleCode")
        @SerializedName("RT_ScheduleCode")
        @Expose
        private String rTScheduleCode;

        @ColumnInfo(name = "RTM_FromCityID")
        @SerializedName("RTM_FromCityID")
        @Expose
        private String rTMFromCityID;

        @ColumnInfo(name = "FromCityName")
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;

        @ColumnInfo(name = "RTM_ToCityID")
        @SerializedName("RTM_ToCityID")
        @Expose
        private String rTMToCityID;

        @ColumnInfo(name = "ToCityName")
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;

        @ColumnInfo(name = "TimeCategory")
        @SerializedName("TimeCategory")
        @Expose
        private String timeCategory;

        @ColumnInfo(name = "RT_Count")
        @SerializedName("RT_Count")
        @Expose
        private String rTCount;

        @ColumnInfo(name = "RT_RouteTimeID")
        @SerializedName("RT_RouteTimeID")
        @Expose
        private String rTRouteTimeID;

        @ColumnInfo(name = "RT_RouteTimeCode")
        @SerializedName("RT_RouteTimeCode")
        @Expose
        private String rTRouteTimeCode;

        @ColumnInfo(name = "RT_Time")
        @SerializedName("RT_Time")
        @Expose
        private String rTTime;

        @ColumnInfo(name = "StartTime")
        @SerializedName("StartTime")
        @Expose
        private String startTime;

        @ColumnInfo(name = "RTM_DisplayMin")
        @SerializedName("RTM_DisplayMin")
        @Expose
        private String rTMDisplayMin;

        @ColumnInfo(name = "PSI_IsSameDay")
        @SerializedName("PSI_IsSameDay")
        @Expose
        private String pSIIsSameDay;

        @ColumnInfo(name = "RT_IsFareCaption")
        @SerializedName("RT_IsFareCaption")
        @Expose
        private String rTIsFareCaption;

        @ColumnInfo(name = "RT_FromDate")
        @SerializedName("RT_FromDate")
        @Expose
        private Date rTFromDate;

        @ColumnInfo(name = "RT_ToDate")
        @SerializedName("RT_ToDate")
        @Expose
        private Date rTToDate;

        @ColumnInfo(name = "RT_FromDate_Int")
        @SerializedName("RT_FromDate_Int")
        @Expose
        private String rTFromDateInt;

        @ColumnInfo(name = "RT_ToDate_Int")
        @SerializedName("RT_ToDate_Int")
        @Expose
        private String rTToDateInt;

        @ColumnInfo(name = "CM_CompanyCode")
        @SerializedName("CM_CompanyCode")
        @Expose
        private String cMCompanyCode;

        @ColumnInfo(name = "RT_BusCategoryID")
        @SerializedName("RT_BusCategoryID")
        @Expose
        private String rTBusCategoryID;

        @ColumnInfo(name = "RM_RouteName")
        @SerializedName("RM_RouteName")
        @Expose
        private String rMRouteName;

        @ColumnInfo(name = "RT_IsServiceTax")
        @SerializedName("RT_IsServiceTax")
        @Expose
        private String rTIsServiceTax;

        @ColumnInfo(name = "RPCM_Hours")
        @SerializedName("RPCM_Hours")
        @Expose
        private int rPCMHours;

        @ColumnInfo(name = "RPCM_Min")
        @SerializedName("RPCM_Min")
        @Expose
        private int rPCMMin;

        @ColumnInfo(name = "RT_RouteType")
        @SerializedName("RT_RouteType")
        @Expose
        private String rTRouteType;

        @ColumnInfo(name = "BTM_BusTypeID")
        @SerializedName("BTM_BusTypeID")
        @Expose
        private String bTMBusTypeID;

        @ColumnInfo(name = "BTM_BusType")
        @SerializedName("BTM_BusType")
        @Expose
        private String bTMBusType;

        @ColumnInfo(name = "BusFilter")
        @SerializedName("BusFilter")
        @Expose
        private String busFilter;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(String rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public String getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(String cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getRMFromCityID() {
            return rMFromCityID;
        }

        public void setRMFromCityID(String rMFromCityID) {
            this.rMFromCityID = rMFromCityID;
        }

        public String getRMToCityID() {
            return rMToCityID;
        }

        public void setRMToCityID(String rMToCityID) {
            this.rMToCityID = rMToCityID;
        }

        public String getRMRouteNameDisplay() {
            return rMRouteNameDisplay;
        }

        public void setRMRouteNameDisplay(String rMRouteNameDisplay) {
            this.rMRouteNameDisplay = rMRouteNameDisplay;
        }

        public String getCSAMCompanyArrangementID() {
            return cSAMCompanyArrangementID;
        }

        public void setCSAMCompanyArrangementID(String cSAMCompanyArrangementID) {
            this.cSAMCompanyArrangementID = cSAMCompanyArrangementID;
        }

        public String getRMRouteCat() {
            return rMRouteCat;
        }

        public void setRMRouteCat(String rMRouteCat) {
            this.rMRouteCat = rMRouteCat;
        }

        public String getRTMRouteTimeMatrixID() {
            return rTMRouteTimeMatrixID;
        }

        public void setRTMRouteTimeMatrixID(String rTMRouteTimeMatrixID) {
            this.rTMRouteTimeMatrixID = rTMRouteTimeMatrixID;
        }

        public String getRTScheduleCode() {
            return rTScheduleCode;
        }

        public void setRTScheduleCode(String rTScheduleCode) {
            this.rTScheduleCode = rTScheduleCode;
        }

        public String getRTMFromCityID() {
            return rTMFromCityID;
        }

        public void setRTMFromCityID(String rTMFromCityID) {
            this.rTMFromCityID = rTMFromCityID;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getRTMToCityID() {
            return rTMToCityID;
        }

        public void setRTMToCityID(String rTMToCityID) {
            this.rTMToCityID = rTMToCityID;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getTimeCategory() {
            return timeCategory;
        }

        public void setTimeCategory(String timeCategory) {
            this.timeCategory = timeCategory;
        }

        public String getRTCount() {
            return rTCount;
        }

        public void setRTCount(String rTCount) {
            this.rTCount = rTCount;
        }

        public String getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(String rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public String getRTRouteTimeCode() {
            return rTRouteTimeCode;
        }

        public void setRTRouteTimeCode(String rTRouteTimeCode) {
            this.rTRouteTimeCode = rTRouteTimeCode;
        }

        public String getRTTime() {
            return rTTime;
        }

        public void setRTTime(String rTTime) {
            this.rTTime = rTTime;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getRTMDisplayMin() {
            return rTMDisplayMin;
        }

        public void setRTMDisplayMin(String rTMDisplayMin) {
            this.rTMDisplayMin = rTMDisplayMin;
        }

        public String getPSIIsSameDay() {
            return pSIIsSameDay;
        }

        public void setPSIIsSameDay(String pSIIsSameDay) {
            this.pSIIsSameDay = pSIIsSameDay;
        }

        public String getRTIsFareCaption() {
            return rTIsFareCaption;
        }

        public void setRTIsFareCaption(String rTIsFareCaption) {
            this.rTIsFareCaption = rTIsFareCaption;
        }

        public Date getRTFromDate() {
            return rTFromDate;
        }

        public void setRTFromDate(Date rTFromDate) {
            this.rTFromDate = rTFromDate;
        }

        public Date getRTToDate() {
            return rTToDate;
        }

        public void setRTToDate(Date rTToDate) {
            this.rTToDate = rTToDate;
        }

        public String getRTFromDateInt() {
            return rTFromDateInt;
        }

        public void setRTFromDateInt(String rTFromDateInt) {
            this.rTFromDateInt = rTFromDateInt;
        }

        public String getRTToDateInt() {
            return rTToDateInt;
        }

        public void setRTToDateInt(String rTToDateInt) {
            this.rTToDateInt = rTToDateInt;
        }

        public String getCMCompanyCode() {
            return cMCompanyCode;
        }

        public void setCMCompanyCode(String cMCompanyCode) {
            this.cMCompanyCode = cMCompanyCode;
        }

        public String getRTBusCategoryID() {
            return rTBusCategoryID;
        }

        public void setRTBusCategoryID(String rTBusCategoryID) {
            this.rTBusCategoryID = rTBusCategoryID;
        }

        public String getRMRouteName() {
            return rMRouteName;
        }

        public void setRMRouteName(String rMRouteName) {
            this.rMRouteName = rMRouteName;
        }

        public String getRTIsServiceTax() {
            return rTIsServiceTax;
        }

        public void setRTIsServiceTax(String rTIsServiceTax) {
            this.rTIsServiceTax = rTIsServiceTax;
        }

        public int getRPCMHours() {
            return rPCMHours;
        }

        public void setRPCMHours(int rPCMHours) {
            this.rPCMHours = rPCMHours;
        }

        public int getRPCMMin() {
            return rPCMMin;
        }

        public void setRPCMMin(int rPCMMin) {
            this.rPCMMin = rPCMMin;
        }

        public String getRTRouteType() {
            return rTRouteType;
        }

        public void setRTRouteType(String rTRouteType) {
            this.rTRouteType = rTRouteType;
        }

        public String getBTMBusTypeID() {
            return bTMBusTypeID;
        }

        public void setBTMBusTypeID(String bTMBusTypeID) {
            this.bTMBusTypeID = bTMBusTypeID;
        }

        public String getBTMBusType() {
            return bTMBusType;
        }

        public void setBTMBusType(String bTMBusType) {
            this.bTMBusType = bTMBusType;
        }

        public String getBusFilter() {
            return busFilter;
        }

        public void setBusFilter(String busFilter) {
            this.busFilter = busFilter;
        }

    }

}
