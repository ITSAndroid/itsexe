package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class checkVersionInfo_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("android_id")
        @Expose
        private String androidId;
        @SerializedName("version_name")
        @Expose
        private String versionName;
        @SerializedName("version_code")
        @Expose
        private Integer versionCode;
        @SerializedName("update_severity")
        @Expose
        private Integer updateSeverity;
        @SerializedName("Apk_url")
        @Expose
        private String apkUrl;

        public String getAndroidId() {
            return androidId;
        }

        public void setAndroidId(String androidId) {
            this.androidId = androidId;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public Integer getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(Integer versionCode) {
            this.versionCode = versionCode;
        }

        public Integer getUpdateSeverity() {
            return updateSeverity;
        }

        public void setUpdateSeverity(Integer updateSeverity) {
            this.updateSeverity = updateSeverity;
        }

        public String getApkUrl() {
            return apkUrl;
        }

        public void setApkUrl(String apkUrl) {
            this.apkUrl = apkUrl;
        }

    }

}
