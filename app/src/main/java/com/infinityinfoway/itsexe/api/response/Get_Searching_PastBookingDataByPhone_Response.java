package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Searching_PastBookingDataByPhone_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Searching_PastBookingDataByPhone")
        @Expose
        private List<SearchingPastBookingDataByPhone> searchingPastBookingDataByPhone = null;

        public List<SearchingPastBookingDataByPhone> getSearchingPastBookingDataByPhone() {
            return searchingPastBookingDataByPhone;
        }

        public void setSearchingPastBookingDataByPhone(List<SearchingPastBookingDataByPhone> searchingPastBookingDataByPhone) {
            this.searchingPastBookingDataByPhone = searchingPastBookingDataByPhone;
        }

    }

    public class SearchingPastBookingDataByPhone {

        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("PNRNO")
        @Expose
        private String pNRNO;
        @SerializedName("PassengerName")
        @Expose
        private String passengerName;
        @SerializedName("PhoneNo")
        @Expose
        private String phoneNo;
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("CityTime")
        @Expose
        private String cityTime;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("default_name")
        @Expose
        private String defaultName;
        @SerializedName("BookingDate")
        @Expose
        private String bookingDate;
        @SerializedName("RouteNameDisplay")
        @Expose
        private String routeNameDisplay;
        @SerializedName("JourneyFromDate")
        @Expose
        private String journeyFromDate;
        @SerializedName("JourneyToDate")
        @Expose
        private String journeyToDate;
        @SerializedName("Status")
        @Expose
        private Integer status;

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(String pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getPassengerName() {
            return passengerName;
        }

        public void setPassengerName(String passengerName) {
            this.passengerName = passengerName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getCityTime() {
            return cityTime;
        }

        public void setCityTime(String cityTime) {
            this.cityTime = cityTime;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getDefaultName() {
            return defaultName;
        }

        public void setDefaultName(String defaultName) {
            this.defaultName = defaultName;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getRouteNameDisplay() {
            return routeNameDisplay;
        }

        public void setRouteNameDisplay(String routeNameDisplay) {
            this.routeNameDisplay = routeNameDisplay;
        }

        public String getJourneyFromDate() {
            return journeyFromDate;
        }

        public void setJourneyFromDate(String journeyFromDate) {
            this.journeyFromDate = journeyFromDate;
        }

        public String getJourneyToDate() {
            return journeyToDate;
        }

        public void setJourneyToDate(String journeyToDate) {
            this.journeyToDate = journeyToDate;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }


}
