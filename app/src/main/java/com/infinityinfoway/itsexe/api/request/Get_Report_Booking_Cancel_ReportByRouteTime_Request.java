package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Report_Booking_Cancel_ReportByRouteTime_Request {
    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;
    @SerializedName("RouteID")
    @Expose
    private Integer routeID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("ArrangementID")
    @Expose
    private Integer arrangementID;
    @SerializedName("JourneyStartTime")
    @Expose
    private String journeyStartTime;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("CityID")
    @Expose
    private Integer cityID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("JourneyFrom")
    @Expose
    private Integer journeyFrom;
    @SerializedName("BookedBy_CM_CompanyID")
    @Expose
    private Integer bookedByCMCompanyID;

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public Integer getRouteID() {
        return routeID;
    }

    public void setRouteID(Integer routeID) {
        this.routeID = routeID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public Integer getArrangementID() {
        return arrangementID;
    }

    public void setArrangementID(Integer arrangementID) {
        this.arrangementID = arrangementID;
    }

    public String getJourneyStartTime() {
        return journeyStartTime;
    }

    public void setJourneyStartTime(String journeyStartTime) {
        this.journeyStartTime = journeyStartTime;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getJourneyFrom() {
        return journeyFrom;
    }

    public void setJourneyFrom(Integer journeyFrom) {
        this.journeyFrom = journeyFrom;
    }

    public Integer getBookedByCMCompanyID() {
        return bookedByCMCompanyID;
    }

    public void setBookedByCMCompanyID(Integer bookedByCMCompanyID) {
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }

    public Get_Report_Booking_Cancel_ReportByRouteTime_Request(String journeyStartDate, Integer routeID, Integer rTTime, Integer arrangementID, String journeyStartTime, Integer companyID, Integer cityID, Integer uMUserID, Integer journeyFrom, Integer bookedByCMCompanyID) {
        this.journeyStartDate = journeyStartDate;
        this.routeID = routeID;
        this.rTTime = rTTime;
        this.arrangementID = arrangementID;
        this.journeyStartTime = journeyStartTime;
        this.companyID = companyID;
        this.cityID = cityID;
        this.uMUserID = uMUserID;
        this.journeyFrom = journeyFrom;
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }
}
