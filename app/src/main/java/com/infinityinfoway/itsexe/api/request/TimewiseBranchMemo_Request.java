package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimewiseBranchMemo_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("JM_JourneyStartTime")
    @Expose
    private String jMJourneyStartTime;
    @SerializedName("BAM_ArrangementID")
    @Expose
    private Integer bAMArrangementID;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchUserID")
    @Expose
    private Integer bMBranchUserID;
    @SerializedName("PSI_IsSameDay")
    @Expose
    private String pSIIsSameDay;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getJMJourneyStartTime() {
        return jMJourneyStartTime;
    }

    public void setJMJourneyStartTime(String jMJourneyStartTime) {
        this.jMJourneyStartTime = jMJourneyStartTime;
    }

    public Integer getBAMArrangementID() {
        return bAMArrangementID;
    }

    public void setBAMArrangementID(Integer bAMArrangementID) {
        this.bAMArrangementID = bAMArrangementID;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchUserID() {
        return bMBranchUserID;
    }

    public void setBMBranchUserID(Integer bMBranchUserID) {
        this.bMBranchUserID = bMBranchUserID;
    }

    public String getPSIIsSameDay() {
        return pSIIsSameDay;
    }

    public void setPSIIsSameDay(String pSIIsSameDay) {
        this.pSIIsSameDay = pSIIsSameDay;
    }

    public TimewiseBranchMemo_Request(Integer cMCompanyID, Integer bMBranchID, String jMJourneyStartDate, Integer rMRouteID, Integer rTTime, String jMJourneyStartTime, Integer bAMArrangementID, Integer jMJourneyFrom, Integer jMBookedByCMCompanyID, Integer bMBranchUserID, String pSIIsSameDay) {
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.jMJourneyStartTime = jMJourneyStartTime;
        this.bAMArrangementID = bAMArrangementID;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchUserID = bMBranchUserID;
        this.pSIIsSameDay = pSIIsSameDay;
    }
}
