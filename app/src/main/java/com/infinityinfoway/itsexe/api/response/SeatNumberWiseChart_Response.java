package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SeatNumberWiseChart_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Details")
        @Expose
        private List<Detail> details = null;
        @SerializedName("Summary")
        @Expose
        private List<Summary> summary = null;

        public List<Detail> getDetails() {
            return details;
        }

        public void setDetails(List<Detail> details) {
            this.details = details;
        }

        public List<Summary> getSummary() {
            return summary;
        }

        public void setSummary(List<Summary> summary) {
            this.summary = summary;
        }

    }

    public class Detail {

        @SerializedName("PNRNO")
        @Expose
        private String pNRNO;
        @SerializedName("TicketNo")
        @Expose
        private String ticketNo;
        @SerializedName("BookingTypeName")
        @Expose
        private String bookingTypeName;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("CustomerName")
        @Expose
        private String customerName;
        @SerializedName("Phone")
        @Expose
        private String phone;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("BookingTypeID")
        @Expose
        private Integer bookingTypeID;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("JourneyStartTime")
        @Expose
        private String journeyStartTime;
        @SerializedName("JourneyCityTime")
        @Expose
        private String journeyCityTime;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("TotalEmptySeat")
        @Expose
        private Integer totalEmptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("TotalEmptySleeper")
        @Expose
        private Integer totalEmptySleeper;
        @SerializedName("RemarkDetail")
        @Expose
        private String remarkDetail;
        @SerializedName("FromCitySeat")
        @Expose
        private String fromCitySeat;
        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("BookbyUser")
        @Expose
        private String bookbyUser;
        @SerializedName("SeatType")
        @Expose
        private Integer seatType;
        @SerializedName("IsShowHorizoltalSeatList")
        @Expose
        private Integer isShowHorizoltalSeatList;

        public String getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(String pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getTicketNo() {
            return ticketNo;
        }

        public void setTicketNo(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        public String getBookingTypeName() {
            return bookingTypeName;
        }

        public void setBookingTypeName(String bookingTypeName) {
            this.bookingTypeName = bookingTypeName;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public Integer getBookingTypeID() {
            return bookingTypeID;
        }

        public void setBookingTypeID(Integer bookingTypeID) {
            this.bookingTypeID = bookingTypeID;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getJourneyStartTime() {
            return journeyStartTime;
        }

        public void setJourneyStartTime(String journeyStartTime) {
            this.journeyStartTime = journeyStartTime;
        }

        public String getJourneyCityTime() {
            return journeyCityTime;
        }

        public void setJourneyCityTime(String journeyCityTime) {
            this.journeyCityTime = journeyCityTime;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public Integer getTotalEmptySeat() {
            return totalEmptySeat;
        }

        public void setTotalEmptySeat(Integer totalEmptySeat) {
            this.totalEmptySeat = totalEmptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public Integer getTotalEmptySleeper() {
            return totalEmptySleeper;
        }

        public void setTotalEmptySleeper(Integer totalEmptySleeper) {
            this.totalEmptySleeper = totalEmptySleeper;
        }

        public String getRemarkDetail() {
            return remarkDetail;
        }

        public void setRemarkDetail(String remarkDetail) {
            this.remarkDetail = remarkDetail;
        }

        public String getFromCitySeat() {
            return fromCitySeat;
        }

        public void setFromCitySeat(String fromCitySeat) {
            this.fromCitySeat = fromCitySeat;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getBookbyUser() {
            return bookbyUser;
        }

        public void setBookbyUser(String bookbyUser) {
            this.bookbyUser = bookbyUser;
        }

        public Integer getSeatType() {
            return seatType;
        }

        public void setSeatType(Integer seatType) {
            this.seatType = seatType;
        }

        public Integer getIsShowHorizoltalSeatList() {
            return isShowHorizoltalSeatList;
        }

        public void setIsShowHorizoltalSeatList(Integer isShowHorizoltalSeatList) {
            this.isShowHorizoltalSeatList = isShowHorizoltalSeatList;
        }

    }

    public class Summary {

        @SerializedName("SubRoute")
        @Expose
        private String subRoute;
        @SerializedName("TotalPassengers")
        @Expose
        private Integer totalPassengers;
        @SerializedName("RMC_SrNo")
        @Expose
        private Integer rMCSrNo;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;

        public String getSubRoute() {
            return subRoute;
        }

        public void setSubRoute(String subRoute) {
            this.subRoute = subRoute;
        }

        public Integer getTotalPassengers() {
            return totalPassengers;
        }

        public void setTotalPassengers(Integer totalPassengers) {
            this.totalPassengers = totalPassengers;
        }

        public Integer getRMCSrNo() {
            return rMCSrNo;
        }

        public void setRMCSrNo(Integer rMCSrNo) {
            this.rMCSrNo = rMCSrNo;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

    }

}
