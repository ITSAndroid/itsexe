package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimewiseBranchMemo_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("BaseFare")
        @Expose
        private Double baseFare;
        @SerializedName("ServiceTax")
        @Expose
        private Double serviceTax;
        @SerializedName("Amount")
        @Expose
        private Double amount;
        @SerializedName("TotalSeat")
        @Expose
        private String totalSeat;
        @SerializedName("TotalAmount")
        @Expose
        private Double totalAmount;
        @SerializedName("Commission")
        @Expose
        private String commission;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("BranchAddress")
        @Expose
        private String branchAddress;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("JDate")
        @Expose
        private String jDate;
        @SerializedName("JTime")
        @Expose
        private String jTime;
        @SerializedName("TotalCollection")
        @Expose
        private Double totalCollection;
        @SerializedName("NetSeat")
        @Expose
        private Integer netSeat;
        @SerializedName("CommissionAmount")
        @Expose
        private Double commissionAmount;
        @SerializedName("NetCollection")
        @Expose
        private Double netCollection;
        @SerializedName("GroupHeader")
        @Expose
        private String groupHeader;
        @SerializedName("TotalBaseFare")
        @Expose
        private Double totalBaseFare;
        @SerializedName("TotalServiceTax")
        @Expose
        private Double totalServiceTax;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public Double getBaseFare() {
            return baseFare;
        }

        public void setBaseFare(Double baseFare) {
            this.baseFare = baseFare;
        }

        public Double getServiceTax() {
            return serviceTax;
        }

        public void setServiceTax(Double serviceTax) {
            this.serviceTax = serviceTax;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public String getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(String totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getBranchAddress() {
            return branchAddress;
        }

        public void setBranchAddress(String branchAddress) {
            this.branchAddress = branchAddress;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getJTime() {
            return jTime;
        }

        public void setJTime(String jTime) {
            this.jTime = jTime;
        }

        public Double getTotalCollection() {
            return totalCollection;
        }

        public void setTotalCollection(Double totalCollection) {
            this.totalCollection = totalCollection;
        }

        public Integer getNetSeat() {
            return netSeat;
        }

        public void setNetSeat(Integer netSeat) {
            this.netSeat = netSeat;
        }

        public Double getCommissionAmount() {
            return commissionAmount;
        }

        public void setCommissionAmount(Double commissionAmount) {
            this.commissionAmount = commissionAmount;
        }

        public Double getNetCollection() {
            return netCollection;
        }

        public void setNetCollection(Double netCollection) {
            this.netCollection = netCollection;
        }

        public String getGroupHeader() {
            return groupHeader;
        }

        public void setGroupHeader(String groupHeader) {
            this.groupHeader = groupHeader;
        }

        public Double getTotalBaseFare() {
            return totalBaseFare;
        }

        public void setTotalBaseFare(Double totalBaseFare) {
            this.totalBaseFare = totalBaseFare;
        }

        public Double getTotalServiceTax() {
            return totalServiceTax;
        }

        public void setTotalServiceTax(Double totalServiceTax) {
            this.totalServiceTax = totalServiceTax;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }


}
