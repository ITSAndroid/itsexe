package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SeatAllocationChart_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("SubRouteName")
        @Expose
        private String subRouteName;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Allocation")
        @Expose
        private String allocation;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("StartTime")
        @Expose
        private String startTime;
        @SerializedName("ByWhom")
        @Expose
        private String byWhom;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("TotalEmptySeat")
        @Expose
        private Integer totalEmptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("TotalEmptySleeper")
        @Expose
        private Integer totalEmptySleeper;
        @SerializedName("RemarkDetail")
        @Expose
        private String remarkDetail;
        @SerializedName("BM_BUSNO")
        @Expose
        private String bMBUSNO;
        @SerializedName("DriverDetail")
        @Expose
        private String driverDetail;
        @SerializedName("ConductorDetail")
        @Expose
        private String conductorDetail;
        @SerializedName("PickupManDetail")
        @Expose
        private String pickupManDetail;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("BaseFare")
        @Expose
        private Double baseFare;
        @SerializedName("GST")
        @Expose
        private Double gST;
        @SerializedName("Amount")
        @Expose
        private Double amount;

        public String getSubRouteName() {
            return subRouteName;
        }

        public void setSubRouteName(String subRouteName) {
            this.subRouteName = subRouteName;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getAllocation() {
            return allocation;
        }

        public void setAllocation(String allocation) {
            this.allocation = allocation;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getByWhom() {
            return byWhom;
        }

        public void setByWhom(String byWhom) {
            this.byWhom = byWhom;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public Integer getTotalEmptySeat() {
            return totalEmptySeat;
        }

        public void setTotalEmptySeat(Integer totalEmptySeat) {
            this.totalEmptySeat = totalEmptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public Integer getTotalEmptySleeper() {
            return totalEmptySleeper;
        }

        public void setTotalEmptySleeper(Integer totalEmptySleeper) {
            this.totalEmptySleeper = totalEmptySleeper;
        }

        public String getRemarkDetail() {
            return remarkDetail;
        }

        public void setRemarkDetail(String remarkDetail) {
            this.remarkDetail = remarkDetail;
        }

        public String getBMBUSNO() {
            return bMBUSNO;
        }

        public void setBMBUSNO(String bMBUSNO) {
            this.bMBUSNO = bMBUSNO;
        }

        public String getDriverDetail() {
            return driverDetail;
        }

        public void setDriverDetail(String driverDetail) {
            this.driverDetail = driverDetail;
        }

        public String getConductorDetail() {
            return conductorDetail;
        }

        public void setConductorDetail(String conductorDetail) {
            this.conductorDetail = conductorDetail;
        }

        public String getPickupManDetail() {
            return pickupManDetail;
        }

        public void setPickupManDetail(String pickupManDetail) {
            this.pickupManDetail = pickupManDetail;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public Double getBaseFare() {
            return baseFare;
        }

        public void setBaseFare(Double baseFare) {
            this.baseFare = baseFare;
        }

        public Double getGST() {
            return gST;
        }

        public void setGST(Double gST) {
            this.gST = gST;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

    }


}
