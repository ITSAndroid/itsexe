package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class UpdatePhoneBookingTime_Request {
    @Expose
    @SerializedName("PhoneMin")
    private int PhoneMin;
    @Expose
    @SerializedName("PhoneHour")
    private int PhoneHour;
    @Expose
    @SerializedName("CM_CompanyID")
    private int CM_CompanyID;
    @Expose
    @SerializedName("JM_PNRNO")
    private String JM_PNRNO;

    public int getPhoneMin() {
        return PhoneMin;
    }

    public void setPhoneMin(int PhoneMin) {
        this.PhoneMin = PhoneMin;
    }

    public int getPhoneHour() {
        return PhoneHour;
    }

    public void setPhoneHour(int PhoneHour) {
        this.PhoneHour = PhoneHour;
    }

    public int getCM_CompanyID() {
        return CM_CompanyID;
    }

    public void setCM_CompanyID(int CM_CompanyID) {
        this.CM_CompanyID = CM_CompanyID;
    }

    public String getJM_PNRNO() {
        return JM_PNRNO;
    }

    public void setJM_PNRNO(String JM_PNRNO) {
        this.JM_PNRNO = JM_PNRNO;
    }
}
