package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Searching_GPSLastLocationByBusNo_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Searching_GPSLastLocationByBusNo")
        @Expose
        private List<SearchingGPSLastLocationByBusNo> searchingGPSLastLocationByBusNo = null;

        public List<SearchingGPSLastLocationByBusNo> getSearchingGPSLastLocationByBusNo() {
            return searchingGPSLastLocationByBusNo;
        }

        public void setSearchingGPSLastLocationByBusNo(List<SearchingGPSLastLocationByBusNo> searchingGPSLastLocationByBusNo) {
            this.searchingGPSLastLocationByBusNo = searchingGPSLastLocationByBusNo;
        }

    }

    public class SearchingGPSLastLocationByBusNo {

        @SerializedName("BusID")
        @Expose
        private Integer busID;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("BusMapURL")
        @Expose
        private Integer busMapURL;
        @SerializedName("GCM_ID")
        @Expose
        private Integer gCMID;
        @SerializedName("Status")
        @Expose
        private Integer status;

        public Integer getBusID() {
            return busID;
        }

        public void setBusID(Integer busID) {
            this.busID = busID;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public Integer getBusMapURL() {
            return busMapURL;
        }

        public void setBusMapURL(Integer busMapURL) {
            this.busMapURL = busMapURL;
        }

        public Integer getGCMID() {
            return gCMID;
        }

        public void setGCMID(Integer gCMID) {
            this.gCMID = gCMID;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }




}
