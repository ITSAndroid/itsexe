package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_TabVerifyLogin_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("LoginDetails")
        @Expose
        private List<LoginDetail> loginDetails = null;

        public List<LoginDetail> getLoginDetails() {
            return loginDetails;
        }

        public void setLoginDetails(List<LoginDetail> loginDetails) {
            this.loginDetails = loginDetails;
        }

    }

    public class LoginDetail {

        @SerializedName("BUM_UserName")
        @Expose
        private String bUMUserName;
        @SerializedName("BUM_Password")
        @Expose
        private String bUMPassword;
        @SerializedName("BUM_FullName")
        @Expose
        private String bUMFullName;
        @SerializedName("BUM_BranchUserID")
        @Expose
        private Integer bUMBranchUserID;
        @SerializedName("BM_BranchCode")
        @Expose
        private String bMBranchCode;
        @SerializedName("CM_Email")
        @Expose
        private String cMEmail;
        @SerializedName("BM_RequiredPhone")
        @Expose
        private Integer bMRequiredPhone;
        @SerializedName("BM_IsPrepaid")
        @Expose
        private Integer bMIsPrepaid;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("BUM_PasswordChangeDate")
        @Expose
        private String bUMPasswordChangeDate;
        @SerializedName("BM_IsBranchTicketPrint")
        @Expose
        private Integer bMIsBranchTicketPrint;
        @SerializedName("ServerDateTime")
        @Expose
        private String serverDateTime;
        @SerializedName("GA_XmlSourceMemory")
        @Expose
        private Integer gAXmlSourceMemory;
        @SerializedName("IsStoreOnLocalPC")
        @Expose
        private Integer isStoreOnLocalPC;
        @SerializedName("BM_IsShowParcel")
        @Expose
        private Integer bMIsShowParcel;
        @SerializedName("CargoRedirect")
        @Expose
        private Integer cargoRedirect;
        @SerializedName("ISAppOpen")
        @Expose
        private Integer iSAppOpen;
        @SerializedName("BM_IsAskReprintReason")
        @Expose
        private Integer bMIsAskReprintReason;
        @SerializedName("BM_IsAskModifyReason")
        @Expose
        private Integer bMIsAskModifyReason;
        @SerializedName("BM_IsAskCancelReason")
        @Expose
        private Integer bMIsAskCancelReason;
        @SerializedName("CM_CityID")
        @Expose
        private Integer cMCityID;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;
        @SerializedName("OTP")
        @Expose
        private String oTP;
        @SerializedName("SMSString")
        @Expose
        private String sMSString;
        @SerializedName("BM_AskOTPonPhoneBooking")
        @Expose
        private Integer bMAskOTPonPhoneBooking;
        @SerializedName("OS_SetDefaultBrowser")
        @Expose
        private String oSSetDefaultBrowser;
        @SerializedName("BM_IsAskNonReportedReason")
        @Expose
        private Integer bMIsAskNonReportedReason;
        @SerializedName("IsAdminlogin")
        @Expose
        private Integer isAdminlogin;

        public String getBUMUserName() {
            return bUMUserName;
        }

        public void setBUMUserName(String bUMUserName) {
            this.bUMUserName = bUMUserName;
        }

        public String getBUMPassword() {
            return bUMPassword;
        }

        public void setBUMPassword(String bUMPassword) {
            this.bUMPassword = bUMPassword;
        }

        public String getBUMFullName() {
            return bUMFullName;
        }

        public void setBUMFullName(String bUMFullName) {
            this.bUMFullName = bUMFullName;
        }

        public Integer getBUMBranchUserID() {
            return bUMBranchUserID;
        }

        public void setBUMBranchUserID(Integer bUMBranchUserID) {
            this.bUMBranchUserID = bUMBranchUserID;
        }

        public String getBMBranchCode() {
            return bMBranchCode;
        }

        public void setBMBranchCode(String bMBranchCode) {
            this.bMBranchCode = bMBranchCode;
        }

        public String getCMEmail() {
            return cMEmail;
        }

        public void setCMEmail(String cMEmail) {
            this.cMEmail = cMEmail;
        }

        public Integer getBMRequiredPhone() {
            return bMRequiredPhone;
        }

        public void setBMRequiredPhone(Integer bMRequiredPhone) {
            this.bMRequiredPhone = bMRequiredPhone;
        }

        public Integer getBMIsPrepaid() {
            return bMIsPrepaid;
        }

        public void setBMIsPrepaid(Integer bMIsPrepaid) {
            this.bMIsPrepaid = bMIsPrepaid;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public String getBUMPasswordChangeDate() {
            return bUMPasswordChangeDate;
        }

        public void setBUMPasswordChangeDate(String bUMPasswordChangeDate) {
            this.bUMPasswordChangeDate = bUMPasswordChangeDate;
        }

        public Integer getBMIsBranchTicketPrint() {
            return bMIsBranchTicketPrint;
        }

        public void setBMIsBranchTicketPrint(Integer bMIsBranchTicketPrint) {
            this.bMIsBranchTicketPrint = bMIsBranchTicketPrint;
        }

        public String getServerDateTime() {
            return serverDateTime;
        }

        public void setServerDateTime(String serverDateTime) {
            this.serverDateTime = serverDateTime;
        }

        public Integer getGAXmlSourceMemory() {
            return gAXmlSourceMemory;
        }

        public void setGAXmlSourceMemory(Integer gAXmlSourceMemory) {
            this.gAXmlSourceMemory = gAXmlSourceMemory;
        }

        public Integer getIsStoreOnLocalPC() {
            return isStoreOnLocalPC;
        }

        public void setIsStoreOnLocalPC(Integer isStoreOnLocalPC) {
            this.isStoreOnLocalPC = isStoreOnLocalPC;
        }

        public Integer getBMIsShowParcel() {
            return bMIsShowParcel;
        }

        public void setBMIsShowParcel(Integer bMIsShowParcel) {
            this.bMIsShowParcel = bMIsShowParcel;
        }

        public Integer getCargoRedirect() {
            return cargoRedirect;
        }

        public void setCargoRedirect(Integer cargoRedirect) {
            this.cargoRedirect = cargoRedirect;
        }

        public Integer getISAppOpen() {
            return iSAppOpen;
        }

        public void setISAppOpen(Integer iSAppOpen) {
            this.iSAppOpen = iSAppOpen;
        }

        public Integer getBMIsAskReprintReason() {
            return bMIsAskReprintReason;
        }

        public void setBMIsAskReprintReason(Integer bMIsAskReprintReason) {
            this.bMIsAskReprintReason = bMIsAskReprintReason;
        }

        public Integer getBMIsAskModifyReason() {
            return bMIsAskModifyReason;
        }

        public void setBMIsAskModifyReason(Integer bMIsAskModifyReason) {
            this.bMIsAskModifyReason = bMIsAskModifyReason;
        }

        public Integer getBMIsAskCancelReason() {
            return bMIsAskCancelReason;
        }

        public void setBMIsAskCancelReason(Integer bMIsAskCancelReason) {
            this.bMIsAskCancelReason = bMIsAskCancelReason;
        }

        public Integer getCMCityID() {
            return cMCityID;
        }

        public void setCMCityID(Integer cMCityID) {
            this.cMCityID = cMCityID;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

        public String getOTP() {
            return oTP;
        }

        public void setOTP(String oTP) {
            this.oTP = oTP;
        }

        public String getSMSString() {
            return sMSString;
        }

        public void setSMSString(String sMSString) {
            this.sMSString = sMSString;
        }

        public Integer getBMAskOTPonPhoneBooking() {
            return bMAskOTPonPhoneBooking;
        }

        public void setBMAskOTPonPhoneBooking(Integer bMAskOTPonPhoneBooking) {
            this.bMAskOTPonPhoneBooking = bMAskOTPonPhoneBooking;
        }

        public String getOSSetDefaultBrowser() {
            return oSSetDefaultBrowser;
        }

        public void setOSSetDefaultBrowser(String oSSetDefaultBrowser) {
            this.oSSetDefaultBrowser = oSSetDefaultBrowser;
        }

        public Integer getBMIsAskNonReportedReason() {
            return bMIsAskNonReportedReason;
        }

        public void setBMIsAskNonReportedReason(Integer bMIsAskNonReportedReason) {
            this.bMIsAskNonReportedReason = bMIsAskNonReportedReason;
        }

        public Integer getIsAdminlogin() {
            return isAdminlogin;
        }

        public void setIsAdminlogin(Integer isAdminlogin) {
            this.isAdminlogin = isAdminlogin;
        }
    }

}
