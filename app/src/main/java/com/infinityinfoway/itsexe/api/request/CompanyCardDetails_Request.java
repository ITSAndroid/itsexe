package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyCardDetails_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("CCM_CardNo")
    @Expose
    private String cCMCardNo;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("Route_CompanyId")
    @Expose
    private Integer routeCompanyId;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getCCMCardNo() {
        return cCMCardNo;
    }

    public void setCCMCardNo(String cCMCardNo) {
        this.cCMCardNo = cCMCardNo;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public Integer getRouteCompanyId() {
        return routeCompanyId;
    }

    public void setRouteCompanyId(Integer routeCompanyId) {
        this.routeCompanyId = routeCompanyId;
    }

    public CompanyCardDetails_Request(Integer cMCompanyID, String cCMCardNo, String jMJourneyStartDate, Integer rMRouteID, Integer rTRouteTimeID, Integer jMJourneyFrom, Integer jMJourneyTo, Integer routeCompanyId) {
        this.cMCompanyID = cMCompanyID;
        this.cCMCardNo = cCMCardNo;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.routeCompanyId = routeCompanyId;
    }
}
