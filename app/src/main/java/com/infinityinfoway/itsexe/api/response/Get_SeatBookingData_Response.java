package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_SeatBookingData_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Booking Color")
        @Expose
        private List<BookingColor> bookingColor = null;
        @SerializedName("SeatBooking")
        @Expose
        private List<SeatBooking> seatBooking = null;

        public List<BookingColor> getBookingColor() {
            return bookingColor;
        }

        public void setBookingColor(List<BookingColor> bookingColor) {
            this.bookingColor = bookingColor;
        }

        public List<SeatBooking> getSeatBooking() {
            return seatBooking;
        }

        public void setSeatBooking(List<SeatBooking> seatBooking) {
            this.seatBooking = seatBooking;
        }

    }

    public class BookingColor {

        @SerializedName("ES_ConfColor")
        @Expose
        private String eSConfColor;
        @SerializedName("ES_PhColor")
        @Expose
        private String eSPhColor;
        @SerializedName("ES_AgColor")
        @Expose
        private String eSAgColor;
        @SerializedName("ES_AgQuotaColor")
        @Expose
        private String eSAgQuotaColor;
        @SerializedName("ES_GuestColor")
        @Expose
        private String eSGuestColor;
        @SerializedName("ES_BankC")
        @Expose
        private String eSBankC;
        @SerializedName("ES_CompC")
        @Expose
        private String eSCompC;
        @SerializedName("ES_BranchColor")
        @Expose
        private String eSBranchColor;
        @SerializedName("ES_OnlineAgentColor")
        @Expose
        private String eSOnlineAgentColor;
        @SerializedName("ES_B2CColor")
        @Expose
        private String eSB2CColor;
        @SerializedName("ES_APiColor")
        @Expose
        private String eSAPiColor;
        @SerializedName("ES_OnlineAgentPhoneColor")
        @Expose
        private String eSOnlineAgentPhoneColor;

        public String getESConfColor() {
            return eSConfColor;
        }

        public void setESConfColor(String eSConfColor) {
            this.eSConfColor = eSConfColor;
        }

        public String getESPhColor() {
            return eSPhColor;
        }

        public void setESPhColor(String eSPhColor) {
            this.eSPhColor = eSPhColor;
        }

        public String getESAgColor() {
            return eSAgColor;
        }

        public void setESAgColor(String eSAgColor) {
            this.eSAgColor = eSAgColor;
        }

        public String getESAgQuotaColor() {
            return eSAgQuotaColor;
        }

        public void setESAgQuotaColor(String eSAgQuotaColor) {
            this.eSAgQuotaColor = eSAgQuotaColor;
        }

        public String getESGuestColor() {
            return eSGuestColor;
        }

        public void setESGuestColor(String eSGuestColor) {
            this.eSGuestColor = eSGuestColor;
        }

        public String getESBankC() {
            return eSBankC;
        }

        public void setESBankC(String eSBankC) {
            this.eSBankC = eSBankC;
        }

        public String getESCompC() {
            return eSCompC;
        }

        public void setESCompC(String eSCompC) {
            this.eSCompC = eSCompC;
        }

        public String getESBranchColor() {
            return eSBranchColor;
        }

        public void setESBranchColor(String eSBranchColor) {
            this.eSBranchColor = eSBranchColor;
        }

        public String getESOnlineAgentColor() {
            return eSOnlineAgentColor;
        }

        public void setESOnlineAgentColor(String eSOnlineAgentColor) {
            this.eSOnlineAgentColor = eSOnlineAgentColor;
        }

        public String getESB2CColor() {
            return eSB2CColor;
        }

        public void setESB2CColor(String eSB2CColor) {
            this.eSB2CColor = eSB2CColor;
        }

        public String getESAPiColor() {
            return eSAPiColor;
        }

        public void setESAPiColor(String eSAPiColor) {
            this.eSAPiColor = eSAPiColor;
        }

        public String getESOnlineAgentPhoneColor() {
            return eSOnlineAgentPhoneColor;
        }

        public void setESOnlineAgentPhoneColor(String eSOnlineAgentPhoneColor) {
            this.eSOnlineAgentPhoneColor = eSOnlineAgentPhoneColor;
        }

    }

    public class SeatBooking {

        @SerializedName("Confirm")
        @Expose
        private Integer confirm;
        @SerializedName("Phone")
        @Expose
        private Integer phone;
        @SerializedName("Agent")
        @Expose
        private Integer agent;
        @SerializedName("Branch")
        @Expose
        private Integer branch;
        @SerializedName("Guest")
        @Expose
        private Integer guest;
        @SerializedName("BankCard")
        @Expose
        private Integer bankCard;
        @SerializedName("CompanyCard")
        @Expose
        private Integer companyCard;
        @SerializedName("Waiting")
        @Expose
        private Integer waiting;
        @SerializedName("AgentQuota")
        @Expose
        private Integer agentQuota;

        public Integer getConfirm() {
            return confirm;
        }

        public void setConfirm(Integer confirm) {
            this.confirm = confirm;
        }

        public Integer getPhone() {
            return phone;
        }

        public void setPhone(Integer phone) {
            this.phone = phone;
        }

        public Integer getAgent() {
            return agent;
        }

        public void setAgent(Integer agent) {
            this.agent = agent;
        }

        public Integer getBranch() {
            return branch;
        }

        public void setBranch(Integer branch) {
            this.branch = branch;
        }

        public Integer getGuest() {
            return guest;
        }

        public void setGuest(Integer guest) {
            this.guest = guest;
        }

        public Integer getBankCard() {
            return bankCard;
        }

        public void setBankCard(Integer bankCard) {
            this.bankCard = bankCard;
        }

        public Integer getCompanyCard() {
            return companyCard;
        }

        public void setCompanyCard(Integer companyCard) {
            this.companyCard = companyCard;
        }

        public Integer getWaiting() {
            return waiting;
        }

        public void setWaiting(Integer waiting) {
            this.waiting = waiting;
        }

        public Integer getAgentQuota() {
            return agentQuota;
        }

        public void setAgentQuota(Integer agentQuota) {
            this.agentQuota = agentQuota;
        }

    }

}
