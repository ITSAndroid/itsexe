package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchDetailsByPNRNO_Request {

    @SerializedName("JM_PNRNO")
    @Expose
    private String jMPNRNO;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("BUM_BranchUserID")
    @Expose
    private Integer bUMBranchUserID;

    public String getJMPNRNO() {
        return jMPNRNO;
    }

    public void setJMPNRNO(String jMPNRNO) {
        this.jMPNRNO = jMPNRNO;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getBUMBranchUserID() {
        return bUMBranchUserID;
    }

    public void setBUMBranchUserID(Integer bUMBranchUserID) {
        this.bUMBranchUserID = bUMBranchUserID;
    }

    public FetchDetailsByPNRNO_Request(String jMPNRNO, Integer jMBookedByCMCompanyID, Integer bMBranchID, Integer bUMBranchUserID) {
        this.jMPNRNO = jMPNRNO;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchID = bMBranchID;
        this.bUMBranchUserID = bUMBranchUserID;
    }
}
