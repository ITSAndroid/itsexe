package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PickupDrop_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("PSM_PickupID")
        @Expose
        private Integer pSMPickupID;
        @SerializedName("PickUP_Time")
        @Expose
        private String pickUPTime;
        @SerializedName("PM_Address")
        @Expose
        private String pMAddress;
        @SerializedName("PM_PickupPhone")
        @Expose
        private String pMPickupPhone;
        @SerializedName("PSI_departTime")
        @Expose
        private String pSIDepartTime;
        @SerializedName("PickupDrop")
        @Expose
        private String pickupDrop;
        @SerializedName("PM_LandMark")
        @Expose
        private String pMLandMark;
        @SerializedName("SrOrder")
        @Expose
        private Integer srOrder;

        public Integer getPSMPickupID() {
            return pSMPickupID;
        }

        public void setPSMPickupID(Integer pSMPickupID) {
            this.pSMPickupID = pSMPickupID;
        }

        public String getPickUPTime() {
            return pickUPTime;
        }

        public void setPickUPTime(String pickUPTime) {
            this.pickUPTime = pickUPTime;
        }

        public String getPMAddress() {
            return pMAddress;
        }

        public void setPMAddress(String pMAddress) {
            this.pMAddress = pMAddress;
        }

        public String getPMPickupPhone() {
            return pMPickupPhone;
        }

        public void setPMPickupPhone(String pMPickupPhone) {
            this.pMPickupPhone = pMPickupPhone;
        }

        public String getPSIDepartTime() {
            return pSIDepartTime;
        }

        public void setPSIDepartTime(String pSIDepartTime) {
            this.pSIDepartTime = pSIDepartTime;
        }

        public String getPickupDrop() {
            return pickupDrop;
        }

        public void setPickupDrop(String pickupDrop) {
            this.pickupDrop = pickupDrop;
        }

        public String getPMLandMark() {
            return pMLandMark;
        }

        public void setPMLandMark(String pMLandMark) {
            this.pMLandMark = pMLandMark;
        }

        public Integer getSrOrder() {
            return srOrder;
        }

        public void setSrOrder(Integer srOrder) {
            this.srOrder = srOrder;
        }

    }

}
