package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Report_AllDayMemo_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("AllDayMemo")
        @Expose
        private List<AllDayMemo> allDayMemo = null;

        public List<AllDayMemo> getAllDayMemo() {
            return allDayMemo;
        }

        public void setAllDayMemo(List<AllDayMemo> allDayMemo) {
            this.allDayMemo = allDayMemo;
        }

    }

    public class AllDayMemo {

        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("TotalAmount")
        @Expose
        private Double totalAmount;
        @SerializedName("TotalCollection")
        @Expose
        private Double totalCollection;
        @SerializedName("CommissionAmount")
        @Expose
        private Double commissionAmount;
        @SerializedName("NetSeat")
        @Expose
        private Integer netSeat;
        @SerializedName("NetCollection")
        @Expose
        private Double netCollection;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("JDate")
        @Expose
        private String jDate;
        @SerializedName("TMP1")
        @Expose
        private String tmp1;
        @SerializedName("TMP2")
        @Expose
        private String tmp2;
        @SerializedName("CommInPer")
        @Expose
        private Double commInPer;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Double getTotalCollection() {
            return totalCollection;
        }

        public void setTotalCollection(Double totalCollection) {
            this.totalCollection = totalCollection;
        }

        public Double getCommissionAmount() {
            return commissionAmount;
        }

        public void setCommissionAmount(Double commissionAmount) {
            this.commissionAmount = commissionAmount;
        }

        public Integer getNetSeat() {
            return netSeat;
        }

        public void setNetSeat(Integer netSeat) {
            this.netSeat = netSeat;
        }

        public Double getNetCollection() {
            return netCollection;
        }

        public void setNetCollection(Double netCollection) {
            this.netCollection = netCollection;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getTmp1() {
            return tmp1;
        }

        public void setTmp1(String tmp1) {
            this.tmp1 = tmp1;
        }

        public String getTmp2() {
            return tmp2;
        }

        public void setTmp2(String tmp2) {
            this.tmp2 = tmp2;
        }

        public Double getCommInPer() {
            return commInPer;
        }

        public void setCommInPer(Double commInPer) {
            this.commInPer = commInPer;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

    }
}
