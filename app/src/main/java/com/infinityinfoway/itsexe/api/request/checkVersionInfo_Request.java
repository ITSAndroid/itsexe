package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class checkVersionInfo_Request {
    @SerializedName("version_code")
    @Expose
    private Integer versionCode;
    @SerializedName("update_severity")
    @Expose
    private Integer updateSeverity;
    @SerializedName("android_id")
    @Expose
    private String androidId;
    @SerializedName("version_name")
    @Expose
    private String versionName;

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public Integer getUpdateSeverity() {
        return updateSeverity;
    }

    public void setUpdateSeverity(Integer updateSeverity) {
        this.updateSeverity = updateSeverity;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public checkVersionInfo_Request(Integer versionCode, Integer updateSeverity, String androidId, String versionName) {
        this.versionCode = versionCode;
        this.updateSeverity = updateSeverity;
        this.androidId = androidId;
        this.versionName = versionName;
    }
}
