package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneTicketCount_Request {

    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("CountTotalSeat")
    @Expose
    private String countTotalSeat;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public String getCountTotalSeat() {
        return countTotalSeat;
    }

    public void setCountTotalSeat(String countTotalSeat) {
        this.countTotalSeat = countTotalSeat;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public PhoneTicketCount_Request(String jMJourneyStartDate, String countTotalSeat, Integer jMBookedByCMCompanyID, Integer cMCompanyID, Integer bMBranchID) {
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.countTotalSeat = countTotalSeat;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
    }


}
