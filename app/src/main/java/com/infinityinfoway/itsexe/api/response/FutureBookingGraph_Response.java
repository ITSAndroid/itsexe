package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FutureBookingGraph_Response {

    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("RT_Time")
        @Expose
        private String rTTime;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private Integer jMTotalPassengers;

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getRTTime() {
            return rTTime;
        }

        public void setRTTime(String rTTime) {
            this.rTTime = rTTime;
        }

        public Integer getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(Integer jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

    }

}
