package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdminLogin_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("adminLoginDetails")
        @Expose
        private List<AdminLoginDetail> adminLoginDetails = null;

        public List<AdminLoginDetail> getAdminLoginDetails() {
            return adminLoginDetails;
        }

        public void setAdminLoginDetails(List<AdminLoginDetail> adminLoginDetails) {
            this.adminLoginDetails = adminLoginDetails;
        }

    }

    public class AdminLoginDetail {

        @SerializedName("UM_UserID")
        @Expose
        private Integer uMUserID;
        @SerializedName("UM_EmpID")
        @Expose
        private String uMEmpID;
        @SerializedName("UM_Name")
        @Expose
        private String uMName;
        @SerializedName("UM_Address")
        @Expose
        private String uMAddress;
        @SerializedName("UM_PhoneNo1")
        @Expose
        private String uMPhoneNo1;
        @SerializedName("UM_PhoneNo2")
        @Expose
        private String uMPhoneNo2;
        @SerializedName("UM_Photo")
        @Expose
        private String uMPhoto;
        @SerializedName("CM_CountryID")
        @Expose
        private String cMCountryID;
        @SerializedName("SM_StateID")
        @Expose
        private String sMStateID;
        @SerializedName("CM_CityID")
        @Expose
        private String cMCityID;
        @SerializedName("UM_UserEmail")
        @Expose
        private String uMUserEmail;
        @SerializedName("UM_RefenceBy")
        @Expose
        private String uMRefenceBy;
        @SerializedName("UM_Remarks")
        @Expose
        private String uMRemarks;
        @SerializedName("UM_JoiningDate")
        @Expose
        private String uMJoiningDate;
        @SerializedName("UM_BirthDate")
        @Expose
        private String uMBirthDate;
        @SerializedName("UM_UserName")
        @Expose
        private String uMUserName;
        @SerializedName("UM_Password")
        @Expose
        private String uMPassword;
        @SerializedName("UM_CreatedDate")
        @Expose
        private String uMCreatedDate;
        @SerializedName("URM_UserRight")
        @Expose
        private String uRMUserRight;
        @SerializedName("UM_IsActive")
        @Expose
        private Integer uMIsActive;
        @SerializedName("UM_MarketingUser")
        @Expose
        private Integer uMMarketingUser;
        @SerializedName("SMS_URL")
        @Expose
        private String sMSURL;
        @SerializedName("SMS_String")
        @Expose
        private String sMSString;
        @SerializedName("OTP")
        @Expose
        private String oTP;
        @SerializedName("Status")
        @Expose
        private Integer status;
        @SerializedName("LoginStatus")
        @Expose
        private String loginStatus;
        @SerializedName("OTPMessage")
        @Expose
        private String oTPMessage;
        @SerializedName("OTPNotes")
        @Expose
        private String oTPNotes;

        public Integer getUMUserID() {
            return uMUserID;
        }

        public void setUMUserID(Integer uMUserID) {
            this.uMUserID = uMUserID;
        }

        public String getUMEmpID() {
            return uMEmpID;
        }

        public void setUMEmpID(String uMEmpID) {
            this.uMEmpID = uMEmpID;
        }

        public String getUMName() {
            return uMName;
        }

        public void setUMName(String uMName) {
            this.uMName = uMName;
        }

        public String getUMAddress() {
            return uMAddress;
        }

        public void setUMAddress(String uMAddress) {
            this.uMAddress = uMAddress;
        }

        public String getUMPhoneNo1() {
            return uMPhoneNo1;
        }

        public void setUMPhoneNo1(String uMPhoneNo1) {
            this.uMPhoneNo1 = uMPhoneNo1;
        }

        public String getUMPhoneNo2() {
            return uMPhoneNo2;
        }

        public void setUMPhoneNo2(String uMPhoneNo2) {
            this.uMPhoneNo2 = uMPhoneNo2;
        }

        public String getUMPhoto() {
            return uMPhoto;
        }

        public void setUMPhoto(String uMPhoto) {
            this.uMPhoto = uMPhoto;
        }

        public String getCMCountryID() {
            return cMCountryID;
        }

        public void setCMCountryID(String cMCountryID) {
            this.cMCountryID = cMCountryID;
        }

        public String getSMStateID() {
            return sMStateID;
        }

        public void setSMStateID(String sMStateID) {
            this.sMStateID = sMStateID;
        }

        public String getCMCityID() {
            return cMCityID;
        }

        public void setCMCityID(String cMCityID) {
            this.cMCityID = cMCityID;
        }

        public String getUMUserEmail() {
            return uMUserEmail;
        }

        public void setUMUserEmail(String uMUserEmail) {
            this.uMUserEmail = uMUserEmail;
        }

        public String getUMRefenceBy() {
            return uMRefenceBy;
        }

        public void setUMRefenceBy(String uMRefenceBy) {
            this.uMRefenceBy = uMRefenceBy;
        }

        public String getUMRemarks() {
            return uMRemarks;
        }

        public void setUMRemarks(String uMRemarks) {
            this.uMRemarks = uMRemarks;
        }

        public String getUMJoiningDate() {
            return uMJoiningDate;
        }

        public void setUMJoiningDate(String uMJoiningDate) {
            this.uMJoiningDate = uMJoiningDate;
        }

        public String getUMBirthDate() {
            return uMBirthDate;
        }

        public void setUMBirthDate(String uMBirthDate) {
            this.uMBirthDate = uMBirthDate;
        }

        public String getUMUserName() {
            return uMUserName;
        }

        public void setUMUserName(String uMUserName) {
            this.uMUserName = uMUserName;
        }

        public String getUMPassword() {
            return uMPassword;
        }

        public void setUMPassword(String uMPassword) {
            this.uMPassword = uMPassword;
        }

        public String getUMCreatedDate() {
            return uMCreatedDate;
        }

        public void setUMCreatedDate(String uMCreatedDate) {
            this.uMCreatedDate = uMCreatedDate;
        }

        public String getURMUserRight() {
            return uRMUserRight;
        }

        public void setURMUserRight(String uRMUserRight) {
            this.uRMUserRight = uRMUserRight;
        }

        public Integer getUMIsActive() {
            return uMIsActive;
        }

        public void setUMIsActive(Integer uMIsActive) {
            this.uMIsActive = uMIsActive;
        }

        public Integer getUMMarketingUser() {
            return uMMarketingUser;
        }

        public void setUMMarketingUser(Integer uMMarketingUser) {
            this.uMMarketingUser = uMMarketingUser;
        }

        public String getSMSURL() {
            return sMSURL;
        }

        public void setSMSURL(String sMSURL) {
            this.sMSURL = sMSURL;
        }

        public String getSMSString() {
            return sMSString;
        }

        public void setSMSString(String sMSString) {
            this.sMSString = sMSString;
        }

        public String getOTP() {
            return oTP;
        }

        public void setOTP(String oTP) {
            this.oTP = oTP;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getOTPMessage() {
            return oTPMessage;
        }

        public void setOTPMessage(String oTPMessage) {
            this.oTPMessage = oTPMessage;
        }

        public String getOTPNotes() {
            return oTPNotes;
        }

        public void setOTPNotes(String oTPNotes) {
            this.oTPNotes = oTPNotes;
        }

    }

}
