package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Report_CityPickUpWise_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("BookingReport_CityPickUpWise")
        @Expose
        private List<BookingReportCityPickUpWise> bookingReportCityPickUpWise = null;

        public List<BookingReportCityPickUpWise> getBookingReportCityPickUpWise() {
            return bookingReportCityPickUpWise;
        }

        public void setBookingReportCityPickUpWise(List<BookingReportCityPickUpWise> bookingReportCityPickUpWise) {
            this.bookingReportCityPickUpWise = bookingReportCityPickUpWise;
        }

    }

    public class BookingReportCityPickUpWise {

        @SerializedName("PNRNO")
        @Expose
        private Integer pNRNO;
        @SerializedName("TicketNo")
        @Expose
        private String ticketNo;
        @SerializedName("UserID")
        @Expose
        private Integer userID;
        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("CustomerName")
        @Expose
        private String customerName;
        @SerializedName("Phone")
        @Expose
        private String phone;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("BookingTypeID")
        @Expose
        private Integer bookingTypeID;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("JourneyStartTime")
        @Expose
        private String journeyStartTime;
        @SerializedName("JourneyCityTime")
        @Expose
        private String journeyCityTime;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("TotalEmptySeat")
        @Expose
        private Integer totalEmptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("TotalEmptySleeper")
        @Expose
        private Integer totalEmptySleeper;
        @SerializedName("RemarkDetail")
        @Expose
        private String remarkDetail;
        @SerializedName("SubRouteName")
        @Expose
        private String subRouteName;
        @SerializedName("JM_Remarks")
        @Expose
        private String jMRemarks;
        @SerializedName("DM_Driver1")
        @Expose
        private String dMDriver1;
        @SerializedName("DM_Driver2")
        @Expose
        private String dMDriver2;
        @SerializedName("DM_Conductor")
        @Expose
        private String dMConductor;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("JM_BookedBy_CM_CompanyID")
        @Expose
        private Integer jMBookedByCMCompanyID;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("IntPickupDateTime")
        @Expose
        private String intPickupDateTime;

        public Integer getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(Integer pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getTicketNo() {
            return ticketNo;
        }

        public void setTicketNo(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        public Integer getUserID() {
            return userID;
        }

        public void setUserID(Integer userID) {
            this.userID = userID;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public Integer getBookingTypeID() {
            return bookingTypeID;
        }

        public void setBookingTypeID(Integer bookingTypeID) {
            this.bookingTypeID = bookingTypeID;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getJourneyStartTime() {
            return journeyStartTime;
        }

        public void setJourneyStartTime(String journeyStartTime) {
            this.journeyStartTime = journeyStartTime;
        }

        public String getJourneyCityTime() {
            return journeyCityTime;
        }

        public void setJourneyCityTime(String journeyCityTime) {
            this.journeyCityTime = journeyCityTime;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public Integer getTotalEmptySeat() {
            return totalEmptySeat;
        }

        public void setTotalEmptySeat(Integer totalEmptySeat) {
            this.totalEmptySeat = totalEmptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public Integer getTotalEmptySleeper() {
            return totalEmptySleeper;
        }

        public void setTotalEmptySleeper(Integer totalEmptySleeper) {
            this.totalEmptySleeper = totalEmptySleeper;
        }

        public String getRemarkDetail() {
            return remarkDetail;
        }

        public void setRemarkDetail(String remarkDetail) {
            this.remarkDetail = remarkDetail;
        }

        public String getSubRouteName() {
            return subRouteName;
        }

        public void setSubRouteName(String subRouteName) {
            this.subRouteName = subRouteName;
        }

        public String getJMRemarks() {
            return jMRemarks;
        }

        public void setJMRemarks(String jMRemarks) {
            this.jMRemarks = jMRemarks;
        }

        public String getDMDriver1() {
            return dMDriver1;
        }

        public void setDMDriver1(String dMDriver1) {
            this.dMDriver1 = dMDriver1;
        }

        public String getDMDriver2() {
            return dMDriver2;
        }

        public void setDMDriver2(String dMDriver2) {
            this.dMDriver2 = dMDriver2;
        }

        public String getDMConductor() {
            return dMConductor;
        }

        public void setDMConductor(String dMConductor) {
            this.dMConductor = dMConductor;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getJMBookedByCMCompanyID() {
            return jMBookedByCMCompanyID;
        }

        public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
            this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public String getIntPickupDateTime() {
            return intPickupDateTime;
        }

        public void setIntPickupDateTime(String intPickupDateTime) {
            this.intPickupDateTime = intPickupDateTime;
        }

    }

}
