package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllDayBranchWiseReturnOnwardMemo_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("ReportType")
    @Expose
    private Integer reportType;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchUserID")
    @Expose
    private Integer bMBranchUserID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchUserID() {
        return bMBranchUserID;
    }

    public void setBMBranchUserID(Integer bMBranchUserID) {
        this.bMBranchUserID = bMBranchUserID;
    }

    public AllDayBranchWiseReturnOnwardMemo_Request(Integer cMCompanyID, String jMJourneyStartDate, Integer bMBranchID, Integer reportType, Integer jMBookedByCMCompanyID, Integer bMBranchUserID) {
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.bMBranchID = bMBranchID;
        this.reportType = reportType;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchUserID = bMBranchUserID;
    }
}
