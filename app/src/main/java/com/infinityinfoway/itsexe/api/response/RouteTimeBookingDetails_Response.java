package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RouteTimeBookingDetails_Response {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("TotalSeat")
        @Expose
        private String totalSeat;
        @SerializedName("TotalAmount")
        @Expose
        private String totalAmount;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("JDate")
        @Expose
        private String jDate;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("RouteFromCity")
        @Expose
        private String routeFromCity;
        @SerializedName("RouteToCity")
        @Expose
        private String routeToCity;
        @SerializedName("Memo")
        @Expose
        private String memo;
        @SerializedName("BM_BusNo")
        @Expose
        private String bMBusNo;
        @SerializedName("GroupByType")
        @Expose
        private String groupByType;
        @SerializedName("OrderBY")
        @Expose
        private Integer orderBY;
        @SerializedName("CommPer")
        @Expose
        private Double commPer;
        @SerializedName("GroupTotal")
        @Expose
        private String groupTotal;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(String totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getRouteFromCity() {
            return routeFromCity;
        }

        public void setRouteFromCity(String routeFromCity) {
            this.routeFromCity = routeFromCity;
        }

        public String getRouteToCity() {
            return routeToCity;
        }

        public void setRouteToCity(String routeToCity) {
            this.routeToCity = routeToCity;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getBMBusNo() {
            return bMBusNo;
        }

        public void setBMBusNo(String bMBusNo) {
            this.bMBusNo = bMBusNo;
        }

        public String getGroupByType() {
            return groupByType;
        }

        public void setGroupByType(String groupByType) {
            this.groupByType = groupByType;
        }

        public Integer getOrderBY() {
            return orderBY;
        }

        public void setOrderBY(Integer orderBY) {
            this.orderBY = orderBY;
        }

        public Double getCommPer() {
            return commPer;
        }

        public void setCommPer(Double commPer) {
            this.commPer = commPer;
        }

        public String getGroupTotal() {
            return groupTotal;
        }

        public void setGroupTotal(String groupTotal) {
            this.groupTotal = groupTotal;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
