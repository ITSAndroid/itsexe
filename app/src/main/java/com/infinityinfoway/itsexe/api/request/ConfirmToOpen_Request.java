package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmToOpen_Request {

    @SerializedName("JM_PNRNO")
    @Expose
    private Integer jmPnrno;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("JM_PCRegistrationID")
    @Expose
    private Integer jMPCRegistrationID;

    public Integer getJmPnrno() {
        return jmPnrno;
    }

    public void setJmPnrno(Integer jmPnrno) {
        this.jmPnrno = jmPnrno;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getJMPCRegistrationID() {
        return jMPCRegistrationID;
    }

    public void setJMPCRegistrationID(Integer jMPCRegistrationID) {
        this.jMPCRegistrationID = jMPCRegistrationID;
    }

    public ConfirmToOpen_Request(Integer jmPnrno, Integer cMCompanyID, Integer bMBranchID, Integer uMUserID, Integer jMBookedByCMCompanyID, Integer jMPCRegistrationID) {
        this.jmPnrno = jmPnrno;
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
        this.uMUserID = uMUserID;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.jMPCRegistrationID = jMPCRegistrationID;
    }


}
