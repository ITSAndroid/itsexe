package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_SearchingPhoneSeatCount_Request {

    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;
    @SerializedName("CountTotalSeat")
    @Expose
    private String countTotalSeat;
    @SerializedName("BookedBy_CM_CompanyID")
    @Expose
    private Integer bookedByCMCompanyID;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public String getCountTotalSeat() {
        return countTotalSeat;
    }

    public void setCountTotalSeat(String countTotalSeat) {
        this.countTotalSeat = countTotalSeat;
    }

    public Integer getBookedByCMCompanyID() {
        return bookedByCMCompanyID;
    }

    public void setBookedByCMCompanyID(Integer bookedByCMCompanyID) {
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Get_SearchingPhoneSeatCount_Request(String journeyStartDate, String countTotalSeat, Integer bookedByCMCompanyID, Integer companyID) {
        this.journeyStartDate = journeyStartDate;
        this.countTotalSeat = countTotalSeat;
        this.bookedByCMCompanyID = bookedByCMCompanyID;
        this.companyID = companyID;
    }
}
