package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PNRBookingLog_Request {

    @SerializedName("JM_PNRNO")
    @Expose
    private String jmPnrno;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;

    public String getJmPnrno() {
        return jmPnrno;
    }

    public void setJmPnrno(String jmPnrno) {
        this.jmPnrno = jmPnrno;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public PNRBookingLog_Request(String jmPnrno, Integer uMUserID, Integer cMCompanyID) {
        this.jmPnrno = jmPnrno;
        this.uMUserID = uMUserID;
        this.cMCompanyID = cMCompanyID;
    }
}
