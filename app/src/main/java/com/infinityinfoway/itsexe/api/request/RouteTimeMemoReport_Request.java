package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteTimeMemoReport_Request {

    @SerializedName("JourneyDate")
    @Expose
    private String journeyDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchUserID")
    @Expose
    private Integer bMBranchUserID;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("PSI_IsSameDay")
    @Expose
    private String pSIIsSameDay;

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchUserID() {
        return bMBranchUserID;
    }

    public void setBMBranchUserID(Integer bMBranchUserID) {
        this.bMBranchUserID = bMBranchUserID;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public String getPSIIsSameDay() {
        return pSIIsSameDay;
    }

    public void setPSIIsSameDay(String pSIIsSameDay) {
        this.pSIIsSameDay = pSIIsSameDay;
    }

    public RouteTimeMemoReport_Request(String journeyDate, Integer rMRouteID, Integer rTTime, Integer cMCompanyID, Integer jMBookedByCMCompanyID, Integer bMBranchUserID, Integer jMJourneyFrom, Integer jMJourneyTo, String pSIIsSameDay) {
        this.journeyDate = journeyDate;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.cMCompanyID = cMCompanyID;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchUserID = bMBranchUserID;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.pSIIsSameDay = pSIIsSameDay;
    }
}
