package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DailyRoutetimeWiseMemo_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("BranchAgentName")
        @Expose
        private String branchAgentName;
        @SerializedName("BranchAgentID")
        @Expose
        private Integer branchAgentID;
        @SerializedName("BookingType")
        @Expose
        private Integer bookingType;
        @SerializedName("SeatList")
        @Expose
        private String seatList;
        @SerializedName("TotalPax")
        @Expose
        private Integer totalPax;
        @SerializedName("TotalBaseFare")
        @Expose
        private Double totalBaseFare;
        @SerializedName("TotalServieTax")
        @Expose
        private Double totalServieTax;
        @SerializedName("TotalAmount")
        @Expose
        private Double totalAmount;
        @SerializedName("TotalCommission")
        @Expose
        private Double totalCommission;
        @SerializedName("TotalNetAmount")
        @Expose
        private Double totalNetAmount;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("RouteNam")
        @Expose
        private String routeNam;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("PickupManName")
        @Expose
        private String pickupManName;
        @SerializedName("DriverName")
        @Expose
        private String driverName;
        @SerializedName("EmptySeats")
        @Expose
        private String emptySeats;
        @SerializedName("TotalEmptySeats")
        @Expose
        private Integer totalEmptySeats;
        @SerializedName("ReportHeader")
        @Expose
        private String reportHeader;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;

        public String getBranchAgentName() {
            return branchAgentName;
        }

        public void setBranchAgentName(String branchAgentName) {
            this.branchAgentName = branchAgentName;
        }

        public Integer getBranchAgentID() {
            return branchAgentID;
        }

        public void setBranchAgentID(Integer branchAgentID) {
            this.branchAgentID = branchAgentID;
        }

        public Integer getBookingType() {
            return bookingType;
        }

        public void setBookingType(Integer bookingType) {
            this.bookingType = bookingType;
        }

        public String getSeatList() {
            return seatList;
        }

        public void setSeatList(String seatList) {
            this.seatList = seatList;
        }

        public Integer getTotalPax() {
            return totalPax;
        }

        public void setTotalPax(Integer totalPax) {
            this.totalPax = totalPax;
        }

        public Double getTotalBaseFare() {
            return totalBaseFare;
        }

        public void setTotalBaseFare(Double totalBaseFare) {
            this.totalBaseFare = totalBaseFare;
        }

        public Double getTotalServieTax() {
            return totalServieTax;
        }

        public void setTotalServieTax(Double totalServieTax) {
            this.totalServieTax = totalServieTax;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Double getTotalCommission() {
            return totalCommission;
        }

        public void setTotalCommission(Double totalCommission) {
            this.totalCommission = totalCommission;
        }

        public Double getTotalNetAmount() {
            return totalNetAmount;
        }

        public void setTotalNetAmount(Double totalNetAmount) {
            this.totalNetAmount = totalNetAmount;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public String getRouteNam() {
            return routeNam;
        }

        public void setRouteNam(String routeNam) {
            this.routeNam = routeNam;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getPickupManName() {
            return pickupManName;
        }

        public void setPickupManName(String pickupManName) {
            this.pickupManName = pickupManName;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getEmptySeats() {
            return emptySeats;
        }

        public void setEmptySeats(String emptySeats) {
            this.emptySeats = emptySeats;
        }

        public Integer getTotalEmptySeats() {
            return totalEmptySeats;
        }

        public void setTotalEmptySeats(Integer totalEmptySeats) {
            this.totalEmptySeats = totalEmptySeats;
        }

        public String getReportHeader() {
            return reportHeader;
        }

        public void setReportHeader(String reportHeader) {
            this.reportHeader = reportHeader;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

    }

}
