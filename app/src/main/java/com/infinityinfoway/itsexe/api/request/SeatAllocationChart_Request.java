package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeatAllocationChart_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("FromCityID")
    @Expose
    private Integer fromCityID;
    @SerializedName("ToCityID")
    @Expose
    private Integer toCityID;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("JM_JourneyStartTime")
    @Expose
    private String jMJourneyStartTime;
    @SerializedName("JM_JourneyCityTime")
    @Expose
    private String jMJourneyCityTime;
    @SerializedName("IsSubRoute")
    @Expose
    private Integer isSubRoute;
    @SerializedName("BAM_ArrangementID")
    @Expose
    private Integer bAMArrangementID;
    @SerializedName("CM_CityID")
    @Expose
    private Integer cMCityID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("PSI_IsSameDay")
    @Expose
    private String pSIIsSameDay;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getFromCityID() {
        return fromCityID;
    }

    public void setFromCityID(Integer fromCityID) {
        this.fromCityID = fromCityID;
    }

    public Integer getToCityID() {
        return toCityID;
    }

    public void setToCityID(Integer toCityID) {
        this.toCityID = toCityID;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getJMJourneyStartTime() {
        return jMJourneyStartTime;
    }

    public void setJMJourneyStartTime(String jMJourneyStartTime) {
        this.jMJourneyStartTime = jMJourneyStartTime;
    }

    public String getJMJourneyCityTime() {
        return jMJourneyCityTime;
    }

    public void setJMJourneyCityTime(String jMJourneyCityTime) {
        this.jMJourneyCityTime = jMJourneyCityTime;
    }

    public Integer getIsSubRoute() {
        return isSubRoute;
    }

    public void setIsSubRoute(Integer isSubRoute) {
        this.isSubRoute = isSubRoute;
    }

    public Integer getBAMArrangementID() {
        return bAMArrangementID;
    }

    public void setBAMArrangementID(Integer bAMArrangementID) {
        this.bAMArrangementID = bAMArrangementID;
    }

    public Integer getCMCityID() {
        return cMCityID;
    }

    public void setCMCityID(Integer cMCityID) {
        this.cMCityID = cMCityID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public String getPSIIsSameDay() {
        return pSIIsSameDay;
    }

    public void setPSIIsSameDay(String pSIIsSameDay) {
        this.pSIIsSameDay = pSIIsSameDay;
    }

    public SeatAllocationChart_Request(Integer cMCompanyID, String fromDate, Integer fromCityID, Integer toCityID, Integer rMRouteID, Integer rTTime, String jMJourneyStartTime, String jMJourneyCityTime, Integer isSubRoute, Integer bAMArrangementID, Integer cMCityID, Integer uMUserID, Integer jMBookedByCMCompanyID, String pSIIsSameDay) {
        this.cMCompanyID = cMCompanyID;
        this.fromDate = fromDate;
        this.fromCityID = fromCityID;
        this.toCityID = toCityID;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.jMJourneyStartTime = jMJourneyStartTime;
        this.jMJourneyCityTime = jMJourneyCityTime;
        this.isSubRoute = isSubRoute;
        this.bAMArrangementID = bAMArrangementID;
        this.cMCityID = cMCityID;
        this.uMUserID = uMUserID;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.pSIIsSameDay = pSIIsSameDay;
    }
}
