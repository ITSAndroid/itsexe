package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_SearchingSeatNo_Request {

    @SerializedName("JourneyFrom")
    @Expose
    private Integer journeyFrom;
    @SerializedName("JourneyTo")
    @Expose
    private Integer journeyTo;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("JourneyCityTime")
    @Expose
    private String journeyCityTime;
    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;
    @SerializedName("SeatNo")
    @Expose
    private String seatNo;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("ArrangementID")
    @Expose
    private Integer arrangementID;
    @SerializedName("BookedBy_CM_CompanyID")
    @Expose
    private Integer bookedByCMCompanyID;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;
    @SerializedName("BranchUserID")
    @Expose
    private Integer branchUserID;

    public Integer getJourneyFrom() {
        return journeyFrom;
    }

    public void setJourneyFrom(Integer journeyFrom) {
        this.journeyFrom = journeyFrom;
    }

    public Integer getJourneyTo() {
        return journeyTo;
    }

    public void setJourneyTo(Integer journeyTo) {
        this.journeyTo = journeyTo;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getJourneyCityTime() {
        return journeyCityTime;
    }

    public void setJourneyCityTime(String journeyCityTime) {
        this.journeyCityTime = journeyCityTime;
    }

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Integer getArrangementID() {
        return arrangementID;
    }

    public void setArrangementID(Integer arrangementID) {
        this.arrangementID = arrangementID;
    }

    public Integer getBookedByCMCompanyID() {
        return bookedByCMCompanyID;
    }

    public void setBookedByCMCompanyID(Integer bookedByCMCompanyID) {
        this.bookedByCMCompanyID = bookedByCMCompanyID;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Integer getBranchUserID() {
        return branchUserID;
    }

    public void setBranchUserID(Integer branchUserID) {
        this.branchUserID = branchUserID;
    }

    public Get_SearchingSeatNo_Request(Integer journeyFrom, Integer journeyTo, Integer rTTime, String journeyCityTime, String journeyStartDate, String seatNo, Integer rMRouteID, Integer companyID, Integer arrangementID, Integer bookedByCMCompanyID, Integer branchID, Integer branchUserID) {
        this.journeyFrom = journeyFrom;
        this.journeyTo = journeyTo;
        this.rTTime = rTTime;
        this.journeyCityTime = journeyCityTime;
        this.journeyStartDate = journeyStartDate;
        this.seatNo = seatNo;
        this.rMRouteID = rMRouteID;
        this.companyID = companyID;
        this.arrangementID = arrangementID;
        this.bookedByCMCompanyID = bookedByCMCompanyID;
        this.branchID = branchID;
        this.branchUserID = branchUserID;
    }
}
