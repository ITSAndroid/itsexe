package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubRouteWiseChart_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SubRouteWiseChart_Response.Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubRouteWiseChart_Response.Datum> getData() {
        return data;
    }

    public void setData(List<SubRouteWiseChart_Response.Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("PNRNO")
        @Expose
        private String pNRNO;
        @SerializedName("TicketNo")
        @Expose
        private String ticketNo;
        @SerializedName("UserID")
        @Expose
        private Integer userID;
        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("TotalSeat")
        @Expose
        private String totalSeat;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("CustomerName")
        @Expose
        private String customerName;
        @SerializedName("Phone")
        @Expose
        private String phone;
        @SerializedName("FromCityID")
        @Expose
        private Integer fromCityID;
        @SerializedName("ToCityID")
        @Expose
        private Integer toCityID;
        @SerializedName("FromCityToCity")
        @Expose
        private String fromCityToCity;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("BookingTypeID")
        @Expose
        private Integer bookingTypeID;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("JourneyStartTime")
        @Expose
        private String journeyStartTime;
        @SerializedName("JourneyCityTime")
        @Expose
        private String journeyCityTime;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("EmptySeat")
        @Expose
        private String emptySeat;
        @SerializedName("TotalEmptySeat")
        @Expose
        private Integer totalEmptySeat;
        @SerializedName("EmptySleeper")
        @Expose
        private String emptySleeper;
        @SerializedName("TotalEmptySleeper")
        @Expose
        private Integer totalEmptySleeper;
        @SerializedName("RemarkDetail")
        @Expose
        private String remarkDetail;
        @SerializedName("SubRouteName")
        @Expose
        private String subRouteName;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("CM_CityID")
        @Expose
        private Integer cMCityID;
        @SerializedName("Symbol")
        @Expose
        private String symbol;
        @SerializedName("JM_Remarks")
        @Expose
        private String jMRemarks;
        @SerializedName("DM_Driver1")
        @Expose
        private String dMDriver1;
        @SerializedName("DM_Driver2")
        @Expose
        private String dMDriver2;
        @SerializedName("DM_Conductor")
        @Expose
        private String dMConductor;
        @SerializedName("BaseFare")
        @Expose
        private Double baseFare;
        @SerializedName("JM_ServiceTax")
        @Expose
        private Double jMServiceTax;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("JM_BookedBy_CM_CompanyID")
        @Expose
        private Integer jMBookedByCMCompanyID;
        @SerializedName("Is_ShowFare")
        @Expose
        private Integer isShowFare;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("OwnBookingAmount")
        @Expose
        private Double ownBookingAmount;
        @SerializedName("OtherBookingAmount")
        @Expose
        private String otherBookingAmount;
        @SerializedName("OwnBookingSeat")
        @Expose
        private Integer ownBookingSeat;
        @SerializedName("OtherBookingSeat")
        @Expose
        private String otherBookingSeat;
        @SerializedName("API_BaseFare")
        @Expose
        private Double aPIBaseFare;
        @SerializedName("API_LassFare")
        @Expose
        private Double aPILassFare;
        @SerializedName("Branch_BaseFare")
        @Expose
        private Double branchBaseFare;
        @SerializedName("Branch_LassFare")
        @Expose
        private Double branchLassFare;
        @SerializedName("API_Amount")
        @Expose
        private Double aPIAmount;
        @SerializedName("Branch_Amount")
        @Expose
        private Double branchAmount;
        @SerializedName("LessAPI")
        @Expose
        private Double lessAPI;
        @SerializedName("Final_Amount")
        @Expose
        private Double finalAmount;
        @SerializedName("PickUpTime")
        @Expose
        private String pickUpTime;

        public String getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(String pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getTicketNo() {
            return ticketNo;
        }

        public void setTicketNo(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        public Integer getUserID() {
            return userID;
        }

        public void setUserID(Integer userID) {
            this.userID = userID;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(String totalSeat) {
            this.totalSeat = totalSeat;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Integer getFromCityID() {
            return fromCityID;
        }

        public void setFromCityID(Integer fromCityID) {
            this.fromCityID = fromCityID;
        }

        public Integer getToCityID() {
            return toCityID;
        }

        public void setToCityID(Integer toCityID) {
            this.toCityID = toCityID;
        }

        public String getFromCityToCity() {
            return fromCityToCity;
        }

        public void setFromCityToCity(String fromCityToCity) {
            this.fromCityToCity = fromCityToCity;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public Integer getBookingTypeID() {
            return bookingTypeID;
        }

        public void setBookingTypeID(Integer bookingTypeID) {
            this.bookingTypeID = bookingTypeID;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getJourneyStartTime() {
            return journeyStartTime;
        }

        public void setJourneyStartTime(String journeyStartTime) {
            this.journeyStartTime = journeyStartTime;
        }

        public String getJourneyCityTime() {
            return journeyCityTime;
        }

        public void setJourneyCityTime(String journeyCityTime) {
            this.journeyCityTime = journeyCityTime;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getEmptySeat() {
            return emptySeat;
        }

        public void setEmptySeat(String emptySeat) {
            this.emptySeat = emptySeat;
        }

        public Integer getTotalEmptySeat() {
            return totalEmptySeat;
        }

        public void setTotalEmptySeat(Integer totalEmptySeat) {
            this.totalEmptySeat = totalEmptySeat;
        }

        public String getEmptySleeper() {
            return emptySleeper;
        }

        public void setEmptySleeper(String emptySleeper) {
            this.emptySleeper = emptySleeper;
        }

        public Integer getTotalEmptySleeper() {
            return totalEmptySleeper;
        }

        public void setTotalEmptySleeper(Integer totalEmptySleeper) {
            this.totalEmptySleeper = totalEmptySleeper;
        }

        public String getRemarkDetail() {
            return remarkDetail;
        }

        public void setRemarkDetail(String remarkDetail) {
            this.remarkDetail = remarkDetail;
        }

        public String getSubRouteName() {
            return subRouteName;
        }

        public void setSubRouteName(String subRouteName) {
            this.subRouteName = subRouteName;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public Integer getCMCityID() {
            return cMCityID;
        }

        public void setCMCityID(Integer cMCityID) {
            this.cMCityID = cMCityID;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getJMRemarks() {
            return jMRemarks;
        }

        public void setJMRemarks(String jMRemarks) {
            this.jMRemarks = jMRemarks;
        }

        public String getDMDriver1() {
            return dMDriver1;
        }

        public void setDMDriver1(String dMDriver1) {
            this.dMDriver1 = dMDriver1;
        }

        public String getDMDriver2() {
            return dMDriver2;
        }

        public void setDMDriver2(String dMDriver2) {
            this.dMDriver2 = dMDriver2;
        }

        public String getDMConductor() {
            return dMConductor;
        }

        public void setDMConductor(String dMConductor) {
            this.dMConductor = dMConductor;
        }

        public Double getBaseFare() {
            return baseFare;
        }

        public void setBaseFare(Double baseFare) {
            this.baseFare = baseFare;
        }

        public Double getJMServiceTax() {
            return jMServiceTax;
        }

        public void setJMServiceTax(Double jMServiceTax) {
            this.jMServiceTax = jMServiceTax;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getJMBookedByCMCompanyID() {
            return jMBookedByCMCompanyID;
        }

        public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
            this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        }

        public Integer getIsShowFare() {
            return isShowFare;
        }

        public void setIsShowFare(Integer isShowFare) {
            this.isShowFare = isShowFare;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public Double getOwnBookingAmount() {
            return ownBookingAmount;
        }

        public void setOwnBookingAmount(Double ownBookingAmount) {
            this.ownBookingAmount = ownBookingAmount;
        }

        public String getOtherBookingAmount() {
            return otherBookingAmount;
        }

        public void setOtherBookingAmount(String otherBookingAmount) {
            this.otherBookingAmount = otherBookingAmount;
        }

        public Integer getOwnBookingSeat() {
            return ownBookingSeat;
        }

        public void setOwnBookingSeat(Integer ownBookingSeat) {
            this.ownBookingSeat = ownBookingSeat;
        }

        public String getOtherBookingSeat() {
            return otherBookingSeat;
        }

        public void setOtherBookingSeat(String otherBookingSeat) {
            this.otherBookingSeat = otherBookingSeat;
        }

        public Double getAPIBaseFare() {
            return aPIBaseFare;
        }

        public void setAPIBaseFare(Double aPIBaseFare) {
            this.aPIBaseFare = aPIBaseFare;
        }

        public Double getAPILassFare() {
            return aPILassFare;
        }

        public void setAPILassFare(Double aPILassFare) {
            this.aPILassFare = aPILassFare;
        }

        public Double getBranchBaseFare() {
            return branchBaseFare;
        }

        public void setBranchBaseFare(Double branchBaseFare) {
            this.branchBaseFare = branchBaseFare;
        }

        public Double getBranchLassFare() {
            return branchLassFare;
        }

        public void setBranchLassFare(Double branchLassFare) {
            this.branchLassFare = branchLassFare;
        }

        public Double getAPIAmount() {
            return aPIAmount;
        }

        public void setAPIAmount(Double aPIAmount) {
            this.aPIAmount = aPIAmount;
        }

        public Double getBranchAmount() {
            return branchAmount;
        }

        public void setBranchAmount(Double branchAmount) {
            this.branchAmount = branchAmount;
        }

        public Double getLessAPI() {
            return lessAPI;
        }

        public void setLessAPI(Double lessAPI) {
            this.lessAPI = lessAPI;
        }

        public Double getFinalAmount() {
            return finalAmount;
        }

        public void setFinalAmount(Double finalAmount) {
            this.finalAmount = finalAmount;
        }

        public String getPickUpTime() {
            return pickUpTime;
        }

        public void setPickUpTime(String pickUpTime) {
            this.pickUpTime = pickUpTime;
        }

    }

}
