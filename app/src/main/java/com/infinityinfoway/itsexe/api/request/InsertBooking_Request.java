package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertBooking_Request {
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("JM_JourneyStartTime")
    @Expose
    private String jMJourneyStartTime;
    @SerializedName("JM_JourneyCityTime")
    @Expose
    private String jMJourneyCityTime;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyEndDate")
    @Expose
    private String jMJourneyEndDate;
    @SerializedName("JM_JourneyEndTime")
    @Expose
    private String jMJourneyEndTime;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("RT_RouteFrequency")
    @Expose
    private Integer rTRouteFrequency;
    @SerializedName("JM_ReportingDate")
    @Expose
    private String jMReportingDate;
    @SerializedName("JM_ReportingTime")
    @Expose
    private String jMReportingTime;
    @SerializedName("PM_PickupID")
    @Expose
    private Integer pMPickupID;
    @SerializedName("DM_DropID")
    @Expose
    private Integer dMDropID;
    @SerializedName("BTM_BookingTypeID")
    @Expose
    private Integer bTMBookingTypeID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("JM_TicketStatus")
    @Expose
    private Integer jMTicketStatus;
    @SerializedName("JM_JourneyStatus")
    @Expose
    private Integer jMJourneyStatus;
    @SerializedName("PM_PaymentMode")
    @Expose
    private Integer pMPaymentMode;
    @SerializedName("JM_PayableAmount")
    @Expose
    private Double jMPayableAmount;
    @SerializedName("JM_TotalPassengers")
    @Expose
    private Integer jMTotalPassengers;
    @SerializedName("BAM_ArrangementID")
    @Expose
    private Integer bAMArrangementID;
    @SerializedName("RM_ACSeatRate")
    @Expose
    private Integer rMACSeatRate;
    @SerializedName("RM_ACSleeperRate")
    @Expose
    private Integer rMACSleeperRate;
    @SerializedName("RM_ACSlumberRate")
    @Expose
    private Integer rMACSlumberRate;
    @SerializedName("RM_NonACSeatRate")
    @Expose
    private Integer rMNonACSeatRate;
    @SerializedName("RM_NonACSleeperRate")
    @Expose
    private Integer rMNonACSleeperRate;
    @SerializedName("RM_NonAcSlumberRate")
    @Expose
    private Integer rMNonAcSlumberRate;
    @SerializedName("RM_ACSeatQuantity")
    @Expose
    private Integer rMACSeatQuantity;
    @SerializedName("RM_ACSleeperQuantity")
    @Expose
    private Integer rMACSleeperQuantity;
    @SerializedName("RM_ACSlumberQuantity")
    @Expose
    private Integer rMACSlumberQuantity;
    @SerializedName("RM_NonACSeatQuantity")
    @Expose
    private Integer rMNonACSeatQuantity;
    @SerializedName("RM_NonACSleeperQuantity")
    @Expose
    private Integer rMNonACSleeperQuantity;
    @SerializedName("RM_NonACSlumberQuantity")
    @Expose
    private Integer rMNonACSlumberQuantity;
    @SerializedName("JM_PassengerName")
    @Expose
    private String jMPassengerName;
    @SerializedName("JM_Phone1")
    @Expose
    private String jMPhone1;
    @SerializedName("JM_Phone2")
    @Expose
    private String jMPhone2;
    @SerializedName("JM_EmailID")
    @Expose
    private String jMEmailID;
    @SerializedName("JM_Remarks")
    @Expose
    private String jMRemarks;
    @SerializedName("JM_ModifyBy")
    @Expose
    private Integer jMModifyBy;
    @SerializedName("PH_CardHolderName")
    @Expose
    private String pHCardHolderName;
    @SerializedName("PH_CardID")
    @Expose
    private String pHCardID;
    @SerializedName("PH_CardProvidorID")
    @Expose
    private Integer pHCardProvidorID;
    @SerializedName("PH_TransStatus")
    @Expose
    private Integer pHTransStatus;
    @SerializedName("PH_DateTime")
    @Expose
    private String pHDateTime;
    @SerializedName("PH_IPAddress")
    @Expose
    private String pHIPAddress;
    @SerializedName("PH_Amount")
    @Expose
    private Double pHAmount;
    @SerializedName("GTM_GuestTypeID")
    @Expose
    private Integer gTMGuestTypeID;
    @SerializedName("AM_AgentID")
    @Expose
    private Integer aMAgentID;
    @SerializedName("ABI_TicketNo")
    @Expose
    private String aBITicketNo;
    @SerializedName("ABI_NeteRate")
    @Expose
    private Integer aBINeteRate;
    @SerializedName("ABI_TotalSeat")
    @Expose
    private Integer aBITotalSeat;
    @SerializedName("ABI_AgentCityID")
    @Expose
    private Integer aBIAgentCityID;
    @SerializedName("BBI_BM_BranchID")
    @Expose
    private Integer bBIBMBranchID;
    @SerializedName("BBI_UM_UserID")
    @Expose
    private Integer bBIUMUserID;
    @SerializedName("BBI_TicketNo")
    @Expose
    private String bBITicketNo;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("Parameters")
    @Expose
    private String parameters;
    @SerializedName("multiBook")
    @Expose
    private Integer multiBook;
    @SerializedName("JM_OriginalTotalPayable")
    @Expose
    private Double jMOriginalTotalPayable;
    @SerializedName("JM_PCRegistrationID")
    @Expose
    private Integer jMPCRegistrationID;
    @SerializedName("JM_ServiceTax")
    @Expose
    private Double jMServiceTax;
    @SerializedName("JM_RoundUP")
    @Expose
    private String jMRoundUP;
    @SerializedName("JM_B2C_OrderNo")
    @Expose
    private String jMB2COrderNo;
    @SerializedName("JM_TDRCharge")
    @Expose
    private Double jMTDRCharge;
    @SerializedName("JM_IsMagicBox")
    @Expose
    private Integer jMIsMagicBox;
    @SerializedName("Modify_RemarkID")
    @Expose
    private Integer modifyRemarkID;
    @SerializedName("JM_GSTState")
    @Expose
    private Integer jMGSTState;
    @SerializedName("JM_GSTCompanyName")
    @Expose
    private String jMGSTCompanyName;
    @SerializedName("JM_GSTRegNo")
    @Expose
    private String jMGSTRegNo;
    @SerializedName("JM_ITSAdminID")
    @Expose
    private Integer jMITSAdminID;
    @SerializedName("JM_BirthDate")
    @Expose
    private String jMBirthDate;
    @SerializedName("JM_Discount")
    @Expose
    private Double jMDiscount;
    @SerializedName("PSI_IsSameDay")
    @Expose
    private Integer pSIIsSameDay;
    @SerializedName("PM_IDProofString")
    @Expose
    private String pMIDProofString;
    @SerializedName("PhoneHour")
    @Expose
    private String phoneHour;
    @SerializedName("PhoneMin")
    @Expose
    private String phoneMin;
    @SerializedName("Walletkey")
    @Expose
    private String walletkey;
    @SerializedName("WalletType")
    @Expose
    private int walletType;
    @SerializedName("WalletOTP")
    @Expose
    private String walletOTP;
    @SerializedName("State")
    @Expose
    private String state;

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public String getJMJourneyStartTime() {
        return jMJourneyStartTime;
    }

    public void setJMJourneyStartTime(String jMJourneyStartTime) {
        this.jMJourneyStartTime = jMJourneyStartTime;
    }

    public String getJMJourneyCityTime() {
        return jMJourneyCityTime;
    }

    public void setJMJourneyCityTime(String jMJourneyCityTime) {
        this.jMJourneyCityTime = jMJourneyCityTime;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getJMJourneyEndDate() {
        return jMJourneyEndDate;
    }

    public void setJMJourneyEndDate(String jMJourneyEndDate) {
        this.jMJourneyEndDate = jMJourneyEndDate;
    }

    public String getJMJourneyEndTime() {
        return jMJourneyEndTime;
    }

    public void setJMJourneyEndTime(String jMJourneyEndTime) {
        this.jMJourneyEndTime = jMJourneyEndTime;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public Integer getRTRouteFrequency() {
        return rTRouteFrequency;
    }

    public void setRTRouteFrequency(Integer rTRouteFrequency) {
        this.rTRouteFrequency = rTRouteFrequency;
    }

    public String getJMReportingDate() {
        return jMReportingDate;
    }

    public void setJMReportingDate(String jMReportingDate) {
        this.jMReportingDate = jMReportingDate;
    }

    public String getJMReportingTime() {
        return jMReportingTime;
    }

    public void setJMReportingTime(String jMReportingTime) {
        this.jMReportingTime = jMReportingTime;
    }

    public Integer getPMPickupID() {
        return pMPickupID;
    }

    public void setPMPickupID(Integer pMPickupID) {
        this.pMPickupID = pMPickupID;
    }

    public Integer getDMDropID() {
        return dMDropID;
    }

    public void setDMDropID(Integer dMDropID) {
        this.dMDropID = dMDropID;
    }

    public Integer getBTMBookingTypeID() {
        return bTMBookingTypeID;
    }

    public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
        this.bTMBookingTypeID = bTMBookingTypeID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getJMTicketStatus() {
        return jMTicketStatus;
    }

    public void setJMTicketStatus(Integer jMTicketStatus) {
        this.jMTicketStatus = jMTicketStatus;
    }

    public Integer getJMJourneyStatus() {
        return jMJourneyStatus;
    }

    public void setJMJourneyStatus(Integer jMJourneyStatus) {
        this.jMJourneyStatus = jMJourneyStatus;
    }

    public Integer getPMPaymentMode() {
        return pMPaymentMode;
    }

    public void setPMPaymentMode(Integer pMPaymentMode) {
        this.pMPaymentMode = pMPaymentMode;
    }

    public Double getJMPayableAmount() {
        return jMPayableAmount;
    }

    public void setJMPayableAmount(Double jMPayableAmount) {
        this.jMPayableAmount = jMPayableAmount;
    }

    public Integer getJMTotalPassengers() {
        return jMTotalPassengers;
    }

    public void setJMTotalPassengers(Integer jMTotalPassengers) {
        this.jMTotalPassengers = jMTotalPassengers;
    }

    public Integer getBAMArrangementID() {
        return bAMArrangementID;
    }

    public void setBAMArrangementID(Integer bAMArrangementID) {
        this.bAMArrangementID = bAMArrangementID;
    }

    public Integer getRMACSeatRate() {
        return rMACSeatRate;
    }

    public void setRMACSeatRate(Integer rMACSeatRate) {
        this.rMACSeatRate = rMACSeatRate;
    }

    public Integer getRMACSleeperRate() {
        return rMACSleeperRate;
    }

    public void setRMACSleeperRate(Integer rMACSleeperRate) {
        this.rMACSleeperRate = rMACSleeperRate;
    }

    public Integer getRMACSlumberRate() {
        return rMACSlumberRate;
    }

    public void setRMACSlumberRate(Integer rMACSlumberRate) {
        this.rMACSlumberRate = rMACSlumberRate;
    }

    public Integer getRMNonACSeatRate() {
        return rMNonACSeatRate;
    }

    public void setRMNonACSeatRate(Integer rMNonACSeatRate) {
        this.rMNonACSeatRate = rMNonACSeatRate;
    }

    public Integer getRMNonACSleeperRate() {
        return rMNonACSleeperRate;
    }

    public void setRMNonACSleeperRate(Integer rMNonACSleeperRate) {
        this.rMNonACSleeperRate = rMNonACSleeperRate;
    }

    public Integer getRMNonAcSlumberRate() {
        return rMNonAcSlumberRate;
    }

    public void setRMNonAcSlumberRate(Integer rMNonAcSlumberRate) {
        this.rMNonAcSlumberRate = rMNonAcSlumberRate;
    }

    public Integer getRMACSeatQuantity() {
        return rMACSeatQuantity;
    }

    public void setRMACSeatQuantity(Integer rMACSeatQuantity) {
        this.rMACSeatQuantity = rMACSeatQuantity;
    }

    public Integer getRMACSleeperQuantity() {
        return rMACSleeperQuantity;
    }

    public void setRMACSleeperQuantity(Integer rMACSleeperQuantity) {
        this.rMACSleeperQuantity = rMACSleeperQuantity;
    }

    public Integer getRMACSlumberQuantity() {
        return rMACSlumberQuantity;
    }

    public void setRMACSlumberQuantity(Integer rMACSlumberQuantity) {
        this.rMACSlumberQuantity = rMACSlumberQuantity;
    }

    public Integer getRMNonACSeatQuantity() {
        return rMNonACSeatQuantity;
    }

    public void setRMNonACSeatQuantity(Integer rMNonACSeatQuantity) {
        this.rMNonACSeatQuantity = rMNonACSeatQuantity;
    }

    public Integer getRMNonACSleeperQuantity() {
        return rMNonACSleeperQuantity;
    }

    public void setRMNonACSleeperQuantity(Integer rMNonACSleeperQuantity) {
        this.rMNonACSleeperQuantity = rMNonACSleeperQuantity;
    }

    public Integer getRMNonACSlumberQuantity() {
        return rMNonACSlumberQuantity;
    }

    public void setRMNonACSlumberQuantity(Integer rMNonACSlumberQuantity) {
        this.rMNonACSlumberQuantity = rMNonACSlumberQuantity;
    }

    public String getJMPassengerName() {
        return jMPassengerName;
    }

    public void setJMPassengerName(String jMPassengerName) {
        this.jMPassengerName = jMPassengerName;
    }

    public String getJMPhone1() {
        return jMPhone1;
    }

    public void setJMPhone1(String jMPhone1) {
        this.jMPhone1 = jMPhone1;
    }

    public String getJMPhone2() {
        return jMPhone2;
    }

    public void setJMPhone2(String jMPhone2) {
        this.jMPhone2 = jMPhone2;
    }

    public String getJMEmailID() {
        return jMEmailID;
    }

    public void setJMEmailID(String jMEmailID) {
        this.jMEmailID = jMEmailID;
    }

    public String getJMRemarks() {
        return jMRemarks;
    }

    public void setJMRemarks(String jMRemarks) {
        this.jMRemarks = jMRemarks;
    }

    public Integer getJMModifyBy() {
        return jMModifyBy;
    }

    public void setJMModifyBy(Integer jMModifyBy) {
        this.jMModifyBy = jMModifyBy;
    }

    public String getPHCardHolderName() {
        return pHCardHolderName;
    }

    public void setPHCardHolderName(String pHCardHolderName) {
        this.pHCardHolderName = pHCardHolderName;
    }

    public String getPHCardID() {
        return pHCardID;
    }

    public void setPHCardID(String pHCardID) {
        this.pHCardID = pHCardID;
    }

    public Integer getPHCardProvidorID() {
        return pHCardProvidorID;
    }

    public void setPHCardProvidorID(Integer pHCardProvidorID) {
        this.pHCardProvidorID = pHCardProvidorID;
    }

    public Integer getPHTransStatus() {
        return pHTransStatus;
    }

    public void setPHTransStatus(Integer pHTransStatus) {
        this.pHTransStatus = pHTransStatus;
    }

    public String getPHDateTime() {
        return pHDateTime;
    }

    public void setPHDateTime(String pHDateTime) {
        this.pHDateTime = pHDateTime;
    }

    public String getPHIPAddress() {
        return pHIPAddress;
    }

    public void setPHIPAddress(String pHIPAddress) {
        this.pHIPAddress = pHIPAddress;
    }

    public Double getPHAmount() {
        return pHAmount;
    }

    public void setPHAmount(Double pHAmount) {
        this.pHAmount = pHAmount;
    }

    public Integer getGTMGuestTypeID() {
        return gTMGuestTypeID;
    }

    public void setGTMGuestTypeID(Integer gTMGuestTypeID) {
        this.gTMGuestTypeID = gTMGuestTypeID;
    }

    public Integer getAMAgentID() {
        return aMAgentID;
    }

    public void setAMAgentID(Integer aMAgentID) {
        this.aMAgentID = aMAgentID;
    }

    public String getABITicketNo() {
        return aBITicketNo;
    }

    public void setABITicketNo(String aBITicketNo) {
        this.aBITicketNo = aBITicketNo;
    }

    public Integer getABINeteRate() {
        return aBINeteRate;
    }

    public void setABINeteRate(Integer aBINeteRate) {
        this.aBINeteRate = aBINeteRate;
    }

    public Integer getABITotalSeat() {
        return aBITotalSeat;
    }

    public void setABITotalSeat(Integer aBITotalSeat) {
        this.aBITotalSeat = aBITotalSeat;
    }

    public Integer getABIAgentCityID() {
        return aBIAgentCityID;
    }

    public void setABIAgentCityID(Integer aBIAgentCityID) {
        this.aBIAgentCityID = aBIAgentCityID;
    }

    public Integer getBBIBMBranchID() {
        return bBIBMBranchID;
    }

    public void setBBIBMBranchID(Integer bBIBMBranchID) {
        this.bBIBMBranchID = bBIBMBranchID;
    }

    public Integer getBBIUMUserID() {
        return bBIUMUserID;
    }

    public void setBBIUMUserID(Integer bBIUMUserID) {
        this.bBIUMUserID = bBIUMUserID;
    }

    public String getBBITicketNo() {
        return bBITicketNo;
    }

    public void setBBITicketNo(String bBITicketNo) {
        this.bBITicketNo = bBITicketNo;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public Integer getMultiBook() {
        return multiBook;
    }

    public void setMultiBook(Integer multiBook) {
        this.multiBook = multiBook;
    }

    public Double getJMOriginalTotalPayable() {
        return jMOriginalTotalPayable;
    }

    public void setJMOriginalTotalPayable(Double jMOriginalTotalPayable) {
        this.jMOriginalTotalPayable = jMOriginalTotalPayable;
    }

    public Integer getJMPCRegistrationID() {
        return jMPCRegistrationID;
    }

    public void setJMPCRegistrationID(Integer jMPCRegistrationID) {
        this.jMPCRegistrationID = jMPCRegistrationID;
    }

    public Double getJMServiceTax() {
        return jMServiceTax;
    }

    public void setJMServiceTax(Double jMServiceTax) {
        this.jMServiceTax = jMServiceTax;
    }

    public String getJMRoundUP() {
        return jMRoundUP;
    }

    public void setJMRoundUP(String jMRoundUP) {
        this.jMRoundUP = jMRoundUP;
    }

    public String getJMB2COrderNo() {
        return jMB2COrderNo;
    }

    public void setJMB2COrderNo(String jMB2COrderNo) {
        this.jMB2COrderNo = jMB2COrderNo;
    }

    public Double getJMTDRCharge() {
        return jMTDRCharge;
    }

    public void setJMTDRCharge(Double jMTDRCharge) {
        this.jMTDRCharge = jMTDRCharge;
    }

    public Integer getJMIsMagicBox() {
        return jMIsMagicBox;
    }

    public void setJMIsMagicBox(Integer jMIsMagicBox) {
        this.jMIsMagicBox = jMIsMagicBox;
    }

    public Integer getModifyRemarkID() {
        return modifyRemarkID;
    }

    public void setModifyRemarkID(Integer modifyRemarkID) {
        this.modifyRemarkID = modifyRemarkID;
    }

    public Integer getJMGSTState() {
        return jMGSTState;
    }

    public void setJMGSTState(Integer jMGSTState) {
        this.jMGSTState = jMGSTState;
    }

    public String getJMGSTCompanyName() {
        return jMGSTCompanyName;
    }

    public void setJMGSTCompanyName(String jMGSTCompanyName) {
        this.jMGSTCompanyName = jMGSTCompanyName;
    }

    public String getJMGSTRegNo() {
        return jMGSTRegNo;
    }

    public void setJMGSTRegNo(String jMGSTRegNo) {
        this.jMGSTRegNo = jMGSTRegNo;
    }

    public Integer getJMITSAdminID() {
        return jMITSAdminID;
    }

    public void setJMITSAdminID(Integer jMITSAdminID) {
        this.jMITSAdminID = jMITSAdminID;
    }

    public String getJMBirthDate() {
        return jMBirthDate;
    }

    public void setJMBirthDate(String jMBirthDate) {
        this.jMBirthDate = jMBirthDate;
    }

    public Double getJMDiscount() {
        return jMDiscount;
    }

    public void setJMDiscount(Double jMDiscount) {
        this.jMDiscount = jMDiscount;
    }

    public Integer getPSIIsSameDay() {
        return pSIIsSameDay;
    }

    public void setPSIIsSameDay(Integer pSIIsSameDay) {
        this.pSIIsSameDay = pSIIsSameDay;
    }

    public String getPMIDProofString() {
        return pMIDProofString;
    }

    public void setPMIDProofString(String pMIDProofString) {
        this.pMIDProofString = pMIDProofString;
    }

    public String getPhoneHour() {
        return phoneHour;
    }

    public void setPhoneHour(String phoneHour) {
        this.phoneHour = phoneHour;
    }

    public String getPhoneMin() {
        return phoneMin;
    }

    public void setPhoneMin(String phoneMin) {
        this.phoneMin = phoneMin;
    }

    public String getWalletkey() {
        return walletkey;
    }

    public void setWalletkey(String walletkey) {
        this.walletkey = walletkey;
    }

    public int getWalletType() {
        return walletType;
    }

    public void setWalletType(int walletType) {
        this.walletType = walletType;
    }

    public String getWalletOTP() {
        return walletOTP;
    }

    public void setWalletOTP(String walletOTP) {
        this.walletOTP = walletOTP;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public InsertBooking_Request(String jMJourneyStartDate, String jMJourneyStartTime, String jMJourneyCityTime, Integer cMCompanyID, String jMJourneyEndDate, String jMJourneyEndTime, Integer jMJourneyFrom, Integer jMJourneyTo, Integer rMRouteID, Integer rTTime, Integer rTRouteFrequency, String jMReportingDate, String jMReportingTime, Integer pMPickupID, Integer dMDropID, Integer bTMBookingTypeID, Integer bMBranchID, Integer uMUserID, Integer jMTicketStatus, Integer jMJourneyStatus, Integer pMPaymentMode, Double jMPayableAmount, Integer jMTotalPassengers, Integer bAMArrangementID, Integer rMACSeatRate, Integer rMACSleeperRate, Integer rMACSlumberRate, Integer rMNonACSeatRate, Integer rMNonACSleeperRate, Integer rMNonAcSlumberRate, Integer rMACSeatQuantity, Integer rMACSleeperQuantity, Integer rMACSlumberQuantity, Integer rMNonACSeatQuantity, Integer rMNonACSleeperQuantity, Integer rMNonACSlumberQuantity, String jMPassengerName, String jMPhone1, String jMPhone2, String jMEmailID, String jMRemarks, Integer jMModifyBy, String pHCardHolderName, String pHCardID, Integer pHCardProvidorID, Integer pHTransStatus, String pHDateTime, String pHIPAddress, Double pHAmount, Integer gTMGuestTypeID, Integer aMAgentID, String aBITicketNo, Integer aBINeteRate, Integer aBITotalSeat, Integer aBIAgentCityID, Integer bBIBMBranchID, Integer bBIUMUserID, String bBITicketNo, Integer jMBookedByCMCompanyID, String parameters, Integer multiBook, Double jMOriginalTotalPayable, Integer jMPCRegistrationID, Double jMServiceTax, String jMRoundUP, String jMB2COrderNo, Double jMTDRCharge, Integer jMIsMagicBox, Integer modifyRemarkID, Integer jMGSTState, String jMGSTCompanyName, String jMGSTRegNo, Integer jMITSAdminID, String jMBirthDate, Double jMDiscount, Integer pSIIsSameDay, String pMIDProofString, String phoneHour, String phoneMin, String walletkey, int walletType, String walletOTP, String state) {
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.jMJourneyStartTime = jMJourneyStartTime;
        this.jMJourneyCityTime = jMJourneyCityTime;
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyEndDate = jMJourneyEndDate;
        this.jMJourneyEndTime = jMJourneyEndTime;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.rTRouteFrequency = rTRouteFrequency;
        this.jMReportingDate = jMReportingDate;
        this.jMReportingTime = jMReportingTime;
        this.pMPickupID = pMPickupID;
        this.dMDropID = dMDropID;
        this.bTMBookingTypeID = bTMBookingTypeID;
        this.bMBranchID = bMBranchID;
        this.uMUserID = uMUserID;
        this.jMTicketStatus = jMTicketStatus;
        this.jMJourneyStatus = jMJourneyStatus;
        this.pMPaymentMode = pMPaymentMode;
        this.jMPayableAmount = jMPayableAmount;
        this.jMTotalPassengers = jMTotalPassengers;
        this.bAMArrangementID = bAMArrangementID;
        this.rMACSeatRate = rMACSeatRate;
        this.rMACSleeperRate = rMACSleeperRate;
        this.rMACSlumberRate = rMACSlumberRate;
        this.rMNonACSeatRate = rMNonACSeatRate;
        this.rMNonACSleeperRate = rMNonACSleeperRate;
        this.rMNonAcSlumberRate = rMNonAcSlumberRate;
        this.rMACSeatQuantity = rMACSeatQuantity;
        this.rMACSleeperQuantity = rMACSleeperQuantity;
        this.rMACSlumberQuantity = rMACSlumberQuantity;
        this.rMNonACSeatQuantity = rMNonACSeatQuantity;
        this.rMNonACSleeperQuantity = rMNonACSleeperQuantity;
        this.rMNonACSlumberQuantity = rMNonACSlumberQuantity;
        this.jMPassengerName = jMPassengerName;
        this.jMPhone1 = jMPhone1;
        this.jMPhone2 = jMPhone2;
        this.jMEmailID = jMEmailID;
        this.jMRemarks = jMRemarks;
        this.jMModifyBy = jMModifyBy;
        this.pHCardHolderName = pHCardHolderName;
        this.pHCardID = pHCardID;
        this.pHCardProvidorID = pHCardProvidorID;
        this.pHTransStatus = pHTransStatus;
        this.pHDateTime = pHDateTime;
        this.pHIPAddress = pHIPAddress;
        this.pHAmount = pHAmount;
        this.gTMGuestTypeID = gTMGuestTypeID;
        this.aMAgentID = aMAgentID;
        this.aBITicketNo = aBITicketNo;
        this.aBINeteRate = aBINeteRate;
        this.aBITotalSeat = aBITotalSeat;
        this.aBIAgentCityID = aBIAgentCityID;
        this.bBIBMBranchID = bBIBMBranchID;
        this.bBIUMUserID = bBIUMUserID;
        this.bBITicketNo = bBITicketNo;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.parameters = parameters;
        this.multiBook = multiBook;
        this.jMOriginalTotalPayable = jMOriginalTotalPayable;
        this.jMPCRegistrationID = jMPCRegistrationID;
        this.jMServiceTax = jMServiceTax;
        this.jMRoundUP = jMRoundUP;
        this.jMB2COrderNo = jMB2COrderNo;
        this.jMTDRCharge = jMTDRCharge;
        this.jMIsMagicBox = jMIsMagicBox;
        this.modifyRemarkID = modifyRemarkID;
        this.jMGSTState = jMGSTState;
        this.jMGSTCompanyName = jMGSTCompanyName;
        this.jMGSTRegNo = jMGSTRegNo;
        this.jMITSAdminID = jMITSAdminID;
        this.jMBirthDate = jMBirthDate;
        this.jMDiscount = jMDiscount;
        this.pSIIsSameDay = pSIIsSameDay;
        this.pMIDProofString = pMIDProofString;
        this.phoneHour = phoneHour;
        this.phoneMin = phoneMin;
        this.walletkey = walletkey;
        this.walletType = walletType;
        this.walletOTP = walletOTP;
        this.state = state;
    }
}
