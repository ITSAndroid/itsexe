package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Route_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("RouteDetails")
        @Expose
        private List<RouteDetail> routeDetails = null;

        public List<RouteDetail> getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(List<RouteDetail> routeDetails) {
            this.routeDetails = routeDetails;
        }

    }

    public class RouteDetail {

        @SerializedName("RouteTimeID")
        @Expose
        private Integer routeTimeID;
        @SerializedName("RouteID")
        @Expose
        private Integer routeID;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("StartTime")
        @Expose
        private String startTime;
        @SerializedName("OrderBy")
        @Expose
        private Integer orderBy;
        @SerializedName("RouteCompanyID")
        @Expose
        private Integer routeCompanyID;
        @SerializedName("BusID")
        @Expose
        private String busID;
        @SerializedName("CurrentKM")
        @Expose
        private String currentKM;
        @SerializedName("RouteNameTIME")
        @Expose
        private String routeNameTIME;

        public Integer getRouteTimeID() {
            return routeTimeID;
        }

        public void setRouteTimeID(Integer routeTimeID) {
            this.routeTimeID = routeTimeID;
        }

        public Integer getRouteID() {
            return routeID;
        }

        public void setRouteID(Integer routeID) {
            this.routeID = routeID;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public Integer getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Integer orderBy) {
            this.orderBy = orderBy;
        }

        public Integer getRouteCompanyID() {
            return routeCompanyID;
        }

        public void setRouteCompanyID(Integer routeCompanyID) {
            this.routeCompanyID = routeCompanyID;
        }

        public String getBusID() {
            return busID;
        }

        public void setBusID(String busID) {
            this.busID = busID;
        }

        public String getCurrentKM() {
            return currentKM;
        }

        public void setCurrentKM(String currentKM) {
            this.currentKM = currentKM;
        }

        public String getRouteNameTIME() {
            return routeNameTIME;
        }

        public void setRouteNameTIME(String routeNameTIME) {
            this.routeNameTIME = routeNameTIME;
        }

    }

}
