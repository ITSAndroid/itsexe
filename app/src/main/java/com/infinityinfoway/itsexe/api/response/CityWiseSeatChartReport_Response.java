package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityWiseSeatChartReport_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("Tocity")
        @Expose
        private String tocity;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;
        @SerializedName("BBI_CommAmount")
        @Expose
        private String bBICommAmount;
        @SerializedName("BBI_NetAmount")
        @Expose
        private String bBINetAmount;
        @SerializedName("Ser")
        @Expose
        private String ser;
        @SerializedName("Sec")
        @Expose
        private Integer sec;
        @SerializedName("Seat")
        @Expose
        private int seat;
        @SerializedName("SLP")
        @Expose
        private int sLP;
        @SerializedName("SEAT FARE")
        @Expose
        private String sEATFARE;
        @SerializedName("SLP FARE")
        @Expose
        private String sLPFARE;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("JDate")
        @Expose
        private String jDate;
        @SerializedName("JM_JourneyStartTime")
        @Expose
        private String jMJourneyStartTime;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getTocity() {
            return tocity;
        }

        public void setTocity(String tocity) {
            this.tocity = tocity;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

        public String getBBICommAmount() {
            return bBICommAmount;
        }

        public void setBBICommAmount(String bBICommAmount) {
            this.bBICommAmount = bBICommAmount;
        }

        public String getBBINetAmount() {
            return bBINetAmount;
        }

        public void setBBINetAmount(String bBINetAmount) {
            this.bBINetAmount = bBINetAmount;
        }

        public String getSer() {
            return ser;
        }

        public void setSer(String ser) {
            this.ser = ser;
        }

        public Integer getSec() {
            return sec;
        }

        public void setSec(Integer sec) {
            this.sec = sec;
        }

        public int getSeat() {
            return seat;
        }

        public void setSeat(int seat) {
            this.seat = seat;
        }

        public int getSLP() {
            return sLP;
        }

        public void setSLP(int sLP) {
            this.sLP = sLP;
        }

        public String getSEATFARE() {
            return sEATFARE;
        }

        public void setSEATFARE(String sEATFARE) {
            this.sEATFARE = sEATFARE;
        }

        public String getSLPFARE() {
            return sLPFARE;
        }

        public void setSLPFARE(String sLPFARE) {
            this.sLPFARE = sLPFARE;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getJMJourneyStartTime() {
            return jMJourneyStartTime;
        }

        public void setJMJourneyStartTime(String jMJourneyStartTime) {
            this.jMJourneyStartTime = jMJourneyStartTime;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
