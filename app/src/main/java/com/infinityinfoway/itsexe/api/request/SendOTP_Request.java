package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendOTP_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_RouteTimeID")
    @Expose
    private Integer rTRouteTimeID;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("JM_Phone1")
    @Expose
    private String jMPhone1;
    @SerializedName("BTM_BookingTypeID")
    @Expose
    private Integer bTMBookingTypeID;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTRouteTimeID() {
        return rTRouteTimeID;
    }

    public void setRTRouteTimeID(Integer rTRouteTimeID) {
        this.rTRouteTimeID = rTRouteTimeID;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public String getJMPhone1() {
        return jMPhone1;
    }

    public void setJMPhone1(String jMPhone1) {
        this.jMPhone1 = jMPhone1;
    }

    public Integer getBTMBookingTypeID() {
        return bTMBookingTypeID;
    }

    public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
        this.bTMBookingTypeID = bTMBookingTypeID;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public SendOTP_Request(Integer cMCompanyID, String jMJourneyStartDate, Integer rMRouteID, Integer rTRouteTimeID, Integer jMJourneyFrom, Integer jMJourneyTo, Integer uMUserID, String jMPhone1, Integer bTMBookingTypeID, Integer jMBookedByCMCompanyID) {
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.rMRouteID = rMRouteID;
        this.rTRouteTimeID = rTRouteTimeID;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.uMUserID = uMUserID;
        this.jMPhone1 = jMPhone1;
        this.bTMBookingTypeID = bTMBookingTypeID;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }
}
