package com.infinityinfoway.itsexe.api;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.infinityinfoway.itsexe.BuildConfig;
import com.infinityinfoway.itsexe.config.Config;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    private static final String BASE_URL = Config.JSON_API_URL;
    private static Retrofit retrofit = null;

    private static final Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private static RxJava3CallAdapterFactory rxAdapter = RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io());


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                    //.addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .addCallAdapterFactory(rxAdapter)
                    .build();
        }
        return retrofit;
    }

    public static OkHttpClient getOkHttpClient() {

        HttpLoggingInterceptor.Level levelType;
        if (BuildConfig.BUILD_TYPE.contentEquals("debug")) {
            levelType = HttpLoggingInterceptor.Level.BODY;
        } else {
            levelType = HttpLoggingInterceptor.Level.NONE;
        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(levelType);

        return new OkHttpClient.Builder()
                .readTimeout(180, TimeUnit.SECONDS)
                .connectTimeout(180, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .addInterceptor(new Interceptor() {
                    @NonNull
                    @Override
                    public Response intercept(@NonNull Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Content-Type", "application/json")
                                .build();
                        return chain.proceed(request);
                    }
                }).build();
    }

}
