package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BusIncomeReceipt_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("Fromcity")
        @Expose
        private String fromcity;
        @SerializedName("Tocity")
        @Expose
        private String tocity;
        @SerializedName("JP_Amount")
        @Expose
        private Double jPAmount;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("Total")
        @Expose
        private Double total;
        @SerializedName("AgentComm")
        @Expose
        private Double agentComm;
        @SerializedName("GrandTotal")
        @Expose
        private Double grandTotal;
        @SerializedName("HoComm")
        @Expose
        private Double hoComm;
        @SerializedName("Net")
        @Expose
        private Double net;
        @SerializedName("JM_JourneyStartTime")
        @Expose
        private String jMJourneyStartTime;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("RT_RouteName")
        @Expose
        private String rTRouteName;
        @SerializedName("Header")
        @Expose
        private String header;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("JP_ServiceTax")
        @Expose
        private Double jPServiceTax;
        @SerializedName("GSTTxt")
        @Expose
        private String gSTTxt;
        @SerializedName("GSTAmt")
        @Expose
        private Double gSTAmt;
        @SerializedName("HOCommPer")
        @Expose
        private Double hOCommPer;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("GroupBy")
        @Expose
        private String groupBy;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getFromcity() {
            return fromcity;
        }

        public void setFromcity(String fromcity) {
            this.fromcity = fromcity;
        }

        public String getTocity() {
            return tocity;
        }

        public void setTocity(String tocity) {
            this.tocity = tocity;
        }

        public Double getJPAmount() {
            return jPAmount;
        }

        public void setJPAmount(Double jPAmount) {
            this.jPAmount = jPAmount;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public Double getAgentComm() {
            return agentComm;
        }

        public void setAgentComm(Double agentComm) {
            this.agentComm = agentComm;
        }

        public Double getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(Double grandTotal) {
            this.grandTotal = grandTotal;
        }

        public Double getHoComm() {
            return hoComm;
        }

        public void setHoComm(Double hoComm) {
            this.hoComm = hoComm;
        }

        public Double getNet() {
            return net;
        }

        public void setNet(Double net) {
            this.net = net;
        }

        public String getJMJourneyStartTime() {
            return jMJourneyStartTime;
        }

        public void setJMJourneyStartTime(String jMJourneyStartTime) {
            this.jMJourneyStartTime = jMJourneyStartTime;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getRTRouteName() {
            return rTRouteName;
        }

        public void setRTRouteName(String rTRouteName) {
            this.rTRouteName = rTRouteName;
        }

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public Double getJPServiceTax() {
            return jPServiceTax;
        }

        public void setJPServiceTax(Double jPServiceTax) {
            this.jPServiceTax = jPServiceTax;
        }

        public String getGSTTxt() {
            return gSTTxt;
        }

        public void setGSTTxt(String gSTTxt) {
            this.gSTTxt = gSTTxt;
        }

        public Double getGSTAmt() {
            return gSTAmt;
        }

        public void setGSTAmt(Double gSTAmt) {
            this.gSTAmt = gSTAmt;
        }

        public Double getHOCommPer() {
            return hOCommPer;
        }

        public void setHOCommPer(Double hOCommPer) {
            this.hOCommPer = hOCommPer;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getGroupBy() {
            return groupBy;
        }

        public void setGroupBy(String groupBy) {
            this.groupBy = groupBy;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
