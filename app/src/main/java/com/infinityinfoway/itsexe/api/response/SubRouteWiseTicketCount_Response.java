package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubRouteWiseTicketCount_Response {

    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_Time")
        @Expose
        private Integer rTTime;
        @SerializedName("RM_FromCityID")
        @Expose
        private Integer rMFromCityID;
        @SerializedName("JM_JourneyFrom")
        @Expose
        private Integer jMJourneyFrom;
        @SerializedName("JM_JourneyTo")
        @Expose
        private Integer jMJourneyTo;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("RM_RouteNameDisplay")
        @Expose
        private String rMRouteNameDisplay;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("FromCityToCity")
        @Expose
        private String fromCityToCity;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_Address")
        @Expose
        private String cMAddress;
        @SerializedName("CM_CompanyShortName")
        @Expose
        private String cMCompanyShortName;
        @SerializedName("Tmp1")
        @Expose
        private String tmp1;
        @SerializedName("Tmp2")
        @Expose
        private String tmp2;
        @SerializedName("Tmp3")
        @Expose
        private String tmp3;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTTime() {
            return rTTime;
        }

        public void setRTTime(Integer rTTime) {
            this.rTTime = rTTime;
        }

        public Integer getRMFromCityID() {
            return rMFromCityID;
        }

        public void setRMFromCityID(Integer rMFromCityID) {
            this.rMFromCityID = rMFromCityID;
        }

        public Integer getJMJourneyFrom() {
            return jMJourneyFrom;
        }

        public void setJMJourneyFrom(Integer jMJourneyFrom) {
            this.jMJourneyFrom = jMJourneyFrom;
        }

        public Integer getJMJourneyTo() {
            return jMJourneyTo;
        }

        public void setJMJourneyTo(Integer jMJourneyTo) {
            this.jMJourneyTo = jMJourneyTo;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public String getRMRouteNameDisplay() {
            return rMRouteNameDisplay;
        }

        public void setRMRouteNameDisplay(String rMRouteNameDisplay) {
            this.rMRouteNameDisplay = rMRouteNameDisplay;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getFromCityToCity() {
            return fromCityToCity;
        }

        public void setFromCityToCity(String fromCityToCity) {
            this.fromCityToCity = fromCityToCity;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMAddress() {
            return cMAddress;
        }

        public void setCMAddress(String cMAddress) {
            this.cMAddress = cMAddress;
        }

        public String getCMCompanyShortName() {
            return cMCompanyShortName;
        }

        public void setCMCompanyShortName(String cMCompanyShortName) {
            this.cMCompanyShortName = cMCompanyShortName;
        }

        public String getTmp1() {
            return tmp1;
        }

        public void setTmp1(String tmp1) {
            this.tmp1 = tmp1;
        }

        public String getTmp2() {
            return tmp2;
        }

        public void setTmp2(String tmp2) {
            this.tmp2 = tmp2;
        }

        public String getTmp3() {
            return tmp3;
        }

        public void setTmp3(String tmp3) {
            this.tmp3 = tmp3;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
