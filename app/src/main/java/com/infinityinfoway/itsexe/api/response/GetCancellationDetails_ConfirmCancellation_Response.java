package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCancellationDetails_ConfirmCancellation_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("GetCancellationDetail")
        @Expose
        private List<GetCancellationDetail> getCancellationDetail = null;
        @SerializedName("FinalCancel")
        @Expose
        private List<FinalCancel> finalCancel = null;

        public List<GetCancellationDetail> getGetCancellationDetail() {
            return getCancellationDetail;
        }

        public void setGetCancellationDetail(List<GetCancellationDetail> getCancellationDetail) {
            this.getCancellationDetail = getCancellationDetail;
        }

        public List<FinalCancel> getFinalCancel() {
            return finalCancel;
        }

        public void setFinalCancel(List<FinalCancel> finalCancel) {
            this.finalCancel = finalCancel;
        }

    }

    public class FinalCancel {

        @SerializedName("CancelStatus")
        @Expose
        private int cancelStatus;
        @SerializedName("ModifyBranchUser")
        @Expose
        private String modifyBranchUser;

        public int getCancelStatus() {
            return cancelStatus;
        }

        public void setCancelStatus(int cancelStatus) {
            this.cancelStatus = cancelStatus;
        }

        public String getModifyBranchUser() {
            return modifyBranchUser;
        }

        public void setModifyBranchUser(String modifyBranchUser) {
            this.modifyBranchUser = modifyBranchUser;
        }

    }

    public class GetCancellationDetail {

        @SerializedName("Status")
        @Expose
        private int status;
        @SerializedName("DiffMin")
        @Expose
        private String diffMin;
        @SerializedName("JM_RefundCharges")
        @Expose
        private Double jMRefundCharges;
        @SerializedName("DeductPer")
        @Expose
        private String deductPer;
        @SerializedName("DiffMinCancel")
        @Expose
        private String diffMinCancel;
        @SerializedName("StatusMsg")
        @Expose
        private String statusMsg;
        @SerializedName("CancellationCharge")
        @Expose
        private String cancellationCharge;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getDiffMin() {
            return diffMin;
        }

        public void setDiffMin(String diffMin) {
            this.diffMin = diffMin;
        }

        public Double getJMRefundCharges() {
            return jMRefundCharges;
        }

        public void setJMRefundCharges(Double jMRefundCharges) {
            this.jMRefundCharges = jMRefundCharges;
        }

        public String getDeductPer() {
            return deductPer;
        }

        public void setDeductPer(String deductPer) {
            this.deductPer = deductPer;
        }

        public String getDiffMinCancel() {
            return diffMinCancel;
        }

        public void setDiffMinCancel(String diffMinCancel) {
            this.diffMinCancel = diffMinCancel;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

        public String getCancellationCharge() {
            return cancellationCharge;
        }

        public void setCancellationCharge(String cancellationCharge) {
            this.cancellationCharge = cancellationCharge;
        }

    }

}
