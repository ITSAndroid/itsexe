package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Searching_PrintGSTInvoice_Request {
    @SerializedName("JM_PNRNO")
    @Expose
    private String jMPNRNO;
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;

    public String getJMPNRNO() {
        return jMPNRNO;
    }

    public void setJMPNRNO(String jMPNRNO) {
        this.jMPNRNO = jMPNRNO;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Get_Searching_PrintGSTInvoice_Request(String jMPNRNO, Integer companyID) {
        this.jMPNRNO = jMPNRNO;
        this.companyID = companyID;
    }
}
