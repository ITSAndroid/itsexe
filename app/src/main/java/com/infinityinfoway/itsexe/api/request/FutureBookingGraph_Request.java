package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FutureBookingGraph_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyStartDate")
    @Expose
    private String jMJourneyStartDate;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("PageIndex")
    @Expose
    private Integer pageIndex;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getJMJourneyStartDate() {
        return jMJourneyStartDate;
    }

    public void setJMJourneyStartDate(String jMJourneyStartDate) {
        this.jMJourneyStartDate = jMJourneyStartDate;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }


    public FutureBookingGraph_Request(Integer cMCompanyID, String jMJourneyStartDate, Integer jMJourneyFrom, Integer jMJourneyTo, Integer pageIndex) {
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyStartDate = jMJourneyStartDate;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.pageIndex = pageIndex;
    }
}
