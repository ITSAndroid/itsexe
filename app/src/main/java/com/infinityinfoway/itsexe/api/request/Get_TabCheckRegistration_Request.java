package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_TabCheckRegistration_Request {
    @SerializedName("DeviceID")
    @Expose
    private String deviceID;
    @SerializedName("TBRM_ID")
    @Expose
    private String tBRMID;

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTBRMID() {
        return tBRMID;
    }

    public void setTBRMID(String tBRMID) {
        this.tBRMID = tBRMID;
    }

    public Get_TabCheckRegistration_Request(String deviceID, String tBRMID) {
        this.deviceID = deviceID;
        this.tBRMID = tBRMID;
    }
}
