package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_TabCheckRegistration_Response {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("Registration")
        @Expose
        private List<Registration> registration = null;
        @SerializedName("RegistrationStatus")
        @Expose
        private Integer registrationStatus;
        @SerializedName("RegistrationMessage")
        @Expose
        private String registrationMessage;

        public List<Registration> getRegistration() {
            return registration;
        }

        public void setRegistration(List<Registration> registration) {
            this.registration = registration;
        }

        public Integer getRegistrationStatus() {
            return registrationStatus;
        }

        public void setRegistrationStatus(Integer registrationStatus) {
            this.registrationStatus = registrationStatus;
        }

        public String getRegistrationMessage() {
            return registrationMessage;
        }

        public void setRegistrationMessage(String registrationMessage) {
            this.registrationMessage = registrationMessage;
        }


    }

    public class Registration {

        @SerializedName("TBRM_ID")
        @Expose
        private Integer tBRMID;
        @SerializedName("TBRM_OSDeviceID")
        @Expose
        private String tBRMOSDeviceID;
        @SerializedName("TBRM_OSDeviceModel")
        @Expose
        private String tBRMOSDeviceModel;
        @SerializedName("TBRM_OSDeviceMake")
        @Expose
        private String tBRMOSDeviceMake;
        @SerializedName("TBRM_OSDeviceProduct")
        @Expose
        private String tBRMOSDeviceProduct;
        @SerializedName("TBRM_IsActive")
        @Expose
        private Integer tBRMIsActive;
        @SerializedName("TBRM_OSMobileNo")
        @Expose
        private String tBRMOSMobileNo;
        @SerializedName("TBRM_VersionName")
        @Expose
        private String tBRMVersionName;
        @SerializedName("TBRM_VersionCode")
        @Expose
        private Integer tBRMVersionCode;

        public Integer getTBRMID() {
            return tBRMID;
        }

        public void setTBRMID(Integer tBRMID) {
            this.tBRMID = tBRMID;
        }

        public String getTBRMOSDeviceID() {
            return tBRMOSDeviceID;
        }

        public void setTBRMOSDeviceID(String tBRMOSDeviceID) {
            this.tBRMOSDeviceID = tBRMOSDeviceID;
        }

        public String getTBRMOSDeviceModel() {
            return tBRMOSDeviceModel;
        }

        public void setTBRMOSDeviceModel(String tBRMOSDeviceModel) {
            this.tBRMOSDeviceModel = tBRMOSDeviceModel;
        }

        public String getTBRMOSDeviceMake() {
            return tBRMOSDeviceMake;
        }

        public void setTBRMOSDeviceMake(String tBRMOSDeviceMake) {
            this.tBRMOSDeviceMake = tBRMOSDeviceMake;
        }

        public String getTBRMOSDeviceProduct() {
            return tBRMOSDeviceProduct;
        }

        public void setTBRMOSDeviceProduct(String tBRMOSDeviceProduct) {
            this.tBRMOSDeviceProduct = tBRMOSDeviceProduct;
        }

        public Integer getTBRMIsActive() {
            return tBRMIsActive;
        }

        public void setTBRMIsActive(Integer tBRMIsActive) {
            this.tBRMIsActive = tBRMIsActive;
        }

        public String getTBRMOSMobileNo() {
            return tBRMOSMobileNo;
        }

        public void setTBRMOSMobileNo(String tBRMOSMobileNo) {
            this.tBRMOSMobileNo = tBRMOSMobileNo;
        }

        public String getTBRMVersionName() {
            return tBRMVersionName;
        }

        public void setTBRMVersionName(String tBRMVersionName) {
            this.tBRMVersionName = tBRMVersionName;
        }

        public Integer getTBRMVersionCode() {
            return tBRMVersionCode;
        }

        public void setTBRMVersionCode(Integer tBRMVersionCode) {
            this.tBRMVersionCode = tBRMVersionCode;
        }

    }

}
