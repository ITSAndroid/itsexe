package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_MenuOperation_Request {

    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("JourneyStartDate")
    @Expose
    private String journeyStartDate;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;
    @SerializedName("UserID")
    @Expose
    private Integer userID;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Get_MenuOperation_Request(Integer companyID, String journeyStartDate, Integer branchID, Integer userID) {
        this.companyID = companyID;
        this.journeyStartDate = journeyStartDate;
        this.branchID = branchID;
        this.userID = userID;
    }
}
