package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_API_EXE_RouteTimeXML_Request {
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Get_API_EXE_RouteTimeXML_Request(Integer cMCompanyID, Integer bMBranchID) {
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
    }
}
