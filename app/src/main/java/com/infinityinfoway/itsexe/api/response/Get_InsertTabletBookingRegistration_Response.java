package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_InsertTabletBookingRegistration_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("InsertTabletBookingRegistration")
        @Expose
        private List<InsertTabletBookingRegistration> insertTabletBookingRegistration = null;

        public List<InsertTabletBookingRegistration> getInsertTabletBookingRegistration() {
            return insertTabletBookingRegistration;
        }

        public void setInsertTabletBookingRegistration(List<InsertTabletBookingRegistration> insertTabletBookingRegistration) {
            this.insertTabletBookingRegistration = insertTabletBookingRegistration;
        }

    }

    public class InsertTabletBookingRegistration {

        @SerializedName("TDRM_ID")
        @Expose
        private String tDRMID;
        @SerializedName("StatusMsg")
        @Expose
        private String statusMsg;

        public String getTDRMID() {
            return tDRMID;
        }

        public void setTDRMID(String tDRMID) {
            this.tDRMID = tDRMID;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

    }

}
