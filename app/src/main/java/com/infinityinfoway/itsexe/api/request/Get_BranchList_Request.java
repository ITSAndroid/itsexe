package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_BranchList_Request {
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("TBRM_ID")
    @Expose
    private String tBRMID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public String getTBRMID() {
        return tBRMID;
    }

    public void setTBRMID(String tBRMID) {
        this.tBRMID = tBRMID;
    }

    public Get_BranchList_Request(Integer cMCompanyID, String tBRMID) {
        this.cMCompanyID = cMCompanyID;
        this.tBRMID = tBRMID;
    }
}
