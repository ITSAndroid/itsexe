package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoutePickupDropChart_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("JM_PNRNo")
        @Expose
        private String jMPNRNo;
        @SerializedName("PassengerName")
        @Expose
        private String passengerName;
        @SerializedName("PhoneNo")
        @Expose
        private String phoneNo;
        @SerializedName("PickUpName")
        @Expose
        private String pickUpName;
        @SerializedName("DropName")
        @Expose
        private String dropName;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAdd")
        @Expose
        private String companyAdd;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;
        @SerializedName("BookedBy")
        @Expose
        private String bookedBy;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("Header")
        @Expose
        private String header;
        @SerializedName("JP_Age")
        @Expose
        private String jPAge;
        @SerializedName("JP_Temperature")
        @Expose
        private String jPTemperature;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getJMPNRNo() {
            return jMPNRNo;
        }

        public void setJMPNRNo(String jMPNRNo) {
            this.jMPNRNo = jMPNRNo;
        }

        public String getPassengerName() {
            return passengerName;
        }

        public void setPassengerName(String passengerName) {
            this.passengerName = passengerName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getPickUpName() {
            return pickUpName;
        }

        public void setPickUpName(String pickUpName) {
            this.pickUpName = pickUpName;
        }

        public String getDropName() {
            return dropName;
        }

        public void setDropName(String dropName) {
            this.dropName = dropName;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAdd() {
            return companyAdd;
        }

        public void setCompanyAdd(String companyAdd) {
            this.companyAdd = companyAdd;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

        public String getBookedBy() {
            return bookedBy;
        }

        public void setBookedBy(String bookedBy) {
            this.bookedBy = bookedBy;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }

        public String getJPAge() {
            return jPAge;
        }

        public void setJPAge(String jPAge) {
            this.jPAge = jPAge;
        }

        public String getJPTemperature() {
            return jPTemperature;
        }

        public void setJPTemperature(String jPTemperature) {
            this.jPTemperature = jPTemperature;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

    }

}
