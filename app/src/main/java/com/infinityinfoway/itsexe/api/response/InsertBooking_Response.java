package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InsertBooking_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("RouteTimeStatus")
        @Expose
        private String routeTimeStatus;
        @SerializedName("CheckBAM_ArrangementID")
        @Expose
        private String checkBAMArrangementID;
        @SerializedName("CheckRT_TimeChange")
        @Expose
        private String checkRTTimeChange;
        @SerializedName("BookingStatus")
        @Expose
        private Integer bookingStatus;
        // Wallet Booking Response
        @SerializedName("Status")
        @Expose
        private String Status;
        @SerializedName("Message")
        @Expose
        private String Message;

        public String getstatus() {
            return status;
        }

        public void setstatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            this.Status = status;
        }

        public String getRouteTimeStatus() {
            return routeTimeStatus;
        }

        public void setRouteTimeStatus(String routeTimeStatus) {
            this.routeTimeStatus = routeTimeStatus;
        }

        public String getCheckBAMArrangementID() {
            return checkBAMArrangementID;
        }

        public void setCheckBAMArrangementID(String checkBAMArrangementID) {
            this.checkBAMArrangementID = checkBAMArrangementID;
        }

        public String getCheckRTTimeChange() {
            return checkRTTimeChange;
        }

        public void setCheckRTTimeChange(String checkRTTimeChange) {
            this.checkRTTimeChange = checkRTTimeChange;
        }

        public Integer getBookingStatus() {
            return bookingStatus;
        }

        public void setBookingStatus(Integer bookingStatus) {
            this.bookingStatus = bookingStatus;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String message) {
            Message = message;
        }
    }
}
