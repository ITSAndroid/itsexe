package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityWiseSeatChartReport_Request {
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("JourneyDate")
    @Expose
    private String journeyDate;
    @SerializedName("FromCityID")
    @Expose
    private Integer fromCityID;
    @SerializedName("BM_CityID")
    @Expose
    private Integer bMCityID;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;
    @SerializedName("BM_BranchUserID")
    @Expose
    private Integer bMBranchUserID;
    @SerializedName("PSI_IsSameDay")
    @Expose
    private String pSIIsSameDay;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public Integer getFromCityID() {
        return fromCityID;
    }

    public void setFromCityID(Integer fromCityID) {
        this.fromCityID = fromCityID;
    }

    public Integer getBMCityID() {
        return bMCityID;
    }

    public void setBMCityID(Integer bMCityID) {
        this.bMCityID = bMCityID;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public Integer getBMBranchUserID() {
        return bMBranchUserID;
    }

    public void setBMBranchUserID(Integer bMBranchUserID) {
        this.bMBranchUserID = bMBranchUserID;
    }

    public String getPSIIsSameDay() {
        return pSIIsSameDay;
    }

    public void setPSIIsSameDay(String pSIIsSameDay) {
        this.pSIIsSameDay = pSIIsSameDay;
    }

    public CityWiseSeatChartReport_Request(Integer cMCompanyID, Integer rMRouteID, Integer rTTime, String journeyDate, Integer fromCityID, Integer bMCityID, Integer jMBookedByCMCompanyID, Integer bMBranchUserID, String pSIIsSameDay) {
        this.cMCompanyID = cMCompanyID;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.journeyDate = journeyDate;
        this.fromCityID = fromCityID;
        this.bMCityID = bMCityID;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        this.bMBranchUserID = bMBranchUserID;
        this.pSIIsSameDay = pSIIsSameDay;
    }
}
