package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_SearchingSeatNo_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("SearchingSeatNo")
        @Expose
        private List<SearchingSeatNo> searchingSeatNo = null;
        @SerializedName("Table1")
        @Expose
        private List<Table1> table1 = null;

        public List<SearchingSeatNo> getSearchingSeatNo() {
            return searchingSeatNo;
        }

        public void setSearchingSeatNo(List<SearchingSeatNo> searchingSeatNo) {
            this.searchingSeatNo = searchingSeatNo;
        }

        public List<Table1> getTable1() {
            return table1;
        }

        public void setTable1(List<Table1> table1) {
            this.table1 = table1;
        }

    }

    public class SearchingSeatNo {

        @SerializedName("StatusMsg")
        @Expose
        private String statusMsg;

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

    }

    public class Table1 {

        @SerializedName("JM_PNRNO")
        @Expose
        private Integer jMPNRNO;
        @SerializedName("JM_BookingDateTime")
        @Expose
        private String jMBookingDateTime;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_JourneyStartTime")
        @Expose
        private String jMJourneyStartTime;
        @SerializedName("JM_JourneyCityTime")
        @Expose
        private String jMJourneyCityTime;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("JM_JourneyEndDate")
        @Expose
        private String jMJourneyEndDate;
        @SerializedName("JM_JourneyEndTime")
        @Expose
        private String jMJourneyEndTime;
        @SerializedName("JM_JourneyFrom")
        @Expose
        private Integer jMJourneyFrom;
        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("JM_JourneyTo")
        @Expose
        private Integer jMJourneyTo;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_Time")
        @Expose
        private Integer rTTime;
        @SerializedName("RT_RouteFrequency")
        @Expose
        private Integer rTRouteFrequency;
        @SerializedName("JM_ReportingDate")
        @Expose
        private String jMReportingDate;
        @SerializedName("JM_ReportingTime")
        @Expose
        private String jMReportingTime;
        @SerializedName("PM_PickupID")
        @Expose
        private Integer pMPickupID;
        @SerializedName("DM_DropID")
        @Expose
        private Integer dMDropID;
        @SerializedName("BTM_BookingTypeID")
        @Expose
        private Integer bTMBookingTypeID;
        @SerializedName("JM_TicketNo")
        @Expose
        private Integer jMTicketNo;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("UM_UserID")
        @Expose
        private Integer uMUserID;
        @SerializedName("JM_TicketStatus")
        @Expose
        private Integer jMTicketStatus;
        @SerializedName("JM_JourneyStatus")
        @Expose
        private Integer jMJourneyStatus;
        @SerializedName("PM_PaymentMode")
        @Expose
        private Integer pMPaymentMode;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private Integer jMTotalPassengers;
        @SerializedName("BTM_BusTypeID")
        @Expose
        private String bTMBusTypeID;
        @SerializedName("BAM_ArrangementID")
        @Expose
        private Integer bAMArrangementID;
        @SerializedName("RM_ACSeatRate")
        @Expose
        private Integer rMACSeatRate;
        @SerializedName("RM_ACSleeperRate")
        @Expose
        private Integer rMACSleeperRate;
        @SerializedName("RM_ACSlumberRate")
        @Expose
        private Integer rMACSlumberRate;
        @SerializedName("RM_NonACSeatRate")
        @Expose
        private Integer rMNonACSeatRate;
        @SerializedName("RM_NonACSleeperRate")
        @Expose
        private Integer rMNonACSleeperRate;
        @SerializedName("RM_NonAcSlumberRate")
        @Expose
        private Integer rMNonAcSlumberRate;
        @SerializedName("RM_ACSeatQuantity")
        @Expose
        private Integer rMACSeatQuantity;
        @SerializedName("RM_ACSleeperQuantity")
        @Expose
        private Integer rMACSleeperQuantity;
        @SerializedName("RM_ACSlumberQuantity")
        @Expose
        private Integer rMACSlumberQuantity;
        @SerializedName("RM_NonACSeatQuantity")
        @Expose
        private Integer rMNonACSeatQuantity;
        @SerializedName("RM_NonACSleeperQuantity")
        @Expose
        private Integer rMNonACSleeperQuantity;
        @SerializedName("RM_NonACSlumberQuantity")
        @Expose
        private Integer rMNonACSlumberQuantity;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("JM_Phone2")
        @Expose
        private String jMPhone2;
        @SerializedName("JM_EmailID")
        @Expose
        private String jMEmailID;
        @SerializedName("JM_Remarks")
        @Expose
        private String jMRemarks;
        @SerializedName("JP_JPID")
        @Expose
        private Integer jPJPID;
        @SerializedName("BAD_SeatID")
        @Expose
        private String bADSeatID;
        @SerializedName("BAD_SeatNo")
        @Expose
        private String bADSeatNo;
        @SerializedName("JP_Gender")
        @Expose
        private String jPGender;
        @SerializedName("JP_PassengerName")
        @Expose
        private String jPPassengerName;
        @SerializedName("JP_PhoneNo")
        @Expose
        private String jPPhoneNo;
        @SerializedName("JP_Amount")
        @Expose
        private Double jPAmount;
        @SerializedName("JP_Discount")
        @Expose
        private Double jPDiscount;
        @SerializedName("CAD_BusType")
        @Expose
        private Integer cADBusType;
        @SerializedName("CAD_SeatType")
        @Expose
        private Integer cADSeatType;
        @SerializedName("PH_PHID")
        @Expose
        private Integer pHPHID;
        @SerializedName("PH_CardHolderName")
        @Expose
        private String pHCardHolderName;
        @SerializedName("PH_CardID")
        @Expose
        private String pHCardID;
        @SerializedName("PH_CardProvidorID")
        @Expose
        private Integer pHCardProvidorID;
        @SerializedName("PH_TransStatus")
        @Expose
        private Integer pHTransStatus;
        @SerializedName("PH_DateTime")
        @Expose
        private String pHDateTime;
        @SerializedName("PH_IPAddress")
        @Expose
        private String pHIPAddress;
        @SerializedName("UM_UserID1")
        @Expose
        private Integer uMUserID1;
        @SerializedName("BM_BranchID1")
        @Expose
        private Integer bMBranchID1;
        @SerializedName("PH_Amount")
        @Expose
        private Double pHAmount;
        @SerializedName("PickUpTime")
        @Expose
        private String pickUpTime;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;
        @SerializedName("RM_RouteNameDisplay")
        @Expose
        private String rMRouteNameDisplay;
        @SerializedName("CAM_IsOnline")
        @Expose
        private Integer cAMIsOnline;
        @SerializedName("JM_EmailID1")
        @Expose
        private String jMEmailID1;
        @SerializedName("JM_IsTicketPrint")
        @Expose
        private Integer jMIsTicketPrint;
        @SerializedName("JM_ChartFinish")
        @Expose
        private Integer jMChartFinish;
        @SerializedName("JM_PhoneOnHold")
        @Expose
        private Integer jMPhoneOnHold;
        @SerializedName("BM_IsPrepaid")
        @Expose
        private Integer bMIsPrepaid;
        @SerializedName("JM_BookedBy_CM_CompanyID")
        @Expose
        private Integer jMBookedByCMCompanyID;
        @SerializedName("AgentBranchCityID")
        @Expose
        private Integer agentBranchCityID;
        @SerializedName("AgentBranchCityName")
        @Expose
        private String agentBranchCityName;
        @SerializedName("AgentBranchID")
        @Expose
        private Integer agentBranchID;
        @SerializedName("AgentBranchName")
        @Expose
        private String agentBranchName;
        @SerializedName("JM_OriginalTotalPayable")
        @Expose
        private Double jMOriginalTotalPayable;
        @SerializedName("ServiceTaxAmt")
        @Expose
        private Double serviceTaxAmt;
        @SerializedName("JM_ServiceTax")
        @Expose
        private Double jMServiceTax;
        @SerializedName("JM_RoundUP")
        @Expose
        private Double jMRoundUP;
        @SerializedName("JM_IsIncludeTax")
        @Expose
        private Integer jMIsIncludeTax;
        @SerializedName("RT_BusCategoryID")
        @Expose
        private Integer rTBusCategoryID;
        @SerializedName("JP_ServiceTax")
        @Expose
        private Double jPServiceTax;
        @SerializedName("JP_RoundUP")
        @Expose
        private Double jPRoundUP;
        @SerializedName("JM_IsPrepaidCard")
        @Expose
        private Integer jMIsPrepaidCard;
        @SerializedName("JM_B2C_OrderNo")
        @Expose
        private String jMB2COrderNo;
        @SerializedName("WalletDiscount")
        @Expose
        private Double walletDiscount;
        @SerializedName("CNM_IsWallet")
        @Expose
        private Integer cNMIsWallet;
        @SerializedName("JM_TDRCharge")
        @Expose
        private Double jMTDRCharge;
        @SerializedName("JM_PGType")
        @Expose
        private Integer jMPGType;
        @SerializedName("DeductTDRCharges")
        @Expose
        private Double deductTDRCharges;
        @SerializedName("JM_MagicBoxCharges")
        @Expose
        private Double jMMagicBoxCharges;
        @SerializedName("JM_GSTState")
        @Expose
        private Integer jMGSTState;
        @SerializedName("JM_GSTCompanyName")
        @Expose
        private String jMGSTCompanyName;
        @SerializedName("JM_GSTRegNo")
        @Expose
        private String jMGSTRegNo;
        @SerializedName("IsAllowModify")
        @Expose
        private Integer isAllowModify;
        @SerializedName("IsDeductBalanceOnPhoneBooking")
        @Expose
        private Integer isDeductBalanceOnPhoneBooking;

        public Integer getJMPNRNO() {
            return jMPNRNO;
        }

        public void setJMPNRNO(Integer jMPNRNO) {
            this.jMPNRNO = jMPNRNO;
        }

        public String getJMBookingDateTime() {
            return jMBookingDateTime;
        }

        public void setJMBookingDateTime(String jMBookingDateTime) {
            this.jMBookingDateTime = jMBookingDateTime;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMJourneyStartTime() {
            return jMJourneyStartTime;
        }

        public void setJMJourneyStartTime(String jMJourneyStartTime) {
            this.jMJourneyStartTime = jMJourneyStartTime;
        }

        public String getJMJourneyCityTime() {
            return jMJourneyCityTime;
        }

        public void setJMJourneyCityTime(String jMJourneyCityTime) {
            this.jMJourneyCityTime = jMJourneyCityTime;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getJMJourneyEndDate() {
            return jMJourneyEndDate;
        }

        public void setJMJourneyEndDate(String jMJourneyEndDate) {
            this.jMJourneyEndDate = jMJourneyEndDate;
        }

        public String getJMJourneyEndTime() {
            return jMJourneyEndTime;
        }

        public void setJMJourneyEndTime(String jMJourneyEndTime) {
            this.jMJourneyEndTime = jMJourneyEndTime;
        }

        public Integer getJMJourneyFrom() {
            return jMJourneyFrom;
        }

        public void setJMJourneyFrom(Integer jMJourneyFrom) {
            this.jMJourneyFrom = jMJourneyFrom;
        }

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public Integer getJMJourneyTo() {
            return jMJourneyTo;
        }

        public void setJMJourneyTo(Integer jMJourneyTo) {
            this.jMJourneyTo = jMJourneyTo;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTTime() {
            return rTTime;
        }

        public void setRTTime(Integer rTTime) {
            this.rTTime = rTTime;
        }

        public Integer getRTRouteFrequency() {
            return rTRouteFrequency;
        }

        public void setRTRouteFrequency(Integer rTRouteFrequency) {
            this.rTRouteFrequency = rTRouteFrequency;
        }

        public String getJMReportingDate() {
            return jMReportingDate;
        }

        public void setJMReportingDate(String jMReportingDate) {
            this.jMReportingDate = jMReportingDate;
        }

        public String getJMReportingTime() {
            return jMReportingTime;
        }

        public void setJMReportingTime(String jMReportingTime) {
            this.jMReportingTime = jMReportingTime;
        }

        public Integer getPMPickupID() {
            return pMPickupID;
        }

        public void setPMPickupID(Integer pMPickupID) {
            this.pMPickupID = pMPickupID;
        }

        public Integer getDMDropID() {
            return dMDropID;
        }

        public void setDMDropID(Integer dMDropID) {
            this.dMDropID = dMDropID;
        }

        public Integer getBTMBookingTypeID() {
            return bTMBookingTypeID;
        }

        public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
            this.bTMBookingTypeID = bTMBookingTypeID;
        }

        public Integer getJMTicketNo() {
            return jMTicketNo;
        }

        public void setJMTicketNo(Integer jMTicketNo) {
            this.jMTicketNo = jMTicketNo;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public Integer getUMUserID() {
            return uMUserID;
        }

        public void setUMUserID(Integer uMUserID) {
            this.uMUserID = uMUserID;
        }

        public Integer getJMTicketStatus() {
            return jMTicketStatus;
        }

        public void setJMTicketStatus(Integer jMTicketStatus) {
            this.jMTicketStatus = jMTicketStatus;
        }

        public Integer getJMJourneyStatus() {
            return jMJourneyStatus;
        }

        public void setJMJourneyStatus(Integer jMJourneyStatus) {
            this.jMJourneyStatus = jMJourneyStatus;
        }

        public Integer getPMPaymentMode() {
            return pMPaymentMode;
        }

        public void setPMPaymentMode(Integer pMPaymentMode) {
            this.pMPaymentMode = pMPaymentMode;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public Integer getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(Integer jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public String getBTMBusTypeID() {
            return bTMBusTypeID;
        }

        public void setBTMBusTypeID(String bTMBusTypeID) {
            this.bTMBusTypeID = bTMBusTypeID;
        }

        public Integer getBAMArrangementID() {
            return bAMArrangementID;
        }

        public void setBAMArrangementID(Integer bAMArrangementID) {
            this.bAMArrangementID = bAMArrangementID;
        }

        public Integer getRMACSeatRate() {
            return rMACSeatRate;
        }

        public void setRMACSeatRate(Integer rMACSeatRate) {
            this.rMACSeatRate = rMACSeatRate;
        }

        public Integer getRMACSleeperRate() {
            return rMACSleeperRate;
        }

        public void setRMACSleeperRate(Integer rMACSleeperRate) {
            this.rMACSleeperRate = rMACSleeperRate;
        }

        public Integer getRMACSlumberRate() {
            return rMACSlumberRate;
        }

        public void setRMACSlumberRate(Integer rMACSlumberRate) {
            this.rMACSlumberRate = rMACSlumberRate;
        }

        public Integer getRMNonACSeatRate() {
            return rMNonACSeatRate;
        }

        public void setRMNonACSeatRate(Integer rMNonACSeatRate) {
            this.rMNonACSeatRate = rMNonACSeatRate;
        }

        public Integer getRMNonACSleeperRate() {
            return rMNonACSleeperRate;
        }

        public void setRMNonACSleeperRate(Integer rMNonACSleeperRate) {
            this.rMNonACSleeperRate = rMNonACSleeperRate;
        }

        public Integer getRMNonAcSlumberRate() {
            return rMNonAcSlumberRate;
        }

        public void setRMNonAcSlumberRate(Integer rMNonAcSlumberRate) {
            this.rMNonAcSlumberRate = rMNonAcSlumberRate;
        }

        public Integer getRMACSeatQuantity() {
            return rMACSeatQuantity;
        }

        public void setRMACSeatQuantity(Integer rMACSeatQuantity) {
            this.rMACSeatQuantity = rMACSeatQuantity;
        }

        public Integer getRMACSleeperQuantity() {
            return rMACSleeperQuantity;
        }

        public void setRMACSleeperQuantity(Integer rMACSleeperQuantity) {
            this.rMACSleeperQuantity = rMACSleeperQuantity;
        }

        public Integer getRMACSlumberQuantity() {
            return rMACSlumberQuantity;
        }

        public void setRMACSlumberQuantity(Integer rMACSlumberQuantity) {
            this.rMACSlumberQuantity = rMACSlumberQuantity;
        }

        public Integer getRMNonACSeatQuantity() {
            return rMNonACSeatQuantity;
        }

        public void setRMNonACSeatQuantity(Integer rMNonACSeatQuantity) {
            this.rMNonACSeatQuantity = rMNonACSeatQuantity;
        }

        public Integer getRMNonACSleeperQuantity() {
            return rMNonACSleeperQuantity;
        }

        public void setRMNonACSleeperQuantity(Integer rMNonACSleeperQuantity) {
            this.rMNonACSleeperQuantity = rMNonACSleeperQuantity;
        }

        public Integer getRMNonACSlumberQuantity() {
            return rMNonACSlumberQuantity;
        }

        public void setRMNonACSlumberQuantity(Integer rMNonACSlumberQuantity) {
            this.rMNonACSlumberQuantity = rMNonACSlumberQuantity;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getJMPhone2() {
            return jMPhone2;
        }

        public void setJMPhone2(String jMPhone2) {
            this.jMPhone2 = jMPhone2;
        }

        public String getJMEmailID() {
            return jMEmailID;
        }

        public void setJMEmailID(String jMEmailID) {
            this.jMEmailID = jMEmailID;
        }

        public String getJMRemarks() {
            return jMRemarks;
        }

        public void setJMRemarks(String jMRemarks) {
            this.jMRemarks = jMRemarks;
        }

        public Integer getJPJPID() {
            return jPJPID;
        }

        public void setJPJPID(Integer jPJPID) {
            this.jPJPID = jPJPID;
        }

        public String getBADSeatID() {
            return bADSeatID;
        }

        public void setBADSeatID(String bADSeatID) {
            this.bADSeatID = bADSeatID;
        }

        public String getBADSeatNo() {
            return bADSeatNo;
        }

        public void setBADSeatNo(String bADSeatNo) {
            this.bADSeatNo = bADSeatNo;
        }

        public String getJPGender() {
            return jPGender;
        }

        public void setJPGender(String jPGender) {
            this.jPGender = jPGender;
        }

        public String getJPPassengerName() {
            return jPPassengerName;
        }

        public void setJPPassengerName(String jPPassengerName) {
            this.jPPassengerName = jPPassengerName;
        }

        public String getJPPhoneNo() {
            return jPPhoneNo;
        }

        public void setJPPhoneNo(String jPPhoneNo) {
            this.jPPhoneNo = jPPhoneNo;
        }

        public Double getJPAmount() {
            return jPAmount;
        }

        public void setJPAmount(Double jPAmount) {
            this.jPAmount = jPAmount;
        }

        public Double getJPDiscount() {
            return jPDiscount;
        }

        public void setJPDiscount(Double jPDiscount) {
            this.jPDiscount = jPDiscount;
        }

        public Integer getCADBusType() {
            return cADBusType;
        }

        public void setCADBusType(Integer cADBusType) {
            this.cADBusType = cADBusType;
        }

        public Integer getCADSeatType() {
            return cADSeatType;
        }

        public void setCADSeatType(Integer cADSeatType) {
            this.cADSeatType = cADSeatType;
        }

        public Integer getPHPHID() {
            return pHPHID;
        }

        public void setPHPHID(Integer pHPHID) {
            this.pHPHID = pHPHID;
        }

        public String getPHCardHolderName() {
            return pHCardHolderName;
        }

        public void setPHCardHolderName(String pHCardHolderName) {
            this.pHCardHolderName = pHCardHolderName;
        }

        public String getPHCardID() {
            return pHCardID;
        }

        public void setPHCardID(String pHCardID) {
            this.pHCardID = pHCardID;
        }

        public Integer getPHCardProvidorID() {
            return pHCardProvidorID;
        }

        public void setPHCardProvidorID(Integer pHCardProvidorID) {
            this.pHCardProvidorID = pHCardProvidorID;
        }

        public Integer getPHTransStatus() {
            return pHTransStatus;
        }

        public void setPHTransStatus(Integer pHTransStatus) {
            this.pHTransStatus = pHTransStatus;
        }

        public String getPHDateTime() {
            return pHDateTime;
        }

        public void setPHDateTime(String pHDateTime) {
            this.pHDateTime = pHDateTime;
        }

        public String getPHIPAddress() {
            return pHIPAddress;
        }

        public void setPHIPAddress(String pHIPAddress) {
            this.pHIPAddress = pHIPAddress;
        }

        public Integer getUMUserID1() {
            return uMUserID1;
        }

        public void setUMUserID1(Integer uMUserID1) {
            this.uMUserID1 = uMUserID1;
        }

        public Integer getBMBranchID1() {
            return bMBranchID1;
        }

        public void setBMBranchID1(Integer bMBranchID1) {
            this.bMBranchID1 = bMBranchID1;
        }

        public Double getPHAmount() {
            return pHAmount;
        }

        public void setPHAmount(Double pHAmount) {
            this.pHAmount = pHAmount;
        }

        public String getPickUpTime() {
            return pickUpTime;
        }

        public void setPickUpTime(String pickUpTime) {
            this.pickUpTime = pickUpTime;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

        public String getRMRouteNameDisplay() {
            return rMRouteNameDisplay;
        }

        public void setRMRouteNameDisplay(String rMRouteNameDisplay) {
            this.rMRouteNameDisplay = rMRouteNameDisplay;
        }

        public Integer getCAMIsOnline() {
            return cAMIsOnline;
        }

        public void setCAMIsOnline(Integer cAMIsOnline) {
            this.cAMIsOnline = cAMIsOnline;
        }

        public String getJMEmailID1() {
            return jMEmailID1;
        }

        public void setJMEmailID1(String jMEmailID1) {
            this.jMEmailID1 = jMEmailID1;
        }

        public Integer getJMIsTicketPrint() {
            return jMIsTicketPrint;
        }

        public void setJMIsTicketPrint(Integer jMIsTicketPrint) {
            this.jMIsTicketPrint = jMIsTicketPrint;
        }

        public Integer getJMChartFinish() {
            return jMChartFinish;
        }

        public void setJMChartFinish(Integer jMChartFinish) {
            this.jMChartFinish = jMChartFinish;
        }

        public Integer getJMPhoneOnHold() {
            return jMPhoneOnHold;
        }

        public void setJMPhoneOnHold(Integer jMPhoneOnHold) {
            this.jMPhoneOnHold = jMPhoneOnHold;
        }

        public Integer getBMIsPrepaid() {
            return bMIsPrepaid;
        }

        public void setBMIsPrepaid(Integer bMIsPrepaid) {
            this.bMIsPrepaid = bMIsPrepaid;
        }

        public Integer getJMBookedByCMCompanyID() {
            return jMBookedByCMCompanyID;
        }

        public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
            this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
        }

        public Integer getAgentBranchCityID() {
            return agentBranchCityID;
        }

        public void setAgentBranchCityID(Integer agentBranchCityID) {
            this.agentBranchCityID = agentBranchCityID;
        }

        public String getAgentBranchCityName() {
            return agentBranchCityName;
        }

        public void setAgentBranchCityName(String agentBranchCityName) {
            this.agentBranchCityName = agentBranchCityName;
        }

        public Integer getAgentBranchID() {
            return agentBranchID;
        }

        public void setAgentBranchID(Integer agentBranchID) {
            this.agentBranchID = agentBranchID;
        }

        public String getAgentBranchName() {
            return agentBranchName;
        }

        public void setAgentBranchName(String agentBranchName) {
            this.agentBranchName = agentBranchName;
        }

        public Double getJMOriginalTotalPayable() {
            return jMOriginalTotalPayable;
        }

        public void setJMOriginalTotalPayable(Double jMOriginalTotalPayable) {
            this.jMOriginalTotalPayable = jMOriginalTotalPayable;
        }

        public Double getServiceTaxAmt() {
            return serviceTaxAmt;
        }

        public void setServiceTaxAmt(Double serviceTaxAmt) {
            this.serviceTaxAmt = serviceTaxAmt;
        }

        public Double getJMServiceTax() {
            return jMServiceTax;
        }

        public void setJMServiceTax(Double jMServiceTax) {
            this.jMServiceTax = jMServiceTax;
        }

        public Double getJMRoundUP() {
            return jMRoundUP;
        }

        public void setJMRoundUP(Double jMRoundUP) {
            this.jMRoundUP = jMRoundUP;
        }

        public Integer getJMIsIncludeTax() {
            return jMIsIncludeTax;
        }

        public void setJMIsIncludeTax(Integer jMIsIncludeTax) {
            this.jMIsIncludeTax = jMIsIncludeTax;
        }

        public Integer getRTBusCategoryID() {
            return rTBusCategoryID;
        }

        public void setRTBusCategoryID(Integer rTBusCategoryID) {
            this.rTBusCategoryID = rTBusCategoryID;
        }

        public Double getJPServiceTax() {
            return jPServiceTax;
        }

        public void setJPServiceTax(Double jPServiceTax) {
            this.jPServiceTax = jPServiceTax;
        }

        public Double getJPRoundUP() {
            return jPRoundUP;
        }

        public void setJPRoundUP(Double jPRoundUP) {
            this.jPRoundUP = jPRoundUP;
        }

        public Integer getJMIsPrepaidCard() {
            return jMIsPrepaidCard;
        }

        public void setJMIsPrepaidCard(Integer jMIsPrepaidCard) {
            this.jMIsPrepaidCard = jMIsPrepaidCard;
        }

        public String getJMB2COrderNo() {
            return jMB2COrderNo;
        }

        public void setJMB2COrderNo(String jMB2COrderNo) {
            this.jMB2COrderNo = jMB2COrderNo;
        }

        public Double getWalletDiscount() {
            return walletDiscount;
        }

        public void setWalletDiscount(Double walletDiscount) {
            this.walletDiscount = walletDiscount;
        }

        public Integer getCNMIsWallet() {
            return cNMIsWallet;
        }

        public void setCNMIsWallet(Integer cNMIsWallet) {
            this.cNMIsWallet = cNMIsWallet;
        }

        public Double getJMTDRCharge() {
            return jMTDRCharge;
        }

        public void setJMTDRCharge(Double jMTDRCharge) {
            this.jMTDRCharge = jMTDRCharge;
        }

        public Integer getJMPGType() {
            return jMPGType;
        }

        public void setJMPGType(Integer jMPGType) {
            this.jMPGType = jMPGType;
        }

        public Double getDeductTDRCharges() {
            return deductTDRCharges;
        }

        public void setDeductTDRCharges(Double deductTDRCharges) {
            this.deductTDRCharges = deductTDRCharges;
        }

        public Double getJMMagicBoxCharges() {
            return jMMagicBoxCharges;
        }

        public void setJMMagicBoxCharges(Double jMMagicBoxCharges) {
            this.jMMagicBoxCharges = jMMagicBoxCharges;
        }

        public Integer getJMGSTState() {
            return jMGSTState;
        }

        public void setJMGSTState(Integer jMGSTState) {
            this.jMGSTState = jMGSTState;
        }

        public String getJMGSTCompanyName() {
            return jMGSTCompanyName;
        }

        public void setJMGSTCompanyName(String jMGSTCompanyName) {
            this.jMGSTCompanyName = jMGSTCompanyName;
        }

        public String getJMGSTRegNo() {
            return jMGSTRegNo;
        }

        public void setJMGSTRegNo(String jMGSTRegNo) {
            this.jMGSTRegNo = jMGSTRegNo;
        }

        public Integer getIsAllowModify() {
            return isAllowModify;
        }

        public void setIsAllowModify(Integer isAllowModify) {
            this.isAllowModify = isAllowModify;
        }

        public Integer getIsDeductBalanceOnPhoneBooking() {
            return isDeductBalanceOnPhoneBooking;
        }

        public void setIsDeductBalanceOnPhoneBooking(Integer isDeductBalanceOnPhoneBooking) {
            this.isDeductBalanceOnPhoneBooking = isDeductBalanceOnPhoneBooking;
        }

    }


}
