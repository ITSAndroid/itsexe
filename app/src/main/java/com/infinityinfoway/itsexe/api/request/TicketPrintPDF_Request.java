package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketPrintPDF_Request {

    @SerializedName("JM_PNRNO")
    @Expose
    private String jmPnrno;
    @SerializedName("BUM_BranchUserID")
    @Expose
    private Integer bUMBranchUserID;
    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("BAM_ArrangementID")
    @Expose
    private Integer bAMArrangementID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("PM_PickupID")
    @Expose
    private Integer pMPickupID;
    @SerializedName("CompTimeFormat")
    @Expose
    private Integer compTimeFormat;
    @SerializedName("JM_BookedBy_CM_CompanyID")
    @Expose
    private Integer jMBookedByCMCompanyID;

    public String getJmPnrno() {
        return jmPnrno;
    }

    public void setJmPnrno(String jmPnrno) {
        this.jmPnrno = jmPnrno;
    }

    public Integer getBUMBranchUserID() {
        return bUMBranchUserID;
    }

    public void setBUMBranchUserID(Integer bUMBranchUserID) {
        this.bUMBranchUserID = bUMBranchUserID;
    }

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public Integer getBAMArrangementID() {
        return bAMArrangementID;
    }

    public void setBAMArrangementID(Integer bAMArrangementID) {
        this.bAMArrangementID = bAMArrangementID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getPMPickupID() {
        return pMPickupID;
    }

    public void setPMPickupID(Integer pMPickupID) {
        this.pMPickupID = pMPickupID;
    }

    public Integer getCompTimeFormat() {
        return compTimeFormat;
    }

    public void setCompTimeFormat(Integer compTimeFormat) {
        this.compTimeFormat = compTimeFormat;
    }

    public Integer getJMBookedByCMCompanyID() {
        return jMBookedByCMCompanyID;
    }

    public void setJMBookedByCMCompanyID(Integer jMBookedByCMCompanyID) {
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }

    public TicketPrintPDF_Request(String jmPnrno, Integer bUMBranchUserID, Integer cMCompanyID, Integer jMJourneyFrom, Integer rMRouteID, Integer rTTime, Integer bAMArrangementID, Integer bMBranchID, Integer pMPickupID, Integer compTimeFormat, Integer jMBookedByCMCompanyID) {
        this.jmPnrno = jmPnrno;
        this.bUMBranchUserID = bUMBranchUserID;
        this.cMCompanyID = cMCompanyID;
        this.jMJourneyFrom = jMJourneyFrom;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.bAMArrangementID = bAMArrangementID;
        this.bMBranchID = bMBranchID;
        this.pMPickupID = pMPickupID;
        this.compTimeFormat = compTimeFormat;
        this.jMBookedByCMCompanyID = jMBookedByCMCompanyID;
    }
}
