package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Rights_Request {
    @SerializedName("CompanyID")
    @Expose
    private Integer companyID;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;
    @SerializedName("BranchUserID")
    @Expose
    private Integer branchUserID;

    public Integer getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Integer getBranchUserID() {
        return branchUserID;
    }

    public void setBranchUserID(Integer branchUserID) {
        this.branchUserID = branchUserID;
    }

    public Get_Rights_Request(Integer companyID, Integer branchID, Integer branchUserID) {
        this.companyID = companyID;
        this.branchID = branchID;
        this.branchUserID = branchUserID;
    }
}
