package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Report_Booking_Cancel_ReportByRouteTime_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("BookingCancelReportByRouteTime")
        @Expose
        private List<BookingCancelReportByRouteTime> bookingCancelReportByRouteTime = null;

        public List<BookingCancelReportByRouteTime> getBookingCancelReportByRouteTime() {
            return bookingCancelReportByRouteTime;
        }

        public void setBookingCancelReportByRouteTime(List<BookingCancelReportByRouteTime> bookingCancelReportByRouteTime) {
            this.bookingCancelReportByRouteTime = bookingCancelReportByRouteTime;
        }

    }

    public class BookingCancelReportByRouteTime {

        @SerializedName("PNRTKT")
        @Expose
        private String pNRTKT;
        @SerializedName("UM_UserID")
        @Expose
        private String uMUserID;
        @SerializedName("BUM_UserName")
        @Expose
        private String bUMUserName;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("BM_BranchCode")
        @Expose
        private String bMBranchCode;
        @SerializedName("JM_BookingDateTime")
        @Expose
        private String jMBookingDateTime;
        @SerializedName("BTM_BookingTypeID")
        @Expose
        private Integer bTMBookingTypeID;
        @SerializedName("default_name")
        @Expose
        private String defaultName;
        @SerializedName("JM_CancelBy")
        @Expose
        private Integer jMCancelBy;
        @SerializedName("CancelBy")
        @Expose
        private String cancelBy;
        @SerializedName("JM_CancelBy_BranchID")
        @Expose
        private Integer jMCancelByBranchID;
        @SerializedName("CancelBranch")
        @Expose
        private String cancelBranch;
        @SerializedName("JM_CancelDate")
        @Expose
        private String jMCancelDate;
        @SerializedName("JM_Cancel_Remarks")
        @Expose
        private String jMCancelRemarks;
        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("JM_Phone1")
        @Expose
        private String jMPhone1;
        @SerializedName("RT_RouteName")
        @Expose
        private String rTRouteName;
        @SerializedName("JM_JourneyCityTime")
        @Expose
        private String jMJourneyCityTime;
        @SerializedName("Journey")
        @Expose
        private String journey;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_Address")
        @Expose
        private String cMAddress;
        @SerializedName("BookingType")
        @Expose
        private String bookingType;
        @SerializedName("JM_JourneyStartDate")
        @Expose
        private String jMJourneyStartDate;
        @SerializedName("JM_PayableAmount")
        @Expose
        private String jMPayableAmount;
        @SerializedName("GroupHeader")
        @Expose
        private String groupHeader;

        public String getPNRTKT() {
            return pNRTKT;
        }

        public void setPNRTKT(String pNRTKT) {
            this.pNRTKT = pNRTKT;
        }

        public String getUMUserID() {
            return uMUserID;
        }

        public void setUMUserID(String uMUserID) {
            this.uMUserID = uMUserID;
        }

        public String getBUMUserName() {
            return bUMUserName;
        }

        public void setBUMUserName(String bUMUserName) {
            this.bUMUserName = bUMUserName;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public String getBMBranchCode() {
            return bMBranchCode;
        }

        public void setBMBranchCode(String bMBranchCode) {
            this.bMBranchCode = bMBranchCode;
        }

        public String getJMBookingDateTime() {
            return jMBookingDateTime;
        }

        public void setJMBookingDateTime(String jMBookingDateTime) {
            this.jMBookingDateTime = jMBookingDateTime;
        }

        public Integer getBTMBookingTypeID() {
            return bTMBookingTypeID;
        }

        public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
            this.bTMBookingTypeID = bTMBookingTypeID;
        }

        public String getDefaultName() {
            return defaultName;
        }

        public void setDefaultName(String defaultName) {
            this.defaultName = defaultName;
        }

        public Integer getJMCancelBy() {
            return jMCancelBy;
        }

        public void setJMCancelBy(Integer jMCancelBy) {
            this.jMCancelBy = jMCancelBy;
        }

        public String getCancelBy() {
            return cancelBy;
        }

        public void setCancelBy(String cancelBy) {
            this.cancelBy = cancelBy;
        }

        public Integer getJMCancelByBranchID() {
            return jMCancelByBranchID;
        }

        public void setJMCancelByBranchID(Integer jMCancelByBranchID) {
            this.jMCancelByBranchID = jMCancelByBranchID;
        }

        public String getCancelBranch() {
            return cancelBranch;
        }

        public void setCancelBranch(String cancelBranch) {
            this.cancelBranch = cancelBranch;
        }

        public String getJMCancelDate() {
            return jMCancelDate;
        }

        public void setJMCancelDate(String jMCancelDate) {
            this.jMCancelDate = jMCancelDate;
        }

        public String getJMCancelRemarks() {
            return jMCancelRemarks;
        }

        public void setJMCancelRemarks(String jMCancelRemarks) {
            this.jMCancelRemarks = jMCancelRemarks;
        }

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public String getJMPhone1() {
            return jMPhone1;
        }

        public void setJMPhone1(String jMPhone1) {
            this.jMPhone1 = jMPhone1;
        }

        public String getRTRouteName() {
            return rTRouteName;
        }

        public void setRTRouteName(String rTRouteName) {
            this.rTRouteName = rTRouteName;
        }

        public String getJMJourneyCityTime() {
            return jMJourneyCityTime;
        }

        public void setJMJourneyCityTime(String jMJourneyCityTime) {
            this.jMJourneyCityTime = jMJourneyCityTime;
        }

        public String getJourney() {
            return journey;
        }

        public void setJourney(String journey) {
            this.journey = journey;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMAddress() {
            return cMAddress;
        }

        public void setCMAddress(String cMAddress) {
            this.cMAddress = cMAddress;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getJMJourneyStartDate() {
            return jMJourneyStartDate;
        }

        public void setJMJourneyStartDate(String jMJourneyStartDate) {
            this.jMJourneyStartDate = jMJourneyStartDate;
        }

        public String getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(String jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public String getGroupHeader() {
            return groupHeader;
        }

        public void setGroupHeader(String groupHeader) {
            this.groupHeader = groupHeader;
        }

    }


}
