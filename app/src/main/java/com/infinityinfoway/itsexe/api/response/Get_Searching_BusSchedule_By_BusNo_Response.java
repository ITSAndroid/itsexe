package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Searching_BusSchedule_By_BusNo_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("SearchingBusSchedule_By_BusNo")
        @Expose
        private List<SearchingBusScheduleByBusNo> searchingBusScheduleByBusNo = null;

        public List<SearchingBusScheduleByBusNo> getSearchingBusScheduleByBusNo() {
            return searchingBusScheduleByBusNo;
        }

        public void setSearchingBusScheduleByBusNo(List<SearchingBusScheduleByBusNo> searchingBusScheduleByBusNo) {
            this.searchingBusScheduleByBusNo = searchingBusScheduleByBusNo;
        }

    }

    public class SearchingBusScheduleByBusNo {

        @SerializedName("BM_BusID")
        @Expose
        private Integer bMBusID;
        @SerializedName("BM_BusNo")
        @Expose
        private String bMBusNo;
        @SerializedName("RT_RouteName")
        @Expose
        private String rTRouteName;
        @SerializedName("RT_Time")
        @Expose
        private String rTTime;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("DM_Driver1")
        @Expose
        private String dMDriver1;
        @SerializedName("DM_Driver2")
        @Expose
        private String dMDriver2;
        @SerializedName("DM_PhoneNo")
        @Expose
        private String dMPhoneNo;

        public Integer getBMBusID() {
            return bMBusID;
        }

        public void setBMBusID(Integer bMBusID) {
            this.bMBusID = bMBusID;
        }

        public String getBMBusNo() {
            return bMBusNo;
        }

        public void setBMBusNo(String bMBusNo) {
            this.bMBusNo = bMBusNo;
        }

        public String getRTRouteName() {
            return rTRouteName;
        }

        public void setRTRouteName(String rTRouteName) {
            this.rTRouteName = rTRouteName;
        }

        public String getRTTime() {
            return rTTime;
        }

        public void setRTTime(String rTTime) {
            this.rTTime = rTTime;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getDMDriver1() {
            return dMDriver1;
        }

        public void setDMDriver1(String dMDriver1) {
            this.dMDriver1 = dMDriver1;
        }

        public String getDMDriver2() {
            return dMDriver2;
        }

        public void setDMDriver2(String dMDriver2) {
            this.dMDriver2 = dMDriver2;
        }

        public String getDMPhoneNo() {
            return dMPhoneNo;
        }

        public void setDMPhoneNo(String dMPhoneNo) {
            this.dMPhoneNo = dMPhoneNo;
        }

    }

}
