package com.infinityinfoway.itsexe.api.response;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Get_API_EXE_RouteTimeALLXML_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("ServerDate")
        @Expose
        private List<ServerDate> serverDate = null;
        @SerializedName("ITS_FetchBusNumber")
        @Expose
        private List<ITSFetchBusNumber> iTSFetchBusNumber = null;
        @SerializedName("ITS_Select_ExeSetting")
        @Expose
        private List<ITSSelectExeSetting> iTSSelectExeSetting = null;
        @SerializedName("ITS_Get_BookingRights_ByUser")
        @Expose
        private List<ITSGetBookingRightsByUser> iTSGetBookingRightsByUser = null;
        @SerializedName("ITS_GetAllAgent_By_CompanyID")
        @Expose
        private List<ITSGetAllAgentByCompanyID> iTSGetAllAgentByCompanyID = null;
        @SerializedName("ITS_GetCompanyMarquee")
        @Expose
        private List<ITSGetCompanyMarquee> iTSGetCompanyMarquee = null;
        @SerializedName("ITS_GetITSMarquee")
        @Expose
        private List<ITSGetITSMarquee> iTSGetITSMarquee = null;
        @SerializedName("ITS_BranchMasterSelect_XML")
        @Expose
        private List<ITSBranchMasterSelectXML> iTSBranchMasterSelectXML = null;
        @SerializedName("ITS_GetGuestType_By_CompanyID")
        @Expose
        private List<ITSGetGuestTypeByCompanyID> iTSGetGuestTypeByCompanyID = null;
        @SerializedName("ITS_BusChartArrangement")
        @Expose
        private List<ITSBusChartArrangement> iTSBusChartArrangement = null;
        @SerializedName("ITS_GetBookingType_By_CompanyID_XML")
        @Expose
        private List<ITSGetBookingTypeByCompanyIDXML> iTSGetBookingTypeByCompanyIDXML = null;
        @SerializedName("ITS_GetOtherCompanyBookingRights_Exe")
        @Expose
        private List<ITSGetOtherCompanyBookingRightsExe> iTSGetOtherCompanyBookingRightsExe = null;
        @SerializedName("ITS_GetBranchUser_By_CompanyID")
        @Expose
        private List<ITSGetBranchUserByCompanyID> iTSGetBranchUserByCompanyID = null;
        @SerializedName("ITS_GetAllStopBooking")
        @Expose
        private List<Object> iTSGetAllStopBooking = null;
        @SerializedName("ITS_GetDisplayRouteBeforeMin")
        @Expose
        private List<ITSGetDisplayRouteBeforeMin> iTSGetDisplayRouteBeforeMin = null;
        @SerializedName("XMLKey")
        @Expose
        private List<XMLKey> xMLKey = null;
        @SerializedName("ITS_GetExtraRouteTime")
        @Expose
        private List<ITSGetExtraRouteTime> iTSGetExtraRouteTime = null;
        @SerializedName("ITS_StaticData_Select")
        @Expose
        private List<Object> iTSStaticDataSelect = null;
        @SerializedName("ITS_GetRemarksType")
        @Expose
        private List<Object> iTSGetRemarksType = null;
        @SerializedName("ITS_GetFareCaption_XML")
        @Expose
        private List<ITSGetFareCaptionXML> iTSGetFareCaptionXML = null;
        @SerializedName("ITS_FetchChartForeColor")
        @Expose
        private List<ITSFetchChartForeColor> iTSFetchChartForeColor = null;
        @SerializedName("ITS_AutoDeletePhoneBookingAfterBookingTime")
        @Expose
        private List<ITSAutoDeletePhoneBookingAfterBookingTime> iTSAutoDeletePhoneBookingAfterBookingTime = null;
        @SerializedName("ITS_BranchUserwisehotroute")
        @Expose
        private List<ITSBranchUserwisehotroute> iTSBranchUserwisehotroute = null;
        @SerializedName("ITS_DoNotAllowFareChangeInBelowRouteTime")
        @Expose
        private List<ITSDoNotAllowFareChangeInBelowRouteTime> iTSDoNotAllowFareChangeInBelowRouteTime = null;
        @SerializedName("ITS_RouteWiseChargedAmenity")
        @Expose
        private List<ITSRouteWiseChargedAmenity> iTSRouteWiseChargedAmenity = null;
        @SerializedName("ITS_OnlineWallet")
        @Expose
        private List<ITSOnlineWallet> iTSOnlineWallet = null;
        @SerializedName("ITS_RemarkRePrint")
        @Expose
        private List<ITSRemarkRePrint> iTSRemarkRePrint = null;
        @SerializedName("ITS_RemarkModify")
        @Expose
        private List<ITSRemarkModify> iTSRemarkModify = null;
        @SerializedName("ITS_State")
        @Expose
        private List<ITSState> iTSState = null;
        @SerializedName("ITS_Cancelremarks")
        @Expose
        private List<ITSCancelremark> iTSCancelremarks = null;
        @SerializedName("ITS_RouteTimePhoneStopBooking")
        @Expose
        private List<Object> iTSRouteTimePhoneStopBooking = null;
        @SerializedName("ITS_RouteTimeExtraFareGet")
        @Expose
        private List<ITSRouteTimeExtraFareGet> iTSRouteTimeExtraFareGet = null;
        @SerializedName("ITS_RouteWiseExtraFareForAgent")
        @Expose
        private List<ITSRouteWiseExtraFareForAgent> iTSRouteWiseExtraFareForAgent = null;
        @SerializedName("ITS_RouteTimeInsuranceCharge")
        @Expose
        private List<ITSRouteTimeInsuranceCharge> iTSRouteTimeInsuranceCharge = null;
        @SerializedName("ITS_BusTypeColorCode")
        @Expose
        private List<ITSBusTypeColorCode> iTSBusTypeColorCode = null;
        @SerializedName("ITS_RouteTimeWiseAgent")
        @Expose
        private List<ITSRouteTimeWiseAgent> iTSRouteTimeWiseAgent = null;
        @SerializedName("ITS_GetIdProof")
        @Expose
        private List<ITSGetIdProof> iTSGetIdProof = null;

        public List<ServerDate> getServerDate() {
            return serverDate;
        }

        public void setServerDate(List<ServerDate> serverDate) {
            this.serverDate = serverDate;
        }

        public List<ITSFetchBusNumber> getITSFetchBusNumber() {
            return iTSFetchBusNumber;
        }

        public void setITSFetchBusNumber(List<ITSFetchBusNumber> iTSFetchBusNumber) {
            this.iTSFetchBusNumber = iTSFetchBusNumber;
        }

        public List<ITSSelectExeSetting> getITSSelectExeSetting() {
            return iTSSelectExeSetting;
        }

        public void setITSSelectExeSetting(List<ITSSelectExeSetting> iTSSelectExeSetting) {
            this.iTSSelectExeSetting = iTSSelectExeSetting;
        }

        public List<ITSGetBookingRightsByUser> getITSGetBookingRightsByUser() {
            return iTSGetBookingRightsByUser;
        }

        public void setITSGetBookingRightsByUser(List<ITSGetBookingRightsByUser> iTSGetBookingRightsByUser) {
            this.iTSGetBookingRightsByUser = iTSGetBookingRightsByUser;
        }

        public List<ITSGetAllAgentByCompanyID> getITSGetAllAgentByCompanyID() {
            return iTSGetAllAgentByCompanyID;
        }

        public void setITSGetAllAgentByCompanyID(List<ITSGetAllAgentByCompanyID> iTSGetAllAgentByCompanyID) {
            this.iTSGetAllAgentByCompanyID = iTSGetAllAgentByCompanyID;
        }

        public List<ITSGetCompanyMarquee> getITSGetCompanyMarquee() {
            return iTSGetCompanyMarquee;
        }

        public void setITSGetCompanyMarquee(List<ITSGetCompanyMarquee> iTSGetCompanyMarquee) {
            this.iTSGetCompanyMarquee = iTSGetCompanyMarquee;
        }

        public List<ITSGetITSMarquee> getITSGetITSMarquee() {
            return iTSGetITSMarquee;
        }

        public void setITSGetITSMarquee(List<ITSGetITSMarquee> iTSGetITSMarquee) {
            this.iTSGetITSMarquee = iTSGetITSMarquee;
        }

        public List<ITSBranchMasterSelectXML> getITSBranchMasterSelectXML() {
            return iTSBranchMasterSelectXML;
        }

        public void setITSBranchMasterSelectXML(List<ITSBranchMasterSelectXML> iTSBranchMasterSelectXML) {
            this.iTSBranchMasterSelectXML = iTSBranchMasterSelectXML;
        }

        public List<ITSGetGuestTypeByCompanyID> getITSGetGuestTypeByCompanyID() {
            return iTSGetGuestTypeByCompanyID;
        }

        public void setITSGetGuestTypeByCompanyID(List<ITSGetGuestTypeByCompanyID> iTSGetGuestTypeByCompanyID) {
            this.iTSGetGuestTypeByCompanyID = iTSGetGuestTypeByCompanyID;
        }

        public List<ITSBusChartArrangement> getITSBusChartArrangement() {
            return iTSBusChartArrangement;
        }

        public void setITSBusChartArrangement(List<ITSBusChartArrangement> iTSBusChartArrangement) {
            this.iTSBusChartArrangement = iTSBusChartArrangement;
        }

        public List<ITSGetBookingTypeByCompanyIDXML> getITSGetBookingTypeByCompanyIDXML() {
            return iTSGetBookingTypeByCompanyIDXML;
        }

        public void setITSGetBookingTypeByCompanyIDXML(List<ITSGetBookingTypeByCompanyIDXML> iTSGetBookingTypeByCompanyIDXML) {
            this.iTSGetBookingTypeByCompanyIDXML = iTSGetBookingTypeByCompanyIDXML;
        }

        public List<ITSGetOtherCompanyBookingRightsExe> getITSGetOtherCompanyBookingRightsExe() {
            return iTSGetOtherCompanyBookingRightsExe;
        }

        public void setITSGetOtherCompanyBookingRightsExe(List<ITSGetOtherCompanyBookingRightsExe> iTSGetOtherCompanyBookingRightsExe) {
            this.iTSGetOtherCompanyBookingRightsExe = iTSGetOtherCompanyBookingRightsExe;
        }

        public List<ITSGetBranchUserByCompanyID> getITSGetBranchUserByCompanyID() {
            return iTSGetBranchUserByCompanyID;
        }

        public void setITSGetBranchUserByCompanyID(List<ITSGetBranchUserByCompanyID> iTSGetBranchUserByCompanyID) {
            this.iTSGetBranchUserByCompanyID = iTSGetBranchUserByCompanyID;
        }

        public List<Object> getITSGetAllStopBooking() {
            return iTSGetAllStopBooking;
        }

        public void setITSGetAllStopBooking(List<Object> iTSGetAllStopBooking) {
            this.iTSGetAllStopBooking = iTSGetAllStopBooking;
        }

        public List<ITSGetDisplayRouteBeforeMin> getITSGetDisplayRouteBeforeMin() {
            return iTSGetDisplayRouteBeforeMin;
        }

        public void setITSGetDisplayRouteBeforeMin(List<ITSGetDisplayRouteBeforeMin> iTSGetDisplayRouteBeforeMin) {
            this.iTSGetDisplayRouteBeforeMin = iTSGetDisplayRouteBeforeMin;
        }

        public List<XMLKey> getXMLKey() {
            return xMLKey;
        }

        public void setXMLKey(List<XMLKey> xMLKey) {
            this.xMLKey = xMLKey;
        }

        public List<ITSGetExtraRouteTime> getITSGetExtraRouteTime() {
            return iTSGetExtraRouteTime;
        }

        public void setITSGetExtraRouteTime(List<ITSGetExtraRouteTime> iTSGetExtraRouteTime) {
            this.iTSGetExtraRouteTime = iTSGetExtraRouteTime;
        }

        public List<Object> getITSStaticDataSelect() {
            return iTSStaticDataSelect;
        }

        public void setITSStaticDataSelect(List<Object> iTSStaticDataSelect) {
            this.iTSStaticDataSelect = iTSStaticDataSelect;
        }

        public List<Object> getITSGetRemarksType() {
            return iTSGetRemarksType;
        }

        public void setITSGetRemarksType(List<Object> iTSGetRemarksType) {
            this.iTSGetRemarksType = iTSGetRemarksType;
        }

        public List<ITSGetFareCaptionXML> getITSGetFareCaptionXML() {
            return iTSGetFareCaptionXML;
        }

        public void setITSGetFareCaptionXML(List<ITSGetFareCaptionXML> iTSGetFareCaptionXML) {
            this.iTSGetFareCaptionXML = iTSGetFareCaptionXML;
        }

        public List<ITSFetchChartForeColor> getITSFetchChartForeColor() {
            return iTSFetchChartForeColor;
        }

        public void setITSFetchChartForeColor(List<ITSFetchChartForeColor> iTSFetchChartForeColor) {
            this.iTSFetchChartForeColor = iTSFetchChartForeColor;
        }

        public List<ITSAutoDeletePhoneBookingAfterBookingTime> getITSAutoDeletePhoneBookingAfterBookingTime() {
            return iTSAutoDeletePhoneBookingAfterBookingTime;
        }

        public void setITSAutoDeletePhoneBookingAfterBookingTime(List<ITSAutoDeletePhoneBookingAfterBookingTime> iTSAutoDeletePhoneBookingAfterBookingTime) {
            this.iTSAutoDeletePhoneBookingAfterBookingTime = iTSAutoDeletePhoneBookingAfterBookingTime;
        }

        public List<ITSBranchUserwisehotroute> getITSBranchUserwisehotroute() {
            return iTSBranchUserwisehotroute;
        }

        public void setITSBranchUserwisehotroute(List<ITSBranchUserwisehotroute> iTSBranchUserwisehotroute) {
            this.iTSBranchUserwisehotroute = iTSBranchUserwisehotroute;
        }

        public List<ITSDoNotAllowFareChangeInBelowRouteTime> getITSDoNotAllowFareChangeInBelowRouteTime() {
            return iTSDoNotAllowFareChangeInBelowRouteTime;
        }

        public void setITSDoNotAllowFareChangeInBelowRouteTime(List<ITSDoNotAllowFareChangeInBelowRouteTime> iTSDoNotAllowFareChangeInBelowRouteTime) {
            this.iTSDoNotAllowFareChangeInBelowRouteTime = iTSDoNotAllowFareChangeInBelowRouteTime;
        }

        public List<ITSRouteWiseChargedAmenity> getITSRouteWiseChargedAmenity() {
            return iTSRouteWiseChargedAmenity;
        }

        public void setITSRouteWiseChargedAmenity(List<ITSRouteWiseChargedAmenity> iTSRouteWiseChargedAmenity) {
            this.iTSRouteWiseChargedAmenity = iTSRouteWiseChargedAmenity;
        }

        public List<ITSOnlineWallet> getITSOnlineWallet() {
            return iTSOnlineWallet;
        }

        public void setITSOnlineWallet(List<ITSOnlineWallet> iTSOnlineWallet) {
            this.iTSOnlineWallet = iTSOnlineWallet;
        }

        public List<ITSRemarkRePrint> getITSRemarkRePrint() {
            return iTSRemarkRePrint;
        }

        public void setITSRemarkRePrint(List<ITSRemarkRePrint> iTSRemarkRePrint) {
            this.iTSRemarkRePrint = iTSRemarkRePrint;
        }

        public List<ITSRemarkModify> getITSRemarkModify() {
            return iTSRemarkModify;
        }

        public void setITSRemarkModify(List<ITSRemarkModify> iTSRemarkModify) {
            this.iTSRemarkModify = iTSRemarkModify;
        }

        public List<ITSState> getITSState() {
            return iTSState;
        }

        public void setITSState(List<ITSState> iTSState) {
            this.iTSState = iTSState;
        }

        public List<ITSCancelremark> getITSCancelremarks() {
            return iTSCancelremarks;
        }

        public void setITSCancelremarks(List<ITSCancelremark> iTSCancelremarks) {
            this.iTSCancelremarks = iTSCancelremarks;
        }

        public List<Object> getITSRouteTimePhoneStopBooking() {
            return iTSRouteTimePhoneStopBooking;
        }

        public void setITSRouteTimePhoneStopBooking(List<Object> iTSRouteTimePhoneStopBooking) {
            this.iTSRouteTimePhoneStopBooking = iTSRouteTimePhoneStopBooking;
        }

        public List<ITSRouteTimeExtraFareGet> getITSRouteTimeExtraFareGet() {
            return iTSRouteTimeExtraFareGet;
        }

        public void setITSRouteTimeExtraFareGet(List<ITSRouteTimeExtraFareGet> iTSRouteTimeExtraFareGet) {
            this.iTSRouteTimeExtraFareGet = iTSRouteTimeExtraFareGet;
        }

        public List<ITSRouteWiseExtraFareForAgent> getITSRouteWiseExtraFareForAgent() {
            return iTSRouteWiseExtraFareForAgent;
        }

        public void setITSRouteWiseExtraFareForAgent(List<ITSRouteWiseExtraFareForAgent> iTSRouteWiseExtraFareForAgent) {
            this.iTSRouteWiseExtraFareForAgent = iTSRouteWiseExtraFareForAgent;
        }

        public List<ITSRouteTimeInsuranceCharge> getITSRouteTimeInsuranceCharge() {
            return iTSRouteTimeInsuranceCharge;
        }

        public void setITSRouteTimeInsuranceCharge(List<ITSRouteTimeInsuranceCharge> iTSRouteTimeInsuranceCharge) {
            this.iTSRouteTimeInsuranceCharge = iTSRouteTimeInsuranceCharge;
        }

        public List<ITSBusTypeColorCode> getITSBusTypeColorCode() {
            return iTSBusTypeColorCode;
        }

        public void setITSBusTypeColorCode(List<ITSBusTypeColorCode> iTSBusTypeColorCode) {
            this.iTSBusTypeColorCode = iTSBusTypeColorCode;
        }

        public List<ITSRouteTimeWiseAgent> getITSRouteTimeWiseAgent() {
            return iTSRouteTimeWiseAgent;
        }

        public void setITSRouteTimeWiseAgent(List<ITSRouteTimeWiseAgent> iTSRouteTimeWiseAgent) {
            this.iTSRouteTimeWiseAgent = iTSRouteTimeWiseAgent;
        }

        public List<ITSGetIdProof> getITSGetIdProof() {
            return iTSGetIdProof;
        }

        public void setITSGetIdProof(List<ITSGetIdProof> iTSGetIdProof) {
            this.iTSGetIdProof = iTSGetIdProof;
        }

    }


    public class ServerDate {

        @SerializedName("ServerDate")
        @Expose
        private String serverDate;

        public String getServerDate() {
            return serverDate;
        }

        public void setServerDate(String serverDate) {
            this.serverDate = serverDate;
        }

    }

    public class ITSFetchBusNumber {

        @SerializedName("CM_CompanyID")
        @Expose
        private String cMCompanyID;
        @SerializedName("BM_BusID")
        @Expose
        private String bMBusID;
        @SerializedName("BM_BusNo")
        @Expose
        private String bMBusNo;
        @SerializedName("BTM_BusTypeID")
        @Expose
        private String bTMBusTypeID;
        @SerializedName("BM_seating")
        @Expose
        private String bMSeating;
        @SerializedName("BM_Remarks")
        @Expose
        private String bMRemarks;
        @SerializedName("BM_IsActive")
        @Expose
        private String bMIsActive;
        @SerializedName("BM_No_Of_Tyres")
        @Expose
        private String bMNoOfTyres;

        public String getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(String cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getBMBusID() {
            return bMBusID;
        }

        public void setBMBusID(String bMBusID) {
            this.bMBusID = bMBusID;
        }

        public String getBMBusNo() {
            return bMBusNo;
        }

        public void setBMBusNo(String bMBusNo) {
            this.bMBusNo = bMBusNo;
        }

        public String getBTMBusTypeID() {
            return bTMBusTypeID;
        }

        public void setBTMBusTypeID(String bTMBusTypeID) {
            this.bTMBusTypeID = bTMBusTypeID;
        }

        public String getBMSeating() {
            return bMSeating;
        }

        public void setBMSeating(String bMSeating) {
            this.bMSeating = bMSeating;
        }

        public String getBMRemarks() {
            return bMRemarks;
        }

        public void setBMRemarks(String bMRemarks) {
            this.bMRemarks = bMRemarks;
        }

        public String getBMIsActive() {
            return bMIsActive;
        }

        public void setBMIsActive(String bMIsActive) {
            this.bMIsActive = bMIsActive;
        }

        public String getBMNoOfTyres() {
            return bMNoOfTyres;
        }

        public void setBMNoOfTyres(String bMNoOfTyres) {
            this.bMNoOfTyres = bMNoOfTyres;
        }

    }

    public class ITSSelectExeSetting {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("UM_UserID")
        @Expose
        private Integer uMUserID;
        @SerializedName("ES_ThemeID")
        @Expose
        private Integer eSThemeID;
        @SerializedName("ES_DefaultBookingTypeID")
        @Expose
        private Integer eSDefaultBookingTypeID;
        @SerializedName("ES_BookingDateSetting")
        @Expose
        private Integer eSBookingDateSetting;
        @SerializedName("ES_TicketReport")
        @Expose
        private Integer eSTicketReport;
        @SerializedName("ES_OnlineAgentNeed")
        @Expose
        private Integer eSOnlineAgentNeed;
        @SerializedName("ES_OpenRoutePopUp")
        @Expose
        private Integer eSOpenRoutePopUp;
        @SerializedName("ES_PhoneValidationNeed")
        @Expose
        private Integer eSPhoneValidationNeed;
        @SerializedName("ES_DefaultToolTip")
        @Expose
        private Integer eSDefaultToolTip;
        @SerializedName("ES_IsShowCalender")
        @Expose
        private Integer eSIsShowCalender;
        @SerializedName("PhoneStartCharacter")
        @Expose
        private String phoneStartCharacter;
        @SerializedName("ES_IsShowBusTypeFilter")
        @Expose
        private Integer eSIsShowBusTypeFilter;
        @SerializedName("ES_IsHTTPS")
        @Expose
        private Integer eSIsHTTPS;
        @SerializedName("ES_ClearSeatsOnNewRoute")
        @Expose
        private Integer eSClearSeatsOnNewRoute;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public Integer getUMUserID() {
            return uMUserID;
        }

        public void setUMUserID(Integer uMUserID) {
            this.uMUserID = uMUserID;
        }

        public Integer getESThemeID() {
            return eSThemeID;
        }

        public void setESThemeID(Integer eSThemeID) {
            this.eSThemeID = eSThemeID;
        }

        public Integer getESDefaultBookingTypeID() {
            return eSDefaultBookingTypeID;
        }

        public void setESDefaultBookingTypeID(Integer eSDefaultBookingTypeID) {
            this.eSDefaultBookingTypeID = eSDefaultBookingTypeID;
        }

        public Integer getESBookingDateSetting() {
            return eSBookingDateSetting;
        }

        public void setESBookingDateSetting(Integer eSBookingDateSetting) {
            this.eSBookingDateSetting = eSBookingDateSetting;
        }

        public Integer getESTicketReport() {
            return eSTicketReport;
        }

        public void setESTicketReport(Integer eSTicketReport) {
            this.eSTicketReport = eSTicketReport;
        }

        public Integer getESOnlineAgentNeed() {
            return eSOnlineAgentNeed;
        }

        public void setESOnlineAgentNeed(Integer eSOnlineAgentNeed) {
            this.eSOnlineAgentNeed = eSOnlineAgentNeed;
        }

        public Integer getESOpenRoutePopUp() {
            return eSOpenRoutePopUp;
        }

        public void setESOpenRoutePopUp(Integer eSOpenRoutePopUp) {
            this.eSOpenRoutePopUp = eSOpenRoutePopUp;
        }

        public Integer getESPhoneValidationNeed() {
            return eSPhoneValidationNeed;
        }

        public void setESPhoneValidationNeed(Integer eSPhoneValidationNeed) {
            this.eSPhoneValidationNeed = eSPhoneValidationNeed;
        }

        public Integer getESDefaultToolTip() {
            return eSDefaultToolTip;
        }

        public void setESDefaultToolTip(Integer eSDefaultToolTip) {
            this.eSDefaultToolTip = eSDefaultToolTip;
        }

        public Integer getESIsShowCalender() {
            return eSIsShowCalender;
        }

        public void setESIsShowCalender(Integer eSIsShowCalender) {
            this.eSIsShowCalender = eSIsShowCalender;
        }

        public String getPhoneStartCharacter() {
            return phoneStartCharacter;
        }

        public void setPhoneStartCharacter(String phoneStartCharacter) {
            this.phoneStartCharacter = phoneStartCharacter;
        }

        public Integer getESIsShowBusTypeFilter() {
            return eSIsShowBusTypeFilter;
        }

        public void setESIsShowBusTypeFilter(Integer eSIsShowBusTypeFilter) {
            this.eSIsShowBusTypeFilter = eSIsShowBusTypeFilter;
        }

        public Integer getESIsHTTPS() {
            return eSIsHTTPS;
        }

        public void setESIsHTTPS(Integer eSIsHTTPS) {
            this.eSIsHTTPS = eSIsHTTPS;
        }

        public Integer getESClearSeatsOnNewRoute() {
            return eSClearSeatsOnNewRoute;
        }

        public void setESClearSeatsOnNewRoute(Integer eSClearSeatsOnNewRoute) {
            this.eSClearSeatsOnNewRoute = eSClearSeatsOnNewRoute;
        }

    }

    @Entity(tableName = "ITSGetBookingRightsByUser")
    public static class ITSGetBookingRightsByUser {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("BR_userID")
        @Expose
        private int bRUserID;
        @SerializedName("BM_BranchID")
        @Expose
        private int bMBranchID;
        @SerializedName("BR_ConfirmCreate")
        @Expose
        private int bRConfirmCreate;
        @SerializedName("BR_ConfirmModify")
        @Expose
        private int bRConfirmModify;
        @SerializedName("BR_ConfirmCancel")
        @Expose
        private int bRConfirmCancel;
        @SerializedName("BR_PhoneCreate")
        @Expose
        private int bRPhoneCreate;
        @SerializedName("BR_PhoneModify")
        @Expose
        private int bRPhoneModify;
        @SerializedName("BR_PhoneCancel")
        @Expose
        private int bRPhoneCancel;
        @SerializedName("BR_AgentCreate")
        @Expose
        private int bRAgentCreate;
        @SerializedName("BR_AgentModify")
        @Expose
        private int bRAgentModify;
        @SerializedName("BR_AgentCancel")
        @Expose
        private int bRAgentCancel;
        @SerializedName("BR_BranchCreate")
        @Expose
        private int bRBranchCreate;
        @SerializedName("BR_BranchModify")
        @Expose
        private int bRBranchModify;
        @SerializedName("BR_BranchCancel")
        @Expose
        private int bRBranchCancel;
        @SerializedName("BR_GuestCreate")
        @Expose
        private int bRGuestCreate;
        @SerializedName("BR_GuestModify")
        @Expose
        private int bRGuestModify;
        @SerializedName("BR_GuestCancel")
        @Expose
        private int bRGuestCancel;
        @SerializedName("BR_BankCardCreate")
        @Expose
        private int bRBankCardCreate;
        @SerializedName("BR_BankCardModify")
        @Expose
        private int bRBankCardModify;
        @SerializedName("BR_BankCardCancel")
        @Expose
        private int bRBankCardCancel;
        @SerializedName("BR_CompanyCardCreate")
        @Expose
        private int bRCompanyCardCreate;
        @SerializedName("BR_CompanyCardModify")
        @Expose
        private int bRCompanyCardModify;
        @SerializedName("BR_CompanyCardCancel")
        @Expose
        private int bRCompanyCardCancel;
        @SerializedName("BR_WaitingCreate")
        @Expose
        private int bRWaitingCreate;
        @SerializedName("BR_WaitingModify")
        @Expose
        private int bRWaitingModify;
        @SerializedName("BR_WaitingCancel")
        @Expose
        private int bRWaitingCancel;
        @SerializedName("BR_AgentQuataCreate")
        @Expose
        private int bRAgentQuataCreate;
        @SerializedName("BR_AgentQuataModify")
        @Expose
        private int bRAgentQuataModify;
        @SerializedName("BR_AgentQuataCancel")
        @Expose
        private int bRAgentQuataCancel;
        @SerializedName("BR_OpenCreate")
        @Expose
        private int bROpenCreate;
        @SerializedName("BR_OpenModify")
        @Expose
        private int bROpenModify;
        @SerializedName("BR_OpenCancel")
        @Expose
        private int bROpenCancel;
        @SerializedName("BR_APICreate")
        @Expose
        private int bRAPICreate;
        @SerializedName("BR_APIModify")
        @Expose
        private int bRAPIModify;
        @SerializedName("BR_APICancel")
        @Expose
        private int bRAPICancel;
        @SerializedName("BR_B2CCreate")
        @Expose
        private int bRB2CCreate;
        @SerializedName("BR_B2CModify")
        @Expose
        private int bRB2CModify;
        @SerializedName("BR_B2CCancel")
        @Expose
        private int bRB2CCancel;
        @SerializedName("BR_OnlineAgentCreate")
        @Expose
        private int bROnlineAgentCreate;
        @SerializedName("BR_OnlineAgentModify")
        @Expose
        private int bROnlineAgentModify;
        @SerializedName("BR_OnlineAgentCancel")
        @Expose
        private int bROnlineAgentCancel;
        @SerializedName("BR_OnlineAgentPhoneCreate")
        @Expose
        private int bROnlineAgentPhoneCreate;
        @SerializedName("BR_OnlineAgentPhoneModify")
        @Expose
        private int bROnlineAgentPhoneModify;
        @SerializedName("BR_OnlineAgentPhoneCancel")
        @Expose
        private int bROnlineAgentPhoneCancel;
        @SerializedName("BR_APIPhoneCreate")
        @Expose
        private int bRAPIPhoneCreate;
        @SerializedName("BR_APIPhoneModify")
        @Expose
        private int bRAPIPhoneModify;
        @SerializedName("BR_APIPhoneCancel")
        @Expose
        private int bRAPIPhoneCancel;
        @SerializedName("BR_OnlineAgentCardCreate")
        @Expose
        private int bROnlineAgentCardCreate;
        @SerializedName("BR_OnlineAgentCardModify")
        @Expose
        private int bROnlineAgentCardModify;
        @SerializedName("BR_OnlineAgentCardCancel")
        @Expose
        private int bROnlineAgentCardCancel;
        @SerializedName("BR_OnlineWalletCreate")
        @Expose
        private int bROnlineWalletCreate;
        @SerializedName("BR_OnlineWalletModify")
        @Expose
        private int bROnlineWalletModify;
        @SerializedName("BR_OnlineWalletCancel")
        @Expose
        private int bROnlineWalletCancel;
        @SerializedName("BR_AskChargeOnModify")
        @Expose
        private int bRAskChargeOnModify;
        @SerializedName("BR_RemotePaymentCreate")
        @Expose
        private int bRRemotePaymentCreate;
        @SerializedName("BR_RemotePaymentModify")
        @Expose
        private int bRRemotePaymentModify;
        @SerializedName("BR_RemotePaymentCancel")
        @Expose
        private int bRRemotePaymentCancel;
        @SerializedName("BOA_ConfirmModify")
        @Expose
        private int bOAConfirmModify;
        @SerializedName("BOA_ConfirmCancel")
        @Expose
        private int bOAConfirmCancel;
        @SerializedName("BOA_PhoneModify")
        @Expose
        private int bOAPhoneModify;
        @SerializedName("BOA_PhoneCancel")
        @Expose
        private int bOAPhoneCancel;
        @SerializedName("BOA_AgentModify")
        @Expose
        private int bOAAgentModify;
        @SerializedName("BOA_AgentCancel")
        @Expose
        private int bOAAgentCancel;
        @SerializedName("BOA_BranchModify")
        @Expose
        private int bOABranchModify;
        @SerializedName("BOA_BranchCancel")
        @Expose
        private int bOABranchCancel;
        @SerializedName("BOA_GuestModify")
        @Expose
        private int bOAGuestModify;
        @SerializedName("BOA_GuestCancel")
        @Expose
        private int bOAGuestCancel;
        @SerializedName("BOA_BankCardModify")
        @Expose
        private int bOABankCardModify;
        @SerializedName("BOA_BankCardCancel")
        @Expose
        private int bOABankCardCancel;
        @SerializedName("BOA_CompanyCardModify")
        @Expose
        private int bOACompanyCardModify;
        @SerializedName("BOA_CompanyCardCancel")
        @Expose
        private int bOACompanyCardCancel;
        @SerializedName("BOA_WaitingModify")
        @Expose
        private int bOAWaitingModify;
        @SerializedName("BOA_WaitingCancel")
        @Expose
        private int bOAWaitingCancel;
        @SerializedName("BOA_AgentQuataModify")
        @Expose
        private int bOAAgentQuataModify;
        @SerializedName("BOA_AgentQuataCancel")
        @Expose
        private int bOAAgentQuataCancel;
        @SerializedName("BOA_OpenModify")
        @Expose
        private int bOAOpenModify;
        @SerializedName("BOA_OpenCancel")
        @Expose
        private int bOAOpenCancel;
        @SerializedName("BOA_OnlineWalletModify")
        @Expose
        private int bOAOnlineWalletModify;
        @SerializedName("BOA_OnlineWalletCancel")
        @Expose
        private int bOAOnlineWalletCancel;
        @SerializedName("BOA_RemotePaymentModify")
        @Expose
        private int bOARemotePaymentModify;
        @SerializedName("BOA_RemotePaymentCancel")
        @Expose
        private int bOARemotePaymentCancel;
        @SerializedName("GA_AllowReprint")
        @Expose
        private int gAAllowReprint;
        @SerializedName("GA_AllowBookingAFMin")
        @Expose
        private int gAAllowBookingAFMin;
        @SerializedName("GA_CancellationBFMin")
        @Expose
        private int gACancellationBFMin;
        @SerializedName("GA_AllowFareChange")
        @Expose
        private int gAAllowFareChange;
        @SerializedName("GA_AllowBackDateBooking")
        @Expose
        private int gAAllowBackDateBooking;
        @SerializedName("GA_AllowBackDateCancellation")
        @Expose
        private int gAAllowBackDateCancellation;
        @SerializedName("GA_AllowDiscount")
        @Expose
        private int gAAllowDiscount;
        @SerializedName("GA_AllowInternetAuthorization")
        @Expose
        private int gAAllowInternetAuthorization;
        @SerializedName("GA_AllowModify")
        @Expose
        private int gAAllowModify;
        @SerializedName("GA_AllowRemoveQuota")
        @Expose
        private int gAAllowRemoveQuota;
        @SerializedName("GA_StopBooking")
        @Expose
        private int gAStopBooking;
        @SerializedName("GA_ShowAllRoute")
        @Expose
        private int gAShowAllRoute;
        @SerializedName("GA_ShowCollection")
        @Expose
        private int gAShowCollection;
        @SerializedName("GA_ArrangementTransfer")
        @Expose
        private int gAArrangementTransfer;
        @SerializedName("GA_ShowBackDate")
        @Expose
        private int gAShowBackDate;
        @SerializedName("GA_RouteTransfer")
        @Expose
        private int gARouteTransfer;
        @SerializedName("GA_ChartSMS")
        @Expose
        private int gAChartSMS;
        @SerializedName("GA_BranchWiseCollection")
        @Expose
        private int gABranchWiseCollection;
        @SerializedName("GA_XmlSourceMemory")
        @Expose
        private int gAXmlSourceMemory;
        @SerializedName("GA_AllowMultipleBooking")
        @Expose
        private int gAAllowMultipleBooking;
        @SerializedName("GA_AllowModifyNamePhone")
        @Expose
        private int gAAllowModifyNamePhone;
        @SerializedName("GA_AllowModifyToCity")
        @Expose
        private int gAAllowModifyToCity;
        @SerializedName("GA_ApplyRouteBFMin")
        @Expose
        private int gAApplyRouteBFMin;
        @SerializedName("GA_AllowConfirmToOpen")
        @Expose
        private int gAAllowConfirmToOpen;
        @SerializedName("GA_ShowOnlineAgent")
        @Expose
        private int gAShowOnlineAgent;
        @SerializedName("GA_ShowOnlyBranchCityAgent")
        @Expose
        private int gAShowOnlyBranchCityAgent;
        @SerializedName("GA_SiteAccess")
        @Expose
        private int gASiteAccess;
        @SerializedName("GA_SiteNames")
        @Expose
        private String gASiteNames;
        @SerializedName("GA_AllowPrepaidBranchBooking")
        @Expose
        private int gAAllowPrepaidBranchBooking;
        @SerializedName("GA_AllowChangeRefundCharges")
        @Expose
        private int gAAllowChangeRefundCharges;
        @SerializedName("GA_AllowLessRefundCharges")
        @Expose
        private int gAAllowLessRefundCharges;
        @SerializedName("GA_AllowPetrolPumpModule")
        @Expose
        private int gAAllowPetrolPumpModule;
        @SerializedName("GA_AllowKMEntry")
        @Expose
        private int gAAllowKMEntry;
        @SerializedName("GA_AllowReturnPhoneBooking")
        @Expose
        private int gAAllowReturnPhoneBooking;
        @SerializedName("GA_ShowPrepaidBalance")
        @Expose
        private int gAShowPrepaidBalance;
        @SerializedName("GA_AllowLiveBusStatusEntry")
        @Expose
        private int gAAllowLiveBusStatusEntry;
        @SerializedName("GA_AllowLiveBusStatusEntryOtherPickup")
        @Expose
        private int gAAllowLiveBusStatusEntryOtherPickup;
        @SerializedName("GA_IsChangeBusStatusPickupTime")
        @Expose
        private int gAIsChangeBusStatusPickupTime;
        @SerializedName("GA_IsSendBranchAddressSMS")
        @Expose
        private int gAIsSendBranchAddressSMS;
        @SerializedName("GA_AllowModifyFromCity")
        @Expose
        private int gAAllowModifyFromCity;
        @SerializedName("GA_IsPromptBackDateBooking")
        @Expose
        private int gAIsPromptBackDateBooking;
        @SerializedName("GA_AllowBusHubRecharge")
        @Expose
        private int gAAllowBusHubRecharge;
        @SerializedName("GA_RemoveSameCityHold")
        @Expose
        private int gARemoveSameCityHold;
        @SerializedName("GA_RemoveOtherCityHold")
        @Expose
        private int gARemoveOtherCityHold;
        @SerializedName("GA_AllowNonReported")
        @Expose
        private int gAAllowNonReported;
        @SerializedName("GA_ReleaseAPIB2CHold")
        @Expose
        private int gAReleaseAPIB2CHold;
        @SerializedName("GA_AllowFareChangeInPhoneModify")
        @Expose
        private int gAAllowFareChangeInPhoneModify;
        @SerializedName("GA_AllowLessFareChange")
        @Expose
        private int gAAllowLessFareChange;
        @SerializedName("RA_ViewInquiry0")
        @Expose
        private int rAViewInquiry0;
        @SerializedName("RA_ViewInquiry1")
        @Expose
        private int rAViewInquiry1;
        @SerializedName("RA_ViewInquiry2")
        @Expose
        private int rAViewInquiry2;
        @SerializedName("RA_ViewInquiry3")
        @Expose
        private int rAViewInquiry3;
        @SerializedName("RA_ViewInquiry4")
        @Expose
        private int rAViewInquiry4;
        @SerializedName("RA_ViewInquiry5")
        @Expose
        private int rAViewInquiry5;
        @SerializedName("RA_ViewInquiry6")
        @Expose
        private int rAViewInquiry6;
        @SerializedName("RA_ViewInquiry7")
        @Expose
        private int rAViewInquiry7;
        @SerializedName("RA_ViewInquiry8")
        @Expose
        private int rAViewInquiry8;
        @SerializedName("RA_ViewInquiry9")
        @Expose
        private int rAViewInquiry9;
        @SerializedName("RA_ViewInquiry10")
        @Expose
        private int rAViewInquiry10;
        @SerializedName("RA_ViewInquiry11")
        @Expose
        private int rAViewInquiry11;
        @SerializedName("RA_ViewInquiry12")
        @Expose
        private int rAViewInquiry12;
        @SerializedName("RA_CityDropWise")
        @Expose
        private int rACityDropWise;
        @SerializedName("RA_SubRouteTicketCount")
        @Expose
        private int rASubRouteTicketCount;
        @SerializedName("RA_PassengerBooking")
        @Expose
        private int rAPassengerBooking;
        @SerializedName("RA_RouteDeparture")
        @Expose
        private int rARouteDeparture;
        @SerializedName("RA_RouteTimeWiseMemo")
        @Expose
        private int rARouteTimeWiseMemo;
        @SerializedName("RA_PickupWiseInquiry")
        @Expose
        private int rAPickupWiseInquiry;
        @SerializedName("RA_MainRouteWisePassengerDetails")
        @Expose
        private int rAMainRouteWisePassengerDetails;
        @SerializedName("RA_RouteTimeBookingMemo")
        @Expose
        private int rARouteTimeBookingMemo;
        @SerializedName("RA_BusIncomeReceipt")
        @Expose
        private int rABusIncomeReceipt;
        @SerializedName("RA_AllowCityWiseSeatChartReport")
        @Expose
        private int rAAllowCityWiseSeatChartReport;
        @SerializedName("RA_AllowBulkTicketPrint")
        @Expose
        private int rAAllowBulkTicketPrint;
        @SerializedName("RA_PNRWiseAmenities")
        @Expose
        private int rAPNRWiseAmenities;
        @SerializedName("RA_RoutePickUpDropChart")
        @Expose
        private int rARoutePickUpDropChart;
        @SerializedName("RA_DailyRouteTimeWiseMemo")
        @Expose
        private int rADailyRouteTimeWiseMemo;
        @SerializedName("RA_FutureBookingGraph")
        @Expose
        private int rAFutureBookingGraph;
        @SerializedName("RA_CityPickupDropWisechart")
        @Expose
        private int rACityPickupDropWisechart;
        @SerializedName("RA_PickupDropReport")
        @Expose
        private int rAPickupDropReport;
        @SerializedName("RA_ReservationDetailReport")
        @Expose
        private int rAReservationDetailReport;
        @SerializedName("RA_PassengerList")
        @Expose
        private int rAPassengerList;
        @SerializedName("RA_DepartureReportSeatWise")
        @Expose
        private int rADepartureReportSeatWise;
        @SerializedName("RA_SeatWiseInquiryReport")
        @Expose
        private int rASeatWiseInquiryReport;
        @SerializedName("RA_BranchWiseInquiryReport")
        @Expose
        private int rABranchWiseInquiryReport;
        @SerializedName("RA_ReservationDetailReport_Amount")
        @Expose
        private int rAReservationDetailReportAmount;
        @SerializedName("OS_AdlabsURL")
        @Expose
        private String oSAdlabsURL;
        @SerializedName("OS_IsLabelDisplay")
        @Expose
        private int oSIsLabelDisplay;
        @SerializedName("OS_LableText")
        @Expose
        private String oSLableText;
        @SerializedName("IsParcelBookingRights")
        @Expose
        private int isParcelBookingRights;
        @SerializedName("GA_AllowToSetLessPhoneCancelTime")
        @Expose
        private int gAAllowToSetLessPhoneCancelTime;
        @SerializedName("GA_AllowBookWithNewFare")
        @Expose
        private int gAAllowBookWithNewFare;
        @SerializedName("BR_BranchQuotaCreate")
        @Expose
        private int bRBranchQuotaCreate;
        @SerializedName("BR_BranchQuotaModify")
        @Expose
        private int bRBranchQuotaModify;
        @SerializedName("BR_BranchQuotaCancel")
        @Expose
        private int bRBranchQuotaCancel;
        @SerializedName("BOA_BranchQuataModify")
        @Expose
        private int bOABranchQuataModify;
        @SerializedName("BOA_BranchQuataCancel")
        @Expose
        private int bOABranchQuataCancel;
        @SerializedName("GA_AllowMultyDateAgentQuota_Exe")
        @Expose
        private int gAAllowMultyDateAgentQuotaExe;
        @SerializedName("GA_AllowMultyDateBranchQuota_Exe")
        @Expose
        private int gAAllowMultyDateBranchQuotaExe;
        @SerializedName("GA_AllowCopyTime_Exe")
        @Expose
        private int gAAllowCopyTimeExe;
        @SerializedName("GA_AllowSetRouteFareFromExe")
        @Expose
        private int gAAllowSetRouteFareFromExe;
        @SerializedName("GA_AllowSetSeatPermanentHold")
        @Expose
        private int gAAllowSetSeatPermanentHold;
        @SerializedName("GA_ModifyOnOtherCompanyRoute")
        @Expose
        private int gAModifyOnOtherCompanyRoute;
        @SerializedName("GA_AllowUserToSetSeatWiseFare")
        @Expose
        private int gAAllowUserToSetSeatWiseFare;
        @SerializedName("GA_AllowToChangePhoneNoOnResendSMS")
        @Expose
        private int gAAllowToChangePhoneNoOnResendSMS;
        @SerializedName("GA_AllowToSendBusTrackSMS")
        @Expose
        private int gAAllowToSendBusTrackSMS;
        @SerializedName("GA_AddFareOnModify")
        @Expose
        private int gAAddFareOnModify;
        @SerializedName("GA_AllowToPrintCollectionReport")
        @Expose
        private int gAAllowToPrintCollectionReport;
        @SerializedName("GA_AskCancelBranchUser")
        @Expose
        private int gAAskCancelBranchUser;
        @SerializedName("GA_AllowMultipleLogin")
        @Expose
        private int gAAllowMultipleLogin;
        @SerializedName("GA_AllowBusScheduleOnEXE")
        @Expose
        private int gAAllowBusScheduleOnEXE;
        @SerializedName("GA_AllowPhoneOnHoldSeat")
        @Expose
        private int gAAllowPhoneOnHoldSeat;
        @SerializedName("GA_IsBranchPNRModifyCharges")
        @Expose
        private int gAIsBranchPNRModifyCharges;
        @SerializedName("FBR_ConfirmCreate")
        @Expose
        private int fBRConfirmCreate;
        @SerializedName("FBR_ConfirmModify")
        @Expose
        private int fBRConfirmModify;
        @SerializedName("FBR_ConfirmCancel")
        @Expose
        private int fBRConfirmCancel;
        @SerializedName("FBR_PhoneCreate")
        @Expose
        private int fBRPhoneCreate;
        @SerializedName("FBR_PhoneModify")
        @Expose
        private int fBRPhoneModify;
        @SerializedName("FBR_PhoneCancel")
        @Expose
        private int fBRPhoneCancel;
        @SerializedName("FBR_AgentCreate")
        @Expose
        private int fBRAgentCreate;
        @SerializedName("FBR_AgentModify")
        @Expose
        private int fBRAgentModify;
        @SerializedName("FBR_AgentCancel")
        @Expose
        private int fBRAgentCancel;
        @SerializedName("FBR_BranchCreate")
        @Expose
        private int fBRBranchCreate;
        @SerializedName("FBR_BranchModify")
        @Expose
        private int fBRBranchModify;
        @SerializedName("FBR_BranchCancel")
        @Expose
        private int fBRBranchCancel;
        @SerializedName("FBR_GuestCreate")
        @Expose
        private int fBRGuestCreate;
        @SerializedName("FBR_GuestModify")
        @Expose
        private int fBRGuestModify;
        @SerializedName("FBR_GuestCancel")
        @Expose
        private int fBRGuestCancel;
        @SerializedName("FBR_BankCardCreate")
        @Expose
        private int fBRBankCardCreate;
        @SerializedName("FBR_BankCardModify")
        @Expose
        private int fBRBankCardModify;
        @SerializedName("FBR_BankCardCancel")
        @Expose
        private int fBRBankCardCancel;
        @SerializedName("FBR_WaitingCreate")
        @Expose
        private int fBRWaitingCreate;
        @SerializedName("FBR_WaitingModify")
        @Expose
        private int fBRWaitingModify;
        @SerializedName("FBR_WaitingCancel")
        @Expose
        private int fBRWaitingCancel;
        @SerializedName("FBR_CompanyCardCreate")
        @Expose
        private int fBRCompanyCardCreate;
        @SerializedName("FBR_CompanyCardModify")
        @Expose
        private int fBRCompanyCardModify;
        @SerializedName("FBR_CompanyCardCancel")
        @Expose
        private int fBRCompanyCardCancel;
        @SerializedName("FBR_OnlineAgentCreate")
        @Expose
        private int fBROnlineAgentCreate;
        @SerializedName("FBR_OnlineAgentModify")
        @Expose
        private int fBROnlineAgentModify;
        @SerializedName("FBR_OnlineAgentCancel")
        @Expose
        private int fBROnlineAgentCancel;
        @SerializedName("FBR_APIAgentCreate")
        @Expose
        private int fBRAPIAgentCreate;
        @SerializedName("FBR_APIAgentModify")
        @Expose
        private int fBRAPIAgentModify;
        @SerializedName("FBR_APIAgentCancel")
        @Expose
        private int fBRAPIAgentCancel;
        @SerializedName("FBR_B2CAgentCreate")
        @Expose
        private int fBRB2CAgentCreate;
        @SerializedName("FBR_B2CAgentModify")
        @Expose
        private int fBRB2CAgentModify;
        @SerializedName("FBR_B2CAgentCancel")
        @Expose
        private int fBRB2CAgentCancel;
        @SerializedName("FBR_AgentQuotaCreate")
        @Expose
        private int fBRAgentQuotaCreate;
        @SerializedName("FBR_AgentQuotaModify")
        @Expose
        private int fBRAgentQuotaModify;
        @SerializedName("FBR_AgentQuotaCancel")
        @Expose
        private int fBRAgentQuotaCancel;
        @SerializedName("FBR_ITSPLWalletCreate")
        @Expose
        private int fBRITSPLWalletCreate;
        @SerializedName("FBR_ITSPLWalletModify")
        @Expose
        private int fBRITSPLWalletModify;
        @SerializedName("FBR_ITSPLWalletCancel")
        @Expose
        private int fBRITSPLWalletCancel;
        @SerializedName("FBR_RemotePaymentCreate")
        @Expose
        private int fBRRemotePaymentCreate;
        @SerializedName("FBR_RemotePaymentModify")
        @Expose
        private int fBRRemotePaymentModify;
        @SerializedName("FBR_RemotePaymentCancel")
        @Expose
        private int fBRRemotePaymentCancel;
        @SerializedName("IsSetDefaultPageSize")
        @Expose
        private int isSetDefaultPageSize;
        @SerializedName("GA_AllowPickupChangeInModify")
        @Expose
        private int gAAllowPickupChangeInModify;
        @SerializedName("IsBookingLog")
        @Expose
        private int isBookingLog;
        @SerializedName("GA_IsShowFeedBack")
        @Expose
        private int gAIsShowFeedBack;
        @SerializedName("GA_CalculateCommissionForPrepaidBranch")
        @Expose
        private int gACalculateCommissionForPrepaidBranch;

        public int getBRUserID() {
            return bRUserID;
        }

        public void setBRUserID(int bRUserID) {
            this.bRUserID = bRUserID;
        }

        public int getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(int bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public int getBRConfirmCreate() {
            return bRConfirmCreate;
        }

        public void setBRConfirmCreate(int bRConfirmCreate) {
            this.bRConfirmCreate = bRConfirmCreate;
        }

        public int getBRConfirmModify() {
            return bRConfirmModify;
        }

        public void setBRConfirmModify(int bRConfirmModify) {
            this.bRConfirmModify = bRConfirmModify;
        }

        public int getBRConfirmCancel() {
            return bRConfirmCancel;
        }

        public void setBRConfirmCancel(int bRConfirmCancel) {
            this.bRConfirmCancel = bRConfirmCancel;
        }

        public int getBRPhoneCreate() {
            return bRPhoneCreate;
        }

        public void setBRPhoneCreate(int bRPhoneCreate) {
            this.bRPhoneCreate = bRPhoneCreate;
        }

        public int getBRPhoneModify() {
            return bRPhoneModify;
        }

        public void setBRPhoneModify(int bRPhoneModify) {
            this.bRPhoneModify = bRPhoneModify;
        }

        public int getBRPhoneCancel() {
            return bRPhoneCancel;
        }

        public void setBRPhoneCancel(int bRPhoneCancel) {
            this.bRPhoneCancel = bRPhoneCancel;
        }

        public int getBRAgentCreate() {
            return bRAgentCreate;
        }

        public void setBRAgentCreate(int bRAgentCreate) {
            this.bRAgentCreate = bRAgentCreate;
        }

        public int getBRAgentModify() {
            return bRAgentModify;
        }

        public void setBRAgentModify(int bRAgentModify) {
            this.bRAgentModify = bRAgentModify;
        }

        public int getBRAgentCancel() {
            return bRAgentCancel;
        }

        public void setBRAgentCancel(int bRAgentCancel) {
            this.bRAgentCancel = bRAgentCancel;
        }

        public int getBRBranchCreate() {
            return bRBranchCreate;
        }

        public void setBRBranchCreate(int bRBranchCreate) {
            this.bRBranchCreate = bRBranchCreate;
        }

        public int getBRBranchModify() {
            return bRBranchModify;
        }

        public void setBRBranchModify(int bRBranchModify) {
            this.bRBranchModify = bRBranchModify;
        }

        public int getBRBranchCancel() {
            return bRBranchCancel;
        }

        public void setBRBranchCancel(int bRBranchCancel) {
            this.bRBranchCancel = bRBranchCancel;
        }

        public int getBRGuestCreate() {
            return bRGuestCreate;
        }

        public void setBRGuestCreate(int bRGuestCreate) {
            this.bRGuestCreate = bRGuestCreate;
        }

        public int getBRGuestModify() {
            return bRGuestModify;
        }

        public void setBRGuestModify(int bRGuestModify) {
            this.bRGuestModify = bRGuestModify;
        }

        public int getBRGuestCancel() {
            return bRGuestCancel;
        }

        public void setBRGuestCancel(int bRGuestCancel) {
            this.bRGuestCancel = bRGuestCancel;
        }

        public int getBRBankCardCreate() {
            return bRBankCardCreate;
        }

        public void setBRBankCardCreate(int bRBankCardCreate) {
            this.bRBankCardCreate = bRBankCardCreate;
        }

        public int getBRBankCardModify() {
            return bRBankCardModify;
        }

        public void setBRBankCardModify(int bRBankCardModify) {
            this.bRBankCardModify = bRBankCardModify;
        }

        public int getBRBankCardCancel() {
            return bRBankCardCancel;
        }

        public void setBRBankCardCancel(int bRBankCardCancel) {
            this.bRBankCardCancel = bRBankCardCancel;
        }

        public int getBRCompanyCardCreate() {
            return bRCompanyCardCreate;
        }

        public void setBRCompanyCardCreate(int bRCompanyCardCreate) {
            this.bRCompanyCardCreate = bRCompanyCardCreate;
        }

        public int getBRCompanyCardModify() {
            return bRCompanyCardModify;
        }

        public void setBRCompanyCardModify(int bRCompanyCardModify) {
            this.bRCompanyCardModify = bRCompanyCardModify;
        }

        public int getBRCompanyCardCancel() {
            return bRCompanyCardCancel;
        }

        public void setBRCompanyCardCancel(int bRCompanyCardCancel) {
            this.bRCompanyCardCancel = bRCompanyCardCancel;
        }

        public int getBRWaitingCreate() {
            return bRWaitingCreate;
        }

        public void setBRWaitingCreate(int bRWaitingCreate) {
            this.bRWaitingCreate = bRWaitingCreate;
        }

        public int getBRWaitingModify() {
            return bRWaitingModify;
        }

        public void setBRWaitingModify(int bRWaitingModify) {
            this.bRWaitingModify = bRWaitingModify;
        }

        public int getBRWaitingCancel() {
            return bRWaitingCancel;
        }

        public void setBRWaitingCancel(int bRWaitingCancel) {
            this.bRWaitingCancel = bRWaitingCancel;
        }

        public int getBRAgentQuataCreate() {
            return bRAgentQuataCreate;
        }

        public void setBRAgentQuataCreate(int bRAgentQuataCreate) {
            this.bRAgentQuataCreate = bRAgentQuataCreate;
        }

        public int getBRAgentQuataModify() {
            return bRAgentQuataModify;
        }

        public void setBRAgentQuataModify(int bRAgentQuataModify) {
            this.bRAgentQuataModify = bRAgentQuataModify;
        }

        public int getBRAgentQuataCancel() {
            return bRAgentQuataCancel;
        }

        public void setBRAgentQuataCancel(int bRAgentQuataCancel) {
            this.bRAgentQuataCancel = bRAgentQuataCancel;
        }

        public int getBROpenCreate() {
            return bROpenCreate;
        }

        public void setBROpenCreate(int bROpenCreate) {
            this.bROpenCreate = bROpenCreate;
        }

        public int getBROpenModify() {
            return bROpenModify;
        }

        public void setBROpenModify(int bROpenModify) {
            this.bROpenModify = bROpenModify;
        }

        public int getBROpenCancel() {
            return bROpenCancel;
        }

        public void setBROpenCancel(int bROpenCancel) {
            this.bROpenCancel = bROpenCancel;
        }

        public int getBRAPICreate() {
            return bRAPICreate;
        }

        public void setBRAPICreate(int bRAPICreate) {
            this.bRAPICreate = bRAPICreate;
        }

        public int getBRAPIModify() {
            return bRAPIModify;
        }

        public void setBRAPIModify(int bRAPIModify) {
            this.bRAPIModify = bRAPIModify;
        }

        public int getBRAPICancel() {
            return bRAPICancel;
        }

        public void setBRAPICancel(int bRAPICancel) {
            this.bRAPICancel = bRAPICancel;
        }

        public int getBRB2CCreate() {
            return bRB2CCreate;
        }

        public void setBRB2CCreate(int bRB2CCreate) {
            this.bRB2CCreate = bRB2CCreate;
        }

        public int getBRB2CModify() {
            return bRB2CModify;
        }

        public void setBRB2CModify(int bRB2CModify) {
            this.bRB2CModify = bRB2CModify;
        }

        public int getBRB2CCancel() {
            return bRB2CCancel;
        }

        public void setBRB2CCancel(int bRB2CCancel) {
            this.bRB2CCancel = bRB2CCancel;
        }

        public int getBROnlineAgentCreate() {
            return bROnlineAgentCreate;
        }

        public void setBROnlineAgentCreate(int bROnlineAgentCreate) {
            this.bROnlineAgentCreate = bROnlineAgentCreate;
        }

        public int getBROnlineAgentModify() {
            return bROnlineAgentModify;
        }

        public void setBROnlineAgentModify(int bROnlineAgentModify) {
            this.bROnlineAgentModify = bROnlineAgentModify;
        }

        public int getBROnlineAgentCancel() {
            return bROnlineAgentCancel;
        }

        public void setBROnlineAgentCancel(int bROnlineAgentCancel) {
            this.bROnlineAgentCancel = bROnlineAgentCancel;
        }

        public int getBROnlineAgentPhoneCreate() {
            return bROnlineAgentPhoneCreate;
        }

        public void setBROnlineAgentPhoneCreate(int bROnlineAgentPhoneCreate) {
            this.bROnlineAgentPhoneCreate = bROnlineAgentPhoneCreate;
        }

        public int getBROnlineAgentPhoneModify() {
            return bROnlineAgentPhoneModify;
        }

        public void setBROnlineAgentPhoneModify(int bROnlineAgentPhoneModify) {
            this.bROnlineAgentPhoneModify = bROnlineAgentPhoneModify;
        }

        public int getBROnlineAgentPhoneCancel() {
            return bROnlineAgentPhoneCancel;
        }

        public void setBROnlineAgentPhoneCancel(int bROnlineAgentPhoneCancel) {
            this.bROnlineAgentPhoneCancel = bROnlineAgentPhoneCancel;
        }

        public int getBRAPIPhoneCreate() {
            return bRAPIPhoneCreate;
        }

        public void setBRAPIPhoneCreate(int bRAPIPhoneCreate) {
            this.bRAPIPhoneCreate = bRAPIPhoneCreate;
        }

        public int getBRAPIPhoneModify() {
            return bRAPIPhoneModify;
        }

        public void setBRAPIPhoneModify(int bRAPIPhoneModify) {
            this.bRAPIPhoneModify = bRAPIPhoneModify;
        }

        public int getBRAPIPhoneCancel() {
            return bRAPIPhoneCancel;
        }

        public void setBRAPIPhoneCancel(int bRAPIPhoneCancel) {
            this.bRAPIPhoneCancel = bRAPIPhoneCancel;
        }

        public int getBROnlineAgentCardCreate() {
            return bROnlineAgentCardCreate;
        }

        public void setBROnlineAgentCardCreate(int bROnlineAgentCardCreate) {
            this.bROnlineAgentCardCreate = bROnlineAgentCardCreate;
        }

        public int getBROnlineAgentCardModify() {
            return bROnlineAgentCardModify;
        }

        public void setBROnlineAgentCardModify(int bROnlineAgentCardModify) {
            this.bROnlineAgentCardModify = bROnlineAgentCardModify;
        }

        public int getBROnlineAgentCardCancel() {
            return bROnlineAgentCardCancel;
        }

        public void setBROnlineAgentCardCancel(int bROnlineAgentCardCancel) {
            this.bROnlineAgentCardCancel = bROnlineAgentCardCancel;
        }

        public int getBROnlineWalletCreate() {
            return bROnlineWalletCreate;
        }

        public void setBROnlineWalletCreate(int bROnlineWalletCreate) {
            this.bROnlineWalletCreate = bROnlineWalletCreate;
        }

        public int getBROnlineWalletModify() {
            return bROnlineWalletModify;
        }

        public void setBROnlineWalletModify(int bROnlineWalletModify) {
            this.bROnlineWalletModify = bROnlineWalletModify;
        }

        public int getBROnlineWalletCancel() {
            return bROnlineWalletCancel;
        }

        public void setBROnlineWalletCancel(int bROnlineWalletCancel) {
            this.bROnlineWalletCancel = bROnlineWalletCancel;
        }

        public int getBRAskChargeOnModify() {
            return bRAskChargeOnModify;
        }

        public void setBRAskChargeOnModify(int bRAskChargeOnModify) {
            this.bRAskChargeOnModify = bRAskChargeOnModify;
        }

        public int getBRRemotePaymentCreate() {
            return bRRemotePaymentCreate;
        }

        public void setBRRemotePaymentCreate(int bRRemotePaymentCreate) {
            this.bRRemotePaymentCreate = bRRemotePaymentCreate;
        }

        public int getBRRemotePaymentModify() {
            return bRRemotePaymentModify;
        }

        public void setBRRemotePaymentModify(int bRRemotePaymentModify) {
            this.bRRemotePaymentModify = bRRemotePaymentModify;
        }

        public int getBRRemotePaymentCancel() {
            return bRRemotePaymentCancel;
        }

        public void setBRRemotePaymentCancel(int bRRemotePaymentCancel) {
            this.bRRemotePaymentCancel = bRRemotePaymentCancel;
        }

        public int getBOAConfirmModify() {
            return bOAConfirmModify;
        }

        public void setBOAConfirmModify(int bOAConfirmModify) {
            this.bOAConfirmModify = bOAConfirmModify;
        }

        public int getBOAConfirmCancel() {
            return bOAConfirmCancel;
        }

        public void setBOAConfirmCancel(int bOAConfirmCancel) {
            this.bOAConfirmCancel = bOAConfirmCancel;
        }

        public int getBOAPhoneModify() {
            return bOAPhoneModify;
        }

        public void setBOAPhoneModify(int bOAPhoneModify) {
            this.bOAPhoneModify = bOAPhoneModify;
        }

        public int getBOAPhoneCancel() {
            return bOAPhoneCancel;
        }

        public void setBOAPhoneCancel(int bOAPhoneCancel) {
            this.bOAPhoneCancel = bOAPhoneCancel;
        }

        public int getBOAAgentModify() {
            return bOAAgentModify;
        }

        public void setBOAAgentModify(int bOAAgentModify) {
            this.bOAAgentModify = bOAAgentModify;
        }

        public int getBOAAgentCancel() {
            return bOAAgentCancel;
        }

        public void setBOAAgentCancel(int bOAAgentCancel) {
            this.bOAAgentCancel = bOAAgentCancel;
        }

        public int getBOABranchModify() {
            return bOABranchModify;
        }

        public void setBOABranchModify(int bOABranchModify) {
            this.bOABranchModify = bOABranchModify;
        }

        public int getBOABranchCancel() {
            return bOABranchCancel;
        }

        public void setBOABranchCancel(int bOABranchCancel) {
            this.bOABranchCancel = bOABranchCancel;
        }

        public int getBOAGuestModify() {
            return bOAGuestModify;
        }

        public void setBOAGuestModify(int bOAGuestModify) {
            this.bOAGuestModify = bOAGuestModify;
        }

        public int getBOAGuestCancel() {
            return bOAGuestCancel;
        }

        public void setBOAGuestCancel(int bOAGuestCancel) {
            this.bOAGuestCancel = bOAGuestCancel;
        }

        public int getBOABankCardModify() {
            return bOABankCardModify;
        }

        public void setBOABankCardModify(int bOABankCardModify) {
            this.bOABankCardModify = bOABankCardModify;
        }

        public int getBOABankCardCancel() {
            return bOABankCardCancel;
        }

        public void setBOABankCardCancel(int bOABankCardCancel) {
            this.bOABankCardCancel = bOABankCardCancel;
        }

        public int getBOACompanyCardModify() {
            return bOACompanyCardModify;
        }

        public void setBOACompanyCardModify(int bOACompanyCardModify) {
            this.bOACompanyCardModify = bOACompanyCardModify;
        }

        public int getBOACompanyCardCancel() {
            return bOACompanyCardCancel;
        }

        public void setBOACompanyCardCancel(int bOACompanyCardCancel) {
            this.bOACompanyCardCancel = bOACompanyCardCancel;
        }

        public int getBOAWaitingModify() {
            return bOAWaitingModify;
        }

        public void setBOAWaitingModify(int bOAWaitingModify) {
            this.bOAWaitingModify = bOAWaitingModify;
        }

        public int getBOAWaitingCancel() {
            return bOAWaitingCancel;
        }

        public void setBOAWaitingCancel(int bOAWaitingCancel) {
            this.bOAWaitingCancel = bOAWaitingCancel;
        }

        public int getBOAAgentQuataModify() {
            return bOAAgentQuataModify;
        }

        public void setBOAAgentQuataModify(int bOAAgentQuataModify) {
            this.bOAAgentQuataModify = bOAAgentQuataModify;
        }

        public int getBOAAgentQuataCancel() {
            return bOAAgentQuataCancel;
        }

        public void setBOAAgentQuataCancel(int bOAAgentQuataCancel) {
            this.bOAAgentQuataCancel = bOAAgentQuataCancel;
        }

        public int getBOAOpenModify() {
            return bOAOpenModify;
        }

        public void setBOAOpenModify(int bOAOpenModify) {
            this.bOAOpenModify = bOAOpenModify;
        }

        public int getBOAOpenCancel() {
            return bOAOpenCancel;
        }

        public void setBOAOpenCancel(int bOAOpenCancel) {
            this.bOAOpenCancel = bOAOpenCancel;
        }

        public int getBOAOnlineWalletModify() {
            return bOAOnlineWalletModify;
        }

        public void setBOAOnlineWalletModify(int bOAOnlineWalletModify) {
            this.bOAOnlineWalletModify = bOAOnlineWalletModify;
        }

        public int getBOAOnlineWalletCancel() {
            return bOAOnlineWalletCancel;
        }

        public void setBOAOnlineWalletCancel(int bOAOnlineWalletCancel) {
            this.bOAOnlineWalletCancel = bOAOnlineWalletCancel;
        }

        public int getBOARemotePaymentModify() {
            return bOARemotePaymentModify;
        }

        public void setBOARemotePaymentModify(int bOARemotePaymentModify) {
            this.bOARemotePaymentModify = bOARemotePaymentModify;
        }

        public int getBOARemotePaymentCancel() {
            return bOARemotePaymentCancel;
        }

        public void setBOARemotePaymentCancel(int bOARemotePaymentCancel) {
            this.bOARemotePaymentCancel = bOARemotePaymentCancel;
        }

        public int getGAAllowReprint() {
            return gAAllowReprint;
        }

        public void setGAAllowReprint(int gAAllowReprint) {
            this.gAAllowReprint = gAAllowReprint;
        }

        public int getGAAllowBookingAFMin() {
            return gAAllowBookingAFMin;
        }

        public void setGAAllowBookingAFMin(int gAAllowBookingAFMin) {
            this.gAAllowBookingAFMin = gAAllowBookingAFMin;
        }

        public int getGACancellationBFMin() {
            return gACancellationBFMin;
        }

        public void setGACancellationBFMin(int gACancellationBFMin) {
            this.gACancellationBFMin = gACancellationBFMin;
        }

        public int getGAAllowFareChange() {
            return gAAllowFareChange;
        }

        public void setGAAllowFareChange(int gAAllowFareChange) {
            this.gAAllowFareChange = gAAllowFareChange;
        }

        public int getGAAllowBackDateBooking() {
            return gAAllowBackDateBooking;
        }

        public void setGAAllowBackDateBooking(int gAAllowBackDateBooking) {
            this.gAAllowBackDateBooking = gAAllowBackDateBooking;
        }

        public int getGAAllowBackDateCancellation() {
            return gAAllowBackDateCancellation;
        }

        public void setGAAllowBackDateCancellation(int gAAllowBackDateCancellation) {
            this.gAAllowBackDateCancellation = gAAllowBackDateCancellation;
        }

        public int getGAAllowDiscount() {
            return gAAllowDiscount;
        }

        public void setGAAllowDiscount(int gAAllowDiscount) {
            this.gAAllowDiscount = gAAllowDiscount;
        }

        public int getGAAllowInternetAuthorization() {
            return gAAllowInternetAuthorization;
        }

        public void setGAAllowInternetAuthorization(int gAAllowInternetAuthorization) {
            this.gAAllowInternetAuthorization = gAAllowInternetAuthorization;
        }

        public int getGAAllowModify() {
            return gAAllowModify;
        }

        public void setGAAllowModify(int gAAllowModify) {
            this.gAAllowModify = gAAllowModify;
        }

        public int getGAAllowRemoveQuota() {
            return gAAllowRemoveQuota;
        }

        public void setGAAllowRemoveQuota(int gAAllowRemoveQuota) {
            this.gAAllowRemoveQuota = gAAllowRemoveQuota;
        }

        public int getGAStopBooking() {
            return gAStopBooking;
        }

        public void setGAStopBooking(int gAStopBooking) {
            this.gAStopBooking = gAStopBooking;
        }

        public int getGAShowAllRoute() {
            return gAShowAllRoute;
        }

        public void setGAShowAllRoute(int gAShowAllRoute) {
            this.gAShowAllRoute = gAShowAllRoute;
        }

        public int getGAShowCollection() {
            return gAShowCollection;
        }

        public void setGAShowCollection(int gAShowCollection) {
            this.gAShowCollection = gAShowCollection;
        }

        public int getGAArrangementTransfer() {
            return gAArrangementTransfer;
        }

        public void setGAArrangementTransfer(int gAArrangementTransfer) {
            this.gAArrangementTransfer = gAArrangementTransfer;
        }

        public int getGAShowBackDate() {
            return gAShowBackDate;
        }

        public void setGAShowBackDate(int gAShowBackDate) {
            this.gAShowBackDate = gAShowBackDate;
        }

        public int getGARouteTransfer() {
            return gARouteTransfer;
        }

        public void setGARouteTransfer(int gARouteTransfer) {
            this.gARouteTransfer = gARouteTransfer;
        }

        public int getGAChartSMS() {
            return gAChartSMS;
        }

        public void setGAChartSMS(int gAChartSMS) {
            this.gAChartSMS = gAChartSMS;
        }

        public int getGABranchWiseCollection() {
            return gABranchWiseCollection;
        }

        public void setGABranchWiseCollection(int gABranchWiseCollection) {
            this.gABranchWiseCollection = gABranchWiseCollection;
        }

        public int getGAXmlSourceMemory() {
            return gAXmlSourceMemory;
        }

        public void setGAXmlSourceMemory(int gAXmlSourceMemory) {
            this.gAXmlSourceMemory = gAXmlSourceMemory;
        }

        public int getGAAllowMultipleBooking() {
            return gAAllowMultipleBooking;
        }

        public void setGAAllowMultipleBooking(int gAAllowMultipleBooking) {
            this.gAAllowMultipleBooking = gAAllowMultipleBooking;
        }

        public int getGAAllowModifyNamePhone() {
            return gAAllowModifyNamePhone;
        }

        public void setGAAllowModifyNamePhone(int gAAllowModifyNamePhone) {
            this.gAAllowModifyNamePhone = gAAllowModifyNamePhone;
        }

        public int getGAAllowModifyToCity() {
            return gAAllowModifyToCity;
        }

        public void setGAAllowModifyToCity(int gAAllowModifyToCity) {
            this.gAAllowModifyToCity = gAAllowModifyToCity;
        }

        public int getGAApplyRouteBFMin() {
            return gAApplyRouteBFMin;
        }

        public void setGAApplyRouteBFMin(int gAApplyRouteBFMin) {
            this.gAApplyRouteBFMin = gAApplyRouteBFMin;
        }

        public int getGAAllowConfirmToOpen() {
            return gAAllowConfirmToOpen;
        }

        public void setGAAllowConfirmToOpen(int gAAllowConfirmToOpen) {
            this.gAAllowConfirmToOpen = gAAllowConfirmToOpen;
        }

        public int getGAShowOnlineAgent() {
            return gAShowOnlineAgent;
        }

        public void setGAShowOnlineAgent(int gAShowOnlineAgent) {
            this.gAShowOnlineAgent = gAShowOnlineAgent;
        }

        public int getGAShowOnlyBranchCityAgent() {
            return gAShowOnlyBranchCityAgent;
        }

        public void setGAShowOnlyBranchCityAgent(int gAShowOnlyBranchCityAgent) {
            this.gAShowOnlyBranchCityAgent = gAShowOnlyBranchCityAgent;
        }

        public int getGASiteAccess() {
            return gASiteAccess;
        }

        public void setGASiteAccess(int gASiteAccess) {
            this.gASiteAccess = gASiteAccess;
        }

        public String getGASiteNames() {
            return gASiteNames;
        }

        public void setGASiteNames(String gASiteNames) {
            this.gASiteNames = gASiteNames;
        }

        public int getGAAllowPrepaidBranchBooking() {
            return gAAllowPrepaidBranchBooking;
        }

        public void setGAAllowPrepaidBranchBooking(int gAAllowPrepaidBranchBooking) {
            this.gAAllowPrepaidBranchBooking = gAAllowPrepaidBranchBooking;
        }

        public int getGAAllowChangeRefundCharges() {
            return gAAllowChangeRefundCharges;
        }

        public void setGAAllowChangeRefundCharges(int gAAllowChangeRefundCharges) {
            this.gAAllowChangeRefundCharges = gAAllowChangeRefundCharges;
        }

        public int getGAAllowLessRefundCharges() {
            return gAAllowLessRefundCharges;
        }

        public void setGAAllowLessRefundCharges(int gAAllowLessRefundCharges) {
            this.gAAllowLessRefundCharges = gAAllowLessRefundCharges;
        }

        public int getGAAllowPetrolPumpModule() {
            return gAAllowPetrolPumpModule;
        }

        public void setGAAllowPetrolPumpModule(int gAAllowPetrolPumpModule) {
            this.gAAllowPetrolPumpModule = gAAllowPetrolPumpModule;
        }

        public int getGAAllowKMEntry() {
            return gAAllowKMEntry;
        }

        public void setGAAllowKMEntry(int gAAllowKMEntry) {
            this.gAAllowKMEntry = gAAllowKMEntry;
        }

        public int getGAAllowReturnPhoneBooking() {
            return gAAllowReturnPhoneBooking;
        }

        public void setGAAllowReturnPhoneBooking(int gAAllowReturnPhoneBooking) {
            this.gAAllowReturnPhoneBooking = gAAllowReturnPhoneBooking;
        }

        public int getGAShowPrepaidBalance() {
            return gAShowPrepaidBalance;
        }

        public void setGAShowPrepaidBalance(int gAShowPrepaidBalance) {
            this.gAShowPrepaidBalance = gAShowPrepaidBalance;
        }

        public int getGAAllowLiveBusStatusEntry() {
            return gAAllowLiveBusStatusEntry;
        }

        public void setGAAllowLiveBusStatusEntry(int gAAllowLiveBusStatusEntry) {
            this.gAAllowLiveBusStatusEntry = gAAllowLiveBusStatusEntry;
        }

        public int getGAAllowLiveBusStatusEntryOtherPickup() {
            return gAAllowLiveBusStatusEntryOtherPickup;
        }

        public void setGAAllowLiveBusStatusEntryOtherPickup(int gAAllowLiveBusStatusEntryOtherPickup) {
            this.gAAllowLiveBusStatusEntryOtherPickup = gAAllowLiveBusStatusEntryOtherPickup;
        }

        public int getGAIsChangeBusStatusPickupTime() {
            return gAIsChangeBusStatusPickupTime;
        }

        public void setGAIsChangeBusStatusPickupTime(int gAIsChangeBusStatusPickupTime) {
            this.gAIsChangeBusStatusPickupTime = gAIsChangeBusStatusPickupTime;
        }

        public int getGAIsSendBranchAddressSMS() {
            return gAIsSendBranchAddressSMS;
        }

        public void setGAIsSendBranchAddressSMS(int gAIsSendBranchAddressSMS) {
            this.gAIsSendBranchAddressSMS = gAIsSendBranchAddressSMS;
        }

        public int getGAAllowModifyFromCity() {
            return gAAllowModifyFromCity;
        }

        public void setGAAllowModifyFromCity(int gAAllowModifyFromCity) {
            this.gAAllowModifyFromCity = gAAllowModifyFromCity;
        }

        public int getGAIsPromptBackDateBooking() {
            return gAIsPromptBackDateBooking;
        }

        public void setGAIsPromptBackDateBooking(int gAIsPromptBackDateBooking) {
            this.gAIsPromptBackDateBooking = gAIsPromptBackDateBooking;
        }

        public int getGAAllowBusHubRecharge() {
            return gAAllowBusHubRecharge;
        }

        public void setGAAllowBusHubRecharge(int gAAllowBusHubRecharge) {
            this.gAAllowBusHubRecharge = gAAllowBusHubRecharge;
        }

        public int getGARemoveSameCityHold() {
            return gARemoveSameCityHold;
        }

        public void setGARemoveSameCityHold(int gARemoveSameCityHold) {
            this.gARemoveSameCityHold = gARemoveSameCityHold;
        }

        public int getGARemoveOtherCityHold() {
            return gARemoveOtherCityHold;
        }

        public void setGARemoveOtherCityHold(int gARemoveOtherCityHold) {
            this.gARemoveOtherCityHold = gARemoveOtherCityHold;
        }

        public int getGAAllowNonReported() {
            return gAAllowNonReported;
        }

        public void setGAAllowNonReported(int gAAllowNonReported) {
            this.gAAllowNonReported = gAAllowNonReported;
        }

        public int getGAReleaseAPIB2CHold() {
            return gAReleaseAPIB2CHold;
        }

        public void setGAReleaseAPIB2CHold(int gAReleaseAPIB2CHold) {
            this.gAReleaseAPIB2CHold = gAReleaseAPIB2CHold;
        }

        public int getGAAllowFareChangeInPhoneModify() {
            return gAAllowFareChangeInPhoneModify;
        }

        public void setGAAllowFareChangeInPhoneModify(int gAAllowFareChangeInPhoneModify) {
            this.gAAllowFareChangeInPhoneModify = gAAllowFareChangeInPhoneModify;
        }

        public int getGAAllowLessFareChange() {
            return gAAllowLessFareChange;
        }

        public void setGAAllowLessFareChange(int gAAllowLessFareChange) {
            this.gAAllowLessFareChange = gAAllowLessFareChange;
        }

        public int getRAViewInquiry0() {
            return rAViewInquiry0;
        }

        public void setRAViewInquiry0(int rAViewInquiry0) {
            this.rAViewInquiry0 = rAViewInquiry0;
        }

        public int getRAViewInquiry1() {
            return rAViewInquiry1;
        }

        public void setRAViewInquiry1(int rAViewInquiry1) {
            this.rAViewInquiry1 = rAViewInquiry1;
        }

        public int getRAViewInquiry2() {
            return rAViewInquiry2;
        }

        public void setRAViewInquiry2(int rAViewInquiry2) {
            this.rAViewInquiry2 = rAViewInquiry2;
        }

        public int getRAViewInquiry3() {
            return rAViewInquiry3;
        }

        public void setRAViewInquiry3(int rAViewInquiry3) {
            this.rAViewInquiry3 = rAViewInquiry3;
        }

        public int getRAViewInquiry4() {
            return rAViewInquiry4;
        }

        public void setRAViewInquiry4(int rAViewInquiry4) {
            this.rAViewInquiry4 = rAViewInquiry4;
        }

        public int getRAViewInquiry5() {
            return rAViewInquiry5;
        }

        public void setRAViewInquiry5(int rAViewInquiry5) {
            this.rAViewInquiry5 = rAViewInquiry5;
        }

        public int getRAViewInquiry6() {
            return rAViewInquiry6;
        }

        public void setRAViewInquiry6(int rAViewInquiry6) {
            this.rAViewInquiry6 = rAViewInquiry6;
        }

        public int getRAViewInquiry7() {
            return rAViewInquiry7;
        }

        public void setRAViewInquiry7(int rAViewInquiry7) {
            this.rAViewInquiry7 = rAViewInquiry7;
        }

        public int getRAViewInquiry8() {
            return rAViewInquiry8;
        }

        public void setRAViewInquiry8(int rAViewInquiry8) {
            this.rAViewInquiry8 = rAViewInquiry8;
        }

        public int getRAViewInquiry9() {
            return rAViewInquiry9;
        }

        public void setRAViewInquiry9(int rAViewInquiry9) {
            this.rAViewInquiry9 = rAViewInquiry9;
        }

        public int getRAViewInquiry10() {
            return rAViewInquiry10;
        }

        public void setRAViewInquiry10(int rAViewInquiry10) {
            this.rAViewInquiry10 = rAViewInquiry10;
        }

        public int getRAViewInquiry11() {
            return rAViewInquiry11;
        }

        public void setRAViewInquiry11(int rAViewInquiry11) {
            this.rAViewInquiry11 = rAViewInquiry11;
        }

        public int getRAViewInquiry12() {
            return rAViewInquiry12;
        }

        public void setRAViewInquiry12(int rAViewInquiry12) {
            this.rAViewInquiry12 = rAViewInquiry12;
        }

        public int getRACityDropWise() {
            return rACityDropWise;
        }

        public void setRACityDropWise(int rACityDropWise) {
            this.rACityDropWise = rACityDropWise;
        }

        public int getRASubRouteTicketCount() {
            return rASubRouteTicketCount;
        }

        public void setRASubRouteTicketCount(int rASubRouteTicketCount) {
            this.rASubRouteTicketCount = rASubRouteTicketCount;
        }

        public int getRAPassengerBooking() {
            return rAPassengerBooking;
        }

        public void setRAPassengerBooking(int rAPassengerBooking) {
            this.rAPassengerBooking = rAPassengerBooking;
        }

        public int getRARouteDeparture() {
            return rARouteDeparture;
        }

        public void setRARouteDeparture(int rARouteDeparture) {
            this.rARouteDeparture = rARouteDeparture;
        }

        public int getRARouteTimeWiseMemo() {
            return rARouteTimeWiseMemo;
        }

        public void setRARouteTimeWiseMemo(int rARouteTimeWiseMemo) {
            this.rARouteTimeWiseMemo = rARouteTimeWiseMemo;
        }

        public int getRAPickupWiseInquiry() {
            return rAPickupWiseInquiry;
        }

        public void setRAPickupWiseInquiry(int rAPickupWiseInquiry) {
            this.rAPickupWiseInquiry = rAPickupWiseInquiry;
        }

        public int getRAMainRouteWisePassengerDetails() {
            return rAMainRouteWisePassengerDetails;
        }

        public void setRAMainRouteWisePassengerDetails(int rAMainRouteWisePassengerDetails) {
            this.rAMainRouteWisePassengerDetails = rAMainRouteWisePassengerDetails;
        }

        public int getRARouteTimeBookingMemo() {
            return rARouteTimeBookingMemo;
        }

        public void setRARouteTimeBookingMemo(int rARouteTimeBookingMemo) {
            this.rARouteTimeBookingMemo = rARouteTimeBookingMemo;
        }

        public int getRABusIncomeReceipt() {
            return rABusIncomeReceipt;
        }

        public void setRABusIncomeReceipt(int rABusIncomeReceipt) {
            this.rABusIncomeReceipt = rABusIncomeReceipt;
        }

        public int getRAAllowCityWiseSeatChartReport() {
            return rAAllowCityWiseSeatChartReport;
        }

        public void setRAAllowCityWiseSeatChartReport(int rAAllowCityWiseSeatChartReport) {
            this.rAAllowCityWiseSeatChartReport = rAAllowCityWiseSeatChartReport;
        }

        public int getRAAllowBulkTicketPrint() {
            return rAAllowBulkTicketPrint;
        }

        public void setRAAllowBulkTicketPrint(int rAAllowBulkTicketPrint) {
            this.rAAllowBulkTicketPrint = rAAllowBulkTicketPrint;
        }

        public int getRAPNRWiseAmenities() {
            return rAPNRWiseAmenities;
        }

        public void setRAPNRWiseAmenities(int rAPNRWiseAmenities) {
            this.rAPNRWiseAmenities = rAPNRWiseAmenities;
        }

        public int getRARoutePickUpDropChart() {
            return rARoutePickUpDropChart;
        }

        public void setRARoutePickUpDropChart(int rARoutePickUpDropChart) {
            this.rARoutePickUpDropChart = rARoutePickUpDropChart;
        }

        public int getRADailyRouteTimeWiseMemo() {
            return rADailyRouteTimeWiseMemo;
        }

        public void setRADailyRouteTimeWiseMemo(int rADailyRouteTimeWiseMemo) {
            this.rADailyRouteTimeWiseMemo = rADailyRouteTimeWiseMemo;
        }

        public int getRAFutureBookingGraph() {
            return rAFutureBookingGraph;
        }

        public void setRAFutureBookingGraph(int rAFutureBookingGraph) {
            this.rAFutureBookingGraph = rAFutureBookingGraph;
        }

        public int getRACityPickupDropWisechart() {
            return rACityPickupDropWisechart;
        }

        public void setRACityPickupDropWisechart(int rACityPickupDropWisechart) {
            this.rACityPickupDropWisechart = rACityPickupDropWisechart;
        }

        public int getRAPickupDropReport() {
            return rAPickupDropReport;
        }

        public void setRAPickupDropReport(int rAPickupDropReport) {
            this.rAPickupDropReport = rAPickupDropReport;
        }

        public int getRAReservationDetailReport() {
            return rAReservationDetailReport;
        }

        public void setRAReservationDetailReport(int rAReservationDetailReport) {
            this.rAReservationDetailReport = rAReservationDetailReport;
        }

        public int getRAPassengerList() {
            return rAPassengerList;
        }

        public void setRAPassengerList(int rAPassengerList) {
            this.rAPassengerList = rAPassengerList;
        }

        public int getRADepartureReportSeatWise() {
            return rADepartureReportSeatWise;
        }

        public void setRADepartureReportSeatWise(int rADepartureReportSeatWise) {
            this.rADepartureReportSeatWise = rADepartureReportSeatWise;
        }

        public int getRASeatWiseInquiryReport() {
            return rASeatWiseInquiryReport;
        }

        public void setRASeatWiseInquiryReport(int rASeatWiseInquiryReport) {
            this.rASeatWiseInquiryReport = rASeatWiseInquiryReport;
        }

        public int getRABranchWiseInquiryReport() {
            return rABranchWiseInquiryReport;
        }

        public void setRABranchWiseInquiryReport(int rABranchWiseInquiryReport) {
            this.rABranchWiseInquiryReport = rABranchWiseInquiryReport;
        }

        public int getRAReservationDetailReportAmount() {
            return rAReservationDetailReportAmount;
        }

        public void setRAReservationDetailReportAmount(int rAReservationDetailReportAmount) {
            this.rAReservationDetailReportAmount = rAReservationDetailReportAmount;
        }

        public String getOSAdlabsURL() {
            return oSAdlabsURL;
        }

        public void setOSAdlabsURL(String oSAdlabsURL) {
            this.oSAdlabsURL = oSAdlabsURL;
        }

        public int getOSIsLabelDisplay() {
            return oSIsLabelDisplay;
        }

        public void setOSIsLabelDisplay(int oSIsLabelDisplay) {
            this.oSIsLabelDisplay = oSIsLabelDisplay;
        }

        public String getOSLableText() {
            return oSLableText;
        }

        public void setOSLableText(String oSLableText) {
            this.oSLableText = oSLableText;
        }

        public int getIsParcelBookingRights() {
            return isParcelBookingRights;
        }

        public void setIsParcelBookingRights(int isParcelBookingRights) {
            this.isParcelBookingRights = isParcelBookingRights;
        }

        public int getGAAllowToSetLessPhoneCancelTime() {
            return gAAllowToSetLessPhoneCancelTime;
        }

        public void setGAAllowToSetLessPhoneCancelTime(int gAAllowToSetLessPhoneCancelTime) {
            this.gAAllowToSetLessPhoneCancelTime = gAAllowToSetLessPhoneCancelTime;
        }

        public int getGAAllowBookWithNewFare() {
            return gAAllowBookWithNewFare;
        }

        public void setGAAllowBookWithNewFare(int gAAllowBookWithNewFare) {
            this.gAAllowBookWithNewFare = gAAllowBookWithNewFare;
        }

        public int getBRBranchQuotaCreate() {
            return bRBranchQuotaCreate;
        }

        public void setBRBranchQuotaCreate(int bRBranchQuotaCreate) {
            this.bRBranchQuotaCreate = bRBranchQuotaCreate;
        }

        public int getBRBranchQuotaModify() {
            return bRBranchQuotaModify;
        }

        public void setBRBranchQuotaModify(int bRBranchQuotaModify) {
            this.bRBranchQuotaModify = bRBranchQuotaModify;
        }

        public int getBRBranchQuotaCancel() {
            return bRBranchQuotaCancel;
        }

        public void setBRBranchQuotaCancel(int bRBranchQuotaCancel) {
            this.bRBranchQuotaCancel = bRBranchQuotaCancel;
        }

        public int getBOABranchQuataModify() {
            return bOABranchQuataModify;
        }

        public void setBOABranchQuataModify(int bOABranchQuataModify) {
            this.bOABranchQuataModify = bOABranchQuataModify;
        }

        public int getBOABranchQuataCancel() {
            return bOABranchQuataCancel;
        }

        public void setBOABranchQuataCancel(int bOABranchQuataCancel) {
            this.bOABranchQuataCancel = bOABranchQuataCancel;
        }

        public int getGAAllowMultyDateAgentQuotaExe() {
            return gAAllowMultyDateAgentQuotaExe;
        }

        public void setGAAllowMultyDateAgentQuotaExe(int gAAllowMultyDateAgentQuotaExe) {
            this.gAAllowMultyDateAgentQuotaExe = gAAllowMultyDateAgentQuotaExe;
        }

        public int getGAAllowMultyDateBranchQuotaExe() {
            return gAAllowMultyDateBranchQuotaExe;
        }

        public void setGAAllowMultyDateBranchQuotaExe(int gAAllowMultyDateBranchQuotaExe) {
            this.gAAllowMultyDateBranchQuotaExe = gAAllowMultyDateBranchQuotaExe;
        }

        public int getGAAllowCopyTimeExe() {
            return gAAllowCopyTimeExe;
        }

        public void setGAAllowCopyTimeExe(int gAAllowCopyTimeExe) {
            this.gAAllowCopyTimeExe = gAAllowCopyTimeExe;
        }

        public int getGAAllowSetRouteFareFromExe() {
            return gAAllowSetRouteFareFromExe;
        }

        public void setGAAllowSetRouteFareFromExe(int gAAllowSetRouteFareFromExe) {
            this.gAAllowSetRouteFareFromExe = gAAllowSetRouteFareFromExe;
        }

        public int getGAAllowSetSeatPermanentHold() {
            return gAAllowSetSeatPermanentHold;
        }

        public void setGAAllowSetSeatPermanentHold(int gAAllowSetSeatPermanentHold) {
            this.gAAllowSetSeatPermanentHold = gAAllowSetSeatPermanentHold;
        }

        public int getGAModifyOnOtherCompanyRoute() {
            return gAModifyOnOtherCompanyRoute;
        }

        public void setGAModifyOnOtherCompanyRoute(int gAModifyOnOtherCompanyRoute) {
            this.gAModifyOnOtherCompanyRoute = gAModifyOnOtherCompanyRoute;
        }

        public int getGAAllowUserToSetSeatWiseFare() {
            return gAAllowUserToSetSeatWiseFare;
        }

        public void setGAAllowUserToSetSeatWiseFare(int gAAllowUserToSetSeatWiseFare) {
            this.gAAllowUserToSetSeatWiseFare = gAAllowUserToSetSeatWiseFare;
        }

        public int getGAAllowToChangePhoneNoOnResendSMS() {
            return gAAllowToChangePhoneNoOnResendSMS;
        }

        public void setGAAllowToChangePhoneNoOnResendSMS(int gAAllowToChangePhoneNoOnResendSMS) {
            this.gAAllowToChangePhoneNoOnResendSMS = gAAllowToChangePhoneNoOnResendSMS;
        }

        public int getGAAllowToSendBusTrackSMS() {
            return gAAllowToSendBusTrackSMS;
        }

        public void setGAAllowToSendBusTrackSMS(int gAAllowToSendBusTrackSMS) {
            this.gAAllowToSendBusTrackSMS = gAAllowToSendBusTrackSMS;
        }

        public int getGAAddFareOnModify() {
            return gAAddFareOnModify;
        }

        public void setGAAddFareOnModify(int gAAddFareOnModify) {
            this.gAAddFareOnModify = gAAddFareOnModify;
        }

        public int getGAAllowToPrintCollectionReport() {
            return gAAllowToPrintCollectionReport;
        }

        public void setGAAllowToPrintCollectionReport(int gAAllowToPrintCollectionReport) {
            this.gAAllowToPrintCollectionReport = gAAllowToPrintCollectionReport;
        }

        public int getGAAskCancelBranchUser() {
            return gAAskCancelBranchUser;
        }

        public void setGAAskCancelBranchUser(int gAAskCancelBranchUser) {
            this.gAAskCancelBranchUser = gAAskCancelBranchUser;
        }

        public int getGAAllowMultipleLogin() {
            return gAAllowMultipleLogin;
        }

        public void setGAAllowMultipleLogin(int gAAllowMultipleLogin) {
            this.gAAllowMultipleLogin = gAAllowMultipleLogin;
        }

        public int getGAAllowBusScheduleOnEXE() {
            return gAAllowBusScheduleOnEXE;
        }

        public void setGAAllowBusScheduleOnEXE(int gAAllowBusScheduleOnEXE) {
            this.gAAllowBusScheduleOnEXE = gAAllowBusScheduleOnEXE;
        }

        public int getGAAllowPhoneOnHoldSeat() {
            return gAAllowPhoneOnHoldSeat;
        }

        public void setGAAllowPhoneOnHoldSeat(int gAAllowPhoneOnHoldSeat) {
            this.gAAllowPhoneOnHoldSeat = gAAllowPhoneOnHoldSeat;
        }

        public int getGAIsBranchPNRModifyCharges() {
            return gAIsBranchPNRModifyCharges;
        }

        public void setGAIsBranchPNRModifyCharges(int gAIsBranchPNRModifyCharges) {
            this.gAIsBranchPNRModifyCharges = gAIsBranchPNRModifyCharges;
        }

        public int getFBRConfirmCreate() {
            return fBRConfirmCreate;
        }

        public void setFBRConfirmCreate(int fBRConfirmCreate) {
            this.fBRConfirmCreate = fBRConfirmCreate;
        }

        public int getFBRConfirmModify() {
            return fBRConfirmModify;
        }

        public void setFBRConfirmModify(int fBRConfirmModify) {
            this.fBRConfirmModify = fBRConfirmModify;
        }

        public int getFBRConfirmCancel() {
            return fBRConfirmCancel;
        }

        public void setFBRConfirmCancel(int fBRConfirmCancel) {
            this.fBRConfirmCancel = fBRConfirmCancel;
        }

        public int getFBRPhoneCreate() {
            return fBRPhoneCreate;
        }

        public void setFBRPhoneCreate(int fBRPhoneCreate) {
            this.fBRPhoneCreate = fBRPhoneCreate;
        }

        public int getFBRPhoneModify() {
            return fBRPhoneModify;
        }

        public void setFBRPhoneModify(int fBRPhoneModify) {
            this.fBRPhoneModify = fBRPhoneModify;
        }

        public int getFBRPhoneCancel() {
            return fBRPhoneCancel;
        }

        public void setFBRPhoneCancel(int fBRPhoneCancel) {
            this.fBRPhoneCancel = fBRPhoneCancel;
        }

        public int getFBRAgentCreate() {
            return fBRAgentCreate;
        }

        public void setFBRAgentCreate(int fBRAgentCreate) {
            this.fBRAgentCreate = fBRAgentCreate;
        }

        public int getFBRAgentModify() {
            return fBRAgentModify;
        }

        public void setFBRAgentModify(int fBRAgentModify) {
            this.fBRAgentModify = fBRAgentModify;
        }

        public int getFBRAgentCancel() {
            return fBRAgentCancel;
        }

        public void setFBRAgentCancel(int fBRAgentCancel) {
            this.fBRAgentCancel = fBRAgentCancel;
        }

        public int getFBRBranchCreate() {
            return fBRBranchCreate;
        }

        public void setFBRBranchCreate(int fBRBranchCreate) {
            this.fBRBranchCreate = fBRBranchCreate;
        }

        public int getFBRBranchModify() {
            return fBRBranchModify;
        }

        public void setFBRBranchModify(int fBRBranchModify) {
            this.fBRBranchModify = fBRBranchModify;
        }

        public int getFBRBranchCancel() {
            return fBRBranchCancel;
        }

        public void setFBRBranchCancel(int fBRBranchCancel) {
            this.fBRBranchCancel = fBRBranchCancel;
        }

        public int getFBRGuestCreate() {
            return fBRGuestCreate;
        }

        public void setFBRGuestCreate(int fBRGuestCreate) {
            this.fBRGuestCreate = fBRGuestCreate;
        }

        public int getFBRGuestModify() {
            return fBRGuestModify;
        }

        public void setFBRGuestModify(int fBRGuestModify) {
            this.fBRGuestModify = fBRGuestModify;
        }

        public int getFBRGuestCancel() {
            return fBRGuestCancel;
        }

        public void setFBRGuestCancel(int fBRGuestCancel) {
            this.fBRGuestCancel = fBRGuestCancel;
        }

        public int getFBRBankCardCreate() {
            return fBRBankCardCreate;
        }

        public void setFBRBankCardCreate(int fBRBankCardCreate) {
            this.fBRBankCardCreate = fBRBankCardCreate;
        }

        public int getFBRBankCardModify() {
            return fBRBankCardModify;
        }

        public void setFBRBankCardModify(int fBRBankCardModify) {
            this.fBRBankCardModify = fBRBankCardModify;
        }

        public int getFBRBankCardCancel() {
            return fBRBankCardCancel;
        }

        public void setFBRBankCardCancel(int fBRBankCardCancel) {
            this.fBRBankCardCancel = fBRBankCardCancel;
        }

        public int getFBRWaitingCreate() {
            return fBRWaitingCreate;
        }

        public void setFBRWaitingCreate(int fBRWaitingCreate) {
            this.fBRWaitingCreate = fBRWaitingCreate;
        }

        public int getFBRWaitingModify() {
            return fBRWaitingModify;
        }

        public void setFBRWaitingModify(int fBRWaitingModify) {
            this.fBRWaitingModify = fBRWaitingModify;
        }

        public int getFBRWaitingCancel() {
            return fBRWaitingCancel;
        }

        public void setFBRWaitingCancel(int fBRWaitingCancel) {
            this.fBRWaitingCancel = fBRWaitingCancel;
        }

        public int getFBRCompanyCardCreate() {
            return fBRCompanyCardCreate;
        }

        public void setFBRCompanyCardCreate(int fBRCompanyCardCreate) {
            this.fBRCompanyCardCreate = fBRCompanyCardCreate;
        }

        public int getFBRCompanyCardModify() {
            return fBRCompanyCardModify;
        }

        public void setFBRCompanyCardModify(int fBRCompanyCardModify) {
            this.fBRCompanyCardModify = fBRCompanyCardModify;
        }

        public int getFBRCompanyCardCancel() {
            return fBRCompanyCardCancel;
        }

        public void setFBRCompanyCardCancel(int fBRCompanyCardCancel) {
            this.fBRCompanyCardCancel = fBRCompanyCardCancel;
        }

        public int getFBROnlineAgentCreate() {
            return fBROnlineAgentCreate;
        }

        public void setFBROnlineAgentCreate(int fBROnlineAgentCreate) {
            this.fBROnlineAgentCreate = fBROnlineAgentCreate;
        }

        public int getFBROnlineAgentModify() {
            return fBROnlineAgentModify;
        }

        public void setFBROnlineAgentModify(int fBROnlineAgentModify) {
            this.fBROnlineAgentModify = fBROnlineAgentModify;
        }

        public int getFBROnlineAgentCancel() {
            return fBROnlineAgentCancel;
        }

        public void setFBROnlineAgentCancel(int fBROnlineAgentCancel) {
            this.fBROnlineAgentCancel = fBROnlineAgentCancel;
        }

        public int getFBRAPIAgentCreate() {
            return fBRAPIAgentCreate;
        }

        public void setFBRAPIAgentCreate(int fBRAPIAgentCreate) {
            this.fBRAPIAgentCreate = fBRAPIAgentCreate;
        }

        public int getFBRAPIAgentModify() {
            return fBRAPIAgentModify;
        }

        public void setFBRAPIAgentModify(int fBRAPIAgentModify) {
            this.fBRAPIAgentModify = fBRAPIAgentModify;
        }

        public int getFBRAPIAgentCancel() {
            return fBRAPIAgentCancel;
        }

        public void setFBRAPIAgentCancel(int fBRAPIAgentCancel) {
            this.fBRAPIAgentCancel = fBRAPIAgentCancel;
        }

        public int getFBRB2CAgentCreate() {
            return fBRB2CAgentCreate;
        }

        public void setFBRB2CAgentCreate(int fBRB2CAgentCreate) {
            this.fBRB2CAgentCreate = fBRB2CAgentCreate;
        }

        public int getFBRB2CAgentModify() {
            return fBRB2CAgentModify;
        }

        public void setFBRB2CAgentModify(int fBRB2CAgentModify) {
            this.fBRB2CAgentModify = fBRB2CAgentModify;
        }

        public int getFBRB2CAgentCancel() {
            return fBRB2CAgentCancel;
        }

        public void setFBRB2CAgentCancel(int fBRB2CAgentCancel) {
            this.fBRB2CAgentCancel = fBRB2CAgentCancel;
        }

        public int getFBRAgentQuotaCreate() {
            return fBRAgentQuotaCreate;
        }

        public void setFBRAgentQuotaCreate(int fBRAgentQuotaCreate) {
            this.fBRAgentQuotaCreate = fBRAgentQuotaCreate;
        }

        public int getFBRAgentQuotaModify() {
            return fBRAgentQuotaModify;
        }

        public void setFBRAgentQuotaModify(int fBRAgentQuotaModify) {
            this.fBRAgentQuotaModify = fBRAgentQuotaModify;
        }

        public int getFBRAgentQuotaCancel() {
            return fBRAgentQuotaCancel;
        }

        public void setFBRAgentQuotaCancel(int fBRAgentQuotaCancel) {
            this.fBRAgentQuotaCancel = fBRAgentQuotaCancel;
        }

        public int getFBRITSPLWalletCreate() {
            return fBRITSPLWalletCreate;
        }

        public void setFBRITSPLWalletCreate(int fBRITSPLWalletCreate) {
            this.fBRITSPLWalletCreate = fBRITSPLWalletCreate;
        }

        public int getFBRITSPLWalletModify() {
            return fBRITSPLWalletModify;
        }

        public void setFBRITSPLWalletModify(int fBRITSPLWalletModify) {
            this.fBRITSPLWalletModify = fBRITSPLWalletModify;
        }

        public int getFBRITSPLWalletCancel() {
            return fBRITSPLWalletCancel;
        }

        public void setFBRITSPLWalletCancel(int fBRITSPLWalletCancel) {
            this.fBRITSPLWalletCancel = fBRITSPLWalletCancel;
        }

        public int getFBRRemotePaymentCreate() {
            return fBRRemotePaymentCreate;
        }

        public void setFBRRemotePaymentCreate(int fBRRemotePaymentCreate) {
            this.fBRRemotePaymentCreate = fBRRemotePaymentCreate;
        }

        public int getFBRRemotePaymentModify() {
            return fBRRemotePaymentModify;
        }

        public void setFBRRemotePaymentModify(int fBRRemotePaymentModify) {
            this.fBRRemotePaymentModify = fBRRemotePaymentModify;
        }

        public int getFBRRemotePaymentCancel() {
            return fBRRemotePaymentCancel;
        }

        public void setFBRRemotePaymentCancel(int fBRRemotePaymentCancel) {
            this.fBRRemotePaymentCancel = fBRRemotePaymentCancel;
        }

        public int getIsSetDefaultPageSize() {
            return isSetDefaultPageSize;
        }

        public void setIsSetDefaultPageSize(int isSetDefaultPageSize) {
            this.isSetDefaultPageSize = isSetDefaultPageSize;
        }

        public int getGAAllowPickupChangeInModify() {
            return gAAllowPickupChangeInModify;
        }

        public void setGAAllowPickupChangeInModify(int gAAllowPickupChangeInModify) {
            this.gAAllowPickupChangeInModify = gAAllowPickupChangeInModify;
        }

        public int getIsBookingLog() {
            return isBookingLog;
        }

        public void setIsBookingLog(int isBookingLog) {
            this.isBookingLog = isBookingLog;
        }

        public int getGAIsShowFeedBack() {
            return gAIsShowFeedBack;
        }

        public void setGAIsShowFeedBack(int gAIsShowFeedBack) {
            this.gAIsShowFeedBack = gAIsShowFeedBack;
        }

        public int getGACalculateCommissionForPrepaidBranch() {
            return gACalculateCommissionForPrepaidBranch;
        }

        public void setGACalculateCommissionForPrepaidBranch(int gACalculateCommissionForPrepaidBranch) {
            this.gACalculateCommissionForPrepaidBranch = gACalculateCommissionForPrepaidBranch;
        }

    }

    public class ITSGetAllAgentByCompanyID {

        @SerializedName("AM_AgentID")
        @Expose
        private Integer aMAgentID;
        @SerializedName("AM_AgentName")
        @Expose
        private String aMAgentName;
        @SerializedName("AM_PhoneNo1")
        @Expose
        private String aMPhoneNo1;
        @SerializedName("CM_CityName")
        @Expose
        private String cMCityName;
        @SerializedName("CM_CityId")
        @Expose
        private Integer cMCityId;
        @SerializedName("CAM_IsOnline")
        @Expose
        private Integer cAMIsOnline;
        @SerializedName("CAM_AgentCode")
        @Expose
        private String cAMAgentCode;
        @SerializedName("IsAgentCredit")
        @Expose
        private Integer isAgentCredit;
        @SerializedName("IS_DisplayOnAgentList")
        @Expose
        private Integer iSDisplayOnAgentList;
        @SerializedName("CAM_OtherCharge")
        @Expose
        private Double cAMOtherCharge;
        @SerializedName("CAM_OtherChargeType")
        @Expose
        private Integer cAMOtherChargeType;

        public Integer getAMAgentID() {
            return aMAgentID;
        }

        public void setAMAgentID(Integer aMAgentID) {
            this.aMAgentID = aMAgentID;
        }

        public String getAMAgentName() {
            return aMAgentName;
        }

        public void setAMAgentName(String aMAgentName) {
            this.aMAgentName = aMAgentName;
        }

        public String getAMPhoneNo1() {
            return aMPhoneNo1;
        }

        public void setAMPhoneNo1(String aMPhoneNo1) {
            this.aMPhoneNo1 = aMPhoneNo1;
        }

        public String getCMCityName() {
            return cMCityName;
        }

        public void setCMCityName(String cMCityName) {
            this.cMCityName = cMCityName;
        }

        public Integer getCMCityId() {
            return cMCityId;
        }

        public void setCMCityId(Integer cMCityId) {
            this.cMCityId = cMCityId;
        }

        public Integer getCAMIsOnline() {
            return cAMIsOnline;
        }

        public void setCAMIsOnline(Integer cAMIsOnline) {
            this.cAMIsOnline = cAMIsOnline;
        }

        public String getCAMAgentCode() {
            return cAMAgentCode;
        }

        public void setCAMAgentCode(String cAMAgentCode) {
            this.cAMAgentCode = cAMAgentCode;
        }

        public Integer getIsAgentCredit() {
            return isAgentCredit;
        }

        public void setIsAgentCredit(Integer isAgentCredit) {
            this.isAgentCredit = isAgentCredit;
        }

        public Integer getISDisplayOnAgentList() {
            return iSDisplayOnAgentList;
        }

        public void setISDisplayOnAgentList(Integer iSDisplayOnAgentList) {
            this.iSDisplayOnAgentList = iSDisplayOnAgentList;
        }

        public Double getCAMOtherCharge() {
            return cAMOtherCharge;
        }

        public void setCAMOtherCharge(Double cAMOtherCharge) {
            this.cAMOtherCharge = cAMOtherCharge;
        }

        public Integer getCAMOtherChargeType() {
            return cAMOtherChargeType;
        }

        public void setCAMOtherChargeType(Integer cAMOtherChargeType) {
            this.cAMOtherChargeType = cAMOtherChargeType;
        }

    }

    public class ITSGetCompanyMarquee {

        @SerializedName("mm_message")
        @Expose
        private String mmMessage;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("JM_FromDateint")
        @Expose
        private Date jMFromDateint;
        @SerializedName("JM_ToDateint")
        @Expose
        private Date jMToDateint;

        public String getMmMessage() {
            return mmMessage;
        }

        public void setMmMessage(String mmMessage) {
            this.mmMessage = mmMessage;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public Date getJMFromDateint() {
            return jMFromDateint;
        }

        public void setJMFromDateint(Date jMFromDateint) {
            this.jMFromDateint = jMFromDateint;
        }

        public Date getJMToDateint() {
            return jMToDateint;
        }

        public void setJMToDateint(Date jMToDateint) {
            this.jMToDateint = jMToDateint;
        }

    }

    @Entity(tableName = "ITSGetITSMarquee")
    public static class ITSGetITSMarquee {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("MM_MarqueeMessage")
        @Expose
        private String mMMarqueeMessage;
        @SerializedName("MM_URL")
        @Expose
        private String mMURL;
        @SerializedName("ITS_SupportURL")
        @Expose
        private String iTSSupportURL;
        @SerializedName("ITS_Support")
        @Expose
        private String iTSSupport;

        public String getMMMarqueeMessage() {
            return mMMarqueeMessage;
        }

        public void setMMMarqueeMessage(String mMMarqueeMessage) {
            this.mMMarqueeMessage = mMMarqueeMessage;
        }

        public String getMMURL() {
            return mMURL;
        }

        public void setMMURL(String mMURL) {
            this.mMURL = mMURL;
        }

        public String getITSSupportURL() {
            return iTSSupportURL;
        }

        public void setITSSupportURL(String iTSSupportURL) {
            this.iTSSupportURL = iTSSupportURL;
        }

        public String getITSSupport() {
            return iTSSupport;
        }

        public void setITSSupport(String iTSSupport) {
            this.iTSSupport = iTSSupport;
        }

    }

    public class ITSBranchMasterSelectXML {

        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;
        @SerializedName("BM_BranchCode")
        @Expose
        private String bMBranchCode;
        @SerializedName("BM_UserName")
        @Expose
        private String bMUserName;
        @SerializedName("BM_Password")
        @Expose
        private String bMPassword;
        @SerializedName("BM_BranchManager")
        @Expose
        private String bMBranchManager;
        @SerializedName("BM_ContactNo")
        @Expose
        private String bMContactNo;
        @SerializedName("BM_ContactNo1")
        @Expose
        private String bMContactNo1;
        @SerializedName("CM_CountryID")
        @Expose
        private Integer cMCountryID;
        @SerializedName("SM_StateID")
        @Expose
        private Integer sMStateID;
        @SerializedName("CM_CityID")
        @Expose
        private Integer cMCityID;
        @SerializedName("BM_Address")
        @Expose
        private String bMAddress;
        @SerializedName("PM_PickupID")
        @Expose
        private Integer pMPickupID;
        @SerializedName("PM_PickupName")
        @Expose
        private String pMPickupName;
        @SerializedName("BM_PhoneNo")
        @Expose
        private String bMPhoneNo;
        @SerializedName("BM_CommissionAmt")
        @Expose
        private String bMCommissionAmt;
        @SerializedName("BM_CommissionPer")
        @Expose
        private Double bMCommissionPer;
        @SerializedName("BM_NoOfUsers")
        @Expose
        private Integer bMNoOfUsers;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("BM_CreatedDate")
        @Expose
        private String bMCreatedDate;
        @SerializedName("BM_IsActive")
        @Expose
        private Integer bMIsActive;
        @SerializedName("BM_AutoDeletePhoneHour")
        @Expose
        private Integer bMAutoDeletePhoneHour;
        @SerializedName("BM_AutoDeletePhoneMin")
        @Expose
        private Integer bMAutoDeletePhoneMin;
        @SerializedName("BM_IsPrepaid")
        @Expose
        private Integer bMIsPrepaid;
        @SerializedName("BM_AutoAvailability")
        @Expose
        private Integer bMAutoAvailability;

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

        public String getBMBranchCode() {
            return bMBranchCode;
        }

        public void setBMBranchCode(String bMBranchCode) {
            this.bMBranchCode = bMBranchCode;
        }

        public String getBMUserName() {
            return bMUserName;
        }

        public void setBMUserName(String bMUserName) {
            this.bMUserName = bMUserName;
        }

        public String getBMPassword() {
            return bMPassword;
        }

        public void setBMPassword(String bMPassword) {
            this.bMPassword = bMPassword;
        }

        public String getBMBranchManager() {
            return bMBranchManager;
        }

        public void setBMBranchManager(String bMBranchManager) {
            this.bMBranchManager = bMBranchManager;
        }

        public String getBMContactNo() {
            return bMContactNo;
        }

        public void setBMContactNo(String bMContactNo) {
            this.bMContactNo = bMContactNo;
        }

        public String getBMContactNo1() {
            return bMContactNo1;
        }

        public void setBMContactNo1(String bMContactNo1) {
            this.bMContactNo1 = bMContactNo1;
        }

        public Integer getCMCountryID() {
            return cMCountryID;
        }

        public void setCMCountryID(Integer cMCountryID) {
            this.cMCountryID = cMCountryID;
        }

        public Integer getSMStateID() {
            return sMStateID;
        }

        public void setSMStateID(Integer sMStateID) {
            this.sMStateID = sMStateID;
        }

        public Integer getCMCityID() {
            return cMCityID;
        }

        public void setCMCityID(Integer cMCityID) {
            this.cMCityID = cMCityID;
        }

        public String getBMAddress() {
            return bMAddress;
        }

        public void setBMAddress(String bMAddress) {
            this.bMAddress = bMAddress;
        }

        public Integer getPMPickupID() {
            return pMPickupID;
        }

        public void setPMPickupID(Integer pMPickupID) {
            this.pMPickupID = pMPickupID;
        }

        public String getPMPickupName() {
            return pMPickupName;
        }

        public void setPMPickupName(String pMPickupName) {
            this.pMPickupName = pMPickupName;
        }

        public String getBMPhoneNo() {
            return bMPhoneNo;
        }

        public void setBMPhoneNo(String bMPhoneNo) {
            this.bMPhoneNo = bMPhoneNo;
        }

        public String getBMCommissionAmt() {
            return bMCommissionAmt;
        }

        public void setBMCommissionAmt(String bMCommissionAmt) {
            this.bMCommissionAmt = bMCommissionAmt;
        }

        public Double getBMCommissionPer() {
            return bMCommissionPer;
        }

        public void setBMCommissionPer(Double bMCommissionPer) {
            this.bMCommissionPer = bMCommissionPer;
        }

        public Integer getBMNoOfUsers() {
            return bMNoOfUsers;
        }

        public void setBMNoOfUsers(Integer bMNoOfUsers) {
            this.bMNoOfUsers = bMNoOfUsers;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getBMCreatedDate() {
            return bMCreatedDate;
        }

        public void setBMCreatedDate(String bMCreatedDate) {
            this.bMCreatedDate = bMCreatedDate;
        }

        public Integer getBMIsActive() {
            return bMIsActive;
        }

        public void setBMIsActive(Integer bMIsActive) {
            this.bMIsActive = bMIsActive;
        }

        public Integer getBMAutoDeletePhoneHour() {
            return bMAutoDeletePhoneHour;
        }

        public void setBMAutoDeletePhoneHour(Integer bMAutoDeletePhoneHour) {
            this.bMAutoDeletePhoneHour = bMAutoDeletePhoneHour;
        }

        public Integer getBMAutoDeletePhoneMin() {
            return bMAutoDeletePhoneMin;
        }

        public void setBMAutoDeletePhoneMin(Integer bMAutoDeletePhoneMin) {
            this.bMAutoDeletePhoneMin = bMAutoDeletePhoneMin;
        }

        public Integer getBMIsPrepaid() {
            return bMIsPrepaid;
        }

        public void setBMIsPrepaid(Integer bMIsPrepaid) {
            this.bMIsPrepaid = bMIsPrepaid;
        }

        public Integer getBMAutoAvailability() {
            return bMAutoAvailability;
        }

        public void setBMAutoAvailability(Integer bMAutoAvailability) {
            this.bMAutoAvailability = bMAutoAvailability;
        }

    }

    public class ITSGetGuestTypeByCompanyID {

        @SerializedName("GTM_GuestTypeID")
        @Expose
        private Integer gTMGuestTypeID;
        @SerializedName("GTM_GuestName")
        @Expose
        private String gTMGuestName;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;

        public Integer getGTMGuestTypeID() {
            return gTMGuestTypeID;
        }

        public void setGTMGuestTypeID(Integer gTMGuestTypeID) {
            this.gTMGuestTypeID = gTMGuestTypeID;
        }

        public String getGTMGuestName() {
            return gTMGuestName;
        }

        public void setGTMGuestName(String gTMGuestName) {
            this.gTMGuestName = gTMGuestName;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

    }

    public class ITSBusChartArrangement {

        @SerializedName("CAM_ID")
        @Expose
        private Integer cAMID;
        @SerializedName("BAM_ID")
        @Expose
        private Integer bAMID;
        @SerializedName("BAM_Rows")
        @Expose
        private Integer bAMRows;
        @SerializedName("BAM_Columns")
        @Expose
        private Integer bAMColumns;
        @SerializedName("BAM_Arrangement")
        @Expose
        private String bAMArrangement;
        @SerializedName("BAM_IsActive")
        @Expose
        private Integer bAMIsActive;
        @SerializedName("Detail_ID")
        @Expose
        private Integer detailID;
        @SerializedName("BAD_Row")
        @Expose
        private Integer bADRow;
        @SerializedName("BAD_Column")
        @Expose
        private Integer bADColumn;
        @SerializedName("BAD_Cell")
        @Expose
        private Integer bADCell;
        @SerializedName("BAD_SeatNo")
        @Expose
        private String bADSeatNo;
        @SerializedName("BAD_BusType")
        @Expose
        private Integer bADBusType;
        @SerializedName("BAD_SeatType")
        @Expose
        private Integer bADSeatType;
        @SerializedName("BAD_BlockType")
        @Expose
        private Integer bADBlockType;
        @SerializedName("BAD_UDType")
        @Expose
        private Integer bADUDType;
        @SerializedName("BAD_Rowspan")
        @Expose
        private Integer bADRowspan;
        @SerializedName("BAD_Colspan")
        @Expose
        private Integer bADColspan;
        @SerializedName("BAD_Display")
        @Expose
        private String bADDisplay;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("CAD_FareType")
        @Expose
        private String cADFareType;
        @SerializedName("OS_IsSeatWiseFare")
        @Expose
        private Integer oSIsSeatWiseFare;

        public Integer getCAMID() {
            return cAMID;
        }

        public void setCAMID(Integer cAMID) {
            this.cAMID = cAMID;
        }

        public Integer getBAMID() {
            return bAMID;
        }

        public void setBAMID(Integer bAMID) {
            this.bAMID = bAMID;
        }

        public Integer getBAMRows() {
            return bAMRows;
        }

        public void setBAMRows(Integer bAMRows) {
            this.bAMRows = bAMRows;
        }

        public Integer getBAMColumns() {
            return bAMColumns;
        }

        public void setBAMColumns(Integer bAMColumns) {
            this.bAMColumns = bAMColumns;
        }

        public String getBAMArrangement() {
            return bAMArrangement;
        }

        public void setBAMArrangement(String bAMArrangement) {
            this.bAMArrangement = bAMArrangement;
        }

        public Integer getBAMIsActive() {
            return bAMIsActive;
        }

        public void setBAMIsActive(Integer bAMIsActive) {
            this.bAMIsActive = bAMIsActive;
        }

        public Integer getDetailID() {
            return detailID;
        }

        public void setDetailID(Integer detailID) {
            this.detailID = detailID;
        }

        public Integer getBADRow() {
            return bADRow;
        }

        public void setBADRow(Integer bADRow) {
            this.bADRow = bADRow;
        }

        public Integer getBADColumn() {
            return bADColumn;
        }

        public void setBADColumn(Integer bADColumn) {
            this.bADColumn = bADColumn;
        }

        public Integer getBADCell() {
            return bADCell;
        }

        public void setBADCell(Integer bADCell) {
            this.bADCell = bADCell;
        }

        public String getBADSeatNo() {
            return bADSeatNo;
        }

        public void setBADSeatNo(String bADSeatNo) {
            this.bADSeatNo = bADSeatNo;
        }

        public Integer getBADBusType() {
            return bADBusType;
        }

        public void setBADBusType(Integer bADBusType) {
            this.bADBusType = bADBusType;
        }

        public Integer getBADSeatType() {
            return bADSeatType;
        }

        public void setBADSeatType(Integer bADSeatType) {
            this.bADSeatType = bADSeatType;
        }

        public Integer getBADBlockType() {
            return bADBlockType;
        }

        public void setBADBlockType(Integer bADBlockType) {
            this.bADBlockType = bADBlockType;
        }

        public Integer getBADUDType() {
            return bADUDType;
        }

        public void setBADUDType(Integer bADUDType) {
            this.bADUDType = bADUDType;
        }

        public Integer getBADRowspan() {
            return bADRowspan;
        }

        public void setBADRowspan(Integer bADRowspan) {
            this.bADRowspan = bADRowspan;
        }

        public Integer getBADColspan() {
            return bADColspan;
        }

        public void setBADColspan(Integer bADColspan) {
            this.bADColspan = bADColspan;
        }

        public String getBADDisplay() {
            return bADDisplay;
        }

        public void setBADDisplay(String bADDisplay) {
            this.bADDisplay = bADDisplay;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getCADFareType() {
            return cADFareType;
        }

        public void setCADFareType(String cADFareType) {
            this.cADFareType = cADFareType;
        }

        public Integer getOSIsSeatWiseFare() {
            return oSIsSeatWiseFare;
        }

        public void setOSIsSeatWiseFare(Integer oSIsSeatWiseFare) {
            this.oSIsSeatWiseFare = oSIsSeatWiseFare;
        }

    }

    public class ITSBusTypeColorCode {

        @SerializedName("BTM_BusTypeID")
        @Expose
        private Integer bTMBusTypeID;
        @SerializedName("BTM_RouteNameDisplayColor")
        @Expose
        private String bTMRouteNameDisplayColor;

        public Integer getBTMBusTypeID() {
            return bTMBusTypeID;
        }

        public void setBTMBusTypeID(Integer bTMBusTypeID) {
            this.bTMBusTypeID = bTMBusTypeID;
        }

        public String getBTMRouteNameDisplayColor() {
            return bTMRouteNameDisplayColor;
        }

        public void setBTMRouteNameDisplayColor(String bTMRouteNameDisplayColor) {
            this.bTMRouteNameDisplayColor = bTMRouteNameDisplayColor;
        }

    }

    @Entity(tableName = "ITSCancelremark")
    public static class ITSCancelremark {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("RM_RemarkID")
        @Expose
        private Integer rMRemarkID;
        @SerializedName("RM_RemarkType")
        @Expose
        private String rMRemarkType;
        @SerializedName("RM_RemarkName")
        @Expose
        private String rMRemarkName;

        public Integer getRMRemarkID() {
            return rMRemarkID;
        }

        public void setRMRemarkID(Integer rMRemarkID) {
            this.rMRemarkID = rMRemarkID;
        }

        public String getRMRemarkType() {
            return rMRemarkType;
        }

        public void setRMRemarkType(String rMRemarkType) {
            this.rMRemarkType = rMRemarkType;
        }

        public String getRMRemarkName() {
            return rMRemarkName;
        }

        public void setRMRemarkName(String rMRemarkName) {
            this.rMRemarkName = rMRemarkName;
        }

    }

    @Entity(tableName = "ITSGetBookingTypeByCompanyIDXML")
    public static class ITSGetBookingTypeByCompanyIDXML {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("booking_type_id")
        @Expose
        private int bookingTypeId;
        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("default_name")
        @Expose
        private String defaultName;
        @SerializedName("IsPrepaid")
        @Expose
        private Integer isPrepaid;
        @SerializedName("OS_EmailConfirm")
        @Expose
        private Integer oSEmailConfirm;
        @SerializedName("OS_EmailPhone")
        @Expose
        private Integer oSEmailPhone;
        @SerializedName("OS_EmailAgent")
        @Expose
        private Integer oSEmailAgent;
        @SerializedName("OS_EmailBranch")
        @Expose
        private Integer oSEmailBranch;
        @SerializedName("OS_EmailGuest")
        @Expose
        private Integer oSEmailGuest;
        @SerializedName("OS_EmailBankCard")
        @Expose
        private Integer oSEmailBankCard;
        @SerializedName("OS_EmailCompanyCard")
        @Expose
        private Integer oSEmailCompanyCard;
        @SerializedName("OS_EmailWaiting")
        @Expose
        private Integer oSEmailWaiting;
        @SerializedName("OS_EmailAgentQuata")
        @Expose
        private Integer oSEmailAgentQuata;
        @SerializedName("OS_EmailOpen")
        @Expose
        private Integer oSEmailOpen;
        @SerializedName("OS_SMSConfirm")
        @Expose
        private Integer oSSMSConfirm;
        @SerializedName("OS_SMSPhone")
        @Expose
        private Integer oSSMSPhone;
        @SerializedName("OS_SMSAgent")
        @Expose
        private Integer oSSMSAgent;
        @SerializedName("OS_SMSBranch")
        @Expose
        private Integer oSSMSBranch;
        @SerializedName("OS_SMSGuest")
        @Expose
        private Integer oSSMSGuest;
        @SerializedName("OS_SMSBankCard")
        @Expose
        private Integer oSSMSBankCard;
        @SerializedName("OS_SMSCompanyCard")
        @Expose
        private Integer oSSMSCompanyCard;
        @SerializedName("OS_SMSWaiting")
        @Expose
        private Integer oSSMSWaiting;
        @SerializedName("OS_SMSAgentQuata")
        @Expose
        private Integer oSSMSAgentQuata;
        @SerializedName("OS_SMSOpen")
        @Expose
        private Integer oSSMSOpen;
        @SerializedName("OS_PDFConfirm")
        @Expose
        private Integer oSPDFConfirm;
        @SerializedName("OS_PDFPhone")
        @Expose
        private Integer oSPDFPhone;
        @SerializedName("OS_PDFAgent")
        @Expose
        private Integer oSPDFAgent;
        @SerializedName("OS_PDFOnlineAgent")
        @Expose
        private Integer oSPDFOnlineAgent;
        @SerializedName("OS_PDFAPIAgent")
        @Expose
        private Integer oSPDFAPIAgent;
        @SerializedName("OS_PDFB2CAgent")
        @Expose
        private Integer oSPDFB2CAgent;
        @SerializedName("OS_PDFBranch")
        @Expose
        private Integer oSPDFBranch;
        @SerializedName("OS_PDFGuest")
        @Expose
        private Integer oSPDFGuest;
        @SerializedName("OS_PDFBankCard")
        @Expose
        private Integer oSPDFBankCard;
        @SerializedName("OS_PDFCompanyCard")
        @Expose
        private Integer oSPDFCompanyCard;
        @SerializedName("OS_PDFAgentQuata")
        @Expose
        private Integer oSPDFAgentQuata;

        public int getBookingTypeId() {
            return bookingTypeId;
        }

        public void setBookingTypeId(int bookingTypeId) {
            this.bookingTypeId = bookingTypeId;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getDefaultName() {
            return defaultName;
        }

        public void setDefaultName(String defaultName) {
            this.defaultName = defaultName;
        }

        public Integer getIsPrepaid() {
            return isPrepaid;
        }

        public void setIsPrepaid(Integer isPrepaid) {
            this.isPrepaid = isPrepaid;
        }

        public Integer getOSEmailConfirm() {
            return oSEmailConfirm;
        }

        public void setOSEmailConfirm(Integer oSEmailConfirm) {
            this.oSEmailConfirm = oSEmailConfirm;
        }

        public Integer getOSEmailPhone() {
            return oSEmailPhone;
        }

        public void setOSEmailPhone(Integer oSEmailPhone) {
            this.oSEmailPhone = oSEmailPhone;
        }

        public Integer getOSEmailAgent() {
            return oSEmailAgent;
        }

        public void setOSEmailAgent(Integer oSEmailAgent) {
            this.oSEmailAgent = oSEmailAgent;
        }

        public Integer getOSEmailBranch() {
            return oSEmailBranch;
        }

        public void setOSEmailBranch(Integer oSEmailBranch) {
            this.oSEmailBranch = oSEmailBranch;
        }

        public Integer getOSEmailGuest() {
            return oSEmailGuest;
        }

        public void setOSEmailGuest(Integer oSEmailGuest) {
            this.oSEmailGuest = oSEmailGuest;
        }

        public Integer getOSEmailBankCard() {
            return oSEmailBankCard;
        }

        public void setOSEmailBankCard(Integer oSEmailBankCard) {
            this.oSEmailBankCard = oSEmailBankCard;
        }

        public Integer getOSEmailCompanyCard() {
            return oSEmailCompanyCard;
        }

        public void setOSEmailCompanyCard(Integer oSEmailCompanyCard) {
            this.oSEmailCompanyCard = oSEmailCompanyCard;
        }

        public Integer getOSEmailWaiting() {
            return oSEmailWaiting;
        }

        public void setOSEmailWaiting(Integer oSEmailWaiting) {
            this.oSEmailWaiting = oSEmailWaiting;
        }

        public Integer getOSEmailAgentQuata() {
            return oSEmailAgentQuata;
        }

        public void setOSEmailAgentQuata(Integer oSEmailAgentQuata) {
            this.oSEmailAgentQuata = oSEmailAgentQuata;
        }

        public Integer getOSEmailOpen() {
            return oSEmailOpen;
        }

        public void setOSEmailOpen(Integer oSEmailOpen) {
            this.oSEmailOpen = oSEmailOpen;
        }

        public Integer getOSSMSConfirm() {
            return oSSMSConfirm;
        }

        public void setOSSMSConfirm(Integer oSSMSConfirm) {
            this.oSSMSConfirm = oSSMSConfirm;
        }

        public Integer getOSSMSPhone() {
            return oSSMSPhone;
        }

        public void setOSSMSPhone(Integer oSSMSPhone) {
            this.oSSMSPhone = oSSMSPhone;
        }

        public Integer getOSSMSAgent() {
            return oSSMSAgent;
        }

        public void setOSSMSAgent(Integer oSSMSAgent) {
            this.oSSMSAgent = oSSMSAgent;
        }

        public Integer getOSSMSBranch() {
            return oSSMSBranch;
        }

        public void setOSSMSBranch(Integer oSSMSBranch) {
            this.oSSMSBranch = oSSMSBranch;
        }

        public Integer getOSSMSGuest() {
            return oSSMSGuest;
        }

        public void setOSSMSGuest(Integer oSSMSGuest) {
            this.oSSMSGuest = oSSMSGuest;
        }

        public Integer getOSSMSBankCard() {
            return oSSMSBankCard;
        }

        public void setOSSMSBankCard(Integer oSSMSBankCard) {
            this.oSSMSBankCard = oSSMSBankCard;
        }

        public Integer getOSSMSCompanyCard() {
            return oSSMSCompanyCard;
        }

        public void setOSSMSCompanyCard(Integer oSSMSCompanyCard) {
            this.oSSMSCompanyCard = oSSMSCompanyCard;
        }

        public Integer getOSSMSWaiting() {
            return oSSMSWaiting;
        }

        public void setOSSMSWaiting(Integer oSSMSWaiting) {
            this.oSSMSWaiting = oSSMSWaiting;
        }

        public Integer getOSSMSAgentQuata() {
            return oSSMSAgentQuata;
        }

        public void setOSSMSAgentQuata(Integer oSSMSAgentQuata) {
            this.oSSMSAgentQuata = oSSMSAgentQuata;
        }

        public Integer getOSSMSOpen() {
            return oSSMSOpen;
        }

        public void setOSSMSOpen(Integer oSSMSOpen) {
            this.oSSMSOpen = oSSMSOpen;
        }

        public Integer getOSPDFConfirm() {
            return oSPDFConfirm;
        }

        public void setOSPDFConfirm(Integer oSPDFConfirm) {
            this.oSPDFConfirm = oSPDFConfirm;
        }

        public Integer getOSPDFPhone() {
            return oSPDFPhone;
        }

        public void setOSPDFPhone(Integer oSPDFPhone) {
            this.oSPDFPhone = oSPDFPhone;
        }

        public Integer getOSPDFAgent() {
            return oSPDFAgent;
        }

        public void setOSPDFAgent(Integer oSPDFAgent) {
            this.oSPDFAgent = oSPDFAgent;
        }

        public Integer getOSPDFOnlineAgent() {
            return oSPDFOnlineAgent;
        }

        public void setOSPDFOnlineAgent(Integer oSPDFOnlineAgent) {
            this.oSPDFOnlineAgent = oSPDFOnlineAgent;
        }

        public Integer getOSPDFAPIAgent() {
            return oSPDFAPIAgent;
        }

        public void setOSPDFAPIAgent(Integer oSPDFAPIAgent) {
            this.oSPDFAPIAgent = oSPDFAPIAgent;
        }

        public Integer getOSPDFB2CAgent() {
            return oSPDFB2CAgent;
        }

        public void setOSPDFB2CAgent(Integer oSPDFB2CAgent) {
            this.oSPDFB2CAgent = oSPDFB2CAgent;
        }

        public Integer getOSPDFBranch() {
            return oSPDFBranch;
        }

        public void setOSPDFBranch(Integer oSPDFBranch) {
            this.oSPDFBranch = oSPDFBranch;
        }

        public Integer getOSPDFGuest() {
            return oSPDFGuest;
        }

        public void setOSPDFGuest(Integer oSPDFGuest) {
            this.oSPDFGuest = oSPDFGuest;
        }

        public Integer getOSPDFBankCard() {
            return oSPDFBankCard;
        }

        public void setOSPDFBankCard(Integer oSPDFBankCard) {
            this.oSPDFBankCard = oSPDFBankCard;
        }

        public Integer getOSPDFCompanyCard() {
            return oSPDFCompanyCard;
        }

        public void setOSPDFCompanyCard(Integer oSPDFCompanyCard) {
            this.oSPDFCompanyCard = oSPDFCompanyCard;
        }

        public Integer getOSPDFAgentQuata() {
            return oSPDFAgentQuata;
        }

        public void setOSPDFAgentQuata(Integer oSPDFAgentQuata) {
            this.oSPDFAgentQuata = oSPDFAgentQuata;
        }

    }

    @Entity(tableName = "ITSGetOtherCompanyBookingRightsExe")
    public static class ITSGetOtherCompanyBookingRightsExe {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        @SerializedName("From_CM_CompanyID")
        @Expose
        private Integer fromCMCompanyID;
        @SerializedName("To_CM_CompanyID")
        @Expose
        private Integer toCMCompanyID;
        @SerializedName("OCBR_ConfirmCreate")
        @Expose
        private Integer oCBRConfirmCreate;
        @SerializedName("OCBR_ConfirmModify")
        @Expose
        private Integer oCBRConfirmModify;
        @SerializedName("OCBR_ConfirmCancel")
        @Expose
        private Integer oCBRConfirmCancel;
        @SerializedName("OCBR_PhoneCreate")
        @Expose
        private Integer oCBRPhoneCreate;
        @SerializedName("OCBR_PhoneModify")
        @Expose
        private Integer oCBRPhoneModify;
        @SerializedName("OCBR_PhoneCancel")
        @Expose
        private Integer oCBRPhoneCancel;
        @SerializedName("OCBR_AgentCreate")
        @Expose
        private Integer oCBRAgentCreate;
        @SerializedName("OCBR_AgentModify")
        @Expose
        private Integer oCBRAgentModify;
        @SerializedName("OCBR_AgentCancel")
        @Expose
        private Integer oCBRAgentCancel;
        @SerializedName("OCBR_BranchCreate")
        @Expose
        private Integer oCBRBranchCreate;
        @SerializedName("OCBR_BranchModify")
        @Expose
        private Integer oCBRBranchModify;
        @SerializedName("OCBR_BranchCancel")
        @Expose
        private Integer oCBRBranchCancel;
        @SerializedName("OCBR_GuestCreate")
        @Expose
        private Integer oCBRGuestCreate;
        @SerializedName("OCBR_GuestModify")
        @Expose
        private Integer oCBRGuestModify;
        @SerializedName("OCBR_GuestCancel")
        @Expose
        private Integer oCBRGuestCancel;
        @SerializedName("OCBR_BankCardCreate")
        @Expose
        private Integer oCBRBankCardCreate;
        @SerializedName("OCBR_BankCardModify")
        @Expose
        private Integer oCBRBankCardModify;
        @SerializedName("OCBR_BankCardCancel")
        @Expose
        private Integer oCBRBankCardCancel;
        @SerializedName("OCBR_CompanyCardCreate")
        @Expose
        private Integer oCBRCompanyCardCreate;
        @SerializedName("OCBR_CompanyCardModify")
        @Expose
        private Integer oCBRCompanyCardModify;
        @SerializedName("OCBR_CompanyCardCancel")
        @Expose
        private Integer oCBRCompanyCardCancel;
        @SerializedName("OCBR_WaitingCreate")
        @Expose
        private Integer oCBRWaitingCreate;
        @SerializedName("OCBR_WaitingModify")
        @Expose
        private Integer oCBRWaitingModify;
        @SerializedName("OCBR_WaitingCancel")
        @Expose
        private Integer oCBRWaitingCancel;
        @SerializedName("OCBR_AgentQuotaCreate")
        @Expose
        private Integer oCBRAgentQuotaCreate;
        @SerializedName("OCBR_AgentQuotaModify")
        @Expose
        private Integer oCBRAgentQuotaModify;
        @SerializedName("OCBR_AgentQuotaCancel")
        @Expose
        private Integer oCBRAgentQuotaCancel;
        @SerializedName("OCBR_OpenCreate")
        @Expose
        private Integer oCBROpenCreate;
        @SerializedName("OCBR_OpenModify")
        @Expose
        private Integer oCBROpenModify;
        @SerializedName("OCBR_OpenCancel")
        @Expose
        private Integer oCBROpenCancel;
        @SerializedName("OCBR_IsTDS")
        @Expose
        private Integer oCBRIsTDS;
        @SerializedName("OCBR_TDS")
        @Expose
        private Double oCBRTDS;
        @SerializedName("OCBR_Confirm")
        @Expose
        private Integer oCBRConfirm;
        @SerializedName("OCBR_Phone")
        @Expose
        private Integer oCBRPhone;
        @SerializedName("OCBR_Agent")
        @Expose
        private Integer oCBRAgent;
        @SerializedName("OCBR_Branch")
        @Expose
        private Integer oCBRBranch;
        @SerializedName("OCBR_Guest")
        @Expose
        private Integer oCBRGuest;
        @SerializedName("OCBR_Waiting")
        @Expose
        private Integer oCBRWaiting;
        @SerializedName("OCBR_BankCard")
        @Expose
        private Integer oCBRBankCard;
        @SerializedName("OCBR_CompanyCard")
        @Expose
        private Integer oCBRCompanyCard;
        @SerializedName("OCBR_AgentQuota")
        @Expose
        private Integer oCBRAgentQuota;
        @SerializedName("OCBR_Open")
        @Expose
        private Integer oCBROpen;
        @SerializedName("CM_IsACServiceTax")
        @Expose
        private Integer cMIsACServiceTax;
        @SerializedName("CM_IsNonACServiceTax")
        @Expose
        private Integer cMIsNonACServiceTax;
        @SerializedName("CM_ACServiceTax")
        @Expose
        private Double cMACServiceTax;
        @SerializedName("CM_NonACServiceTax")
        @Expose
        private Double cMNonACServiceTax;
        @SerializedName("CM_ACFareIncludeTax")
        @Expose
        private Integer cMACFareIncludeTax;
        @SerializedName("CM_NonACFareIncludeTax")
        @Expose
        private Integer cMNonACFareIncludeTax;
        @SerializedName("CM_IsSTOffline")
        @Expose
        private Integer cMIsSTOffline;
        @SerializedName("CM_IsSTLogin")
        @Expose
        private Integer cMIsSTLogin;
        @SerializedName("CM_IsSTAPI")
        @Expose
        private Integer cMIsSTAPI;
        @SerializedName("CM_IsSTB2C")
        @Expose
        private Integer cMIsSTB2C;
        @SerializedName("CM_IsSTBranch")
        @Expose
        private Integer cMIsSTBranch;
        @SerializedName("OCBR_AllowModifyPhoneOfRouteCompany")
        @Expose
        private Integer oCBRAllowModifyPhoneOfRouteCompany;
        @SerializedName("CM_ServiceTaxRoundUp")
        @Expose
        private Double cMServiceTaxRoundUp;
        @SerializedName("CM_ACPackageServiceTax")
        @Expose
        private Double cMACPackageServiceTax;
        @SerializedName("CM_NonACPackageServiceTax")
        @Expose
        private Double cMNonACPackageServiceTax;
        @SerializedName("CM_ACOwnScheduleServiceTax")
        @Expose
        private Double cMACOwnScheduleServiceTax;
        @SerializedName("CM_NonACOwnScheduleServiceTax")
        @Expose
        private Double cMNonACOwnScheduleServiceTax;
        @SerializedName("OCBR_AllowCouponCode")
        @Expose
        private Integer oCBRAllowCouponCode;
        @SerializedName("OCBR_AllowFareChange")
        @Expose
        private Integer oCBRAllowFareChange;
        @SerializedName("CM_ACOtherOwnScheduleServiceTax")
        @Expose
        private Double cMACOtherOwnScheduleServiceTax;
        @SerializedName("CM_NonACOtherOwnScheduleServiceTax")
        @Expose
        private Double cMNonACOtherOwnScheduleServiceTax;
        @SerializedName("OCBR_ModifyonOtherRoute")
        @Expose
        private Integer oCBRModifyonOtherRoute;
        @SerializedName("OCBR_AllowNonReported")
        @Expose
        private Integer oCBRAllowNonReported;
        @SerializedName("OCBR_StopBooking")
        @Expose
        private Integer oCBRStopBooking;
        @SerializedName("OCBR_OnlineAgentCreate")
        @Expose
        private Integer oCBROnlineAgentCreate;
        @SerializedName("OCBR_OnlineAgentModify")
        @Expose
        private Integer oCBROnlineAgentModify;
        @SerializedName("OCBR_OnlineAgentCancel")
        @Expose
        private Integer oCBROnlineAgentCancel;
        @SerializedName("OCBR_APIAgentCreate")
        @Expose
        private Integer oCBRAPIAgentCreate;
        @SerializedName("OCBR_APIAgentModify")
        @Expose
        private Integer oCBRAPIAgentModify;
        @SerializedName("OCBR_APIAgentCancel")
        @Expose
        private Integer oCBRAPIAgentCancel;
        @SerializedName("OCBR_B2CAgentCreate")
        @Expose
        private Integer oCBRB2CAgentCreate;
        @SerializedName("OCBR_B2CAgentModify")
        @Expose
        private Integer oCBRB2CAgentModify;
        @SerializedName("OCBR_B2CAgentCancel")
        @Expose
        private Integer oCBRB2CAgentCancel;
        @SerializedName("OCBR_AllowReprint")
        @Expose
        private Integer oCBRAllowReprint;
        @SerializedName("OCBR_AllowFromToCity")
        @Expose
        private Integer oCBRAllowFromToCity;
        @SerializedName("OS_AllowModifyCharges")
        @Expose
        private Integer oSAllowModifyCharges;
        @SerializedName("OCBR_ChartSMS")
        @Expose
        private Integer oCBRChartSMS;
        @SerializedName("OCBR_ReSendBookingSMS")
        @Expose
        private Integer oCBRReSendBookingSMS;
        @SerializedName("OCBR_Wallet")
        @Expose
        private Integer oCBRWallet;
        @SerializedName("OCBR_WalletCreate")
        @Expose
        private Integer oCBRWalletCreate;
        @SerializedName("OCBR_WalletModify")
        @Expose
        private Integer oCBRWalletModify;
        @SerializedName("OCBR_WalletCancel")
        @Expose
        private Integer oCBRWalletCancel;
        @SerializedName("OCBR_SentOwnBookingSMS")
        @Expose
        private Integer oCBRSentOwnBookingSMS;
        @SerializedName("CM_IsSTBankCard")
        @Expose
        private Integer cMIsSTBankCard;
        @SerializedName("CM_IsSTITSPLWallet")
        @Expose
        private Integer cMIsSTITSPLWallet;
        @SerializedName("CM_IsSTRemotePayment")
        @Expose
        private Integer cMIsSTRemotePayment;
        @SerializedName("OCBR_RemotePayment")
        @Expose
        private Integer oCBRRemotePayment;
        @SerializedName("OCBR_RemotePaymentCreate")
        @Expose
        private Integer oCBRRemotePaymentCreate;
        @SerializedName("OCBR_RemotePaymentModify")
        @Expose
        private Integer oCBRRemotePaymentModify;
        @SerializedName("OCBR_RemotePaymentCancel")
        @Expose
        private Integer oCBRRemotePaymentCancel;
        @SerializedName("OS_IsAskIDProof")
        @Expose
        private Integer oSIsAskIDProof;
        @SerializedName("OS_IsIDProofMandatory")
        @Expose
        private Integer oSIsIDProofMandatory;
        @SerializedName("OS_IsCrossStateIDProofMandatory")
        @Expose
        private Integer oSIsCrossStateIDProofMandatory;
        @SerializedName("OCBR_DisplayBookingTagToolTip")
        @Expose
        private Integer oCBRDisplayBookingTagToolTip;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getFromCMCompanyID() {
            return fromCMCompanyID;
        }

        public void setFromCMCompanyID(Integer fromCMCompanyID) {
            this.fromCMCompanyID = fromCMCompanyID;
        }

        public Integer getToCMCompanyID() {
            return toCMCompanyID;
        }

        public void setToCMCompanyID(Integer toCMCompanyID) {
            this.toCMCompanyID = toCMCompanyID;
        }

        public Integer getOCBRConfirmCreate() {
            return oCBRConfirmCreate;
        }

        public void setOCBRConfirmCreate(Integer oCBRConfirmCreate) {
            this.oCBRConfirmCreate = oCBRConfirmCreate;
        }

        public Integer getOCBRConfirmModify() {
            return oCBRConfirmModify;
        }

        public void setOCBRConfirmModify(Integer oCBRConfirmModify) {
            this.oCBRConfirmModify = oCBRConfirmModify;
        }

        public Integer getOCBRConfirmCancel() {
            return oCBRConfirmCancel;
        }

        public void setOCBRConfirmCancel(Integer oCBRConfirmCancel) {
            this.oCBRConfirmCancel = oCBRConfirmCancel;
        }

        public Integer getOCBRPhoneCreate() {
            return oCBRPhoneCreate;
        }

        public void setOCBRPhoneCreate(Integer oCBRPhoneCreate) {
            this.oCBRPhoneCreate = oCBRPhoneCreate;
        }

        public Integer getOCBRPhoneModify() {
            return oCBRPhoneModify;
        }

        public void setOCBRPhoneModify(Integer oCBRPhoneModify) {
            this.oCBRPhoneModify = oCBRPhoneModify;
        }

        public Integer getOCBRPhoneCancel() {
            return oCBRPhoneCancel;
        }

        public void setOCBRPhoneCancel(Integer oCBRPhoneCancel) {
            this.oCBRPhoneCancel = oCBRPhoneCancel;
        }

        public Integer getOCBRAgentCreate() {
            return oCBRAgentCreate;
        }

        public void setOCBRAgentCreate(Integer oCBRAgentCreate) {
            this.oCBRAgentCreate = oCBRAgentCreate;
        }

        public Integer getOCBRAgentModify() {
            return oCBRAgentModify;
        }

        public void setOCBRAgentModify(Integer oCBRAgentModify) {
            this.oCBRAgentModify = oCBRAgentModify;
        }

        public Integer getOCBRAgentCancel() {
            return oCBRAgentCancel;
        }

        public void setOCBRAgentCancel(Integer oCBRAgentCancel) {
            this.oCBRAgentCancel = oCBRAgentCancel;
        }

        public Integer getOCBRBranchCreate() {
            return oCBRBranchCreate;
        }

        public void setOCBRBranchCreate(Integer oCBRBranchCreate) {
            this.oCBRBranchCreate = oCBRBranchCreate;
        }

        public Integer getOCBRBranchModify() {
            return oCBRBranchModify;
        }

        public void setOCBRBranchModify(Integer oCBRBranchModify) {
            this.oCBRBranchModify = oCBRBranchModify;
        }

        public Integer getOCBRBranchCancel() {
            return oCBRBranchCancel;
        }

        public void setOCBRBranchCancel(Integer oCBRBranchCancel) {
            this.oCBRBranchCancel = oCBRBranchCancel;
        }

        public Integer getOCBRGuestCreate() {
            return oCBRGuestCreate;
        }

        public void setOCBRGuestCreate(Integer oCBRGuestCreate) {
            this.oCBRGuestCreate = oCBRGuestCreate;
        }

        public Integer getOCBRGuestModify() {
            return oCBRGuestModify;
        }

        public void setOCBRGuestModify(Integer oCBRGuestModify) {
            this.oCBRGuestModify = oCBRGuestModify;
        }

        public Integer getOCBRGuestCancel() {
            return oCBRGuestCancel;
        }

        public void setOCBRGuestCancel(Integer oCBRGuestCancel) {
            this.oCBRGuestCancel = oCBRGuestCancel;
        }

        public Integer getOCBRBankCardCreate() {
            return oCBRBankCardCreate;
        }

        public void setOCBRBankCardCreate(Integer oCBRBankCardCreate) {
            this.oCBRBankCardCreate = oCBRBankCardCreate;
        }

        public Integer getOCBRBankCardModify() {
            return oCBRBankCardModify;
        }

        public void setOCBRBankCardModify(Integer oCBRBankCardModify) {
            this.oCBRBankCardModify = oCBRBankCardModify;
        }

        public Integer getOCBRBankCardCancel() {
            return oCBRBankCardCancel;
        }

        public void setOCBRBankCardCancel(Integer oCBRBankCardCancel) {
            this.oCBRBankCardCancel = oCBRBankCardCancel;
        }

        public Integer getOCBRCompanyCardCreate() {
            return oCBRCompanyCardCreate;
        }

        public void setOCBRCompanyCardCreate(Integer oCBRCompanyCardCreate) {
            this.oCBRCompanyCardCreate = oCBRCompanyCardCreate;
        }

        public Integer getOCBRCompanyCardModify() {
            return oCBRCompanyCardModify;
        }

        public void setOCBRCompanyCardModify(Integer oCBRCompanyCardModify) {
            this.oCBRCompanyCardModify = oCBRCompanyCardModify;
        }

        public Integer getOCBRCompanyCardCancel() {
            return oCBRCompanyCardCancel;
        }

        public void setOCBRCompanyCardCancel(Integer oCBRCompanyCardCancel) {
            this.oCBRCompanyCardCancel = oCBRCompanyCardCancel;
        }

        public Integer getOCBRWaitingCreate() {
            return oCBRWaitingCreate;
        }

        public void setOCBRWaitingCreate(Integer oCBRWaitingCreate) {
            this.oCBRWaitingCreate = oCBRWaitingCreate;
        }

        public Integer getOCBRWaitingModify() {
            return oCBRWaitingModify;
        }

        public void setOCBRWaitingModify(Integer oCBRWaitingModify) {
            this.oCBRWaitingModify = oCBRWaitingModify;
        }

        public Integer getOCBRWaitingCancel() {
            return oCBRWaitingCancel;
        }

        public void setOCBRWaitingCancel(Integer oCBRWaitingCancel) {
            this.oCBRWaitingCancel = oCBRWaitingCancel;
        }

        public Integer getOCBRAgentQuotaCreate() {
            return oCBRAgentQuotaCreate;
        }

        public void setOCBRAgentQuotaCreate(Integer oCBRAgentQuotaCreate) {
            this.oCBRAgentQuotaCreate = oCBRAgentQuotaCreate;
        }

        public Integer getOCBRAgentQuotaModify() {
            return oCBRAgentQuotaModify;
        }

        public void setOCBRAgentQuotaModify(Integer oCBRAgentQuotaModify) {
            this.oCBRAgentQuotaModify = oCBRAgentQuotaModify;
        }

        public Integer getOCBRAgentQuotaCancel() {
            return oCBRAgentQuotaCancel;
        }

        public void setOCBRAgentQuotaCancel(Integer oCBRAgentQuotaCancel) {
            this.oCBRAgentQuotaCancel = oCBRAgentQuotaCancel;
        }

        public Integer getOCBROpenCreate() {
            return oCBROpenCreate;
        }

        public void setOCBROpenCreate(Integer oCBROpenCreate) {
            this.oCBROpenCreate = oCBROpenCreate;
        }

        public Integer getOCBROpenModify() {
            return oCBROpenModify;
        }

        public void setOCBROpenModify(Integer oCBROpenModify) {
            this.oCBROpenModify = oCBROpenModify;
        }

        public Integer getOCBROpenCancel() {
            return oCBROpenCancel;
        }

        public void setOCBROpenCancel(Integer oCBROpenCancel) {
            this.oCBROpenCancel = oCBROpenCancel;
        }

        public Integer getOCBRIsTDS() {
            return oCBRIsTDS;
        }

        public void setOCBRIsTDS(Integer oCBRIsTDS) {
            this.oCBRIsTDS = oCBRIsTDS;
        }

        public Double getOCBRTDS() {
            return oCBRTDS;
        }

        public void setOCBRTDS(Double oCBRTDS) {
            this.oCBRTDS = oCBRTDS;
        }

        public Integer getOCBRConfirm() {
            return oCBRConfirm;
        }

        public void setOCBRConfirm(Integer oCBRConfirm) {
            this.oCBRConfirm = oCBRConfirm;
        }

        public Integer getOCBRPhone() {
            return oCBRPhone;
        }

        public void setOCBRPhone(Integer oCBRPhone) {
            this.oCBRPhone = oCBRPhone;
        }

        public Integer getOCBRAgent() {
            return oCBRAgent;
        }

        public void setOCBRAgent(Integer oCBRAgent) {
            this.oCBRAgent = oCBRAgent;
        }

        public Integer getOCBRBranch() {
            return oCBRBranch;
        }

        public void setOCBRBranch(Integer oCBRBranch) {
            this.oCBRBranch = oCBRBranch;
        }

        public Integer getOCBRGuest() {
            return oCBRGuest;
        }

        public void setOCBRGuest(Integer oCBRGuest) {
            this.oCBRGuest = oCBRGuest;
        }

        public Integer getOCBRWaiting() {
            return oCBRWaiting;
        }

        public void setOCBRWaiting(Integer oCBRWaiting) {
            this.oCBRWaiting = oCBRWaiting;
        }

        public Integer getOCBRBankCard() {
            return oCBRBankCard;
        }

        public void setOCBRBankCard(Integer oCBRBankCard) {
            this.oCBRBankCard = oCBRBankCard;
        }

        public Integer getOCBRCompanyCard() {
            return oCBRCompanyCard;
        }

        public void setOCBRCompanyCard(Integer oCBRCompanyCard) {
            this.oCBRCompanyCard = oCBRCompanyCard;
        }

        public Integer getOCBRAgentQuota() {
            return oCBRAgentQuota;
        }

        public void setOCBRAgentQuota(Integer oCBRAgentQuota) {
            this.oCBRAgentQuota = oCBRAgentQuota;
        }

        public Integer getOCBROpen() {
            return oCBROpen;
        }

        public void setOCBROpen(Integer oCBROpen) {
            this.oCBROpen = oCBROpen;
        }

        public Integer getCMIsACServiceTax() {
            return cMIsACServiceTax;
        }

        public void setCMIsACServiceTax(Integer cMIsACServiceTax) {
            this.cMIsACServiceTax = cMIsACServiceTax;
        }

        public Integer getCMIsNonACServiceTax() {
            return cMIsNonACServiceTax;
        }

        public void setCMIsNonACServiceTax(Integer cMIsNonACServiceTax) {
            this.cMIsNonACServiceTax = cMIsNonACServiceTax;
        }

        public Double getCMACServiceTax() {
            return cMACServiceTax;
        }

        public void setCMACServiceTax(Double cMACServiceTax) {
            this.cMACServiceTax = cMACServiceTax;
        }

        public Double getCMNonACServiceTax() {
            return cMNonACServiceTax;
        }

        public void setCMNonACServiceTax(Double cMNonACServiceTax) {
            this.cMNonACServiceTax = cMNonACServiceTax;
        }

        public Integer getCMACFareIncludeTax() {
            return cMACFareIncludeTax;
        }

        public void setCMACFareIncludeTax(Integer cMACFareIncludeTax) {
            this.cMACFareIncludeTax = cMACFareIncludeTax;
        }

        public Integer getCMNonACFareIncludeTax() {
            return cMNonACFareIncludeTax;
        }

        public void setCMNonACFareIncludeTax(Integer cMNonACFareIncludeTax) {
            this.cMNonACFareIncludeTax = cMNonACFareIncludeTax;
        }

        public Integer getCMIsSTOffline() {
            return cMIsSTOffline;
        }

        public void setCMIsSTOffline(Integer cMIsSTOffline) {
            this.cMIsSTOffline = cMIsSTOffline;
        }

        public Integer getCMIsSTLogin() {
            return cMIsSTLogin;
        }

        public void setCMIsSTLogin(Integer cMIsSTLogin) {
            this.cMIsSTLogin = cMIsSTLogin;
        }

        public Integer getCMIsSTAPI() {
            return cMIsSTAPI;
        }

        public void setCMIsSTAPI(Integer cMIsSTAPI) {
            this.cMIsSTAPI = cMIsSTAPI;
        }

        public Integer getCMIsSTB2C() {
            return cMIsSTB2C;
        }

        public void setCMIsSTB2C(Integer cMIsSTB2C) {
            this.cMIsSTB2C = cMIsSTB2C;
        }

        public Integer getCMIsSTBranch() {
            return cMIsSTBranch;
        }

        public void setCMIsSTBranch(Integer cMIsSTBranch) {
            this.cMIsSTBranch = cMIsSTBranch;
        }

        public Integer getOCBRAllowModifyPhoneOfRouteCompany() {
            return oCBRAllowModifyPhoneOfRouteCompany;
        }

        public void setOCBRAllowModifyPhoneOfRouteCompany(Integer oCBRAllowModifyPhoneOfRouteCompany) {
            this.oCBRAllowModifyPhoneOfRouteCompany = oCBRAllowModifyPhoneOfRouteCompany;
        }

        public Double getCMServiceTaxRoundUp() {
            return cMServiceTaxRoundUp;
        }

        public void setCMServiceTaxRoundUp(Double cMServiceTaxRoundUp) {
            this.cMServiceTaxRoundUp = cMServiceTaxRoundUp;
        }

        public Double getCMACPackageServiceTax() {
            return cMACPackageServiceTax;
        }

        public void setCMACPackageServiceTax(Double cMACPackageServiceTax) {
            this.cMACPackageServiceTax = cMACPackageServiceTax;
        }

        public Double getCMNonACPackageServiceTax() {
            return cMNonACPackageServiceTax;
        }

        public void setCMNonACPackageServiceTax(Double cMNonACPackageServiceTax) {
            this.cMNonACPackageServiceTax = cMNonACPackageServiceTax;
        }

        public Double getCMACOwnScheduleServiceTax() {
            return cMACOwnScheduleServiceTax;
        }

        public void setCMACOwnScheduleServiceTax(Double cMACOwnScheduleServiceTax) {
            this.cMACOwnScheduleServiceTax = cMACOwnScheduleServiceTax;
        }

        public Double getCMNonACOwnScheduleServiceTax() {
            return cMNonACOwnScheduleServiceTax;
        }

        public void setCMNonACOwnScheduleServiceTax(Double cMNonACOwnScheduleServiceTax) {
            this.cMNonACOwnScheduleServiceTax = cMNonACOwnScheduleServiceTax;
        }

        public Integer getOCBRAllowCouponCode() {
            return oCBRAllowCouponCode;
        }

        public void setOCBRAllowCouponCode(Integer oCBRAllowCouponCode) {
            this.oCBRAllowCouponCode = oCBRAllowCouponCode;
        }

        public Integer getOCBRAllowFareChange() {
            return oCBRAllowFareChange;
        }

        public void setOCBRAllowFareChange(Integer oCBRAllowFareChange) {
            this.oCBRAllowFareChange = oCBRAllowFareChange;
        }

        public Double getCMACOtherOwnScheduleServiceTax() {
            return cMACOtherOwnScheduleServiceTax;
        }

        public void setCMACOtherOwnScheduleServiceTax(Double cMACOtherOwnScheduleServiceTax) {
            this.cMACOtherOwnScheduleServiceTax = cMACOtherOwnScheduleServiceTax;
        }

        public Double getCMNonACOtherOwnScheduleServiceTax() {
            return cMNonACOtherOwnScheduleServiceTax;
        }

        public void setCMNonACOtherOwnScheduleServiceTax(Double cMNonACOtherOwnScheduleServiceTax) {
            this.cMNonACOtherOwnScheduleServiceTax = cMNonACOtherOwnScheduleServiceTax;
        }

        public Integer getOCBRModifyonOtherRoute() {
            return oCBRModifyonOtherRoute;
        }

        public void setOCBRModifyonOtherRoute(Integer oCBRModifyonOtherRoute) {
            this.oCBRModifyonOtherRoute = oCBRModifyonOtherRoute;
        }

        public Integer getOCBRAllowNonReported() {
            return oCBRAllowNonReported;
        }

        public void setOCBRAllowNonReported(Integer oCBRAllowNonReported) {
            this.oCBRAllowNonReported = oCBRAllowNonReported;
        }

        public Integer getOCBRStopBooking() {
            return oCBRStopBooking;
        }

        public void setOCBRStopBooking(Integer oCBRStopBooking) {
            this.oCBRStopBooking = oCBRStopBooking;
        }

        public Integer getOCBROnlineAgentCreate() {
            return oCBROnlineAgentCreate;
        }

        public void setOCBROnlineAgentCreate(Integer oCBROnlineAgentCreate) {
            this.oCBROnlineAgentCreate = oCBROnlineAgentCreate;
        }

        public Integer getOCBROnlineAgentModify() {
            return oCBROnlineAgentModify;
        }

        public void setOCBROnlineAgentModify(Integer oCBROnlineAgentModify) {
            this.oCBROnlineAgentModify = oCBROnlineAgentModify;
        }

        public Integer getOCBROnlineAgentCancel() {
            return oCBROnlineAgentCancel;
        }

        public void setOCBROnlineAgentCancel(Integer oCBROnlineAgentCancel) {
            this.oCBROnlineAgentCancel = oCBROnlineAgentCancel;
        }

        public Integer getOCBRAPIAgentCreate() {
            return oCBRAPIAgentCreate;
        }

        public void setOCBRAPIAgentCreate(Integer oCBRAPIAgentCreate) {
            this.oCBRAPIAgentCreate = oCBRAPIAgentCreate;
        }

        public Integer getOCBRAPIAgentModify() {
            return oCBRAPIAgentModify;
        }

        public void setOCBRAPIAgentModify(Integer oCBRAPIAgentModify) {
            this.oCBRAPIAgentModify = oCBRAPIAgentModify;
        }

        public Integer getOCBRAPIAgentCancel() {
            return oCBRAPIAgentCancel;
        }

        public void setOCBRAPIAgentCancel(Integer oCBRAPIAgentCancel) {
            this.oCBRAPIAgentCancel = oCBRAPIAgentCancel;
        }

        public Integer getOCBRB2CAgentCreate() {
            return oCBRB2CAgentCreate;
        }

        public void setOCBRB2CAgentCreate(Integer oCBRB2CAgentCreate) {
            this.oCBRB2CAgentCreate = oCBRB2CAgentCreate;
        }

        public Integer getOCBRB2CAgentModify() {
            return oCBRB2CAgentModify;
        }

        public void setOCBRB2CAgentModify(Integer oCBRB2CAgentModify) {
            this.oCBRB2CAgentModify = oCBRB2CAgentModify;
        }

        public Integer getOCBRB2CAgentCancel() {
            return oCBRB2CAgentCancel;
        }

        public void setOCBRB2CAgentCancel(Integer oCBRB2CAgentCancel) {
            this.oCBRB2CAgentCancel = oCBRB2CAgentCancel;
        }

        public Integer getOCBRAllowReprint() {
            return oCBRAllowReprint;
        }

        public void setOCBRAllowReprint(Integer oCBRAllowReprint) {
            this.oCBRAllowReprint = oCBRAllowReprint;
        }

        public Integer getOCBRAllowFromToCity() {
            return oCBRAllowFromToCity;
        }

        public void setOCBRAllowFromToCity(Integer oCBRAllowFromToCity) {
            this.oCBRAllowFromToCity = oCBRAllowFromToCity;
        }

        public Integer getOSAllowModifyCharges() {
            return oSAllowModifyCharges;
        }

        public void setOSAllowModifyCharges(Integer oSAllowModifyCharges) {
            this.oSAllowModifyCharges = oSAllowModifyCharges;
        }

        public Integer getOCBRChartSMS() {
            return oCBRChartSMS;
        }

        public void setOCBRChartSMS(Integer oCBRChartSMS) {
            this.oCBRChartSMS = oCBRChartSMS;
        }

        public Integer getOCBRReSendBookingSMS() {
            return oCBRReSendBookingSMS;
        }

        public void setOCBRReSendBookingSMS(Integer oCBRReSendBookingSMS) {
            this.oCBRReSendBookingSMS = oCBRReSendBookingSMS;
        }

        public Integer getOCBRWallet() {
            return oCBRWallet;
        }

        public void setOCBRWallet(Integer oCBRWallet) {
            this.oCBRWallet = oCBRWallet;
        }

        public Integer getOCBRWalletCreate() {
            return oCBRWalletCreate;
        }

        public void setOCBRWalletCreate(Integer oCBRWalletCreate) {
            this.oCBRWalletCreate = oCBRWalletCreate;
        }

        public Integer getOCBRWalletModify() {
            return oCBRWalletModify;
        }

        public void setOCBRWalletModify(Integer oCBRWalletModify) {
            this.oCBRWalletModify = oCBRWalletModify;
        }

        public Integer getOCBRWalletCancel() {
            return oCBRWalletCancel;
        }

        public void setOCBRWalletCancel(Integer oCBRWalletCancel) {
            this.oCBRWalletCancel = oCBRWalletCancel;
        }

        public Integer getOCBRSentOwnBookingSMS() {
            return oCBRSentOwnBookingSMS;
        }

        public void setOCBRSentOwnBookingSMS(Integer oCBRSentOwnBookingSMS) {
            this.oCBRSentOwnBookingSMS = oCBRSentOwnBookingSMS;
        }

        public Integer getCMIsSTBankCard() {
            return cMIsSTBankCard;
        }

        public void setCMIsSTBankCard(Integer cMIsSTBankCard) {
            this.cMIsSTBankCard = cMIsSTBankCard;
        }

        public Integer getCMIsSTITSPLWallet() {
            return cMIsSTITSPLWallet;
        }

        public void setCMIsSTITSPLWallet(Integer cMIsSTITSPLWallet) {
            this.cMIsSTITSPLWallet = cMIsSTITSPLWallet;
        }

        public Integer getCMIsSTRemotePayment() {
            return cMIsSTRemotePayment;
        }

        public void setCMIsSTRemotePayment(Integer cMIsSTRemotePayment) {
            this.cMIsSTRemotePayment = cMIsSTRemotePayment;
        }

        public Integer getOCBRRemotePayment() {
            return oCBRRemotePayment;
        }

        public void setOCBRRemotePayment(Integer oCBRRemotePayment) {
            this.oCBRRemotePayment = oCBRRemotePayment;
        }

        public Integer getOCBRRemotePaymentCreate() {
            return oCBRRemotePaymentCreate;
        }

        public void setOCBRRemotePaymentCreate(Integer oCBRRemotePaymentCreate) {
            this.oCBRRemotePaymentCreate = oCBRRemotePaymentCreate;
        }

        public Integer getOCBRRemotePaymentModify() {
            return oCBRRemotePaymentModify;
        }

        public void setOCBRRemotePaymentModify(Integer oCBRRemotePaymentModify) {
            this.oCBRRemotePaymentModify = oCBRRemotePaymentModify;
        }

        public Integer getOCBRRemotePaymentCancel() {
            return oCBRRemotePaymentCancel;
        }

        public void setOCBRRemotePaymentCancel(Integer oCBRRemotePaymentCancel) {
            this.oCBRRemotePaymentCancel = oCBRRemotePaymentCancel;
        }

        public Integer getOSIsAskIDProof() {
            return oSIsAskIDProof;
        }

        public void setOSIsAskIDProof(Integer oSIsAskIDProof) {
            this.oSIsAskIDProof = oSIsAskIDProof;
        }

        public Integer getOSIsIDProofMandatory() {
            return oSIsIDProofMandatory;
        }

        public void setOSIsIDProofMandatory(Integer oSIsIDProofMandatory) {
            this.oSIsIDProofMandatory = oSIsIDProofMandatory;
        }

        public Integer getOSIsCrossStateIDProofMandatory() {
            return oSIsCrossStateIDProofMandatory;
        }

        public void setOSIsCrossStateIDProofMandatory(Integer oSIsCrossStateIDProofMandatory) {
            this.oSIsCrossStateIDProofMandatory = oSIsCrossStateIDProofMandatory;
        }

        public Integer getOCBRDisplayBookingTagToolTip() {
            return oCBRDisplayBookingTagToolTip;
        }

        public void setOCBRDisplayBookingTagToolTip(Integer oCBRDisplayBookingTagToolTip) {
            this.oCBRDisplayBookingTagToolTip = oCBRDisplayBookingTagToolTip;
        }

    }

    public class ITSOnlineWallet {

        @SerializedName("WCM_ID")
        @Expose
        private Integer wCMID;
        @SerializedName("WCM_CompanyName")
        @Expose
        private String wCMCompanyName;
        @SerializedName("WCD_WalletKey")
        @Expose
        private String wCDWalletKey;
        @SerializedName("WCD_TDRCharges")
        @Expose
        private Double wCDTDRCharges;
        @SerializedName("WCM_IsOTP")
        @Expose
        private Integer wCMIsOTP;

        public Integer getWCMID() {
            return wCMID;
        }

        public void setWCMID(Integer wCMID) {
            this.wCMID = wCMID;
        }

        public String getWCMCompanyName() {
            return wCMCompanyName;
        }

        public void setWCMCompanyName(String wCMCompanyName) {
            this.wCMCompanyName = wCMCompanyName;
        }

        public String getWCDWalletKey() {
            return wCDWalletKey;
        }

        public void setWCDWalletKey(String wCDWalletKey) {
            this.wCDWalletKey = wCDWalletKey;
        }

        public Double getWCDTDRCharges() {
            return wCDTDRCharges;
        }

        public void setWCDTDRCharges(Double wCDTDRCharges) {
            this.wCDTDRCharges = wCDTDRCharges;
        }

        public Integer getWCMIsOTP() {
            return wCMIsOTP;
        }

        public void setWCMIsOTP(Integer wCMIsOTP) {
            this.wCMIsOTP = wCMIsOTP;
        }

    }

    @Entity(tableName = "ITSRemarkModify")
    public static class ITSRemarkModify {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("RM_RemarkID")
        @Expose
        private Integer rMRemarkID;
        @SerializedName("RM_RemarkType")
        @Expose
        private String rMRemarkType;
        @SerializedName("RM_RemarkName")
        @Expose
        private String rMRemarkName;

        public Integer getRMRemarkID() {
            return rMRemarkID;
        }

        public void setRMRemarkID(Integer rMRemarkID) {
            this.rMRemarkID = rMRemarkID;
        }

        public String getRMRemarkType() {
            return rMRemarkType;
        }

        public void setRMRemarkType(String rMRemarkType) {
            this.rMRemarkType = rMRemarkType;
        }

        public String getRMRemarkName() {
            return rMRemarkName;
        }

        public void setRMRemarkName(String rMRemarkName) {
            this.rMRemarkName = rMRemarkName;
        }

    }

    public class ITSGetBranchUserByCompanyID {

        @SerializedName("BUM_BranchUserID")
        @Expose
        private Integer bUMBranchUserID;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("BUM_UserName")
        @Expose
        private String bUMUserName;
        @SerializedName("BUM_Password")
        @Expose
        private String bUMPassword;
        @SerializedName("BUM_Position")
        @Expose
        private String bUMPosition;
        @SerializedName("BUM_FullName")
        @Expose
        private String bUMFullName;
        @SerializedName("BUM_Address")
        @Expose
        private String bUMAddress;
        @SerializedName("BUM_ContactNo")
        @Expose
        private String bUMContactNo;
        @SerializedName("BUM_Photo")
        @Expose
        private String bUMPhoto;
        @SerializedName("SM_StateID")
        @Expose
        private Integer sMStateID;
        @SerializedName("CM_CityID")
        @Expose
        private Integer cMCityID;
        @SerializedName("BUM_ReferenceBy")
        @Expose
        private String bUMReferenceBy;
        @SerializedName("BUM_ContactNoOfReference")
        @Expose
        private String bUMContactNoOfReference;
        @SerializedName("BUM_Remarks")
        @Expose
        private String bUMRemarks;
        @SerializedName("BUM_BirthDate")
        @Expose
        private String bUMBirthDate;
        @SerializedName("BUM_BranchCode")
        @Expose
        private String bUMBranchCode;
        @SerializedName("BUM_CreatedDate")
        @Expose
        private String bUMCreatedDate;
        @SerializedName("BUM_IsActive")
        @Expose
        private Integer bUMIsActive;
        @SerializedName("BUM_Agent_Number")
        @Expose
        private String bUMAgentNumber;

        public Integer getBUMBranchUserID() {
            return bUMBranchUserID;
        }

        public void setBUMBranchUserID(Integer bUMBranchUserID) {
            this.bUMBranchUserID = bUMBranchUserID;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public String getBUMUserName() {
            return bUMUserName;
        }

        public void setBUMUserName(String bUMUserName) {
            this.bUMUserName = bUMUserName;
        }

        public String getBUMPassword() {
            return bUMPassword;
        }

        public void setBUMPassword(String bUMPassword) {
            this.bUMPassword = bUMPassword;
        }

        public String getBUMPosition() {
            return bUMPosition;
        }

        public void setBUMPosition(String bUMPosition) {
            this.bUMPosition = bUMPosition;
        }

        public String getBUMFullName() {
            return bUMFullName;
        }

        public void setBUMFullName(String bUMFullName) {
            this.bUMFullName = bUMFullName;
        }

        public String getBUMAddress() {
            return bUMAddress;
        }

        public void setBUMAddress(String bUMAddress) {
            this.bUMAddress = bUMAddress;
        }

        public String getBUMContactNo() {
            return bUMContactNo;
        }

        public void setBUMContactNo(String bUMContactNo) {
            this.bUMContactNo = bUMContactNo;
        }

        public String getBUMPhoto() {
            return bUMPhoto;
        }

        public void setBUMPhoto(String bUMPhoto) {
            this.bUMPhoto = bUMPhoto;
        }

        public Integer getSMStateID() {
            return sMStateID;
        }

        public void setSMStateID(Integer sMStateID) {
            this.sMStateID = sMStateID;
        }

        public Integer getCMCityID() {
            return cMCityID;
        }

        public void setCMCityID(Integer cMCityID) {
            this.cMCityID = cMCityID;
        }

        public String getBUMReferenceBy() {
            return bUMReferenceBy;
        }

        public void setBUMReferenceBy(String bUMReferenceBy) {
            this.bUMReferenceBy = bUMReferenceBy;
        }

        public String getBUMContactNoOfReference() {
            return bUMContactNoOfReference;
        }

        public void setBUMContactNoOfReference(String bUMContactNoOfReference) {
            this.bUMContactNoOfReference = bUMContactNoOfReference;
        }

        public String getBUMRemarks() {
            return bUMRemarks;
        }

        public void setBUMRemarks(String bUMRemarks) {
            this.bUMRemarks = bUMRemarks;
        }

        public String getBUMBirthDate() {
            return bUMBirthDate;
        }

        public void setBUMBirthDate(String bUMBirthDate) {
            this.bUMBirthDate = bUMBirthDate;
        }

        public String getBUMBranchCode() {
            return bUMBranchCode;
        }

        public void setBUMBranchCode(String bUMBranchCode) {
            this.bUMBranchCode = bUMBranchCode;
        }

        public String getBUMCreatedDate() {
            return bUMCreatedDate;
        }

        public void setBUMCreatedDate(String bUMCreatedDate) {
            this.bUMCreatedDate = bUMCreatedDate;
        }

        public Integer getBUMIsActive() {
            return bUMIsActive;
        }

        public void setBUMIsActive(Integer bUMIsActive) {
            this.bUMIsActive = bUMIsActive;
        }

        public String getBUMAgentNumber() {
            return bUMAgentNumber;
        }

        public void setBUMAgentNumber(String bUMAgentNumber) {
            this.bUMAgentNumber = bUMAgentNumber;
        }

    }

    @Entity(tableName = "ITSGetDisplayRouteBeforeMin")
    public static class ITSGetDisplayRouteBeforeMin {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("CDH_RouteBeforeMin")
        @Expose
        private Integer cDHRouteBeforeMin;
        @SerializedName("CDH_RouteBeforeMinAgent")
        @Expose
        private Integer cDHRouteBeforeMinAgent;
        @SerializedName("CDH_BackDateShow")
        @Expose
        private Integer cDHBackDateShow;
        @SerializedName("CDH_AdvanceDateShow")
        @Expose
        private Integer cDHAdvanceDateShow;
        @SerializedName("CDH_ShowStopBooking")
        @Expose
        private Integer cDHShowStopBooking;
        @SerializedName("CDH_RouteTimeCode")
        @Expose
        private Integer cDHRouteTimeCode;
        @SerializedName("CDH_AgentCode")
        @Expose
        private Integer cDHAgentCode;
        @SerializedName("CM_IsSMSActive")
        @Expose
        private Integer cMIsSMSActive;
        @SerializedName("OSA_OnChartSMS")
        @Expose
        private Integer oSAOnChartSMS;
        @SerializedName("CDH_AutoDeletePhoneTime")
        @Expose
        private Integer cDHAutoDeletePhoneTime;
        @SerializedName("CDH_AutoDeleteHour")
        @Expose
        private Integer cDHAutoDeleteHour;
        @SerializedName("CDH_AutoDeleteMinute")
        @Expose
        private Integer cDHAutoDeleteMinute;
        @SerializedName("OS_DisplayNetRate")
        @Expose
        private Integer oSDisplayNetRate;
        @SerializedName("OS_CheckLadiesSeat")
        @Expose
        private Integer oSCheckLadiesSeat;
        @SerializedName("OS_LadiesSeatBesideMale")
        @Expose
        private Integer oSLadiesSeatBesideMale;
        @SerializedName("OS_ShowAvailability")
        @Expose
        private Integer oSShowAvailability;
        @SerializedName("OS_FocusOnRate")
        @Expose
        private Integer oSFocusOnRate;
        @SerializedName("OS_EmailConfirm")
        @Expose
        private Integer oSEmailConfirm;
        @SerializedName("OS_EmailPhone")
        @Expose
        private Integer oSEmailPhone;
        @SerializedName("OS_EmailAgent")
        @Expose
        private Integer oSEmailAgent;
        @SerializedName("OS_EmailBranch")
        @Expose
        private Integer oSEmailBranch;
        @SerializedName("OS_EmailGuest")
        @Expose
        private Integer oSEmailGuest;
        @SerializedName("OS_EmailBankCard")
        @Expose
        private Integer oSEmailBankCard;
        @SerializedName("OS_EmailCompanyCard")
        @Expose
        private Integer oSEmailCompanyCard;
        @SerializedName("OS_EmailWaiting")
        @Expose
        private Integer oSEmailWaiting;
        @SerializedName("OS_EmailAgentQuata")
        @Expose
        private Integer oSEmailAgentQuata;
        @SerializedName("OS_EmailOpen")
        @Expose
        private Integer oSEmailOpen;
        @SerializedName("OS_SMSConfirm")
        @Expose
        private Integer oSSMSConfirm;
        @SerializedName("OS_SMSPhone")
        @Expose
        private Integer oSSMSPhone;
        @SerializedName("OS_SMSAgent")
        @Expose
        private Integer oSSMSAgent;
        @SerializedName("OS_SMSBranch")
        @Expose
        private Integer oSSMSBranch;
        @SerializedName("OS_SMSGuest")
        @Expose
        private Integer oSSMSGuest;
        @SerializedName("OS_SMSBankCard")
        @Expose
        private Integer oSSMSBankCard;
        @SerializedName("OS_SMSCompanyCard")
        @Expose
        private Integer oSSMSCompanyCard;
        @SerializedName("OS_SMSWaiting")
        @Expose
        private Integer oSSMSWaiting;
        @SerializedName("OS_SMSAgentQuata")
        @Expose
        private Integer oSSMSAgentQuata;
        @SerializedName("OS_SMSOpen")
        @Expose
        private Integer oSSMSOpen;
        @SerializedName("OS_EmailGeneralAgent")
        @Expose
        private Integer oSEmailGeneralAgent;
        @SerializedName("OS_EmailOnlineAgent")
        @Expose
        private Integer oSEmailOnlineAgent;
        @SerializedName("OS_EmailAPIAgent")
        @Expose
        private Integer oSEmailAPIAgent;
        @SerializedName("OS_EmailB2CAgent")
        @Expose
        private Integer oSEmailB2CAgent;
        @SerializedName("OS_SMSGeneralAgent")
        @Expose
        private Integer oSSMSGeneralAgent;
        @SerializedName("OS_SMSOnlineAgent")
        @Expose
        private Integer oSSMSOnlineAgent;
        @SerializedName("OS_SMSAPIAgent")
        @Expose
        private Integer oSSMSAPIAgent;
        @SerializedName("OS_SMSB2CAgent")
        @Expose
        private Integer oSSMSB2CAgent;
        @SerializedName("OS_StaticData")
        @Expose
        private Integer oSStaticData;
        @SerializedName("OS_PDFConfirm")
        @Expose
        private Integer oSPDFConfirm;
        @SerializedName("OS_PDFPhone")
        @Expose
        private Integer oSPDFPhone;
        @SerializedName("OS_PDFGeneralAgent")
        @Expose
        private Integer oSPDFGeneralAgent;
        @SerializedName("OS_PDFOnlineAgent")
        @Expose
        private Integer oSPDFOnlineAgent;
        @SerializedName("OS_PDFAPIAgent")
        @Expose
        private Integer oSPDFAPIAgent;
        @SerializedName("OS_PDFB2CAgent")
        @Expose
        private Integer oSPDFB2CAgent;
        @SerializedName("OS_PDFBranch")
        @Expose
        private Integer oSPDFBranch;
        @SerializedName("OS_PDFGuest")
        @Expose
        private Integer oSPDFGuest;
        @SerializedName("OS_PDFBankCard")
        @Expose
        private Integer oSPDFBankCard;
        @SerializedName("OS_PDFCompanyCard")
        @Expose
        private Integer oSPDFCompanyCard;
        @SerializedName("OS_PDFAgentQuata")
        @Expose
        private Integer oSPDFAgentQuata;
        @SerializedName("OS_ReprintGeneralAgent")
        @Expose
        private Integer oSReprintGeneralAgent;
        @SerializedName("OS_HoldBranch")
        @Expose
        private Integer oSHoldBranch;
        @SerializedName("OS_HoldCompanyCard")
        @Expose
        private Integer oSHoldCompanyCard;
        @SerializedName("OS_HoldBankCard")
        @Expose
        private Integer oSHoldBankCard;
        @SerializedName("OS_HoldGeneralAgent")
        @Expose
        private Integer oSHoldGeneralAgent;
        @SerializedName("OS_HoldAgentQuota")
        @Expose
        private Integer oSHoldAgentQuota;
        @SerializedName("OS_HoldGuest")
        @Expose
        private Integer oSHoldGuest;
        @SerializedName("OS_BSPrintButton")
        @Expose
        private Integer oSBSPrintButton;
        @SerializedName("OS_FemaleSeatInBirth")
        @Expose
        private Integer oSFemaleSeatInBirth;
        @SerializedName("CDH_ChartIdleMins")
        @Expose
        private String cDHChartIdleMins;
        @SerializedName("OS_IsPendingAmount")
        @Expose
        private Integer oSIsPendingAmount;
        @SerializedName("OS_RefreshChartMin")
        @Expose
        private Integer oSRefreshChartMin;
        @SerializedName("OS_AllowAutoF5")
        @Expose
        private Integer oSAllowAutoF5;
        @SerializedName("OS_AutoF5Min")
        @Expose
        private Integer oSAutoF5Min;
        @SerializedName("OS_AllowAutoChartRefresh")
        @Expose
        private Integer oSAllowAutoChartRefresh;
        @SerializedName("OS_TicketPrint_Cancel")
        @Expose
        private Integer oSTicketPrintCancel;
        @SerializedName("OS_IsHighlightMF")
        @Expose
        private Integer oSIsHighlightMF;
        @SerializedName("OS_AdlabsURL")
        @Expose
        private String oSAdlabsURL;
        @SerializedName("OS_IsLabelDisplay")
        @Expose
        private Integer oSIsLabelDisplay;
        @SerializedName("OS_LableText")
        @Expose
        private String oSLableText;
        @SerializedName("OS_IsSeatWiseFare")
        @Expose
        private Integer oSIsSeatWiseFare;
        @SerializedName("OS_IsChangePassword")
        @Expose
        private Integer oSIsChangePassword;
        @SerializedName("OS_AllowToAskBusSchedule")
        @Expose
        private Integer oSAllowToAskBusSchedule;
        @SerializedName("OS_IsCheckAgentCredit")
        @Expose
        private Integer oSIsCheckAgentCredit;
        @SerializedName("OS_IsScreenSaver")
        @Expose
        private Integer oSIsScreenSaver;
        @SerializedName("CM_IsACServiceTax")
        @Expose
        private Integer cMIsACServiceTax;
        @SerializedName("CM_ACFareIncludeTax")
        @Expose
        private Integer cMACFareIncludeTax;
        @SerializedName("CM_ACServiceTax")
        @Expose
        private Double cMACServiceTax;
        @SerializedName("CM_IsNonACServiceTax")
        @Expose
        private Integer cMIsNonACServiceTax;
        @SerializedName("CM_NonACFareIncludeTax")
        @Expose
        private Integer cMNonACFareIncludeTax;
        @SerializedName("CM_NonACServiceTax")
        @Expose
        private Double cMNonACServiceTax;
        @SerializedName("CM_IsSTOffline")
        @Expose
        private Integer cMIsSTOffline;
        @SerializedName("CM_IsSTLogin")
        @Expose
        private Integer cMIsSTLogin;
        @SerializedName("CM_IsSTAPI")
        @Expose
        private Integer cMIsSTAPI;
        @SerializedName("CM_IsSTB2C")
        @Expose
        private Integer cMIsSTB2C;
        @SerializedName("CM_IsSTBranch")
        @Expose
        private Integer cMIsSTBranch;
        @SerializedName("OS_ScreenSaverMinute")
        @Expose
        private Integer oSScreenSaverMinute;
        @SerializedName("CDH_AutoDeletePhoneBookMinutes")
        @Expose
        private Integer cDHAutoDeletePhoneBookMinutes;
        @SerializedName("OS_IsFocusOnPickup")
        @Expose
        private Integer oSIsFocusOnPickup;
        @SerializedName("MyBookingURL")
        @Expose
        private String myBookingURL;
        @SerializedName("CM_ServiceTaxRoundUp")
        @Expose
        private Integer cMServiceTaxRoundUp;
        @SerializedName("CM_ACPackageServiceTax")
        @Expose
        private Double cMACPackageServiceTax;
        @SerializedName("CM_NonACPackageServiceTax")
        @Expose
        private Double cMNonACPackageServiceTax;
        @SerializedName("CM_ACOwnScheduleServiceTax")
        @Expose
        private Double cMACOwnScheduleServiceTax;
        @SerializedName("CM_NonACOwnScheduleServiceTax")
        @Expose
        private Double cMNonACOwnScheduleServiceTax;
        @SerializedName("OS_AllowToAskAgentRefund")
        @Expose
        private Integer oSAllowToAskAgentRefund;
        @SerializedName("OS_AllowCoupanCode")
        @Expose
        private Integer oSAllowCoupanCode;
        @SerializedName("OS_AllowToShowPastBooking")
        @Expose
        private Integer oSAllowToShowPastBooking;
        @SerializedName("OS_IsBranchBookingAsConfirm")
        @Expose
        private Integer oSIsBranchBookingAsConfirm;
        @SerializedName("OS_IsReturnPhoneBookBranchCityOnly")
        @Expose
        private Integer oSIsReturnPhoneBookBranchCityOnly;
        @SerializedName("OS_MaxCompanyCardBooking")
        @Expose
        private Integer oSMaxCompanyCardBooking;
        @SerializedName("OS_DisplayAllFareBox")
        @Expose
        private Integer oSDisplayAllFareBox;
        @SerializedName("OS_AllowToSetAmenities")
        @Expose
        private Integer oSAllowToSetAmenities;
        @SerializedName("OS_LastLocationByBusNo")
        @Expose
        private Integer oSLastLocationByBusNo;
        @SerializedName("OS_EmailOnlineWallet")
        @Expose
        private Integer oSEmailOnlineWallet;
        @SerializedName("OS_PDFOnlineWallet")
        @Expose
        private Integer oSPDFOnlineWallet;
        @SerializedName("OS_CheckMaleSeatBesideFemaleInExe")
        @Expose
        private Integer oSCheckMaleSeatBesideFemaleInExe;
        @SerializedName("OS_IsShowAgentPendingAmount")
        @Expose
        private Integer oSIsShowAgentPendingAmount;
        @SerializedName("ISCountTableInBooked")
        @Expose
        private Integer iSCountTableInBooked;
        @SerializedName("OS_IsRemotePaymentTDR")
        @Expose
        private Integer oSIsRemotePaymentTDR;
        @SerializedName("OS_RemotePaymentTDRCharge")
        @Expose
        private Double oSRemotePaymentTDRCharge;
        @SerializedName("OS_IsIncludeRemotePaymentTDR")
        @Expose
        private Integer oSIsIncludeRemotePaymentTDR;
        @SerializedName("CM_ACOtherOwnScheduleServiceTax")
        @Expose
        private Double cMACOtherOwnScheduleServiceTax;
        @SerializedName("CM_NonACOtherOwnScheduleServiceTax")
        @Expose
        private Double cMNonACOtherOwnScheduleServiceTax;
        @SerializedName("OS_IsFirstYesAndPrint")
        @Expose
        private Integer oSIsFirstYesAndPrint;
        @SerializedName("OS_IsHideYesAndPrint")
        @Expose
        private Integer oSIsHideYesAndPrint;
        @SerializedName("OS_PhoneBookingToRemotePayment")
        @Expose
        private Integer oSPhoneBookingToRemotePayment;
        @SerializedName("OS_CouponCodeMaxSeatBook")
        @Expose
        private Integer oSCouponCodeMaxSeatBook;
        @SerializedName("OS_AllowToFetchAllPaxDetail")
        @Expose
        private Integer oSAllowToFetchAllPaxDetail;
        @SerializedName("OS_IsApplyGST")
        @Expose
        private Integer oSIsApplyGST;
        @SerializedName("OS_ShowRateOnSeat")
        @Expose
        private Integer oSShowRateOnSeat;
        @SerializedName("OS_AskOTPOnPhoneBooking")
        @Expose
        private Integer oSAskOTPOnPhoneBooking;
        @SerializedName("OS_AllowModifyCharges")
        @Expose
        private Integer oSAllowModifyCharges;
        @SerializedName("OS_ISAskBirthDate")
        @Expose
        private Integer oSISAskBirthDate;
        @SerializedName("OS_StartYear")
        @Expose
        private Integer oSStartYear;
        @SerializedName("OS_EndYear")
        @Expose
        private Integer oSEndYear;
        @SerializedName("OS_DoNotFillAgentNamePhoneInExe")
        @Expose
        private Integer oSDoNotFillAgentNamePhoneInExe;
        @SerializedName("OS_AskOTPOnModifyBooking")
        @Expose
        private Integer oSAskOTPOnModifyBooking;
        @SerializedName("OS_ApplyEnRouteQuota")
        @Expose
        private Integer oSApplyEnRouteQuota;
        @SerializedName("OS_DisplayBookSeatFareOnChart")
        @Expose
        private Integer oSDisplayBookSeatFareOnChart;
        @SerializedName("OS_IsBranchPNRModifyCharges")
        @Expose
        private Integer oSIsBranchPNRModifyCharges;
        @SerializedName("OS_PRINTQRCodeInTicket")
        @Expose
        private Integer oSPRINTQRCodeInTicket;
        @SerializedName("OS_PrintGSTInvoice")
        @Expose
        private Integer oSPrintGSTInvoice;
        @SerializedName("OS_ESendGSTInvoice")
        @Expose
        private Integer oSESendGSTInvoice;
        @SerializedName("OS_IsCheckDiscount")
        @Expose
        private Integer oSIsCheckDiscount;
        @SerializedName("OS_DisplayTotalFareOnChart")
        @Expose
        private Integer oSDisplayTotalFareOnChart;
        @SerializedName("OS_IsMultiViewColorSet")
        @Expose
        private Integer oSIsMultiViewColorSet;
        @SerializedName("OS_MultiViewColorCode")
        @Expose
        private String oSMultiViewColorCode;
        @SerializedName("OS_SetDefaultBrowser")
        @Expose
        private String oSSetDefaultBrowser;
        @SerializedName("OS_NotDisplayUBLBOnChart")
        @Expose
        private Integer oSNotDisplayUBLBOnChart;
        @SerializedName("OS_AskOTPOnBankCardBooking")
        @Expose
        private Integer oSAskOTPOnBankCardBooking;
        @SerializedName("OS_GetExtraValueOnFare")
        @Expose
        private Integer oSGetExtraValueOnFare;
        @SerializedName("OS_InsuranceBranch")
        @Expose
        private Integer oSInsuranceBranch;
        @SerializedName("OS_InsuranceLogin")
        @Expose
        private Integer oSInsuranceLogin;
        @SerializedName("OS_InsuranceOffline")
        @Expose
        private Integer oSInsuranceOffline;
        @SerializedName("OS_InsuranceAmount")
        @Expose
        private Integer oSInsuranceAmount;
        @SerializedName("AllowToRechargeFromPayTM")
        @Expose
        private Integer allowToRechargeFromPayTM;
        @SerializedName("OS_AgeRequired")
        @Expose
        private Integer oSAgeRequired;
        @SerializedName("CM_IsSTBankCard")
        @Expose
        private Integer cMIsSTBankCard;
        @SerializedName("CM_IsSTITSPLWallet")
        @Expose
        private Integer cMIsSTITSPLWallet;
        @SerializedName("CM_IsSTRemotePayment")
        @Expose
        private Integer cMIsSTRemotePayment;
        @SerializedName("OS_IsAskIDProof")
        @Expose
        private Integer oSIsAskIDProof;
        @SerializedName("OS_IsIDProofMandatory")
        @Expose
        private Integer oSIsIDProofMandatory;
        @SerializedName("OS_IsCrossStateIDProofMandatory")
        @Expose
        private Integer oSIsCrossStateIDProofMandatory;
        @SerializedName("FemaleSeatHalfColor")
        @Expose
        private String femaleSeatHalfColor;
        @SerializedName("CDH_IsCallCenterAPI")
        @Expose
        private Integer cDHIsCallCenterAPI;

        public Integer getCDHRouteBeforeMin() {
            return cDHRouteBeforeMin;
        }

        public void setCDHRouteBeforeMin(Integer cDHRouteBeforeMin) {
            this.cDHRouteBeforeMin = cDHRouteBeforeMin;
        }

        public Integer getCDHRouteBeforeMinAgent() {
            return cDHRouteBeforeMinAgent;
        }

        public void setCDHRouteBeforeMinAgent(Integer cDHRouteBeforeMinAgent) {
            this.cDHRouteBeforeMinAgent = cDHRouteBeforeMinAgent;
        }

        public Integer getCDHBackDateShow() {
            return cDHBackDateShow;
        }

        public void setCDHBackDateShow(Integer cDHBackDateShow) {
            this.cDHBackDateShow = cDHBackDateShow;
        }

        public Integer getCDHAdvanceDateShow() {
            return cDHAdvanceDateShow;
        }

        public void setCDHAdvanceDateShow(Integer cDHAdvanceDateShow) {
            this.cDHAdvanceDateShow = cDHAdvanceDateShow;
        }

        public Integer getCDHShowStopBooking() {
            return cDHShowStopBooking;
        }

        public void setCDHShowStopBooking(Integer cDHShowStopBooking) {
            this.cDHShowStopBooking = cDHShowStopBooking;
        }

        public Integer getCDHRouteTimeCode() {
            return cDHRouteTimeCode;
        }

        public void setCDHRouteTimeCode(Integer cDHRouteTimeCode) {
            this.cDHRouteTimeCode = cDHRouteTimeCode;
        }

        public Integer getCDHAgentCode() {
            return cDHAgentCode;
        }

        public void setCDHAgentCode(Integer cDHAgentCode) {
            this.cDHAgentCode = cDHAgentCode;
        }

        public Integer getCMIsSMSActive() {
            return cMIsSMSActive;
        }

        public void setCMIsSMSActive(Integer cMIsSMSActive) {
            this.cMIsSMSActive = cMIsSMSActive;
        }

        public Integer getOSAOnChartSMS() {
            return oSAOnChartSMS;
        }

        public void setOSAOnChartSMS(Integer oSAOnChartSMS) {
            this.oSAOnChartSMS = oSAOnChartSMS;
        }

        public Integer getCDHAutoDeletePhoneTime() {
            return cDHAutoDeletePhoneTime;
        }

        public void setCDHAutoDeletePhoneTime(Integer cDHAutoDeletePhoneTime) {
            this.cDHAutoDeletePhoneTime = cDHAutoDeletePhoneTime;
        }

        public Integer getCDHAutoDeleteHour() {
            return cDHAutoDeleteHour;
        }

        public void setCDHAutoDeleteHour(Integer cDHAutoDeleteHour) {
            this.cDHAutoDeleteHour = cDHAutoDeleteHour;
        }

        public Integer getCDHAutoDeleteMinute() {
            return cDHAutoDeleteMinute;
        }

        public void setCDHAutoDeleteMinute(Integer cDHAutoDeleteMinute) {
            this.cDHAutoDeleteMinute = cDHAutoDeleteMinute;
        }

        public Integer getOSDisplayNetRate() {
            return oSDisplayNetRate;
        }

        public void setOSDisplayNetRate(Integer oSDisplayNetRate) {
            this.oSDisplayNetRate = oSDisplayNetRate;
        }

        public Integer getOSCheckLadiesSeat() {
            return oSCheckLadiesSeat;
        }

        public void setOSCheckLadiesSeat(Integer oSCheckLadiesSeat) {
            this.oSCheckLadiesSeat = oSCheckLadiesSeat;
        }

        public Integer getOSLadiesSeatBesideMale() {
            return oSLadiesSeatBesideMale;
        }

        public void setOSLadiesSeatBesideMale(Integer oSLadiesSeatBesideMale) {
            this.oSLadiesSeatBesideMale = oSLadiesSeatBesideMale;
        }

        public Integer getOSShowAvailability() {
            return oSShowAvailability;
        }

        public void setOSShowAvailability(Integer oSShowAvailability) {
            this.oSShowAvailability = oSShowAvailability;
        }

        public Integer getOSFocusOnRate() {
            return oSFocusOnRate;
        }

        public void setOSFocusOnRate(Integer oSFocusOnRate) {
            this.oSFocusOnRate = oSFocusOnRate;
        }

        public Integer getOSEmailConfirm() {
            return oSEmailConfirm;
        }

        public void setOSEmailConfirm(Integer oSEmailConfirm) {
            this.oSEmailConfirm = oSEmailConfirm;
        }

        public Integer getOSEmailPhone() {
            return oSEmailPhone;
        }

        public void setOSEmailPhone(Integer oSEmailPhone) {
            this.oSEmailPhone = oSEmailPhone;
        }

        public Integer getOSEmailAgent() {
            return oSEmailAgent;
        }

        public void setOSEmailAgent(Integer oSEmailAgent) {
            this.oSEmailAgent = oSEmailAgent;
        }

        public Integer getOSEmailBranch() {
            return oSEmailBranch;
        }

        public void setOSEmailBranch(Integer oSEmailBranch) {
            this.oSEmailBranch = oSEmailBranch;
        }

        public Integer getOSEmailGuest() {
            return oSEmailGuest;
        }

        public void setOSEmailGuest(Integer oSEmailGuest) {
            this.oSEmailGuest = oSEmailGuest;
        }

        public Integer getOSEmailBankCard() {
            return oSEmailBankCard;
        }

        public void setOSEmailBankCard(Integer oSEmailBankCard) {
            this.oSEmailBankCard = oSEmailBankCard;
        }

        public Integer getOSEmailCompanyCard() {
            return oSEmailCompanyCard;
        }

        public void setOSEmailCompanyCard(Integer oSEmailCompanyCard) {
            this.oSEmailCompanyCard = oSEmailCompanyCard;
        }

        public Integer getOSEmailWaiting() {
            return oSEmailWaiting;
        }

        public void setOSEmailWaiting(Integer oSEmailWaiting) {
            this.oSEmailWaiting = oSEmailWaiting;
        }

        public Integer getOSEmailAgentQuata() {
            return oSEmailAgentQuata;
        }

        public void setOSEmailAgentQuata(Integer oSEmailAgentQuata) {
            this.oSEmailAgentQuata = oSEmailAgentQuata;
        }

        public Integer getOSEmailOpen() {
            return oSEmailOpen;
        }

        public void setOSEmailOpen(Integer oSEmailOpen) {
            this.oSEmailOpen = oSEmailOpen;
        }

        public Integer getOSSMSConfirm() {
            return oSSMSConfirm;
        }

        public void setOSSMSConfirm(Integer oSSMSConfirm) {
            this.oSSMSConfirm = oSSMSConfirm;
        }

        public Integer getOSSMSPhone() {
            return oSSMSPhone;
        }

        public void setOSSMSPhone(Integer oSSMSPhone) {
            this.oSSMSPhone = oSSMSPhone;
        }

        public Integer getOSSMSAgent() {
            return oSSMSAgent;
        }

        public void setOSSMSAgent(Integer oSSMSAgent) {
            this.oSSMSAgent = oSSMSAgent;
        }

        public Integer getOSSMSBranch() {
            return oSSMSBranch;
        }

        public void setOSSMSBranch(Integer oSSMSBranch) {
            this.oSSMSBranch = oSSMSBranch;
        }

        public Integer getOSSMSGuest() {
            return oSSMSGuest;
        }

        public void setOSSMSGuest(Integer oSSMSGuest) {
            this.oSSMSGuest = oSSMSGuest;
        }

        public Integer getOSSMSBankCard() {
            return oSSMSBankCard;
        }

        public void setOSSMSBankCard(Integer oSSMSBankCard) {
            this.oSSMSBankCard = oSSMSBankCard;
        }

        public Integer getOSSMSCompanyCard() {
            return oSSMSCompanyCard;
        }

        public void setOSSMSCompanyCard(Integer oSSMSCompanyCard) {
            this.oSSMSCompanyCard = oSSMSCompanyCard;
        }

        public Integer getOSSMSWaiting() {
            return oSSMSWaiting;
        }

        public void setOSSMSWaiting(Integer oSSMSWaiting) {
            this.oSSMSWaiting = oSSMSWaiting;
        }

        public Integer getOSSMSAgentQuata() {
            return oSSMSAgentQuata;
        }

        public void setOSSMSAgentQuata(Integer oSSMSAgentQuata) {
            this.oSSMSAgentQuata = oSSMSAgentQuata;
        }

        public Integer getOSSMSOpen() {
            return oSSMSOpen;
        }

        public void setOSSMSOpen(Integer oSSMSOpen) {
            this.oSSMSOpen = oSSMSOpen;
        }

        public Integer getOSEmailGeneralAgent() {
            return oSEmailGeneralAgent;
        }

        public void setOSEmailGeneralAgent(Integer oSEmailGeneralAgent) {
            this.oSEmailGeneralAgent = oSEmailGeneralAgent;
        }

        public Integer getOSEmailOnlineAgent() {
            return oSEmailOnlineAgent;
        }

        public void setOSEmailOnlineAgent(Integer oSEmailOnlineAgent) {
            this.oSEmailOnlineAgent = oSEmailOnlineAgent;
        }

        public Integer getOSEmailAPIAgent() {
            return oSEmailAPIAgent;
        }

        public void setOSEmailAPIAgent(Integer oSEmailAPIAgent) {
            this.oSEmailAPIAgent = oSEmailAPIAgent;
        }

        public Integer getOSEmailB2CAgent() {
            return oSEmailB2CAgent;
        }

        public void setOSEmailB2CAgent(Integer oSEmailB2CAgent) {
            this.oSEmailB2CAgent = oSEmailB2CAgent;
        }

        public Integer getOSSMSGeneralAgent() {
            return oSSMSGeneralAgent;
        }

        public void setOSSMSGeneralAgent(Integer oSSMSGeneralAgent) {
            this.oSSMSGeneralAgent = oSSMSGeneralAgent;
        }

        public Integer getOSSMSOnlineAgent() {
            return oSSMSOnlineAgent;
        }

        public void setOSSMSOnlineAgent(Integer oSSMSOnlineAgent) {
            this.oSSMSOnlineAgent = oSSMSOnlineAgent;
        }

        public Integer getOSSMSAPIAgent() {
            return oSSMSAPIAgent;
        }

        public void setOSSMSAPIAgent(Integer oSSMSAPIAgent) {
            this.oSSMSAPIAgent = oSSMSAPIAgent;
        }

        public Integer getOSSMSB2CAgent() {
            return oSSMSB2CAgent;
        }

        public void setOSSMSB2CAgent(Integer oSSMSB2CAgent) {
            this.oSSMSB2CAgent = oSSMSB2CAgent;
        }

        public Integer getOSStaticData() {
            return oSStaticData;
        }

        public void setOSStaticData(Integer oSStaticData) {
            this.oSStaticData = oSStaticData;
        }

        public Integer getOSPDFConfirm() {
            return oSPDFConfirm;
        }

        public void setOSPDFConfirm(Integer oSPDFConfirm) {
            this.oSPDFConfirm = oSPDFConfirm;
        }

        public Integer getOSPDFPhone() {
            return oSPDFPhone;
        }

        public void setOSPDFPhone(Integer oSPDFPhone) {
            this.oSPDFPhone = oSPDFPhone;
        }

        public Integer getOSPDFGeneralAgent() {
            return oSPDFGeneralAgent;
        }

        public void setOSPDFGeneralAgent(Integer oSPDFGeneralAgent) {
            this.oSPDFGeneralAgent = oSPDFGeneralAgent;
        }

        public Integer getOSPDFOnlineAgent() {
            return oSPDFOnlineAgent;
        }

        public void setOSPDFOnlineAgent(Integer oSPDFOnlineAgent) {
            this.oSPDFOnlineAgent = oSPDFOnlineAgent;
        }

        public Integer getOSPDFAPIAgent() {
            return oSPDFAPIAgent;
        }

        public void setOSPDFAPIAgent(Integer oSPDFAPIAgent) {
            this.oSPDFAPIAgent = oSPDFAPIAgent;
        }

        public Integer getOSPDFB2CAgent() {
            return oSPDFB2CAgent;
        }

        public void setOSPDFB2CAgent(Integer oSPDFB2CAgent) {
            this.oSPDFB2CAgent = oSPDFB2CAgent;
        }

        public Integer getOSPDFBranch() {
            return oSPDFBranch;
        }

        public void setOSPDFBranch(Integer oSPDFBranch) {
            this.oSPDFBranch = oSPDFBranch;
        }

        public Integer getOSPDFGuest() {
            return oSPDFGuest;
        }

        public void setOSPDFGuest(Integer oSPDFGuest) {
            this.oSPDFGuest = oSPDFGuest;
        }

        public Integer getOSPDFBankCard() {
            return oSPDFBankCard;
        }

        public void setOSPDFBankCard(Integer oSPDFBankCard) {
            this.oSPDFBankCard = oSPDFBankCard;
        }

        public Integer getOSPDFCompanyCard() {
            return oSPDFCompanyCard;
        }

        public void setOSPDFCompanyCard(Integer oSPDFCompanyCard) {
            this.oSPDFCompanyCard = oSPDFCompanyCard;
        }

        public Integer getOSPDFAgentQuata() {
            return oSPDFAgentQuata;
        }

        public void setOSPDFAgentQuata(Integer oSPDFAgentQuata) {
            this.oSPDFAgentQuata = oSPDFAgentQuata;
        }

        public Integer getOSReprintGeneralAgent() {
            return oSReprintGeneralAgent;
        }

        public void setOSReprintGeneralAgent(Integer oSReprintGeneralAgent) {
            this.oSReprintGeneralAgent = oSReprintGeneralAgent;
        }

        public Integer getOSHoldBranch() {
            return oSHoldBranch;
        }

        public void setOSHoldBranch(Integer oSHoldBranch) {
            this.oSHoldBranch = oSHoldBranch;
        }

        public Integer getOSHoldCompanyCard() {
            return oSHoldCompanyCard;
        }

        public void setOSHoldCompanyCard(Integer oSHoldCompanyCard) {
            this.oSHoldCompanyCard = oSHoldCompanyCard;
        }

        public Integer getOSHoldBankCard() {
            return oSHoldBankCard;
        }

        public void setOSHoldBankCard(Integer oSHoldBankCard) {
            this.oSHoldBankCard = oSHoldBankCard;
        }

        public Integer getOSHoldGeneralAgent() {
            return oSHoldGeneralAgent;
        }

        public void setOSHoldGeneralAgent(Integer oSHoldGeneralAgent) {
            this.oSHoldGeneralAgent = oSHoldGeneralAgent;
        }

        public Integer getOSHoldAgentQuota() {
            return oSHoldAgentQuota;
        }

        public void setOSHoldAgentQuota(Integer oSHoldAgentQuota) {
            this.oSHoldAgentQuota = oSHoldAgentQuota;
        }

        public Integer getOSHoldGuest() {
            return oSHoldGuest;
        }

        public void setOSHoldGuest(Integer oSHoldGuest) {
            this.oSHoldGuest = oSHoldGuest;
        }

        public Integer getOSBSPrintButton() {
            return oSBSPrintButton;
        }

        public void setOSBSPrintButton(Integer oSBSPrintButton) {
            this.oSBSPrintButton = oSBSPrintButton;
        }

        public Integer getOSFemaleSeatInBirth() {
            return oSFemaleSeatInBirth;
        }

        public void setOSFemaleSeatInBirth(Integer oSFemaleSeatInBirth) {
            this.oSFemaleSeatInBirth = oSFemaleSeatInBirth;
        }

        public String getCDHChartIdleMins() {
            return cDHChartIdleMins;
        }

        public void setCDHChartIdleMins(String cDHChartIdleMins) {
            this.cDHChartIdleMins = cDHChartIdleMins;
        }

        public Integer getOSIsPendingAmount() {
            return oSIsPendingAmount;
        }

        public void setOSIsPendingAmount(Integer oSIsPendingAmount) {
            this.oSIsPendingAmount = oSIsPendingAmount;
        }

        public Integer getOSRefreshChartMin() {
            return oSRefreshChartMin;
        }

        public void setOSRefreshChartMin(Integer oSRefreshChartMin) {
            this.oSRefreshChartMin = oSRefreshChartMin;
        }

        public Integer getOSAllowAutoF5() {
            return oSAllowAutoF5;
        }

        public void setOSAllowAutoF5(Integer oSAllowAutoF5) {
            this.oSAllowAutoF5 = oSAllowAutoF5;
        }

        public Integer getOSAutoF5Min() {
            return oSAutoF5Min;
        }

        public void setOSAutoF5Min(Integer oSAutoF5Min) {
            this.oSAutoF5Min = oSAutoF5Min;
        }

        public Integer getOSAllowAutoChartRefresh() {
            return oSAllowAutoChartRefresh;
        }

        public void setOSAllowAutoChartRefresh(Integer oSAllowAutoChartRefresh) {
            this.oSAllowAutoChartRefresh = oSAllowAutoChartRefresh;
        }

        public Integer getOSTicketPrintCancel() {
            return oSTicketPrintCancel;
        }

        public void setOSTicketPrintCancel(Integer oSTicketPrintCancel) {
            this.oSTicketPrintCancel = oSTicketPrintCancel;
        }

        public Integer getOSIsHighlightMF() {
            return oSIsHighlightMF;
        }

        public void setOSIsHighlightMF(Integer oSIsHighlightMF) {
            this.oSIsHighlightMF = oSIsHighlightMF;
        }

        public String getOSAdlabsURL() {
            return oSAdlabsURL;
        }

        public void setOSAdlabsURL(String oSAdlabsURL) {
            this.oSAdlabsURL = oSAdlabsURL;
        }

        public Integer getOSIsLabelDisplay() {
            return oSIsLabelDisplay;
        }

        public void setOSIsLabelDisplay(Integer oSIsLabelDisplay) {
            this.oSIsLabelDisplay = oSIsLabelDisplay;
        }

        public String getOSLableText() {
            return oSLableText;
        }

        public void setOSLableText(String oSLableText) {
            this.oSLableText = oSLableText;
        }

        public Integer getOSIsSeatWiseFare() {
            return oSIsSeatWiseFare;
        }

        public void setOSIsSeatWiseFare(Integer oSIsSeatWiseFare) {
            this.oSIsSeatWiseFare = oSIsSeatWiseFare;
        }

        public Integer getOSIsChangePassword() {
            return oSIsChangePassword;
        }

        public void setOSIsChangePassword(Integer oSIsChangePassword) {
            this.oSIsChangePassword = oSIsChangePassword;
        }

        public Integer getOSAllowToAskBusSchedule() {
            return oSAllowToAskBusSchedule;
        }

        public void setOSAllowToAskBusSchedule(Integer oSAllowToAskBusSchedule) {
            this.oSAllowToAskBusSchedule = oSAllowToAskBusSchedule;
        }

        public Integer getOSIsCheckAgentCredit() {
            return oSIsCheckAgentCredit;
        }

        public void setOSIsCheckAgentCredit(Integer oSIsCheckAgentCredit) {
            this.oSIsCheckAgentCredit = oSIsCheckAgentCredit;
        }

        public Integer getOSIsScreenSaver() {
            return oSIsScreenSaver;
        }

        public void setOSIsScreenSaver(Integer oSIsScreenSaver) {
            this.oSIsScreenSaver = oSIsScreenSaver;
        }

        public Integer getCMIsACServiceTax() {
            return cMIsACServiceTax;
        }

        public void setCMIsACServiceTax(Integer cMIsACServiceTax) {
            this.cMIsACServiceTax = cMIsACServiceTax;
        }

        public Integer getCMACFareIncludeTax() {
            return cMACFareIncludeTax;
        }

        public void setCMACFareIncludeTax(Integer cMACFareIncludeTax) {
            this.cMACFareIncludeTax = cMACFareIncludeTax;
        }

        public Double getCMACServiceTax() {
            return cMACServiceTax;
        }

        public void setCMACServiceTax(Double cMACServiceTax) {
            this.cMACServiceTax = cMACServiceTax;
        }

        public Integer getCMIsNonACServiceTax() {
            return cMIsNonACServiceTax;
        }

        public void setCMIsNonACServiceTax(Integer cMIsNonACServiceTax) {
            this.cMIsNonACServiceTax = cMIsNonACServiceTax;
        }

        public Integer getCMNonACFareIncludeTax() {
            return cMNonACFareIncludeTax;
        }

        public void setCMNonACFareIncludeTax(Integer cMNonACFareIncludeTax) {
            this.cMNonACFareIncludeTax = cMNonACFareIncludeTax;
        }

        public Double getCMNonACServiceTax() {
            return cMNonACServiceTax;
        }

        public void setCMNonACServiceTax(Double cMNonACServiceTax) {
            this.cMNonACServiceTax = cMNonACServiceTax;
        }

        public Integer getCMIsSTOffline() {
            return cMIsSTOffline;
        }

        public void setCMIsSTOffline(Integer cMIsSTOffline) {
            this.cMIsSTOffline = cMIsSTOffline;
        }

        public Integer getCMIsSTLogin() {
            return cMIsSTLogin;
        }

        public void setCMIsSTLogin(Integer cMIsSTLogin) {
            this.cMIsSTLogin = cMIsSTLogin;
        }

        public Integer getCMIsSTAPI() {
            return cMIsSTAPI;
        }

        public void setCMIsSTAPI(Integer cMIsSTAPI) {
            this.cMIsSTAPI = cMIsSTAPI;
        }

        public Integer getCMIsSTB2C() {
            return cMIsSTB2C;
        }

        public void setCMIsSTB2C(Integer cMIsSTB2C) {
            this.cMIsSTB2C = cMIsSTB2C;
        }

        public Integer getCMIsSTBranch() {
            return cMIsSTBranch;
        }

        public void setCMIsSTBranch(Integer cMIsSTBranch) {
            this.cMIsSTBranch = cMIsSTBranch;
        }

        public Integer getOSScreenSaverMinute() {
            return oSScreenSaverMinute;
        }

        public void setOSScreenSaverMinute(Integer oSScreenSaverMinute) {
            this.oSScreenSaverMinute = oSScreenSaverMinute;
        }

        public Integer getCDHAutoDeletePhoneBookMinutes() {
            return cDHAutoDeletePhoneBookMinutes;
        }

        public void setCDHAutoDeletePhoneBookMinutes(Integer cDHAutoDeletePhoneBookMinutes) {
            this.cDHAutoDeletePhoneBookMinutes = cDHAutoDeletePhoneBookMinutes;
        }

        public Integer getOSIsFocusOnPickup() {
            return oSIsFocusOnPickup;
        }

        public void setOSIsFocusOnPickup(Integer oSIsFocusOnPickup) {
            this.oSIsFocusOnPickup = oSIsFocusOnPickup;
        }

        public String getMyBookingURL() {
            return myBookingURL;
        }

        public void setMyBookingURL(String myBookingURL) {
            this.myBookingURL = myBookingURL;
        }

        public Integer getCMServiceTaxRoundUp() {
            return cMServiceTaxRoundUp;
        }

        public void setCMServiceTaxRoundUp(Integer cMServiceTaxRoundUp) {
            this.cMServiceTaxRoundUp = cMServiceTaxRoundUp;
        }

        public Double getCMACPackageServiceTax() {
            return cMACPackageServiceTax;
        }

        public void setCMACPackageServiceTax(Double cMACPackageServiceTax) {
            this.cMACPackageServiceTax = cMACPackageServiceTax;
        }

        public Double getCMNonACPackageServiceTax() {
            return cMNonACPackageServiceTax;
        }

        public void setCMNonACPackageServiceTax(Double cMNonACPackageServiceTax) {
            this.cMNonACPackageServiceTax = cMNonACPackageServiceTax;
        }

        public Double getCMACOwnScheduleServiceTax() {
            return cMACOwnScheduleServiceTax;
        }

        public void setCMACOwnScheduleServiceTax(Double cMACOwnScheduleServiceTax) {
            this.cMACOwnScheduleServiceTax = cMACOwnScheduleServiceTax;
        }

        public Double getCMNonACOwnScheduleServiceTax() {
            return cMNonACOwnScheduleServiceTax;
        }

        public void setCMNonACOwnScheduleServiceTax(Double cMNonACOwnScheduleServiceTax) {
            this.cMNonACOwnScheduleServiceTax = cMNonACOwnScheduleServiceTax;
        }

        public Integer getOSAllowToAskAgentRefund() {
            return oSAllowToAskAgentRefund;
        }

        public void setOSAllowToAskAgentRefund(Integer oSAllowToAskAgentRefund) {
            this.oSAllowToAskAgentRefund = oSAllowToAskAgentRefund;
        }

        public Integer getOSAllowCoupanCode() {
            return oSAllowCoupanCode;
        }

        public void setOSAllowCoupanCode(Integer oSAllowCoupanCode) {
            this.oSAllowCoupanCode = oSAllowCoupanCode;
        }

        public Integer getOSAllowToShowPastBooking() {
            return oSAllowToShowPastBooking;
        }

        public void setOSAllowToShowPastBooking(Integer oSAllowToShowPastBooking) {
            this.oSAllowToShowPastBooking = oSAllowToShowPastBooking;
        }

        public Integer getOSIsBranchBookingAsConfirm() {
            return oSIsBranchBookingAsConfirm;
        }

        public void setOSIsBranchBookingAsConfirm(Integer oSIsBranchBookingAsConfirm) {
            this.oSIsBranchBookingAsConfirm = oSIsBranchBookingAsConfirm;
        }

        public Integer getOSIsReturnPhoneBookBranchCityOnly() {
            return oSIsReturnPhoneBookBranchCityOnly;
        }

        public void setOSIsReturnPhoneBookBranchCityOnly(Integer oSIsReturnPhoneBookBranchCityOnly) {
            this.oSIsReturnPhoneBookBranchCityOnly = oSIsReturnPhoneBookBranchCityOnly;
        }

        public Integer getOSMaxCompanyCardBooking() {
            return oSMaxCompanyCardBooking;
        }

        public void setOSMaxCompanyCardBooking(Integer oSMaxCompanyCardBooking) {
            this.oSMaxCompanyCardBooking = oSMaxCompanyCardBooking;
        }

        public Integer getOSDisplayAllFareBox() {
            return oSDisplayAllFareBox;
        }

        public void setOSDisplayAllFareBox(Integer oSDisplayAllFareBox) {
            this.oSDisplayAllFareBox = oSDisplayAllFareBox;
        }

        public Integer getOSAllowToSetAmenities() {
            return oSAllowToSetAmenities;
        }

        public void setOSAllowToSetAmenities(Integer oSAllowToSetAmenities) {
            this.oSAllowToSetAmenities = oSAllowToSetAmenities;
        }

        public Integer getOSLastLocationByBusNo() {
            return oSLastLocationByBusNo;
        }

        public void setOSLastLocationByBusNo(Integer oSLastLocationByBusNo) {
            this.oSLastLocationByBusNo = oSLastLocationByBusNo;
        }

        public Integer getOSEmailOnlineWallet() {
            return oSEmailOnlineWallet;
        }

        public void setOSEmailOnlineWallet(Integer oSEmailOnlineWallet) {
            this.oSEmailOnlineWallet = oSEmailOnlineWallet;
        }

        public Integer getOSPDFOnlineWallet() {
            return oSPDFOnlineWallet;
        }

        public void setOSPDFOnlineWallet(Integer oSPDFOnlineWallet) {
            this.oSPDFOnlineWallet = oSPDFOnlineWallet;
        }

        public Integer getOSCheckMaleSeatBesideFemaleInExe() {
            return oSCheckMaleSeatBesideFemaleInExe;
        }

        public void setOSCheckMaleSeatBesideFemaleInExe(Integer oSCheckMaleSeatBesideFemaleInExe) {
            this.oSCheckMaleSeatBesideFemaleInExe = oSCheckMaleSeatBesideFemaleInExe;
        }

        public Integer getOSIsShowAgentPendingAmount() {
            return oSIsShowAgentPendingAmount;
        }

        public void setOSIsShowAgentPendingAmount(Integer oSIsShowAgentPendingAmount) {
            this.oSIsShowAgentPendingAmount = oSIsShowAgentPendingAmount;
        }

        public Integer getISCountTableInBooked() {
            return iSCountTableInBooked;
        }

        public void setISCountTableInBooked(Integer iSCountTableInBooked) {
            this.iSCountTableInBooked = iSCountTableInBooked;
        }

        public Integer getOSIsRemotePaymentTDR() {
            return oSIsRemotePaymentTDR;
        }

        public void setOSIsRemotePaymentTDR(Integer oSIsRemotePaymentTDR) {
            this.oSIsRemotePaymentTDR = oSIsRemotePaymentTDR;
        }

        public Double getOSRemotePaymentTDRCharge() {
            return oSRemotePaymentTDRCharge;
        }

        public void setOSRemotePaymentTDRCharge(Double oSRemotePaymentTDRCharge) {
            this.oSRemotePaymentTDRCharge = oSRemotePaymentTDRCharge;
        }

        public Integer getOSIsIncludeRemotePaymentTDR() {
            return oSIsIncludeRemotePaymentTDR;
        }

        public void setOSIsIncludeRemotePaymentTDR(Integer oSIsIncludeRemotePaymentTDR) {
            this.oSIsIncludeRemotePaymentTDR = oSIsIncludeRemotePaymentTDR;
        }

        public Double getCMACOtherOwnScheduleServiceTax() {
            return cMACOtherOwnScheduleServiceTax;
        }

        public void setCMACOtherOwnScheduleServiceTax(Double cMACOtherOwnScheduleServiceTax) {
            this.cMACOtherOwnScheduleServiceTax = cMACOtherOwnScheduleServiceTax;
        }

        public Double getCMNonACOtherOwnScheduleServiceTax() {
            return cMNonACOtherOwnScheduleServiceTax;
        }

        public void setCMNonACOtherOwnScheduleServiceTax(Double cMNonACOtherOwnScheduleServiceTax) {
            this.cMNonACOtherOwnScheduleServiceTax = cMNonACOtherOwnScheduleServiceTax;
        }

        public Integer getOSIsFirstYesAndPrint() {
            return oSIsFirstYesAndPrint;
        }

        public void setOSIsFirstYesAndPrint(Integer oSIsFirstYesAndPrint) {
            this.oSIsFirstYesAndPrint = oSIsFirstYesAndPrint;
        }

        public Integer getOSIsHideYesAndPrint() {
            return oSIsHideYesAndPrint;
        }

        public void setOSIsHideYesAndPrint(Integer oSIsHideYesAndPrint) {
            this.oSIsHideYesAndPrint = oSIsHideYesAndPrint;
        }

        public Integer getOSPhoneBookingToRemotePayment() {
            return oSPhoneBookingToRemotePayment;
        }

        public void setOSPhoneBookingToRemotePayment(Integer oSPhoneBookingToRemotePayment) {
            this.oSPhoneBookingToRemotePayment = oSPhoneBookingToRemotePayment;
        }

        public Integer getOSCouponCodeMaxSeatBook() {
            return oSCouponCodeMaxSeatBook;
        }

        public void setOSCouponCodeMaxSeatBook(Integer oSCouponCodeMaxSeatBook) {
            this.oSCouponCodeMaxSeatBook = oSCouponCodeMaxSeatBook;
        }

        public Integer getOSAllowToFetchAllPaxDetail() {
            return oSAllowToFetchAllPaxDetail;
        }

        public void setOSAllowToFetchAllPaxDetail(Integer oSAllowToFetchAllPaxDetail) {
            this.oSAllowToFetchAllPaxDetail = oSAllowToFetchAllPaxDetail;
        }

        public Integer getOSIsApplyGST() {
            return oSIsApplyGST;
        }

        public void setOSIsApplyGST(Integer oSIsApplyGST) {
            this.oSIsApplyGST = oSIsApplyGST;
        }

        public Integer getOSShowRateOnSeat() {
            return oSShowRateOnSeat;
        }

        public void setOSShowRateOnSeat(Integer oSShowRateOnSeat) {
            this.oSShowRateOnSeat = oSShowRateOnSeat;
        }

        public Integer getOSAskOTPOnPhoneBooking() {
            return oSAskOTPOnPhoneBooking;
        }

        public void setOSAskOTPOnPhoneBooking(Integer oSAskOTPOnPhoneBooking) {
            this.oSAskOTPOnPhoneBooking = oSAskOTPOnPhoneBooking;
        }

        public Integer getOSAllowModifyCharges() {
            return oSAllowModifyCharges;
        }

        public void setOSAllowModifyCharges(Integer oSAllowModifyCharges) {
            this.oSAllowModifyCharges = oSAllowModifyCharges;
        }

        public Integer getOSISAskBirthDate() {
            return oSISAskBirthDate;
        }

        public void setOSISAskBirthDate(Integer oSISAskBirthDate) {
            this.oSISAskBirthDate = oSISAskBirthDate;
        }

        public Integer getOSStartYear() {
            return oSStartYear;
        }

        public void setOSStartYear(Integer oSStartYear) {
            this.oSStartYear = oSStartYear;
        }

        public Integer getOSEndYear() {
            return oSEndYear;
        }

        public void setOSEndYear(Integer oSEndYear) {
            this.oSEndYear = oSEndYear;
        }

        public Integer getOSDoNotFillAgentNamePhoneInExe() {
            return oSDoNotFillAgentNamePhoneInExe;
        }

        public void setOSDoNotFillAgentNamePhoneInExe(Integer oSDoNotFillAgentNamePhoneInExe) {
            this.oSDoNotFillAgentNamePhoneInExe = oSDoNotFillAgentNamePhoneInExe;
        }

        public Integer getOSAskOTPOnModifyBooking() {
            return oSAskOTPOnModifyBooking;
        }

        public void setOSAskOTPOnModifyBooking(Integer oSAskOTPOnModifyBooking) {
            this.oSAskOTPOnModifyBooking = oSAskOTPOnModifyBooking;
        }

        public Integer getOSApplyEnRouteQuota() {
            return oSApplyEnRouteQuota;
        }

        public void setOSApplyEnRouteQuota(Integer oSApplyEnRouteQuota) {
            this.oSApplyEnRouteQuota = oSApplyEnRouteQuota;
        }

        public Integer getOSDisplayBookSeatFareOnChart() {
            return oSDisplayBookSeatFareOnChart;
        }

        public void setOSDisplayBookSeatFareOnChart(Integer oSDisplayBookSeatFareOnChart) {
            this.oSDisplayBookSeatFareOnChart = oSDisplayBookSeatFareOnChart;
        }

        public Integer getOSIsBranchPNRModifyCharges() {
            return oSIsBranchPNRModifyCharges;
        }

        public void setOSIsBranchPNRModifyCharges(Integer oSIsBranchPNRModifyCharges) {
            this.oSIsBranchPNRModifyCharges = oSIsBranchPNRModifyCharges;
        }

        public Integer getOSPRINTQRCodeInTicket() {
            return oSPRINTQRCodeInTicket;
        }

        public void setOSPRINTQRCodeInTicket(Integer oSPRINTQRCodeInTicket) {
            this.oSPRINTQRCodeInTicket = oSPRINTQRCodeInTicket;
        }

        public Integer getOSPrintGSTInvoice() {
            return oSPrintGSTInvoice;
        }

        public void setOSPrintGSTInvoice(Integer oSPrintGSTInvoice) {
            this.oSPrintGSTInvoice = oSPrintGSTInvoice;
        }

        public Integer getOSESendGSTInvoice() {
            return oSESendGSTInvoice;
        }

        public void setOSESendGSTInvoice(Integer oSESendGSTInvoice) {
            this.oSESendGSTInvoice = oSESendGSTInvoice;
        }

        public Integer getOSIsCheckDiscount() {
            return oSIsCheckDiscount;
        }

        public void setOSIsCheckDiscount(Integer oSIsCheckDiscount) {
            this.oSIsCheckDiscount = oSIsCheckDiscount;
        }

        public Integer getOSDisplayTotalFareOnChart() {
            return oSDisplayTotalFareOnChart;
        }

        public void setOSDisplayTotalFareOnChart(Integer oSDisplayTotalFareOnChart) {
            this.oSDisplayTotalFareOnChart = oSDisplayTotalFareOnChart;
        }

        public Integer getOSIsMultiViewColorSet() {
            return oSIsMultiViewColorSet;
        }

        public void setOSIsMultiViewColorSet(Integer oSIsMultiViewColorSet) {
            this.oSIsMultiViewColorSet = oSIsMultiViewColorSet;
        }

        public String getOSMultiViewColorCode() {
            return oSMultiViewColorCode;
        }

        public void setOSMultiViewColorCode(String oSMultiViewColorCode) {
            this.oSMultiViewColorCode = oSMultiViewColorCode;
        }

        public String getOSSetDefaultBrowser() {
            return oSSetDefaultBrowser;
        }

        public void setOSSetDefaultBrowser(String oSSetDefaultBrowser) {
            this.oSSetDefaultBrowser = oSSetDefaultBrowser;
        }

        public Integer getOSNotDisplayUBLBOnChart() {
            return oSNotDisplayUBLBOnChart;
        }

        public void setOSNotDisplayUBLBOnChart(Integer oSNotDisplayUBLBOnChart) {
            this.oSNotDisplayUBLBOnChart = oSNotDisplayUBLBOnChart;
        }

        public Integer getOSAskOTPOnBankCardBooking() {
            return oSAskOTPOnBankCardBooking;
        }

        public void setOSAskOTPOnBankCardBooking(Integer oSAskOTPOnBankCardBooking) {
            this.oSAskOTPOnBankCardBooking = oSAskOTPOnBankCardBooking;
        }

        public Integer getOSGetExtraValueOnFare() {
            return oSGetExtraValueOnFare;
        }

        public void setOSGetExtraValueOnFare(Integer oSGetExtraValueOnFare) {
            this.oSGetExtraValueOnFare = oSGetExtraValueOnFare;
        }

        public Integer getOSInsuranceBranch() {
            return oSInsuranceBranch;
        }

        public void setOSInsuranceBranch(Integer oSInsuranceBranch) {
            this.oSInsuranceBranch = oSInsuranceBranch;
        }

        public Integer getOSInsuranceLogin() {
            return oSInsuranceLogin;
        }

        public void setOSInsuranceLogin(Integer oSInsuranceLogin) {
            this.oSInsuranceLogin = oSInsuranceLogin;
        }

        public Integer getOSInsuranceOffline() {
            return oSInsuranceOffline;
        }

        public void setOSInsuranceOffline(Integer oSInsuranceOffline) {
            this.oSInsuranceOffline = oSInsuranceOffline;
        }

        public Integer getOSInsuranceAmount() {
            return oSInsuranceAmount;
        }

        public void setOSInsuranceAmount(Integer oSInsuranceAmount) {
            this.oSInsuranceAmount = oSInsuranceAmount;
        }

        public Integer getAllowToRechargeFromPayTM() {
            return allowToRechargeFromPayTM;
        }

        public void setAllowToRechargeFromPayTM(Integer allowToRechargeFromPayTM) {
            this.allowToRechargeFromPayTM = allowToRechargeFromPayTM;
        }

        public Integer getOSAgeRequired() {
            return oSAgeRequired;
        }

        public void setOSAgeRequired(Integer oSAgeRequired) {
            this.oSAgeRequired = oSAgeRequired;
        }

        public Integer getCMIsSTBankCard() {
            return cMIsSTBankCard;
        }

        public void setCMIsSTBankCard(Integer cMIsSTBankCard) {
            this.cMIsSTBankCard = cMIsSTBankCard;
        }

        public Integer getCMIsSTITSPLWallet() {
            return cMIsSTITSPLWallet;
        }

        public void setCMIsSTITSPLWallet(Integer cMIsSTITSPLWallet) {
            this.cMIsSTITSPLWallet = cMIsSTITSPLWallet;
        }

        public Integer getCMIsSTRemotePayment() {
            return cMIsSTRemotePayment;
        }

        public void setCMIsSTRemotePayment(Integer cMIsSTRemotePayment) {
            this.cMIsSTRemotePayment = cMIsSTRemotePayment;
        }

        public Integer getOSIsAskIDProof() {
            return oSIsAskIDProof;
        }

        public void setOSIsAskIDProof(Integer oSIsAskIDProof) {
            this.oSIsAskIDProof = oSIsAskIDProof;
        }

        public Integer getOSIsIDProofMandatory() {
            return oSIsIDProofMandatory;
        }

        public void setOSIsIDProofMandatory(Integer oSIsIDProofMandatory) {
            this.oSIsIDProofMandatory = oSIsIDProofMandatory;
        }

        public Integer getOSIsCrossStateIDProofMandatory() {
            return oSIsCrossStateIDProofMandatory;
        }

        public void setOSIsCrossStateIDProofMandatory(Integer oSIsCrossStateIDProofMandatory) {
            this.oSIsCrossStateIDProofMandatory = oSIsCrossStateIDProofMandatory;
        }

        public String getFemaleSeatHalfColor() {
            return femaleSeatHalfColor;
        }

        public void setFemaleSeatHalfColor(String femaleSeatHalfColor) {
            this.femaleSeatHalfColor = femaleSeatHalfColor;
        }

        public Integer getCDHIsCallCenterAPI() {
            return cDHIsCallCenterAPI;
        }

        public void setCDHIsCallCenterAPI(Integer cDHIsCallCenterAPI) {
            this.cDHIsCallCenterAPI = cDHIsCallCenterAPI;
        }

    }

    public class XMLKey {

        @SerializedName("XK_id")
        @Expose
        private Integer xKId;
        @SerializedName("Xk_Remarks")
        @Expose
        private String xkRemarks;
        @SerializedName("XK_key")
        @Expose
        private String xKKey;
        @SerializedName("XK_exeversion")
        @Expose
        private String xKExeversion;
        @SerializedName("XK_AssemblyBuild")
        @Expose
        private Integer xKAssemblyBuild;
        @SerializedName("XK_AssemblyRevision")
        @Expose
        private Integer xKAssemblyRevision;
        @SerializedName("XK_ShowError")
        @Expose
        private Integer xKShowError;
        @SerializedName("XK_Test")
        @Expose
        private Integer xKTest;
        @SerializedName("Xk_TestConn")
        @Expose
        private String xkTestConn;
        @SerializedName("Xk_Tracert")
        @Expose
        private String xkTracert;
        @SerializedName("XK_HelpLineNo")
        @Expose
        private String xKHelpLineNo;
        @SerializedName("XK_EXE_Domain")
        @Expose
        private String xKEXEDomain;
        @SerializedName("XK_SMS_Domain")
        @Expose
        private String xKSMSDomain;
        @SerializedName("XK_Email_Domain")
        @Expose
        private String xKEmailDomain;
        @SerializedName("XK_VersionRemarks")
        @Expose
        private String xKVersionRemarks;
        @SerializedName("XK_CopyRightYear")
        @Expose
        private String xKCopyRightYear;
        @SerializedName("XK_InfinityQSVersion")
        @Expose
        private String xKInfinityQSVersion;
        @SerializedName("XK_PrinterVersion")
        @Expose
        private String xKPrinterVersion;
        @SerializedName("WCM_ID")
        @Expose
        private Integer wCMID;
        @SerializedName("WCM_CompanyName")
        @Expose
        private String wCMCompanyName;
        @SerializedName("WCD_WalletKey")
        @Expose
        private String wCDWalletKey;
        @SerializedName("WCD_TDRCharges")
        @Expose
        private Double wCDTDRCharges;

        public Integer getXKId() {
            return xKId;
        }

        public void setXKId(Integer xKId) {
            this.xKId = xKId;
        }

        public String getXkRemarks() {
            return xkRemarks;
        }

        public void setXkRemarks(String xkRemarks) {
            this.xkRemarks = xkRemarks;
        }

        public String getXKKey() {
            return xKKey;
        }

        public void setXKKey(String xKKey) {
            this.xKKey = xKKey;
        }

        public String getXKExeversion() {
            return xKExeversion;
        }

        public void setXKExeversion(String xKExeversion) {
            this.xKExeversion = xKExeversion;
        }

        public Integer getXKAssemblyBuild() {
            return xKAssemblyBuild;
        }

        public void setXKAssemblyBuild(Integer xKAssemblyBuild) {
            this.xKAssemblyBuild = xKAssemblyBuild;
        }

        public Integer getXKAssemblyRevision() {
            return xKAssemblyRevision;
        }

        public void setXKAssemblyRevision(Integer xKAssemblyRevision) {
            this.xKAssemblyRevision = xKAssemblyRevision;
        }

        public Integer getXKShowError() {
            return xKShowError;
        }

        public void setXKShowError(Integer xKShowError) {
            this.xKShowError = xKShowError;
        }

        public Integer getXKTest() {
            return xKTest;
        }

        public void setXKTest(Integer xKTest) {
            this.xKTest = xKTest;
        }

        public String getXkTestConn() {
            return xkTestConn;
        }

        public void setXkTestConn(String xkTestConn) {
            this.xkTestConn = xkTestConn;
        }

        public String getXkTracert() {
            return xkTracert;
        }

        public void setXkTracert(String xkTracert) {
            this.xkTracert = xkTracert;
        }

        public String getXKHelpLineNo() {
            return xKHelpLineNo;
        }

        public void setXKHelpLineNo(String xKHelpLineNo) {
            this.xKHelpLineNo = xKHelpLineNo;
        }

        public String getXKEXEDomain() {
            return xKEXEDomain;
        }

        public void setXKEXEDomain(String xKEXEDomain) {
            this.xKEXEDomain = xKEXEDomain;
        }

        public String getXKSMSDomain() {
            return xKSMSDomain;
        }

        public void setXKSMSDomain(String xKSMSDomain) {
            this.xKSMSDomain = xKSMSDomain;
        }

        public String getXKEmailDomain() {
            return xKEmailDomain;
        }

        public void setXKEmailDomain(String xKEmailDomain) {
            this.xKEmailDomain = xKEmailDomain;
        }

        public String getXKVersionRemarks() {
            return xKVersionRemarks;
        }

        public void setXKVersionRemarks(String xKVersionRemarks) {
            this.xKVersionRemarks = xKVersionRemarks;
        }

        public String getXKCopyRightYear() {
            return xKCopyRightYear;
        }

        public void setXKCopyRightYear(String xKCopyRightYear) {
            this.xKCopyRightYear = xKCopyRightYear;
        }

        public String getXKInfinityQSVersion() {
            return xKInfinityQSVersion;
        }

        public void setXKInfinityQSVersion(String xKInfinityQSVersion) {
            this.xKInfinityQSVersion = xKInfinityQSVersion;
        }

        public String getXKPrinterVersion() {
            return xKPrinterVersion;
        }

        public void setXKPrinterVersion(String xKPrinterVersion) {
            this.xKPrinterVersion = xKPrinterVersion;
        }

        public Integer getWCMID() {
            return wCMID;
        }

        public void setWCMID(Integer wCMID) {
            this.wCMID = wCMID;
        }

        public String getWCMCompanyName() {
            return wCMCompanyName;
        }

        public void setWCMCompanyName(String wCMCompanyName) {
            this.wCMCompanyName = wCMCompanyName;
        }

        public String getWCDWalletKey() {
            return wCDWalletKey;
        }

        public void setWCDWalletKey(String wCDWalletKey) {
            this.wCDWalletKey = wCDWalletKey;
        }

        public Double getWCDTDRCharges() {
            return wCDTDRCharges;
        }

        public void setWCDTDRCharges(Double wCDTDRCharges) {
            this.wCDTDRCharges = wCDTDRCharges;
        }

    }

    public class ITSGetExtraRouteTime {

        @SerializedName("RM_RouteID")
        @Expose
        private String rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private String rTRouteTimeID;
        @SerializedName("DATE")
        @Expose
        private String dATE;
        @SerializedName("RTM_CategoryType")
        @Expose
        private Integer rTMCategoryType;

        public String getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(String rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public String getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(String rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public String getDATE() {
            return dATE;
        }

        public void setDATE(String dATE) {
            this.dATE = dATE;
        }

        public Integer getRTMCategoryType() {
            return rTMCategoryType;
        }

        public void setRTMCategoryType(Integer rTMCategoryType) {
            this.rTMCategoryType = rTMCategoryType;
        }

    }

    public class ITSGetFareCaptionXML {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("FC_ACFare")
        @Expose
        private String fCACFare;
        @SerializedName("FC_NonACFare")
        @Expose
        private String fCNonACFare;
        @SerializedName("FC_ACSeat")
        @Expose
        private String fCACSeat;
        @SerializedName("FC_ACSLP")
        @Expose
        private String fCACSLP;
        @SerializedName("FC_ACSlmb")
        @Expose
        private String fCACSlmb;
        @SerializedName("FC_NonACSeat")
        @Expose
        private String fCNonACSeat;
        @SerializedName("FC_NonACSLP")
        @Expose
        private String fCNonACSLP;
        @SerializedName("FC_NonACSlmb")
        @Expose
        private String fCNonACSlmb;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public String getFCACFare() {
            return fCACFare;
        }

        public void setFCACFare(String fCACFare) {
            this.fCACFare = fCACFare;
        }

        public String getFCNonACFare() {
            return fCNonACFare;
        }

        public void setFCNonACFare(String fCNonACFare) {
            this.fCNonACFare = fCNonACFare;
        }

        public String getFCACSeat() {
            return fCACSeat;
        }

        public void setFCACSeat(String fCACSeat) {
            this.fCACSeat = fCACSeat;
        }

        public String getFCACSLP() {
            return fCACSLP;
        }

        public void setFCACSLP(String fCACSLP) {
            this.fCACSLP = fCACSLP;
        }

        public String getFCACSlmb() {
            return fCACSlmb;
        }

        public void setFCACSlmb(String fCACSlmb) {
            this.fCACSlmb = fCACSlmb;
        }

        public String getFCNonACSeat() {
            return fCNonACSeat;
        }

        public void setFCNonACSeat(String fCNonACSeat) {
            this.fCNonACSeat = fCNonACSeat;
        }

        public String getFCNonACSLP() {
            return fCNonACSLP;
        }

        public void setFCNonACSLP(String fCNonACSLP) {
            this.fCNonACSLP = fCNonACSLP;
        }

        public String getFCNonACSlmb() {
            return fCNonACSlmb;
        }

        public void setFCNonACSlmb(String fCNonACSlmb) {
            this.fCNonACSlmb = fCNonACSlmb;
        }

    }

    public class ITSFetchChartForeColor {

        @SerializedName("ES_ConfColor")
        @Expose
        private String eSConfColor;
        @SerializedName("ES_PhColor")
        @Expose
        private String eSPhColor;
        @SerializedName("ES_AgColor")
        @Expose
        private String eSAgColor;
        @SerializedName("ES_AgQuotaColor")
        @Expose
        private String eSAgQuotaColor;
        @SerializedName("ES_GuestColor")
        @Expose
        private String eSGuestColor;
        @SerializedName("ES_BankC")
        @Expose
        private String eSBankC;
        @SerializedName("ES_CompC")
        @Expose
        private String eSCompC;
        @SerializedName("ES_BranchColor")
        @Expose
        private String eSBranchColor;
        @SerializedName("ES_OnlineAgentColor")
        @Expose
        private String eSOnlineAgentColor;
        @SerializedName("ES_B2CColor")
        @Expose
        private String eSB2CColor;
        @SerializedName("ES_APiColor")
        @Expose
        private String eSAPiColor;
        @SerializedName("ES_OnlineAgentPhoneColor")
        @Expose
        private String eSOnlineAgentPhoneColor;
        @SerializedName("ES_OnlineAgentCardColor")
        @Expose
        private String eSOnlineAgentCardColor;
        @SerializedName("ES_OnlineWallet")
        @Expose
        private String eSOnlineWallet;
        @SerializedName("ES_RemotePayment")
        @Expose
        private String eSRemotePayment;

        public String getESConfColor() {
            return eSConfColor;
        }

        public void setESConfColor(String eSConfColor) {
            this.eSConfColor = eSConfColor;
        }

        public String getESPhColor() {
            return eSPhColor;
        }

        public void setESPhColor(String eSPhColor) {
            this.eSPhColor = eSPhColor;
        }

        public String getESAgColor() {
            return eSAgColor;
        }

        public void setESAgColor(String eSAgColor) {
            this.eSAgColor = eSAgColor;
        }

        public String getESAgQuotaColor() {
            return eSAgQuotaColor;
        }

        public void setESAgQuotaColor(String eSAgQuotaColor) {
            this.eSAgQuotaColor = eSAgQuotaColor;
        }

        public String getESGuestColor() {
            return eSGuestColor;
        }

        public void setESGuestColor(String eSGuestColor) {
            this.eSGuestColor = eSGuestColor;
        }

        public String getESBankC() {
            return eSBankC;
        }

        public void setESBankC(String eSBankC) {
            this.eSBankC = eSBankC;
        }

        public String getESCompC() {
            return eSCompC;
        }

        public void setESCompC(String eSCompC) {
            this.eSCompC = eSCompC;
        }

        public String getESBranchColor() {
            return eSBranchColor;
        }

        public void setESBranchColor(String eSBranchColor) {
            this.eSBranchColor = eSBranchColor;
        }

        public String getESOnlineAgentColor() {
            return eSOnlineAgentColor;
        }

        public void setESOnlineAgentColor(String eSOnlineAgentColor) {
            this.eSOnlineAgentColor = eSOnlineAgentColor;
        }

        public String getESB2CColor() {
            return eSB2CColor;
        }

        public void setESB2CColor(String eSB2CColor) {
            this.eSB2CColor = eSB2CColor;
        }

        public String getESAPiColor() {
            return eSAPiColor;
        }

        public void setESAPiColor(String eSAPiColor) {
            this.eSAPiColor = eSAPiColor;
        }

        public String getESOnlineAgentPhoneColor() {
            return eSOnlineAgentPhoneColor;
        }

        public void setESOnlineAgentPhoneColor(String eSOnlineAgentPhoneColor) {
            this.eSOnlineAgentPhoneColor = eSOnlineAgentPhoneColor;
        }

        public String getESOnlineAgentCardColor() {
            return eSOnlineAgentCardColor;
        }

        public void setESOnlineAgentCardColor(String eSOnlineAgentCardColor) {
            this.eSOnlineAgentCardColor = eSOnlineAgentCardColor;
        }

        public String getESOnlineWallet() {
            return eSOnlineWallet;
        }

        public void setESOnlineWallet(String eSOnlineWallet) {
            this.eSOnlineWallet = eSOnlineWallet;
        }

        public String getESRemotePayment() {
            return eSRemotePayment;
        }

        public void setESRemotePayment(String eSRemotePayment) {
            this.eSRemotePayment = eSRemotePayment;
        }

    }

    public class ITSAutoDeletePhoneBookingAfterBookingTime {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("APB_AutoDeletePhoneBookMinutes")
        @Expose
        private Integer aPBAutoDeletePhoneBookMinutes;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public Integer getAPBAutoDeletePhoneBookMinutes() {
            return aPBAutoDeletePhoneBookMinutes;
        }

        public void setAPBAutoDeletePhoneBookMinutes(Integer aPBAutoDeletePhoneBookMinutes) {
            this.aPBAutoDeletePhoneBookMinutes = aPBAutoDeletePhoneBookMinutes;
        }

    }

    public class ITSBranchUserwisehotroute {

        @SerializedName("srno")
        @Expose
        private Integer srno;
        @SerializedName("BUHR_ID")
        @Expose
        private Integer bUHRID;
        @SerializedName("CM_FromCityID")
        @Expose
        private Integer cMFromCityID;
        @SerializedName("CM_ToCityID")
        @Expose
        private Integer cMToCityID;
        @SerializedName("HotRoute")
        @Expose
        private String hotRoute;

        public Integer getSrno() {
            return srno;
        }

        public void setSrno(Integer srno) {
            this.srno = srno;
        }

        public Integer getBUHRID() {
            return bUHRID;
        }

        public void setBUHRID(Integer bUHRID) {
            this.bUHRID = bUHRID;
        }

        public Integer getCMFromCityID() {
            return cMFromCityID;
        }

        public void setCMFromCityID(Integer cMFromCityID) {
            this.cMFromCityID = cMFromCityID;
        }

        public Integer getCMToCityID() {
            return cMToCityID;
        }

        public void setCMToCityID(Integer cMToCityID) {
            this.cMToCityID = cMToCityID;
        }

        public String getHotRoute() {
            return hotRoute;
        }

        public void setHotRoute(String hotRoute) {
            this.hotRoute = hotRoute;
        }

    }

    public class ITSDoNotAllowFareChangeInBelowRouteTime {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

    }

    public class ITSRouteWiseChargedAmenity {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("AM_ID")
        @Expose
        private Integer aMID;
        @SerializedName("AM_AmenityName")
        @Expose
        private String aMAmenityName;
        @SerializedName("RAM_Rate")
        @Expose
        private Double rAMRate;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public Integer getAMID() {
            return aMID;
        }

        public void setAMID(Integer aMID) {
            this.aMID = aMID;
        }

        public String getAMAmenityName() {
            return aMAmenityName;
        }

        public void setAMAmenityName(String aMAmenityName) {
            this.aMAmenityName = aMAmenityName;
        }

        public Double getRAMRate() {
            return rAMRate;
        }

        public void setRAMRate(Double rAMRate) {
            this.rAMRate = rAMRate;
        }

    }

    @Entity(tableName = "ITSRemarkRePrint")
    public static class ITSRemarkRePrint {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("RM_RemarkType")
        @Expose
        private String rMRemarkType;
        @SerializedName("RM_RemarkName")
        @Expose
        private String rMRemarkName;

        public String getRMRemarkType() {
            return rMRemarkType;
        }

        public void setRMRemarkType(String rMRemarkType) {
            this.rMRemarkType = rMRemarkType;
        }

        public String getRMRemarkName() {
            return rMRemarkName;
        }

        public void setRMRemarkName(String rMRemarkName) {
            this.rMRemarkName = rMRemarkName;
        }

    }

    @Entity(tableName = "ITSState")
    public static class ITSState {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        @SerializedName("SM_StateID")
        @Expose
        private Integer sMStateID;
        @SerializedName("SM_StateName")
        @Expose
        private String sMStateName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getSMStateID() {
            return sMStateID;
        }

        public void setSMStateID(Integer sMStateID) {
            this.sMStateID = sMStateID;
        }

        public String getSMStateName() {
            return sMStateName;
        }

        public void setSMStateName(String sMStateName) {
            this.sMStateName = sMStateName;
        }

    }

    @Entity(tableName = "ITSRouteTimeExtraFareGet")
    public static class ITSRouteTimeExtraFareGet {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("MinPax")
        @Expose
        private Integer minPax;
        @SerializedName("ValueType")
        @Expose
        private Integer valueType;
        @SerializedName("ExtraValue")
        @Expose
        private Double extraValue;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public Integer getMinPax() {
            return minPax;
        }

        public void setMinPax(Integer minPax) {
            this.minPax = minPax;
        }

        public Integer getValueType() {
            return valueType;
        }

        public void setValueType(Integer valueType) {
            this.valueType = valueType;
        }

        public Double getExtraValue() {
            return extraValue;
        }

        public void setExtraValue(Double extraValue) {
            this.extraValue = extraValue;
        }

    }

    public class ITSRouteWiseExtraFareForAgent {

        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("RTM_FromCityID")
        @Expose
        private Integer rTMFromCityID;
        @SerializedName("RTM_ToCityID")
        @Expose
        private Integer rTMToCityID;
        @SerializedName("OABC_ACSeatRate")
        @Expose
        private Double oABCACSeatRate;
        @SerializedName("OABC_ACSleeperRate")
        @Expose
        private Double oABCACSleeperRate;
        @SerializedName("OABC_ACSlumberRate")
        @Expose
        private Double oABCACSlumberRate;
        @SerializedName("OABC_NonACSeatRate")
        @Expose
        private Double oABCNonACSeatRate;
        @SerializedName("OABC_NonACSleeperRate")
        @Expose
        private Double oABCNonACSleeperRate;
        @SerializedName("OABC_NonACSlumberRate")
        @Expose
        private Double oABCNonACSlumberRate;
        @SerializedName("OABC_OnlineAgent")
        @Expose
        private Integer oABCOnlineAgent;
        @SerializedName("OABC_OfflineAgent")
        @Expose
        private Integer oABCOfflineAgent;

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public Integer getRTMFromCityID() {
            return rTMFromCityID;
        }

        public void setRTMFromCityID(Integer rTMFromCityID) {
            this.rTMFromCityID = rTMFromCityID;
        }

        public Integer getRTMToCityID() {
            return rTMToCityID;
        }

        public void setRTMToCityID(Integer rTMToCityID) {
            this.rTMToCityID = rTMToCityID;
        }

        public Double getOABCACSeatRate() {
            return oABCACSeatRate;
        }

        public void setOABCACSeatRate(Double oABCACSeatRate) {
            this.oABCACSeatRate = oABCACSeatRate;
        }

        public Double getOABCACSleeperRate() {
            return oABCACSleeperRate;
        }

        public void setOABCACSleeperRate(Double oABCACSleeperRate) {
            this.oABCACSleeperRate = oABCACSleeperRate;
        }

        public Double getOABCACSlumberRate() {
            return oABCACSlumberRate;
        }

        public void setOABCACSlumberRate(Double oABCACSlumberRate) {
            this.oABCACSlumberRate = oABCACSlumberRate;
        }

        public Double getOABCNonACSeatRate() {
            return oABCNonACSeatRate;
        }

        public void setOABCNonACSeatRate(Double oABCNonACSeatRate) {
            this.oABCNonACSeatRate = oABCNonACSeatRate;
        }

        public Double getOABCNonACSleeperRate() {
            return oABCNonACSleeperRate;
        }

        public void setOABCNonACSleeperRate(Double oABCNonACSleeperRate) {
            this.oABCNonACSleeperRate = oABCNonACSleeperRate;
        }

        public Double getOABCNonACSlumberRate() {
            return oABCNonACSlumberRate;
        }

        public void setOABCNonACSlumberRate(Double oABCNonACSlumberRate) {
            this.oABCNonACSlumberRate = oABCNonACSlumberRate;
        }

        public Integer getOABCOnlineAgent() {
            return oABCOnlineAgent;
        }

        public void setOABCOnlineAgent(Integer oABCOnlineAgent) {
            this.oABCOnlineAgent = oABCOnlineAgent;
        }

        public Integer getOABCOfflineAgent() {
            return oABCOfflineAgent;
        }

        public void setOABCOfflineAgent(Integer oABCOfflineAgent) {
            this.oABCOfflineAgent = oABCOfflineAgent;
        }

    }

    public class ITSRouteTimeInsuranceCharge {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;
        @SerializedName("RTIM_IsBranch")
        @Expose
        private Integer rTIMIsBranch;
        @SerializedName("RTIM_BranchAmount")
        @Expose
        private Integer rTIMBranchAmount;
        @SerializedName("RTIM_IsGeneralAgent")
        @Expose
        private Integer rTIMIsGeneralAgent;
        @SerializedName("RTIM_GeneralAgentAmount")
        @Expose
        private Integer rTIMGeneralAgentAmount;
        @SerializedName("RTIM_IsLoginAgent")
        @Expose
        private Integer rTIMIsLoginAgent;
        @SerializedName("RTIM_LoginAgentAmount")
        @Expose
        private Integer rTIMLoginAgentAmount;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

        public Integer getRTIMIsBranch() {
            return rTIMIsBranch;
        }

        public void setRTIMIsBranch(Integer rTIMIsBranch) {
            this.rTIMIsBranch = rTIMIsBranch;
        }

        public Integer getRTIMBranchAmount() {
            return rTIMBranchAmount;
        }

        public void setRTIMBranchAmount(Integer rTIMBranchAmount) {
            this.rTIMBranchAmount = rTIMBranchAmount;
        }

        public Integer getRTIMIsGeneralAgent() {
            return rTIMIsGeneralAgent;
        }

        public void setRTIMIsGeneralAgent(Integer rTIMIsGeneralAgent) {
            this.rTIMIsGeneralAgent = rTIMIsGeneralAgent;
        }

        public Integer getRTIMGeneralAgentAmount() {
            return rTIMGeneralAgentAmount;
        }

        public void setRTIMGeneralAgentAmount(Integer rTIMGeneralAgentAmount) {
            this.rTIMGeneralAgentAmount = rTIMGeneralAgentAmount;
        }

        public Integer getRTIMIsLoginAgent() {
            return rTIMIsLoginAgent;
        }

        public void setRTIMIsLoginAgent(Integer rTIMIsLoginAgent) {
            this.rTIMIsLoginAgent = rTIMIsLoginAgent;
        }

        public Integer getRTIMLoginAgentAmount() {
            return rTIMLoginAgentAmount;
        }

        public void setRTIMLoginAgentAmount(Integer rTIMLoginAgentAmount) {
            this.rTIMLoginAgentAmount = rTIMLoginAgentAmount;
        }

    }

    public class ITSRouteTimeWiseAgent {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("AM_AgentID")
        @Expose
        private Integer aMAgentID;
        @SerializedName("CAM_CompanyAgentID")
        @Expose
        private Integer cAMCompanyAgentID;
        @SerializedName("RM_RouteID")
        @Expose
        private Integer rMRouteID;
        @SerializedName("RT_RouteTimeID")
        @Expose
        private Integer rTRouteTimeID;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public Integer getAMAgentID() {
            return aMAgentID;
        }

        public void setAMAgentID(Integer aMAgentID) {
            this.aMAgentID = aMAgentID;
        }

        public Integer getCAMCompanyAgentID() {
            return cAMCompanyAgentID;
        }

        public void setCAMCompanyAgentID(Integer cAMCompanyAgentID) {
            this.cAMCompanyAgentID = cAMCompanyAgentID;
        }

        public Integer getRMRouteID() {
            return rMRouteID;
        }

        public void setRMRouteID(Integer rMRouteID) {
            this.rMRouteID = rMRouteID;
        }

        public Integer getRTRouteTimeID() {
            return rTRouteTimeID;
        }

        public void setRTRouteTimeID(Integer rTRouteTimeID) {
            this.rTRouteTimeID = rTRouteTimeID;
        }

    }

    @Entity(tableName = "ITSGetIdProof")
    public static class ITSGetIdProof {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        private Integer id;

        @SerializedName("PM_ID")
        @Expose
        private Integer pMID;
        @SerializedName("PM_ProofName")
        @Expose
        private String pMProofName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPMID() {
            return pMID;
        }

        public void setPMID(Integer pMID) {
            this.pMID = pMID;
        }

        public String getPMProofName() {
            return pMProofName;
        }

        public void setPMProofName(String pMProofName) {
            this.pMProofName = pMProofName;
        }

    }

}
