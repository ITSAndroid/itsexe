package com.infinityinfoway.itsexe.api;



import com.infinityinfoway.itsexe.api.request.AdminLogin_Request;
import com.infinityinfoway.itsexe.api.request.AllDayBranchWiseReturnOnwardMemo_Request;
import com.infinityinfoway.itsexe.api.request.BookingUpdate_Request;
import com.infinityinfoway.itsexe.api.request.BranchWiseInquiryReport_Request;
import com.infinityinfoway.itsexe.api.request.BusIncomeReceipt_Request;
import com.infinityinfoway.itsexe.api.request.CompanyCardDetails_Request;
import com.infinityinfoway.itsexe.api.request.ConfirmToOpen_Request;
import com.infinityinfoway.itsexe.api.request.CouponCodeDetails_Request;
import com.infinityinfoway.itsexe.api.request.ESendGSTInvoice_Request;
import com.infinityinfoway.itsexe.api.request.ESendTicketRequest;
import com.infinityinfoway.itsexe.api.request.FutureBookingGraph_Request;
import com.infinityinfoway.itsexe.api.request.GetCancellationDetails_ConfirmCancellation_Request;
import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeALLXML_Request;
import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeXML_Request;
import com.infinityinfoway.itsexe.api.request.Get_BranchList_Request;
import com.infinityinfoway.itsexe.api.request.Get_CompanyList_Request;
import com.infinityinfoway.itsexe.api.request.Get_InsertTabletBookingRegistration_Request;
import com.infinityinfoway.itsexe.api.request.Get_ReportType_Request;
import com.infinityinfoway.itsexe.api.request.Get_Report_AllDayMemo_BranchWise_Request;
import com.infinityinfoway.itsexe.api.request.Get_Report_AllDayMemo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Report_Booking_Cancel_ReportByRouteTime_Request;
import com.infinityinfoway.itsexe.api.request.BranchUserWiseCollection_RefundChart_Request;
import com.infinityinfoway.itsexe.api.request.CityDropUpWiseChart_Request;
import com.infinityinfoway.itsexe.api.request.ITSPLWalletGenerateOTP_Request;
import com.infinityinfoway.itsexe.api.request.InsertAgentQuotaBookingCancle_Request;
import com.infinityinfoway.itsexe.api.request.NonReported_Request;
import com.infinityinfoway.itsexe.api.request.PNRBookingLog_Request;
import com.infinityinfoway.itsexe.api.request.PNRFeedBack_Request;
import com.infinityinfoway.itsexe.api.request.PassengerList_Request;
import com.infinityinfoway.itsexe.api.request.PhoneTicketCount_Request;
import com.infinityinfoway.itsexe.api.request.PickupDropReport_Request;
import com.infinityinfoway.itsexe.api.request.PickupDropWiseBookingDetails_Request;
import com.infinityinfoway.itsexe.api.request.CityWiseSeatChartReport_Request;
import com.infinityinfoway.itsexe.api.request.DailyRoutetimeWiseMemo_Request;
import com.infinityinfoway.itsexe.api.request.MainRouteWisePassengerDetails_Request;
import com.infinityinfoway.itsexe.api.request.InsertBooking_Request;
import com.infinityinfoway.itsexe.api.request.MainRouteWiseChart_Request;
import com.infinityinfoway.itsexe.api.request.PNRWiseAmenities_Request;
import com.infinityinfoway.itsexe.api.request.PassengerBookingReport_Request;
import com.infinityinfoway.itsexe.api.request.PickupWiseInquiry_Request;
import com.infinityinfoway.itsexe.api.request.ReleaseStopBooking_Request;
import com.infinityinfoway.itsexe.api.request.Reported_Request;
import com.infinityinfoway.itsexe.api.request.ReservationDetailReportWithAmount_Request;
import com.infinityinfoway.itsexe.api.request.ReservationDetailReport_Request;
import com.infinityinfoway.itsexe.api.request.RouteDepartureReport_Request;
import com.infinityinfoway.itsexe.api.request.RoutePickupDropChart_Request;
import com.infinityinfoway.itsexe.api.request.RouteTimeBookingDetails_Request;
import com.infinityinfoway.itsexe.api.request.PickupDrop_Request;
import com.infinityinfoway.itsexe.api.request.RouteTimeMemoReport_Request;
import com.infinityinfoway.itsexe.api.request.SeatAllocationChart_Request;
import com.infinityinfoway.itsexe.api.request.SeatArrangement_Request;
import com.infinityinfoway.itsexe.api.request.SeatHold_Request;
import com.infinityinfoway.itsexe.api.request.SeatNumberWiseChart_Request;
import com.infinityinfoway.itsexe.api.request.SeatWiseInquiryReport_Request;
import com.infinityinfoway.itsexe.api.request.SeatWiseRouteDepartureReport_Request;
import com.infinityinfoway.itsexe.api.request.SendSMS_Request;
import com.infinityinfoway.itsexe.api.request.StopBooking_Request;
import com.infinityinfoway.itsexe.api.request.SubRouteWiseTicketCount_Request;
import com.infinityinfoway.itsexe.api.request.Get_Rights_Request;
import com.infinityinfoway.itsexe.api.request.Get_Route_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingCancelTkTPrint_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingMobileNo_Request;
import com.infinityinfoway.itsexe.api.request.FetchDetailsByPNRNO_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingPhoneSeatCount_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingSeatNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_BusSchedule_By_BusNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_GPSLastLocationByBusNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_PastBookingDataByPhone_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_PrintGSTInvoice_Request;
import com.infinityinfoway.itsexe.api.request.Get_TabCheckRegistration_Request;
import com.infinityinfoway.itsexe.api.request.Get_TabVerifyLogin_Request;
import com.infinityinfoway.itsexe.api.request.RouteTimeWisePickupChart_Request;
import com.infinityinfoway.itsexe.api.request.SendOTP_Request;
import com.infinityinfoway.itsexe.api.request.SubRouteWiseChart_Request;
import com.infinityinfoway.itsexe.api.request.TicketPrintPDF_Request;
import com.infinityinfoway.itsexe.api.request.TimewiseAllBranchMemo_Request;
import com.infinityinfoway.itsexe.api.request.TimewiseBranchMemo_Request;
import com.infinityinfoway.itsexe.api.request.WaitingReport_Request;
import com.infinityinfoway.itsexe.api.request.checkVersionInfo_Request;
import com.infinityinfoway.itsexe.api.response.AdminLogin_Response;
import com.infinityinfoway.itsexe.api.response.AllDayBranchWiseReturnOnwardMemo_Response;
import com.infinityinfoway.itsexe.api.response.BookingUpdate_Response;
import com.infinityinfoway.itsexe.api.response.BranchWiseInquiryReport_Response;
import com.infinityinfoway.itsexe.api.response.BusIncomeReceipt_Response;
import com.infinityinfoway.itsexe.api.response.CompanyCardDetails_Response;
import com.infinityinfoway.itsexe.api.response.ConfirmToOpen_Response;
import com.infinityinfoway.itsexe.api.response.CouponCodeDetails_Response;
import com.infinityinfoway.itsexe.api.response.ESendGSTInvoice_Response;
import com.infinityinfoway.itsexe.api.response.ESendTicketResponse;
import com.infinityinfoway.itsexe.api.response.FutureBookingGraph_Response;
import com.infinityinfoway.itsexe.api.response.GetCancellationDetails_ConfirmCancellation_Response;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;
import com.infinityinfoway.itsexe.api.response.Get_BranchList_Response;
import com.infinityinfoway.itsexe.api.response.Get_CompanyList_Response;
import com.infinityinfoway.itsexe.api.response.Get_InsertTabletBookingRegistration_Response;
import com.infinityinfoway.itsexe.api.response.Get_ReportType_Response;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_BranchWise_Response;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Report_Booking_Cancel_ReportByRouteTime_Response;
import com.infinityinfoway.itsexe.api.response.BranchUserWiseCollection_RefundChart_Response;
import com.infinityinfoway.itsexe.api.response.CityDropUpWiseChart_Response;
import com.infinityinfoway.itsexe.api.response.ITSPLWalletGenerateOTP_Response;
import com.infinityinfoway.itsexe.api.response.InsertAgentQuotaBookingCancle_Response;
import com.infinityinfoway.itsexe.api.response.NonReported_Response;
import com.infinityinfoway.itsexe.api.response.PNRBookingLog_Response;
import com.infinityinfoway.itsexe.api.response.PNRFeedBack_Response;
import com.infinityinfoway.itsexe.api.response.PassengerList_Response;
import com.infinityinfoway.itsexe.api.response.PhoneTicketCount_Response;
import com.infinityinfoway.itsexe.api.response.PickupDropReport_Response;
import com.infinityinfoway.itsexe.api.response.PickupDropWiseBookingDetails_Response;
import com.infinityinfoway.itsexe.api.response.CityWiseSeatChartReport_Response;
import com.infinityinfoway.itsexe.api.response.DailyRoutetimeWiseMemo_Response;
import com.infinityinfoway.itsexe.api.response.MainRouteWisePassengerDetails_Response;
import com.infinityinfoway.itsexe.api.response.InsertBooking_Response;
import com.infinityinfoway.itsexe.api.response.MainRouteWiseChart_Response;
import com.infinityinfoway.itsexe.api.response.PNRWiseAmenities_Response;
import com.infinityinfoway.itsexe.api.response.PassengerBookingReport_Response;
import com.infinityinfoway.itsexe.api.response.PickupWiseInquiry_Response;
import com.infinityinfoway.itsexe.api.response.ReleaseStopBooking_Response;
import com.infinityinfoway.itsexe.api.response.Reported_Response;
import com.infinityinfoway.itsexe.api.response.ReservationDetailReportWithAmount_Response;
import com.infinityinfoway.itsexe.api.response.ReservationDetailReport_Response;
import com.infinityinfoway.itsexe.api.response.RouteDepartureReport_Response;
import com.infinityinfoway.itsexe.api.response.RoutePickupDropChart_Response;
import com.infinityinfoway.itsexe.api.response.RouteTimeBookingDetails_Response;
import com.infinityinfoway.itsexe.api.response.PickupDrop_Response;
import com.infinityinfoway.itsexe.api.response.RouteTimeMemoReport_Response;
import com.infinityinfoway.itsexe.api.response.SeatAllocationChart_Response;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.api.response.SeatHold_Response;
import com.infinityinfoway.itsexe.api.response.SeatNumberWiseChart_Response;
import com.infinityinfoway.itsexe.api.response.SeatWiseInquiryReport_Response;
import com.infinityinfoway.itsexe.api.response.SeatWiseRouteDepartureReport_Response;
import com.infinityinfoway.itsexe.api.response.SendSMS_Response;
import com.infinityinfoway.itsexe.api.response.StopBooking_Response;
import com.infinityinfoway.itsexe.api.response.SubRouteWiseTicketCount_Response;
import com.infinityinfoway.itsexe.api.response.Get_Rights_Response;
import com.infinityinfoway.itsexe.api.response.Get_Route_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingCancelTkTPrint_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingMobileNo_Response;
import com.infinityinfoway.itsexe.api.response.FetchDetailsByPNRNO_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingPhoneSeatCount_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingSeatNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_BusSchedule_By_BusNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_GPSLastLocationByBusNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_PastBookingDataByPhone_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_PrintGSTInvoice_Response;
import com.infinityinfoway.itsexe.api.response.Get_TabCheckRegistration_Response;
import com.infinityinfoway.itsexe.api.response.Get_TabVerifyLogin_Response;
import com.infinityinfoway.itsexe.api.response.RouteTimeWisePickupChart_Response;
import com.infinityinfoway.itsexe.api.response.SendOTP_Response;
import com.infinityinfoway.itsexe.api.response.SubRouteWiseChart_Response;
import com.infinityinfoway.itsexe.api.response.TicketPrintPDF_Response;
import com.infinityinfoway.itsexe.api.response.TimewiseAllBranchMemo_Response;
import com.infinityinfoway.itsexe.api.response.TimewiseBranchMemo_Response;
import com.infinityinfoway.itsexe.api.response.WaitingReport_Response;
import com.infinityinfoway.itsexe.api.response.checkVersionInfo_Response;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

    //------------------------------------------------------------------------------------
    // Registration , Login
    //------------------------------------------------------------------------------------

    @POST("checkVersionInfo")
    Call<checkVersionInfo_Response> checkVersionInfo(@Body checkVersionInfo_Request request);

    @POST("Get_TabCheckRegistration")
    Call<Get_TabCheckRegistration_Response> Get_TabCheckRegistration(@Body Get_TabCheckRegistration_Request request);

    @POST("Get_InsertTabletBookingRegistration")
    Call<Get_InsertTabletBookingRegistration_Response> Get_InsertTabletBookingRegistration(@Body Get_InsertTabletBookingRegistration_Request request);

    /*@POST("Get_TabVerifyLogin")
    Call<Get_TabVerifyLogin_Response> Get_TabVerifyLogin(@Body Get_TabVerifyLogin_Request request);*/

    @POST("Get_TabVerifyLogin")
    Observable<Get_TabVerifyLogin_Response> Get_TabVerifyLogin(@Body Get_TabVerifyLogin_Request request);

    /*@POST("AdminLogin")
    Call<AdminLogin_Response> AdminLogin(@Body AdminLogin_Request request);*/

    @POST("AdminLogin")
    Observable<AdminLogin_Response> AdminLogin(@Body AdminLogin_Request request);

   /* @POST("Get_CompanyList")
    Call<Get_CompanyList_Response> Get_CompanyList(@Body Get_CompanyList_Request request);

    @POST("Get_BranchList")
    Call<Get_BranchList_Response> Get_BranchList(@Body Get_BranchList_Request request);*/

    @POST("Get_CompanyList")
    Observable<Get_CompanyList_Response> Get_CompanyList(@Body Get_CompanyList_Request request);

    @POST("Get_BranchList")
    Observable<Get_BranchList_Response> Get_BranchList(@Body Get_BranchList_Request request);


    //------------------------------------------------------------------------------------
    // Searching
    //------------------------------------------------------------------------------------

    @POST("FetchDetailsByPNRNO")
    Call<FetchDetailsByPNRNO_Response> FetchDetailsByPNRNO(@Body FetchDetailsByPNRNO_Request request);

    @POST("Get_SearchingSeatNo")
    Call<Get_SearchingSeatNo_Response> Get_SearchingSeatNo(@Body Get_SearchingSeatNo_Request request);

    @POST("Get_SearchingMobileNo")
    Call<Get_SearchingMobileNo_Response> Get_SearchingMobileNo(@Body Get_SearchingMobileNo_Request request);

    @POST("Get_SearchingPhoneSeatCount")
    Call<Get_SearchingPhoneSeatCount_Response> Get_SearchingPhoneSeatCount(@Body Get_SearchingPhoneSeatCount_Request request);

    @POST("Get_SearchingCancelTkTPrint")
    Call<Get_SearchingCancelTkTPrint_Response> Get_SearchingCancelTkTPrint(@Body Get_SearchingCancelTkTPrint_Request request);

    @POST("Get_Searching_BusSchedule_By_BusNo")
    Call<Get_Searching_BusSchedule_By_BusNo_Response> Get_Searching_BusSchedule_By_BusNo(@Body Get_Searching_BusSchedule_By_BusNo_Request request);

    @POST("Get_Searching_PastBookingDataByPhone")
    Call<Get_Searching_PastBookingDataByPhone_Response> Get_Searching_PastBookingDataByPhone(@Body Get_Searching_PastBookingDataByPhone_Request request);

    @POST("Get_Searching_GPSLastLocationByBusNo")
    Call<Get_Searching_GPSLastLocationByBusNo_Response> Get_Searching_GPSLastLocationByBusNo(@Body Get_Searching_GPSLastLocationByBusNo_Request request);

    @POST("Get_Searching_PrintGSTInvoice")
    Call<Get_Searching_PrintGSTInvoice_Response> Get_Searching_PrintGSTInvoice(@Body Get_Searching_PrintGSTInvoice_Request request);

    @POST("StopBooking")
    Call<StopBooking_Response> StopBooking(@Body StopBooking_Request request);


    //-------------------------------------------------------------------------------------
    //  Reports
    //-------------------------------------------------------------------------------------

    @POST("RouteTimeWisePickupChart")
    Call<RouteTimeWisePickupChart_Response> RouteTimeWisePickupChart(@Body RouteTimeWisePickupChart_Request request);

    @POST("SubRouteWiseChart")
    Call<SubRouteWiseChart_Response> SubRouteWiseChart(@Body SubRouteWiseChart_Request request);

    @POST("BranchUserWiseCollection_RefundChart")
    Call<BranchUserWiseCollection_RefundChart_Response> BranchUserWiseCollection_RefundChart(@Body BranchUserWiseCollection_RefundChart_Request request);

    @POST("PassengerBookingReport")
    Call<PassengerBookingReport_Response> PassengerBookingReport(@Body PassengerBookingReport_Request request);

    @POST("MainRouteWiseChart")
    Call<MainRouteWiseChart_Response> MainRouteWiseChart(@Body MainRouteWiseChart_Request request);

    @POST("SeatAllocationChart")
    Call<SeatAllocationChart_Response> SeatAllocationChart(@Body SeatAllocationChart_Request request);

    @POST("TimewiseBranchMemo")
    Call<TimewiseBranchMemo_Response> TimewiseBranchMemo(@Body TimewiseBranchMemo_Request request);

    @POST("TimewiseAllBranchMemo")
    Call<TimewiseAllBranchMemo_Response> TimewiseAllBranchMemo(@Body TimewiseAllBranchMemo_Request request);

    @POST("MainRouteWisePassengerDetails")
    Call<MainRouteWisePassengerDetails_Response> MainRouteWisePassengerDetails(@Body MainRouteWisePassengerDetails_Request request);

    @POST("CityDropUpWiseChart")
    Call<CityDropUpWiseChart_Response> CityDropUpWiseChart(@Body CityDropUpWiseChart_Request request);

//    @POST("Get_Report_AllDayMemo")
//    Call<Get_Report_AllDayMemo_Response> Get_Report_AllDayMemo(@Body Get_Report_AllDayMemo_Request request);

    @POST("Get_Report_AllDayMemo")
    Observable<Get_Report_AllDayMemo_Response> Get_Report_AllDayMemo(@Body Get_Report_AllDayMemo_Request request);

    @POST("Get_Report_AllDayMemo_BranchWise")
    Call<Get_Report_AllDayMemo_BranchWise_Response> Get_Report_AllDayMemo_BranchWise(@Body Get_Report_AllDayMemo_BranchWise_Request request);

    @POST("AllDayBranchWiseReturnOnwardMemo")
    Call<AllDayBranchWiseReturnOnwardMemo_Response> AllDayBranchWiseReturnOnwardMemo(@Body AllDayBranchWiseReturnOnwardMemo_Request request);

    @POST("Get_Report_Booking_Cancel_ReportByRouteTime")
    Call<Get_Report_Booking_Cancel_ReportByRouteTime_Response> Get_Report_Booking_Cancel_ReportByRouteTime(@Body Get_Report_Booking_Cancel_ReportByRouteTime_Request request);

    @POST("SubRouteWiseTicketCount")
    Call<SubRouteWiseTicketCount_Response> SubRouteWiseTicketCount(@Body SubRouteWiseTicketCount_Request request);

    @POST("RouteDepartureReport")
    Call<RouteDepartureReport_Response> RouteDepartureReport(@Body RouteDepartureReport_Request request);

    @POST("SeatWiseRouteDepartureReport")
    Call<SeatWiseRouteDepartureReport_Response> SeatWiseRouteDepartureReport(@Body SeatWiseRouteDepartureReport_Request request);

    @POST("RouteTimeMemoReport")
    Call<RouteTimeMemoReport_Response> RouteTimeMemoReport(@Body RouteTimeMemoReport_Request request);

    @POST("RouteTimeBookingDetails")
    Call<RouteTimeBookingDetails_Response> RouteTimeBookingDetails(@Body RouteTimeBookingDetails_Request request);

    @POST("BusIncomeReceipt")
    Call<BusIncomeReceipt_Response> BusIncomeReceipt(@Body BusIncomeReceipt_Request request);

    @POST("SeatNumberWiseChart")
    Call<SeatNumberWiseChart_Response> SeatNumberWiseChart(@Body SeatNumberWiseChart_Request request);

    @POST("DailyRoutetimeWiseMemo")
    Call<DailyRoutetimeWiseMemo_Response> DailyRoutetimeWiseMemo(@Body DailyRoutetimeWiseMemo_Request request);

    @POST("RoutePickupDropChart")
    Call<RoutePickupDropChart_Response> RoutePickupDropChart(@Body RoutePickupDropChart_Request request);

    @POST("PickupDropReport")
    Call<PickupDropReport_Response> PickupDropReport(@Body PickupDropReport_Request request);

    @POST("PickupDropWiseBookingDetails")
    Call<PickupDropWiseBookingDetails_Response> PickupDropWiseBookingDetails(@Body PickupDropWiseBookingDetails_Request request);

    @POST("PickupWiseInquiry")
    Call<PickupWiseInquiry_Response> PickupWiseInquiry(@Body PickupWiseInquiry_Request request);

    @POST("CityWiseSeatChartReport")
    Call<CityWiseSeatChartReport_Response> CityWiseSeatChartReport(@Body CityWiseSeatChartReport_Request request);

    @POST("PNRWiseAmenities")
    Call<PNRWiseAmenities_Response> PNRWiseAmenities(@Body PNRWiseAmenities_Request request);

    @POST("PassengerList")
    Call<PassengerList_Response> PassengerList(@Body PassengerList_Request request);

    @POST("SeatWiseInquiryReport")
    Call<SeatWiseInquiryReport_Response> SeatWiseInquiryReport(@Body SeatWiseInquiryReport_Request request);

    @POST("ReservationDetailReport")
    Call<ReservationDetailReport_Response> ReservationDetailReport(@Body ReservationDetailReport_Request request);

    @POST("BranchWiseInquiryReport")
    Call<BranchWiseInquiryReport_Response> BranchWiseInquiryReport(@Body BranchWiseInquiryReport_Request request);

    @POST("ReservationDetailReportWithAmount")
    Call<ReservationDetailReportWithAmount_Response> ReservationDetailReportWithAmount(@Body ReservationDetailReportWithAmount_Request request);

    @POST("FutureBookingGraph")
    Call<FutureBookingGraph_Response> FutureBookingGraph(@Body FutureBookingGraph_Request request);

    @POST("WaitingReport")
    Call<WaitingReport_Response> WaitingReport(@Body WaitingReport_Request request);

    @POST("Reported")
    Call<Reported_Response> Reported(@Body Reported_Request request);

    @POST("NonReported")
    Call<NonReported_Response> NonReported(@Body NonReported_Request request);

    @POST("SendSMS")
    Call<SendSMS_Response> SendSMS(@Body SendSMS_Request request);

    @POST("ConfirmToOpen")
    Call<ConfirmToOpen_Response> ConfirmToOpen(@Body ConfirmToOpen_Request request);

    @POST("ESendGSTInvoice")
    Call<ESendGSTInvoice_Response> ESendGSTInvoice(@Body ESendGSTInvoice_Request request);

    @POST("ESendTicket")
    Call<ESendTicketResponse> ESendTicket(@Body ESendTicketRequest request);

    @POST("TicketPrintPDF")
    Call<TicketPrintPDF_Response> TicketPrintPDF(@Body TicketPrintPDF_Request request);


    //-------------------------------------------------------------------------------------
    //  Seat Booking
    //-------------------------------------------------------------------------------------

    @POST("Get_API_EXE_RouteTimeXML")
    Call<Get_API_EXE_RouteTimeXML_Response> Get_API_EXE_RouteTimeXML(@Body Get_API_EXE_RouteTimeXML_Request request);

    @POST("Get_API_EXE_RouteTimeALLXML")
    Call<Get_API_EXE_RouteTimeALLXML_Response> Get_API_EXE_RouteTimeALLXML(@Body Get_API_EXE_RouteTimeALLXML_Request request);

    @POST("Get_ReportType")
    Call<Get_ReportType_Response> Get_ReportType(@Body Get_ReportType_Request request);

    @POST("Get_Route")
    Call<Get_Route_Response> Get_Route(@Body Get_Route_Request request);

    // Used for Seat Chart as well 12 no report

    @POST("SeatArrangement")
    Call<SeatArrangement_Response> SeatArrangement(@Body SeatArrangement_Request request);

    @POST("PickupDrop")
    Call<PickupDrop_Response> PickupDrop(@Body PickupDrop_Request request);

    @POST("GetCancellationDetails_ConfirmCancellation")
    Call<GetCancellationDetails_ConfirmCancellation_Response> GetCancellationDetails_ConfirmCancellation(@Body GetCancellationDetails_ConfirmCancellation_Request request);

    @POST("InsertBooking")
    Call<InsertBooking_Response> InsertBooking(@Body InsertBooking_Request request);

    @POST("BookingUpdate")
    Call<BookingUpdate_Response> BookingUpdate(@Body BookingUpdate_Request request);

    @POST("ITSPLWalletGenerateOTP")
    Call<ITSPLWalletGenerateOTP_Response> ITSPLWalletGenerateOTP(@Body ITSPLWalletGenerateOTP_Request request);

    @POST("InsertAgentQuotaBookingCancle")
    Call<InsertAgentQuotaBookingCancle_Response> InsertAgentQuotaBookingCancle(@Body InsertAgentQuotaBookingCancle_Request request);

    @POST("PhoneTicketCount")
    Call<PhoneTicketCount_Response> PhoneTicketCount(@Body PhoneTicketCount_Request request);

    @POST("PNRBookingLog")
    Call<PNRBookingLog_Response> PNRBookingLog(@Body PNRBookingLog_Request request);

    @POST("PNRFeedBack")
    Call<PNRFeedBack_Response> PNRFeedBack(@Body PNRFeedBack_Request request);

    @POST("SeatHold")
    Call<SeatHold_Response> SeatHold(@Body SeatHold_Request request);

    @POST("ReleaseStopBooking")
    Call<ReleaseStopBooking_Response> ReleaseStopBooking(@Body ReleaseStopBooking_Request request);

    //-------------------------------------------------------------------------------------
    //  Rights
    //-------------------------------------------------------------------------------------

    @POST("Get_Rights")
    Call<Get_Rights_Response> Get_Rights(@Body Get_Rights_Request request);

    //-------------------------------------------------------------------------------------
    //  Discount Coupons
    //-------------------------------------------------------------------------------------

    @POST("CouponCodeDetails")
    Call<CouponCodeDetails_Response> CouponCodeDetails(@Body CouponCodeDetails_Request request);

    @POST("CompanyCardDetails")
    Call<CompanyCardDetails_Response> CompanyCardDetails(@Body CompanyCardDetails_Request request);

    //-------------------------------------------------------------------------------------
    //  Send OTP
    //-------------------------------------------------------------------------------------

    @POST("SendOTP")
    Call<SendOTP_Response> SendOTP(@Body SendOTP_Request request);


}

