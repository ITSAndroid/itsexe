package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllDayBranchWiseReturnOnwardMemo_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("RouteTime")
        @Expose
        private String routeTime;
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Amount")
        @Expose
        private Double amount;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("TotalAmount")
        @Expose
        private Double totalAmount;
        @SerializedName("TotalCollection")
        @Expose
        private Double totalCollection;
        @SerializedName("CommissionAmount")
        @Expose
        private Double commissionAmount;
        @SerializedName("NetSeat")
        @Expose
        private Integer netSeat;
        @SerializedName("NetCollection")
        @Expose
        private Double netCollection;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("JDate")
        @Expose
        private String jDate;
        @SerializedName("Commission")
        @Expose
        private String commission;
        @SerializedName("BM_BranchName")
        @Expose
        private String bMBranchName;
        @SerializedName("BM_BranchAddress")
        @Expose
        private String bMBranchAddress;
        @SerializedName("Memo")
        @Expose
        private String memo;
        @SerializedName("TMP1")
        @Expose
        private String tMP1;
        @SerializedName("TMP2")
        @Expose
        private String tMP2;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getRouteTime() {
            return routeTime;
        }

        public void setRouteTime(String routeTime) {
            this.routeTime = routeTime;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Double getTotalCollection() {
            return totalCollection;
        }

        public void setTotalCollection(Double totalCollection) {
            this.totalCollection = totalCollection;
        }

        public Double getCommissionAmount() {
            return commissionAmount;
        }

        public void setCommissionAmount(Double commissionAmount) {
            this.commissionAmount = commissionAmount;
        }

        public Integer getNetSeat() {
            return netSeat;
        }

        public void setNetSeat(Integer netSeat) {
            this.netSeat = netSeat;
        }

        public Double getNetCollection() {
            return netCollection;
        }

        public void setNetCollection(Double netCollection) {
            this.netCollection = netCollection;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getBMBranchName() {
            return bMBranchName;
        }

        public void setBMBranchName(String bMBranchName) {
            this.bMBranchName = bMBranchName;
        }

        public String getBMBranchAddress() {
            return bMBranchAddress;
        }

        public void setBMBranchAddress(String bMBranchAddress) {
            this.bMBranchAddress = bMBranchAddress;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getTMP1() {
            return tMP1;
        }

        public void setTMP1(String tMP1) {
            this.tMP1 = tMP1;
        }

        public String getTMP2() {
            return tMP2;
        }

        public void setTMP2(String tMP2) {
            this.tMP2 = tMP2;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }


}
