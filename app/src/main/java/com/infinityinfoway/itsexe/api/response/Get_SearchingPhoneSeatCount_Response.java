package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_SearchingPhoneSeatCount_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("SearchingPhoneSeatCount")
        @Expose
        private List<SearchingPhoneSeatCount> searchingPhoneSeatCount = null;

        public List<SearchingPhoneSeatCount> getSearchingPhoneSeatCount() {
            return searchingPhoneSeatCount;
        }

        public void setSearchingPhoneSeatCount(List<SearchingPhoneSeatCount> searchingPhoneSeatCount) {
            this.searchingPhoneSeatCount = searchingPhoneSeatCount;
        }

    }

    public class SearchingPhoneSeatCount {

        @SerializedName("PNRNO")
        @Expose
        private Integer pNRNO;
        @SerializedName("BookingDate")
        @Expose
        private String bookingDate;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("RouteStartTime")
        @Expose
        private String routeStartTime;
        @SerializedName("CityTime")
        @Expose
        private String cityTime;
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("RouteNameDisplay")
        @Expose
        private String routeNameDisplay;
        @SerializedName("PassengerName")
        @Expose
        private String passengerName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("PhoneNo")
        @Expose
        private String phoneNo;
        @SerializedName("JM_IsTicketPrint")
        @Expose
        private Integer jMIsTicketPrint;
        @SerializedName("JM_ChartFinish")
        @Expose
        private Integer jMChartFinish;
        @SerializedName("JourneyStartDate")
        @Expose
        private String journeyStartDate;
        @SerializedName("CourentDate")
        @Expose
        private String courentDate;
        @SerializedName("BTM_BookingTypeID")
        @Expose
        private Integer bTMBookingTypeID;
        @SerializedName("JM_PhoneOnHold")
        @Expose
        private Integer jMPhoneOnHold;
        @SerializedName("Status")
        @Expose
        private Integer status;
        @SerializedName("StatusMSg")
        @Expose
        private String statusMSg;

        public Integer getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(Integer pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getRouteStartTime() {
            return routeStartTime;
        }

        public void setRouteStartTime(String routeStartTime) {
            this.routeStartTime = routeStartTime;
        }

        public String getCityTime() {
            return cityTime;
        }

        public void setCityTime(String cityTime) {
            this.cityTime = cityTime;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getRouteNameDisplay() {
            return routeNameDisplay;
        }

        public void setRouteNameDisplay(String routeNameDisplay) {
            this.routeNameDisplay = routeNameDisplay;
        }

        public String getPassengerName() {
            return passengerName;
        }

        public void setPassengerName(String passengerName) {
            this.passengerName = passengerName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public Integer getJMIsTicketPrint() {
            return jMIsTicketPrint;
        }

        public void setJMIsTicketPrint(Integer jMIsTicketPrint) {
            this.jMIsTicketPrint = jMIsTicketPrint;
        }

        public Integer getJMChartFinish() {
            return jMChartFinish;
        }

        public void setJMChartFinish(Integer jMChartFinish) {
            this.jMChartFinish = jMChartFinish;
        }

        public String getJourneyStartDate() {
            return journeyStartDate;
        }

        public void setJourneyStartDate(String journeyStartDate) {
            this.journeyStartDate = journeyStartDate;
        }

        public String getCourentDate() {
            return courentDate;
        }

        public void setCourentDate(String courentDate) {
            this.courentDate = courentDate;
        }

        public Integer getBTMBookingTypeID() {
            return bTMBookingTypeID;
        }

        public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
            this.bTMBookingTypeID = bTMBookingTypeID;
        }

        public Integer getJMPhoneOnHold() {
            return jMPhoneOnHold;
        }

        public void setJMPhoneOnHold(Integer jMPhoneOnHold) {
            this.jMPhoneOnHold = jMPhoneOnHold;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getStatusMSg() {
            return statusMSg;
        }

        public void setStatusMSg(String statusMSg) {
            this.statusMSg = statusMSg;
        }

    }

}
