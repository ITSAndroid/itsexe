package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Rights_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("OtherSetting")
        @Expose
        private List<OtherSetting> otherSetting = null;
        @SerializedName("BookingRights")
        @Expose
        private List<BookingRight> bookingRights = null;
        @SerializedName("OtherCompanyBookingRights")
        @Expose
        private List<OtherCompanyBookingRight> otherCompanyBookingRights = null;

        public List<OtherSetting> getOtherSetting() {
            return otherSetting;
        }

        public void setOtherSetting(List<OtherSetting> otherSetting) {
            this.otherSetting = otherSetting;
        }

        public List<BookingRight> getBookingRights() {
            return bookingRights;
        }

        public void setBookingRights(List<BookingRight> bookingRights) {
            this.bookingRights = bookingRights;
        }

        public List<OtherCompanyBookingRight> getOtherCompanyBookingRights() {
            return otherCompanyBookingRights;
        }

        public void setOtherCompanyBookingRights(List<OtherCompanyBookingRight> otherCompanyBookingRights) {
            this.otherCompanyBookingRights = otherCompanyBookingRights;
        }

    }

    public class BookingRight {

        @SerializedName("BR_userID")
        @Expose
        private Integer bRUserID;
        @SerializedName("BM_BranchID")
        @Expose
        private Integer bMBranchID;
        @SerializedName("BR_ConfirmCreate")
        @Expose
        private Integer bRConfirmCreate;
        @SerializedName("BR_ConfirmModify")
        @Expose
        private Integer bRConfirmModify;
        @SerializedName("BR_ConfirmCancel")
        @Expose
        private Integer bRConfirmCancel;
        @SerializedName("BR_PhoneCreate")
        @Expose
        private Integer bRPhoneCreate;
        @SerializedName("BR_PhoneModify")
        @Expose
        private Integer bRPhoneModify;
        @SerializedName("BR_PhoneCancel")
        @Expose
        private Integer bRPhoneCancel;
        @SerializedName("BR_AgentCreate")
        @Expose
        private Integer bRAgentCreate;
        @SerializedName("BR_AgentModify")
        @Expose
        private Integer bRAgentModify;
        @SerializedName("BR_AgentCancel")
        @Expose
        private Integer bRAgentCancel;
        @SerializedName("BR_BranchCreate")
        @Expose
        private Integer bRBranchCreate;
        @SerializedName("BR_BranchModify")
        @Expose
        private Integer bRBranchModify;
        @SerializedName("BR_BranchCancel")
        @Expose
        private Integer bRBranchCancel;
        @SerializedName("BR_GuestCreate")
        @Expose
        private Integer bRGuestCreate;
        @SerializedName("BR_GuestModify")
        @Expose
        private Integer bRGuestModify;
        @SerializedName("BR_GuestCancel")
        @Expose
        private Integer bRGuestCancel;
        @SerializedName("BR_BankCardCreate")
        @Expose
        private Integer bRBankCardCreate;
        @SerializedName("BR_BankCardModify")
        @Expose
        private Integer bRBankCardModify;
        @SerializedName("BR_BankCardCancel")
        @Expose
        private Integer bRBankCardCancel;
        @SerializedName("BR_CompanyCardCreate")
        @Expose
        private Integer bRCompanyCardCreate;
        @SerializedName("BR_CompanyCardModify")
        @Expose
        private Integer bRCompanyCardModify;
        @SerializedName("BR_CompanyCardCancel")
        @Expose
        private Integer bRCompanyCardCancel;
        @SerializedName("BR_WaitingCreate")
        @Expose
        private Integer bRWaitingCreate;
        @SerializedName("BR_WaitingModify")
        @Expose
        private Integer bRWaitingModify;
        @SerializedName("BR_WaitingCancel")
        @Expose
        private Integer bRWaitingCancel;
        @SerializedName("BR_AgentQuataCreate")
        @Expose
        private Integer bRAgentQuataCreate;
        @SerializedName("BR_AgentQuataModify")
        @Expose
        private Integer bRAgentQuataModify;
        @SerializedName("BR_AgentQuataCancel")
        @Expose
        private Integer bRAgentQuataCancel;
        @SerializedName("BR_OpenCreate")
        @Expose
        private Integer bROpenCreate;
        @SerializedName("BR_OpenModify")
        @Expose
        private Integer bROpenModify;
        @SerializedName("BR_OpenCancel")
        @Expose
        private Integer bROpenCancel;
        @SerializedName("BR_APICreate")
        @Expose
        private Integer bRAPICreate;
        @SerializedName("BR_APIModify")
        @Expose
        private Integer bRAPIModify;
        @SerializedName("BR_APICancel")
        @Expose
        private Integer bRAPICancel;
        @SerializedName("BR_B2CCreate")
        @Expose
        private Integer bRB2CCreate;
        @SerializedName("BR_B2CModify")
        @Expose
        private Integer bRB2CModify;
        @SerializedName("BR_B2CCancel")
        @Expose
        private Integer bRB2CCancel;
        @SerializedName("BR_OnlineAgentCreate")
        @Expose
        private Integer bROnlineAgentCreate;
        @SerializedName("BR_OnlineAgentModify")
        @Expose
        private Integer bROnlineAgentModify;
        @SerializedName("BR_OnlineAgentCancel")
        @Expose
        private Integer bROnlineAgentCancel;
        @SerializedName("BR_OnlineAgentPhoneCreate")
        @Expose
        private Integer bROnlineAgentPhoneCreate;
        @SerializedName("BR_OnlineAgentPhoneModify")
        @Expose
        private Integer bROnlineAgentPhoneModify;
        @SerializedName("BR_OnlineAgentPhoneCancel")
        @Expose
        private Integer bROnlineAgentPhoneCancel;
        @SerializedName("BR_APIPhoneCreate")
        @Expose
        private Integer bRAPIPhoneCreate;
        @SerializedName("BR_APIPhoneModify")
        @Expose
        private Integer bRAPIPhoneModify;
        @SerializedName("BR_APIPhoneCancel")
        @Expose
        private Integer bRAPIPhoneCancel;
        @SerializedName("BR_OnlineAgentCardCreate")
        @Expose
        private Integer bROnlineAgentCardCreate;
        @SerializedName("BR_OnlineAgentCardModify")
        @Expose
        private Integer bROnlineAgentCardModify;
        @SerializedName("BR_OnlineAgentCardCancel")
        @Expose
        private Integer bROnlineAgentCardCancel;
        @SerializedName("BOA_ConfirmModify")
        @Expose
        private Integer bOAConfirmModify;
        @SerializedName("BOA_ConfirmCancel")
        @Expose
        private Integer bOAConfirmCancel;
        @SerializedName("BOA_PhoneModify")
        @Expose
        private Integer bOAPhoneModify;
        @SerializedName("BOA_PhoneCancel")
        @Expose
        private Integer bOAPhoneCancel;
        @SerializedName("BOA_AgentModify")
        @Expose
        private Integer bOAAgentModify;
        @SerializedName("BOA_AgentCancel")
        @Expose
        private Integer bOAAgentCancel;
        @SerializedName("BOA_BranchModify")
        @Expose
        private Integer bOABranchModify;
        @SerializedName("BOA_BranchCancel")
        @Expose
        private Integer bOABranchCancel;
        @SerializedName("BOA_GuestModify")
        @Expose
        private Integer bOAGuestModify;
        @SerializedName("BOA_GuestCancel")
        @Expose
        private Integer bOAGuestCancel;
        @SerializedName("BOA_BankCardModify")
        @Expose
        private Integer bOABankCardModify;
        @SerializedName("BOA_BankCardCancel")
        @Expose
        private Integer bOABankCardCancel;
        @SerializedName("BOA_CompanyCardModify")
        @Expose
        private Integer bOACompanyCardModify;
        @SerializedName("BOA_CompanyCardCancel")
        @Expose
        private Integer bOACompanyCardCancel;
        @SerializedName("BOA_WaitingModify")
        @Expose
        private Integer bOAWaitingModify;
        @SerializedName("BOA_WaitingCancel")
        @Expose
        private Integer bOAWaitingCancel;
        @SerializedName("BOA_AgentQuataModify")
        @Expose
        private Integer bOAAgentQuataModify;
        @SerializedName("BOA_AgentQuataCancel")
        @Expose
        private Integer bOAAgentQuataCancel;
        @SerializedName("BOA_OpenModify")
        @Expose
        private Integer bOAOpenModify;
        @SerializedName("BOA_OpenCancel")
        @Expose
        private Integer bOAOpenCancel;
        @SerializedName("GA_AllowReprint")
        @Expose
        private Integer gAAllowReprint;
        @SerializedName("GA_AllowBookingAFMin")
        @Expose
        private Integer gAAllowBookingAFMin;
        @SerializedName("GA_CancellationBFMin")
        @Expose
        private Integer gACancellationBFMin;
        @SerializedName("GA_AllowFareChange")
        @Expose
        private Integer gAAllowFareChange;
        @SerializedName("GA_AllowBackDateBooking")
        @Expose
        private Integer gAAllowBackDateBooking;
        @SerializedName("GA_AllowBackDateCancellation")
        @Expose
        private Integer gAAllowBackDateCancellation;
        @SerializedName("GA_AllowDiscount")
        @Expose
        private Integer gAAllowDiscount;
        @SerializedName("GA_AllowInternetAuthorization")
        @Expose
        private Integer gAAllowInternetAuthorization;
        @SerializedName("GA_AllowModify")
        @Expose
        private Integer gAAllowModify;
        @SerializedName("GA_AllowRemoveQuota")
        @Expose
        private Integer gAAllowRemoveQuota;
        @SerializedName("GA_StopBooking")
        @Expose
        private Integer gAStopBooking;
        @SerializedName("GA_ShowAllRoute")
        @Expose
        private Integer gAShowAllRoute;
        @SerializedName("GA_ShowCollection")
        @Expose
        private Integer gAShowCollection;
        @SerializedName("GA_ArrangementTransfer")
        @Expose
        private Integer gAArrangementTransfer;
        @SerializedName("GA_ShowBackDate")
        @Expose
        private Integer gAShowBackDate;
        @SerializedName("GA_RouteTransfer")
        @Expose
        private Integer gARouteTransfer;
        @SerializedName("GA_ChartSMS")
        @Expose
        private Integer gAChartSMS;
        @SerializedName("GA_BranchWiseCollection")
        @Expose
        private Integer gABranchWiseCollection;
        @SerializedName("GA_XmlSourceMemory")
        @Expose
        private Integer gAXmlSourceMemory;
        @SerializedName("GA_AllowMultipleBooking")
        @Expose
        private Integer gAAllowMultipleBooking;
        @SerializedName("GA_AllowModifyNamePhone")
        @Expose
        private Integer gAAllowModifyNamePhone;
        @SerializedName("GA_AllowModifyToCity")
        @Expose
        private Integer gAAllowModifyToCity;
        @SerializedName("GA_ApplyRouteBFMin")
        @Expose
        private Integer gAApplyRouteBFMin;
        @SerializedName("GA_AllowConfirmToOpen")
        @Expose
        private Integer gAAllowConfirmToOpen;
        @SerializedName("GA_ShowOnlineAgent")
        @Expose
        private Integer gAShowOnlineAgent;
        @SerializedName("GA_ShowOnlyBranchCityAgent")
        @Expose
        private Integer gAShowOnlyBranchCityAgent;
        @SerializedName("GA_SiteAccess")
        @Expose
        private Integer gASiteAccess;
        @SerializedName("GA_SiteNames")
        @Expose
        private String gASiteNames;
        @SerializedName("GA_AllowPrepaidBranchBooking")
        @Expose
        private Integer gAAllowPrepaidBranchBooking;
        @SerializedName("GA_AllowChangeRefundCharges")
        @Expose
        private Integer gAAllowChangeRefundCharges;
        @SerializedName("GA_AllowLessRefundCharges")
        @Expose
        private Integer gAAllowLessRefundCharges;
        @SerializedName("GA_AllowPetrolPumpModule")
        @Expose
        private Integer gAAllowPetrolPumpModule;
        @SerializedName("GA_AllowKMEntry")
        @Expose
        private Integer gAAllowKMEntry;
        @SerializedName("GA_AllowReturnPhoneBooking")
        @Expose
        private Integer gAAllowReturnPhoneBooking;
        @SerializedName("GA_ShowPrepaidBalance")
        @Expose
        private Integer gAShowPrepaidBalance;
        @SerializedName("GA_AllowLiveBusStatusEntry")
        @Expose
        private Integer gAAllowLiveBusStatusEntry;
        @SerializedName("GA_AllowLiveBusStatusEntryOtherPickup")
        @Expose
        private Integer gAAllowLiveBusStatusEntryOtherPickup;
        @SerializedName("GA_IsChangeBusStatusPickupTime")
        @Expose
        private Integer gAIsChangeBusStatusPickupTime;
        @SerializedName("GA_IsSendBranchAddressSMS")
        @Expose
        private Integer gAIsSendBranchAddressSMS;
        @SerializedName("GA_AllowModifyFromCity")
        @Expose
        private Integer gAAllowModifyFromCity;
        @SerializedName("GA_IsPromptBackDateBooking")
        @Expose
        private Integer gAIsPromptBackDateBooking;
        @SerializedName("GA_AllowBusHubRecharge")
        @Expose
        private Integer gAAllowBusHubRecharge;
        @SerializedName("GA_RemoveSameCityHold")
        @Expose
        private Integer gARemoveSameCityHold;
        @SerializedName("GA_RemoveOtherCityHold")
        @Expose
        private Integer gARemoveOtherCityHold;
        @SerializedName("GA_AllowNonReported")
        @Expose
        private Integer gAAllowNonReported;
        @SerializedName("GA_ReleaseAPIB2CHold")
        @Expose
        private Integer gAReleaseAPIB2CHold;
        @SerializedName("GA_AllowFareChangeInPhoneModify")
        @Expose
        private Integer gAAllowFareChangeInPhoneModify;
        @SerializedName("RA_ViewInquiry0")
        @Expose
        private Integer rAViewInquiry0;
        @SerializedName("RA_ViewInquiry1")
        @Expose
        private Integer rAViewInquiry1;
        @SerializedName("RA_ViewInquiry2")
        @Expose
        private Integer rAViewInquiry2;
        @SerializedName("RA_ViewInquiry3")
        @Expose
        private Integer rAViewInquiry3;
        @SerializedName("RA_ViewInquiry4")
        @Expose
        private Integer rAViewInquiry4;
        @SerializedName("RA_ViewInquiry5")
        @Expose
        private Integer rAViewInquiry5;
        @SerializedName("RA_ViewInquiry6")
        @Expose
        private Integer rAViewInquiry6;
        @SerializedName("RA_ViewInquiry7")
        @Expose
        private Integer rAViewInquiry7;
        @SerializedName("RA_ViewInquiry8")
        @Expose
        private Integer rAViewInquiry8;
        @SerializedName("RA_ViewInquiry9")
        @Expose
        private Integer rAViewInquiry9;
        @SerializedName("RA_ViewInquiry10")
        @Expose
        private Integer rAViewInquiry10;
        @SerializedName("RA_ViewInquiry11")
        @Expose
        private Integer rAViewInquiry11;
        @SerializedName("RA_ViewInquiry12")
        @Expose
        private Integer rAViewInquiry12;
        @SerializedName("RouteTimeWisePickupChart")
        @Expose
        private Integer routeTimeWisePickupChart;
        @SerializedName("SubRouteWiseChart")
        @Expose
        private Integer subRouteWiseChart;
        @SerializedName("BranchUserWiseCollectionRefundChart")
        @Expose
        private Integer branchUserWiseCollectionRefundChart;
        @SerializedName("SeatNumberWiseChart")
        @Expose
        private Integer seatNumberWiseChart;
        @SerializedName("AllDayBranchWiseReturnOnwardsMemo")
        @Expose
        private Integer allDayBranchWiseReturnOnwardsMemo;
        @SerializedName("MainRouteWiseChart")
        @Expose
        private Integer mainRouteWiseChart;
        @SerializedName("SeatAllocationChart")
        @Expose
        private Integer seatAllocationChart;
        @SerializedName("TimeWiseBranchMemo")
        @Expose
        private Integer timeWiseBranchMemo;
        @SerializedName("TimeWiseAllBranchMemo")
        @Expose
        private Integer timeWiseAllBranchMemo;
        @SerializedName("AllDayMemo")
        @Expose
        private Integer allDayMemo;
        @SerializedName("AllDayBranchWiseMemo")
        @Expose
        private Integer allDayBranchWiseMemo;
        @SerializedName("CancelReportByRouteAndTime")
        @Expose
        private Integer cancelReportByRouteAndTime;
        @SerializedName("SeatArrangementReport")
        @Expose
        private Integer seatArrangementReport;
        @SerializedName("RA_CityDropWise")
        @Expose
        private Integer rACityDropWise;
        @SerializedName("RA_SubRouteTicketCount")
        @Expose
        private Integer rASubRouteTicketCount;
        @SerializedName("RA_PassengerBooking")
        @Expose
        private Integer rAPassengerBooking;
        @SerializedName("RA_RouteDeparture")
        @Expose
        private Integer rARouteDeparture;
        @SerializedName("RA_RouteTimeWiseMemo")
        @Expose
        private Integer rARouteTimeWiseMemo;
        @SerializedName("RA_PickupWiseInquiry")
        @Expose
        private Integer rAPickupWiseInquiry;
        @SerializedName("RA_MainRouteWisePassengerDetails")
        @Expose
        private Integer rAMainRouteWisePassengerDetails;
        @SerializedName("RA_RouteTimeBookingMemo")
        @Expose
        private Integer rARouteTimeBookingMemo;
        @SerializedName("RA_BusIncomeReceipt")
        @Expose
        private Integer rABusIncomeReceipt;
        @SerializedName("RA_AllowCityWiseSeatChartReport")
        @Expose
        private Integer rAAllowCityWiseSeatChartReport;
        @SerializedName("RA_AllowBulkTicketPrint")
        @Expose
        private Integer rAAllowBulkTicketPrint;
        @SerializedName("CityPickupDropWiseChart")
        @Expose
        private Integer cityPickupDropWiseChart;
        @SerializedName("DailyRouteTimeWiseMemo")
        @Expose
        private Integer dailyRouteTimeWiseMemo;
        @SerializedName("FutureBookingGraph")
        @Expose
        private Integer futureBookingGraph;
        @SerializedName("PickupDropReport")
        @Expose
        private Integer pickupDropReport;
        @SerializedName("RA_PNRWiseAmenities")
        @Expose
        private Integer rAPNRWiseAmenities;
        @SerializedName("RA_RoutePickUpDropChart")
        @Expose
        private Integer rARoutePickUpDropChart;
        @SerializedName("OS_AdlabsURL")
        @Expose
        private String oSAdlabsURL;
        @SerializedName("OS_IsLabelDisplay")
        @Expose
        private Integer oSIsLabelDisplay;
        @SerializedName("OS_LableText")
        @Expose
        private String oSLableText;

        public Integer getBRUserID() {
            return bRUserID;
        }

        public void setBRUserID(Integer bRUserID) {
            this.bRUserID = bRUserID;
        }

        public Integer getBMBranchID() {
            return bMBranchID;
        }

        public void setBMBranchID(Integer bMBranchID) {
            this.bMBranchID = bMBranchID;
        }

        public Integer getBRConfirmCreate() {
            return bRConfirmCreate;
        }

        public void setBRConfirmCreate(Integer bRConfirmCreate) {
            this.bRConfirmCreate = bRConfirmCreate;
        }

        public Integer getBRConfirmModify() {
            return bRConfirmModify;
        }

        public void setBRConfirmModify(Integer bRConfirmModify) {
            this.bRConfirmModify = bRConfirmModify;
        }

        public Integer getBRConfirmCancel() {
            return bRConfirmCancel;
        }

        public void setBRConfirmCancel(Integer bRConfirmCancel) {
            this.bRConfirmCancel = bRConfirmCancel;
        }

        public Integer getBRPhoneCreate() {
            return bRPhoneCreate;
        }

        public void setBRPhoneCreate(Integer bRPhoneCreate) {
            this.bRPhoneCreate = bRPhoneCreate;
        }

        public Integer getBRPhoneModify() {
            return bRPhoneModify;
        }

        public void setBRPhoneModify(Integer bRPhoneModify) {
            this.bRPhoneModify = bRPhoneModify;
        }

        public Integer getBRPhoneCancel() {
            return bRPhoneCancel;
        }

        public void setBRPhoneCancel(Integer bRPhoneCancel) {
            this.bRPhoneCancel = bRPhoneCancel;
        }

        public Integer getBRAgentCreate() {
            return bRAgentCreate;
        }

        public void setBRAgentCreate(Integer bRAgentCreate) {
            this.bRAgentCreate = bRAgentCreate;
        }

        public Integer getBRAgentModify() {
            return bRAgentModify;
        }

        public void setBRAgentModify(Integer bRAgentModify) {
            this.bRAgentModify = bRAgentModify;
        }

        public Integer getBRAgentCancel() {
            return bRAgentCancel;
        }

        public void setBRAgentCancel(Integer bRAgentCancel) {
            this.bRAgentCancel = bRAgentCancel;
        }

        public Integer getBRBranchCreate() {
            return bRBranchCreate;
        }

        public void setBRBranchCreate(Integer bRBranchCreate) {
            this.bRBranchCreate = bRBranchCreate;
        }

        public Integer getBRBranchModify() {
            return bRBranchModify;
        }

        public void setBRBranchModify(Integer bRBranchModify) {
            this.bRBranchModify = bRBranchModify;
        }

        public Integer getBRBranchCancel() {
            return bRBranchCancel;
        }

        public void setBRBranchCancel(Integer bRBranchCancel) {
            this.bRBranchCancel = bRBranchCancel;
        }

        public Integer getBRGuestCreate() {
            return bRGuestCreate;
        }

        public void setBRGuestCreate(Integer bRGuestCreate) {
            this.bRGuestCreate = bRGuestCreate;
        }

        public Integer getBRGuestModify() {
            return bRGuestModify;
        }

        public void setBRGuestModify(Integer bRGuestModify) {
            this.bRGuestModify = bRGuestModify;
        }

        public Integer getBRGuestCancel() {
            return bRGuestCancel;
        }

        public void setBRGuestCancel(Integer bRGuestCancel) {
            this.bRGuestCancel = bRGuestCancel;
        }

        public Integer getBRBankCardCreate() {
            return bRBankCardCreate;
        }

        public void setBRBankCardCreate(Integer bRBankCardCreate) {
            this.bRBankCardCreate = bRBankCardCreate;
        }

        public Integer getBRBankCardModify() {
            return bRBankCardModify;
        }

        public void setBRBankCardModify(Integer bRBankCardModify) {
            this.bRBankCardModify = bRBankCardModify;
        }

        public Integer getBRBankCardCancel() {
            return bRBankCardCancel;
        }

        public void setBRBankCardCancel(Integer bRBankCardCancel) {
            this.bRBankCardCancel = bRBankCardCancel;
        }

        public Integer getBRCompanyCardCreate() {
            return bRCompanyCardCreate;
        }

        public void setBRCompanyCardCreate(Integer bRCompanyCardCreate) {
            this.bRCompanyCardCreate = bRCompanyCardCreate;
        }

        public Integer getBRCompanyCardModify() {
            return bRCompanyCardModify;
        }

        public void setBRCompanyCardModify(Integer bRCompanyCardModify) {
            this.bRCompanyCardModify = bRCompanyCardModify;
        }

        public Integer getBRCompanyCardCancel() {
            return bRCompanyCardCancel;
        }

        public void setBRCompanyCardCancel(Integer bRCompanyCardCancel) {
            this.bRCompanyCardCancel = bRCompanyCardCancel;
        }

        public Integer getBRWaitingCreate() {
            return bRWaitingCreate;
        }

        public void setBRWaitingCreate(Integer bRWaitingCreate) {
            this.bRWaitingCreate = bRWaitingCreate;
        }

        public Integer getBRWaitingModify() {
            return bRWaitingModify;
        }

        public void setBRWaitingModify(Integer bRWaitingModify) {
            this.bRWaitingModify = bRWaitingModify;
        }

        public Integer getBRWaitingCancel() {
            return bRWaitingCancel;
        }

        public void setBRWaitingCancel(Integer bRWaitingCancel) {
            this.bRWaitingCancel = bRWaitingCancel;
        }

        public Integer getBRAgentQuataCreate() {
            return bRAgentQuataCreate;
        }

        public void setBRAgentQuataCreate(Integer bRAgentQuataCreate) {
            this.bRAgentQuataCreate = bRAgentQuataCreate;
        }

        public Integer getBRAgentQuataModify() {
            return bRAgentQuataModify;
        }

        public void setBRAgentQuataModify(Integer bRAgentQuataModify) {
            this.bRAgentQuataModify = bRAgentQuataModify;
        }

        public Integer getBRAgentQuataCancel() {
            return bRAgentQuataCancel;
        }

        public void setBRAgentQuataCancel(Integer bRAgentQuataCancel) {
            this.bRAgentQuataCancel = bRAgentQuataCancel;
        }

        public Integer getBROpenCreate() {
            return bROpenCreate;
        }

        public void setBROpenCreate(Integer bROpenCreate) {
            this.bROpenCreate = bROpenCreate;
        }

        public Integer getBROpenModify() {
            return bROpenModify;
        }

        public void setBROpenModify(Integer bROpenModify) {
            this.bROpenModify = bROpenModify;
        }

        public Integer getBROpenCancel() {
            return bROpenCancel;
        }

        public void setBROpenCancel(Integer bROpenCancel) {
            this.bROpenCancel = bROpenCancel;
        }

        public Integer getBRAPICreate() {
            return bRAPICreate;
        }

        public void setBRAPICreate(Integer bRAPICreate) {
            this.bRAPICreate = bRAPICreate;
        }

        public Integer getBRAPIModify() {
            return bRAPIModify;
        }

        public void setBRAPIModify(Integer bRAPIModify) {
            this.bRAPIModify = bRAPIModify;
        }

        public Integer getBRAPICancel() {
            return bRAPICancel;
        }

        public void setBRAPICancel(Integer bRAPICancel) {
            this.bRAPICancel = bRAPICancel;
        }

        public Integer getBRB2CCreate() {
            return bRB2CCreate;
        }

        public void setBRB2CCreate(Integer bRB2CCreate) {
            this.bRB2CCreate = bRB2CCreate;
        }

        public Integer getBRB2CModify() {
            return bRB2CModify;
        }

        public void setBRB2CModify(Integer bRB2CModify) {
            this.bRB2CModify = bRB2CModify;
        }

        public Integer getBRB2CCancel() {
            return bRB2CCancel;
        }

        public void setBRB2CCancel(Integer bRB2CCancel) {
            this.bRB2CCancel = bRB2CCancel;
        }

        public Integer getBROnlineAgentCreate() {
            return bROnlineAgentCreate;
        }

        public void setBROnlineAgentCreate(Integer bROnlineAgentCreate) {
            this.bROnlineAgentCreate = bROnlineAgentCreate;
        }

        public Integer getBROnlineAgentModify() {
            return bROnlineAgentModify;
        }

        public void setBROnlineAgentModify(Integer bROnlineAgentModify) {
            this.bROnlineAgentModify = bROnlineAgentModify;
        }

        public Integer getBROnlineAgentCancel() {
            return bROnlineAgentCancel;
        }

        public void setBROnlineAgentCancel(Integer bROnlineAgentCancel) {
            this.bROnlineAgentCancel = bROnlineAgentCancel;
        }

        public Integer getBROnlineAgentPhoneCreate() {
            return bROnlineAgentPhoneCreate;
        }

        public void setBROnlineAgentPhoneCreate(Integer bROnlineAgentPhoneCreate) {
            this.bROnlineAgentPhoneCreate = bROnlineAgentPhoneCreate;
        }

        public Integer getBROnlineAgentPhoneModify() {
            return bROnlineAgentPhoneModify;
        }

        public void setBROnlineAgentPhoneModify(Integer bROnlineAgentPhoneModify) {
            this.bROnlineAgentPhoneModify = bROnlineAgentPhoneModify;
        }

        public Integer getBROnlineAgentPhoneCancel() {
            return bROnlineAgentPhoneCancel;
        }

        public void setBROnlineAgentPhoneCancel(Integer bROnlineAgentPhoneCancel) {
            this.bROnlineAgentPhoneCancel = bROnlineAgentPhoneCancel;
        }

        public Integer getBRAPIPhoneCreate() {
            return bRAPIPhoneCreate;
        }

        public void setBRAPIPhoneCreate(Integer bRAPIPhoneCreate) {
            this.bRAPIPhoneCreate = bRAPIPhoneCreate;
        }

        public Integer getBRAPIPhoneModify() {
            return bRAPIPhoneModify;
        }

        public void setBRAPIPhoneModify(Integer bRAPIPhoneModify) {
            this.bRAPIPhoneModify = bRAPIPhoneModify;
        }

        public Integer getBRAPIPhoneCancel() {
            return bRAPIPhoneCancel;
        }

        public void setBRAPIPhoneCancel(Integer bRAPIPhoneCancel) {
            this.bRAPIPhoneCancel = bRAPIPhoneCancel;
        }

        public Integer getBROnlineAgentCardCreate() {
            return bROnlineAgentCardCreate;
        }

        public void setBROnlineAgentCardCreate(Integer bROnlineAgentCardCreate) {
            this.bROnlineAgentCardCreate = bROnlineAgentCardCreate;
        }

        public Integer getBROnlineAgentCardModify() {
            return bROnlineAgentCardModify;
        }

        public void setBROnlineAgentCardModify(Integer bROnlineAgentCardModify) {
            this.bROnlineAgentCardModify = bROnlineAgentCardModify;
        }

        public Integer getBROnlineAgentCardCancel() {
            return bROnlineAgentCardCancel;
        }

        public void setBROnlineAgentCardCancel(Integer bROnlineAgentCardCancel) {
            this.bROnlineAgentCardCancel = bROnlineAgentCardCancel;
        }

        public Integer getBOAConfirmModify() {
            return bOAConfirmModify;
        }

        public void setBOAConfirmModify(Integer bOAConfirmModify) {
            this.bOAConfirmModify = bOAConfirmModify;
        }

        public Integer getBOAConfirmCancel() {
            return bOAConfirmCancel;
        }

        public void setBOAConfirmCancel(Integer bOAConfirmCancel) {
            this.bOAConfirmCancel = bOAConfirmCancel;
        }

        public Integer getBOAPhoneModify() {
            return bOAPhoneModify;
        }

        public void setBOAPhoneModify(Integer bOAPhoneModify) {
            this.bOAPhoneModify = bOAPhoneModify;
        }

        public Integer getBOAPhoneCancel() {
            return bOAPhoneCancel;
        }

        public void setBOAPhoneCancel(Integer bOAPhoneCancel) {
            this.bOAPhoneCancel = bOAPhoneCancel;
        }

        public Integer getBOAAgentModify() {
            return bOAAgentModify;
        }

        public void setBOAAgentModify(Integer bOAAgentModify) {
            this.bOAAgentModify = bOAAgentModify;
        }

        public Integer getBOAAgentCancel() {
            return bOAAgentCancel;
        }

        public void setBOAAgentCancel(Integer bOAAgentCancel) {
            this.bOAAgentCancel = bOAAgentCancel;
        }

        public Integer getBOABranchModify() {
            return bOABranchModify;
        }

        public void setBOABranchModify(Integer bOABranchModify) {
            this.bOABranchModify = bOABranchModify;
        }

        public Integer getBOABranchCancel() {
            return bOABranchCancel;
        }

        public void setBOABranchCancel(Integer bOABranchCancel) {
            this.bOABranchCancel = bOABranchCancel;
        }

        public Integer getBOAGuestModify() {
            return bOAGuestModify;
        }

        public void setBOAGuestModify(Integer bOAGuestModify) {
            this.bOAGuestModify = bOAGuestModify;
        }

        public Integer getBOAGuestCancel() {
            return bOAGuestCancel;
        }

        public void setBOAGuestCancel(Integer bOAGuestCancel) {
            this.bOAGuestCancel = bOAGuestCancel;
        }

        public Integer getBOABankCardModify() {
            return bOABankCardModify;
        }

        public void setBOABankCardModify(Integer bOABankCardModify) {
            this.bOABankCardModify = bOABankCardModify;
        }

        public Integer getBOABankCardCancel() {
            return bOABankCardCancel;
        }

        public void setBOABankCardCancel(Integer bOABankCardCancel) {
            this.bOABankCardCancel = bOABankCardCancel;
        }

        public Integer getBOACompanyCardModify() {
            return bOACompanyCardModify;
        }

        public void setBOACompanyCardModify(Integer bOACompanyCardModify) {
            this.bOACompanyCardModify = bOACompanyCardModify;
        }

        public Integer getBOACompanyCardCancel() {
            return bOACompanyCardCancel;
        }

        public void setBOACompanyCardCancel(Integer bOACompanyCardCancel) {
            this.bOACompanyCardCancel = bOACompanyCardCancel;
        }

        public Integer getBOAWaitingModify() {
            return bOAWaitingModify;
        }

        public void setBOAWaitingModify(Integer bOAWaitingModify) {
            this.bOAWaitingModify = bOAWaitingModify;
        }

        public Integer getBOAWaitingCancel() {
            return bOAWaitingCancel;
        }

        public void setBOAWaitingCancel(Integer bOAWaitingCancel) {
            this.bOAWaitingCancel = bOAWaitingCancel;
        }

        public Integer getBOAAgentQuataModify() {
            return bOAAgentQuataModify;
        }

        public void setBOAAgentQuataModify(Integer bOAAgentQuataModify) {
            this.bOAAgentQuataModify = bOAAgentQuataModify;
        }

        public Integer getBOAAgentQuataCancel() {
            return bOAAgentQuataCancel;
        }

        public void setBOAAgentQuataCancel(Integer bOAAgentQuataCancel) {
            this.bOAAgentQuataCancel = bOAAgentQuataCancel;
        }

        public Integer getBOAOpenModify() {
            return bOAOpenModify;
        }

        public void setBOAOpenModify(Integer bOAOpenModify) {
            this.bOAOpenModify = bOAOpenModify;
        }

        public Integer getBOAOpenCancel() {
            return bOAOpenCancel;
        }

        public void setBOAOpenCancel(Integer bOAOpenCancel) {
            this.bOAOpenCancel = bOAOpenCancel;
        }

        public Integer getGAAllowReprint() {
            return gAAllowReprint;
        }

        public void setGAAllowReprint(Integer gAAllowReprint) {
            this.gAAllowReprint = gAAllowReprint;
        }

        public Integer getGAAllowBookingAFMin() {
            return gAAllowBookingAFMin;
        }

        public void setGAAllowBookingAFMin(Integer gAAllowBookingAFMin) {
            this.gAAllowBookingAFMin = gAAllowBookingAFMin;
        }

        public Integer getGACancellationBFMin() {
            return gACancellationBFMin;
        }

        public void setGACancellationBFMin(Integer gACancellationBFMin) {
            this.gACancellationBFMin = gACancellationBFMin;
        }

        public Integer getGAAllowFareChange() {
            return gAAllowFareChange;
        }

        public void setGAAllowFareChange(Integer gAAllowFareChange) {
            this.gAAllowFareChange = gAAllowFareChange;
        }

        public Integer getGAAllowBackDateBooking() {
            return gAAllowBackDateBooking;
        }

        public void setGAAllowBackDateBooking(Integer gAAllowBackDateBooking) {
            this.gAAllowBackDateBooking = gAAllowBackDateBooking;
        }

        public Integer getGAAllowBackDateCancellation() {
            return gAAllowBackDateCancellation;
        }

        public void setGAAllowBackDateCancellation(Integer gAAllowBackDateCancellation) {
            this.gAAllowBackDateCancellation = gAAllowBackDateCancellation;
        }

        public Integer getGAAllowDiscount() {
            return gAAllowDiscount;
        }

        public void setGAAllowDiscount(Integer gAAllowDiscount) {
            this.gAAllowDiscount = gAAllowDiscount;
        }

        public Integer getGAAllowInternetAuthorization() {
            return gAAllowInternetAuthorization;
        }

        public void setGAAllowInternetAuthorization(Integer gAAllowInternetAuthorization) {
            this.gAAllowInternetAuthorization = gAAllowInternetAuthorization;
        }

        public Integer getGAAllowModify() {
            return gAAllowModify;
        }

        public void setGAAllowModify(Integer gAAllowModify) {
            this.gAAllowModify = gAAllowModify;
        }

        public Integer getGAAllowRemoveQuota() {
            return gAAllowRemoveQuota;
        }

        public void setGAAllowRemoveQuota(Integer gAAllowRemoveQuota) {
            this.gAAllowRemoveQuota = gAAllowRemoveQuota;
        }

        public Integer getGAStopBooking() {
            return gAStopBooking;
        }

        public void setGAStopBooking(Integer gAStopBooking) {
            this.gAStopBooking = gAStopBooking;
        }

        public Integer getGAShowAllRoute() {
            return gAShowAllRoute;
        }

        public void setGAShowAllRoute(Integer gAShowAllRoute) {
            this.gAShowAllRoute = gAShowAllRoute;
        }

        public Integer getGAShowCollection() {
            return gAShowCollection;
        }

        public void setGAShowCollection(Integer gAShowCollection) {
            this.gAShowCollection = gAShowCollection;
        }

        public Integer getGAArrangementTransfer() {
            return gAArrangementTransfer;
        }

        public void setGAArrangementTransfer(Integer gAArrangementTransfer) {
            this.gAArrangementTransfer = gAArrangementTransfer;
        }

        public Integer getGAShowBackDate() {
            return gAShowBackDate;
        }

        public void setGAShowBackDate(Integer gAShowBackDate) {
            this.gAShowBackDate = gAShowBackDate;
        }

        public Integer getGARouteTransfer() {
            return gARouteTransfer;
        }

        public void setGARouteTransfer(Integer gARouteTransfer) {
            this.gARouteTransfer = gARouteTransfer;
        }

        public Integer getGAChartSMS() {
            return gAChartSMS;
        }

        public void setGAChartSMS(Integer gAChartSMS) {
            this.gAChartSMS = gAChartSMS;
        }

        public Integer getGABranchWiseCollection() {
            return gABranchWiseCollection;
        }

        public void setGABranchWiseCollection(Integer gABranchWiseCollection) {
            this.gABranchWiseCollection = gABranchWiseCollection;
        }

        public Integer getGAXmlSourceMemory() {
            return gAXmlSourceMemory;
        }

        public void setGAXmlSourceMemory(Integer gAXmlSourceMemory) {
            this.gAXmlSourceMemory = gAXmlSourceMemory;
        }

        public Integer getGAAllowMultipleBooking() {
            return gAAllowMultipleBooking;
        }

        public void setGAAllowMultipleBooking(Integer gAAllowMultipleBooking) {
            this.gAAllowMultipleBooking = gAAllowMultipleBooking;
        }

        public Integer getGAAllowModifyNamePhone() {
            return gAAllowModifyNamePhone;
        }

        public void setGAAllowModifyNamePhone(Integer gAAllowModifyNamePhone) {
            this.gAAllowModifyNamePhone = gAAllowModifyNamePhone;
        }

        public Integer getGAAllowModifyToCity() {
            return gAAllowModifyToCity;
        }

        public void setGAAllowModifyToCity(Integer gAAllowModifyToCity) {
            this.gAAllowModifyToCity = gAAllowModifyToCity;
        }

        public Integer getGAApplyRouteBFMin() {
            return gAApplyRouteBFMin;
        }

        public void setGAApplyRouteBFMin(Integer gAApplyRouteBFMin) {
            this.gAApplyRouteBFMin = gAApplyRouteBFMin;
        }

        public Integer getGAAllowConfirmToOpen() {
            return gAAllowConfirmToOpen;
        }

        public void setGAAllowConfirmToOpen(Integer gAAllowConfirmToOpen) {
            this.gAAllowConfirmToOpen = gAAllowConfirmToOpen;
        }

        public Integer getGAShowOnlineAgent() {
            return gAShowOnlineAgent;
        }

        public void setGAShowOnlineAgent(Integer gAShowOnlineAgent) {
            this.gAShowOnlineAgent = gAShowOnlineAgent;
        }

        public Integer getGAShowOnlyBranchCityAgent() {
            return gAShowOnlyBranchCityAgent;
        }

        public void setGAShowOnlyBranchCityAgent(Integer gAShowOnlyBranchCityAgent) {
            this.gAShowOnlyBranchCityAgent = gAShowOnlyBranchCityAgent;
        }

        public Integer getGASiteAccess() {
            return gASiteAccess;
        }

        public void setGASiteAccess(Integer gASiteAccess) {
            this.gASiteAccess = gASiteAccess;
        }

        public String getGASiteNames() {
            return gASiteNames;
        }

        public void setGASiteNames(String gASiteNames) {
            this.gASiteNames = gASiteNames;
        }

        public Integer getGAAllowPrepaidBranchBooking() {
            return gAAllowPrepaidBranchBooking;
        }

        public void setGAAllowPrepaidBranchBooking(Integer gAAllowPrepaidBranchBooking) {
            this.gAAllowPrepaidBranchBooking = gAAllowPrepaidBranchBooking;
        }

        public Integer getGAAllowChangeRefundCharges() {
            return gAAllowChangeRefundCharges;
        }

        public void setGAAllowChangeRefundCharges(Integer gAAllowChangeRefundCharges) {
            this.gAAllowChangeRefundCharges = gAAllowChangeRefundCharges;
        }

        public Integer getGAAllowLessRefundCharges() {
            return gAAllowLessRefundCharges;
        }

        public void setGAAllowLessRefundCharges(Integer gAAllowLessRefundCharges) {
            this.gAAllowLessRefundCharges = gAAllowLessRefundCharges;
        }

        public Integer getGAAllowPetrolPumpModule() {
            return gAAllowPetrolPumpModule;
        }

        public void setGAAllowPetrolPumpModule(Integer gAAllowPetrolPumpModule) {
            this.gAAllowPetrolPumpModule = gAAllowPetrolPumpModule;
        }

        public Integer getGAAllowKMEntry() {
            return gAAllowKMEntry;
        }

        public void setGAAllowKMEntry(Integer gAAllowKMEntry) {
            this.gAAllowKMEntry = gAAllowKMEntry;
        }

        public Integer getGAAllowReturnPhoneBooking() {
            return gAAllowReturnPhoneBooking;
        }

        public void setGAAllowReturnPhoneBooking(Integer gAAllowReturnPhoneBooking) {
            this.gAAllowReturnPhoneBooking = gAAllowReturnPhoneBooking;
        }

        public Integer getGAShowPrepaidBalance() {
            return gAShowPrepaidBalance;
        }

        public void setGAShowPrepaidBalance(Integer gAShowPrepaidBalance) {
            this.gAShowPrepaidBalance = gAShowPrepaidBalance;
        }

        public Integer getGAAllowLiveBusStatusEntry() {
            return gAAllowLiveBusStatusEntry;
        }

        public void setGAAllowLiveBusStatusEntry(Integer gAAllowLiveBusStatusEntry) {
            this.gAAllowLiveBusStatusEntry = gAAllowLiveBusStatusEntry;
        }

        public Integer getGAAllowLiveBusStatusEntryOtherPickup() {
            return gAAllowLiveBusStatusEntryOtherPickup;
        }

        public void setGAAllowLiveBusStatusEntryOtherPickup(Integer gAAllowLiveBusStatusEntryOtherPickup) {
            this.gAAllowLiveBusStatusEntryOtherPickup = gAAllowLiveBusStatusEntryOtherPickup;
        }

        public Integer getGAIsChangeBusStatusPickupTime() {
            return gAIsChangeBusStatusPickupTime;
        }

        public void setGAIsChangeBusStatusPickupTime(Integer gAIsChangeBusStatusPickupTime) {
            this.gAIsChangeBusStatusPickupTime = gAIsChangeBusStatusPickupTime;
        }

        public Integer getGAIsSendBranchAddressSMS() {
            return gAIsSendBranchAddressSMS;
        }

        public void setGAIsSendBranchAddressSMS(Integer gAIsSendBranchAddressSMS) {
            this.gAIsSendBranchAddressSMS = gAIsSendBranchAddressSMS;
        }

        public Integer getGAAllowModifyFromCity() {
            return gAAllowModifyFromCity;
        }

        public void setGAAllowModifyFromCity(Integer gAAllowModifyFromCity) {
            this.gAAllowModifyFromCity = gAAllowModifyFromCity;
        }

        public Integer getGAIsPromptBackDateBooking() {
            return gAIsPromptBackDateBooking;
        }

        public void setGAIsPromptBackDateBooking(Integer gAIsPromptBackDateBooking) {
            this.gAIsPromptBackDateBooking = gAIsPromptBackDateBooking;
        }

        public Integer getGAAllowBusHubRecharge() {
            return gAAllowBusHubRecharge;
        }

        public void setGAAllowBusHubRecharge(Integer gAAllowBusHubRecharge) {
            this.gAAllowBusHubRecharge = gAAllowBusHubRecharge;
        }

        public Integer getGARemoveSameCityHold() {
            return gARemoveSameCityHold;
        }

        public void setGARemoveSameCityHold(Integer gARemoveSameCityHold) {
            this.gARemoveSameCityHold = gARemoveSameCityHold;
        }

        public Integer getGARemoveOtherCityHold() {
            return gARemoveOtherCityHold;
        }

        public void setGARemoveOtherCityHold(Integer gARemoveOtherCityHold) {
            this.gARemoveOtherCityHold = gARemoveOtherCityHold;
        }

        public Integer getGAAllowNonReported() {
            return gAAllowNonReported;
        }

        public void setGAAllowNonReported(Integer gAAllowNonReported) {
            this.gAAllowNonReported = gAAllowNonReported;
        }

        public Integer getGAReleaseAPIB2CHold() {
            return gAReleaseAPIB2CHold;
        }

        public void setGAReleaseAPIB2CHold(Integer gAReleaseAPIB2CHold) {
            this.gAReleaseAPIB2CHold = gAReleaseAPIB2CHold;
        }

        public Integer getGAAllowFareChangeInPhoneModify() {
            return gAAllowFareChangeInPhoneModify;
        }

        public void setGAAllowFareChangeInPhoneModify(Integer gAAllowFareChangeInPhoneModify) {
            this.gAAllowFareChangeInPhoneModify = gAAllowFareChangeInPhoneModify;
        }

        public Integer getRAViewInquiry0() {
            return rAViewInquiry0;
        }

        public void setRAViewInquiry0(Integer rAViewInquiry0) {
            this.rAViewInquiry0 = rAViewInquiry0;
        }

        public Integer getRAViewInquiry1() {
            return rAViewInquiry1;
        }

        public void setRAViewInquiry1(Integer rAViewInquiry1) {
            this.rAViewInquiry1 = rAViewInquiry1;
        }

        public Integer getRAViewInquiry2() {
            return rAViewInquiry2;
        }

        public void setRAViewInquiry2(Integer rAViewInquiry2) {
            this.rAViewInquiry2 = rAViewInquiry2;
        }

        public Integer getRAViewInquiry3() {
            return rAViewInquiry3;
        }

        public void setRAViewInquiry3(Integer rAViewInquiry3) {
            this.rAViewInquiry3 = rAViewInquiry3;
        }

        public Integer getRAViewInquiry4() {
            return rAViewInquiry4;
        }

        public void setRAViewInquiry4(Integer rAViewInquiry4) {
            this.rAViewInquiry4 = rAViewInquiry4;
        }

        public Integer getRAViewInquiry5() {
            return rAViewInquiry5;
        }

        public void setRAViewInquiry5(Integer rAViewInquiry5) {
            this.rAViewInquiry5 = rAViewInquiry5;
        }

        public Integer getRAViewInquiry6() {
            return rAViewInquiry6;
        }

        public void setRAViewInquiry6(Integer rAViewInquiry6) {
            this.rAViewInquiry6 = rAViewInquiry6;
        }

        public Integer getRAViewInquiry7() {
            return rAViewInquiry7;
        }

        public void setRAViewInquiry7(Integer rAViewInquiry7) {
            this.rAViewInquiry7 = rAViewInquiry7;
        }

        public Integer getRAViewInquiry8() {
            return rAViewInquiry8;
        }

        public void setRAViewInquiry8(Integer rAViewInquiry8) {
            this.rAViewInquiry8 = rAViewInquiry8;
        }

        public Integer getRAViewInquiry9() {
            return rAViewInquiry9;
        }

        public void setRAViewInquiry9(Integer rAViewInquiry9) {
            this.rAViewInquiry9 = rAViewInquiry9;
        }

        public Integer getRAViewInquiry10() {
            return rAViewInquiry10;
        }

        public void setRAViewInquiry10(Integer rAViewInquiry10) {
            this.rAViewInquiry10 = rAViewInquiry10;
        }

        public Integer getRAViewInquiry11() {
            return rAViewInquiry11;
        }

        public void setRAViewInquiry11(Integer rAViewInquiry11) {
            this.rAViewInquiry11 = rAViewInquiry11;
        }

        public Integer getRAViewInquiry12() {
            return rAViewInquiry12;
        }

        public void setRAViewInquiry12(Integer rAViewInquiry12) {
            this.rAViewInquiry12 = rAViewInquiry12;
        }

        public Integer getRouteTimeWisePickupChart() {
            return routeTimeWisePickupChart;
        }

        public void setRouteTimeWisePickupChart(Integer routeTimeWisePickupChart) {
            this.routeTimeWisePickupChart = routeTimeWisePickupChart;
        }

        public Integer getSubRouteWiseChart() {
            return subRouteWiseChart;
        }

        public void setSubRouteWiseChart(Integer subRouteWiseChart) {
            this.subRouteWiseChart = subRouteWiseChart;
        }

        public Integer getBranchUserWiseCollectionRefundChart() {
            return branchUserWiseCollectionRefundChart;
        }

        public void setBranchUserWiseCollectionRefundChart(Integer branchUserWiseCollectionRefundChart) {
            this.branchUserWiseCollectionRefundChart = branchUserWiseCollectionRefundChart;
        }

        public Integer getSeatNumberWiseChart() {
            return seatNumberWiseChart;
        }

        public void setSeatNumberWiseChart(Integer seatNumberWiseChart) {
            this.seatNumberWiseChart = seatNumberWiseChart;
        }

        public Integer getAllDayBranchWiseReturnOnwardsMemo() {
            return allDayBranchWiseReturnOnwardsMemo;
        }

        public void setAllDayBranchWiseReturnOnwardsMemo(Integer allDayBranchWiseReturnOnwardsMemo) {
            this.allDayBranchWiseReturnOnwardsMemo = allDayBranchWiseReturnOnwardsMemo;
        }

        public Integer getMainRouteWiseChart() {
            return mainRouteWiseChart;
        }

        public void setMainRouteWiseChart(Integer mainRouteWiseChart) {
            this.mainRouteWiseChart = mainRouteWiseChart;
        }

        public Integer getSeatAllocationChart() {
            return seatAllocationChart;
        }

        public void setSeatAllocationChart(Integer seatAllocationChart) {
            this.seatAllocationChart = seatAllocationChart;
        }

        public Integer getTimeWiseBranchMemo() {
            return timeWiseBranchMemo;
        }

        public void setTimeWiseBranchMemo(Integer timeWiseBranchMemo) {
            this.timeWiseBranchMemo = timeWiseBranchMemo;
        }

        public Integer getTimeWiseAllBranchMemo() {
            return timeWiseAllBranchMemo;
        }

        public void setTimeWiseAllBranchMemo(Integer timeWiseAllBranchMemo) {
            this.timeWiseAllBranchMemo = timeWiseAllBranchMemo;
        }

        public Integer getAllDayMemo() {
            return allDayMemo;
        }

        public void setAllDayMemo(Integer allDayMemo) {
            this.allDayMemo = allDayMemo;
        }

        public Integer getAllDayBranchWiseMemo() {
            return allDayBranchWiseMemo;
        }

        public void setAllDayBranchWiseMemo(Integer allDayBranchWiseMemo) {
            this.allDayBranchWiseMemo = allDayBranchWiseMemo;
        }

        public Integer getCancelReportByRouteAndTime() {
            return cancelReportByRouteAndTime;
        }

        public void setCancelReportByRouteAndTime(Integer cancelReportByRouteAndTime) {
            this.cancelReportByRouteAndTime = cancelReportByRouteAndTime;
        }

        public Integer getSeatArrangementReport() {
            return seatArrangementReport;
        }

        public void setSeatArrangementReport(Integer seatArrangementReport) {
            this.seatArrangementReport = seatArrangementReport;
        }

        public Integer getRACityDropWise() {
            return rACityDropWise;
        }

        public void setRACityDropWise(Integer rACityDropWise) {
            this.rACityDropWise = rACityDropWise;
        }

        public Integer getRASubRouteTicketCount() {
            return rASubRouteTicketCount;
        }

        public void setRASubRouteTicketCount(Integer rASubRouteTicketCount) {
            this.rASubRouteTicketCount = rASubRouteTicketCount;
        }

        public Integer getRAPassengerBooking() {
            return rAPassengerBooking;
        }

        public void setRAPassengerBooking(Integer rAPassengerBooking) {
            this.rAPassengerBooking = rAPassengerBooking;
        }

        public Integer getRARouteDeparture() {
            return rARouteDeparture;
        }

        public void setRARouteDeparture(Integer rARouteDeparture) {
            this.rARouteDeparture = rARouteDeparture;
        }

        public Integer getRARouteTimeWiseMemo() {
            return rARouteTimeWiseMemo;
        }

        public void setRARouteTimeWiseMemo(Integer rARouteTimeWiseMemo) {
            this.rARouteTimeWiseMemo = rARouteTimeWiseMemo;
        }

        public Integer getRAPickupWiseInquiry() {
            return rAPickupWiseInquiry;
        }

        public void setRAPickupWiseInquiry(Integer rAPickupWiseInquiry) {
            this.rAPickupWiseInquiry = rAPickupWiseInquiry;
        }

        public Integer getRAMainRouteWisePassengerDetails() {
            return rAMainRouteWisePassengerDetails;
        }

        public void setRAMainRouteWisePassengerDetails(Integer rAMainRouteWisePassengerDetails) {
            this.rAMainRouteWisePassengerDetails = rAMainRouteWisePassengerDetails;
        }

        public Integer getRARouteTimeBookingMemo() {
            return rARouteTimeBookingMemo;
        }

        public void setRARouteTimeBookingMemo(Integer rARouteTimeBookingMemo) {
            this.rARouteTimeBookingMemo = rARouteTimeBookingMemo;
        }

        public Integer getRABusIncomeReceipt() {
            return rABusIncomeReceipt;
        }

        public void setRABusIncomeReceipt(Integer rABusIncomeReceipt) {
            this.rABusIncomeReceipt = rABusIncomeReceipt;
        }

        public Integer getRAAllowCityWiseSeatChartReport() {
            return rAAllowCityWiseSeatChartReport;
        }

        public void setRAAllowCityWiseSeatChartReport(Integer rAAllowCityWiseSeatChartReport) {
            this.rAAllowCityWiseSeatChartReport = rAAllowCityWiseSeatChartReport;
        }

        public Integer getRAAllowBulkTicketPrint() {
            return rAAllowBulkTicketPrint;
        }

        public void setRAAllowBulkTicketPrint(Integer rAAllowBulkTicketPrint) {
            this.rAAllowBulkTicketPrint = rAAllowBulkTicketPrint;
        }

        public Integer getCityPickupDropWiseChart() {
            return cityPickupDropWiseChart;
        }

        public void setCityPickupDropWiseChart(Integer cityPickupDropWiseChart) {
            this.cityPickupDropWiseChart = cityPickupDropWiseChart;
        }

        public Integer getDailyRouteTimeWiseMemo() {
            return dailyRouteTimeWiseMemo;
        }

        public void setDailyRouteTimeWiseMemo(Integer dailyRouteTimeWiseMemo) {
            this.dailyRouteTimeWiseMemo = dailyRouteTimeWiseMemo;
        }

        public Integer getFutureBookingGraph() {
            return futureBookingGraph;
        }

        public void setFutureBookingGraph(Integer futureBookingGraph) {
            this.futureBookingGraph = futureBookingGraph;
        }

        public Integer getPickupDropReport() {
            return pickupDropReport;
        }

        public void setPickupDropReport(Integer pickupDropReport) {
            this.pickupDropReport = pickupDropReport;
        }

        public Integer getRAPNRWiseAmenities() {
            return rAPNRWiseAmenities;
        }

        public void setRAPNRWiseAmenities(Integer rAPNRWiseAmenities) {
            this.rAPNRWiseAmenities = rAPNRWiseAmenities;
        }

        public Integer getRARoutePickUpDropChart() {
            return rARoutePickUpDropChart;
        }

        public void setRARoutePickUpDropChart(Integer rARoutePickUpDropChart) {
            this.rARoutePickUpDropChart = rARoutePickUpDropChart;
        }

        public String getOSAdlabsURL() {
            return oSAdlabsURL;
        }

        public void setOSAdlabsURL(String oSAdlabsURL) {
            this.oSAdlabsURL = oSAdlabsURL;
        }

        public Integer getOSIsLabelDisplay() {
            return oSIsLabelDisplay;
        }

        public void setOSIsLabelDisplay(Integer oSIsLabelDisplay) {
            this.oSIsLabelDisplay = oSIsLabelDisplay;
        }

        public String getOSLableText() {
            return oSLableText;
        }

        public void setOSLableText(String oSLableText) {
            this.oSLableText = oSLableText;
        }

    }


    public class OtherCompanyBookingRight {

        @SerializedName("From_CM_CompanyID")
        @Expose
        private Integer fromCMCompanyID;
        @SerializedName("To_CM_CompanyID")
        @Expose
        private Integer toCMCompanyID;
        @SerializedName("OCBR_ConfirmCreate")
        @Expose
        private Integer oCBRConfirmCreate;
        @SerializedName("OCBR_ConfirmModify")
        @Expose
        private Integer oCBRConfirmModify;
        @SerializedName("OCBR_ConfirmCancel")
        @Expose
        private Integer oCBRConfirmCancel;
        @SerializedName("OCBR_PhoneCreate")
        @Expose
        private Integer oCBRPhoneCreate;
        @SerializedName("OCBR_PhoneModify")
        @Expose
        private Integer oCBRPhoneModify;
        @SerializedName("OCBR_PhoneCancel")
        @Expose
        private Integer oCBRPhoneCancel;
        @SerializedName("OCBR_AgentCreate")
        @Expose
        private Integer oCBRAgentCreate;
        @SerializedName("OCBR_AgentModify")
        @Expose
        private Integer oCBRAgentModify;
        @SerializedName("OCBR_AgentCancel")
        @Expose
        private Integer oCBRAgentCancel;
        @SerializedName("OCBR_BranchCreate")
        @Expose
        private Integer oCBRBranchCreate;
        @SerializedName("OCBR_BranchModify")
        @Expose
        private Integer oCBRBranchModify;
        @SerializedName("OCBR_BranchCancel")
        @Expose
        private Integer oCBRBranchCancel;
        @SerializedName("OCBR_GuestCreate")
        @Expose
        private Integer oCBRGuestCreate;
        @SerializedName("OCBR_GuestModify")
        @Expose
        private Integer oCBRGuestModify;
        @SerializedName("OCBR_GuestCancel")
        @Expose
        private Integer oCBRGuestCancel;
        @SerializedName("OCBR_BankCardCreate")
        @Expose
        private Integer oCBRBankCardCreate;
        @SerializedName("OCBR_BankCardModify")
        @Expose
        private Integer oCBRBankCardModify;
        @SerializedName("OCBR_BankCardCancel")
        @Expose
        private Integer oCBRBankCardCancel;
        @SerializedName("OCBR_CompanyCardCreate")
        @Expose
        private Integer oCBRCompanyCardCreate;
        @SerializedName("OCBR_CompanyCardModify")
        @Expose
        private Integer oCBRCompanyCardModify;
        @SerializedName("OCBR_CompanyCardCancel")
        @Expose
        private Integer oCBRCompanyCardCancel;
        @SerializedName("OCBR_WaitingCreate")
        @Expose
        private Integer oCBRWaitingCreate;
        @SerializedName("OCBR_WaitingModify")
        @Expose
        private Integer oCBRWaitingModify;
        @SerializedName("OCBR_WaitingCancel")
        @Expose
        private Integer oCBRWaitingCancel;
        @SerializedName("OCBR_AgentQuotaCreate")
        @Expose
        private Integer oCBRAgentQuotaCreate;
        @SerializedName("OCBR_AgentQuotaModify")
        @Expose
        private Integer oCBRAgentQuotaModify;
        @SerializedName("OCBR_AgentQuotaCancel")
        @Expose
        private Integer oCBRAgentQuotaCancel;
        @SerializedName("OCBR_OpenCreate")
        @Expose
        private Integer oCBROpenCreate;
        @SerializedName("OCBR_OpenModify")
        @Expose
        private Integer oCBROpenModify;
        @SerializedName("OCBR_OpenCancel")
        @Expose
        private Integer oCBROpenCancel;
        @SerializedName("OCBR_IsTDS")
        @Expose
        private Integer oCBRIsTDS;
        @SerializedName("OCBR_TDS")
        @Expose
        private Double oCBRTDS;
        @SerializedName("OCBR_Confirm")
        @Expose
        private Integer oCBRConfirm;
        @SerializedName("OCBR_Phone")
        @Expose
        private Integer oCBRPhone;
        @SerializedName("OCBR_Agent")
        @Expose
        private Integer oCBRAgent;
        @SerializedName("OCBR_Branch")
        @Expose
        private Integer oCBRBranch;
        @SerializedName("OCBR_Guest")
        @Expose
        private Integer oCBRGuest;
        @SerializedName("OCBR_Waiting")
        @Expose
        private Integer oCBRWaiting;
        @SerializedName("OCBR_BankCard")
        @Expose
        private Integer oCBRBankCard;
        @SerializedName("OCBR_CompanyCard")
        @Expose
        private Integer oCBRCompanyCard;
        @SerializedName("OCBR_AgentQuota")
        @Expose
        private Integer oCBRAgentQuota;
        @SerializedName("OCBR_Open")
        @Expose
        private Integer oCBROpen;
        @SerializedName("CM_IsACServiceTax")
        @Expose
        private Integer cMIsACServiceTax;
        @SerializedName("CM_IsNonACServiceTax")
        @Expose
        private Integer cMIsNonACServiceTax;
        @SerializedName("CM_ACServiceTax")
        @Expose
        private Double cMACServiceTax;
        @SerializedName("CM_NonACServiceTax")
        @Expose
        private Double cMNonACServiceTax;
        @SerializedName("CM_ACFareIncludeTax")
        @Expose
        private Integer cMACFareIncludeTax;
        @SerializedName("CM_NonACFareIncludeTax")
        @Expose
        private Integer cMNonACFareIncludeTax;
        @SerializedName("CM_IsSTOffline")
        @Expose
        private Integer cMIsSTOffline;
        @SerializedName("CM_IsSTLogin")
        @Expose
        private Integer cMIsSTLogin;
        @SerializedName("CM_IsSTAPI")
        @Expose
        private Integer cMIsSTAPI;
        @SerializedName("CM_IsSTB2C")
        @Expose
        private Integer cMIsSTB2C;
        @SerializedName("CM_IsSTBranch")
        @Expose
        private Integer cMIsSTBranch;
        @SerializedName("CM_IsSTBranch1")
        @Expose
        private Integer cMIsSTBranch1;
        @SerializedName("OCBR_AllowModifyPhoneOfRouteCompany")
        @Expose
        private Integer oCBRAllowModifyPhoneOfRouteCompany;
        @SerializedName("CM_ServiceTaxRoundUp")
        @Expose
        private Integer cMServiceTaxRoundUp;
        @SerializedName("CM_ACPackageServiceTax")
        @Expose
        private Double cMACPackageServiceTax;
        @SerializedName("CM_NonACPackageServiceTax")
        @Expose
        private Double cMNonACPackageServiceTax;
        @SerializedName("CM_ACOwnScheduleServiceTax")
        @Expose
        private Double cMACOwnScheduleServiceTax;
        @SerializedName("CM_NonACOwnScheduleServiceTax")
        @Expose
        private Double cMNonACOwnScheduleServiceTax;
        @SerializedName("OCBR_AllowCouponCode")
        @Expose
        private Integer oCBRAllowCouponCode;

        public Integer getFromCMCompanyID() {
            return fromCMCompanyID;
        }

        public void setFromCMCompanyID(Integer fromCMCompanyID) {
            this.fromCMCompanyID = fromCMCompanyID;
        }

        public Integer getToCMCompanyID() {
            return toCMCompanyID;
        }

        public void setToCMCompanyID(Integer toCMCompanyID) {
            this.toCMCompanyID = toCMCompanyID;
        }

        public Integer getOCBRConfirmCreate() {
            return oCBRConfirmCreate;
        }

        public void setOCBRConfirmCreate(Integer oCBRConfirmCreate) {
            this.oCBRConfirmCreate = oCBRConfirmCreate;
        }

        public Integer getOCBRConfirmModify() {
            return oCBRConfirmModify;
        }

        public void setOCBRConfirmModify(Integer oCBRConfirmModify) {
            this.oCBRConfirmModify = oCBRConfirmModify;
        }

        public Integer getOCBRConfirmCancel() {
            return oCBRConfirmCancel;
        }

        public void setOCBRConfirmCancel(Integer oCBRConfirmCancel) {
            this.oCBRConfirmCancel = oCBRConfirmCancel;
        }

        public Integer getOCBRPhoneCreate() {
            return oCBRPhoneCreate;
        }

        public void setOCBRPhoneCreate(Integer oCBRPhoneCreate) {
            this.oCBRPhoneCreate = oCBRPhoneCreate;
        }

        public Integer getOCBRPhoneModify() {
            return oCBRPhoneModify;
        }

        public void setOCBRPhoneModify(Integer oCBRPhoneModify) {
            this.oCBRPhoneModify = oCBRPhoneModify;
        }

        public Integer getOCBRPhoneCancel() {
            return oCBRPhoneCancel;
        }

        public void setOCBRPhoneCancel(Integer oCBRPhoneCancel) {
            this.oCBRPhoneCancel = oCBRPhoneCancel;
        }

        public Integer getOCBRAgentCreate() {
            return oCBRAgentCreate;
        }

        public void setOCBRAgentCreate(Integer oCBRAgentCreate) {
            this.oCBRAgentCreate = oCBRAgentCreate;
        }

        public Integer getOCBRAgentModify() {
            return oCBRAgentModify;
        }

        public void setOCBRAgentModify(Integer oCBRAgentModify) {
            this.oCBRAgentModify = oCBRAgentModify;
        }

        public Integer getOCBRAgentCancel() {
            return oCBRAgentCancel;
        }

        public void setOCBRAgentCancel(Integer oCBRAgentCancel) {
            this.oCBRAgentCancel = oCBRAgentCancel;
        }

        public Integer getOCBRBranchCreate() {
            return oCBRBranchCreate;
        }

        public void setOCBRBranchCreate(Integer oCBRBranchCreate) {
            this.oCBRBranchCreate = oCBRBranchCreate;
        }

        public Integer getOCBRBranchModify() {
            return oCBRBranchModify;
        }

        public void setOCBRBranchModify(Integer oCBRBranchModify) {
            this.oCBRBranchModify = oCBRBranchModify;
        }

        public Integer getOCBRBranchCancel() {
            return oCBRBranchCancel;
        }

        public void setOCBRBranchCancel(Integer oCBRBranchCancel) {
            this.oCBRBranchCancel = oCBRBranchCancel;
        }

        public Integer getOCBRGuestCreate() {
            return oCBRGuestCreate;
        }

        public void setOCBRGuestCreate(Integer oCBRGuestCreate) {
            this.oCBRGuestCreate = oCBRGuestCreate;
        }

        public Integer getOCBRGuestModify() {
            return oCBRGuestModify;
        }

        public void setOCBRGuestModify(Integer oCBRGuestModify) {
            this.oCBRGuestModify = oCBRGuestModify;
        }

        public Integer getOCBRGuestCancel() {
            return oCBRGuestCancel;
        }

        public void setOCBRGuestCancel(Integer oCBRGuestCancel) {
            this.oCBRGuestCancel = oCBRGuestCancel;
        }

        public Integer getOCBRBankCardCreate() {
            return oCBRBankCardCreate;
        }

        public void setOCBRBankCardCreate(Integer oCBRBankCardCreate) {
            this.oCBRBankCardCreate = oCBRBankCardCreate;
        }

        public Integer getOCBRBankCardModify() {
            return oCBRBankCardModify;
        }

        public void setOCBRBankCardModify(Integer oCBRBankCardModify) {
            this.oCBRBankCardModify = oCBRBankCardModify;
        }

        public Integer getOCBRBankCardCancel() {
            return oCBRBankCardCancel;
        }

        public void setOCBRBankCardCancel(Integer oCBRBankCardCancel) {
            this.oCBRBankCardCancel = oCBRBankCardCancel;
        }

        public Integer getOCBRCompanyCardCreate() {
            return oCBRCompanyCardCreate;
        }

        public void setOCBRCompanyCardCreate(Integer oCBRCompanyCardCreate) {
            this.oCBRCompanyCardCreate = oCBRCompanyCardCreate;
        }

        public Integer getOCBRCompanyCardModify() {
            return oCBRCompanyCardModify;
        }

        public void setOCBRCompanyCardModify(Integer oCBRCompanyCardModify) {
            this.oCBRCompanyCardModify = oCBRCompanyCardModify;
        }

        public Integer getOCBRCompanyCardCancel() {
            return oCBRCompanyCardCancel;
        }

        public void setOCBRCompanyCardCancel(Integer oCBRCompanyCardCancel) {
            this.oCBRCompanyCardCancel = oCBRCompanyCardCancel;
        }

        public Integer getOCBRWaitingCreate() {
            return oCBRWaitingCreate;
        }

        public void setOCBRWaitingCreate(Integer oCBRWaitingCreate) {
            this.oCBRWaitingCreate = oCBRWaitingCreate;
        }

        public Integer getOCBRWaitingModify() {
            return oCBRWaitingModify;
        }

        public void setOCBRWaitingModify(Integer oCBRWaitingModify) {
            this.oCBRWaitingModify = oCBRWaitingModify;
        }

        public Integer getOCBRWaitingCancel() {
            return oCBRWaitingCancel;
        }

        public void setOCBRWaitingCancel(Integer oCBRWaitingCancel) {
            this.oCBRWaitingCancel = oCBRWaitingCancel;
        }

        public Integer getOCBRAgentQuotaCreate() {
            return oCBRAgentQuotaCreate;
        }

        public void setOCBRAgentQuotaCreate(Integer oCBRAgentQuotaCreate) {
            this.oCBRAgentQuotaCreate = oCBRAgentQuotaCreate;
        }

        public Integer getOCBRAgentQuotaModify() {
            return oCBRAgentQuotaModify;
        }

        public void setOCBRAgentQuotaModify(Integer oCBRAgentQuotaModify) {
            this.oCBRAgentQuotaModify = oCBRAgentQuotaModify;
        }

        public Integer getOCBRAgentQuotaCancel() {
            return oCBRAgentQuotaCancel;
        }

        public void setOCBRAgentQuotaCancel(Integer oCBRAgentQuotaCancel) {
            this.oCBRAgentQuotaCancel = oCBRAgentQuotaCancel;
        }

        public Integer getOCBROpenCreate() {
            return oCBROpenCreate;
        }

        public void setOCBROpenCreate(Integer oCBROpenCreate) {
            this.oCBROpenCreate = oCBROpenCreate;
        }

        public Integer getOCBROpenModify() {
            return oCBROpenModify;
        }

        public void setOCBROpenModify(Integer oCBROpenModify) {
            this.oCBROpenModify = oCBROpenModify;
        }

        public Integer getOCBROpenCancel() {
            return oCBROpenCancel;
        }

        public void setOCBROpenCancel(Integer oCBROpenCancel) {
            this.oCBROpenCancel = oCBROpenCancel;
        }

        public Integer getOCBRIsTDS() {
            return oCBRIsTDS;
        }

        public void setOCBRIsTDS(Integer oCBRIsTDS) {
            this.oCBRIsTDS = oCBRIsTDS;
        }

        public Double getOCBRTDS() {
            return oCBRTDS;
        }

        public void setOCBRTDS(Double oCBRTDS) {
            this.oCBRTDS = oCBRTDS;
        }

        public Integer getOCBRConfirm() {
            return oCBRConfirm;
        }

        public void setOCBRConfirm(Integer oCBRConfirm) {
            this.oCBRConfirm = oCBRConfirm;
        }

        public Integer getOCBRPhone() {
            return oCBRPhone;
        }

        public void setOCBRPhone(Integer oCBRPhone) {
            this.oCBRPhone = oCBRPhone;
        }

        public Integer getOCBRAgent() {
            return oCBRAgent;
        }

        public void setOCBRAgent(Integer oCBRAgent) {
            this.oCBRAgent = oCBRAgent;
        }

        public Integer getOCBRBranch() {
            return oCBRBranch;
        }

        public void setOCBRBranch(Integer oCBRBranch) {
            this.oCBRBranch = oCBRBranch;
        }

        public Integer getOCBRGuest() {
            return oCBRGuest;
        }

        public void setOCBRGuest(Integer oCBRGuest) {
            this.oCBRGuest = oCBRGuest;
        }

        public Integer getOCBRWaiting() {
            return oCBRWaiting;
        }

        public void setOCBRWaiting(Integer oCBRWaiting) {
            this.oCBRWaiting = oCBRWaiting;
        }

        public Integer getOCBRBankCard() {
            return oCBRBankCard;
        }

        public void setOCBRBankCard(Integer oCBRBankCard) {
            this.oCBRBankCard = oCBRBankCard;
        }

        public Integer getOCBRCompanyCard() {
            return oCBRCompanyCard;
        }

        public void setOCBRCompanyCard(Integer oCBRCompanyCard) {
            this.oCBRCompanyCard = oCBRCompanyCard;
        }

        public Integer getOCBRAgentQuota() {
            return oCBRAgentQuota;
        }

        public void setOCBRAgentQuota(Integer oCBRAgentQuota) {
            this.oCBRAgentQuota = oCBRAgentQuota;
        }

        public Integer getOCBROpen() {
            return oCBROpen;
        }

        public void setOCBROpen(Integer oCBROpen) {
            this.oCBROpen = oCBROpen;
        }

        public Integer getCMIsACServiceTax() {
            return cMIsACServiceTax;
        }

        public void setCMIsACServiceTax(Integer cMIsACServiceTax) {
            this.cMIsACServiceTax = cMIsACServiceTax;
        }

        public Integer getCMIsNonACServiceTax() {
            return cMIsNonACServiceTax;
        }

        public void setCMIsNonACServiceTax(Integer cMIsNonACServiceTax) {
            this.cMIsNonACServiceTax = cMIsNonACServiceTax;
        }

        public Double getCMACServiceTax() {
            return cMACServiceTax;
        }

        public void setCMACServiceTax(Double cMACServiceTax) {
            this.cMACServiceTax = cMACServiceTax;
        }

        public Double getCMNonACServiceTax() {
            return cMNonACServiceTax;
        }

        public void setCMNonACServiceTax(Double cMNonACServiceTax) {
            this.cMNonACServiceTax = cMNonACServiceTax;
        }

        public Integer getCMACFareIncludeTax() {
            return cMACFareIncludeTax;
        }

        public void setCMACFareIncludeTax(Integer cMACFareIncludeTax) {
            this.cMACFareIncludeTax = cMACFareIncludeTax;
        }

        public Integer getCMNonACFareIncludeTax() {
            return cMNonACFareIncludeTax;
        }

        public void setCMNonACFareIncludeTax(Integer cMNonACFareIncludeTax) {
            this.cMNonACFareIncludeTax = cMNonACFareIncludeTax;
        }

        public Integer getCMIsSTOffline() {
            return cMIsSTOffline;
        }

        public void setCMIsSTOffline(Integer cMIsSTOffline) {
            this.cMIsSTOffline = cMIsSTOffline;
        }

        public Integer getCMIsSTLogin() {
            return cMIsSTLogin;
        }

        public void setCMIsSTLogin(Integer cMIsSTLogin) {
            this.cMIsSTLogin = cMIsSTLogin;
        }

        public Integer getCMIsSTAPI() {
            return cMIsSTAPI;
        }

        public void setCMIsSTAPI(Integer cMIsSTAPI) {
            this.cMIsSTAPI = cMIsSTAPI;
        }

        public Integer getCMIsSTB2C() {
            return cMIsSTB2C;
        }

        public void setCMIsSTB2C(Integer cMIsSTB2C) {
            this.cMIsSTB2C = cMIsSTB2C;
        }

        public Integer getCMIsSTBranch() {
            return cMIsSTBranch;
        }

        public void setCMIsSTBranch(Integer cMIsSTBranch) {
            this.cMIsSTBranch = cMIsSTBranch;
        }

        public Integer getCMIsSTBranch1() {
            return cMIsSTBranch1;
        }

        public void setCMIsSTBranch1(Integer cMIsSTBranch1) {
            this.cMIsSTBranch1 = cMIsSTBranch1;
        }

        public Integer getOCBRAllowModifyPhoneOfRouteCompany() {
            return oCBRAllowModifyPhoneOfRouteCompany;
        }

        public void setOCBRAllowModifyPhoneOfRouteCompany(Integer oCBRAllowModifyPhoneOfRouteCompany) {
            this.oCBRAllowModifyPhoneOfRouteCompany = oCBRAllowModifyPhoneOfRouteCompany;
        }

        public Integer getCMServiceTaxRoundUp() {
            return cMServiceTaxRoundUp;
        }

        public void setCMServiceTaxRoundUp(Integer cMServiceTaxRoundUp) {
            this.cMServiceTaxRoundUp = cMServiceTaxRoundUp;
        }

        public Double getCMACPackageServiceTax() {
            return cMACPackageServiceTax;
        }

        public void setCMACPackageServiceTax(Double cMACPackageServiceTax) {
            this.cMACPackageServiceTax = cMACPackageServiceTax;
        }

        public Double getCMNonACPackageServiceTax() {
            return cMNonACPackageServiceTax;
        }

        public void setCMNonACPackageServiceTax(Double cMNonACPackageServiceTax) {
            this.cMNonACPackageServiceTax = cMNonACPackageServiceTax;
        }

        public Double getCMACOwnScheduleServiceTax() {
            return cMACOwnScheduleServiceTax;
        }

        public void setCMACOwnScheduleServiceTax(Double cMACOwnScheduleServiceTax) {
            this.cMACOwnScheduleServiceTax = cMACOwnScheduleServiceTax;
        }

        public Double getCMNonACOwnScheduleServiceTax() {
            return cMNonACOwnScheduleServiceTax;
        }

        public void setCMNonACOwnScheduleServiceTax(Double cMNonACOwnScheduleServiceTax) {
            this.cMNonACOwnScheduleServiceTax = cMNonACOwnScheduleServiceTax;
        }

        public Integer getOCBRAllowCouponCode() {
            return oCBRAllowCouponCode;
        }

        public void setOCBRAllowCouponCode(Integer oCBRAllowCouponCode) {
            this.oCBRAllowCouponCode = oCBRAllowCouponCode;
        }

    }

    public class OtherSetting {

        @SerializedName("OS_IsPendingAmount")
        @Expose
        private Integer oSIsPendingAmount;
        @SerializedName("OS_MultiBookAGQuota")
        @Expose
        private Integer oSMultiBookAGQuota;
        @SerializedName("PastBookingByMobileNo")
        @Expose
        private Integer pastBookingByMobileNo;
        @SerializedName("LastLocationByBusNo")
        @Expose
        private Integer lastLocationByBusNo;
        @SerializedName("ChangeGSTDetail")
        @Expose
        private Integer changeGSTDetail;
        @SerializedName("PrintGSTInvoice")
        @Expose
        private Integer printGSTInvoice;
        @SerializedName("ESendGSTInvoice")
        @Expose
        private Integer eSendGSTInvoice;

        public Integer getOSIsPendingAmount() {
            return oSIsPendingAmount;
        }

        public void setOSIsPendingAmount(Integer oSIsPendingAmount) {
            this.oSIsPendingAmount = oSIsPendingAmount;
        }

        public Integer getOSMultiBookAGQuota() {
            return oSMultiBookAGQuota;
        }

        public void setOSMultiBookAGQuota(Integer oSMultiBookAGQuota) {
            this.oSMultiBookAGQuota = oSMultiBookAGQuota;
        }

        public Integer getPastBookingByMobileNo() {
            return pastBookingByMobileNo;
        }

        public void setPastBookingByMobileNo(Integer pastBookingByMobileNo) {
            this.pastBookingByMobileNo = pastBookingByMobileNo;
        }

        public Integer getLastLocationByBusNo() {
            return lastLocationByBusNo;
        }

        public void setLastLocationByBusNo(Integer lastLocationByBusNo) {
            this.lastLocationByBusNo = lastLocationByBusNo;
        }

        public Integer getChangeGSTDetail() {
            return changeGSTDetail;
        }

        public void setChangeGSTDetail(Integer changeGSTDetail) {
            this.changeGSTDetail = changeGSTDetail;
        }

        public Integer getPrintGSTInvoice() {
            return printGSTInvoice;
        }

        public void setPrintGSTInvoice(Integer printGSTInvoice) {
            this.printGSTInvoice = printGSTInvoice;
        }

        public Integer getESendGSTInvoice() {
            return eSendGSTInvoice;
        }

        public void setESendGSTInvoice(Integer eSendGSTInvoice) {
            this.eSendGSTInvoice = eSendGSTInvoice;
        }

    }

}
