package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CouponCodeDetails_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("JM_PassengerName")
        @Expose
        private String jMPassengerName;
        @SerializedName("JM_Phone")
        @Expose
        private String jMPhone;
        @SerializedName("CC_Amount")
        @Expose
        private Double cCAmount;
        @SerializedName("CC_CouponCode")
        @Expose
        private String cCCouponCode;
        @SerializedName("CC_IsActive")
        @Expose
        private Integer cCIsActive;
        @SerializedName("CC_MinimumAmount")
        @Expose
        private Double cCMinimumAmount;
        @SerializedName("CC_DiscountType")
        @Expose
        private Integer cCDiscountType;
        @SerializedName("ISSpecialDiscountScheme")
        @Expose
        private Integer iSSpecialDiscountScheme;
        @SerializedName("TillSeatDiscount")
        @Expose
        private Integer tillSeatDiscount;
        @SerializedName("DisAmtPer")
        @Expose
        private Double disAmtPer;
        @SerializedName("CC_Dis_Type")
        @Expose
        private Integer cCDisType;
        @SerializedName("ISOff")
        @Expose
        private Integer iSOff;
        @SerializedName("CC_IsBulkCoupon")
        @Expose
        private Integer cCIsBulkCoupon;
        @SerializedName("MaxSeat")
        @Expose
        private Integer maxSeat;
        @SerializedName("Details")
        @Expose
        private String details;
        @SerializedName("CC_DiscountDesc")
        @Expose
        private String cCDiscountDesc;
        @SerializedName("CC_MaxDisCount")
        @Expose
        private String cCMaxDisCount;
        @SerializedName("JM_SeatString")
        @Expose
        private String jMSeatString;

        public String getJMPassengerName() {
            return jMPassengerName;
        }

        public void setJMPassengerName(String jMPassengerName) {
            this.jMPassengerName = jMPassengerName;
        }

        public String getJMPhone() {
            return jMPhone;
        }

        public void setJMPhone(String jMPhone) {
            this.jMPhone = jMPhone;
        }

        public Double getCCAmount() {
            return cCAmount;
        }

        public void setCCAmount(Double cCAmount) {
            this.cCAmount = cCAmount;
        }

        public String getCCCouponCode() {
            return cCCouponCode;
        }

        public void setCCCouponCode(String cCCouponCode) {
            this.cCCouponCode = cCCouponCode;
        }

        public Integer getCCIsActive() {
            return cCIsActive;
        }

        public void setCCIsActive(Integer cCIsActive) {
            this.cCIsActive = cCIsActive;
        }

        public Double getCCMinimumAmount() {
            return cCMinimumAmount;
        }

        public void setCCMinimumAmount(Double cCMinimumAmount) {
            this.cCMinimumAmount = cCMinimumAmount;
        }

        public Integer getCCDiscountType() {
            return cCDiscountType;
        }

        public void setCCDiscountType(Integer cCDiscountType) {
            this.cCDiscountType = cCDiscountType;
        }

        public Integer getISSpecialDiscountScheme() {
            return iSSpecialDiscountScheme;
        }

        public void setISSpecialDiscountScheme(Integer iSSpecialDiscountScheme) {
            this.iSSpecialDiscountScheme = iSSpecialDiscountScheme;
        }

        public Integer getTillSeatDiscount() {
            return tillSeatDiscount;
        }

        public void setTillSeatDiscount(Integer tillSeatDiscount) {
            this.tillSeatDiscount = tillSeatDiscount;
        }

        public Double getDisAmtPer() {
            return disAmtPer;
        }

        public void setDisAmtPer(Double disAmtPer) {
            this.disAmtPer = disAmtPer;
        }

        public Integer getCCDisType() {
            return cCDisType;
        }

        public void setCCDisType(Integer cCDisType) {
            this.cCDisType = cCDisType;
        }

        public Integer getISOff() {
            return iSOff;
        }

        public void setISOff(Integer iSOff) {
            this.iSOff = iSOff;
        }

        public Integer getCCIsBulkCoupon() {
            return cCIsBulkCoupon;
        }

        public void setCCIsBulkCoupon(Integer cCIsBulkCoupon) {
            this.cCIsBulkCoupon = cCIsBulkCoupon;
        }

        public Integer getMaxSeat() {
            return maxSeat;
        }

        public void setMaxSeat(Integer maxSeat) {
            this.maxSeat = maxSeat;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getCCDiscountDesc() {
            return cCDiscountDesc;
        }

        public void setCCDiscountDesc(String cCDiscountDesc) {
            this.cCDiscountDesc = cCDiscountDesc;
        }

        public String getCCMaxDisCount() {
            return cCMaxDisCount;
        }

        public void setCCMaxDisCount(String cCMaxDisCount) {
            this.cCMaxDisCount = cCMaxDisCount;
        }

        public String getJMSeatString() {
            return jMSeatString;
        }

        public void setJMSeatString(String jMSeatString) {
            this.jMSeatString = jMSeatString;
        }

    }

}
