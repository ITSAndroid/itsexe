package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StopBooking_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("RSBR_Remark")
    @Expose
    private String rSBRRemark;
    @SerializedName("CUM_UserID")
    @Expose
    private Integer cUMUserID;
    @SerializedName("RSBR_MultipleDate")
    @Expose
    private String rSBRMultipleDate;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getRSBRRemark() {
        return rSBRRemark;
    }

    public void setRSBRRemark(String rSBRRemark) {
        this.rSBRRemark = rSBRRemark;
    }

    public Integer getCUMUserID() {
        return cUMUserID;
    }

    public void setCUMUserID(Integer cUMUserID) {
        this.cUMUserID = cUMUserID;
    }

    public String getRSBRMultipleDate() {
        return rSBRMultipleDate;
    }

    public void setRSBRMultipleDate(String rSBRMultipleDate) {
        this.rSBRMultipleDate = rSBRMultipleDate;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public StopBooking_Request(Integer cMCompanyID, Integer rMRouteID, Integer rTTime, String rSBRRemark, Integer cUMUserID, String rSBRMultipleDate, Integer jMJourneyFrom) {
        this.cMCompanyID = cMCompanyID;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.rSBRRemark = rSBRRemark;
        this.cUMUserID = cUMUserID;
        this.rSBRMultipleDate = rSBRMultipleDate;
        this.jMJourneyFrom = jMJourneyFrom;
    }


}
