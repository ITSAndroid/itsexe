package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimewiseAllBranchMemo_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("Amount")
        @Expose
        private Double amount;
        @SerializedName("JP_ServiceTax")
        @Expose
        private Double jPServiceTax;
        @SerializedName("JP_RoundUP")
        @Expose
        private Double jPRoundUP;
        @SerializedName("Ex_STaxAmount")
        @Expose
        private Double exSTaxAmount;
        @SerializedName("JP_IsIncludeTax")
        @Expose
        private Double jPIsIncludeTax;
        @SerializedName("TotalSeat")
        @Expose
        private Integer totalSeat;
        @SerializedName("TotalIncl_STaxAmount")
        @Expose
        private Double totalInclSTaxAmount;
        @SerializedName("ServiceTax")
        @Expose
        private Double serviceTax;
        @SerializedName("TotalAmount")
        @Expose
        private Double totalAmount;
        @SerializedName("Commission")
        @Expose
        private String commission;
        @SerializedName("BranchCommission")
        @Expose
        private Double branchCommission;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("BusNo")
        @Expose
        private String busNo;
        @SerializedName("JDate")
        @Expose
        private String jDate;
        @SerializedName("JTime")
        @Expose
        private String jTime;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("TotalCollection")
        @Expose
        private Double totalCollection;
        @SerializedName("TotalServiceTax")
        @Expose
        private Double totalServiceTax;
        @SerializedName("NetCollection")
        @Expose
        private Double netCollection;
        @SerializedName("CommissionAmount")
        @Expose
        private Double commissionAmount;
        @SerializedName("NetBalance")
        @Expose
        private Double netBalance;
        @SerializedName("NetSeat")
        @Expose
        private Integer netSeat;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Double getJPServiceTax() {
            return jPServiceTax;
        }

        public void setJPServiceTax(Double jPServiceTax) {
            this.jPServiceTax = jPServiceTax;
        }

        public Double getJPRoundUP() {
            return jPRoundUP;
        }

        public void setJPRoundUP(Double jPRoundUP) {
            this.jPRoundUP = jPRoundUP;
        }

        public Double getExSTaxAmount() {
            return exSTaxAmount;
        }

        public void setExSTaxAmount(Double exSTaxAmount) {
            this.exSTaxAmount = exSTaxAmount;
        }

        public Double getJPIsIncludeTax() {
            return jPIsIncludeTax;
        }

        public void setJPIsIncludeTax(Double jPIsIncludeTax) {
            this.jPIsIncludeTax = jPIsIncludeTax;
        }

        public Integer getTotalSeat() {
            return totalSeat;
        }

        public void setTotalSeat(Integer totalSeat) {
            this.totalSeat = totalSeat;
        }

        public Double getTotalInclSTaxAmount() {
            return totalInclSTaxAmount;
        }

        public void setTotalInclSTaxAmount(Double totalInclSTaxAmount) {
            this.totalInclSTaxAmount = totalInclSTaxAmount;
        }

        public Double getServiceTax() {
            return serviceTax;
        }

        public void setServiceTax(Double serviceTax) {
            this.serviceTax = serviceTax;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public Double getBranchCommission() {
            return branchCommission;
        }

        public void setBranchCommission(Double branchCommission) {
            this.branchCommission = branchCommission;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getBusNo() {
            return busNo;
        }

        public void setBusNo(String busNo) {
            this.busNo = busNo;
        }

        public String getJDate() {
            return jDate;
        }

        public void setJDate(String jDate) {
            this.jDate = jDate;
        }

        public String getJTime() {
            return jTime;
        }

        public void setJTime(String jTime) {
            this.jTime = jTime;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public Double getTotalCollection() {
            return totalCollection;
        }

        public void setTotalCollection(Double totalCollection) {
            this.totalCollection = totalCollection;
        }

        public Double getTotalServiceTax() {
            return totalServiceTax;
        }

        public void setTotalServiceTax(Double totalServiceTax) {
            this.totalServiceTax = totalServiceTax;
        }

        public Double getNetCollection() {
            return netCollection;
        }

        public void setNetCollection(Double netCollection) {
            this.netCollection = netCollection;
        }

        public Double getCommissionAmount() {
            return commissionAmount;
        }

        public void setCommissionAmount(Double commissionAmount) {
            this.commissionAmount = commissionAmount;
        }

        public Double getNetBalance() {
            return netBalance;
        }

        public void setNetBalance(Double netBalance) {
            this.netBalance = netBalance;
        }

        public Integer getNetSeat() {
            return netSeat;
        }

        public void setNetSeat(Integer netSeat) {
            this.netSeat = netSeat;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
