package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_MenuOperation_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Settings")
        @Expose
        private List<Setting> settings = null;
        @SerializedName("info")
        @Expose
        private List<Info> info = null;
        @SerializedName("SMS")
        @Expose
        private List<SM> sMS = null;
        @SerializedName("Waiting")
        @Expose
        private List<Object> waiting = null;

        public List<Setting> getSettings() {
            return settings;
        }

        public void setSettings(List<Setting> settings) {
            this.settings = settings;
        }

        public List<Info> getInfo() {
            return info;
        }

        public void setInfo(List<Info> info) {
            this.info = info;
        }

        public List<SM> getSMS() {
            return sMS;
        }

        public void setSMS(List<SM> sMS) {
            this.sMS = sMS;
        }

        public List<Object> getWaiting() {
            return waiting;
        }

        public void setWaiting(List<Object> waiting) {
            this.waiting = waiting;
        }

    }

    public class Info {

        @SerializedName("CN_CompanyNotes")
        @Expose
        private String cNCompanyNotes;
        @SerializedName("StatusMsg")
        @Expose
        private String statusMsg;

        public String getCNCompanyNotes() {
            return cNCompanyNotes;
        }

        public void setCNCompanyNotes(String cNCompanyNotes) {
            this.cNCompanyNotes = cNCompanyNotes;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

    }

    public class SM {

        @SerializedName("BTM_BusTypeID")
        @Expose
        private Integer bTMBusTypeID;
        @SerializedName("BTM_BusType")
        @Expose
        private String bTMBusType;
        @SerializedName("StatusMsg")
        @Expose
        private String statusMsg;

        public Integer getBTMBusTypeID() {
            return bTMBusTypeID;
        }

        public void setBTMBusTypeID(Integer bTMBusTypeID) {
            this.bTMBusTypeID = bTMBusTypeID;
        }

        public String getBTMBusType() {
            return bTMBusType;
        }

        public void setBTMBusType(String bTMBusType) {
            this.bTMBusType = bTMBusType;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

    }

    public class Setting {

        @SerializedName("Them")
        @Expose
        private String them;
        @SerializedName("DefaultBookingType")
        @Expose
        private String defaultBookingType;
        @SerializedName("BookingDateSetting")
        @Expose
        private String bookingDateSetting;
        @SerializedName("TicketReport")
        @Expose
        private String ticketReport;
        @SerializedName("ES_OpenRoutePopUp")
        @Expose
        private Integer eSOpenRoutePopUp;
        @SerializedName("ES_PhoneValidationNeed")
        @Expose
        private Integer eSPhoneValidationNeed;
        @SerializedName("ES_DefaultToolTip")
        @Expose
        private Integer eSDefaultToolTip;
        @SerializedName("ES_IsShowCalender")
        @Expose
        private Integer eSIsShowCalender;
        @SerializedName("StatusMsg")
        @Expose
        private String statusMsg;

        public String getThem() {
            return them;
        }

        public void setThem(String them) {
            this.them = them;
        }

        public String getDefaultBookingType() {
            return defaultBookingType;
        }

        public void setDefaultBookingType(String defaultBookingType) {
            this.defaultBookingType = defaultBookingType;
        }

        public String getBookingDateSetting() {
            return bookingDateSetting;
        }

        public void setBookingDateSetting(String bookingDateSetting) {
            this.bookingDateSetting = bookingDateSetting;
        }

        public String getTicketReport() {
            return ticketReport;
        }

        public void setTicketReport(String ticketReport) {
            this.ticketReport = ticketReport;
        }

        public Integer getESOpenRoutePopUp() {
            return eSOpenRoutePopUp;
        }

        public void setESOpenRoutePopUp(Integer eSOpenRoutePopUp) {
            this.eSOpenRoutePopUp = eSOpenRoutePopUp;
        }

        public Integer getESPhoneValidationNeed() {
            return eSPhoneValidationNeed;
        }

        public void setESPhoneValidationNeed(Integer eSPhoneValidationNeed) {
            this.eSPhoneValidationNeed = eSPhoneValidationNeed;
        }

        public Integer getESDefaultToolTip() {
            return eSDefaultToolTip;
        }

        public void setESDefaultToolTip(Integer eSDefaultToolTip) {
            this.eSDefaultToolTip = eSDefaultToolTip;
        }

        public Integer getESIsShowCalender() {
            return eSIsShowCalender;
        }

        public void setESIsShowCalender(Integer eSIsShowCalender) {
            this.eSIsShowCalender = eSIsShowCalender;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

    }

}
