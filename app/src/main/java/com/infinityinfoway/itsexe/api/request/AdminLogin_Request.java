package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminLogin_Request {
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("VerifyCall")
    @Expose
    private String verifyCall;
    @SerializedName("OSDeviceID")
    @Expose
    private String oSDeviceID;
    @SerializedName("AppName")
    @Expose
    private String appName;
    @SerializedName("IMEI")
    @Expose
    private String iMEI;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerifyCall() {
        return verifyCall;
    }

    public void setVerifyCall(String verifyCall) {
        this.verifyCall = verifyCall;
    }

    public String getOSDeviceID() {
        return oSDeviceID;
    }

    public void setOSDeviceID(String oSDeviceID) {
        this.oSDeviceID = oSDeviceID;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getIMEI() {
        return iMEI;
    }

    public void setIMEI(String iMEI) {
        this.iMEI = iMEI;
    }

    public AdminLogin_Request(String userName, String password, String verifyCall, String oSDeviceID, String appName, String iMEI) {
        this.userName = userName;
        this.password = password;
        this.verifyCall = verifyCall;
        this.oSDeviceID = oSDeviceID;
        this.appName = appName;
        this.iMEI = iMEI;
    }
}
