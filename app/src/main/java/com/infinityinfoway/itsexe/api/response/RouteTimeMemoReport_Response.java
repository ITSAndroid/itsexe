package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RouteTimeMemoReport_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("FromCity")
        @Expose
        private String fromCity;
        @SerializedName("ToCity")
        @Expose
        private String toCity;
        @SerializedName("JM_SeatList")
        @Expose
        private String jMSeatList;
        @SerializedName("JM_TotalPassengers")
        @Expose
        private Integer jMTotalPassengers;
        @SerializedName("JM_PayableAmount")
        @Expose
        private Double jMPayableAmount;
        @SerializedName("JP_Amount")
        @Expose
        private Double jPAmount;
        @SerializedName("GroupByName")
        @Expose
        private String groupByName;
        @SerializedName("GroupByID")
        @Expose
        private Integer groupByID;
        @SerializedName("TotalSeat_SELF")
        @Expose
        private String totalSeatSELF;
        @SerializedName("TotalAmount_SELF")
        @Expose
        private String totalAmountSELF;
        @SerializedName("Branch_Charges_SELF")
        @Expose
        private String branchChargesSELF;
        @SerializedName("Net_Collection_SELF")
        @Expose
        private String netCollectionSELF;
        @SerializedName("TotalSeat_Agent")
        @Expose
        private String totalSeatAgent;
        @SerializedName("TotalAmount_Agent")
        @Expose
        private String totalAmountAgent;
        @SerializedName("Branch_Charges_Agent")
        @Expose
        private String branchChargesAgent;
        @SerializedName("Net_Collection_Agent")
        @Expose
        private String netCollectionAgent;
        @SerializedName("RT_TimeHed")
        @Expose
        private String rTTimeHed;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("BM_BUsNo")
        @Expose
        private String bMBUsNo;
        @SerializedName("RM_RouteNameDisplay")
        @Expose
        private String rMRouteNameDisplay;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("FromCityID")
        @Expose
        private Integer fromCityID;
        @SerializedName("ToCityID")
        @Expose
        private Integer toCityID;
        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("PrintBy")
        @Expose
        private String printBy;

        public String getFromCity() {
            return fromCity;
        }

        public void setFromCity(String fromCity) {
            this.fromCity = fromCity;
        }

        public String getToCity() {
            return toCity;
        }

        public void setToCity(String toCity) {
            this.toCity = toCity;
        }

        public String getJMSeatList() {
            return jMSeatList;
        }

        public void setJMSeatList(String jMSeatList) {
            this.jMSeatList = jMSeatList;
        }

        public Integer getJMTotalPassengers() {
            return jMTotalPassengers;
        }

        public void setJMTotalPassengers(Integer jMTotalPassengers) {
            this.jMTotalPassengers = jMTotalPassengers;
        }

        public Double getJMPayableAmount() {
            return jMPayableAmount;
        }

        public void setJMPayableAmount(Double jMPayableAmount) {
            this.jMPayableAmount = jMPayableAmount;
        }

        public Double getJPAmount() {
            return jPAmount;
        }

        public void setJPAmount(Double jPAmount) {
            this.jPAmount = jPAmount;
        }

        public String getGroupByName() {
            return groupByName;
        }

        public void setGroupByName(String groupByName) {
            this.groupByName = groupByName;
        }

        public Integer getGroupByID() {
            return groupByID;
        }

        public void setGroupByID(Integer groupByID) {
            this.groupByID = groupByID;
        }

        public String getTotalSeatSELF() {
            return totalSeatSELF;
        }

        public void setTotalSeatSELF(String totalSeatSELF) {
            this.totalSeatSELF = totalSeatSELF;
        }

        public String getTotalAmountSELF() {
            return totalAmountSELF;
        }

        public void setTotalAmountSELF(String totalAmountSELF) {
            this.totalAmountSELF = totalAmountSELF;
        }

        public String getBranchChargesSELF() {
            return branchChargesSELF;
        }

        public void setBranchChargesSELF(String branchChargesSELF) {
            this.branchChargesSELF = branchChargesSELF;
        }

        public String getNetCollectionSELF() {
            return netCollectionSELF;
        }

        public void setNetCollectionSELF(String netCollectionSELF) {
            this.netCollectionSELF = netCollectionSELF;
        }

        public String getTotalSeatAgent() {
            return totalSeatAgent;
        }

        public void setTotalSeatAgent(String totalSeatAgent) {
            this.totalSeatAgent = totalSeatAgent;
        }

        public String getTotalAmountAgent() {
            return totalAmountAgent;
        }

        public void setTotalAmountAgent(String totalAmountAgent) {
            this.totalAmountAgent = totalAmountAgent;
        }

        public String getBranchChargesAgent() {
            return branchChargesAgent;
        }

        public void setBranchChargesAgent(String branchChargesAgent) {
            this.branchChargesAgent = branchChargesAgent;
        }

        public String getNetCollectionAgent() {
            return netCollectionAgent;
        }

        public void setNetCollectionAgent(String netCollectionAgent) {
            this.netCollectionAgent = netCollectionAgent;
        }

        public String getRTTimeHed() {
            return rTTimeHed;
        }

        public void setRTTimeHed(String rTTimeHed) {
            this.rTTimeHed = rTTimeHed;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getBMBUsNo() {
            return bMBUsNo;
        }

        public void setBMBUsNo(String bMBUsNo) {
            this.bMBUsNo = bMBUsNo;
        }

        public String getRMRouteNameDisplay() {
            return rMRouteNameDisplay;
        }

        public void setRMRouteNameDisplay(String rMRouteNameDisplay) {
            this.rMRouteNameDisplay = rMRouteNameDisplay;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public Integer getFromCityID() {
            return fromCityID;
        }

        public void setFromCityID(Integer fromCityID) {
            this.fromCityID = fromCityID;
        }

        public Integer getToCityID() {
            return toCityID;
        }

        public void setToCityID(Integer toCityID) {
            this.toCityID = toCityID;
        }

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getPrintBy() {
            return printBy;
        }

        public void setPrintBy(String printBy) {
            this.printBy = printBy;
        }

    }

}
