package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_SearchingMobileNo_Response {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("SearchingMobileNo")
        @Expose
        private List<SearchingMobileNo> searchingMobileNo = null;

        public List<SearchingMobileNo> getSearchingMobileNo() {
            return searchingMobileNo;
        }

        public void setSearchingMobileNo(List<SearchingMobileNo> searchingMobileNo) {
            this.searchingMobileNo = searchingMobileNo;
        }

    }

    public class SearchingMobileNo {

        @SerializedName("PNRNO")
        @Expose
        private Integer pNRNO;
        @SerializedName("BookingDate")
        @Expose
        private String bookingDate;
        @SerializedName("FromCityName")
        @Expose
        private String fromCityName;
        @SerializedName("ToCityName")
        @Expose
        private String toCityName;
        @SerializedName("CityTime")
        @Expose
        private String cityTime;
        @SerializedName("PassengerName")
        @Expose
        private String passengerName;
        @SerializedName("JourneyDate")
        @Expose
        private String journeyDate;
        @SerializedName("RouteStartTime")
        @Expose
        private String routeStartTime;
        @SerializedName("RouteNameDisplay")
        @Expose
        private String routeNameDisplay;
        @SerializedName("PhoneNo")
        @Expose
        private String phoneNo;
        @SerializedName("SeatNo")
        @Expose
        private String seatNo;
        @SerializedName("BTM_BookingTypeID")
        @Expose
        private Integer bTMBookingTypeID;
        @SerializedName("JM_PhoneOnHold")
        @Expose
        private Integer jMPhoneOnHold;
        @SerializedName("TimeOrder")
        @Expose
        private Integer timeOrder;
        @SerializedName("BrnchPickupOrder")
        @Expose
        private Integer brnchPickupOrder;
        @SerializedName("BrnchCityOrder")
        @Expose
        private Integer brnchCityOrder;
        @SerializedName("JM_JourneyStartTime")
        @Expose
        private String jMJourneyStartTime;
        @SerializedName("STAUTS")
        @Expose
        private Integer sTAUTS;

        public Integer getPNRNO() {
            return pNRNO;
        }

        public void setPNRNO(Integer pNRNO) {
            this.pNRNO = pNRNO;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getFromCityName() {
            return fromCityName;
        }

        public void setFromCityName(String fromCityName) {
            this.fromCityName = fromCityName;
        }

        public String getToCityName() {
            return toCityName;
        }

        public void setToCityName(String toCityName) {
            this.toCityName = toCityName;
        }

        public String getCityTime() {
            return cityTime;
        }

        public void setCityTime(String cityTime) {
            this.cityTime = cityTime;
        }

        public String getPassengerName() {
            return passengerName;
        }

        public void setPassengerName(String passengerName) {
            this.passengerName = passengerName;
        }

        public String getJourneyDate() {
            return journeyDate;
        }

        public void setJourneyDate(String journeyDate) {
            this.journeyDate = journeyDate;
        }

        public String getRouteStartTime() {
            return routeStartTime;
        }

        public void setRouteStartTime(String routeStartTime) {
            this.routeStartTime = routeStartTime;
        }

        public String getRouteNameDisplay() {
            return routeNameDisplay;
        }

        public void setRouteNameDisplay(String routeNameDisplay) {
            this.routeNameDisplay = routeNameDisplay;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getSeatNo() {
            return seatNo;
        }

        public void setSeatNo(String seatNo) {
            this.seatNo = seatNo;
        }

        public Integer getBTMBookingTypeID() {
            return bTMBookingTypeID;
        }

        public void setBTMBookingTypeID(Integer bTMBookingTypeID) {
            this.bTMBookingTypeID = bTMBookingTypeID;
        }

        public Integer getJMPhoneOnHold() {
            return jMPhoneOnHold;
        }

        public void setJMPhoneOnHold(Integer jMPhoneOnHold) {
            this.jMPhoneOnHold = jMPhoneOnHold;
        }

        public Integer getTimeOrder() {
            return timeOrder;
        }

        public void setTimeOrder(Integer timeOrder) {
            this.timeOrder = timeOrder;
        }

        public Integer getBrnchPickupOrder() {
            return brnchPickupOrder;
        }

        public void setBrnchPickupOrder(Integer brnchPickupOrder) {
            this.brnchPickupOrder = brnchPickupOrder;
        }

        public Integer getBrnchCityOrder() {
            return brnchCityOrder;
        }

        public void setBrnchCityOrder(Integer brnchCityOrder) {
            this.brnchCityOrder = brnchCityOrder;
        }

        public String getJMJourneyStartTime() {
            return jMJourneyStartTime;
        }

        public void setJMJourneyStartTime(String jMJourneyStartTime) {
            this.jMJourneyStartTime = jMJourneyStartTime;
        }

        public Integer getSTAUTS() {
            return sTAUTS;
        }

        public void setSTAUTS(Integer sTAUTS) {
            this.sTAUTS = sTAUTS;
        }

    }

}
