package com.infinityinfoway.itsexe.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertAgentQuotaBookingCancle_Request {

    @SerializedName("CM_CompanyID")
    @Expose
    private Integer cMCompanyID;
    @SerializedName("BM_BranchID")
    @Expose
    private Integer bMBranchID;
    @SerializedName("UM_UserID")
    @Expose
    private Integer uMUserID;
    @SerializedName("RM_RouteID")
    @Expose
    private Integer rMRouteID;
    @SerializedName("RT_Time")
    @Expose
    private Integer rTTime;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("ToDate")
    @Expose
    private String toDate;
    @SerializedName("ABI_AgentCityID")
    @Expose
    private Integer aBIAgentCityID;
    @SerializedName("AM_AgentID")
    @Expose
    private Integer aMAgentID;
    @SerializedName("SeatNo")
    @Expose
    private String seatNo;
    @SerializedName("JM_JourneyFrom")
    @Expose
    private Integer jMJourneyFrom;
    @SerializedName("JM_JourneyTo")
    @Expose
    private Integer jMJourneyTo;
    @SerializedName("JM_PassengerName")
    @Expose
    private String jMPassengerName;
    @SerializedName("TransType")
    @Expose
    private Integer transType;
    @SerializedName("IsAutoInsertInAdvance")
    @Expose
    private Integer isAutoInsertInAdvance;
    @SerializedName("PM_PickupID")
    @Expose
    private Integer pMPickupID;
    @SerializedName("PCRM_RegistrationID")
    @Expose
    private String pCRMRegistrationID;

    public Integer getCMCompanyID() {
        return cMCompanyID;
    }

    public void setCMCompanyID(Integer cMCompanyID) {
        this.cMCompanyID = cMCompanyID;
    }

    public Integer getBMBranchID() {
        return bMBranchID;
    }

    public void setBMBranchID(Integer bMBranchID) {
        this.bMBranchID = bMBranchID;
    }

    public Integer getUMUserID() {
        return uMUserID;
    }

    public void setUMUserID(Integer uMUserID) {
        this.uMUserID = uMUserID;
    }

    public Integer getRMRouteID() {
        return rMRouteID;
    }

    public void setRMRouteID(Integer rMRouteID) {
        this.rMRouteID = rMRouteID;
    }

    public Integer getRTTime() {
        return rTTime;
    }

    public void setRTTime(Integer rTTime) {
        this.rTTime = rTTime;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getABIAgentCityID() {
        return aBIAgentCityID;
    }

    public void setABIAgentCityID(Integer aBIAgentCityID) {
        this.aBIAgentCityID = aBIAgentCityID;
    }

    public Integer getAMAgentID() {
        return aMAgentID;
    }

    public void setAMAgentID(Integer aMAgentID) {
        this.aMAgentID = aMAgentID;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public Integer getJMJourneyFrom() {
        return jMJourneyFrom;
    }

    public void setJMJourneyFrom(Integer jMJourneyFrom) {
        this.jMJourneyFrom = jMJourneyFrom;
    }

    public Integer getJMJourneyTo() {
        return jMJourneyTo;
    }

    public void setJMJourneyTo(Integer jMJourneyTo) {
        this.jMJourneyTo = jMJourneyTo;
    }

    public String getJMPassengerName() {
        return jMPassengerName;
    }

    public void setJMPassengerName(String jMPassengerName) {
        this.jMPassengerName = jMPassengerName;
    }

    public Integer getTransType() {
        return transType;
    }

    public void setTransType(Integer transType) {
        this.transType = transType;
    }

    public Integer getIsAutoInsertInAdvance() {
        return isAutoInsertInAdvance;
    }

    public void setIsAutoInsertInAdvance(Integer isAutoInsertInAdvance) {
        this.isAutoInsertInAdvance = isAutoInsertInAdvance;
    }

    public Integer getPMPickupID() {
        return pMPickupID;
    }

    public void setPMPickupID(Integer pMPickupID) {
        this.pMPickupID = pMPickupID;
    }

    public String getPCRMRegistrationID() {
        return pCRMRegistrationID;
    }

    public void setPCRMRegistrationID(String pCRMRegistrationID) {
        this.pCRMRegistrationID = pCRMRegistrationID;
    }

    public InsertAgentQuotaBookingCancle_Request(Integer cMCompanyID, Integer bMBranchID, Integer uMUserID, Integer rMRouteID, Integer rTTime, String fromDate, String toDate, Integer aBIAgentCityID, Integer aMAgentID, String seatNo, Integer jMJourneyFrom, Integer jMJourneyTo, String jMPassengerName, Integer transType, Integer isAutoInsertInAdvance, Integer pMPickupID, String pCRMRegistrationID) {
        this.cMCompanyID = cMCompanyID;
        this.bMBranchID = bMBranchID;
        this.uMUserID = uMUserID;
        this.rMRouteID = rMRouteID;
        this.rTTime = rTTime;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.aBIAgentCityID = aBIAgentCityID;
        this.aMAgentID = aMAgentID;
        this.seatNo = seatNo;
        this.jMJourneyFrom = jMJourneyFrom;
        this.jMJourneyTo = jMJourneyTo;
        this.jMPassengerName = jMPassengerName;
        this.transType = transType;
        this.isAutoInsertInAdvance = isAutoInsertInAdvance;
        this.pMPickupID = pMPickupID;
        this.pCRMRegistrationID = pCRMRegistrationID;
    }
}
