package com.infinityinfoway.itsexe.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_CompanyList_Response {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("CityDetails")
        @Expose
        private List<CityDetail> cityDetails = null;

        public List<CityDetail> getCityDetails() {
            return cityDetails;
        }

        public void setCityDetails(List<CityDetail> cityDetails) {
            this.cityDetails = cityDetails;
        }

    }

    public class CityDetail {

        @SerializedName("CM_CompanyID")
        @Expose
        private Integer cMCompanyID;
        @SerializedName("CM_CompanyName")
        @Expose
        private String cMCompanyName;
        @SerializedName("CM_CompanyAPIName")
        @Expose
        private String cMCompanyAPIName;
        @SerializedName("CM_CompanyCode")
        @Expose
        private String cMCompanyCode;
        @SerializedName("CM_IsActive")
        @Expose
        private Integer cMIsActive;
        @SerializedName("CM_CityID")
        @Expose
        private Integer cMCityID;
        @SerializedName("SM_StateID")
        @Expose
        private Integer sMStateID;
        @SerializedName("CM_IsActive1")
        @Expose
        private Integer cMIsActive1;

        public Integer getCMCompanyID() {
            return cMCompanyID;
        }

        public void setCMCompanyID(Integer cMCompanyID) {
            this.cMCompanyID = cMCompanyID;
        }

        public String getCMCompanyName() {
            return cMCompanyName;
        }

        public void setCMCompanyName(String cMCompanyName) {
            this.cMCompanyName = cMCompanyName;
        }

        public String getCMCompanyAPIName() {
            return cMCompanyAPIName;
        }

        public void setCMCompanyAPIName(String cMCompanyAPIName) {
            this.cMCompanyAPIName = cMCompanyAPIName;
        }

        public String getCMCompanyCode() {
            return cMCompanyCode;
        }

        public void setCMCompanyCode(String cMCompanyCode) {
            this.cMCompanyCode = cMCompanyCode;
        }

        public Integer getCMIsActive() {
            return cMIsActive;
        }

        public void setCMIsActive(Integer cMIsActive) {
            this.cMIsActive = cMIsActive;
        }

        public Integer getCMCityID() {
            return cMCityID;
        }

        public void setCMCityID(Integer cMCityID) {
            this.cMCityID = cMCityID;
        }

        public Integer getSMStateID() {
            return sMStateID;
        }

        public void setSMStateID(Integer sMStateID) {
            this.sMStateID = sMStateID;
        }

        public Integer getCMIsActive1() {
            return cMIsActive1;
        }

        public void setCMIsActive1(Integer cMIsActive1) {
            this.cMIsActive1 = cMIsActive1;
        }

    }

}
