package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.RoutePickupDropChart_Request;
import com.infinityinfoway.itsexe.api.response.RoutePickupDropChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.exe_adapter.RoutePickUpDropChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutePickUpDropChartActivity extends AppCompatActivity {


    private RecyclerView recycler_pick_drop_chart;
    private RoutePickUpDropChartAdapter routePickUpDropChartAdapter;
    private String method;
    private ProgressDialog progDialog;
    private List<RoutePickupDropChart_Response.Datum> list_RoutePickUpData;
    private TextView txt_company_name, txt_company_address, txt_journey_date, txt_bus_no, txt_print_by, txt_print_date, txt_route_name;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_RoutePickUpDropChart";
    private String resReportTitle,journeyStartDate, journeyStartTime, journeyCityTime,isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId;
    private Bundle getRouteData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_pick_up_drop_chart);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        cd = new ConnectionDetector(RoutePickUpDropChartActivity.this);
        getPref = new ITSExeSharedPref(RoutePickUpDropChartActivity.this);


        recycler_pick_drop_chart = findViewById(R.id.recycler_pick_drop_chart);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_pick_drop_chart.setLayoutManager(linearLayoutManager);


        txt_company_name = findViewById(R.id.txt_company_name);
        txt_company_address = findViewById(R.id.txt_company_address);
        txt_journey_date = findViewById(R.id.txt_journey_date);
        txt_bus_no = findViewById(R.id.txt_bus_no);
        txt_print_by = findViewById(R.id.txt_print_by);
        txt_print_date = findViewById(R.id.txt_print_date);
        txt_route_name = findViewById(R.id.txt_route_name);

        Get_Report_RoutePickUpDropChartApiCall();

    }

    private void Get_Report_RoutePickUpDropChartApiCall() {
        showProgressDialog("Loading....");

        RoutePickupDropChart_Request request = new RoutePickupDropChart_Request(
                cm_CompanyId,
                routeId,
                routeTimeId,
                journeyStartDate,
                fromCity,
                toCity,
                getPref.getBUM_BranchUserID(),
                getPref.getBM_BranchID(),
                isSameDay
        );


        Call<RoutePickupDropChart_Response> call = apiService.RoutePickupDropChart(request);
        call.enqueue(new Callback<RoutePickupDropChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<RoutePickupDropChart_Response> call, @NotNull Response<RoutePickupDropChart_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_RoutePickUpData = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData()!= null && response.body().getData().size() > 0) {
                            list_RoutePickUpData = response.body().getData();
                            txt_company_name.setText(list_RoutePickUpData.get(0).getCompanyName());
                            txt_company_address.setText(list_RoutePickUpData.get(0).getCompanyAdd());


                            txt_journey_date.setText("Journey Date :" + list_RoutePickUpData.get(0).getJourneyDate());
                            txt_bus_no.setText(list_RoutePickUpData.get(0).getBusNo());
                            txt_print_by.setText("Print By :" + list_RoutePickUpData.get(0).getPrintBy());
                            txt_print_date.setText("Print Date: " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            txt_route_name.setText(list_RoutePickUpData.get(0).getRouteName());

                            routePickUpDropChartAdapter = new RoutePickUpDropChartAdapter(RoutePickUpDropChartActivity.this, list_RoutePickUpData);
                            recycler_pick_drop_chart.setAdapter(routePickUpDropChartAdapter);
                        }
                    } else {
                        Toast.makeText(RoutePickUpDropChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<RoutePickupDropChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });


    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(RoutePickUpDropChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_RoutePickUpDropChart")) {
                                        Get_Report_RoutePickUpDropChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(RoutePickUpDropChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }




}
