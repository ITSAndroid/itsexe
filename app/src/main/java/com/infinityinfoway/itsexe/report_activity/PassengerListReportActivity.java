package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.PassengerList_Request;
import com.infinityinfoway.itsexe.api.response.PassengerList_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityPassengerListReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.PassengerListReportAdapter;
import com.infinityinfoway.itsexe.exe_adapter.PassengerListReportAdapter_SubRoute;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassengerListReportActivity extends AppCompatActivity {

    private List<PassengerList_Response.Datum> listPassneger;
    private LinkedHashMap<String, List<PassengerList_Response.Datum>> linkedHashMap;
    private LinkedHashMap<String, List<PassengerList_Response.Datum>> linkedHashMapSubRoute;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "PassengerList";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private Bundle getRouteData;
    private String resReportTitle = "";

    private ActivityPassengerListReportBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPassengerListReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(PassengerListReportActivity.this);
        getPref = new ITSExeSharedPref(PassengerListReportActivity.this);

        PassengerListApiCall();

    }

    private void PassengerListApiCall() {

        PassengerList_Request request = new PassengerList_Request(
                journeyStartDate,
                routeId,
                routeTimeId,
                cm_CompanyId,
                getPref.getBUM_BranchUserID(),
                fromCity,
                toCity,
                bookedByCompanyId,
                isSameDay
        );


        Call<PassengerList_Response> call = apiService.PassengerList(request);
        call.enqueue(new Callback<PassengerList_Response>() {
            @Override
            public void onResponse(@NotNull Call<PassengerList_Response> call, @NotNull Response<PassengerList_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listPassneger = response.body().getData();
                            linkedHashMap = new LinkedHashMap<>();
                            linkedHashMapSubRoute=new LinkedHashMap<>();

                            PassengerList_Response.Datum dataInitial = listPassneger.get(0);

                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");
                            binding.txtRoute.setText(dataInitial.getJourneyStartDate());
                            binding.txtCompanyAddress.setText(dataInitial.getCMCompanyAddress());
                            binding.txtCompanyName.setText(dataInitial.getCompanyName());

                            binding.txtTotalSeat.setText("Total Seats : " + dataInitial.getTotalBookedSeat());
                            binding.txtEmptySeat.setText("Empty Seats : " + dataInitial.getTotalEmptySeat() + " : " + dataInitial.getEmptySeat());
                            binding.txtEmptySleeper.setText("Empty Sleepers : " + dataInitial.getTotalEmptySleeper() + " : " + dataInitial.getEmptySleeper());

                            binding.txtCoachNo.setText("Coach No : "+dataInitial.getBusNo());
                            binding.txtPickupMan.setText("Pickup Man : "+dataInitial.getDMDriverName());
                            binding.txtPhone.setText("Phone : "+dataInitial.getDMPhoneNo());

                            binding.txtPikupWiseList.setText(Config.fromHtml(dataInitial.getPickUpNameList()));
                            binding.txtDropWiseList.setText(Config.fromHtml(dataInitial.getDropNameList()));

                            for (PassengerList_Response.Datum data : listPassneger) {
                                if (!linkedHashMap.containsKey(data.getPickUpName())) {
                                    List<PassengerList_Response.Datum> list = new ArrayList<>();
                                    list.add(data);
                                    linkedHashMap.put(data.getPickUpName(), list);
                                } else {
                                    List<PassengerList_Response.Datum> list = linkedHashMap.get(data.getPickUpName());
                                    list.add(data);
                                    linkedHashMap.put(data.getPickUpName(), list);
                                }

                                if(!linkedHashMapSubRoute.containsKey(data.getFromCity()+ " - "+data.getFromToCity())){
                                    List<PassengerList_Response.Datum> list = new ArrayList<>();
                                    list.add(data);
                                    linkedHashMapSubRoute.put(data.getFromCity()+ " - "+data.getFromToCity(), list);
                                }else{
                                    List<PassengerList_Response.Datum> list = linkedHashMapSubRoute.get(data.getFromCity()+ " - "+data.getFromToCity());
                                    list.add(data);
                                    linkedHashMapSubRoute.put(data.getFromCity()+ " - "+data.getFromToCity(), list);
                                }
                            }

                            binding.rvPassengerBooking.setAdapter(new PassengerListReportAdapter(PassengerListReportActivity.this, linkedHashMap));

                            binding.rvSubData.setAdapter(new PassengerListReportAdapter_SubRoute(PassengerListReportActivity.this,linkedHashMapSubRoute));
                        } else {
                            Toast.makeText(PassengerListReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(PassengerListReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PassengerList_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(PassengerListReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("PassengerList")) {
                                        PassengerListApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(PassengerListReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}
