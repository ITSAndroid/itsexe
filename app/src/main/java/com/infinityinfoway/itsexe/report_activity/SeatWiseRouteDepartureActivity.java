package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatWiseRouteDepartureReport_Request;
import com.infinityinfoway.itsexe.api.response.SeatWiseRouteDepartureReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySeatwiseRouteDepartureBinding;
import com.infinityinfoway.itsexe.exe_adapter.SeatWiseRouteDepartureReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatWiseRouteDepartureActivity extends AppCompatActivity {

    SeatWiseRouteDepartureReportAdapter routeDepartureReportAdapter;
    String method;
    ProgressDialog progDialog;
    List<SeatWiseRouteDepartureReport_Response.Datum> list_route_departure;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "SeatWiseRouteDepartureReport";
    private String resReportTitle, journeyStartDate, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity;
    private Bundle getRouteData;
    private ActivitySeatwiseRouteDepartureBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySeatwiseRouteDepartureBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        cd = new ConnectionDetector(SeatWiseRouteDepartureActivity.this);
        getPref = new ITSExeSharedPref(SeatWiseRouteDepartureActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_RouteDepartureReportApiCall();

    }

    private void Get_Report_RouteDepartureReportApiCall() {

        showProgressDialog("Loading....");

        SeatWiseRouteDepartureReport_Request request = new SeatWiseRouteDepartureReport_Request(
                journeyStartDate,
                cm_CompanyId,
                routeId,
                routeTimeId,
                fromCity,
                toCity,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );


        Call<SeatWiseRouteDepartureReport_Response> call = apiService.SeatWiseRouteDepartureReport(request);
        call.enqueue(new Callback<SeatWiseRouteDepartureReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatWiseRouteDepartureReport_Response> call, @NotNull Response<SeatWiseRouteDepartureReport_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_route_departure = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_route_departure = response.body().getData();
                            binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                            binding.txtCompanyName.setText(list_route_departure.get(0).getCMCompanyName());
                            binding.txtRemarks.setText(list_route_departure.get(0).getJMRemarks());
                            if(!TextUtils.isEmpty(list_route_departure.get(0).getBoardingAt())){
                                binding.txtBoardingAt.setText(list_route_departure.get(0).getBoardingAt().replaceAll("\r", "\n"));
                            }
                            binding.txtEmptySeat.setText(list_route_departure.get(0).getEmptySeat());
                            binding.txtBooked.setText(list_route_departure.get(0).getTotalBookedSeat());
                            binding.txtTotalSeat.setText(list_route_departure.get(0).getTotalSeat());
                            binding.txtDepartTime.setText(list_route_departure.get(0).getRouteTime());
                            binding.txtJourneyDate.setText(list_route_departure.get(0).getJourneyStartDate());
                            binding.txtBusType.setText(list_route_departure.get(0).getBusType());
                            binding.txtService.setText(list_route_departure.get(0).getRouteName());
                            binding.txtBusNumber.setText(list_route_departure.get(0).getBMBusNo());

                            binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                            routeDepartureReportAdapter = new SeatWiseRouteDepartureReportAdapter(SeatWiseRouteDepartureActivity.this, list_route_departure);
                            binding.rvRouteDeparture.setAdapter(routeDepartureReportAdapter);
                        }
                    } else {
                        Toast.makeText(SeatWiseRouteDepartureActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatWiseRouteDepartureReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }


    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SeatWiseRouteDepartureActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("RouteDepartureReport")) {
                                        Get_Report_RouteDepartureReportApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SeatWiseRouteDepartureActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}
