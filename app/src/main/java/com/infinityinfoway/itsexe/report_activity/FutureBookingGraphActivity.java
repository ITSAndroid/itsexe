package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.FutureBookingGraph_Request;
import com.infinityinfoway.itsexe.api.response.FutureBookingGraph_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityFutureBookingGraphBinding;
import com.infinityinfoway.itsexe.utils.TotalTicketAxisValueFormatter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FutureBookingGraphActivity extends AppCompatActivity implements OnChartValueSelectedListener {
    private ActivityFutureBookingGraphBinding binding;
    private List<FutureBookingGraph_Response.Datum> listFutureBookingGraph = new ArrayList<>();
    private int[] MATERIAL_COLORS;
    private String[] RangeName;

    private ArrayList<BarEntry>[] yValues;
    private int routeTimeCounter = 0;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_Booking_Cancel_ReportByRouteTime";
    private String journeyStartDate ;
    private int cm_CompanyId,fromCity, toCity;
    private Bundle getRouteData;
    private String resReportTitle = "";
    private LinkedHashMap<String, List<FutureBookingGraph_Response.Datum>> linkedHashMap;
    private LinkedHashMap<String, List<FutureBookingGraph_Response.Datum>> linkedHashMapRouteTime;
    private List<String> listJDateKey = new ArrayList<>();
    private List<String> listRouteTimeKey = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFutureBookingGraphBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(FutureBookingGraphActivity.this);
        getPref = new ITSExeSharedPref(FutureBookingGraphActivity.this);


//        MATERIAL_COLORS = new int[]{
//                rgb(listDashBoardData.get(0).getColor()), rgb(listDashBoardData.get(1).getColor()), rgb(listDashBoardData.get(2).getColor()), rgb("#3498db"), rgb("#613A32"), rgb("#04F5FC")
//        };

        binding.chart1.setOnChartValueSelectedListener(this);
        futureBookingGraphApiCall();

    }

    private void futureBookingGraphApiCall() {
        showProgressDialog("Loading....");


        FutureBookingGraph_Request request = new FutureBookingGraph_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                0
        );


      /*  FutureBookingGraph_Request request = new FutureBookingGraph_Request(
                890,
                "24-02-2021",
                654,
                703,
                0
        );*/

        Call<FutureBookingGraph_Response> call = apiService.FutureBookingGraph(request);
        call.enqueue(new Callback<FutureBookingGraph_Response>() {
            @Override
            public void onResponse(@NotNull Call<FutureBookingGraph_Response> call, @NotNull Response<FutureBookingGraph_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1 && response.body().getData() != null && response.body().getData().size() > 0) {
                        listFutureBookingGraph = response.body().getData();
                        linkedHashMap = new LinkedHashMap<>();
                        linkedHashMapRouteTime = new LinkedHashMap<>();


                        for (FutureBookingGraph_Response.Datum data : listFutureBookingGraph) {
                            if (!linkedHashMap.containsKey(data.getJourneyDate())) {
                                List<FutureBookingGraph_Response.Datum> list = new ArrayList<>();
                                list.add(data);
                                linkedHashMap.put(data.getJourneyDate(), list);
                                listJDateKey.add(data.getJourneyDate());

                            } else {
                                List<FutureBookingGraph_Response.Datum> list = linkedHashMap.get(data.getJourneyDate());
                                list.add(data);
                                linkedHashMap.put(data.getJourneyDate(), list);
                            }

                            if (!linkedHashMapRouteTime.containsKey(data.getRTTime())) {
                                List<FutureBookingGraph_Response.Datum> list = new ArrayList<>();
                                list.add(data);
                                linkedHashMapRouteTime.put(data.getRTTime(), list);
                                listRouteTimeKey.add(data.getRTTime());
                            } else {
                                List<FutureBookingGraph_Response.Datum> list = linkedHashMapRouteTime.get(data.getRTTime());
                                list.add(data);
                                linkedHashMapRouteTime.put(data.getRTTime(), list);
                            }


                        }
                        drawChart();
                    } else {
                        Toast.makeText(FutureBookingGraphActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<FutureBookingGraph_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void drawChart() {


        binding.chart1.setPinchZoom(false);
        binding.chart1.setDrawValueAboveBar(false);
        binding.chart1.setDrawBarShadow(false);

        binding.chart1.setDrawGridBackground(false);


        IAxisValueFormatter xAxisFormatter = new TotalTicketAxisValueFormatter(binding.chart1, listJDateKey.toArray(new String[listJDateKey.size()]));
        XAxis xAxis = binding.chart1.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1); // only intervals of 1 day
        //xAxis.setLabelCount(5);
        //xAxis.setCenterAxisLabels(true);
        xAxis.setLabelRotationAngle(340);
        xAxis.setValueFormatter(xAxisFormatter);

        //  binding.chart1.setXAxisRenderer(new CustomXAxisRenderer(binding.chart1.getViewPortHandler(), binding.chart1.getXAxis(), binding.chart1.getTransformer(YAxis.AxisDependency.LEFT)));


        YAxis leftAxis = binding.chart1.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        //leftAxis.setValueFormatter(new PercentFormatter());
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15);
        leftAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)


        YAxis rightAxis = binding.chart1.getAxisRight();
        // rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(4, false);
        rightAxis.setValueFormatter(new LargeValueFormatter());
        rightAxis.setSpaceTop(15);
        rightAxis.setAxisMinimum(0);
        rightAxis.setEnabled(false);// this replaces setStartAtZero(true)

        Legend l = binding.chart1.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9);
        l.setTextSize(11);
        l.setXEntrySpace(4);

        setData();


    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;

//        binding.chart1.getBarBounds((BarEntry) e, mOnValueSelectedRectF);
//        MPPointF position = binding.chart1.getPosition(e, YAxis.AxisDependency.LEFT);
//        MPPointF.recycleInstance(position);

    }

    @Override
    public void onNothingSelected() {
    }


    private void setData() {
        routeTimeCounter = listRouteTimeKey.size();

        float groupSpace = 0.02f;
        float barSpace = 0.02f;
        float barWidth = 0.1f;

        float totalInterval=(barSpace+barWidth)*routeTimeCounter+groupSpace;
        while (totalInterval!=1){
            totalInterval=(barSpace+barWidth)*routeTimeCounter+groupSpace;

            if(totalInterval==1){
                break;
            }else if(totalInterval>1){
                barSpace=0.01f;
                groupSpace=0.01f;
                totalInterval=(barSpace+barWidth)*routeTimeCounter+groupSpace;
                if(totalInterval==1){
                    break;
                }else if(totalInterval>1){
                    if((totalInterval-1)<=0.04){
                        groupSpace+=1-totalInterval;
                        totalInterval=(barSpace+barWidth)*routeTimeCounter+groupSpace;
                    }else{
                        barWidth=barWidth-0.01f;
                    }
                }else if(totalInterval<1){
                    if((1-totalInterval)<=0.04){
                        groupSpace+=1-totalInterval;
                        totalInterval=(barSpace+barWidth)*routeTimeCounter+groupSpace;
                    }else{
                        barWidth=barWidth+0.01f;
                    }
                }
            }else if((1-totalInterval)<=0.04){
                groupSpace+=1-totalInterval;
                totalInterval=(barSpace+barWidth)*routeTimeCounter+groupSpace;
            }else{
                barWidth+=0.01;
            }
        }

        yValues = new ArrayList[routeTimeCounter];

        for (int i = 0; i < routeTimeCounter; i++) {
            yValues[i] = new ArrayList<>();
        }

        for (int i = 0; i < listJDateKey.size(); i++) {
            List<FutureBookingGraph_Response.Datum> data = linkedHashMap.get(listJDateKey.get(i));
            List<String> arrayList=new ArrayList<>();
            for (int j = 0; j < routeTimeCounter; j++) {
                String routeTime="";
                if(j<data.size()){
                    routeTime=data.get(j).getRTTime();
                }

                if(!TextUtils.isEmpty(routeTime)){
                    for(int k=0;k<routeTimeCounter;k++){
                        if(listRouteTimeKey.get(k).equals(routeTime)){
                            yValues[k].add(new BarEntry(i, data.get(j).getJMTotalPassengers()));
                            arrayList.add(String.valueOf(k));
                            break;
                        }
                    }

                }else{
                    for(int l=0;l<7;l++){
                        if(!arrayList.contains(String.valueOf(l))){
                            yValues[l].add(new BarEntry(i, 0));
                            arrayList.add(String.valueOf(l));
                        }
                    }

                }
            }
        }


        if (binding.chart1.getData() != null && binding.chart1.getData().getDataSetCount() > 0) {
            binding.chart1.getData().notifyDataChanged();
            binding.chart1.notifyDataSetChanged();

        } else {
            BarDataSet[] set1 = new BarDataSet[listRouteTimeKey.size()];
            BarData data;
            for (int j = 0; j < listRouteTimeKey.size(); j++) {
                set1[j] = new BarDataSet(yValues[j], listRouteTimeKey.get(j));
                //set1[j].setColor(Color.rgb(104 * (j + 1), 241 * (j + 1), 175 * (j + 1)));
                set1[j].setColor(Color.rgb(50 * (j + 1), 100 * (j + 1), 175 * (j + 1)));
            }
            data = new BarData(set1);
            binding.chart1.setData(data);
        }

        // specify the width each bar should have
        binding.chart1.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        binding.chart1.getXAxis().setAxisMinimum(0);

        // binding.chart1.getXAxis().setAxisMaximum(binding.chart1.getBarData().getGroupWidth(groupSpace, barSpace) * listJDateKey.size()+1);
        binding.chart1.groupBars(0, groupSpace, barSpace);

        binding.chart1.getXAxis().setLabelCount(listJDateKey.size(), false);
      //  binding.chart1.getXAxis().setAvoidFirstLastClipping(true);

        binding.chart1.getDescription().setEnabled(false);

        binding.chart1.setTouchEnabled(false);
        binding.chart1.setScaleEnabled(false);

//        binding.chart1.setFitBars(true);
        binding.chart1.invalidate();
        binding.chart1.setVisibleXRange(0, (listJDateKey.size()));
    }



    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(FutureBookingGraphActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("FutureBookingGraph")) {
                                        futureBookingGraphApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(FutureBookingGraphActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}
