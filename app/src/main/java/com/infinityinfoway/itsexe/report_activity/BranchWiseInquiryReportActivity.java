package com.infinityinfoway.itsexe.report_activity;

import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.BranchWiseInquiryReport_Request;
import com.infinityinfoway.itsexe.api.response.BranchWiseInquiryReport_Response;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityBranchwiseInquiryReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.BranchWiseInquiryReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchWiseInquiryReportActivity extends AppCompatActivity {

    private List<BranchWiseInquiryReport_Response.Datum> listBranchInquiry;
    private LinkedHashMap<String, List<BranchWiseInquiryReport_Response.Datum>> linkedHashMap;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private String journeyStartDate, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private String resReportTitle = "";

    private ActivityBranchwiseInquiryReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityBranchwiseInquiryReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            Bundle getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(v -> finish());

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        getPref = new ITSExeSharedPref(BranchWiseInquiryReportActivity.this);

        BranchWiseInquiryReportApiCall();

    }

    private void BranchWiseInquiryReportApiCall() {

        BranchWiseInquiryReport_Request request = new BranchWiseInquiryReport_Request(
                journeyStartDate,
                cm_CompanyId,
                routeId,
                routeTimeId,
                fromCity,
                toCity,
                arrangementId,
                cityId,
                bookedByCompanyId,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                isSameDay
        );

        Call<BranchWiseInquiryReport_Response> call = apiService.BranchWiseInquiryReport(request);
        call.enqueue(new Callback<BranchWiseInquiryReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<BranchWiseInquiryReport_Response> call, @NotNull Response<BranchWiseInquiryReport_Response> response) {
                if (response.isSuccessful() && response.body() != null) {
                    listBranchInquiry = new ArrayList<>();


                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            listBranchInquiry = response.body().getData();

                            linkedHashMap = new LinkedHashMap<>();

                            for (BranchWiseInquiryReport_Response.Datum data : listBranchInquiry) {


                                if (!linkedHashMap.containsKey(data.getGroupBy())) {
                                    List<BranchWiseInquiryReport_Response.Datum> list = new ArrayList<>();
                                    list.add(data);
                                    linkedHashMap.put(data.getGroupBy(), list);
                                } else {
                                    List<BranchWiseInquiryReport_Response.Datum> list = linkedHashMap.get(data.getGroupBy());
                                    list.add(data);
                                    linkedHashMap.put(data.getGroupBy(), list);
                                }
                            }

                            binding.rvData.setAdapter(new BranchWiseInquiryReportAdapter(BranchWiseInquiryReportActivity.this, linkedHashMap));

                            BranchWiseInquiryReport_Response.Datum data = listBranchInquiry.get(0);

                            binding.txtCompanyName.setText(data.getCMCompanyName());
                            binding.txtCompanyAddress.setText(data.getCMCompanyAddress());
                            binding.txtUser.setText(data.getReportUserName());
                            binding.txtPrintDate.setText("Print Date : " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtCoachNo.setText("Coach No: " + data.getBusNo());
                            binding.txtPickupMan.setText("PickUp Man : " + data.getPickupManName());
                            binding.txtPhone.setText("Phone : " + data.getPMPhoneNo());
                            binding.txtDriver.setText("Driver Name : " + data.getDMDriverName1());
                            binding.txtConductor.setText("Conductor Name : " + data.getDMConductor1());


                            binding.txtAgentQuota.setText(data.getAgentQuota());
                            binding.txtRemarks.setText("Remarks : " + data.getRemarks());

                            List<String> arrayList = calSummary(listBranchInquiry);

                            binding.txtGrandSeats.setText(arrayList.get(0));
                            binding.txtGrandFare.setText(arrayList.get(1));
                            binding.txtGrandStax.setText(arrayList.get(2));
                            binding.txtGrandBkgFare.setText(arrayList.get(3));


                        }
                    } else {
                        Toast.makeText(BranchWiseInquiryReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog( getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<BranchWiseInquiryReport_Response> call, @NotNull Throwable t) {

            }
        });

    }

    private void showInternetConnectionDialog(String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(BranchWiseInquiryReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            (dialog, id) -> {

                                try {
                                    if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                        return;
                                    }
                                    lastClickTime = SystemClock.elapsedRealtime();
                                } catch (Exception ignored) {
                                }

                                BranchWiseInquiryReportApiCall();
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), (dialog, id) -> dialog.cancel());

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }




    private List<String> calSummary(List<BranchWiseInquiryReport_Response.Datum> list) {
        int totalSeat = 0;
        double totalFare = 0, totalStax = 0, totalBKGFare = 0;
        for (BranchWiseInquiryReport_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getJMTotalPassengers())) {
                totalSeat += Integer.parseInt(data.getJMTotalPassengers());
            }
            totalFare += data.getFare();
            totalStax += data.getSTAX();
            totalBKGFare += data.getBKGFARE();
        }

        List<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.format(Locale.getDefault(), "%.2f", totalFare));
        arrayList.add(String.format(Locale.getDefault(), "%.2f", totalStax));
        arrayList.add(String.format(Locale.getDefault(), "%.2f", totalBKGFare));
        return arrayList;
    }

}
