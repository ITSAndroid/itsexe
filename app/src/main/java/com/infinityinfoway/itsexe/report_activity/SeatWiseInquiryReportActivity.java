package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatWiseInquiryReport_Request;
import com.infinityinfoway.itsexe.api.response.SeatWiseInquiryReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySeatwiseInquiryReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.SeatWiseInquiryReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatWiseInquiryReportActivity extends AppCompatActivity {
    private List<SeatWiseInquiryReport_Response.Datum> listSeatWiseInquiery;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_Booking_Cancel_ReportByRouteTime";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private Bundle getRouteData;
    private String resReportTitle = "";

    private ActivitySeatwiseInquiryReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySeatwiseInquiryReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(SeatWiseInquiryReportActivity.this);
        getPref = new ITSExeSharedPref(SeatWiseInquiryReportActivity.this);

        SeatWiseInquiryReportApiCall();

    }

    private void SeatWiseInquiryReportApiCall() {

        SeatWiseInquiryReport_Request request = new SeatWiseInquiryReport_Request(
                journeyStartDate,
                cm_CompanyId,
                routeId,
                routeTimeId,
                fromCity,
                toCity,
                arrangementId,
                cityId,
                bookedByCompanyId,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                isSameDay
        );


        Call<SeatWiseInquiryReport_Response> call = apiService.SeatWiseInquiryReport(request);
        call.enqueue(new Callback<SeatWiseInquiryReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatWiseInquiryReport_Response> call, @NotNull Response<SeatWiseInquiryReport_Response> response) {
                disMissDialog();
                listSeatWiseInquiery = new ArrayList<>();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listSeatWiseInquiery = response.body().getData();
                            SeatWiseInquiryReport_Response.Datum data = listSeatWiseInquiery.get(0);

                            binding.txtCompanyName.setText(data.getCMCompanyName());
                            binding.txtCompanyAddress.setText(data.getCMCompanyAddress());
                            binding.txtUser.setText(data.getReportUserName());
                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtCoachNo.setText("Coach No : " + data.getBusNo());
                            binding.txtPickupMan.setText("PickUp Man: " + data.getPickupManName());
                            binding.txtPickupMan.setText("Phone: " + data.getPMPhoneNo());
                            binding.txtDriver.setText("Driver Name : " + data.getDMDriverName1());
                            binding.txtConductor.setText("Conductor Name : " + data.getDMConductor1());


                            binding.txtEmptySeat.setText(data.getEmptySeat());
                            binding.txtEmptySleepers.setText(data.getEmptySleeper());
                            binding.txtRemarks.setText(data.getRemarks());

                            binding.rvData.setAdapter(new SeatWiseInquiryReportAdapter(SeatWiseInquiryReportActivity.this,listSeatWiseInquiery));

                        } else {
                            Toast.makeText(SeatWiseInquiryReportActivity.this, "No data avaialble", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(SeatWiseInquiryReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatWiseInquiryReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SeatWiseInquiryReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("SeatWiseInquiryReport")) {
                                        SeatWiseInquiryReportApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SeatWiseInquiryReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


}
