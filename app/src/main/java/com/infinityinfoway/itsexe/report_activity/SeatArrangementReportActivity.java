package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatArrangement_Request;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySeatArrangementReportBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatArrangementReportActivity extends AppCompatActivity {

    private List<SeatArrangement_Response.Datum> listSeatDetails = new ArrayList<>();
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_Booking_Cancel_ReportByRouteTime";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private Bundle getRouteData;
    private String resReportTitle = "",routeName;
    private ActivitySeatArrangementReportBinding binding;
    private int max_column_length, max_row_length;
    private int upperSeatCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySeatArrangementReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            routeName=getRouteData.getString("RouteName");
        }



        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(SeatArrangementReportActivity.this);
        getPref = new ITSExeSharedPref(SeatArrangementReportActivity.this);

        binding.txtCompanyName.setText(getPref.getLoginCompanyName() + " [ " + getPref.getBM_BranchCode() + " ] ");
        binding.txtBusNo.setText("Coach No");
        binding.txtRoute.setText(routeName+ ", J Date: " + journeyStartDate);

        SeatArrangementApiCall();

    }

    private void SeatArrangementApiCall() {

        showProgressDialog("Loading....");

        SeatArrangement_Request request = new SeatArrangement_Request(
                cm_CompanyId,
                journeyStartDate,
                routeId,
                routeTimeId,
                fromCity,
                toCity,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                Config.VERIFY_CALL,
                isSameDay
        );

        Call<SeatArrangement_Response> call = apiService.SeatArrangement(request);
        call.enqueue(new Callback<SeatArrangement_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatArrangement_Response> call, @NotNull Response<SeatArrangement_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1 && response.body().getData().getData() != null && response.body().getData().getData().size() > 0) {
                        listSeatDetails = response.body().getData().getData();

                      /*  Collections.sort(listSeatDetails, new Comparator<SeatArrangement_Response.Datum>() {
                            @Override
                            public int compare(SeatArrangement_Response.Datum u1, SeatArrangement_Response.Datum u2) {
                                return u1.getCADRow().compareTo(u2.getCADRow());
                            }
                        });


                        for (SeatArrangement_Response.Datum dataSeat : listSeatDetails) {
                            Log.e("SeatNo", dataSeat.getCADRow() + "=>" + dataSeat.getCADCell() + "=>" + dataSeat.getBADSeatNo());
                            if (max_column_length < dataSeat.getCADCell()) {
                                max_column_length = dataSeat.getCADCell();
                            }

                            if (max_row_length < dataSeat.getCADRow()) {
                                max_row_length = dataSeat.getCADRow();
                            }
                        }

                        List<SeatArrangement_Response.Datum> listUpdated = new ArrayList<>();
                        List<SeatArrangement_Response.Datum> listTemp = new ArrayList<>();

                        int firstRow = 0;
                        for (int i = 0; i < listSeatDetails.size(); i++) {

                            if (i < (listSeatDetails.size()-1) && (listSeatDetails.get(i).getCADRow().equals(listSeatDetails.get(i + 1).getCADRow()))) {
                                listTemp.add(listSeatDetails.get(i));
                            }else if(((i+1)%max_column_length==0)){
                                listTemp.add(listSeatDetails.get(i));
                            }else{
                                listUpdated.addAll(listTemp);
                                //listTemp = new ArrayList<>();
                            }
                            Collections.sort(listTemp, new Comparator<SeatArrangement_Response.Datum>() {
                                @Override
                                public int compare(SeatArrangement_Response.Datum u1, SeatArrangement_Response.Datum u2) {
                                    return u1.getCADCell().compareTo(u2.getCADCell());
                                }
                            });

                            if(((i+1)%max_column_length==0)){
                                listUpdated.addAll(listTemp);
                                listTemp = new ArrayList<>();
                            }

                        }





                        GridLayoutManager layoutManager = new GridLayoutManager(SeatArrangementReportActivity.this, max_column_length);
                        binding.rvData.setLayoutManager(layoutManager);
                        binding.rvData.setAdapter(new SeatArrangementReportAdapter(SeatArrangementReportActivity.this, listUpdated, max_column_length));
*/
                        setSeats();
                    } else {
                        Toast.makeText(SeatArrangementReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatArrangement_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    public void setSeats() {

        binding.llUpLower.removeAllViews();
        binding.llArrangement.removeAllViews();

        for (SeatArrangement_Response.Datum dataSeat : listSeatDetails) {
            if (dataSeat.getCADUDType() == 0) {
                if (max_column_length < dataSeat.getCADCell()) {
                    max_column_length = dataSeat.getCADCell();
                }
                upperSeatCounter++;
                //break;
            }

        }


        for (int i = 0; i < listSeatDetails.size(); i++) {

            SeatArrangement_Response.Datum data = listSeatDetails.get(i);

            if (data.getCADBlockType() == 3) {
                if (data.getIsAllowProcess() == 0) {
                    continue;
                }
            }


            RelativeLayout.LayoutParams params;
            final Button btn_Seat = new Button(SeatArrangementReportActivity.this);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;

            int buttonWidth;
            int buttonHeight;
            int buttonSpace;
            int seatnamesize;


            if (max_column_length > 9) {
                buttonWidth = ((width) / 5);
                buttonHeight = width / 10;
                buttonSpace = (width / 9) - 1;
                //seatnamesize = ((width / 6) / 4) - 10;
            } else {
                buttonWidth = (width / 4);
                buttonHeight = width / 9;
                buttonSpace = (width / 8) - 1;
                //seatnamesize = ((width / 6) / 4) - 5;
            }
            btn_Seat.setPadding(-4, -4, -4, -4);


            seatnamesize = 20;


            btn_Seat.setPadding(-4, -4, -4, -4);


            if (data.getCADColspan() > 0) {
                params = new RelativeLayout.LayoutParams(buttonWidth, buttonHeight);
            } else if (data.getCADRowSpan() > 0) {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonWidth);
            } else {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonHeight);
            }


            if (max_column_length >= 8) {
                params.setMargins(((data.getCADCell()) * buttonSpace), ((data.getCADRow() - 1) * buttonSpace), 0, 0);

                if (data.getCADRow() == 1) {
                    if (data.getCADCell() == 1) {
                        params.setMargins(((data.getCADCell()) * buttonSpace), 0, 0, 0);
                    }
                }

            } else {
                params.setMargins(((data.getCADCell() - 1) * buttonSpace), ((data.getCADRow() - 1) * buttonSpace), 0, 0);

                if (data.getCADRow() == 1) {
                    if (data.getCADCell() == 1) {
                        params.setMargins(((data.getCADCell() - 1) * buttonSpace), 0, 0, 0);
                    }
                }

            }


            btn_Seat.setId(i);
            btn_Seat.setLayoutParams(params);
            btn_Seat.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) seatnamesize);
            btn_Seat.setTypeface(null, Typeface.BOLD);
            btn_Seat.setText(data.getBADSeatNo());
            //btn_Seat.setOnClickListener(this);


            if (data.getIsAllowProcess() == 1) {
                if (!data.getJPGender().equalsIgnoreCase("MF")) {
                    String seatRemarks = btn_Seat.getText().toString().trim();
                    if (!TextUtils.isEmpty(data.getJMPassengerName())) {
                        seatRemarks = seatRemarks + "\n" + data.getJMPassengerName();
                        seatRemarks = seatRemarks + "\n" + data.getJMPhone1() + "[" + data.getJPGender() + "]";
                        if (!TextUtils.isEmpty(data.getRemarks())) {
                            seatRemarks = seatRemarks + "\n" + data.getRemarks();
                        }
                        if (!TextUtils.isEmpty(data.getBookingTypeName())) {
                            seatRemarks = seatRemarks + "\n" + data.getBookingTypeName();
                        }
                    }
                    SpannableString ss1 = new SpannableString(seatRemarks);
                    ss1.setSpan(new RelativeSizeSpan(0.5f), btn_Seat.getText().toString().trim().length(), seatRemarks.length(), 0); // set size// set color
                    btn_Seat.setText(ss1);
                }
            }

            Button btn_UpLow = new Button(SeatArrangementReportActivity.this);
            btn_UpLow.setBackgroundResource(android.R.color.transparent);

            if (data.getCADRow() == 1 && upperSeatCounter > 0) {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonHeight);
                if (max_column_length >= 8) {
                    params.leftMargin = ((data.getCADCell()) * buttonSpace) + 10;

                    if (data.getCADRow() == 1) {
                        if (data.getCADCell() == 1) {
                            params.leftMargin = data.getCADCell() * buttonSpace;
                        }
                    }

                } else {
                    params.leftMargin = ((data.getCADCell() - 1) * buttonSpace);
                    if (data.getCADRow() == 1) {
                        if (data.getCADCell() == 1) {
                            params.leftMargin = ((data.getCADCell() - 1) * buttonSpace);
                        }
                    }
                }

                params.topMargin = 0;
                params.rightMargin = 0;
                params.bottomMargin = 0;

                if (data.getCADUDType() == 0) {
                    btn_UpLow.setBackgroundResource(R.drawable.arr_ub);
                } else {
                    btn_UpLow.setBackgroundResource(R.drawable.arr_lb);
                }
                btn_UpLow.setLayoutParams(params);
            }

            if (upperSeatCounter > 0) {
                binding.llUpLower.addView(btn_UpLow);
                binding.llUpLower.setVisibility(View.VISIBLE);
            } else {
                binding.llUpLower.setVisibility(View.GONE);
            }


            binding.llArrangement.addView(btn_Seat);

        }

    }


    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SeatArrangementReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("SeatArrangement")) {
                                        SeatArrangementApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SeatArrangementReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


}
