package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.CityWiseSeatChartReport_Request;
import com.infinityinfoway.itsexe.api.response.CityWiseSeatChartReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityCityWiseSeatTypeChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.CityWiseSeatTypeChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityWiseSeatTypeChartActivity extends AppCompatActivity {

    private List<CityWiseSeatChartReport_Response.Datum> cityWiseSeatTypeChartList;
    private LinkedHashMap<String, List<CityWiseSeatChartReport_Response.Datum>> linkedHashMap;
    private LinkedHashMap<String, LinkedHashMap<String, List<CityWiseSeatChartReport_Response.Datum>>> linkedHashMap1;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "CityWiseSeatChartReport";
    private String resReportTitle, journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId;
    private Bundle getRouteData;
    private ActivityCityWiseSeatTypeChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCityWiseSeatTypeChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        cd = new ConnectionDetector(CityWiseSeatTypeChartActivity.this);
        getPref = new ITSExeSharedPref(CityWiseSeatTypeChartActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_CityWiseSeatTypeChartApiCall();

    }

    private void Get_Report_CityWiseSeatTypeChartApiCall() {

        CityWiseSeatChartReport_Request request = new CityWiseSeatChartReport_Request(
                cm_CompanyId,
                routeId,
                routeTimeId,
                journeyStartDate,
                fromCity,
                cityId,
                bookedByCompanyId,
                getPref.getBUM_BranchUserID(),
                isSameDay
        );

        Call<CityWiseSeatChartReport_Response> call = apiService.CityWiseSeatChartReport(request);
        call.enqueue(new Callback<CityWiseSeatChartReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<CityWiseSeatChartReport_Response> call, @NotNull Response<CityWiseSeatChartReport_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    cityWiseSeatTypeChartList = new ArrayList<>();

                    linkedHashMap1 = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            cityWiseSeatTypeChartList = response.body().getData();

                            CityWiseSeatChartReport_Response.Datum data = cityWiseSeatTypeChartList.get(0);

                            binding.txtJdate.setText("Journey Date: " + data.getJDate());
                            binding.txtJtime.setText("Time: " + data.getJMJourneyStartTime());
                            binding.txtBusNo.setText("Bus No: " + data.getBusNo());
                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtRouteName.setText("Route Name: " + data.getRouteName());



                            Collections.sort(cityWiseSeatTypeChartList, new Comparator<CityWiseSeatChartReport_Response.Datum>() {
                                @Override
                                public int compare(CityWiseSeatChartReport_Response.Datum u1, CityWiseSeatChartReport_Response.Datum u2) {
                                    return u1.getSer().compareTo(u2.getSer());
                                }
                            });

                            for (CityWiseSeatChartReport_Response.Datum pojo : cityWiseSeatTypeChartList) {


                                if (!linkedHashMap1.containsKey(pojo.getSer())) {
                                    linkedHashMap = new LinkedHashMap<>();
                                }

                                if (!linkedHashMap.containsKey(pojo.getBMBranchName())) {
                                    List<CityWiseSeatChartReport_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getBMBranchName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<CityWiseSeatChartReport_Response.Datum> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getBMBranchName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getBMBranchName(), pickDropPassengerDetailPojoList);
                                }


                                linkedHashMap1.put(pojo.getSer(), linkedHashMap);




                            }

                            CityWiseSeatTypeChartAdapter adapter = new CityWiseSeatTypeChartAdapter(CityWiseSeatTypeChartActivity.this, linkedHashMap1);
                            binding.rvBranch.setAdapter(adapter);


                            List<String> arrayList = getSeatDetailsData(cityWiseSeatTypeChartList);


                            binding.txtTotalSeat.setText(arrayList.get(0));
                            binding.txtTotalSlp.setText(arrayList.get(1));
                            binding.txtSeatAmount.setText(arrayList.get(2));
                            binding.txtSlpAmount.setText(arrayList.get(3));
                            binding.txtCommission.setText(arrayList.get(4));
                            binding.txtNetAmount.setText(arrayList.get(5));


                        }
                    } else {
                        Toast.makeText(CityWiseSeatTypeChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<CityWiseSeatChartReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(CityWiseSeatTypeChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_CityWiseSeatTypeChart")) {
                                        Get_Report_CityWiseSeatTypeChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }


    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(CityWiseSeatTypeChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private List<String> getSeatDetailsData(List<CityWiseSeatChartReport_Response.Datum> SeatDataList) {
        double totalSeatAmt = 0.0, totalSlpAmt = 0.0, totalComm = 0.0, totalNetAmt = 0.0;
        int totalSeat = 0, totalSlp = 0;
        List<String> arrayList = new ArrayList<>();
        for (CityWiseSeatChartReport_Response.Datum data : SeatDataList) {
            totalSeat += data.getSeat();
            totalSlp += data.getSLP();
            totalSeatAmt += Double.parseDouble(data.getSEATFARE());
            totalSlpAmt += Double.parseDouble(data.getSLPFARE());
            totalComm += Double.parseDouble(data.getBBICommAmount());
            totalNetAmt += Double.parseDouble(data.getBBINetAmount());
        }
        arrayList.add(String.valueOf(totalSeat));
        arrayList.add(String.valueOf(totalSlp));
        arrayList.add(String.format("%.2f", totalSeatAmt));
        arrayList.add(String.format("%.2f", totalSlpAmt));
        arrayList.add(String.format("%.2f", totalComm));
        arrayList.add(String.format("%.2f", totalNetAmt));
        return arrayList;
    }

}