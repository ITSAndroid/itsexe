package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_Report_Booking_Cancel_ReportByRouteTime_Request;
import com.infinityinfoway.itsexe.api.response.Get_Report_Booking_Cancel_ReportByRouteTime_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityCancelReportByRouteTimeBinding;
import com.infinityinfoway.itsexe.exe_adapter.CancelReportByRouteTimeAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelReportByRouteTimeActivity extends AppCompatActivity {

    CancelReportByRouteTimeAdapter cancelReportByRouteTimeAdapter;

    private List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime> list_cancel_Data;
    private LinkedHashMap<String, List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime>> linkedHashMap;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_Booking_Cancel_ReportByRouteTime";
    private String journeyStartDate, journeyStartTime,journeyCityTime,isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity,arrangementId, cityId;
    private Bundle getRouteData;
    private String resReportTitle = "";

    private ActivityCancelReportByRouteTimeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCancelReportByRouteTimeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
      //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
       // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(CancelReportByRouteTimeActivity.this);
        getPref = new ITSExeSharedPref(CancelReportByRouteTimeActivity.this);

        Get_Report_Booking_Cancel_ReportByRouteTimeApiCall();

    }

    private void Get_Report_Booking_Cancel_ReportByRouteTimeApiCall() {
        showProgressDialog("Loading....");

        Get_Report_Booking_Cancel_ReportByRouteTime_Request request = new Get_Report_Booking_Cancel_ReportByRouteTime_Request(
                journeyStartDate,
                routeId,
                routeTimeId,
                arrangementId,
                journeyStartTime,
                cm_CompanyId,
                cityId,
                getPref.getBUM_BranchUserID(),
                fromCity,
                bookedByCompanyId
        );

        Call<Get_Report_Booking_Cancel_ReportByRouteTime_Response> call = apiService.Get_Report_Booking_Cancel_ReportByRouteTime(request);
        call.enqueue(new Callback<Get_Report_Booking_Cancel_ReportByRouteTime_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Report_Booking_Cancel_ReportByRouteTime_Response> call, @NotNull Response<Get_Report_Booking_Cancel_ReportByRouteTime_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_cancel_Data = new ArrayList<>();


                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getBookingCancelReportByRouteTime() != null && response.body().getData().getBookingCancelReportByRouteTime().size() > 0) {

                            list_cancel_Data = response.body().getData().getBookingCancelReportByRouteTime();

                            linkedHashMap = new LinkedHashMap<>();

                            for (Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime data : list_cancel_Data) {
                                if (!linkedHashMap.containsKey(data.getGroupHeader())) {
                                    List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime> list = new ArrayList<>();
                                    list.add(data);
                                    linkedHashMap.put(data.getGroupHeader(), list);
                                } else {
                                    List<Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime> list = linkedHashMap.get(data.getGroupHeader());
                                    list.add(data);
                                    linkedHashMap.put(data.getGroupHeader(), list);
                                }
                            }

                            Get_Report_Booking_Cancel_ReportByRouteTime_Response.BookingCancelReportByRouteTime data = list_cancel_Data.get(0);

                            setViewUnderLine(binding.txtPnrTktNo);
                            setViewUnderLine(binding.txtUserCode);
                            setViewUnderLine(binding.txtBranchCode);
                            setViewUnderLine(binding.txtBookingDate);
                            setViewUnderLine(binding.txtType);
                            setViewUnderLine(binding.txtCancelBy);
                            setViewUnderLine(binding.txtCancelBranch);
                            setViewUnderLine(binding.txtCancelDate);

                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");
                            binding.txtRouteName.setText("Route : " + data.getRTRouteName());
                            binding.txtJourneyDate.setText("Journey Date: " + data.getJMJourneyStartDate());
                            binding.txtCompanyAddress.setText(data.getCMAddress());
                            binding.txtCompanyName.setText(data.getCMCompanyName());


                            cancelReportByRouteTimeAdapter = new CancelReportByRouteTimeAdapter(CancelReportByRouteTimeActivity.this, linkedHashMap);
                            binding.rvCancelReportRouteTime.setAdapter(cancelReportByRouteTimeAdapter);

                        }
                    } else {
                        Toast.makeText(CancelReportByRouteTimeActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Report_Booking_Cancel_ReportByRouteTime_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(CancelReportByRouteTimeActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_Booking_Cancel_ReportByRouteTime")) {
                                        Get_Report_Booking_Cancel_ReportByRouteTimeApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(CancelReportByRouteTimeActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private void setViewUnderLine(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

}
