package com.infinityinfoway.itsexe.report_activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.RouteTimeWisePickupChart_Request;
import com.infinityinfoway.itsexe.api.response.RouteTimeWisePickupChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityRouteTimeWisePickupChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.RouteTimeWisePickupChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteTimeWisePickupChartActivity extends AppCompatActivity {

    private LinkedHashMap<String, List<RouteTimeWisePickupChart_Response.Datum>> Hashmap_allDayMemo;
    private List<RouteTimeWisePickupChart_Response.Datum> list_pickup_inquiry;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "RouteTimeWisePickupChart";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId;
    private Bundle getRouteData;
    private String resReportTitle = "";
    private ActivityRouteTimeWisePickupChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRouteTimeWisePickupChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());

        cd = new ConnectionDetector(RouteTimeWisePickupChartActivity.this);
        getPref = new ITSExeSharedPref(RouteTimeWisePickupChartActivity.this);

        RouteTimeWisePickupChartApiCall();

    }

    private void RouteTimeWisePickupChartApiCall() {

        showProgressDialog("Loading....");


        /*journeyStartDate = "10/01/2019";
        fromCity = 1;
        toCity = 70;
        routeId = 16647;
        routeTimeId = 44757;
        journeyStartTime = "4:00 PM";
        journeyCityTime = "4:00 PM";
        arrangementId = 510;
        cityId = 70;
        companyId = 1;*/


        final RouteTimeWisePickupChart_Request request = new RouteTimeWisePickupChart_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartTime,
                journeyCityTime,
                1,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );

        Call<RouteTimeWisePickupChart_Response> call = apiService.RouteTimeWisePickupChart(request);
        call.enqueue(new Callback<RouteTimeWisePickupChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<RouteTimeWisePickupChart_Response> call, @NotNull Response<RouteTimeWisePickupChart_Response> response) {
                disMissDialog();


                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_pickup_inquiry = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_pickup_inquiry = response.body().getData();

                            RouteTimeWisePickupChart_Response.Datum data = list_pickup_inquiry.get(0);


                            binding.txtPrintBy.setText("Print By : " + data.getPrintBy());
                            binding.txtPrintDateTime.setText("Print Date : " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtRouteName.setText("Route : " + data.getRouteName());
                            binding.txtCoachNo.setText("Coach No. : " + data.getBusNo());
                            binding.txtJourneyDateTime.setText("Journey Date & Time : " + data.getJourneyDate() + "  " + data.getJourneyStartTime());

                            if (!TextUtils.isEmpty(data.getDMDriver1())) {
                                if (!TextUtils.isEmpty(data.getDMDriver2())) {
                                    binding.txtDriverName.setText("Driver Name : " + data.getDMDriver1() + " / " + data.getDMDriver2());
                                } else {
                                    binding.txtDriverName.setText("Driver Name : " + data.getDMDriver1());
                                }
                            } else {
                                binding.txtDriverName.setText("Driver Name : " + data.getDMDriver2());
                            }

                            binding.txtConductorName.setText("Conductor Name : " + data.getDMConductor());

                            binding.txtEmptySeatTotal.setText("Empty Seater : " + data.getTotalEmptySeat());
                            binding.txtTotalBookedSeat.setText("Total Seats Booked : " + calTotalBookedSeater());
                            binding.txtEmptySeaters.setText(data.getEmptySeat());


                            binding.txtEmptySleepeTotal.setText("Empty Sleeper : " + data.getTotalEmptySleeper());
                            binding.txtEmptySleepers.setText(data.getEmptySleeper());


                            for (RouteTimeWisePickupChart_Response.Datum pojo : list_pickup_inquiry) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getPickUpTime() + " : " + pojo.getPickUpName())) {
                                    List<RouteTimeWisePickupChart_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getPickUpTime() + " : " + pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<RouteTimeWisePickupChart_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getPickUpTime() + " : " + pojo.getPickUpName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getPickUpTime() + " : " + pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                }
                            }

                            binding.rvRouteTimePickup.setAdapter(new RouteTimeWisePickupChartAdapter(RouteTimeWisePickupChartActivity.this, Hashmap_allDayMemo));
                        }


                    } else {
                        Toast.makeText(RouteTimeWisePickupChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<RouteTimeWisePickupChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
            }
        });

    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(RouteTimeWisePickupChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(RouteTimeWisePickupChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("RouteTimeWisePickupChart")) {
                                        RouteTimeWisePickupChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private String calTotalBookedSeater() {
        int totalSubRoute = 0;
        for (RouteTimeWisePickupChart_Response.Datum data : list_pickup_inquiry) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSubRoute += Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSubRoute);
    }


}