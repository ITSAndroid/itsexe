package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.PickupDropWiseBookingDetails_Request;
import com.infinityinfoway.itsexe.api.response.PickupDropWiseBookingDetails_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityPickUpDropWiseBookingDetailsBinding;
import com.infinityinfoway.itsexe.exe_adapter.PickUpDropWiseBookingDetailsAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickUpDropWiseBookingDetailsActivity extends AppCompatActivity {

    PickUpDropWiseBookingDetailsAdapter pickUpDropWiseBookingDetailsAdapter;
    String method;
    ProgressDialog progDialog;
    LinkedHashMap<String, List<PickupDropWiseBookingDetails_Response.Datum>> Hashmap_allDayMemo;
    List<PickupDropWiseBookingDetails_Response.Datum> list_pickup_drop_wise_details;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "PickupDropWiseBookingDetails";
    private String resReportTitle,journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId, isSubRoute = 0;
    private Bundle getRouteData;

    private ActivityPickUpDropWiseBookingDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPickUpDropWiseBookingDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();
            cm_CompanyId = getRouteData.getInt("Company_Id");
            resReportTitle = getRouteData.getString("ReportTitle");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            isSameDay = getRouteData.getString("IsSameDay");
            isSubRoute = getRouteData.getInt("IsSubrRoute");
        }

        cd = new ConnectionDetector(PickUpDropWiseBookingDetailsActivity.this);
        getPref = new ITSExeSharedPref(PickUpDropWiseBookingDetailsActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_CityPickupDropWiseBookingApiCall();

    }

    private void Get_Report_CityPickupDropWiseBookingApiCall() {
        showProgressDialog("Loading....");

        PickupDropWiseBookingDetails_Request request = new PickupDropWiseBookingDetails_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartTime,
                journeyCityTime,
                0,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );


        Call<PickupDropWiseBookingDetails_Response> call = apiService.PickupDropWiseBookingDetails(request);
        call.enqueue(new Callback<PickupDropWiseBookingDetails_Response>() {
            @Override
            public void onResponse(@NotNull Call<PickupDropWiseBookingDetails_Response> call, @NotNull Response<PickupDropWiseBookingDetails_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_pickup_drop_wise_details = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            list_pickup_drop_wise_details = response.body().getData();

                            PickupDropWiseBookingDetails_Response.Datum data = list_pickup_drop_wise_details.get(0);

                            binding.txtPrintDate.setText("Print Date : " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtRouteName.setText("Route : " + data.getRouteName() + " " + data.getJourneyStartTime());
                            binding.txtCoachNo.setText("Coach No. : " + data.getBusNo());
                            binding.txtJourneyDateTime.setText("Journey Date & Time : " + data.getJourneyDate() + "  " + data.getJourneyCityTime());
                            binding.txtDriverName.setText("Driver Name : " + data.getDMDriver1());
                            binding.txtConductorName.setText("Conductor Name : " + data.getDMConductor());

                            binding.txtSeatSleeper.setText(data.getEmptySleeper());


                            binding.emptySleeper.setText("Empty Sleeper : " + data.getTotalEmptySleeper());
                            binding.totalBookedSeaters.setText("Total Seats Booked : " + calTotalSeat(list_pickup_drop_wise_details));
                            binding.txtSeatSeaters.setText(data.getEmptySeat());
                            binding.emptySeaters.setText("Empty Seater : " + data.getTotalEmptySeat());


                            Hashmap_allDayMemo = new LinkedHashMap<>();

                            for (PickupDropWiseBookingDetails_Response.Datum pojo : list_pickup_drop_wise_details) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getPickUpName())) {
                                    List<PickupDropWiseBookingDetails_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<PickupDropWiseBookingDetails_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getPickUpName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                }
                            }
                            pickUpDropWiseBookingDetailsAdapter = new PickUpDropWiseBookingDetailsAdapter(PickUpDropWiseBookingDetailsActivity.this, Hashmap_allDayMemo);
                            binding.recyclerReport.setAdapter(pickUpDropWiseBookingDetailsAdapter);
                        }
                    } else {
                        Toast.makeText(PickUpDropWiseBookingDetailsActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PickupDropWiseBookingDetails_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(PickUpDropWiseBookingDetailsActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_CityPickupDropWiseBooking")) {
                                        Get_Report_CityPickupDropWiseBookingApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(PickUpDropWiseBookingDetailsActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


    private String calTotalSeat(List<PickupDropWiseBookingDetails_Response.Datum> SeatDataList){
        int totalSeat=0;
        for(PickupDropWiseBookingDetails_Response.Datum data:SeatDataList){
            if(!TextUtils.isEmpty(data.getTotalSeat())){
                totalSeat+=Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSeat);
    }
}
