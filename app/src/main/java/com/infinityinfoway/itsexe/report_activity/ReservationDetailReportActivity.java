package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.ReservationDetailReport_Request;
import com.infinityinfoway.itsexe.api.response.ReservationDetailReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityReservationDetailReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.ReservationDetailReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservationDetailReportActivity extends AppCompatActivity {

    private List<ReservationDetailReport_Response.ReservationDetail> reservationDetailList;
    private LinkedHashMap<String, List<ReservationDetailReport_Response.ReservationDetail>> linkedHashMap;


    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "ReservationDetailReport";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId, isSubRoute;
    private Bundle getRouteData;
    private String resReportTitle = "";

    private ActivityReservationDetailReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityReservationDetailReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            isSubRoute = getRouteData.getInt("IsSubRoute");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(ReservationDetailReportActivity.this);
        getPref = new ITSExeSharedPref(ReservationDetailReportActivity.this);

        ReservationDetailReportApiCall();

    }

    private void ReservationDetailReportApiCall() {
        showProgressDialog("Loading....");

        ReservationDetailReport_Request request = new ReservationDetailReport_Request(
                cm_CompanyId,
                routeId,
                routeTimeId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                journeyStartTime,
                journeyCityTime,
                0,
                arrangementId,
                cityId,
                isSameDay
        );


        Call<ReservationDetailReport_Response> call = apiService.ReservationDetailReport(request);
        call.enqueue(new Callback<ReservationDetailReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<ReservationDetailReport_Response> call, @NotNull Response<ReservationDetailReport_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getReservationDetails() != null && response.body().getData().getReservationDetails().size() > 0) {

                            reservationDetailList = response.body().getData().getReservationDetails();

                            linkedHashMap = new LinkedHashMap<>();

                            for (ReservationDetailReport_Response.ReservationDetail dataReservation : reservationDetailList) {
                                if (!linkedHashMap.containsKey(dataReservation.getPMPickupName())) {
                                    List<ReservationDetailReport_Response.ReservationDetail> list = new ArrayList<>();
                                    list.add(dataReservation);
                                    linkedHashMap.put(dataReservation.getPMPickupName(), list);
                                } else {
                                    List<ReservationDetailReport_Response.ReservationDetail> list = linkedHashMap.get(dataReservation.getPMPickupName());
                                    list.add(dataReservation);
                                    linkedHashMap.put(dataReservation.getPMPickupName(), list);
                                }
                            }

                            binding.rvData.setAdapter(new ReservationDetailReportAdapter(ReservationDetailReportActivity.this, linkedHashMap));


                            // Summary
                            ReservationDetailReport_Response.ReservationDetail data = reservationDetailList.get(0);


                            binding.txtCompanyName.setText(data.getCMCompanyName());
                            binding.txtReportName.setText(data.getReprotTitle());

                            binding.txtDate.setText(data.getJMJourneyStartDate());
                            binding.txtCoachNo.setText(data.getBusNo());
                            binding.txtSeats.setText(String.valueOf(data.getTotalSeats()));

                            binding.txtServiceNo.setText(data.getServiceType());
                            binding.txtRoute.setText(data.getRouteName());
                            binding.txtReserved.setText(String.valueOf(data.getBookedSeats()));

                            binding.txtOrigin.setText(data.getRouteOrigin());
                            binding.txtDriver1.setText(data.getDriver1());
                            binding.txtUnreserved.setText(String.valueOf(data.getEmptySeats()));

                            binding.txtDest.setText(data.getRouteDestination());
                            binding.txtDriver2.setText(data.getDriver2());
                            binding.txtBlocked.setText(String.valueOf(data.getBlockedSeats()));

                            binding.txtDepartTime.setText(data.getRouteDepartureTime());
                            binding.txtChkInspector.setText("");
                            binding.txtJtime.setText(data.getFinalJourneyTime());

                            binding.txtDropOff.setText(data.getDroppingInfo());
                            binding.txtBoardingOff.setText(data.getBoardingInfo());


                        } else {
                            Toast.makeText(ReservationDetailReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ReservationDetailReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<ReservationDetailReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ReservationDetailReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("ReservationDetailReport")) {
                                        ReservationDetailReportApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(ReservationDetailReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}
