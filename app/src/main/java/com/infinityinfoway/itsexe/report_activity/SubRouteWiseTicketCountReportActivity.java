package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SubRouteWiseTicketCount_Request;
import com.infinityinfoway.itsexe.api.response.SubRouteWiseTicketCount_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySubRouteWiseTicketCountReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.SubRouteWiseTicketCountReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubRouteWiseTicketCountReportActivity extends AppCompatActivity {

    private LinkedHashMap<String, LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>>> hashMap_data;
    private LinkedHashMap<String, List<SubRouteWiseTicketCount_Response.Datum>> linkedHashMap;
    private List<SubRouteWiseTicketCount_Response.Datum> list_subroute_data;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_SubRouteWiseTicketCountReport";
    private String resReportTitle,journeyStartDate;
    private int cm_CompanyId, cityId,bookedByCompanyId;
    private Bundle getRouteData;

    private ActivitySubRouteWiseTicketCountReportBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivitySubRouteWiseTicketCountReportBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            cityId = getRouteData.getInt("CityId");

        }

        cd = new ConnectionDetector(SubRouteWiseTicketCountReportActivity.this);
        getPref = new ITSExeSharedPref(SubRouteWiseTicketCountReportActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        //binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_SubRouteWiseTicketCountReportApiCall();



    }

    private void Get_Report_SubRouteWiseTicketCountReportApiCall() {
        showProgressDialog("Loading....");


        SubRouteWiseTicketCount_Request request = new SubRouteWiseTicketCount_Request(
                journeyStartDate,
                cm_CompanyId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId
        );


        Call<SubRouteWiseTicketCount_Response> call = apiService.SubRouteWiseTicketCount(request);
        call.enqueue(new Callback<SubRouteWiseTicketCount_Response>() {
            @Override
            public void onResponse(@NotNull Call<SubRouteWiseTicketCount_Response> call, @NotNull Response<SubRouteWiseTicketCount_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_subroute_data = new ArrayList<>();
                    hashMap_data = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            list_subroute_data = response.body().getData();

                            binding.txtCompanyName.setText(list_subroute_data.get(0).getCMCompanyName());
                            binding.txtCompanyAddress.setText(list_subroute_data.get(0).getCMAddress());
                            binding.txtCompanySortName.setText(list_subroute_data.get(0).getCMCompanyShortName());


                            binding.txtJourneyDate.setText("Journey Date: " + list_subroute_data.get(0).getJMJourneyStartDate());
                            binding.txtReportDate.setText("Report Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                            List<String> arrayList=getRouteTotalSeatAmt(list_subroute_data);

                            binding.totalSeat.setText("Total Seats: " + arrayList.get(0));
                            binding.txtTotalAmt.setText("Total Amount (In Rs.):  " + arrayList.get(1));


                            for (SubRouteWiseTicketCount_Response.Datum data : list_subroute_data) {

                                if (!hashMap_data.containsKey(data.getRouteTime())) {
                                    linkedHashMap=new LinkedHashMap<>();
                                }

                                if (!linkedHashMap.containsKey(data.getRMRouteNameDisplay())) {
                                    List<SubRouteWiseTicketCount_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(data);
                                    linkedHashMap.put(data.getRMRouteNameDisplay(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<SubRouteWiseTicketCount_Response.Datum> pickDropPassengerDetailPojoList = linkedHashMap.get(data.getRMRouteNameDisplay());
                                    pickDropPassengerDetailPojoList.add(data);
                                    linkedHashMap.put(data.getRMRouteNameDisplay(), pickDropPassengerDetailPojoList);
                                }

                                hashMap_data.put(data.getRouteTime(),linkedHashMap);

                            }

                            binding.rvRouteTimeTicketCount.setAdapter(new SubRouteWiseTicketCountReportAdapter(SubRouteWiseTicketCountReportActivity.this, hashMap_data));

                        }
                    } else {
                        Toast.makeText(SubRouteWiseTicketCountReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SubRouteWiseTicketCount_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SubRouteWiseTicketCountReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_SubRouteWiseTicketCountReport")) {
                                        Get_Report_SubRouteWiseTicketCountReportApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SubRouteWiseTicketCountReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


    /*public class LazyDataConnection_ExeApi extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData_Exe.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {
            if (xmlResponse.equals("")) {
                if (method.equals("Get_BusConnectExe_Report_SubRouteWiseTicketCountReport")) {
                    try {
                        progDialog.dismiss();
                    } catch (Exception ex) {
                    }
                }
                Intent i = new Intent(SubRouteWiseTicketCountReportActivity.this, InternetErrorActivity.class);
                i.putExtra("caller", "ExeBookingActivity");
                startActivity(i);
                return;
            }
            if (method.equals("Get_BusConnectExe_Report_SubRouteWiseTicketCountReport")) {

                Get_BusConnectExe_Report_SubRouteWiseTicketCountReport(xmlResponse, "SubRouteWiseTicketCountReport");
                try {
                    progDialog.dismiss();
                } catch (Exception ex) {
                }

                if (list_subroute_data != null && list_subroute_data.size() > 0) {
                    txt_company_name.setText(list_subroute_data.get(0).getCM_CompanyName());
                    txt_company_address.setText(list_subroute_data.get(0).getCM_Address());
                    txt_company_sort_name.setText(list_subroute_data.get(0).getCM_CompanyShortName());

                    txt_print_Date.setText("Print Date: " + new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
                    txt_journey_date.setText("Journey Date: " + list_subroute_data.get(0).getJM_JourneyStartDate());

                    txt_total_seats.setText("Total Seats \n" + list_subroute_data.get(0).getTotalSeat());
                    txt_total_amount.setText("Total Commission \n" + list_subroute_data.get(0).getJM_PayableAmount());

                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    for (SubRouteWiseTicketCountReportBean pojo : list_subroute_data) {

                        if (!Hashmap_allDayMemo.containsKey(pojo.getRM_RouteNameDisplay())) {
                            List<SubRouteWiseTicketCountReportBean> pickDropPassengerDetailPojoList = new ArrayList<>();
                            pickDropPassengerDetailPojoList.add(pojo);
                            Hashmap_allDayMemo.put(pojo.getRM_RouteNameDisplay(), pickDropPassengerDetailPojoList);
                        } else {
                            List<SubRouteWiseTicketCountReportBean> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getRM_RouteNameDisplay());
                            pickDropPassengerDetailPojoList.add(pojo);
                            Hashmap_allDayMemo.put(pojo.getRM_RouteNameDisplay(), pickDropPassengerDetailPojoList);
                        }
                    }

                    subRouteWiseTicketCountReportAdapter = new SubRouteWiseTicketCountReportAdapter(SubRouteWiseTicketCountReportActivity.this, Hashmap_allDayMemo);
                    recycler_route_time_ticket_count.setAdapter(subRouteWiseTicketCountReportAdapter);
                }
            }
        }
    }*/

   /* public void Get_BusConnectExe_Report_SubRouteWiseTicketCountReport(String str_RespXML, String str_TagName) {
        Document doc = getData_Exe.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        list_subroute_data = new ArrayList<>();
        for (int i = 0; i <= nodes.getLength(); i++) {
            Node e1 = nodes.item(i);
            Element el = (Element) e1;

            if (el != null) {
                SubRouteWiseTicketCountReportBean allDayMemoReportBean = new SubRouteWiseTicketCountReportBean();


                try {
                    allDayMemoReportBean.setJM_JourneyStartDate(el.getElementsByTagName("JM_JourneyStartDate").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_CompanyID(el.getElementsByTagName("CM_CompanyID").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRM_RouteID(el.getElementsByTagName("RM_RouteID").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRT_Time(el.getElementsByTagName("RT_Time").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRM_FromCityID(el.getElementsByTagName("RM_FromCityID").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_JourneyFrom(el.getElementsByTagName("JM_JourneyFrom").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_JourneyTo(el.getElementsByTagName("JM_JourneyTo").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTotalSeat(el.getElementsByTagName("TotalSeat").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_PayableAmount(el.getElementsByTagName("JM_PayableAmount").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRM_RouteNameDisplay(el.getElementsByTagName("RM_RouteNameDisplay").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRouteTime(el.getElementsByTagName("RouteTime").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setFromCityToCity(el.getElementsByTagName("FromCityToCity").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_CompanyName(el.getElementsByTagName("CM_CompanyName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_Address(el.getElementsByTagName("CM_Address").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_CompanyShortName(el.getElementsByTagName("CM_CompanyShortName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTmp1(el.getElementsByTagName("Tmp1").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTmp2(el.getElementsByTagName("Tmp2").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTmp3(el.getElementsByTagName("Tmp3").item(0).getTextContent());
                } catch (Exception e) {
                }

                list_subroute_data.add(allDayMemoReportBean);
            }
        }
    }*/

    private List<String> getRouteTotalSeatAmt(List<SubRouteWiseTicketCount_Response.Datum> list) {
        int totalRouteSeat = 0;
        double totalRouteAmt = 0.0;

        for (SubRouteWiseTicketCount_Response.Datum data : list) {
            totalRouteSeat += data.getTotalSeat();
            totalRouteAmt += data.getJMPayableAmount();
        }

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.valueOf(totalRouteAmt));
        return arrayList;

    }

}
