package com.infinityinfoway.itsexe.report_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.AllDayBranchWiseReturnOnwardMemo_Request;
import com.infinityinfoway.itsexe.api.response.AllDayBranchWiseReturnOnwardMemo_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.bean.NetworkModel;
import com.infinityinfoway.itsexe.custom_dialog.OptAnimationLoader;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityAllDayBranchWiseReturnOnwardMemoBinding;
import com.infinityinfoway.itsexe.databinding.LayoutBottomSheetInternetConnectionBinding;
import com.infinityinfoway.itsexe.exe_adapter.AllDayBranchWiseReturnOnwardMemoAdapter;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;
import com.infinityinfoway.itsexe.utils.CustomSnackBar;
import com.infinityinfoway.itsexe.utils.CustomToast;
import com.infinityinfoway.itsexe.utils.NetworkLiveData;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_model.AllDayBranchWiseReturnOnwardMemo_ViewModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class AllDayBranchWiseReturnOnwardMemoActivity extends AppCompatActivity {

    private AllDayBranchWiseReturnOnwardMemoAdapter allDayMemoBranchWiseAdapter;
    private LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>> Hashmap_allDayMemo = new LinkedHashMap<>();
    private LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>> linkedHashMap;
    private List<AllDayBranchWiseReturnOnwardMemo_Response.Datum> list_allDayMemo;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private ActivityAllDayBranchWiseReturnOnwardMemoBinding binding;
    private String journeyStartDate;
    private int cm_CompanyId, bookedByCompanyId;
    private String resReportTitle = "";
    private AllDayBranchWiseReturnOnwardMemo_ViewModel allDayMemoBranchWiseViewModel;

    private BottomSheetDialog bottomSheetDialog;
    private LayoutBottomSheetInternetConnectionBinding internetConnectionBinding;
    private int isApiCallSucess=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAllDayBranchWiseReturnOnwardMemoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("Company_Id")) {
            Bundle getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(AllDayBranchWiseReturnOnwardMemoActivity.this);
        getPref = new ITSExeSharedPref(AllDayBranchWiseReturnOnwardMemoActivity.this);


        setupRecyclerView();

        allDayMemoBranchWiseViewModel = new ViewModelProvider(AllDayBranchWiseReturnOnwardMemoActivity.this).get(AllDayBranchWiseReturnOnwardMemo_ViewModel.class);
       // allDayMemoBranchWiseViewModel.init(apiRequest());

        NetworkLiveData networkLiveData = new NetworkLiveData(AllDayBranchWiseReturnOnwardMemoActivity.this);
        networkLiveData.observe(AllDayBranchWiseReturnOnwardMemoActivity.this, new Observer<NetworkModel>() {
            @Override
            public void onChanged(NetworkModel networkModel) {
                switch (networkModel.getIsConnected()) {
                    case CONNECTED:
                        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
                            bottomSheetDialog.dismiss();
                        }
                        if(isApiCallSucess==1){
                            allDayMemoBranchWiseViewModel.init(apiRequest());
                        }
                        break;
                    case CONNECTING:
                        internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                        internetConnectionBinding.loading.setVisibility(View.VISIBLE);
                        break;
                    case DISCONNECTED:
                        showBottomSheetDialog();
                        break;
                    case DISCONNECTING:
                        break;
                    case SUSPENDED:
                        break;
                    case UNKNOWN:
                        break;
                }
            }
        });

        allDayMemoBranchWiseViewModel.mutableProgressStatus.observe(AllDayBranchWiseReturnOnwardMemoActivity.this, new Observer<PROGRESS_STATUS>() {
            @Override
            public void onChanged(PROGRESS_STATUS progress_status) {
                switch (progress_status) {
                    case SHOW:
                        showProgressDialog("Loading....");
                        break;
                    case DISMISS:
                        disMissDialog();
                        break;
                    case BAD_RESPONSE:
                        disMissDialog();
                        isApiCallSucess=1;
                        break;

                }
            }
        });

        allDayMemoBranchWiseViewModel.getAllDayBranchWiseReturnOnwardMemo().observe(AllDayBranchWiseReturnOnwardMemoActivity.this, new Observer<LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>>>() {
            @Override
            public void onChanged(LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>> newsResponses) {
                if (newsResponses != null) {
                    Hashmap_allDayMemo.clear();
                    Hashmap_allDayMemo.putAll(newsResponses);
                    allDayMemoBranchWiseAdapter.refreshAdapter(Hashmap_allDayMemo);
                    isApiCallSucess=2;
                    if (allDayMemoBranchWiseAdapter != null && allDayMemoBranchWiseAdapter.getItemCount() > 0) {
                        setInitialData();
                    }
                } else {
                    showSnackBar(APP_CONSTANTS.NO_DATA, APP_CONSTANTS.NO_DATA_CONTENT, Snackbar.LENGTH_LONG, 1);
                    //mErrorInAnim = OptAnimationLoader.loadAnimation(AllDayBranchWiseReturnOnwardMemo.this, R.anim.error_frame_in);
                    // mErrorXInAnim = (AnimationSet)OptAnimationLoader.loadAnimation(AllDayBranchWiseReturnOnwardMemo.this, R.anim.error_x_in);

                   /* View view = snackbar.getView();
                    TextView tv = view.findViewById(R.id.snackbar_action);
                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackbar.dismiss();
                        }
                    });*/
                }
            }
        });

    }

    private void showSnackBar(String title, String desc, int duration, int is_error_img) {
        final Snackbar snackbar = CustomSnackBar.showSnackbar(AllDayBranchWiseReturnOnwardMemoActivity.this, binding.llMain, title, desc, duration, 1);
        snackbar.show();
        if (is_error_img == 1) {
            ImageView imgError = snackbar.getView().findViewById(R.id.img_error);
            imgError.clearAnimation();
            imgError.setVisibility(View.VISIBLE);

            imgError.setAnimation(OptAnimationLoader.loadAnimation(AllDayBranchWiseReturnOnwardMemoActivity.this, R.anim.error_x_in));
        }
    }

    private void showBottomSheetDialog() {
        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
            bottomSheetDialog.dismiss();
        }

        internetConnectionBinding = LayoutBottomSheetInternetConnectionBinding.inflate(getLayoutInflater());
        // final View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, false);
        final View view = internetConnectionBinding.getRoot();
        internetConnectionBinding.btnCheckNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                } catch (Exception ignored) {
                }

                internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                internetConnectionBinding.loading.setVisibility(View.VISIBLE);

                Handler mHand = new Handler(Looper.getMainLooper());
                mHand.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (!cd.isConnectingToInternet()) {
                            internetConnectionBinding.btnCheckNet.setVisibility(View.VISIBLE);
                            internetConnectionBinding.loading.setVisibility(View.GONE);
                            CustomToast.showToastMessage(AllDayBranchWiseReturnOnwardMemoActivity.this, "No Internet connection", 0);
                        } else {
                            internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                            internetConnectionBinding.loading.setVisibility(View.VISIBLE);
                        }
                    }
                }, 2000);


            }
        });

        //View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, null);
        bottomSheetDialog = new BottomSheetDialog(AllDayBranchWiseReturnOnwardMemoActivity.this);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);


    }

    private AllDayBranchWiseReturnOnwardMemo_Request apiRequest() {


        /*AllDayBranchWiseReturnOnwardMemo_Request request = new AllDayBranchWiseReturnOnwardMemo_Request(
                20,
                "02-03-2021",
                243,
                0,
                20,
                384
        );*/


        return new AllDayBranchWiseReturnOnwardMemo_Request(
                cm_CompanyId,
                journeyStartDate,
                getPref.getBM_BranchID(),
                0,
                bookedByCompanyId,
                getPref.getBUM_BranchUserID()
        );
    }

    private void setupRecyclerView() {
        allDayMemoBranchWiseAdapter = new AllDayBranchWiseReturnOnwardMemoAdapter(AllDayBranchWiseReturnOnwardMemoActivity.this, Hashmap_allDayMemo);
        binding.rvAllDayMemo.setAdapter(allDayMemoBranchWiseAdapter);
    }

    private void setInitialData() {

        linkedHashMap = new LinkedHashMap<>();
        list_allDayMemo = new ArrayList<>();

        List<String> key_list = new ArrayList<>(Hashmap_allDayMemo.keySet());
        linkedHashMap = Hashmap_allDayMemo.get(key_list.get(0));

        List<String> key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        list_allDayMemo = linkedHashMap.get(key_list_V2.get(0));

        AllDayBranchWiseReturnOnwardMemo_Response.Datum data = list_allDayMemo.get(0);

        binding.txtCompanyName.setText(data.getCompanyName());
        binding.txtCompanyAddress.setText(data.getCompanyAddress());
        //binding.txtComissionInfo.setText(data.get);

        binding.txtBranchName.setText(data.getBMBranchName());
        binding.txtDate.setText("Date: " + data.getJDate());
        binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

        binding.txtTotalCollection.setText("Total Collection \n" + String.format("%.2f", data.getTotalCollection()));
        binding.txtCommission.setText("Total Commission \n" + String.format("%.2f", data.getCommissionAmount()));
        binding.txtTotalSeats.setText("Total Seats \n" + data.getNetSeat());
        binding.txtNetCollection.setText("Net Collection \n" + String.format("%.2f", data.getNetCollection()));

    }


    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(AllDayBranchWiseReturnOnwardMemoActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


}