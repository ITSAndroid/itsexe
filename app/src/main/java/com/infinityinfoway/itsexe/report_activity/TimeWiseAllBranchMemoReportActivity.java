package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.TimewiseAllBranchMemo_Request;
import com.infinityinfoway.itsexe.api.response.TimewiseAllBranchMemo_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityTimeWiseAllBranchMemoBinding;
import com.infinityinfoway.itsexe.exe_adapter.TimewiseAllBranchMemoAdapter;
import com.infinityinfoway.itsexe.exe_adapter.TimewiseBranchMemoAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeWiseAllBranchMemoReportActivity extends AppCompatActivity {

    private TimewiseBranchMemoAdapter timewiseBranchMemoAdapter;
    private LinkedHashMap<String, List<TimewiseAllBranchMemo_Response.Datum>> linkedHashMap;
    private List<TimewiseAllBranchMemo_Response.Datum> list_allocation;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "TimewiseAllBranchMemo";

    private String journeyStartDate, journeyStartTime, journeyCityTime,isSameDay;
    private int cm_CompanyId,bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private Bundle getRouteData;

    private ActivityTimeWiseAllBranchMemoBinding binding;
    private String resReportTitle = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityTimeWiseAllBranchMemoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId=getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }


        cd = new ConnectionDetector(TimeWiseAllBranchMemoReportActivity.this);
        getPref = new ITSExeSharedPref(TimeWiseAllBranchMemoReportActivity.this);


        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        TimewiseAllBranchMemoApiCall();


    }

    private void TimewiseAllBranchMemoApiCall() {

        showProgressDialog("Loading....");



        TimewiseAllBranchMemo_Request request = new TimewiseAllBranchMemo_Request(
                cm_CompanyId,
                journeyStartDate,
                routeId,
                routeTimeId,
                journeyStartTime,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                fromCity,
                bookedByCompanyId,
                isSameDay
        );

        Call<TimewiseAllBranchMemo_Response> call = apiService.TimewiseAllBranchMemo(request);
        call.enqueue(new Callback<TimewiseAllBranchMemo_Response>() {
            @Override
            public void onResponse(@NotNull Call<TimewiseAllBranchMemo_Response> call, @NotNull Response<TimewiseAllBranchMemo_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_allocation = new ArrayList<>();
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_allocation = response.body().getData();
                            TimewiseAllBranchMemo_Response.Datum data = list_allocation.get(0);

                            binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                            binding.txtCompanyName.setText(data.getCompanyName());
                            binding.txtCompanyAddress.setText(data.getCompanyAddress());
                            binding.txtRoute.setText("Route: " + data.getRouteName() + "-" + data.getCommission());
                            binding.txtDate.setText("Date: " + data.getJDate());
                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By: " + data.getPrintBy());

                            linkedHashMap = new LinkedHashMap<>();

                            for (TimewiseAllBranchMemo_Response.Datum pojo : list_allocation) {

                                if (!linkedHashMap.containsKey(pojo.getBranchName())) {
                                    List<TimewiseAllBranchMemo_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getBranchName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<TimewiseAllBranchMemo_Response.Datum> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getBranchName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getBranchName(), pickDropPassengerDetailPojoList);
                                }

                            }

                            binding.rvData.setAdapter(new TimewiseAllBranchMemoAdapter(TimeWiseAllBranchMemoReportActivity.this, linkedHashMap));

                            binding.txtTotalCollection.setText("Total Collection\n" + String.format("%.2f", data.getTotalCollection()));
                            binding.txtTotalServiceTax.setText("GST\n" + String.format("%.2f", data.getTotalServiceTax()));
                            binding.txtNetCollection.setText("Net Collection\n" + String.format("%.2f", data.getNetCollection()));
                            binding.txtCommission.setText("Commission\n" + String.format("%.2f", data.getCommissionAmount()));
                            binding.txtTotalSeats.setText("Seats\n" + data.getNetSeat());
                            binding.txtNetBalance.setText("Net Balance\n" + String.format("%.2f", data.getNetBalance()));

                        } else {
                            Toast.makeText(TimeWiseAllBranchMemoReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(TimeWiseAllBranchMemoReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    disMissDialog();
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<TimewiseAllBranchMemo_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(TimeWiseAllBranchMemoReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(TimeWiseAllBranchMemoReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("TimewiseAllBranchMemo")) {
                                        TimewiseAllBranchMemoApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }


}
