package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_Report_AllDayMemo_BranchWise_Request;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_BranchWise_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.bean.NetworkModel;
import com.infinityinfoway.itsexe.custom_dialog.OptAnimationLoader;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityAllDayBranchWiseMemoBinding;
import com.infinityinfoway.itsexe.databinding.LayoutBottomSheetInternetConnectionBinding;
import com.infinityinfoway.itsexe.exe_adapter.AllDayMemoBranchWiseAdapter;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;
import com.infinityinfoway.itsexe.utils.CustomSnackBar;
import com.infinityinfoway.itsexe.utils.CustomToast;
import com.infinityinfoway.itsexe.utils.NetworkLiveData;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_model.AllDayMemo_BranchWise_ViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDayBranchWiseMemoActivity extends AppCompatActivity {

    private AllDayMemoBranchWiseAdapter allDayMemoBranchWiseAdapter;
    private LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> Hashmap_allDayMemo = new LinkedHashMap<>();
    private LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>> linkedHashMap;
    private List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> list_allDayMemo;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_AllDayMemo_BranchWise";
    private String journeyStartDate;
    private int companyId;
    private ActivityAllDayBranchWiseMemoBinding binding;
    private String resReportTitle = "";
    private AllDayMemo_BranchWise_ViewModel allDayMemoBranchWiseViewModel;
    private int isApiCallSucess=1;
    private BottomSheetDialog bottomSheetDialog;
    private LayoutBottomSheetInternetConnectionBinding internetConnectionBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAllDayBranchWiseMemoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //setContentView(R.layout.activity_all_day_branch_wise_memo);

        if (getIntent().hasExtra("Company_Id")) {
            Bundle getRouteData = getIntent().getExtras();
            resReportTitle = getRouteData.getString("ReportTitle");
            companyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(AllDayBranchWiseMemoActivity.this);
        getPref = new ITSExeSharedPref(AllDayBranchWiseMemoActivity.this);

        //Get_Report_AllDayMemo_BranchWiseApiCall();

        setupRecyclerView();

        allDayMemoBranchWiseViewModel = new ViewModelProvider(AllDayBranchWiseMemoActivity.this).get(AllDayMemo_BranchWise_ViewModel.class);

        NetworkLiveData networkLiveData = new NetworkLiveData(AllDayBranchWiseMemoActivity.this);
        networkLiveData.observe(AllDayBranchWiseMemoActivity.this, new Observer<NetworkModel>() {
            @Override
            public void onChanged(NetworkModel networkModel) {
                switch (networkModel.getIsConnected()) {
                    case CONNECTED:
                        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
                            bottomSheetDialog.dismiss();
                        }
                        if(isApiCallSucess==1){
                            allDayMemoBranchWiseViewModel.init(apiRequest());
                        }
                        break;
                    case CONNECTING:
                        internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                        internetConnectionBinding.loading.setVisibility(View.VISIBLE);
                        break;
                    case DISCONNECTED:
                        showBottomSheetDialog();
                        break;
                    case DISCONNECTING:
                        break;
                    case SUSPENDED:
                        break;
                    case UNKNOWN:
                        break;
                }
            }
        });

        allDayMemoBranchWiseViewModel.mutableProgressStatus.observe(AllDayBranchWiseMemoActivity.this, new Observer<PROGRESS_STATUS>() {
            @Override
            public void onChanged(PROGRESS_STATUS progress_status) {
                switch (progress_status) {
                    case SHOW:
                        showProgressDialog("Loading....");
                        break;
                    case DISMISS:
                        disMissDialog();
                        break;
                    case BAD_RESPONSE:
                        disMissDialog();
                        isApiCallSucess=1;
                        break;
                }
            }
        });

        allDayMemoBranchWiseViewModel.getAllDayMemoBranchWiseList().observe(AllDayBranchWiseMemoActivity.this, new Observer<LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>>>() {
            @Override
            public void onChanged(LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> newsResponses) {
                if (newsResponses != null) {
                    Hashmap_allDayMemo.clear();
                    Hashmap_allDayMemo.putAll(newsResponses);
                    allDayMemoBranchWiseAdapter.refreshAdapter(Hashmap_allDayMemo);
                    isApiCallSucess=2;
                    if (allDayMemoBranchWiseAdapter != null && allDayMemoBranchWiseAdapter.getItemCount() > 0) {

                        setInitialData();
                    }
                } else {
                    showSnackBar( APP_CONSTANTS.NO_DATA, APP_CONSTANTS.NO_DATA_CONTENT, Snackbar.LENGTH_LONG,1);
                    //mErrorInAnim = OptAnimationLoader.loadAnimation(AllDayBranchWiseMemoActivity.this, R.anim.error_frame_in);
                    // mErrorXInAnim = (AnimationSet)OptAnimationLoader.loadAnimation(AllDayBranchWiseMemoActivity.this, R.anim.error_x_in);

                   /* View view = snackbar.getView();
                    TextView tv = view.findViewById(R.id.snackbar_action);
                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackbar.dismiss();
                        }
                    });*/
                }
            }
        });

    }

    private void showSnackBar(String title, String desc, int duration, int is_error_img) {
        final Snackbar snackbar = CustomSnackBar.showSnackbar(AllDayBranchWiseMemoActivity.this, binding.llMain, title, desc, duration,1);
        snackbar.show();
        if (is_error_img == 1) {
            ImageView imgError = snackbar.getView().findViewById(R.id.img_error);
            imgError.clearAnimation();
            imgError.setVisibility(View.VISIBLE);

            imgError.setAnimation(OptAnimationLoader.loadAnimation(AllDayBranchWiseMemoActivity.this, R.anim.error_x_in));
        }
    }

    private void showBottomSheetDialog() {
        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
            bottomSheetDialog.dismiss();
        }

        internetConnectionBinding = LayoutBottomSheetInternetConnectionBinding.inflate(getLayoutInflater());
        // final View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, false);
        final View view = internetConnectionBinding.getRoot();
        internetConnectionBinding.btnCheckNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                } catch (Exception ignored) {
                }

                internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                internetConnectionBinding.loading.setVisibility(View.VISIBLE);

                Handler mHand = new Handler(Looper.getMainLooper());
                mHand.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (!cd.isConnectingToInternet()) {
                            internetConnectionBinding.btnCheckNet.setVisibility(View.VISIBLE);
                            internetConnectionBinding.loading.setVisibility(View.GONE);
                            CustomToast.showToastMessage(AllDayBranchWiseMemoActivity.this, "No Internet connection", 0);
                        } else {
                            internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                            internetConnectionBinding.loading.setVisibility(View.VISIBLE);
                        }
                    }
                }, 2000);


            }
        });

        //View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, null);
        bottomSheetDialog = new BottomSheetDialog(AllDayBranchWiseMemoActivity.this);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);


    }

    private Get_Report_AllDayMemo_BranchWise_Request apiRequest() {
        journeyStartDate = "07/11/2020";
        companyId = 1;


        return new Get_Report_AllDayMemo_BranchWise_Request(
                getPref.getCM_CompanyID(),
                journeyStartDate,
                getPref.getBM_BranchID(),
                companyId,
                getPref.getBUM_BranchUserID(),
                1
        );
    }

    private void setupRecyclerView() {
        allDayMemoBranchWiseAdapter = new AllDayMemoBranchWiseAdapter(AllDayBranchWiseMemoActivity.this, Hashmap_allDayMemo);
        binding.rvAllDayMemo.setAdapter(allDayMemoBranchWiseAdapter);
    }

    private void setInitialData() {

        linkedHashMap = new LinkedHashMap<>();
        list_allDayMemo = new ArrayList<>();

        List<String> key_list = new ArrayList<>(Hashmap_allDayMemo.keySet());
        linkedHashMap = Hashmap_allDayMemo.get(key_list.get(0));

        List<String> key_list_V2 = new ArrayList<>(linkedHashMap.keySet());
        list_allDayMemo = linkedHashMap.get(key_list_V2.get(0));

        Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo data = list_allDayMemo.get(0);

        binding.txtCompanyName.setText(data.getCompanyName());
        binding.txtCompanyAddress.setText(data.getCompanyAddress());
        //binding.txtComissionInfo.setText(data.get);

        binding.txtDate.setText("Date: " + data.getJDate());
        binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

        binding.txtTotalCollection.setText("Total Collection \n" + String.format("%.2f", data.getTotalCollection()));
        binding.txtCommission.setText("Total Commission \n" + String.format("%.2f", data.getCommissionAmount()));
        binding.txtTotalSeats.setText("Total Seats \n" + data.getNetSeat());
        binding.txtNetCollection.setText("Net Collection \n" + String.format("%.2f", data.getNetCollection()));

    }


    private void Get_Report_AllDayMemo_BranchWiseApiCall() {
        showProgressDialog("Loading....");

        journeyStartDate = "09/10/2020";
        companyId = 1;

        Get_Report_AllDayMemo_BranchWise_Request request = new Get_Report_AllDayMemo_BranchWise_Request(
                getPref.getCM_CompanyID(),
                journeyStartDate,
                getPref.getBM_BranchID(),
                companyId,
                getPref.getBUM_BranchUserID(),
                1
        );

        Call<Get_Report_AllDayMemo_BranchWise_Response> call = apiService.Get_Report_AllDayMemo_BranchWise(request);
        call.enqueue(new Callback<Get_Report_AllDayMemo_BranchWise_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Report_AllDayMemo_BranchWise_Response> call, @NotNull Response<Get_Report_AllDayMemo_BranchWise_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    list_allDayMemo = new ArrayList<>();


                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getAllDayMemo() != null && response.body().getData().getAllDayMemo().size() > 0) {

                            list_allDayMemo = response.body().getData().getAllDayMemo();
                            Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo data = list_allDayMemo.get(0);

                            binding.txtCompanyName.setText(data.getCompanyName());
                            binding.txtCompanyAddress.setText(data.getCompanyAddress());
                            //binding.txtComissionInfo.setText(data.get);

                            binding.txtDate.setText("Date: " + data.getJDate());
                            binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                            binding.txtTotalCollection.setText("Total Collection \n" + String.format("%.2f", data.getTotalCollection()));
                            binding.txtCommission.setText("Total Commission \n" + String.format("%.2f", data.getCommissionAmount()));
                            binding.txtTotalSeats.setText("Total Seats \n" + data.getNetSeat());
                            binding.txtNetCollection.setText("Net Collection \n" + String.format("%.2f", data.getNetCollection()));

                            Hashmap_allDayMemo = new LinkedHashMap<>();

                            for (Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo pojo : list_allDayMemo) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getRouteTime())) {
                                    linkedHashMap = new LinkedHashMap<>();
                                }

                                if (!Hashmap_allDayMemo.containsKey(pojo.getRouteName())) {
                                    List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getRouteName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                                }

                                Hashmap_allDayMemo.put(pojo.getRouteTime(), linkedHashMap);

                            }

                            allDayMemoBranchWiseAdapter = new AllDayMemoBranchWiseAdapter(AllDayBranchWiseMemoActivity.this, Hashmap_allDayMemo);
                            binding.rvAllDayMemo.setAdapter(allDayMemoBranchWiseAdapter);

                        }
                    } else {
                        Toast.makeText(AllDayBranchWiseMemoActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Report_AllDayMemo_BranchWise_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });


    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(AllDayBranchWiseMemoActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_AllDayMemo_BranchWise")) {
                                        Get_Report_AllDayMemo_BranchWiseApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(AllDayBranchWiseMemoActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


}
