package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.PassengerBookingReport_Request;
import com.infinityinfoway.itsexe.api.response.PassengerBookingReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityPassengerBookingReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.PassengerBookingReportAdapter;
import com.infinityinfoway.itsexe.exe_adapter.PassengerBookingReport_SubRouteAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassengerBookingReportActivity extends AppCompatActivity {


    private LinkedHashMap<String, List<PassengerBookingReport_Response.Datum>> Hashmap_allDayMemo;
    private List<PassengerBookingReport_Response.Datum> list_PassengerBookingReport;

    private LinkedHashMap<String, List<PassengerBookingReport_Response.Datum>> hashMapSubRoute;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "PassengerBookingReoprt";
    private String fromDate, journeyStartDate, journeyStartTime, journeyCityTime;
    private int companyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId, isSameDay;
    private Bundle getRouteData;
    private ActivityPassengerBookingReportBinding binding;
    private String resReportTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPassengerBookingReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();
            companyId = getRouteData.getInt("Company_Id");
            fromDate = getRouteData.getString("FromDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getInt("IsSameDay");
            resReportTitle = getRouteData.getString("ReportTitle");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        binding.layoutBase.txtTitle.setText(resReportTitle.substring(2));
        binding.layoutBase.txtTitle.setText(resReportTitle);


        cd = new ConnectionDetector(PassengerBookingReportActivity.this);
        getPref = new ITSExeSharedPref(PassengerBookingReportActivity.this);

        PassengerBookingReoprtApiCall();

    }

    private void PassengerBookingReoprtApiCall() {
        if (cd != null && cd.isConnectingToInternet()) {
            showProgressDialog("Loading....");


            journeyStartDate = "2019-12-17";
            fromCity = 703;
            toCity = 2157;
            routeId = 21187;
            routeTimeId = 54505;
            journeyStartTime = "12:00 AM";
            journeyCityTime = "12:00 AM";
            arrangementId = 510;
            cityId = 70;
            companyId = 20;
            isSameDay = 0;

            final PassengerBookingReport_Request request = new PassengerBookingReport_Request(
                    journeyStartDate,
                    routeId,
                    routeTimeId,
                    20,
                    684,
                    fromCity,
                    toCity,
                    companyId,
                    isSameDay
            );

            Call<PassengerBookingReport_Response> call = apiService.PassengerBookingReport(request);
            call.enqueue(new Callback<PassengerBookingReport_Response>() {
                @Override
                public void onResponse(@NotNull Call<PassengerBookingReport_Response> call, @NotNull Response<PassengerBookingReport_Response> response) {
                    disMissDialog();
                    if (response != null && response.isSuccessful() && response.body() != null) {
                        list_PassengerBookingReport = new ArrayList<>();
                        Hashmap_allDayMemo = new LinkedHashMap<>();
                        hashMapSubRoute = new LinkedHashMap<>();

                        if (response.body().getStatus() == 1) {
                            if (response.body().getData() != null && response.body().getData().size() > 0) {

                                list_PassengerBookingReport = response.body().getData();

                                for (PassengerBookingReport_Response.Datum pojo : list_PassengerBookingReport) {

                                    if (!Hashmap_allDayMemo.containsKey(pojo.getPickUpName())) {
                                        List<PassengerBookingReport_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        Hashmap_allDayMemo.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<PassengerBookingReport_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getPickUpName());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        Hashmap_allDayMemo.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                    }

                                    if (!hashMapSubRoute.containsKey(pojo.getFromCity() + "-" + pojo.getFromToCity())) {
                                        List<PassengerBookingReport_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        hashMapSubRoute.put(pojo.getFromCity() + "-" + pojo.getFromToCity(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<PassengerBookingReport_Response.Datum> pickDropPassengerDetailPojoList = hashMapSubRoute.get(pojo.getFromCity() + "-" + pojo.getFromToCity());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        hashMapSubRoute.put(pojo.getFromCity() + "-" + pojo.getFromToCity(), pickDropPassengerDetailPojoList);
                                    }

                                }

                                PassengerBookingReport_Response.Datum data = list_PassengerBookingReport.get(0);

                                binding.txtCoachNo.setText("Coach No. : " + list_PassengerBookingReport.get(0).getBusNo());
                                binding.txtPickupmanName.setText("PickUp Man: " + list_PassengerBookingReport.get(0).getPickupManName());
                                binding.txtPhone.setText("Phone: " + list_PassengerBookingReport.get(0).getPMPhoneNo());

                                if (!TextUtils.isEmpty(data.getDMDriverName())) {
                                    binding.txtDriverName.setText("Driver Name: " + data.getDMDriverName() + " " + data.getDMPhoneNo());
                                    if (!TextUtils.isEmpty(data.getDMDriver1())) {
                                        binding.txtDriverName.setText(binding.txtDriverName.getText().toString() + " " + data.getDMDriverName1() + data.getDMDriver1() + " " + data.getDMPhoneNo1());
                                    }
                                    if (!TextUtils.isEmpty(data.getDMDriver2())) {
                                        binding.txtDriverName.setText(binding.txtDriverName.getText().toString() + " " + data.getDMDriverName1() + data.getDMDriver2() + " " + data.getDMPhoneNo2());
                                    }
                                }

                                binding.txtConductorName.setText("Conductor Name: " + data.getDMConductor1() + " " + data.getDMConductorPhone());

                                binding.txtRoute.setText(list_PassengerBookingReport.get(0).getRouteName());
                                binding.txtCompanyAddress.setText(list_PassengerBookingReport.get(0).getCMCompanyAddress());
                                binding.txtCompanyName.setText(list_PassengerBookingReport.get(0).getCompanyName());
                                binding.txtTotalSeat.setText("Total Seats: " + list_PassengerBookingReport.get(0).getTotalBookedSeat());
                                binding.txtEmptySeat.setText("Empty Seats: " + list_PassengerBookingReport.get(0).getTotalEmptySeat() + "  " + list_PassengerBookingReport.get(0).getEmptySeat());
                                binding.txtEmptySleeper.setText("Empty Slpr: " + list_PassengerBookingReport.get(0).getTotalEmptySleeper() + "  " + list_PassengerBookingReport.get(0).getEmptySleeper());
                                binding.txtAgentQuota.setText(list_PassengerBookingReport.get(0).getSubRouteName());


                                binding.rvPassengerBooking.setAdapter(new PassengerBookingReportAdapter(PassengerBookingReportActivity.this, Hashmap_allDayMemo));


                                binding.rvSubRoute.setAdapter(new PassengerBookingReport_SubRouteAdapter(PassengerBookingReportActivity.this, hashMapSubRoute));

                            }


                        } else {
                            Toast.makeText(PassengerBookingReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<PassengerBookingReport_Response> call, @NotNull Throwable t) {
                    disMissDialog();
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
                }
            });

        } else {
            showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
        }
    }


    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(PassengerBookingReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("PassengerBookingReoprt")) {
                                        PassengerBookingReoprtApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(PassengerBookingReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


}
