package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.RouteTimeBookingDetails_Request;
import com.infinityinfoway.itsexe.api.response.RouteTimeBookingDetails_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityRouteTimeBookingMemoReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.RouteTimeBookingMemoReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteTimeBookingMemoReportActivity extends AppCompatActivity {

    RouteTimeBookingMemoReportAdapter routeTimeBookingMemoReportAdapter;
    LinkedHashMap<String, List<RouteTimeBookingDetails_Response.Datum>> Hashmap_allDayMemo;
    List<RouteTimeBookingDetails_Response.Datum> list_routeTime_bookingmemo;


    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_RouteTime_BookingMemo";
    private String resReportTitle, journeyStartDate;
    private int cm_CompanyId, routeId, routeTimeId;
    private Bundle getRouteData;

    private ActivityRouteTimeBookingMemoReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityRouteTimeBookingMemoReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();
            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");

            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
        }

        cd = new ConnectionDetector(RouteTimeBookingMemoReportActivity.this);
        getPref = new ITSExeSharedPref(RouteTimeBookingMemoReportActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);


//          binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
//         binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_RouteTime_BookingMemoApiCall();


    }

    private void Get_Report_RouteTime_BookingMemoApiCall() {
        showProgressDialog("Loading....");

        RouteTimeBookingDetails_Request request = new RouteTimeBookingDetails_Request(
                cm_CompanyId,
                journeyStartDate,
                routeId,
                routeTimeId,
                getPref.getBUM_BranchUserID()
        );

        Call<RouteTimeBookingDetails_Response> call = apiService.RouteTimeBookingDetails(request);
        call.enqueue(new Callback<RouteTimeBookingDetails_Response>() {
            @Override
            public void onResponse(@NotNull Call<RouteTimeBookingDetails_Response> call, @NotNull Response<RouteTimeBookingDetails_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_routeTime_bookingmemo = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_routeTime_bookingmemo = response.body().getData();
                            binding.txtCompanyName.setText(list_routeTime_bookingmemo.get(0).getCompanyName());

                            binding.txtCompanyName.setPaintFlags(binding.txtCompanyName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                            binding.txtCompanyAddress.setText(list_routeTime_bookingmemo.get(0).getCompanyAddress());


                            binding.txtVehicleNo.setText(list_routeTime_bookingmemo.get(0).getBMBusNo());
                            binding.txtRouteDate.setText(list_routeTime_bookingmemo.get(0).getJDate());
                            binding.txtRouteTime.setText(list_routeTime_bookingmemo.get(0).getRouteTime());
                            binding.txtFrom.setText(list_routeTime_bookingmemo.get(0).getFromCityName());
                            binding.txtToCity.setText(list_routeTime_bookingmemo.get(0).getToCityName());
                            binding.txtPrintDate.setText(new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));

                            double totalAmount = 0.0;
                            int totalSeat = 0;

                            Hashmap_allDayMemo = new LinkedHashMap<>();

                            for (RouteTimeBookingDetails_Response.Datum pojo : list_routeTime_bookingmemo) {


                                if (!TextUtils.isEmpty(pojo.getTotalAmount())) {
                                    totalAmount += Double.parseDouble(pojo.getTotalAmount());
                                }
                                if (!TextUtils.isEmpty(pojo.getTotalSeat())) {
                                    totalSeat += Integer.parseInt(pojo.getTotalSeat());
                                }

                                if (!Hashmap_allDayMemo.containsKey(pojo.getGroupByType())) {
                                    List<RouteTimeBookingDetails_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getGroupByType(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<RouteTimeBookingDetails_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getGroupByType());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getGroupByType(), pickDropPassengerDetailPojoList);
                                }
                            }
                            routeTimeBookingMemoReportAdapter = new RouteTimeBookingMemoReportAdapter(RouteTimeBookingMemoReportActivity.this, Hashmap_allDayMemo);
                            binding.recyclerRouteTimeBookingMemo.setAdapter(routeTimeBookingMemoReportAdapter);

                            binding.txtTotalSeat.setText(String.valueOf(totalSeat));
                            binding.txtTotalAmount.setText(String.format("%.2f", totalAmount));
                        }
                    } else {
                        Toast.makeText(RouteTimeBookingMemoReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();

                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<RouteTimeBookingDetails_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(RouteTimeBookingMemoReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_RouteTime_BookingMemo")) {
                                        Get_Report_RouteTime_BookingMemoApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(RouteTimeBookingMemoReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


}
