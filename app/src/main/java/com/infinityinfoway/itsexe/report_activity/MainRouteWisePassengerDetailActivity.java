package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.MainRouteWisePassengerDetails_Request;
import com.infinityinfoway.itsexe.api.response.MainRouteWisePassengerDetails_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityMainRouteWisePassengerDetailBinding;
import com.infinityinfoway.itsexe.exe_adapter.MainRouteWisePassengerDetailAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainRouteWisePassengerDetailActivity extends AppCompatActivity {


    MainRouteWisePassengerDetailAdapter mainRouteWisePassengerDetailAdapter;

    private ITSExeSharedPref getPref;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private List<MainRouteWisePassengerDetails_Response.Datum> list_passenger_details;

    private String resReportTitle, journeyStartDate, isSameDay;
    private int cm_CompanyId,  routeId, routeTimeId, fromCity, toCity;
    private Bundle getRouteData;
    private final String api_method = "Get_Report_MainRouteWisePassengerDetails";

    private ActivityMainRouteWisePassengerDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainRouteWisePassengerDetailBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            isSameDay = getRouteData.getString("IsSameDay");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(MainRouteWisePassengerDetailActivity.this);
        getPref = new ITSExeSharedPref(MainRouteWisePassengerDetailActivity.this);

        Get_Report_MainRouteWisePassengerDetailsApiCall();

    }

    private void Get_Report_MainRouteWisePassengerDetailsApiCall() {
        showProgressDialog("Loading....");

        MainRouteWisePassengerDetails_Request request = new MainRouteWisePassengerDetails_Request(
                cm_CompanyId,
                routeId,
                routeTimeId,
                journeyStartDate,
                fromCity,
                toCity,
                getPref.getBUM_BranchUserID(),
                isSameDay
        );

        Call<MainRouteWisePassengerDetails_Response> call = apiService.MainRouteWisePassengerDetails(request);
        call.enqueue(new Callback<MainRouteWisePassengerDetails_Response>() {
            @Override
            public void onResponse(@NotNull Call<MainRouteWisePassengerDetails_Response> call, @NotNull Response<MainRouteWisePassengerDetails_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_passenger_details = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            list_passenger_details = response.body().getData();

                            binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                            binding.txtCompanyName.setText(list_passenger_details.get(0).getCMCompanyName());
                            binding.txtCompanyAddress.setText(list_passenger_details.get(0).getCMAddress());
                            binding.txtRoute.setText(list_passenger_details.get(0).getRouteName());
                            binding.txtCoachNo.setText("Coach No: " + list_passenger_details.get(0).getBusNo());
                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));


                            mainRouteWisePassengerDetailAdapter = new MainRouteWisePassengerDetailAdapter(MainRouteWisePassengerDetailActivity.this, list_passenger_details);
                            binding.rvMainRoute.setAdapter(mainRouteWisePassengerDetailAdapter);
                        }
                    } else {
                        Toast.makeText(MainRouteWisePassengerDetailActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<MainRouteWisePassengerDetails_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });


    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(MainRouteWisePassengerDetailActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_MainRouteWisePassengerDetails")) {
                                        Get_Report_MainRouteWisePassengerDetailsApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(MainRouteWisePassengerDetailActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    /*public class LazyDataConnection_ExeApi extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData_Exe.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {
            if (xmlResponse.equals("")) {
                if (method.equals("Get_BusConnectExe_Report_SeatAllocation")) {
                    try {
                        progDialog.dismiss();
                    } catch (Exception ex) {
                    }
                }
                Intent i = new Intent(MainRouteWisePassengerDetailActivity.this, InternetErrorActivity.class);
                i.putExtra("caller", "ExeBookingActivity");
                startActivity(i);
                return;
            }
            if (method.equals("Get_BusConnectExe_Report_MainRouteWisePassengerDetails")) {

                Get_BusConnectExe_Report_SeatAllocation(xmlResponse, "MainRouteWisePassengerDetails");
                try {
                    progDialog.dismiss();
                } catch (Exception ex) {
                }


                if (list_passenger_details != null && list_passenger_details.size() > 0) {


                    txt_company_name.setText(list_passenger_details.get(0).getCM_CompanyName());
                    txt_company_address.setText(list_passenger_details.get(0).getCM_Address());
                    txt_route.setText(list_passenger_details.get(0).getRouteName());
                    txt_coach_no.setText(list_passenger_details.get(0).getBusNo());
                    txt_print_date.setText("Print Date: " + new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));


                    mainRouteWisePassengerDetailAdapter = new MainRouteWisePassengerDetailAdapter(MainRouteWisePassengerDetailActivity.this, list_passenger_details);
                    recycler_passenger_details.setAdapter(mainRouteWisePassengerDetailAdapter);
                } else {
                    Toast.makeText(MainRouteWisePassengerDetailActivity.this, "No Data Found'", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }*/

    /*public void Get_BusConnectExe_Report_SeatAllocation(String str_RespXML, String str_TagName) {
        Document doc = getData_Exe.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        list_passenger_details = new ArrayList<>();
        for (int i = 0; i <= nodes.getLength(); i++) {
            Node e1 = nodes.item(i);
            Element el = (Element) e1;

            if (el != null) {
                MainRouteWisePassengerDetailReportBean allDayMemoReportBean = new MainRouteWisePassengerDetailReportBean();

                try {
                    allDayMemoReportBean.setJM_PassengerName(el.getElementsByTagName("JM_PassengerName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_Phone1(el.getElementsByTagName("JM_Phone1").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_SeatList(el.getElementsByTagName("JM_SeatList").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_CompanyName(el.getElementsByTagName("CM_CompanyName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_Address(el.getElementsByTagName("CM_Address").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRouteName(el.getElementsByTagName("RouteName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setBusNo(el.getElementsByTagName("BusNo").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setPM_PickupMan(el.getElementsByTagName("PM_PickupMan").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setPickupManName(el.getElementsByTagName("PickupManName").item(0).getTextContent());
                } catch (Exception e) {
                }


                list_passenger_details.add(allDayMemoReportBean);
            }
        }
    }*/
}
