package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.ReservationDetailReportWithAmount_Request;
import com.infinityinfoway.itsexe.api.response.ReservationDetailReportWithAmount_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityReservationDetailReportWithAmountBinding;
import com.infinityinfoway.itsexe.exe_adapter.ReservationDetailReportWithAmountAdapter;
import com.infinityinfoway.itsexe.exe_adapter.ReservationDetailReportWithAmountAdapter_DroppingPoints;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservationDetailReportWithAmountActivity extends AppCompatActivity {

    private List<ReservationDetailReportWithAmount_Response.Datum> list;
    private LinkedHashMap<String, List<ReservationDetailReportWithAmount_Response.Datum>> linkedHashMap;

    private LinkedHashMap<String,LinkedHashMap<String,List<ReservationDetailReportWithAmount_Response.Datum>>> linkedHashMapDroppOffs;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "ReservationDetailReportWithAmount";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private Bundle getRouteData;
    private String resReportTitle = "";

    private ActivityReservationDetailReportWithAmountBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityReservationDetailReportWithAmountBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(ReservationDetailReportWithAmountActivity.this);
        getPref = new ITSExeSharedPref(ReservationDetailReportWithAmountActivity.this);

        ReservationDetailReportWithAmountApiCall();

    }

    private void ReservationDetailReportWithAmountApiCall() {

        showProgressDialog("Loading....");

        ReservationDetailReportWithAmount_Request request = new ReservationDetailReportWithAmount_Request(
                cm_CompanyId,
                routeId,
                routeTimeId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                journeyStartTime,
                journeyCityTime,
                0,
                arrangementId,
                cityId,
                isSameDay
        );

        Call<ReservationDetailReportWithAmount_Response> call = apiService.ReservationDetailReportWithAmount(request);

        call.enqueue(new Callback<ReservationDetailReportWithAmount_Response>() {
            @Override
            public void onResponse(@NotNull Call<ReservationDetailReportWithAmount_Response> call, @NotNull Response<ReservationDetailReportWithAmount_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list = response.body().getData();
                            linkedHashMap = new LinkedHashMap<>();
                            linkedHashMapDroppOffs=new LinkedHashMap<>();

                            LinkedHashMap<String,List<ReservationDetailReportWithAmount_Response.Datum>> subHashMap=new LinkedHashMap<>();

                            for (ReservationDetailReportWithAmount_Response.Datum data : list) {
                                if (!linkedHashMap.containsKey(data.getPMPickupName())) {
                                    List<ReservationDetailReportWithAmount_Response.Datum> list = new ArrayList<>();
                                    list.add(data);
                                    linkedHashMap.put(data.getPMPickupName(), list);
                                } else {
                                    List<ReservationDetailReportWithAmount_Response.Datum> list = linkedHashMap.get(data.getPMPickupName());
                                    list.add(data);
                                    linkedHashMap.put(data.getPMPickupName(), list);
                                }

                                if(!linkedHashMapDroppOffs.containsKey(data.getToCity())){
                                    subHashMap=new LinkedHashMap<>();
                                }


                                if (!subHashMap.containsKey(data.getPMDropName())) {
                                    List<ReservationDetailReportWithAmount_Response.Datum> list = new ArrayList<>();
                                    list.add(data);
                                    subHashMap.put(data.getPMDropName(), list);
                                } else {
                                    List<ReservationDetailReportWithAmount_Response.Datum> list = subHashMap.get(data.getPMDropName());
                                    list.add(data);
                                    subHashMap.put(data.getPMDropName(), list);
                                }

                                linkedHashMapDroppOffs.put(data.getToCity(),subHashMap);

                            }

                            binding.rvDrop.setAdapter(new ReservationDetailReportWithAmountAdapter_DroppingPoints(ReservationDetailReportWithAmountActivity.this,linkedHashMapDroppOffs));

                            binding.rvData.setAdapter(new ReservationDetailReportWithAmountAdapter(ReservationDetailReportWithAmountActivity.this, linkedHashMap));


                            // Summary
                            ReservationDetailReportWithAmount_Response.Datum data = list.get(0);

                            binding.txtCompanyName.setText(data.getCMCompanyName());
                            binding.txtReportName.setText(data.getReprotTitle());

                            binding.txtDate.setText(data.getJMJourneyStartDate());
                            binding.txtCoachNo.setText(data.getBusNo());
                            binding.txtSeats.setText(data.getTotalSeats());

                            binding.txtServiceNo.setText(data.getServiceType());
                            binding.txtRoute.setText(data.getRouteName());
                            binding.txtReserved.setText(data.getBookedSeats());

                            binding.txtOrigin.setText(data.getRouteOrigin());
                            binding.txtDriver1.setText(data.getDriver1());
                            binding.txtUnreserved.setText(data.getEmptySeats());

                            binding.txtDest.setText(data.getRouteDestination());
                            binding.txtDriver2.setText(data.getDriver2());
                            binding.txtBlocked.setText(data.getBlockedSeats());

                            binding.txtDepartTime.setText(data.getRouteDepartureTime());
                            binding.txtChkInspector.setText("");
                            binding.txtJtime.setText(data.getFinalJourneyTime());

                            binding.txtEmptySeat.setText(data.getEmptySeat());
                            binding.txtEmptySleepers.setText(data.getEmptySleeper());
                            binding.txtAgentQuota.setText(data.getAgentQuota());

                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                        }
                    } else {
                        Toast.makeText(ReservationDetailReportWithAmountActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<ReservationDetailReportWithAmount_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ReservationDetailReportWithAmountActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("ReservationDetailReportWithAmount")) {
                                        ReservationDetailReportWithAmountApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(ReservationDetailReportWithAmountActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}
