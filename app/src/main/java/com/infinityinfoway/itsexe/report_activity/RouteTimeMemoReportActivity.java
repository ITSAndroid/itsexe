package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.RouteTimeMemoReport_Request;
import com.infinityinfoway.itsexe.api.response.RouteTimeMemoReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityRouteTimeMemoReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.RouteTimeMemoReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteTimeMemoReportActivity extends AppCompatActivity {

    private RouteTimeMemoReportAdapter routeTimeMemoReportAdapter;

    private ActivityRouteTimeMemoReportBinding binding;
    private String journeyStartDate, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, routeId, routeTimeId, fromCity, toCity;
    private Bundle getRouteData;
    private String resReportTitle = "";
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private androidx.appcompat.app.AlertDialog alert;
    private final String api_method = "RouteTimeMemoReport";

    private List<RouteTimeMemoReport_Response.Datum> listRouteTimeMemoReport;
    private LinkedHashMap<String, List<RouteTimeMemoReport_Response.Datum>> linkedHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding=ActivityRouteTimeMemoReportBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //  binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        // binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(RouteTimeMemoReportActivity.this);
        getPref = new ITSExeSharedPref(RouteTimeMemoReportActivity.this);



        RouteTimeMemoReportApiCall();

    }

    private void RouteTimeMemoReportApiCall(){
        showProgressDialog("Loading....");

        RouteTimeMemoReport_Request request = new RouteTimeMemoReport_Request(
                journeyStartDate,
                routeId,
                routeTimeId,
                cm_CompanyId,
                bookedByCompanyId,
                getPref.getBUM_BranchUserID(),
                fromCity,
                toCity,
                isSameDay
        );

        Call<RouteTimeMemoReport_Response> call=apiService.RouteTimeMemoReport(request);
        call.enqueue(new Callback<RouteTimeMemoReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<RouteTimeMemoReport_Response> call, @NotNull Response<RouteTimeMemoReport_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    listRouteTimeMemoReport = new ArrayList<>();
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            listRouteTimeMemoReport = response.body().getData();

                            linkedHashMap = new LinkedHashMap<>();

                            for (RouteTimeMemoReport_Response.Datum data : listRouteTimeMemoReport) {

                                if (!linkedHashMap.containsKey(data.getGroupByName())) {
                                    List<RouteTimeMemoReport_Response.Datum> list = new ArrayList<>();
                                    list.add(data);
                                    linkedHashMap.put(data.getGroupByName(), list);
                                } else {
                                    List<RouteTimeMemoReport_Response.Datum> list = linkedHashMap.get(data.getGroupByName());
                                    list.add(data);
                                    linkedHashMap.put(data.getGroupByName(), list);
                                }
                            }

                            RouteTimeMemoReport_Response.Datum data = listRouteTimeMemoReport.get(0);

                            binding.txtCompanyName.setText(data.getCMCompanyName());
                            binding.txtRoute.setText(data.getRMRouteNameDisplay());
                            binding.txtJdate.setText(data.getJourneyDate());
                            binding.txtBusNo.setText(data.getBMBUsNo());
                            binding.txtTime.setText(data.getRTTimeHed());
                            binding.txtReportDate.setText("Report Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                            routeTimeMemoReportAdapter=new RouteTimeMemoReportAdapter(RouteTimeMemoReportActivity.this,linkedHashMap);
                            binding.rvData.setAdapter(routeTimeMemoReportAdapter);

                            double NetCollection=0.0;
                            if(!TextUtils.isEmpty(data.getTotalAmountSELF())){
                                NetCollection=Double.parseDouble(data.getTotalAmountSELF());
                            }
                            if(!TextUtils.isEmpty(data.getTotalAmountAgent())){
                                NetCollection+=Double.parseDouble(data.getTotalAmountAgent());
                            }
                            binding.txtNetCollection.setText("Net Collection \n"+String.format("%.2f",NetCollection));



                            double NetAmount=0.0;
                            if(!TextUtils.isEmpty(data.getNetCollectionSELF())){
                                NetAmount=Double.parseDouble(data.getNetCollectionSELF());
                            }
                            if(!TextUtils.isEmpty(data.getNetCollectionAgent())){
                                NetAmount+=Double.parseDouble(data.getNetCollectionAgent());
                            }

                            binding.txtNetAmt.setText("Net \n"+String.format("%.2f",NetAmount));

                            /*double TotalBranchCharge=0.0;
                            if(!TextUtils.isEmpty(data.getBranchChargesSELF())){
                                TotalBranchCharge=Double.parseDouble(data.getBranchChargesSELF());
                            }
                            if(!TextUtils.isEmpty(data.getBranchChargesAgent())){
                                TotalBranchCharge+=Double.parseDouble(data.getBranchChargesAgent());
                            }*/
                            binding.txtBranchCharge.setText("Branch Charges \n"+String.format("%.2f",(NetCollection-NetAmount)));


                        }
                    } else {
                        Toast.makeText(RouteTimeMemoReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<RouteTimeMemoReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(RouteTimeMemoReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(RouteTimeMemoReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("RouteTimeMemoReport")) {
                                        RouteTimeMemoReportApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }



}
