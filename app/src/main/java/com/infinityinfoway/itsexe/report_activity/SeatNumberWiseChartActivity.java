package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatNumberWiseChart_Request;
import com.infinityinfoway.itsexe.api.response.SeatNumberWiseChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySeatNumberWiseChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.SeatNumberWiseChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatNumberWiseChartActivity extends AppCompatActivity {

    private SeatNumberWiseChartAdapter seatNumberWiseChartAdapter;
    private List<SeatNumberWiseChart_Response.Detail> list_SeatNumberDetails;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "SeatNumberWiseChart";
    private String fromDate, journeyStartDate, journeyStartTime, journeyCityTime,isSameDay;
    private int cm_CompanyId,bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId, checkListFor;
    private Bundle getRouteData;
    private String resReportTitle = "";
    private ActivitySeatNumberWiseChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySeatNumberWiseChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId=getRouteData.getInt("Company_Id");
            fromDate= getRouteData.getString("FromDate");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            checkListFor= getRouteData.getInt("CheckListFor");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(SeatNumberWiseChartActivity.this);
        getPref = new ITSExeSharedPref(SeatNumberWiseChartActivity.this);


        Get_Report_SeatNumberWiseChartApiCall();

    }

    private void Get_Report_SeatNumberWiseChartApiCall() {

        showProgressDialog("Loading....");


        SeatNumberWiseChart_Request request = new SeatNumberWiseChart_Request(
                cm_CompanyId,
                fromDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartDate,
                journeyStartTime,
                journeyCityTime,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay,
                checkListFor,
                getPref.getBM_BranchID(),
                fromCity,
                toCity,
                getPref.getBUM_BranchUserID()
        );


        Call<SeatNumberWiseChart_Response> call = apiService.SeatNumberWiseChart(request);
        call.enqueue(new Callback<SeatNumberWiseChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatNumberWiseChart_Response> call, @NotNull Response<SeatNumberWiseChart_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_SeatNumberDetails = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getDetails() != null && response.body().getData().getDetails().size() > 0) {
                            list_SeatNumberDetails = response.body().getData().getDetails();
                            seatNumberWiseChartAdapter = new SeatNumberWiseChartAdapter(SeatNumberWiseChartActivity.this, list_SeatNumberDetails);
                            binding.rvSeatChart.setAdapter(seatNumberWiseChartAdapter);

                            SeatNumberWiseChart_Response.Detail seatData = list_SeatNumberDetails.get(0);
                            binding.txtBusNo.setText(seatData.getBusNo());
                            binding.txtRouteName.setText("Route:\t\t" + seatData.getRouteName().trim());
                            binding.txtTime.setText("Time: \t\t" + seatData.getJourneyStartTime().trim());
                            binding.txtDate.setText("Date: \t\t\t" + seatData.getJourneyDate().trim());
                            binding.txtPrintTime.setText("Print Time:\t\t" + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By:  \t\t" + getPref.getBUM_UserName() + "[" + getPref.getBM_BranchCode() + "]");
                            binding.txtTotalAmt.setText(getTotalAmount(list_SeatNumberDetails));

                            binding.txtTotEmptySeats.setText(String.valueOf(seatData.getTotalEmptySeat()));
                            binding.txtEmptySeats.setText(seatData.getEmptySeat());

                            binding.txtTotEmptySleepers.setText(String.valueOf(seatData.getTotalEmptySleeper()));
                            binding.txtEmptySleepers.setText(seatData.getEmptySleeper());

                            binding.txtRemarks.setText(seatData.getRemarkDetail());
                            binding.txtSeatAlloation.setText(seatData.getFromCitySeat().replaceAll("\r", "\n"));

                        }
                    } else {
                        Toast.makeText(SeatNumberWiseChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatNumberWiseChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SeatNumberWiseChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_SeatNumberWiseChart")) {
                                        Get_Report_SeatNumberWiseChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SeatNumberWiseChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


    private String getTotalAmount(List<SeatNumberWiseChart_Response.Detail> SeatDataList) {
        double totalAmount = 0.0;
        for (SeatNumberWiseChart_Response.Detail data : SeatDataList) {
            if (!TextUtils.isEmpty(data.getAmount())) {
                totalAmount += Double.parseDouble(data.getAmount());
            }
        }
        return String.format("%.2f", totalAmount);
    }

}
