package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.PickupWiseInquiry_Request;
import com.infinityinfoway.itsexe.api.response.PickupWiseInquiry_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityPickupWiseInquiryReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.PickupWiseInquiryReportAdapter;
import com.infinityinfoway.itsexe.exe_adapter.PickupWiseInquirySubRouteAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickupWiseInquiryReportActivity extends AppCompatActivity {

    private LinkedHashMap<String, List<PickupWiseInquiry_Response.MainDatum>> Hashmap_allDayMemo;
    private List<PickupWiseInquiry_Response.MainDatum> list_pickup_inquiry;
    private List<PickupWiseInquiry_Response.SubRouteWiseDatum> table1List;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_PickupWiseInquiry";
    private String journeyStartDate,resReportTitle, journeyStartTime, journeyCityTime,isSameDay;
    private int cm_CompanyId, cityId,bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId;
    private Bundle getRouteData;

    private ActivityPickupWiseInquiryReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPickupWiseInquiryReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(PickupWiseInquiryReportActivity.this);
        getPref = new ITSExeSharedPref(PickupWiseInquiryReportActivity.this);


        Get_Report_PickupWiseInquiryApiCall();

    }

    private void Get_Report_PickupWiseInquiryApiCall() {

        showProgressDialog("Loading....");

        PickupWiseInquiry_Request request = new PickupWiseInquiry_Request(
                journeyStartDate,
                cm_CompanyId,
                routeId,
                routeTimeId,
                fromCity,
                toCity,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );

        Call<PickupWiseInquiry_Response> call = apiService.PickupWiseInquiry(request);
        call.enqueue(new Callback<PickupWiseInquiry_Response>() {
            @Override
            public void onResponse(@NotNull Call<PickupWiseInquiry_Response> call, @NotNull Response<PickupWiseInquiry_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_pickup_inquiry = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();
                    table1List = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getMainData() != null && response.body().getData().getMainData().size() > 0) {
                            list_pickup_inquiry = response.body().getData().getMainData();

                            PickupWiseInquiry_Response.MainDatum data = list_pickup_inquiry.get(0);

                            binding.txtPrintBy.setText("Print By : " + data.getPrintBy());
                            binding.txtPrintDate.setText("Print Date : " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtRouteName.setText("Route : " + data.getRouteName());
                            binding.txtCoachNo.setText(data.getBusNo());
                            binding.txtJourneyDateTime.setText("Journey Date & Time : " + data.getJourneyDate() + "  " + data.getJourneyStartTime());


                            binding.emptySeaters.setText("Empty Seater : " + data.getTotalEmptySeat());
                            binding.totalBookedSeaters.setText("Total Seats Booked : " + calTotalBookedSeater());
                            binding.txtSeatSeaters.setText(data.getEmptySeat());


                            binding.emptySleeper.setText("Empty Sleeper : " + data.getTotalEmptySleeper());
                            binding.totalBookedSleeper.setText("Total Sleeper Booked : ");
                            binding.txtSeatSleeper.setText(data.getEmptySleeper());


                            for (PickupWiseInquiry_Response.MainDatum pojo : list_pickup_inquiry) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getPickUpName())) {
                                    List<PickupWiseInquiry_Response.MainDatum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<PickupWiseInquiry_Response.MainDatum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getPickUpName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                }
                            }

                            binding.rvPickupInquiry.setAdapter(new PickupWiseInquiryReportAdapter(PickupWiseInquiryReportActivity.this, Hashmap_allDayMemo));
                        }

                        if (response.body().getData().getSubRouteWiseData() != null && response.body().getData().getSubRouteWiseData().size() > 0) {
                            table1List = response.body().getData().getSubRouteWiseData();
                            binding.rvSubRoute.setAdapter(new PickupWiseInquirySubRouteAdapter(table1List, PickupWiseInquiryReportActivity.this));
                            binding.txtTotalSubroute.setText(calTotalSubRoute());
                        }

                    } else {
                        Toast.makeText(PickupWiseInquiryReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PickupWiseInquiry_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private String calTotalBookedSeater() {
        int totalSubRoute = 0;
        for (PickupWiseInquiry_Response.MainDatum data : list_pickup_inquiry) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSubRoute += Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSubRoute);
    }


    private String calTotalBookedSleeper() {
        int totalSubRoute = 0;
        for (PickupWiseInquiry_Response.MainDatum data : list_pickup_inquiry) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSubRoute += Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSubRoute);
    }

    private String calTotalSubRoute() {
        int totalSubRoute = 0;
        for (PickupWiseInquiry_Response.SubRouteWiseDatum data : table1List) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSubRoute += Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSubRoute);
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(PickupWiseInquiryReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_PickupWiseInquiry")) {
                                        Get_Report_PickupWiseInquiryApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(PickupWiseInquiryReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}