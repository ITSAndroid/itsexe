package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.PickupDropReport_Request;
import com.infinityinfoway.itsexe.api.response.PickupDropReport_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityPickupDropReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.PickupDropReportAdapter_DropUp;
import com.infinityinfoway.itsexe.exe_adapter.PickupDropReportAdapter_PickUp;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickupDropReportActivity extends AppCompatActivity {

    String method;
    ProgressDialog progDialog;
    private LinkedHashMap<String, List<PickupDropReport_Response.PickUpDropWiseChart>> pickUpHashMap;
    private List<PickupDropReport_Response.PickUpDropWiseChart> listPickUp;

    private LinkedHashMap<String, List<PickupDropReport_Response.SubDropUpWiseChart>> dropUpHashMap;
    private List<PickupDropReport_Response.SubDropUpWiseChart> listDropUp;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "PickupDropReport";
    private String resReportTitle, journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId, isSubRoute = 0;
    private Bundle getRouteData;

    private ActivityPickupDropReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPickupDropReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();
            cm_CompanyId = getRouteData.getInt("Company_Id");
            resReportTitle = getRouteData.getString("ReportTitle");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            isSameDay = getRouteData.getString("IsSameDay");
            isSubRoute = getRouteData.getInt("IsSubRoute");
        }

        cd = new ConnectionDetector(PickupDropReportActivity.this);
        getPref = new ITSExeSharedPref(PickupDropReportActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        //binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());

        PickupDropReportApiCall();

    }

    private void PickupDropReportApiCall() {
        showProgressDialog("Loading....");

        PickupDropReport_Request request = new PickupDropReport_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartTime,
                journeyCityTime,
                0,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay,
                fromCity
        );


        Call<PickupDropReport_Response> call = apiService.PickupDropReport(request);
        call.enqueue(new Callback<PickupDropReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<PickupDropReport_Response> call, @NotNull Response<PickupDropReport_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null) {

                            if (response.body().getData().getPickUpDropWiseChart() != null && response.body().getData().getPickUpDropWiseChart().size() > 0) {
                                pickUpHashMap = new LinkedHashMap<>();

                                listPickUp = response.body().getData().getPickUpDropWiseChart();
                                PickupDropReport_Response.PickUpDropWiseChart data=listPickUp.get(0);

                                binding.txtPrintDate.setText("Print Date : " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                                binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");
                                binding.txtRoute.setText("Route : " + data.getRouteName() + " " + data.getJourneyStartTime());
                                binding.txtCoachNo.setText("Coach No. : " + data.getBusNo());
                                binding.txtJdate.setText("Journey Date & Time : " + data.getJourneyDate() + "  " + data.getJourneyCityTime());

                                binding.txtDriver.setText("Driver Name : " + data.getDMDriver1());
                                binding.txtDriverName.setText("Driver Name : " + data.getDMDriver2());
                                binding.txtConductor.setText("Conductor Name : " + data.getDMConductor());

                                binding.txtSeatSleeper.setText(data.getEmptySleeper());
                                binding.emptySleeper.setText("Empty Sleeper : " + data.getTotalEmptySleeper());
                                binding.totalBookedSeaters.setText("Total Seats Booked : " + calTotalSeat(listPickUp));
                                binding.txtSeatSeaters.setText(data.getEmptySeat());
                                binding.emptySeaters.setText("Empty Seater : " + data.getTotalEmptySeat());

                                for (PickupDropReport_Response.PickUpDropWiseChart pojo : listPickUp) {

                                    if (!pickUpHashMap.containsKey(pojo.getPickUpName())) {
                                        List<PickupDropReport_Response.PickUpDropWiseChart> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        pickUpHashMap.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<PickupDropReport_Response.PickUpDropWiseChart> pickDropPassengerDetailPojoList = pickUpHashMap.get(pojo.getPickUpName());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        pickUpHashMap.put(pojo.getPickUpName(), pickDropPassengerDetailPojoList);
                                    }
                                }

                                binding.rvPickup.setAdapter(new PickupDropReportAdapter_PickUp(pickUpHashMap, PickupDropReportActivity.this));

                            }

                            if (response.body().getData().getSubDropUpWiseChart() != null && response.body().getData().getSubDropUpWiseChart().size() > 0) {
                                dropUpHashMap = new LinkedHashMap<>();

                                listDropUp = response.body().getData().getSubDropUpWiseChart();

                                for (PickupDropReport_Response.SubDropUpWiseChart pojo : listDropUp) {

                                    if (!dropUpHashMap.containsKey(pojo.getToCityName())) {
                                        List<PickupDropReport_Response.SubDropUpWiseChart> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        dropUpHashMap.put(pojo.getToCityName(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<PickupDropReport_Response.SubDropUpWiseChart> pickDropPassengerDetailPojoList = dropUpHashMap.get(pojo.getToCityName());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        dropUpHashMap.put(pojo.getToCityName(), pickDropPassengerDetailPojoList);
                                    }
                                }

                                binding.rvDrop.setAdapter(new PickupDropReportAdapter_DropUp(PickupDropReportActivity.this, dropUpHashMap));

                                binding.txtDropTotalSeats.setText("Total Seats : " + calTotalSeatDropUp(listDropUp));
                            }

                        } else {
                            Toast.makeText(PickupDropReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(PickupDropReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PickupDropReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(PickupDropReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("PickupDropReport")) {
                                        PickupDropReportApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(PickupDropReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private String calTotalSeat(List<PickupDropReport_Response.PickUpDropWiseChart> SeatDataList){
        int totalSeat=0;
        for(PickupDropReport_Response.PickUpDropWiseChart data:SeatDataList){
            if(!TextUtils.isEmpty(data.getTotalSeat())){
                totalSeat+=Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSeat);
    }
    private String calTotalSeatDropUp(List<PickupDropReport_Response.SubDropUpWiseChart> SeatDataList){
        int totalSeat=0;
        for(PickupDropReport_Response.SubDropUpWiseChart data:SeatDataList){
            if(!TextUtils.isEmpty(data.getTotalSeat())){
                totalSeat+=Integer.parseInt(data.getTotalSeat());
            }
        }
        return String.valueOf(totalSeat);
    }

}
