package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.BusIncomeReceipt_Request;
import com.infinityinfoway.itsexe.api.response.BusIncomeReceipt_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityBusIncomeReceiptReportBinding;
import com.infinityinfoway.itsexe.exe_adapter.BusIncomeReceiptReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusIncomeReceiptReportActivity extends AppCompatActivity {

    private ActivityBusIncomeReceiptReportBinding binding;
    private LinkedHashMap<String, List<BusIncomeReceipt_Response.Datum>> linkedHashMap;
    private List<BusIncomeReceipt_Response.Datum> list;
    private Bundle getRouteData;
    private String resReportTitle = "";
    private String journeyStartDate;
    private int cm_CompanyId, routeId, routeTimeId;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "BusIncomeReceipt";
    // activity declare need in manifiest

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityBusIncomeReceiptReportBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");

        }
        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(BusIncomeReceiptReportActivity.this);
        getPref = new ITSExeSharedPref(BusIncomeReceiptReportActivity.this);

        BusIncomeReceiptApiCall();

    }

    private void BusIncomeReceiptApiCall() {

        showProgressDialog("Loading....");

        BusIncomeReceipt_Request request = new BusIncomeReceipt_Request(
                10,
                70179,
                27671,
                585,
                "21-01-2021"
        );

        /*BusIncomeReceipt_Request request = new BusIncomeReceipt_Request(
                getPref.getBUM_BranchUserID(),
                routeTimeId,
                routeId,
                cm_CompanyId,
                journeyStartDate
        );*/

        Call<BusIncomeReceipt_Response> call = apiService.BusIncomeReceipt(request);
        call.enqueue(new Callback<BusIncomeReceipt_Response>() {
            @Override
            public void onResponse(@NotNull Call<BusIncomeReceipt_Response> call, @NotNull Response<BusIncomeReceipt_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1 && response.body().getData() != null && response.body().getData().size() > 0) {
                        linkedHashMap = new LinkedHashMap<>();
                        list = response.body().getData();

                        for (BusIncomeReceipt_Response.Datum data : list) {
                            if (!linkedHashMap.containsKey(data.getGroupBy())) {
                                List<BusIncomeReceipt_Response.Datum> subList = new ArrayList<>();
                                subList.add(data);
                                linkedHashMap.put(data.getGroupBy(), subList);
                            } else {
                                List<BusIncomeReceipt_Response.Datum> subList = linkedHashMap.get(data.getGroupBy());
                                subList.add(data);
                                linkedHashMap.put(data.getGroupBy(), subList);
                            }
                        }

                        binding.rvData.setAdapter(new BusIncomeReceiptReportAdapter(BusIncomeReceiptReportActivity.this, linkedHashMap));

                        BusIncomeReceipt_Response.Datum data=list.get(0);

                        binding.txtCompanyName.setText(data.getCompanyName());
                        binding.txtBranchName.setText(data.getCompanyAddress());

                        binding.txtRoute.setText("Route : "+data.getRTRouteName());
                        binding.txtJdate.setText("Journet Date & Time : "+data.getJMJourneyStartDate());
                        binding.txtBusNo.setText("Vehicle No : "+data.getBusNo());

                        binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                        binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                        binding.txtGrandTotal.setText(String.format("%.2f", data.getGrandTotal()));
                        binding.txtHoCommTitle.setText("HO Comm (" + String.format("%.2f", data.getHOCommPer()) + "%)");
                        binding.txtHoComm.setText(String.format("%.2f", data.getHoComm()));
                        binding.txtNet.setText(String.format("%.2f", data.getNet()));


                    } else {
                        Toast.makeText(BusIncomeReceiptReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<BusIncomeReceipt_Response> call, @NotNull Throwable t) {

            }
        });


    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(BusIncomeReceiptReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(BusIncomeReceiptReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("BusIncomeReceipt")) {
                                        BusIncomeReceiptApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

}
