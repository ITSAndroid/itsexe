package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.CityDropUpWiseChart_Request;
import com.infinityinfoway.itsexe.api.response.CityDropUpWiseChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityCityDropupWiseChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.CityDropWiseChartReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityDropUpWiseChartActivity extends AppCompatActivity {


    CityDropWiseChartReportAdapter cityDropWiseChartReportAdapter;
    LinkedHashMap<String, List<CityDropUpWiseChart_Response.Datum>> Hashmap_allDayMemo;
    List<CityDropUpWiseChart_Response.Datum> list_cityDropWiseChart;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_CityDropWise";

    private String resReportTitle,journeyStartDate, journeyStartTime,isSameDay;
    private int cm_CompanyId,bookedByCompanyId, routeId, routeTimeId, fromCity,  arrangementId, cityId;
    private Bundle getRouteData;

    private ActivityCityDropupWiseChartBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCityDropupWiseChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        if (getIntent().hasExtra("Company_Id")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            isSameDay = getRouteData.getString("IsSameDay");
        }

        cd = new ConnectionDetector(CityDropUpWiseChartActivity.this);
        getPref = new ITSExeSharedPref(CityDropUpWiseChartActivity.this);

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_CityDropWiseApiCall();

    }

    private void Get_Report_CityDropWiseApiCall() {

        showProgressDialog("Loading....");

        CityDropUpWiseChart_Request request = new CityDropUpWiseChart_Request(
                cm_CompanyId,
                journeyStartDate,
                routeId,
                routeTimeId,
                journeyStartTime,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                fromCity,
                bookedByCompanyId,
                isSameDay
        );

        Call<CityDropUpWiseChart_Response> call = apiService.CityDropUpWiseChart(request);
        call.enqueue(new Callback<CityDropUpWiseChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<CityDropUpWiseChart_Response> call, @NotNull Response<CityDropUpWiseChart_Response> response) {
                disMissDialog();

                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_cityDropWiseChart = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_cityDropWiseChart = response.body().getData();
                            if (list_cityDropWiseChart != null && list_cityDropWiseChart.size() > 0) {

                                binding.txtCompanyName.setText(list_cityDropWiseChart.get(0).getCMCompanyName());
                                binding.txtCompanyAddress.setText(list_cityDropWiseChart.get(0).getCMCompanyAddress());
                                binding.txtRouteName.setText("# Route :" + list_cityDropWiseChart.get(0).getRMRouteNameDisplay());
                                binding.txtRouteTime.setText("Route Time :" + list_cityDropWiseChart.get(0).getStartTime());
                                binding.txtDate.setText("Date :" + list_cityDropWiseChart.get(0).getJMJourneyStartDate());
                                binding.txtTotalSeat.setText("Total Seats: " + getRouteTotalSeatAmt(list_cityDropWiseChart).get(0));
                                binding.txtBusNo.setText("Bus No :");
                                binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                                Hashmap_allDayMemo = new LinkedHashMap<>();

                                for (CityDropUpWiseChart_Response.Datum pojo : list_cityDropWiseChart) {

                                    if (!Hashmap_allDayMemo.containsKey(pojo.getToCityName())) {
                                        List<CityDropUpWiseChart_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        Hashmap_allDayMemo.put(pojo.getToCityName(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<CityDropUpWiseChart_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getToCityName());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        Hashmap_allDayMemo.put(pojo.getToCityName(), pickDropPassengerDetailPojoList);
                                    }
                                }


                                cityDropWiseChartReportAdapter = new CityDropWiseChartReportAdapter(CityDropUpWiseChartActivity.this, Hashmap_allDayMemo);
                                binding.rvCityDrop.setAdapter(cityDropWiseChartReportAdapter);

                            }
                        }
                    } else {
                        Toast.makeText(CityDropUpWiseChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }


            }

            @Override
            public void onFailure(@NotNull Call<CityDropUpWiseChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(CityDropUpWiseChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_CityDropWise")) {
                                        Get_Report_CityDropWiseApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(CityDropUpWiseChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


    /*public class LazyDataConnection_ExeApi extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData_Exe.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {
            if (xmlResponse.equals("")) {
                if (method.equals("Get_BusConnectExe_Report_CityDropWise")) {
                    try {
                        progDialog.dismiss();
                    } catch (Exception ex) {
                    }
                }
                Intent i = new Intent(CityDropWiseChartReportActivity.this, InternetErrorActivity.class);
                i.putExtra("caller", "ExeBookingActivity");
                startActivity(i);
                return;
            }
            if (method.equals("Get_BusConnectExe_Report_CityDropWise")) {

                Get_BusConnectExe_Report_CityDropWise(xmlResponse, "CityDropWise");

                try {
                    progDialog.dismiss();
                } catch (Exception ex) {
                }


                if (list_cityDropWiseChart != null && list_cityDropWiseChart.size() > 0) {
                    txt_company_name.setText(list_cityDropWiseChart.get(0).getCM_CompanyName());
                    txt_company_address.setText(list_cityDropWiseChart.get(0).getCM_CompanyAddress());
                    txt_route_name.setText("Route :" + list_cityDropWiseChart.get(0).getRM_RouteNameDisplay());
                    txt_route_time.setText("Route Time :" + list_cityDropWiseChart.get(0).getStartTime());
                    txt_date.setText("Date :" + list_cityDropWiseChart.get(0).getJM_JourneyStartDate());
                    txt_total_Seat.setText(list_cityDropWiseChart.get(0).getTotalSeat());


                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    for (CityDropWiseChartReportBean pojo : list_cityDropWiseChart) {

                        if (!Hashmap_allDayMemo.containsKey(pojo.getToCityName())) {
                            List<CityDropWiseChartReportBean> pickDropPassengerDetailPojoList = new ArrayList<>();
                            pickDropPassengerDetailPojoList.add(pojo);
                            Hashmap_allDayMemo.put(pojo.getToCityName(), pickDropPassengerDetailPojoList);
                        } else {
                            List<CityDropWiseChartReportBean> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getToCityName());
                            pickDropPassengerDetailPojoList.add(pojo);
                            Hashmap_allDayMemo.put(pojo.getToCityName(), pickDropPassengerDetailPojoList);
                        }
                    }


                    cityDropWiseChartReportAdapter = new CityDropWiseChartReportAdapter(CityDropWiseChartReportActivity.this, Hashmap_allDayMemo);
                    recycler_report.setAdapter(cityDropWiseChartReportAdapter);
                }
            }
        }
    }*/

   /* public void Get_BusConnectExe_Report_CityDropWise(String str_RespXML, String str_TagName) {
        Document doc = getData_Exe.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        list_cityDropWiseChart = new ArrayList<>();
        for (int i = 0; i <= nodes.getLength(); i++) {
            Node e1 = nodes.item(i);
            Element el = (Element) e1;

            if (el != null) {
                CityDropWiseChartReportBean allDayMemoReportBean = new CityDropWiseChartReportBean();

                try {
                    allDayMemoReportBean.setSeatNo(el.getElementsByTagName("SeatNo").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTotalSeat(el.getElementsByTagName("TotalSeat").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setDropName(el.getElementsByTagName("DropName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCityDropTime(el.getElementsByTagName("CityDropTime").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_JourneyStartDate(el.getElementsByTagName("JM_JourneyStartDate").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRM_RouteNameDisplay(el.getElementsByTagName("RM_RouteNameDisplay").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRMC_SrNo(el.getElementsByTagName("RMC_SrNo").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJM_JourneyTo(el.getElementsByTagName("JM_JourneyTo").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setToCityName(el.getElementsByTagName("ToCityName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setStartTime(el.getElementsByTagName("StartTime").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_CompanyName(el.getElementsByTagName("CM_CompanyName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setCM_CompanyAddress(el.getElementsByTagName("CM_CompanyAddress").item(0).getTextContent());
                } catch (Exception e) {
                }


                list_cityDropWiseChart.add(allDayMemoReportBean);
            }
        }
    }*/

    private List<String> getRouteTotalSeatAmt(List<CityDropUpWiseChart_Response.Datum> list) {
        int totalRouteSeat = 0;
        for (CityDropUpWiseChart_Response.Datum data : list) {
            totalRouteSeat += data.getTotalSeat();
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        return arrayList;
    }

}
