package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.DailyRoutetimeWiseMemo_Request;
import com.infinityinfoway.itsexe.api.response.DailyRoutetimeWiseMemo_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.exe_adapter.DailyRouteTimeWiseMemoReportAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyRouteTimeWiseMemoReportActivity extends AppCompatActivity {

    RecyclerView recycler_daily_route_time;
    DailyRouteTimeWiseMemoReportAdapter dailyRouteTimeWiseMemoReportAdapter;
    String method;
    ProgressDialog progDialog;
    List<DailyRoutetimeWiseMemo_Response.Datum> list_dailyRouteTime;
    TextView txt_company_name, txt_empty_seats, txt_report_name, txt_print_by, txt_print_date, txt_journey_date, txt_driver;
    TextView txt_route, txt_amount_Second, txt_total_comission, txt_total_amt, txt_total_seat, txt_total_empty_seat;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_DailyRouteTimeWiseMemo";
    private String isSameDay, resReportTitle, journeyStartDate;
    private int cm_CompanyId, routeId, bookedByCompanyId, routeTimeId, fromCity, toCity;
    private Bundle getRouteData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_route_time_wise_memo_report);


        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            isSameDay = getRouteData.getString("IsSameDay");
        }

        cd = new ConnectionDetector(DailyRouteTimeWiseMemoReportActivity.this);
        getPref = new ITSExeSharedPref(DailyRouteTimeWiseMemoReportActivity.this);

        recycler_daily_route_time = findViewById(R.id.recycler_daily_route_time);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_daily_route_time.setLayoutManager(linearLayoutManager);


        txt_company_name = findViewById(R.id.txt_company_name);
        txt_report_name = findViewById(R.id.txt_report_name);
        txt_print_by = findViewById(R.id.txt_print_by);
        txt_print_date = findViewById(R.id.txt_print_date);
        txt_journey_date = findViewById(R.id.txt_journey_date);
        txt_driver = findViewById(R.id.txt_driver);

        txt_empty_seats = findViewById(R.id.txt_empty_seats);

        txt_route = findViewById(R.id.txt_route);
        txt_amount_Second = findViewById(R.id.txt_amount_Second);
        txt_total_comission = findViewById(R.id.txt_total_comission);
        txt_total_amt = findViewById(R.id.txt_total_amt);
        txt_total_seat = findViewById(R.id.txt_total_seat);
        txt_total_empty_seat = findViewById(R.id.txt_total_empty_seat);

        Get_Report_DailyRouteTimeWiseMemoApiCall();


    }

    private void Get_Report_DailyRouteTimeWiseMemoApiCall() {

        showProgressDialog("Loading....");

        DailyRoutetimeWiseMemo_Request request = new DailyRoutetimeWiseMemo_Request(
                journeyStartDate,
                cm_CompanyId,
                routeId,
                routeTimeId,
                fromCity,
                toCity,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );

        Call<DailyRoutetimeWiseMemo_Response> call = apiService.DailyRoutetimeWiseMemo(request);
        call.enqueue(new Callback<DailyRoutetimeWiseMemo_Response>() {
            @Override
            public void onResponse(@NotNull Call<DailyRoutetimeWiseMemo_Response> call, @NotNull Response<DailyRoutetimeWiseMemo_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_dailyRouteTime = new ArrayList<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_dailyRouteTime = response.body().getData();

                            txt_report_name.setText(list_dailyRouteTime.get(0).getReportHeader());

                            txt_total_empty_seat.setText("Total Empty Seats : " + list_dailyRouteTime.get(0).getTotalEmptySeats());
                            txt_empty_seats.setText(list_dailyRouteTime.get(0).getEmptySeats());

                            txt_company_name.setText(list_dailyRouteTime.get(0).getCMCompanyName());
                            txt_report_name.setText(list_dailyRouteTime.get(0).getReportHeader());
                            txt_print_by.setText(list_dailyRouteTime.get(0).getPrintBy());
                            txt_print_date.setText("Print Date: " + new SimpleDateFormat("dd-MM-yyyy HH:mm aa").format(Calendar.getInstance().getTime()));
                            txt_journey_date.setText(list_dailyRouteTime.get(0).getJourneyDate());
                            txt_driver.setText(list_dailyRouteTime.get(0).getDriverName());

                            txt_route.setText(list_dailyRouteTime.get(0).getRouteNam());

                            List<String> arrayList=getReportSummary();

                            txt_total_seat.setText(arrayList.get(0));
                            txt_total_amt.setText(arrayList.get(1));
                            txt_total_comission.setText(arrayList.get(2));
                            txt_amount_Second.setText(arrayList.get(3));


                            dailyRouteTimeWiseMemoReportAdapter = new DailyRouteTimeWiseMemoReportAdapter(DailyRouteTimeWiseMemoReportActivity.this, list_dailyRouteTime);
                            recycler_daily_route_time.setAdapter(dailyRouteTimeWiseMemoReportAdapter);
                        }
                    } else {
                        Toast.makeText(DailyRouteTimeWiseMemoReportActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<DailyRoutetimeWiseMemo_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(DailyRouteTimeWiseMemoReportActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_DailyRouteTimeWiseMemo")) {
                                        Get_Report_DailyRouteTimeWiseMemoApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(DailyRouteTimeWiseMemoReportActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private List<String> getReportSummary() {
        int totalPax = 0;
        double totalAmount = 0.0, totalCommission = 0.0, totalNetAmount = 0.0;
        for (DailyRoutetimeWiseMemo_Response.Datum data : list_dailyRouteTime) {
            totalPax += data.getTotalPax();
            totalAmount += data.getTotalAmount();
            totalCommission += data.getTotalCommission();
            totalNetAmount += data.getTotalNetAmount();
        }
        List<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalPax));
        arrayList.add(String.format("%.2f", totalAmount));
        arrayList.add(String.format("%.2f", totalCommission));
        arrayList.add(String.format("%.2f", totalNetAmount));
        return arrayList;
    }


}
