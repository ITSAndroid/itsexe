package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.BranchUserWiseCollection_RefundChart_Request;
import com.infinityinfoway.itsexe.api.response.BranchUserWiseCollection_RefundChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityBranchUserWiseCollectionRefundChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.BranchUserWiseCollectionSummaryAdapter;
import com.infinityinfoway.itsexe.exe_adapter.BranchUserWiseRechargeSummaryAdapter;
import com.infinityinfoway.itsexe.exe_adapter.BranchUserWiseRefundSummaryAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchUserWiseCollectionRefundChartActivity extends AppCompatActivity {

    private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending> branchUserWiseRefundListPending;
    private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund> branchUserWiseRefundList;
    private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection> branchUserWiseCollectionList;
    private List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseAgentRecharge> branchUserWiseAgentRechargeList;

    private LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection>> linkedHashMapCollection;
    private LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund>> linkedHashMapRefund;
    private LinkedHashMap<String, List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending>> linkedHashMapRefundPending;


    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "BranchUserWiseCollection_RefundChart";
    private String journeyStartDate;
    private int cm_CompanyId, isPendingAmount;
    private Bundle getRouteData;
    private String resReportTitle = "";
    private ActivityBranchUserWiseCollectionRefundChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBranchUserWiseCollectionRefundChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getPref = new ITSExeSharedPref(BranchUserWiseCollectionRefundChartActivity.this);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId=getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            isPendingAmount=getRouteData.getInt("IsPendingAmount");
        }


        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);

        Get_Report_BranchUserWiseCollectionRefundChartApiCall();

    }

    private void Get_Report_BranchUserWiseCollectionRefundChartApiCall() {
        showProgressDialog("Loading....");


        if (isPendingAmount == 1) {
            binding.llTop.setVisibility(View.VISIBLE);
        }



        BranchUserWiseCollection_RefundChart_Request request = new BranchUserWiseCollection_RefundChart_Request(
                journeyStartDate,
                getPref.getBUM_BranchUserID(),
                getPref.getBM_BranchID(),
                cm_CompanyId,
                isPendingAmount
        );


        Call<BranchUserWiseCollection_RefundChart_Response> call = apiService.BranchUserWiseCollection_RefundChart(request);
        call.enqueue(new Callback<BranchUserWiseCollection_RefundChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<BranchUserWiseCollection_RefundChart_Response> call, @NotNull Response<BranchUserWiseCollection_RefundChart_Response> response) {
                disMissDialog();
                if (response != null && response.isSuccessful() && response.body() != null) {

                    branchUserWiseCollectionList = new ArrayList<>();
                    branchUserWiseAgentRechargeList = new ArrayList<>();
                    branchUserWiseRefundList = new ArrayList<>();

                    linkedHashMapCollection = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {

                        if (isPendingAmount == 1) {
                            // Collection
                            if (response.body().getData().getBranchUserWiseCollection() != null && response.body().getData().getBranchUserWiseCollection().size() > 0) {

                                branchUserWiseCollectionList = response.body().getData().getBranchUserWiseCollection();

                                for (BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection pojo : branchUserWiseCollectionList) {
                                    if (!linkedHashMapCollection.containsKey(pojo.getBookingType())) {
                                        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        linkedHashMapCollection.put(pojo.getBookingType(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection> pickDropPassengerDetailPojoList = linkedHashMapCollection.get(pojo.getBookingType());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        linkedHashMapCollection.put(pojo.getBookingType(), pickDropPassengerDetailPojoList);
                                    }
                                }

                                binding.rvCollection.setAdapter(new BranchUserWiseCollectionSummaryAdapter(linkedHashMapCollection, BranchUserWiseCollectionRefundChartActivity.this));

                                BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection reportHeader = branchUserWiseCollectionList.get(0);

                                binding.txtTitleCollectionSummary.setPaintFlags(binding.txtTitleCollectionSummary.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                                binding.txtBranchAgentName.setText(reportHeader.getBranchNameCode());
                                binding.txtBranchName.setText(reportHeader.getBMAddress());
                                binding.txtBranchUser.setText(reportHeader.getBUMUserName());
                                binding.txtReportDate.setText("Report Date: " + reportHeader.getReportDate());
                                binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));

                                ArrayList<String> arrayList = getGrandTotal(branchUserWiseCollectionList);

                                binding.txtTotalPax.setText(arrayList.get(0));
                                binding.txtGrandTotal.setText(arrayList.get(1));

                            }

                            // Agent Recharge

                            if (response.body().getData().getBranchUserWiseAgentRecharge() != null && response.body().getData().getBranchUserWiseAgentRecharge().size() > 0) {
                                binding.txtTitleRechargeSummary.setPaintFlags(binding.txtTitleRechargeSummary.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                                branchUserWiseAgentRechargeList = response.body().getData().getBranchUserWiseAgentRecharge();

                                binding.rvRechargeSummary.setAdapter(new BranchUserWiseRechargeSummaryAdapter(BranchUserWiseCollectionRefundChartActivity.this, branchUserWiseAgentRechargeList));
                                binding.txtRechargeSummaryTotal.setText(getRechargeSummaryTotal(branchUserWiseAgentRechargeList));

                            }

                            // Refund

                            if (response.body().getData().getBranchUserWiseRefund() != null && response.body().getData().getBranchUserWiseRefund().size() > 0) {
                                binding.txtTitleRefundSummary.setPaintFlags(binding.txtTitleRechargeSummary.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                                branchUserWiseRefundList = response.body().getData().getBranchUserWiseRefund();

                                linkedHashMapRefund = new LinkedHashMap<>();

                                for (BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund pojo : branchUserWiseRefundList) {
                                    if (!linkedHashMapRefund.containsKey(pojo.getAmountType())) {
                                        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        linkedHashMapRefund.put(pojo.getAmountType(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund> pickDropPassengerDetailPojoList = linkedHashMapRefund.get(pojo.getAmountType());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        linkedHashMapRefund.put(pojo.getAmountType(), pickDropPassengerDetailPojoList);
                                    }
                                }

                                binding.rvRefundSummary.setAdapter(new BranchUserWiseRefundSummaryAdapter(BranchUserWiseCollectionRefundChartActivity.this, linkedHashMapRefund));

                                BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund data = branchUserWiseRefundList.get(0);

                                /*if(isPendingAmount==0){
                                    binding.txtTitleCollectionSummary.setPaintFlags(binding.txtTitleCollectionSummary.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                                    binding.txtBranchAgentName.setText(data.getBranchCode());
                                    binding.txtBranchName.setText(data.getBranchAddress());
                                    binding.txtBranchUser.setText(data.getUserName());
                                    binding.txtReportDate.setText("Report Date: " + data.getReportDate());
                                    binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                                }*/

                                ArrayList<String> arrayList = getRefundSummary(branchUserWiseRefundList);
                                if (arrayList.size() > 0) {
                                    binding.txtCancelSeat.setVisibility(View.VISIBLE);
                                    binding.txtTotalRefund.setVisibility(View.VISIBLE);
                                    binding.txtCancelSeat.setText("Total Cancelled Seats: " + arrayList.get(0));
                                    binding.txtTotalRefund.setText("Total: " + arrayList.get(1));
                                }

                               /* binding.txtCollectionCash.setText(String.format("%.2f", data.getCashCollection()));
                                binding.txtCollectionCredit.setText(String.format("%.2f", data.getBankCollection()));
                                binding.txtRefundCharge.setText(String.format("%.2f", data.getRefundCharge()));
                                binding.txtRefund.setText(String.format("%.2f", data.getRefund()));
                                binding.txtAgentRecharge.setText(String.format("%.2f", data.getTotalAgentRecharge()));
                                binding.txtOtherIncome.setText(String.format("%.2f", data.getOtherIncome()));
                                binding.txtOtherExpense.setText(String.format("%.2f", data.getOtherExpense()));*/

                                binding.txtCollectionCash.setText(data.getCashCollection());
                                binding.txtCollectionCredit.setText(data.getBankCollection());
                                binding.txtRefundCharge.setText(data.getRefundCharge());
                                binding.txtRefund.setText(data.getRefund());
                                binding.txtAgentRecharge.setText(data.getTotalAgentRecharge());
                                binding.txtOtherIncome.setText(data.getOtherIncome());
                                binding.txtOtherExpense.setText(data.getOtherExpense());

                                double totalReceive = 0.0, totalComm = 0.0;

                                if (!TextUtils.isEmpty(data.getCashCollection())) {
                                    totalComm += Double.parseDouble(data.getCashCollection());
                                }
                                if (!TextUtils.isEmpty(data.getBankCollection())) {
                                    totalComm += Double.parseDouble(data.getBankCollection());
                                }
                                if (!TextUtils.isEmpty(data.getRefundCharge())) {
                                    totalComm += Double.parseDouble(data.getRefundCharge());
                                }
                                if (!TextUtils.isEmpty(data.getTotalAgentRecharge())) {
                                    totalReceive += Double.parseDouble(data.getTotalAgentRecharge());
                                }
                                if (!TextUtils.isEmpty(data.getOtherIncome())) {
                                    totalReceive += Double.parseDouble(data.getOtherIncome());
                                }

                                binding.txtReceived.setText(String.format("%.2f", totalReceive));
                                binding.txtCommission.setText(String.format("%.2f", totalComm));

                                binding.txtNetBalance.setText("NET BALANCE Rs: " + data.getNetTotal());
                                binding.txtBranchBkg.setText("Branch Bkg: " + data.getBranchBooking());
                                binding.txtBranchBkgRefChg.setText("Branch Bkg Ref. Chrg.: " + data.getBranchBookingRefundCharge());

                                binding.txtFinalBalance.setText("Final Balance: " + data.getFinalTotal());

                            }

                        } else if (isPendingAmount == 0) {


                            // Refund

                            if (response.body().getData().getBranchUserWiseCollectionRefundPending() != null && response.body().getData().getBranchUserWiseCollectionRefundPending().size() > 0) {
                                binding.txtTitleRefundSummary.setPaintFlags(binding.txtTitleRechargeSummary.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                                branchUserWiseRefundListPending = response.body().getData().getBranchUserWiseCollectionRefundPending();

                                linkedHashMapRefundPending = new LinkedHashMap<>();

                                for (BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending pojo : branchUserWiseRefundListPending) {
                                    if (!linkedHashMapRefundPending.containsKey(pojo.getGroupBy())) {
                                        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending> pickDropPassengerDetailPojoList = new ArrayList<>();
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        linkedHashMapRefundPending.put(pojo.getGroupBy(), pickDropPassengerDetailPojoList);
                                    } else {
                                        List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending> pickDropPassengerDetailPojoList = linkedHashMapRefundPending.get(pojo.getGroupBy());
                                        pickDropPassengerDetailPojoList.add(pojo);
                                        linkedHashMapRefundPending.put(pojo.getGroupBy(), pickDropPassengerDetailPojoList);
                                    }
                                }

                                binding.rvRefundSummary.setAdapter(new BranchUserWiseRefundSummaryAdapter(BranchUserWiseCollectionRefundChartActivity.this, linkedHashMapRefundPending, 0));

                                BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollectionRefundPending data = branchUserWiseRefundListPending.get(0);


                                binding.txtCancelSeat.setVisibility(View.GONE);
                                binding.txtTotalRefund.setVisibility(View.GONE);

                                binding.txtTitleCollectionSummary.setPaintFlags(binding.txtTitleCollectionSummary.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                                binding.txtBranchAgentName.setText(data.getBMBranchName());
                                binding.txtBranchName.setText(data.getBMAddress());
                                binding.txtBranchUser.setText(data.getReportHeader());
                                binding.txtReportDate.setText("Report Date: " + data.getReportDate());
                                binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));


                                binding.txtCollectionCash.setText(String.format("%.2f", data.getTotalCashCollection()));
                                binding.txtCollectionCredit.setText(String.format("%.2f", data.getTotalBankCollection()));
                                binding.txtRefundCharge.setText(String.format("%.2f", data.getTotalRefundCharge()));
                                binding.txtRefund.setText(String.format("%.2f", data.getTotalRefund()));
                                binding.txtAgentRecharge.setText("");
                                binding.txtOtherIncome.setText(String.format("%.2f", data.getIncomeAmount()));
                                binding.txtOtherExpense.setText(String.format("%.2f", data.getExpenseAmount()));

                                double totalReceive = 0.0, totalComm = 0.0;

                                totalComm += data.getTotalCashCollection();
                                totalComm += data.getTotalBankCollection();
                                totalComm += data.getTotalRefundCharge();

                                totalReceive += data.getIncomeAmount();

                                binding.txtReceived.setText(String.format("%.2f", totalReceive));
                                binding.txtCommission.setText(String.format("%.2f", totalComm));

                                binding.txtNetBalance.setText("NET BALANCE Rs: " + data.getNetBalance());
                                binding.txtBranchBkg.setText("Branch Bkg: ");
                                binding.txtBranchBkgRefChg.setText("Branch Bkg Ref. Chrg.: ");

                                binding.txtFinalBalance.setText("Final Balance: ");

                            }

                        }


                    } else {
                        Toast.makeText(BranchUserWiseCollectionRefundChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<BranchUserWiseCollection_RefundChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(BranchUserWiseCollectionRefundChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_BranchUserWiseCollectionRefundChart")) {
                                        Get_Report_BranchUserWiseCollectionRefundChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }


    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(BranchUserWiseCollectionRefundChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private ArrayList<String> getGrandTotal(List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection> SeatDataList) {
        double totalAmount = 0.0;
        int totalPax = 0;
        for (BranchUserWiseCollection_RefundChart_Response.BranchUserWiseCollection data : SeatDataList) {
            totalAmount += data.getJMPayableAmount();
            if (!TextUtils.isEmpty(data.getJMTotalPassengers())) {
                totalPax += Integer.parseInt(data.getJMTotalPassengers());
            }
        }
        ArrayList<String> listGrandTotal = new ArrayList<>();
        listGrandTotal.add(String.valueOf(totalPax));
        listGrandTotal.add(String.format("%.2f", totalAmount));
        return listGrandTotal;
    }

    private String getRechargeSummaryTotal(List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseAgentRecharge> list) {
        double totalAmount = 0.0;
        for (BranchUserWiseCollection_RefundChart_Response.BranchUserWiseAgentRecharge data : list) {
            totalAmount += data.getCARMRecharge();
        }
        return String.format("%.2f", totalAmount);
    }

    private ArrayList<String> getRefundSummary(List<BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund> list) {
        double totalAmount = 0.0;
        int totalSeat = 0;

        for (BranchUserWiseCollection_RefundChart_Response.BranchUserWiseRefund data : list) {
            if (data.getAmountType().equalsIgnoreCase("Cancel")) {
                totalAmount += data.getCancelAmount();
                if (!TextUtils.isEmpty(data.getTotalPassanger())) {
                    totalSeat += Integer.parseInt(data.getTotalPassanger());
                }
            }
        }

        ArrayList<String> listGrandTotal = new ArrayList<>();
        if (totalSeat > 0) {
            listGrandTotal.add(String.valueOf(totalSeat));
            listGrandTotal.add(String.format("%.2f", totalAmount));
        }
        return listGrandTotal;
    }

}