package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.MainRouteWiseChart_Request;
import com.infinityinfoway.itsexe.api.response.MainRouteWiseChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityMainRouteWiseChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.MainRouteWiseChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainRouteWiseChartActivity extends AppCompatActivity {

    private LinkedHashMap<String, List<MainRouteWiseChart_Response.Datum>> Hashmap_allDayMemo;
    private List<MainRouteWiseChart_Response.Datum> listMainRoute;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "MainRouteWiseChart";
    private String journeyStartDate, journeyStartTime, journeyCityTime,isSameDay;
    private int cm_CompanyId,bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId;
    private Bundle getRouteData;
    private String resReportTitle = "";
    ActivityMainRouteWiseChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainRouteWiseChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId=getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(MainRouteWiseChartActivity.this);
        getPref = new ITSExeSharedPref(MainRouteWiseChartActivity.this);

        MainRouteWiseChartApiCall();
    }

    private void MainRouteWiseChartApiCall() {

        showProgressDialog("Loading....");



        final MainRouteWiseChart_Request request = new MainRouteWiseChart_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartTime,
                journeyCityTime,
                1,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );

        Call<MainRouteWiseChart_Response> call = apiService.MainRouteWiseChart(request);
        call.enqueue(new Callback<MainRouteWiseChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<MainRouteWiseChart_Response> call, @NotNull Response<MainRouteWiseChart_Response> response) {
                disMissDialog();


                if (response != null && response.isSuccessful() && response.body() != null) {
                    listMainRoute = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listMainRoute = response.body().getData();

                            MainRouteWiseChart_Response.Datum data = listMainRoute.get(0);
                            List<String> listSummary = fetchReportSummary(listMainRoute);

                            binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                            binding.txtPrintBy.setText("Print By : " + data.getPrintBy());
                            binding.txtRoute.setText("Route : " + data.getRouteName());
                            binding.txtCoachNo.setText("Bus No. : " + data.getBusNo());
                            binding.txtDate.setText("Journey Date & Time : " + data.getJourneyDate() + "  " + data.getJourneyStartTime());

                            binding.txtEmptySeatTotal.setText("Empty Seats: " + data.getTotalEmptySeat());
                            binding.txtEmptySeaters.setText(data.getEmptySeat());

                            binding.txtEmptySleepeTotal.setText("Empty Sleepers: " + data.getTotalEmptySleeper());
                            binding.txtEmptySleepers.setText(data.getEmptySleeper());


                            binding.txtTotalBookedSeat.setText("Total Seats Booked: " + listSummary.get(0));
                            binding.txtBaseFare.setText(listSummary.get(1));
                            binding.txtGst.setText(listSummary.get(2));
                            binding.txtTotalAmt.setText(listSummary.get(3));
                            binding.txtRemarks.setText(listSummary.get(4));

                            for (MainRouteWiseChart_Response.Datum pojo : listMainRoute) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getBranchName())) {
                                    List<MainRouteWiseChart_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getBranchName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<MainRouteWiseChart_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getBranchName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getBranchName(), pickDropPassengerDetailPojoList);
                                }
                            }

                            binding.rvMainRoute.setAdapter(new MainRouteWiseChartAdapter(MainRouteWiseChartActivity.this, Hashmap_allDayMemo));

                        }
                    } else {
                        Toast.makeText(MainRouteWiseChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<MainRouteWiseChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
            }
        });

    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(MainRouteWiseChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(MainRouteWiseChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("MainRouteWiseChart")) {
                                        MainRouteWiseChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private List<String> fetchReportSummary(List<MainRouteWiseChart_Response.Datum> list) {
        String remarksDetails = "";
        int totalRouteSeat = 0;
        double totalBaseFare = 0.0, totalGST = 0.0, totalAmount = 0.0;
        for (MainRouteWiseChart_Response.Datum data : list) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalRouteSeat += Integer.parseInt(data.getTotalSeat());
            }
            totalBaseFare += data.getBaseFare();
            totalGST += data.getJMServiceTax();
            totalAmount += data.getJMPayableAmount();
            if (!TextUtils.isEmpty(data.getRemarks())) {
                if (TextUtils.isEmpty(remarksDetails)) {
                    remarksDetails = data.getRemarks();
                } else {
                    remarksDetails += "\n" + data.getRemarks();
                }
            }
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(String.valueOf(totalRouteSeat));
        arrayList.add(String.valueOf(totalBaseFare));
        arrayList.add(String.valueOf(totalGST));
        arrayList.add(String.valueOf(totalAmount));
        arrayList.add(remarksDetails);
        return arrayList;
    }


}