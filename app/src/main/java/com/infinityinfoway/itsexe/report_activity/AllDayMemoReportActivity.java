package com.infinityinfoway.itsexe.report_activity;

import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_Report_AllDayMemo_Request;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_Response;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityAllDayMemoReportBinding;
import com.infinityinfoway.itsexe.databinding.LayoutProgressBarBinding;
import com.infinityinfoway.itsexe.exe_adapter.AllDayMemoAdapter;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AllDayMemoReportActivity extends AppCompatActivity {

    private AllDayMemoAdapter allDayMemoAdapter;
    private LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>>> Hashmap_allDayMemo;
    private LinkedHashMap<String, List<Get_Report_AllDayMemo_Response.AllDayMemo>> linkedHashMap;
    private List<Get_Report_AllDayMemo_Response.AllDayMemo> list_allDayMemo;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ITSExeSharedPref getPref;
    private String journeyStartDate;
    private int cm_CompanyId, bookedByCompanyId;
    private ActivityAllDayMemoReportBinding binding;
    private String resReportTitle = "";
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private LayoutProgressBarBinding progressBarBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAllDayMemoReportBinding.inflate(getLayoutInflater());
        progressBarBinding = LayoutProgressBarBinding.bind(binding.getRoot());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("Company_Id")) {
            Bundle getRouteData = getIntent().getExtras();
            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(v -> finish());

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        getPref = new ITSExeSharedPref(AllDayMemoReportActivity.this);

        progressBarBinding.spinProgress.setVisibility(View.VISIBLE);

        Get_Report_AllDayMemoApiCall();

    }

    private void Get_Report_AllDayMemoApiCall() {

        Get_Report_AllDayMemo_Request request = new Get_Report_AllDayMemo_Request(
                cm_CompanyId,
                journeyStartDate,
                getPref.getBM_BranchID(),
                bookedByCompanyId,
                getPref.getBUM_BranchUserID()
        );

        Log.e("request",""+request.toString());

        Observable<Get_Report_AllDayMemo_Response> observable = apiService.Get_Report_AllDayMemo(request);

        compositeDisposable.add(observable.subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Get_Report_AllDayMemo_Response>() {
            @Override
            public void onNext(@NonNull Get_Report_AllDayMemo_Response response) {
                progressBarBinding.spinProgress.setVisibility(View.GONE);
                if (response.getStatus() != null && response.getStatus() == APP_CONSTANTS.STATUS_SUCCESS) {
                    if (response.getData().getAllDayMemo() != null && response.getData().getAllDayMemo().size() > 0) {

                        list_allDayMemo = response.getData().getAllDayMemo();

                        Get_Report_AllDayMemo_Response.AllDayMemo data = list_allDayMemo.get(0);

                        binding.txtCompanyName.setText(data.getCompanyName());
                        binding.txtCompanyAddress.setText(data.getCompanyAddress());
                        binding.txtDate.setText("Date: " + data.getJDate());
                        binding.txtPrintBy.setText("Print By: " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                        binding.txtTotalCollection.setText("Total Collection \n" + data.getTotalCollection());
                        binding.txtCommission.setText("Total Commission \n" + data.getCommissionAmount());
                        binding.txtTotalSeats.setText("Total Seats \n" + data.getNetSeat());
                        binding.txtNetCollection.setText("Net Collection \n" + data.getNetCollection());

                        Hashmap_allDayMemo = new LinkedHashMap<>();

                        for (Get_Report_AllDayMemo_Response.AllDayMemo pojo : list_allDayMemo) {

                            if (!Hashmap_allDayMemo.containsKey(pojo.getRouteTime())) {
                                linkedHashMap = new LinkedHashMap<>();
                            }

                            if (!linkedHashMap.containsKey(pojo.getRouteName())) {
                                List<Get_Report_AllDayMemo_Response.AllDayMemo> pickDropPassengerDetailPojoList = new ArrayList<>();
                                pickDropPassengerDetailPojoList.add(pojo);
                                linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                            } else {
                                List<Get_Report_AllDayMemo_Response.AllDayMemo> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getRouteName());
                                pickDropPassengerDetailPojoList.add(pojo);
                                linkedHashMap.put(pojo.getRouteName(), pickDropPassengerDetailPojoList);
                            }

                            Hashmap_allDayMemo.put(pojo.getRouteTime(), linkedHashMap);

                        }
                        allDayMemoAdapter = new AllDayMemoAdapter(AllDayMemoReportActivity.this, Hashmap_allDayMemo);
                        binding.recyclerAllDayMemo.setAdapter(allDayMemoAdapter);
                    }
                } else {
                    Toast.makeText(AllDayMemoReportActivity.this, Objects.requireNonNull(response.getMessage(), getResources().getString(R.string.bad_server_response)), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                progressBarBinding.spinProgress.setVisibility(View.GONE);
                Toast.makeText(AllDayMemoReportActivity.this, getResources().getString(R.string.bad_server_response), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() {
            }

        }));

    }


    private void clearDisposable() {
        try {
            if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
                compositeDisposable.clear();
                compositeDisposable.dispose();
            }
        } catch (Exception ignored) { }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearDisposable();
    }
}
