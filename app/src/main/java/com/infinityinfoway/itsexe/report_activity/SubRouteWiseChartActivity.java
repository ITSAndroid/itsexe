package com.infinityinfoway.itsexe.report_activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SubRouteWiseChart_Request;
import com.infinityinfoway.itsexe.api.response.SubRouteWiseChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySubRouteWiseChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.SubRouteWiseChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubRouteWiseChartActivity extends AppCompatActivity {

    private LinkedHashMap<String, LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>>> Hashmap_allDayMemo;
    private LinkedHashMap<String, List<SubRouteWiseChart_Response.Datum>> linkedHashMap;
    private List<SubRouteWiseChart_Response.Datum> list_pickup_inquiry;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "SubRouteWiseChart";
    private String journeyStartDate, journeyStartTime, journeyCityTime, isSameDay;
    private int cm_CompanyId, bookedByCompanyId, cityId, routeId, routeTimeId, fromCity, toCity, arrangementId,isSubRoute;
    private Bundle getRouteData;
    private String resReportTitle = "";

    private ActivitySubRouteWiseChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySubRouteWiseChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId = getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");
            isSubRoute = getRouteData.getInt("IsSubrRoute");
        }

        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        cd = new ConnectionDetector(SubRouteWiseChartActivity.this);
        getPref = new ITSExeSharedPref(SubRouteWiseChartActivity.this);

        SubRouteWiseChartApiCall();

    }

    private void SubRouteWiseChartApiCall() {

        showProgressDialog("Loading....");

        final SubRouteWiseChart_Request request = new SubRouteWiseChart_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartTime,
                journeyCityTime,
                isSubRoute,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );


        Call<SubRouteWiseChart_Response> call = apiService.SubRouteWiseChart(request);
        call.enqueue(new Callback<SubRouteWiseChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<SubRouteWiseChart_Response> call, @NotNull Response<SubRouteWiseChart_Response> response) {
                disMissDialog();


                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_pickup_inquiry = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            list_pickup_inquiry = response.body().getData();

                            SubRouteWiseChart_Response.Datum data = list_pickup_inquiry.get(0);
                            List<String> listSummary = fetchReportSummary();

                            binding.txtCoachNo.setText("Bus No. : " + data.getBusNo());
                            binding.txtRouteName.setText("Route : " + data.getRouteName());
                            binding.txtJourneyDateTime.setText("Journey Date & Time : " + data.getJourneyDate() + "  " + data.getJourneyStartTime());
                            binding.txtPrintBy.setText("Print By : " + data.getPrintBy());


                            binding.txtEmptySeatTotal.setText("Empty Seater : " + data.getTotalEmptySeat());
                            binding.txtTotalBookedSeat.setText("Total Seats Booked >>   " + listSummary.get(0));
                            binding.txtEmptySeaters.setText(data.getEmptySeat());


                            binding.txtEmptySleepeTotal.setText("Empty Sleeper : " + data.getTotalEmptySleeper());
                            binding.txtEmptySleepers.setText(data.getEmptySleeper());


                            binding.txtTotalAmount.setText(listSummary.get(1));
                            binding.txtRemarks.setText(listSummary.get(2));

                            for (SubRouteWiseChart_Response.Datum pojo : list_pickup_inquiry) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getSubRouteName())) {
                                    linkedHashMap = new LinkedHashMap<>();
                                }

                                if (!linkedHashMap.containsKey(pojo.getBranchName())) {
                                    List<SubRouteWiseChart_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getBranchName(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<SubRouteWiseChart_Response.Datum> pickDropPassengerDetailPojoList = linkedHashMap.get(pojo.getBranchName());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    linkedHashMap.put(pojo.getBranchName(), pickDropPassengerDetailPojoList);
                                }

                                Hashmap_allDayMemo.put(pojo.getSubRouteName(), linkedHashMap);

                            }

                            binding.rvRouteTimePickup.setAdapter(new SubRouteWiseChartAdapter(SubRouteWiseChartActivity.this, Hashmap_allDayMemo));
                        }


                    } else {
                        Toast.makeText(SubRouteWiseChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SubRouteWiseChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.bad_server_request), getResources().getString(R.string.bad_server_response));
            }
        });


    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SubRouteWiseChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SubRouteWiseChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("SubRouteWiseChart")) {
                                        SubRouteWiseChartApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private List<String> fetchReportSummary() {
        int totalSubRoute = 0;
        double totalSubRouteAmount = 0;
        String remarksDetails = "";
        List<String> arrayList = new ArrayList<>();
        for (SubRouteWiseChart_Response.Datum data : list_pickup_inquiry) {
            if (!TextUtils.isEmpty(data.getTotalSeat())) {
                totalSubRoute += Integer.parseInt(data.getTotalSeat());
            }
            totalSubRouteAmount += data.getJMPayableAmount();
            if (!TextUtils.isEmpty(data.getRemarks())) {
                if (TextUtils.isEmpty(remarksDetails)) {
                    remarksDetails = data.getSeatNo() + " : " + data.getRemarks();
                } else {
                    remarksDetails += "\n" + data.getSeatNo() + " : " + data.getRemarks();
                }
            }

        }
        arrayList.add(String.valueOf(totalSubRoute));
        arrayList.add(String.valueOf(totalSubRouteAmount));
        arrayList.add(remarksDetails);
        return arrayList;
    }


}