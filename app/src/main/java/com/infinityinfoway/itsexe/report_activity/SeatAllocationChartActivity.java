package com.infinityinfoway.itsexe.report_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatAllocationChart_Request;
import com.infinityinfoway.itsexe.api.response.SeatAllocationChart_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySeatAllocationChartBinding;
import com.infinityinfoway.itsexe.exe_adapter.SeatAllocationChartAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatAllocationChartActivity extends AppCompatActivity {

    SeatAllocationChartAdapter seatAllocationReportAdapter;
    LinkedHashMap<String, List<SeatAllocationChart_Response.Datum>> Hashmap_allDayMemo;
    List<SeatAllocationChart_Response.Datum> list_allocation;

    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private Dialog dialog_loading_bar;
    private androidx.appcompat.app.AlertDialog alert;
    private long lastClickTime = 0;
    private ITSExeSharedPref getPref;
    private final String api_method = "Get_Report_SeatAllocation";

    private String journeyStartDate, journeyStartTime, journeyCityTime,isSameDay;
    private int cm_CompanyId,bookedByCompanyId, routeId, routeTimeId, fromCity, toCity, arrangementId, cityId;
    private Bundle getRouteData;

    private ActivitySeatAllocationChartBinding binding;
    private String resReportTitle = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySeatAllocationChartBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getIntent().hasExtra("RouteId")) {
            getRouteData = getIntent().getExtras();

            resReportTitle = getRouteData.getString("ReportTitle");
            cm_CompanyId=getRouteData.getInt("Company_Id");
            journeyStartDate = getRouteData.getString("JourneyStartDate");
            routeId = getRouteData.getInt("RouteId");
            routeTimeId = getRouteData.getInt("RouteTimeId");
            fromCity = getRouteData.getInt("FromCity");
            toCity = getRouteData.getInt("ToCity");
            journeyStartTime = getRouteData.getString("JourneyStartTime");
            journeyCityTime = getRouteData.getString("JourneyCityTime");
            arrangementId = getRouteData.getInt("ArrangementId");
            cityId = getRouteData.getInt("CityId");
            isSameDay = getRouteData.getString("IsSameDay");
            bookedByCompanyId = getRouteData.getInt("BookedByCompanyId");

        }

        cd = new ConnectionDetector(SeatAllocationChartActivity.this);
        getPref = new ITSExeSharedPref(SeatAllocationChartActivity.this);


        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.layoutBase.txtTitle.setText(resReportTitle);
        binding.txtReportTitle.setText(binding.layoutBase.txtTitle.getText());
        binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Get_Report_SeatAllocationApiCall();

    }

    private void Get_Report_SeatAllocationApiCall() {
        showProgressDialog("Loading....");


        SeatAllocationChart_Request request = new SeatAllocationChart_Request(
                cm_CompanyId,
                journeyStartDate,
                fromCity,
                toCity,
                routeId,
                routeTimeId,
                journeyStartTime,
                journeyCityTime,
                1,
                arrangementId,
                cityId,
                getPref.getBUM_BranchUserID(),
                bookedByCompanyId,
                isSameDay
        );


        Call<SeatAllocationChart_Response> call = apiService.SeatAllocationChart(request);
        call.enqueue(new Callback<SeatAllocationChart_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatAllocationChart_Response> call, @NotNull Response<SeatAllocationChart_Response> response) {
                disMissDialog();


                if (response != null && response.isSuccessful() && response.body() != null) {
                    list_allocation = new ArrayList<>();
                    Hashmap_allDayMemo = new LinkedHashMap<>();

                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            list_allocation = response.body().getData();

                            for (SeatAllocationChart_Response.Datum pojo : list_allocation) {

                                if (!Hashmap_allDayMemo.containsKey(pojo.getAllocation())) {
                                    List<SeatAllocationChart_Response.Datum> pickDropPassengerDetailPojoList = new ArrayList<>();
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getAllocation(), pickDropPassengerDetailPojoList);
                                } else {
                                    List<SeatAllocationChart_Response.Datum> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getAllocation());
                                    pickDropPassengerDetailPojoList.add(pojo);
                                    Hashmap_allDayMemo.put(pojo.getAllocation(), pickDropPassengerDetailPojoList);
                                }
                            }

                            SeatAllocationChart_Response.Datum data = list_allocation.get(0);

                            binding.txtReportTitle.setPaintFlags(binding.txtReportTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                            binding.txtDate.setText("Date: " + data.getJourneyDate() + " " + data.getStartTime());
                            binding.txtRouteName.setText("Route# " + data.getSubRouteName());
                            binding.txtBusNo.setText("Bus No: " + data.getBMBUSNO());
                            binding.txtPickupMan.setText("Pickup Man: " + data.getPickupManDetail());
                            binding.txtConductor.setText("Conductor: " + data.getConductorDetail());
                            binding.txtDriver.setText("Driver: " + data.getDriverDetail());
                            binding.txtPrintDate.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                            binding.txtPrintBy.setText("Print By:      " + getPref.getBUM_UserName() + " [" + getPref.getBM_BranchCode() + "]");

                            seatAllocationReportAdapter = new SeatAllocationChartAdapter(SeatAllocationChartActivity.this, Hashmap_allDayMemo);
                            binding.rvSeatAllocation.setAdapter(seatAllocationReportAdapter);

                            binding.txtEmptySeaters.setText("Empty Seaters: " + data.getEmptySeat() + "\n" + "(" + data.getTotalEmptySeat() + "):");
                            binding.txtEmptySleeper.setText("Empty Sleeper: " + data.getEmptySleeper() + "\n" + "(" + data.getTotalEmptySleeper() + "):");


                            //StringTokenizer st = new StringTokenizer(list_allocation.get(0).getRemarkDetail(), "*");

                            String[] Remarks = data.getRemarkDetail().split("\\*");

                            String Remarks_String = "";

                            for (int i = 0; i < Remarks.length; i++) {

                                if (i == 0) {
                                    Remarks_String = "Remarks" + "\n" + Remarks[i];
                                } else {
                                    Remarks_String = Remarks_String + "\n" + Remarks[i];
                                }

                            }

                            binding.txtRemarks.setText(Remarks_String);


                        }
                    } else {
                        Toast.makeText(SeatAllocationChartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatAllocationChart_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(api_method, getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });


    }

    private void showInternetConnectionDialog(final String method, String title, String message) {
        try {
            if (alert != null && alert.isShowing()) {
                alert.dismiss();
            }
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SeatAllocationChartActivity.this);
            alertDialogBuilder
                    .setMessage(message)
                    .setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.retry),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                                            return;
                                        }
                                        lastClickTime = SystemClock.elapsedRealtime();
                                    } catch (Exception ignored) {
                                    }

                                    if (method.equals("Get_Report_SeatAllocation")) {
                                        Get_Report_SeatAllocationApiCall();
                                    }
                                }
                            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            alert = alertDialogBuilder.create();
            if (!isFinishing())
                alert.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialog_loading_bar = new Dialog(SeatAllocationChartActivity.this);
            dialog_loading_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading_bar.setCancelable(false);
            dialog_loading_bar.setContentView(R.layout.loading_dialog);
            TextView txt_message_title = dialog_loading_bar.findViewById(R.id.txt_message_title);
            txt_message_title.setText(LoadingMessage);
            if (dialog_loading_bar.getWindow() != null) {
                dialog_loading_bar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_loading_bar.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (!isFinishing())
                    dialog_loading_bar.show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disMissDialog() {
        if (dialog_loading_bar != null && !isFinishing() && dialog_loading_bar.isShowing()) {
            dialog_loading_bar.dismiss();
        }
    }


   /* public class LazyDataConnection_ExeApi extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData_Exe.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {
            if (xmlResponse.equals("")) {
                if (method.equals("Get_BusConnectExe_Report_SeatAllocation")) {
                    try {
                        progDialog.dismiss();
                    } catch (Exception ex) {
                    }
                }
                Intent i = new Intent(SeatAllocationReportActivity.this, InternetErrorActivity.class);
                i.putExtra("caller", "ExeBookingActivity");
                startActivity(i);
                return;
            }
            if (method.equals("Get_BusConnectExe_Report_SeatAllocation")) {

                Get_BusConnectExe_Report_SeatAllocation(xmlResponse, "Table");
                try {
                    progDialog.dismiss();
                } catch (Exception ex) {
                }

                Hashmap_allDayMemo = new LinkedHashMap<>();

                for (SeatAllocationReportBean pojo : list_allocation) {

                    if (!Hashmap_allDayMemo.containsKey(pojo.getAllocation())) {
                        List<SeatAllocationReportBean> pickDropPassengerDetailPojoList = new ArrayList<>();
                        pickDropPassengerDetailPojoList.add(pojo);
                        Hashmap_allDayMemo.put(pojo.getAllocation(), pickDropPassengerDetailPojoList);
                    } else {
                        List<SeatAllocationReportBean> pickDropPassengerDetailPojoList = Hashmap_allDayMemo.get(pojo.getAllocation());
                        pickDropPassengerDetailPojoList.add(pojo);
                        Hashmap_allDayMemo.put(pojo.getAllocation(), pickDropPassengerDetailPojoList);
                    }
                }

                if (list_allocation != null && list_allocation.size() > 0) {

                    txt_empty_Seaters.setText("Empty Seaters: " + list_allocation.get(0).getEmptySeat() + "\n" + "(" + list_allocation.get(0).getTotalEmptySeat() + "):");
                    txt_empty_Sleeper.setText("Empty Sleeper: " + list_allocation.get(0).getEmptySleeper() + "\n" + "(" + list_allocation.get(0).getTotalEmptySleeper() + "):");


                    txt_date.setText("Date: " + list_allocation.get(0).getJourneyDate() + "   " + list_allocation.get(0).getStartTime());
                    txt_route_name.setText("Route# " + list_allocation.get(0).getRouteName());
                    txt_print_date.setText("Print Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm aa").format(Calendar.getInstance().getTime()));
                    txt_driver.setText("Driver: " + list_allocation.get(0).getDriverDetail());

                    //StringTokenizer st = new StringTokenizer(list_allocation.get(0).getRemarkDetail(), "*");

                    String[] Remarks = list_allocation.get(0).getRemarkDetail().split("\\*");

                    String Remarks_String = "";

                    for (int i = 0; i < Remarks.length; i++) {

                        if (i == 0) {
                            Remarks_String = "Remarks" + "\n" + Remarks[i];
                        } else {
                            Remarks_String = Remarks_String + "\n" + Remarks[i];
                        }

                    }

                    txt_remarks.setText("Remarks" + Remarks_String);


                    seatAllocationReportAdapter = new SeatAllocationReportAdapter(SeatAllocationReportActivity.this, Hashmap_allDayMemo);
                    recycler_allocation_wise_chart.setAdapter(seatAllocationReportAdapter);
                } else {
                    Toast.makeText(SeatAllocationReportActivity.this, "No Data Found'", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }*/

    /*public void Get_BusConnectExe_Report_SeatAllocation(String str_RespXML, String str_TagName) {
        Document doc = getData_Exe.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        list_allocation = new ArrayList<>();
        for (int i = 0; i <= nodes.getLength(); i++) {
            Node e1 = nodes.item(i);
            Element el = (Element) e1;

            if (el != null) {
                SeatAllocationReportBean allDayMemoReportBean = new SeatAllocationReportBean();

                try {
                    allDayMemoReportBean.setSubRouteName(el.getElementsByTagName("SubRouteName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTotalSeat(el.getElementsByTagName("TotalSeat").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setSeatNo(el.getElementsByTagName("SeatNo").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setAllocation(el.getElementsByTagName("Allocation").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRouteName(el.getElementsByTagName("RouteName").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setJourneyDate(el.getElementsByTagName("JourneyDate").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setStartTime(el.getElementsByTagName("StartTime").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setByWhom(el.getElementsByTagName("ByWhom").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setEmptySeat(el.getElementsByTagName("EmptySeat").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTotalEmptySeat(el.getElementsByTagName("TotalEmptySeat").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setEmptySleeper(el.getElementsByTagName("EmptySleeper").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setTotalEmptySleeper(el.getElementsByTagName("TotalEmptySleeper").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setRemarkDetail(el.getElementsByTagName("RemarkDetail").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setBM_BUSNO(el.getElementsByTagName("BM_BUSNO").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setDriverDetail(el.getElementsByTagName("DriverDetail").item(0).getTextContent());
                } catch (Exception e) {
                }
                try {
                    allDayMemoReportBean.setPickupManDetail(el.getElementsByTagName("PickupManDetail").item(0).getTextContent());
                } catch (Exception e) {
                }


                list_allocation.add(allDayMemoReportBean);
            }
        }
    }*/


}
