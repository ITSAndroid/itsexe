package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.FetchDetailsByPNRNO_Response;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.databinding.ItemSeatModificationPopupBinding;

import java.util.List;

public class SeatModificationListAdapter extends RecyclerView.Adapter<SeatModificationListAdapter.MyViewHolder> {

    private List<FetchDetailsByPNRNO_Response.Datum> listSeatDetails;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(FetchDetailsByPNRNO_Response.Datum item, int position);
    }

    public SeatModificationListAdapter(Context context, List<FetchDetailsByPNRNO_Response.Datum> listSeatDetails, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.listSeatDetails = listSeatDetails;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemSeatModificationPopupBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.binding.chkSeat.setText(listSeatDetails.get(position).getBADSeatNo());
        holder.binding.chkSeat.setChecked(listSeatDetails.get(position).getRadioButton() != 0 && listSeatDetails.get(position).getRadioButton() == 1);



        if (holder.binding.chkSeat.isChecked()) {
            holder.binding.chkSeat.setTextColor(context.getResources().getColor(R.color.bg_button_light));
        } else {
            holder.binding.chkSeat.setTextColor(context.getResources().getColor(R.color.trans_success_stroke_color));
        }

        holder.binding.chkSeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.binding.chkSeat.isChecked()) {
                    listSeatDetails.get(position).setRadioButton(1);
                    holder.binding.chkSeat.setTextColor(context.getResources().getColor(R.color.bg_button_light));
                } else {
                    listSeatDetails.get(position).setRadioButton(0);
                    holder.binding.chkSeat.setTextColor(context.getResources().getColor(R.color.trans_success_stroke_color));
                }
                onItemClickListener.onItemClick(listSeatDetails.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listSeatDetails.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemSeatModificationPopupBinding binding;

        public MyViewHolder(ItemSeatModificationPopupBinding popupBinding) {
            super(popupBinding.getRoot());
            this.binding = popupBinding;
        }
    }

}
