package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.infinityinfoway.itsexe.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DateSelectionAdapter extends ArrayAdapter<String> {
    private List<String> reportTypeList;
    private Context context;

    public DateSelectionAdapter(@NonNull Context context, List<String> reportTypeList) {
        super(context, R.layout.spinner_item, reportTypeList);
        this.context = context;
        this.reportTypeList = reportTypeList;
    }

    @Override
    public int getCount() {
        return reportTypeList.size();
    }

    @Override
    public String getItem(int position) {
        return reportTypeList.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(reportTypeList.get(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(reportTypeList.get(position));

        return view;

    }


}
