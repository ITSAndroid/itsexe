package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_ReportType_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ReportTypeListAdapter extends ArrayAdapter<Get_ReportType_Response.ReportType> {
    private List<Get_ReportType_Response.ReportType> reportTypeList;
    private Context context;

    public ReportTypeListAdapter(@NonNull Context context, List<Get_ReportType_Response.ReportType> reportTypeList) {
        super(context, R.layout.autocomplete_items, reportTypeList);
        this.context = context;
        this.reportTypeList = reportTypeList;
    }

    @Override
    public int getCount() {
        return reportTypeList.size();
    }

    @Override
    public Get_ReportType_Response.ReportType getItem(int position) {
        return reportTypeList.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        Get_ReportType_Response.ReportType data = reportTypeList.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getReportName());

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        Get_ReportType_Response.ReportType data = reportTypeList.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getReportName());

        return view;

    }


}
