package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.database.model.ITSGetAllAgentByCompanyID_Model;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AgentBookingTypeCityAdapter extends ArrayAdapter<ITSGetAllAgentByCompanyID_Model> {

    private Context context;
    int IsAgentCity;
    private List<ITSGetAllAgentByCompanyID_Model> itsGetAllAgentByCompanyIDList;

    public AgentBookingTypeCityAdapter(Context context, List<ITSGetAllAgentByCompanyID_Model> itsGetAllAgentByCompanyIDList, int IsAgentCity) {
        super(context, R.layout.autocomplete_items, itsGetAllAgentByCompanyIDList);
        this.context = context;
        this.itsGetAllAgentByCompanyIDList = itsGetAllAgentByCompanyIDList;
        this.IsAgentCity=IsAgentCity;
    }

    @Override
    public int getCount() {
        return itsGetAllAgentByCompanyIDList.size();
    }

    @Override
    public ITSGetAllAgentByCompanyID_Model getItem(int position) {
        return itsGetAllAgentByCompanyIDList.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        ITSGetAllAgentByCompanyID_Model data = itsGetAllAgentByCompanyIDList.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

        if (IsAgentCity == 1) {
            tv_spinnerItems.setText(data.getCM_CityName());
        } else {
            tv_spinnerItems.setText(data.getAM_AgentName());
        }
        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);

        ITSGetAllAgentByCompanyID_Model data = itsGetAllAgentByCompanyIDList.get(position);

        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


        if (IsAgentCity == 1) {
            tv_spinnerItems.setText(data.getCM_CityName());
        } else {
            tv_spinnerItems.setText(data.getAM_AgentName());
        }

        return rowView;
    }

}
