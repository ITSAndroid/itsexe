package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_BranchList_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BranchListAdapter extends ArrayAdapter<Get_BranchList_Response.BranchDetail> {
    private List<Get_BranchList_Response.BranchDetail> list;
    private Context context;

    public BranchListAdapter(Context context, List<Get_BranchList_Response.BranchDetail> list) {
        super(context, R.layout.spinner_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Get_BranchList_Response.BranchDetail getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        Get_BranchList_Response.BranchDetail data = list.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getBMBranchName());

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        Get_BranchList_Response.BranchDetail data = list.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getBMBranchName());

        return view;

    }
}
