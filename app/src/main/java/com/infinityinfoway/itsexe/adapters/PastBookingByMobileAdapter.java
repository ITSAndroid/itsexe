package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.Get_Searching_PastBookingDataByPhone_Response;
import com.infinityinfoway.itsexe.databinding.ItemPastBookingByMobileNoBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class PastBookingByMobileAdapter extends RecyclerView.Adapter<PastBookingByMobileAdapter.MyViewHolder> {

    private Context context;
    private List<Get_Searching_PastBookingDataByPhone_Response.SearchingPastBookingDataByPhone> list;
    private SimpleDateFormat sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
    private SimpleDateFormat sdf_search_pnr = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", java.util.Locale.getDefault());

    public PastBookingByMobileAdapter(Context context, List<Get_Searching_PastBookingDataByPhone_Response.SearchingPastBookingDataByPhone> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPastBookingByMobileNoBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Get_Searching_PastBookingDataByPhone_Response.SearchingPastBookingDataByPhone data=list.get(position);
        holder.binding.txtSrNo.setText(String.valueOf(position+1));
        holder.binding.txtPnr.setText(data.getPNRNO());
        holder.binding.txtCityTime.setText(data.getCityTime());
        holder.binding.txtFrom.setText(data.getFromCityName()+" To "+data.getToCityName());
        holder.binding.txtName.setText(data.getPassengerName());
        holder.binding.txtBookingType.setText(data.getDefaultName());
        try {
            holder.binding.txtJdate.setText(sdf_full.format(sdf_search_pnr.parse(data.getJourneyDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.binding.txtMainRouteName.setText(data.getRouteNameDisplay());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private ItemPastBookingByMobileNoBinding binding;
        public MyViewHolder(ItemPastBookingByMobileNoBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
