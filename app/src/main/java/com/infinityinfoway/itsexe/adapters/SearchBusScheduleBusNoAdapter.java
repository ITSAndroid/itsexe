package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.Get_Searching_BusSchedule_By_BusNo_Response;
import com.infinityinfoway.itsexe.databinding.ItemScheduleByBusNoBinding;

import java.util.List;

public class SearchBusScheduleBusNoAdapter extends RecyclerView.Adapter<SearchBusScheduleBusNoAdapter.MyViewHolder> {
    private Context context;
    private List<Get_Searching_BusSchedule_By_BusNo_Response.SearchingBusScheduleByBusNo> list;

    public SearchBusScheduleBusNoAdapter(Context context, List<Get_Searching_BusSchedule_By_BusNo_Response.SearchingBusScheduleByBusNo> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemScheduleByBusNoBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Get_Searching_BusSchedule_By_BusNo_Response.SearchingBusScheduleByBusNo data = list.get(position);
        holder.binding.txtBusNo.setText(data.getBMBusNo());
        holder.binding.txtRouteName.setText(data.getRTRouteName());
        holder.binding.txtRouteTime.setText(data.getRouteTime());
        holder.binding.txtDriver1.setText(data.getDMDriver1());
        holder.binding.txtDriver2.setText(data.getDMDriver2());
        holder.binding.txtPhoneNo.setText(data.getDMPhoneNo());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemScheduleByBusNoBinding binding;

        public MyViewHolder(ItemScheduleByBusNoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
