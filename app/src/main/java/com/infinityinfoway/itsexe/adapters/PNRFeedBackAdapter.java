package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PNRFeedBack_Response;
import com.infinityinfoway.itsexe.databinding.ItemPnrFeedbackBinding;

import java.util.List;

public class PNRFeedBackAdapter extends RecyclerView.Adapter<PNRFeedBackAdapter.MyViewHolder> {

    private Context context;
    private List<PNRFeedBack_Response.Datum> list;

    public PNRFeedBackAdapter(Context context, List<PNRFeedBack_Response.Datum> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPnrFeedbackBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.txtPnrDetails.setText(list.get(position).getFeedBack());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemPnrFeedbackBinding binding;

        public MyViewHolder(ItemPnrFeedbackBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
