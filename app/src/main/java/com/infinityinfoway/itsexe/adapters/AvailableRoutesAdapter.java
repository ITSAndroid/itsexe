package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class AvailableRoutesAdapter extends ArrayAdapter<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> {
    private Context context;
    private List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listITSBusSchedule;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSBusTypeColorCode> busTypeColorCodeList;

    public AvailableRoutesAdapter(Context context, int resource, List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listITSBusSchedule, List<Get_API_EXE_RouteTimeALLXML_Response.ITSBusTypeColorCode> busTypeColorCodeList) {
        super(context, R.layout.spinner_item, listITSBusSchedule);
        this.context = context;
        this.listITSBusSchedule = listITSBusSchedule;
        this.busTypeColorCodeList = busTypeColorCodeList;
    }

    @Override
    public int getCount() {
        return listITSBusSchedule.size();
    }

    @Override
    public Get_API_EXE_RouteTimeXML_Response.RouteTimeXML getItem(int position) {
        return listITSBusSchedule.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        Get_API_EXE_RouteTimeXML_Response.RouteTimeXML data = listITSBusSchedule.get(position);

        String parseColor = fetchColorCode(data.getBTMBusTypeID());

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

        if (!TextUtils.isEmpty(parseColor)) {
            tv_spinnerItems.setTextColor(Color.parseColor(parseColor));
        } else if (data.getBusFilter().trim().contains("NonA/C") || data.getBusFilter().trim().contains("Non A/C")) {
            tv_spinnerItems.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            tv_spinnerItems.setTextColor(context.getResources().getColor(R.color.bg_button_light));
        }

        tv_spinnerItems.setText("  " + data.getStartTime() + " : " + data.getRTCount() + " # " + data.getRMRouteNameDisplay());

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);

        Get_API_EXE_RouteTimeXML_Response.RouteTimeXML data = listITSBusSchedule.get(position);

        String parseColor = fetchColorCode(data.getBTMBusTypeID());

        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);



        if (!TextUtils.isEmpty(parseColor)) {
            tv_spinnerItems.setTextColor(Color.parseColor(parseColor));
        } else if (data.getBusFilter().trim().contains("NonA/C") || data.getBusFilter().trim().contains("Non A/C")) {
            tv_spinnerItems.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            tv_spinnerItems.setTextColor(context.getResources().getColor(R.color.bg_button_light));
        }

        tv_spinnerItems.setText(data.getStartTime() + " : " + data.getRTCount() + " # " + data.getRMRouteNameDisplay());
        return rowView;
    }

    private String fetchColorCode(String BusTypeId) {
        String parseColor = "";
        for (Get_API_EXE_RouteTimeALLXML_Response.ITSBusTypeColorCode data : busTypeColorCodeList) {
            if (String.valueOf(data.getBTMBusTypeID()).equals(BusTypeId)) {
                parseColor = data.getBTMRouteNameDisplayColor();
                break;
            }
        }
        return parseColor;
    }
}
