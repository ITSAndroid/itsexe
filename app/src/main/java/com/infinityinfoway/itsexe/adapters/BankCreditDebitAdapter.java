package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;

import org.jetbrains.annotations.NotNull;

public class BankCreditDebitAdapter extends ArrayAdapter<String> {

    private Context context;
    private String[] bankCardCreditDebit;

    public BankCreditDebitAdapter(Context context, String[] bankCardCreditDebit) {
        super(context, R.layout.autocomplete_items, bankCardCreditDebit);
        this.context = context;
        this.bankCardCreditDebit = bankCardCreditDebit;
    }

    @Override
    public int getCount() {
        return bankCardCreditDebit.length;
    }

    @Override
    public String getItem(int position) {
        return bankCardCreditDebit[position];
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

        tv_spinnerItems.setText(bankCardCreditDebit[position]);
        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);


        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


        tv_spinnerItems.setText(bankCardCreditDebit[position]);

        return rowView;
    }


}
