package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.database.model.ITSBranchMasterSelectXML_Model;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BranchNameBookingTypeAdapter extends ArrayAdapter<ITSBranchMasterSelectXML_Model> {
    private Context context;
    private List<ITSBranchMasterSelectXML_Model> itsGetAllAgentByCompanyIDList;

    public BranchNameBookingTypeAdapter(Context context, List<ITSBranchMasterSelectXML_Model> itsGetAllAgentByCompanyIDList) {
        super(context, R.layout.autocomplete_items, itsGetAllAgentByCompanyIDList);
        this.context = context;
        this.itsGetAllAgentByCompanyIDList = itsGetAllAgentByCompanyIDList;
    }

    @Override
    public int getCount() {
        return itsGetAllAgentByCompanyIDList.size();
    }

    @Override
    public ITSBranchMasterSelectXML_Model getItem(int position) {
        return itsGetAllAgentByCompanyIDList.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        ITSBranchMasterSelectXML_Model data = itsGetAllAgentByCompanyIDList.get(position);

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

        tv_spinnerItems.setText(data.getBM_BranchName());
        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);

        ITSBranchMasterSelectXML_Model data = itsGetAllAgentByCompanyIDList.get(position);

        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


        tv_spinnerItems.setText(data.getBM_BranchName());

        return rowView;
    }
}
