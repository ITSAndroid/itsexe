package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PNRBookingLog_Response;
import com.infinityinfoway.itsexe.databinding.ItemPnrBookingLogBinding;

import java.util.List;

public class PNRBookingLogAdapter extends RecyclerView.Adapter<PNRBookingLogAdapter.MyViewHolder> {

    private Context context;
    private List<PNRBookingLog_Response.Datum> list;

    public PNRBookingLogAdapter(Context context, List<PNRBookingLog_Response.Datum> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPnrBookingLogBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.txtPnrDetails.setText(list.get(position).getPNRDetails());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemPnrBookingLogBinding binding;
        public MyViewHolder(ItemPnrBookingLogBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
