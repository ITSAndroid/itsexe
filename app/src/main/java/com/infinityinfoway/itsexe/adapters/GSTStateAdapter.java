package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class GSTStateAdapter extends ArrayAdapter<Get_API_EXE_RouteTimeALLXML_Response.ITSState> {
    private Context context;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSState> listITSBusSchedule;

    public GSTStateAdapter(Context context, List<Get_API_EXE_RouteTimeALLXML_Response.ITSState> listITSBusSchedule) {
        super(context, R.layout.autocomplete_items, listITSBusSchedule);
        this.context = context;
        this.listITSBusSchedule = listITSBusSchedule;
    }

    @Override
    public int getCount() {
        return listITSBusSchedule.size();
    }

    @Override
    public Get_API_EXE_RouteTimeALLXML_Response.ITSState getItem(int position) {
        return listITSBusSchedule.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        Get_API_EXE_RouteTimeALLXML_Response.ITSState data = listITSBusSchedule.get(position);

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getSMStateName());

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);
        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);
        Get_API_EXE_RouteTimeALLXML_Response.ITSState data = listITSBusSchedule.get(position);
        tv_spinnerItems.setText(data.getSMStateName());
        return rowView;
    }
}
