package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.Get_CompanyList_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CompanyListAdapter extends ArrayAdapter<Get_CompanyList_Response.CityDetail> {
    private List<Get_CompanyList_Response.CityDetail> list;
    private Context context;

    public CompanyListAdapter(Context context, List<Get_CompanyList_Response.CityDetail> list) {
        super(context, R.layout.spinner_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Get_CompanyList_Response.CityDetail getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        Get_CompanyList_Response.CityDetail data = list.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getCMCompanyName());

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        Get_CompanyList_Response.CityDetail data = list.get(position);


        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getCMCompanyName());

        return view;

    }

}

