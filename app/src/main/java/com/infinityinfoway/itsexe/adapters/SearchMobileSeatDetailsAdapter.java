package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.Get_SearchingMobileNo_Response;
import com.infinityinfoway.itsexe.databinding.ItemSearchingMobileNoBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class SearchMobileSeatDetailsAdapter extends RecyclerView.Adapter<SearchMobileSeatDetailsAdapter.MyViewHolder> {

    private Context context;
    private OnItemClickListener onItemClickListener;
    private List<Get_SearchingMobileNo_Response.SearchingMobileNo> list;
    private SimpleDateFormat sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
    private SimpleDateFormat sdf_search_pnr = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", java.util.Locale.getDefault());

    public interface OnItemClickListener {
        void onItemClick(Get_SearchingMobileNo_Response.SearchingMobileNo item, int position, int flag);
    }

    public SearchMobileSeatDetailsAdapter(Context context, List<Get_SearchingMobileNo_Response.SearchingMobileNo> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemSearchingMobileNoBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Get_SearchingMobileNo_Response.SearchingMobileNo data = list.get(position);

        holder.bind(data, onItemClickListener, position);

        holder.binding.txtSrNo.setText(String.valueOf(position + 1));
        holder.binding.txtCancel.setPaintFlags(holder.binding.txtCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtLoad.setPaintFlags(holder.binding.txtLoad.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtHold.setPaintFlags(holder.binding.txtHold.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtPnr.setText(String.valueOf(data.getPNRNO()));
        holder.binding.txtCityTime.setText(data.getCityTime());

        holder.binding.txtFrom.setText(data.getFromCityName() + " To " + data.getToCityName());
        holder.binding.txtName.setText(data.getPassengerName());
        holder.binding.txtSeats.setText(data.getSeatNo());
        holder.binding.txtBookingOn.setText(data.getBookingDate());
        try {
            holder.binding.txtJdate.setText(sdf_full.format(sdf_search_pnr.parse(data.getJourneyDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.binding.txtMainRouteName.setText(data.getRouteNameDisplay());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemSearchingMobileNoBinding binding;

        public MyViewHolder(ItemSearchingMobileNoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(final Get_SearchingMobileNo_Response.SearchingMobileNo item, final OnItemClickListener listener, final int position) {

            binding.txtLoad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position, 0);
                }
            });

            binding.txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position, 1);
                }
            });

            binding.txtHold.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position, 2);
                }
            });
        }

    }
}
