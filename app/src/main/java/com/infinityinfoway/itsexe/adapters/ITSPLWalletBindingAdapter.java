package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.database.model.ITSOnlineWallet_Model;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ITSPLWalletBindingAdapter extends ArrayAdapter<ITSOnlineWallet_Model> {
    private Context context;
    private List<ITSOnlineWallet_Model> itsOnlineWalletModelList;

    public ITSPLWalletBindingAdapter(Context context, List<ITSOnlineWallet_Model> itsOnlineWalletModelList) {
        super(context, R.layout.autocomplete_items, itsOnlineWalletModelList);
        this.context = context;
        this.itsOnlineWalletModelList = itsOnlineWalletModelList;
    }

    @Override
    public int getCount() {
        return itsOnlineWalletModelList.size();
    }

    @Override
    public ITSOnlineWallet_Model getItem(int position) {
        return itsOnlineWalletModelList.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        ITSOnlineWallet_Model data = itsOnlineWalletModelList.get(position);

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);

        tv_spinnerItems.setText(data.getWCM_CompanyName());
        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);

        ITSOnlineWallet_Model data = itsOnlineWalletModelList.get(position);

        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);


        tv_spinnerItems.setText(data.getWCM_CompanyName());

        return rowView;
    }

}
