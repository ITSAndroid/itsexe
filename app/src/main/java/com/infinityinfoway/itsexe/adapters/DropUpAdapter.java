package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.response.PickupDrop_Response;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DropUpAdapter extends ArrayAdapter<PickupDrop_Response.Datum> {

    private Context context;
    private List<PickupDrop_Response.Datum> listITSBusSchedule;

    public DropUpAdapter(Context context, List<PickupDrop_Response.Datum> listITSBusSchedule) {
        super(context, R.layout.autocomplete_items, listITSBusSchedule);
        this.context = context;
        this.listITSBusSchedule = listITSBusSchedule;
    }

    @Override
    public int getCount() {
        return listITSBusSchedule.size();
    }

    @Override
    public PickupDrop_Response.Datum getItem(int position) {
        return listITSBusSchedule.get(position);
    }

    @Override
    public View getDropDownView(int position, View view,
                                @NotNull ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.autocomplete_items, parent, false);
        }

        PickupDrop_Response.Datum data = listITSBusSchedule.get(position);

        TextView tv_spinnerItems = view.findViewById(R.id.tv_spinnerItems);
        tv_spinnerItems.setText(data.getPickUPTime());

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spinner_item, parent, false);
        TextView tv_spinnerItems = rowView.findViewById(R.id.tv_spinnerItems);
        PickupDrop_Response.Datum data = listITSBusSchedule.get(position);
        tv_spinnerItems.setText(data.getPickUPTime());
        return rowView;
    }

}
