package com.infinityinfoway.itsexe.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infinityinfoway.itsexe.api.response.PhoneTicketCount_Response;
import com.infinityinfoway.itsexe.databinding.ItemPhoneTicketCountBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class PhoneTicketCountAdapter extends RecyclerView.Adapter<PhoneTicketCountAdapter.MyViewHolder> {

    private Context context;
    private OnItemClickListener onItemClickListener;
    private List<PhoneTicketCount_Response.Datum> list;
    private SimpleDateFormat sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
    private SimpleDateFormat sdf_search_pnr = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", java.util.Locale.getDefault());

    public interface OnItemClickListener {
        void onItemClick(PhoneTicketCount_Response.Datum item, int position, int flag);
    }

    public PhoneTicketCountAdapter(Context context, List<PhoneTicketCount_Response.Datum> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPhoneTicketCountBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        PhoneTicketCount_Response.Datum data = list.get(position);

        holder.bind(data, onItemClickListener, position);

        holder.binding.txtSrNo.setText(String.valueOf(position + 1));
        holder.binding.txtCancel.setPaintFlags(holder.binding.txtCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtLoad.setPaintFlags(holder.binding.txtLoad.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtHold.setPaintFlags(holder.binding.txtHold.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.txtPnr.setText(String.valueOf(data.getPnrno()));
        holder.binding.txtCityTime.setText(data.getCityTime());

        holder.binding.txtFrom.setText(data.getFromCityName() + " To " + data.getToCityName());
        holder.binding.txtName.setText(data.getPassengerName());
        holder.binding.txtSeats.setText(data.getSeatNo());
        holder.binding.txtMobile.setText(data.getPhoneNo());
        holder.binding.txtBookingOn.setText(data.getBookingDate());
        try {
            holder.binding.txtJdate.setText(sdf_full.format(sdf_search_pnr.parse(data.getJourneyDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.binding.txtMainRouteName.setText(data.getRouteNameDisplay());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemPhoneTicketCountBinding binding;

        public MyViewHolder(ItemPhoneTicketCountBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(final PhoneTicketCount_Response.Datum item, final OnItemClickListener listener, final int position) {

            binding.txtLoad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position, 0);
                }
            });

            binding.txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position, 1);
                }
            });

            binding.txtHold.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position, 2);
                }
            });


        }

    }
}
