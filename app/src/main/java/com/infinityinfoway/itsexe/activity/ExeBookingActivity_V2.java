package com.infinityinfoway.itsexe.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.BuildConfig;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.adapters.AgentBookingTypeCityAdapter;
import com.infinityinfoway.itsexe.adapters.AvailableRoutesAdapter;
import com.infinityinfoway.itsexe.adapters.BankCreditDebitAdapter;
import com.infinityinfoway.itsexe.adapters.BookingTypeAdapter;
import com.infinityinfoway.itsexe.adapters.BranchNameBookingTypeAdapter;
import com.infinityinfoway.itsexe.adapters.BranchUserByBranchMasterBookingTypeAdapter;
import com.infinityinfoway.itsexe.adapters.CancelRemarksAdapter;
import com.infinityinfoway.itsexe.adapters.DateSelectionAdapter;
import com.infinityinfoway.itsexe.adapters.GSTStateAdapter;
import com.infinityinfoway.itsexe.adapters.GuestBookingTypeAdapter;
import com.infinityinfoway.itsexe.adapters.ITSPLWalletBindingAdapter;
import com.infinityinfoway.itsexe.adapters.PNRBookingLogAdapter;
import com.infinityinfoway.itsexe.adapters.PNRFeedBackAdapter;
import com.infinityinfoway.itsexe.adapters.PNRSearchAdapter;
import com.infinityinfoway.itsexe.adapters.PastBookingByMobileAdapter;
import com.infinityinfoway.itsexe.adapters.PhoneTicketCountAdapter;
import com.infinityinfoway.itsexe.adapters.PickUpAdapter;
import com.infinityinfoway.itsexe.adapters.ReportTypeListAdapter;
import com.infinityinfoway.itsexe.adapters.Ret_Net_Adapter;
import com.infinityinfoway.itsexe.adapters.SearchBusScheduleBusNoAdapter;
import com.infinityinfoway.itsexe.adapters.SearchMobileSeatDetailsAdapter;
import com.infinityinfoway.itsexe.adapters.SeatModificationListAdapter;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.BookingUpdate_Request;
import com.infinityinfoway.itsexe.api.request.CompanyCardDetails_Request;
import com.infinityinfoway.itsexe.api.request.CouponCodeDetails_Request;
import com.infinityinfoway.itsexe.api.request.ESendGSTInvoice_Request;
import com.infinityinfoway.itsexe.api.request.ESendTicketRequest;
import com.infinityinfoway.itsexe.api.request.FetchDetailsByPNRNO_Request;
import com.infinityinfoway.itsexe.api.request.GetCancellationDetails_ConfirmCancellation_Request;
import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeALLXML_Request;
import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeXML_Request;
import com.infinityinfoway.itsexe.api.request.Get_ReportType_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingCancelTkTPrint_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingMobileNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_SearchingSeatNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_BusSchedule_By_BusNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_GPSLastLocationByBusNo_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_PastBookingDataByPhone_Request;
import com.infinityinfoway.itsexe.api.request.Get_Searching_PrintGSTInvoice_Request;
import com.infinityinfoway.itsexe.api.request.ITSPLWalletGenerateOTP_Request;
import com.infinityinfoway.itsexe.api.request.InsertAgentQuotaBookingCancle_Request;
import com.infinityinfoway.itsexe.api.request.InsertBooking_Request;
import com.infinityinfoway.itsexe.api.request.NonReported_Request;
import com.infinityinfoway.itsexe.api.request.PNRBookingLog_Request;
import com.infinityinfoway.itsexe.api.request.PNRFeedBack_Request;
import com.infinityinfoway.itsexe.api.request.PhoneTicketCount_Request;
import com.infinityinfoway.itsexe.api.request.PickupDrop_Request;
import com.infinityinfoway.itsexe.api.request.ReleaseStopBooking_Request;
import com.infinityinfoway.itsexe.api.request.Reported_Request;
import com.infinityinfoway.itsexe.api.request.SeatArrangement_Request;
import com.infinityinfoway.itsexe.api.request.SeatHold_Request;
import com.infinityinfoway.itsexe.api.request.SendOTP_Request;
import com.infinityinfoway.itsexe.api.request.SendSMS_Request;
import com.infinityinfoway.itsexe.api.request.StopBooking_Request;
import com.infinityinfoway.itsexe.api.request.TicketPrintPDF_Request;
import com.infinityinfoway.itsexe.api.request.WaitingReport_Request;
import com.infinityinfoway.itsexe.api.response.BookingUpdate_Response;
import com.infinityinfoway.itsexe.api.response.CompanyCardDetails_Response;
import com.infinityinfoway.itsexe.api.response.CouponCodeDetails_Response;
import com.infinityinfoway.itsexe.api.response.ESendGSTInvoice_Response;
import com.infinityinfoway.itsexe.api.response.ESendTicketResponse;
import com.infinityinfoway.itsexe.api.response.FetchDetailsByPNRNO_Response;
import com.infinityinfoway.itsexe.api.response.GetCancellationDetails_ConfirmCancellation_Response;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;
import com.infinityinfoway.itsexe.api.response.Get_ReportType_Response;
import com.infinityinfoway.itsexe.api.response.Get_Route_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingCancelTkTPrint_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingMobileNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_SearchingSeatNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_BusSchedule_By_BusNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_GPSLastLocationByBusNo_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_PastBookingDataByPhone_Response;
import com.infinityinfoway.itsexe.api.response.Get_Searching_PrintGSTInvoice_Response;
import com.infinityinfoway.itsexe.api.response.ITSPLWalletGenerateOTP_Response;
import com.infinityinfoway.itsexe.api.response.InsertAgentQuotaBookingCancle_Response;
import com.infinityinfoway.itsexe.api.response.InsertBooking_Response;
import com.infinityinfoway.itsexe.api.response.NonReported_Response;
import com.infinityinfoway.itsexe.api.response.PNRBookingLog_Response;
import com.infinityinfoway.itsexe.api.response.PNRFeedBack_Response;
import com.infinityinfoway.itsexe.api.response.PhoneTicketCount_Response;
import com.infinityinfoway.itsexe.api.response.PickupDrop_Response;
import com.infinityinfoway.itsexe.api.response.ReleaseStopBooking_Response;
import com.infinityinfoway.itsexe.api.response.Reported_Response;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.api.response.SeatHold_Response;
import com.infinityinfoway.itsexe.api.response.SendOTP_Response;
import com.infinityinfoway.itsexe.api.response.SendSMS_Response;
import com.infinityinfoway.itsexe.api.response.StopBooking_Response;
import com.infinityinfoway.itsexe.api.response.TicketPrintPDF_Response;
import com.infinityinfoway.itsexe.api.response.WaitingReport_Response;
import com.infinityinfoway.itsexe.bean.Api_List_Status;
import com.infinityinfoway.itsexe.bean.BookingTypeSeatCounterBean;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.bean.NetworkModel;
import com.infinityinfoway.itsexe.bean.SelectedSeatDetailsBean;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.custom_dialog.OptAnimationLoader;
import com.infinityinfoway.itsexe.custom_dialog.SweetAlertDialog;
import com.infinityinfoway.itsexe.database.AppDatabase;
import com.infinityinfoway.itsexe.database.AppExecutors;
import com.infinityinfoway.itsexe.database.DatabaseClient;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.database.model.ITSBranchMasterSelectXML_Model;
import com.infinityinfoway.itsexe.database.model.ITSDoNotAllowFareChangeInBelowRouteTime_Model;
import com.infinityinfoway.itsexe.database.model.ITSFetchChartForeColor_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetAllAgentByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetBranchUserByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetCompanyMarquee_Model;
import com.infinityinfoway.itsexe.database.model.ITSGetGuestTypeByCompanyID_Model;
import com.infinityinfoway.itsexe.database.model.ITSOnlineWallet_Model;
import com.infinityinfoway.itsexe.databinding.ActivityExeBookingV2Binding;
import com.infinityinfoway.itsexe.databinding.DialogBookingTypeWiseSeatDetailsBinding;
import com.infinityinfoway.itsexe.databinding.DialogBusScheduleByBusNoBinding;
import com.infinityinfoway.itsexe.databinding.DialogCancellationDetailsBinding;
import com.infinityinfoway.itsexe.databinding.DialogChartSmsBinding;
import com.infinityinfoway.itsexe.databinding.DialogCustomCalendarBinding;
import com.infinityinfoway.itsexe.databinding.DialogDriverPickupmanDetailsBinding;
import com.infinityinfoway.itsexe.databinding.DialogPastBookingByMobileNoBinding;
import com.infinityinfoway.itsexe.databinding.DialogPhoneBookingCancelTimeBinding;
import com.infinityinfoway.itsexe.databinding.DialogPhoneTicketCountBinding;
import com.infinityinfoway.itsexe.databinding.DialogPnrBookingLogBinding;
import com.infinityinfoway.itsexe.databinding.DialogPnrDetailsBinding;
import com.infinityinfoway.itsexe.databinding.DialogSearchingMobileNoBinding;
import com.infinityinfoway.itsexe.databinding.DialogSeatDetailsModificationPoupBinding;
import com.infinityinfoway.itsexe.databinding.DialogStopBookingBinding;
import com.infinityinfoway.itsexe.databinding.DialogWaitingListReportBinding;
import com.infinityinfoway.itsexe.databinding.LayoutBottomSheetInternetConnectionBinding;
import com.infinityinfoway.itsexe.databinding.LayoutDialogGstBinding;
import com.infinityinfoway.itsexe.databinding.LayoutFareDisplayBinding;
import com.infinityinfoway.itsexe.databinding.LayoutSeatModificationPopupV2Binding;
import com.infinityinfoway.itsexe.exe_adapter.BookTicketPassengerListAdapter;
import com.infinityinfoway.itsexe.exe_adapter.BookingTypeDialog_Adapter;
import com.infinityinfoway.itsexe.exe_adapter.BookingTypeSeatDetailsDialogAdapter;
import com.infinityinfoway.itsexe.exe_adapter.SeatChartBookingTypeAdapter;
import com.infinityinfoway.itsexe.exe_adapter.WaitingListReportAdapter;
import com.infinityinfoway.itsexe.report_activity.AllDayBranchWiseReturnOnwardMemoActivity;
import com.infinityinfoway.itsexe.report_activity.AllDayMemoReportActivity;
import com.infinityinfoway.itsexe.report_activity.BranchUserWiseCollectionRefundChartActivity;
import com.infinityinfoway.itsexe.report_activity.BusIncomeReceiptReportActivity;
import com.infinityinfoway.itsexe.report_activity.CancelReportByRouteTimeActivity;
import com.infinityinfoway.itsexe.report_activity.CityDropUpWiseChartActivity;
import com.infinityinfoway.itsexe.report_activity.DailyRouteTimeWiseMemoReportActivity;
import com.infinityinfoway.itsexe.report_activity.MainRouteWiseChartActivity;
import com.infinityinfoway.itsexe.report_activity.MainRouteWisePassengerDetailActivity;
import com.infinityinfoway.itsexe.report_activity.PickupWiseInquiryReportActivity;
import com.infinityinfoway.itsexe.report_activity.RouteDepartureReportActivity;
import com.infinityinfoway.itsexe.report_activity.RoutePickUpDropChartActivity;
import com.infinityinfoway.itsexe.report_activity.RouteTimeBookingMemoReportActivity;
import com.infinityinfoway.itsexe.report_activity.RouteTimeMemoReportActivity;
import com.infinityinfoway.itsexe.report_activity.SeatAllocationChartActivity;
import com.infinityinfoway.itsexe.report_activity.SeatArrangementReportActivity;
import com.infinityinfoway.itsexe.report_activity.SeatNumberWiseChartActivity;
import com.infinityinfoway.itsexe.report_activity.SeatWiseRouteDepartureActivity;
import com.infinityinfoway.itsexe.report_activity.SubRouteWiseChartActivity;
import com.infinityinfoway.itsexe.report_activity.SubRouteWiseTicketCountReportActivity;
import com.infinityinfoway.itsexe.report_activity.TimeWiseAllBranchMemoReportActivity;
import com.infinityinfoway.itsexe.report_activity.TimeWiseBranchMemoReportActivity;
import com.infinityinfoway.itsexe.utils.API_RESPONSE_STATUS;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;
import com.infinityinfoway.itsexe.utils.BOOK_TICKET_FLOW;
import com.infinityinfoway.itsexe.utils.CustomCalendarView;
import com.infinityinfoway.itsexe.utils.CustomSnackBar;
import com.infinityinfoway.itsexe.utils.CustomToast;
import com.infinityinfoway.itsexe.utils.ExceptionHandler;
import com.infinityinfoway.itsexe.utils.NetworkLiveData;
import com.infinityinfoway.itsexe.view_model.Get_API_EXE_RouteTimeALLXML_ViewModel;
import com.infinityinfoway.itsexe.view_model.Get_API_EXE_RouteTimeXML_ViewModel;
import com.infinityinfoway.itsexe.view_model.Get_ReportType_ViewModel;
import com.infinityinfoway.itsexe.view_model.PickupDrop_ViewModel;
import com.infinityinfoway.itsexe.view_model.SeatArrangement_ViewModel;
import com.infinityinfoway.itsexe.view_model.SeatSelected_ViewModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.core.content.FileProvider.getUriForFile;

public class ExeBookingActivity_V2 extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private double getServiceTaxRoundUpLocal(double num, int multipleOf) {
        // return Math.floor((num + (double) multipleOf / 2) / multipleOf) * multipleOf;
        return Math.floor((num + (double) multipleOf) / multipleOf) * multipleOf;
    }

    private static final int REQUEST_CODE_PERMISSION = 100;
    private final String[] RunTimePerMissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    // 0-> Login Branch Route , 1-> Other Branch Route , 2-> Franchises Route
    private int IsLoginBranchRoute = 0;

    // Phone booking parameters route level -  branch level - company level
    private int RPCM_Hours, RPCM_Min;
    private int BM_AutoDeletePhoneHour, BM_AutoDeletePhoneMin;
    private int CDH_AutoDeletePhoneTime, CDH_AutoDeleteHour, CDH_AutoDeleteMinute, CDH_AutoDeletePhoneBookMinutes, GA_AllowReturnPhoneBooking, OS_PhoneBookingToRemotePayment,
            BR_AskChargeOnModify, GA_AllowMultyDateAgentQuota_Exe;
    private String reqPhoneBookingHours = "0", reqPhoneBookingMins = "0", BUM_Agent_Number;
    private int cityId = 0, pickUpId = 0, dropId = 0, branchId, routeBranchId, branchUserId = 0, sourceCityId = 0,
            destCityId = 0, routeId = 0, routeTimeId = 0, searchRouteTimeId = 0,
            loginCmpId = 0, bookingTypeId = 0, loginBranchCityId = 0, BM_IsPrepaid;

    private String sel_CompanyName, sel_BranchName, sel_BranchCode, sel_BranchCityName,
            sel_SourceCityName, sel_DestinationCityName, date_today,
            str_journeyDate, jStartTime, jCityTime, reqBookingTypeColor, reqWalletKey, reqWalletOTP, reqWalletState, reqWalletOrderNo;
    private int reqWalletAskOTP;
    private double reqWalletTDRCharge;

    private double totalBaseFare = 0.0, totalGST = 0.0, totalRoundUp = 0.0, totalPayableAmount;
    // At the time of seat modification reason id( from drop down)
    private int Modify_RemarkID, JM_ITSAdminID, IsGstChecked;
    private double JM_TDRCharge;
    private int JM_IsMagicBox, JM_GSTState, reqWalletType;
    private String JM_GSTCompanyName = "", JM_GSTRegNo = "", PM_IDProofString = "";

    private ArrayAdapter<String> fromCityArrayAdapter, toCityArrayAdapter, dateArrayAdapter;
    private Date today;
    private ArrayList<String> datearray = new ArrayList<>();
    private AvailableRoutesAdapter timeArrayAdapter;
    private SimpleDateFormat sdf_full, sdf_search_pnr;
    private ITSExeSharedPref getPref;
    //	private int IsAllowStopBooking;
    private List<Get_Route_Response.RouteDetail> listITSBusSchedule;
    private DatePickerDialog datePickerDialog = null;
    private boolean flag_date_selection = false;
    private int busposition = 0;
    private String tempDestinationCity;
    private List<SelectedSeatDetailsBean> selSeats;
    private LinkedHashMap<String, SelectedSeatDetailsBean> linkedHashMapBookedSeat = new LinkedHashMap<>();

    private BookingTypeDialog_Adapter bookingTypeDialogAdapter;
    private BookingTypeSeatDetailsDialogAdapter bookSeatAdapter;
    private ConnectionDetector cd;
    private LinearLayout linearSeatTitleMain, linearSeatTitleChild;
    private String[] seatTitle = {"Available", "Reported", "Table"};
    private int borderColor = Color.parseColor("#FF0000");
    private String IsSameDay;
    private int max_column_length, IsSubRoute = 0;
    private long lastClickTime = 0;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private List<Get_ReportType_Response.ReportType> reportTypeList = new ArrayList<>();
    private int bookedByCmpId, arrangementId;

    // Searching
    private List<FetchDetailsByPNRNO_Response.Datum> listFetchDetailsPNR;
    private List<Get_SearchingSeatNo_Response.Table1> listSearchSeatTable1;
    private List<Get_SearchingSeatNo_Response.SearchingSeatNo> listSearchSeatNo;
    private List<Get_SearchingMobileNo_Response.SearchingMobileNo> listSearchMobileNo;
    private List<PhoneTicketCount_Response.Datum> listPhoneTicketCount;
    private List<PNRBookingLog_Response.Datum> listPNRBookingLog;
    private List<PNRFeedBack_Response.Datum> listPNRFeedBack;
    private List<Get_SearchingCancelTkTPrint_Response.SearchingCancelTkTPrint> listCancelPNR;
    private List<Get_Searching_PastBookingDataByPhone_Response.SearchingPastBookingDataByPhone> listPassBookingPhone;
    private List<Get_Searching_GPSLastLocationByBusNo_Response.SearchingGPSLastLocationByBusNo> listGPSLastLocationByBusNo;
    private List<Get_Searching_BusSchedule_By_BusNo_Response.SearchingBusScheduleByBusNo> listBusScheduleBusNo;

    private String searchBusGpsLastLocation = "GJ-1-BV-108", searchBusScheduleBusNo = "MH 47 Y 6575", searchPNRInvoice = "113798937";

    private ActivityExeBookingV2Binding binding;

    private SweetAlertDialog dialogSuccess, dialogProgress, dialogWarning;
    private AppDatabase appDatabase;
    private List<String> fromCityList, toCityList;
    private int swapCounter = 0;
    private List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> routeTimeXMLListChild, routeTimeXMLListParent;
    private List<String> listDates = new ArrayList<>();
    private long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
    private Date reqDate;
    String reqBookingTypeName, reqBusNo;
    private Integer reqAgentCityId, reqAgentQuotaCityId, reqBranchMasterBranchId;
    private double discountAmount = 0.0;
    private String discountSeatString = "";

    // Route time all XML
    private List<GetCancellationDetails_ConfirmCancellation_Response.GetCancellationDetail> listCancelDetails;

    private List<BookingTypeSeatCounterBean> bookingTypeSeatCounterList = new ArrayList<>();
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML> bookingTypeList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetExtraRouteTime> extraRouteTimeList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSBusTypeColorCode> busTypeColorCodeList;
    private List<ITSGetAllAgentByCompanyID_Model> itsGetAllAgentByCompanyIDS, itsGetAllAgentCityByCompanyIDS;
    private List<ITSGetGuestTypeByCompanyID_Model> itsGetGuestTypeByCompanyIDModelList;
    private List<ITSBranchMasterSelectXML_Model> itsBranchMasterSelectXMLModelList;
    private List<ITSGetBranchUserByCompanyID_Model> itsGetBranchUserByCompanyIDModelList;
    private List<ITSOnlineWallet_Model> itsOnlineWalletModelList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetDisplayRouteBeforeMin> itsGetDisplayRouteBeforeMinModelsList;

    private List<ITSFetchChartForeColor_Model> itsFetchChartForeColorModelList;
    private List<ITSGetCompanyMarquee_Model> itsGetCompanyMarqueeModelList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetITSMarquee> itsGetITSMarqueeModelList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingRightsByUser> itsGetBookingRightsByUsers;
    private List<ITSDoNotAllowFareChangeInBelowRouteTime_Model> itsDoNotAllowFareChangeRouteList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetOtherCompanyBookingRightsExe> otherCompanyBookingRightsList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSRouteTimeExtraFareGet> itsRouteTimeExtraFareGetList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetIdProof> itsGetIdProofList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSState> itsStateList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSCancelremark> itsCancelremarkList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkModify> itsRemarkModifyList;
    private List<Get_API_EXE_RouteTimeALLXML_Response.ITSRemarkRePrint> itsRemarkRePrintList;

    private List<PickupDrop_Response.Datum> listPickUp, listDropUp;

    private Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingRightsByUser bookingRightsByUserData;
    private Get_API_EXE_RouteTimeALLXML_Response.ITSGetDisplayRouteBeforeMin itsGetDisplayRouteBeforeMin;

    private String[] searchPNR = {"PNR No", "Seat No", "Mobile No", "Phone Tkt Count", "Print Cancelled Ticket", "Get Schedule By Bus No",
            "Past Booking By Mobile No", "GPS Location By Bus No", "Change PNR GST Detail", "Print GST Invoice", "E-Send GST Invoice",
            "PNR Booking Log", "PNR Feedback", "GST Invoice Pdf"};
    private String[] bankCard = {"Debit", "Credit", "Paytm", "UPI", "Google Pay", "Bhim", "Phone Pe", "Mobikwik"};
    private int bankCardId = 0;
    private String bankCardRemarks;

    // Discount Company Copon

    private List<CouponCodeDetails_Response.Datum> listDiscountCopon;
    private List<CompanyCardDetails_Response.Datum> listCompanyCard;

    private int backDate = 0, advanceDate = 0, totalPax = 0, IsStopBooking = 0;
    private String seatNameList;

    // ITS_GetDisplayRouteBeforeMin Validations
    // OS_AllowToFetchAllPaxDetail=1 then max 8 seats otherwise 12 seats
    private String AUTO_DETECT_OTP = "";
    private int OTP_MAX_ATTEMPTS = 0;
    private Handler.Callback resendOTPCallBack = null, downloadInvoiceCallback;

    private int OS_MaxCompanyCardBooking, OS_AllowCoupanCode, OS_CouponCodeMaxSeatBook, OS_AllowToFetchAllPaxDetail,
            AllowMultipleBooking, IsMultipleChecked, AllowFareChnage, AllowLessFareChange, AllowFareChangeInPhoneModify, ISCountTableInBooked = 0, OSIsPendingAmount,
            GA_AllowChangeRefundCharges, GA_AllowLessRefundCharges;
    private int OS_HoldCompanyCard, OS_HoldBankCard, OS_HoldGeneralAgent, OS_HoldGuest, GA_AllowPhoneOnHoldSeat;
    private int GA_AllowReprint, OS_ReprintGeneralAgent;
    private int CDH_IsCallCenterAPI;
    private int OS_EmailConfirm, OS_EmailPhone, OS_EmailAgent, OS_EmailBranch, OS_EmailGuest, OS_EmailBankCard,
            OS_EmailCompanyCard, OS_EmailWaiting, OS_EmailAgentQuata, OS_EmailOpen, OS_EmailGeneralAgent,
            OS_EmailOnlineAgent, OS_EmailAPIAgent, OS_EmailB2CAgent, OS_EmailOnlineWallet;


    private int GA_AllowConfirmToOpen;
    private int OS_AskOTPOnModifyBooking, OS_AskOTPOnPhoneBooking, OS_AskOTPOnBankCardBooking;
    private int CM_IsSMSActive;
    private int OS_PDFConfirm, OS_PDFPhone, OS_PDFGeneralAgent, OS_PDFOnlineAgent, OS_PDFAPIAgent, OS_PDFB2CAgent,
            OS_PDFBranch, OS_PDFGuest, OS_PDFBankCard, OS_PDFCompanyCard, OS_PDFAgentQuata, OS_PDFOnlineWallet;

    private int IsBookingLog, GA_IsShowFeedBack;
    private int CDH_BackDateShow, CDH_AdvanceDateShow, OS_DisplayBookSeatFareOnChart, OS_GetExtraValueOnFare, GA_AllowBackDateBooking, GA_ShowBackDate, GA_AllowBackDateCancellation,
            GA_IsPromptBackDateBooking, OS_IsAskIDProof, OS_IsIDProofMandatory, OS_IsCrossStateIDProofMandatory, OS_DisplayTotalFareOnChart, CDH_ShowStopBooking;

    private int IsSearchOperation = 0;

    // Adapter

    private ReportTypeListAdapter reportTypeListAdapter;

    Animation animITSMarquee, animCmpMarquee, animInProgressTransaction, animCompanyCardDetails;
    private double cmpCardMaxDiscount, cmpCardBalance;
    private String cmpCardNo, cmpCardOTP, cmpCardId;


    // Bottom Network Dialog
    private BottomSheetDialog bottomSheetDialog, bottomBookTicketDialog, bottomSheetModificationDialog, bottomBookingTypeSeatDetailsDialog;
    private LayoutBottomSheetInternetConnectionBinding internetConnectionBinding;
    private LayoutFareDisplayBinding fareDisplayBinding;
    private DialogCancellationDetailsBinding dialogCancellationDetailsBinding;
    private DialogSeatDetailsModificationPoupBinding dialogSeatDetailsModificationPoupBinding;

    // Seat Arrangement Model

    private SeatArrangement_ViewModel seatArrangementViewModel;
    private SeatSelected_ViewModel seatSelectedViewModel;
    private PickupDrop_ViewModel pickupDropViewModel;

    private BookTicketPassengerListAdapter passengerListAdapter;
    private List<SeatArrangement_Response.Datum> listSeatDetails = new ArrayList<>();
    private int upperSeatCounter;


    private int totalCapacity, totalBooked, totalAvailable, totalBookedTable;
    private double totalOpacity;
    private String emptySeatColor = "";

    private AlertDialog alertSeatSelect;

    private Boolean machine_changed_edittext = false;

    private int oldAcSeatRate = 0, oldAcSlpRate = 0, oldAcSlmbRate = 0;
    private int oldNonAcSeatRate = 0, oldNonAcSlpRate = 0, oldNonAcSlmbRate = 0;
    private int newAcSeatRate = 0, newAcSlpRate = 0, newAcSlmbRate = 0;
    private int newNonAcSeatRate = 0, newNonAcSlpRate = 0, newNonAcSlmbRate = 0;

    // Modify Parameters
    private int modifyAcSeatRate = 0, modifyAcSlpRate = 0, modifyAcSlmbRate = 0;
    private int modifyNonAcSeatRate = 0, modifyNonAcSlpRate = 0, modifyNonAcSlmbRate = 0;
    private double modifyTotalBaseFare = 0.0, modifyTotalGST = 0.0, modifyDiscountAmount = 0.0;
    private String modifySeats, modifySeatsTemp;
    private int modifyTotalPax, modifyBookingTypeId;
    private int modifyAcSeatTotal, modifyAcSlpTotal, modifyAcSlmbTotal, modifyNonAcSeatTotal, modifyNonAcSlpTotal, modifyNonAcSlmbTotal;

    private int acSeatTotal, acSlpTotal, acSlmbTotal, nonAcSeatTotal, nonAcSlpTotal, nonAcSlmbTotal;

    private BottomSheetDialog bottomSheetDialogConfirmation;

    // PH_CardID => Company Card ,Coupon
    private int PH_CardProvidorID = 0, PH_TransStatus = 0;
    private double PH_Amount = 0.0;
    private String PH_CardID = "", PH_DateTime = "", PH_IPAddress = "", PH_CardHolderName = "";

    // Booking Type Wise Data
    private int GTM_GuestTypeID = 0, AM_AgentID = 0, ABI_NeteRate = 0, ABI_TotalSeat = 0, ABI_AgentCityID = 0, BBI_BM_BranchID = 0, BBI_UM_UserID = 0;
    private String ABI_TicketNo = "", BBI_TicketNo = "", JM_B2C_OrderNo = "", ModiFyPNRNo;
    private double JM_OriginalTotalPayable, JM_BranchModifyCharges = 0.0, modifyTotalRoundUp = 0.0;
    private int JM_PCRegistrationID;

    // Service Tax

    private int CM_IsACServiceTax, CM_IsNonACServiceTax, CM_ACFareIncludeTax, CM_NonACFareIncludeTax,
            GA_AllowToSetLessPhoneCancelTime, OS_DisplayNetRate;
    private double CM_ACServiceTax, CM_NonACServiceTax;
    private double CM_ACPackageServiceTax, CM_NonACPackageServiceTax, CM_ACOwnScheduleServiceTax, CM_NonACOwnScheduleServiceTax;
    private double CM_ACOtherOwnScheduleServiceTax, CM_NonACOtherOwnScheduleServiceTax;
    private int IsIncludeTax = 0, CM_ServiceTaxRoundUp;

    // private BottomDialog bottomDialog;

    private Calendar calendarForPhoneBookingTime;
    private String AuotPhoneDeleteMinute = "0", AuotPhoneDeleteHour = "0";

    private PopupWindow popupModificationDialog;
    private LayoutSeatModificationPopupV2Binding seatModifyBinding;
    private int TRANSACTION_TYPE = 0, INSERT_UPDATE_BOOKING_STATUS = 0;
    private SweetAlertDialog dialogLoadTransactions;

    private void handleMessage(BOOK_TICKET_FLOW valueFlow) {
        switch (valueFlow) {
            case BOOK_TICKET_API_CALL:

                if (TRANSACTION_TYPE != APP_CONSTANTS.MODIFY) {
                    // Insert Booking

                    if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING && !binding.txtAgentquotaDate.getText().toString().equals(str_journeyDate)) {
                        // tran type 1 => booking , 2 -> cancel
                        InsertAgentQuotaBookingCancleApiCall(1);
                    } else if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING && OS_AskOTPOnPhoneBooking == 1) {
                        showAskOTPDialog(new Handler.Callback() {
                            @Override
                            public boolean handleMessage(@NonNull Message message) {
                                if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                                    insertBookingApiCall();
                                }
                                return false;
                            }
                        });
                    } else if (bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING && OS_AskOTPOnBankCardBooking == 1) {
                        showAskOTPDialog(new Handler.Callback() {
                            @Override
                            public boolean handleMessage(@NonNull Message message) {
                                if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                                    insertBookingApiCall();
                                }
                                return false;
                            }
                        });
                    } else if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                        openWalletGenerateOTPDialog(new Handler.Callback() {
                            @Override
                            public boolean handleMessage(@NonNull Message message) {
                                if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                                    insertBookingApiCall();
                                }
                                return false;
                            }
                        });
                    } else {
                        insertBookingApiCall();
                    }
                } else {
                    // Modify Booking

                    if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING && !binding.txtAgentquotaDate.getText().toString().equals(str_journeyDate)) {
                        // tran type 1 => booking , 2 -> cancel
                        InsertAgentQuotaBookingCancleApiCall(1);
                    } else if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                        insertBookingApiCall();
                    } else if (OS_AskOTPOnModifyBooking == APP_CONSTANTS.STATUS_SUCCESS) {
                        showAskOTPDialog(new Handler.Callback() {
                            @Override
                            public boolean handleMessage(@NonNull Message message) {
                                if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                                    insertBookingApiCall();
                                }
                                return false;
                            }
                        });
                    } else {
                        insertBookingApiCall();
                    }
                }

                break;

            case OPEN_GST_DIALOG:
                openGSTDialog();
                break;

            case PHONE_BOOKING_TIME_POPUP:
                showPhoneBookingCancelTimePopUp();
                break;

        }
    }

    private boolean isChartSmsRadioChecked = false;
    private Dialog dialogChartSms, dialogSeatPermanentHold;
    private String seatHoldFromDate, seatHoldToDate;
    private List<String> listSeatHoldSelectedDays, listSeatHoldSelectedSeats;
    private DisplayMetrics displaymetrics;
    private List<String> listReqParameters = new ArrayList<>();
    private int IsReportedPNR;
    private View btnSeat;

    // String title variables
    private String oops, success, validRoute, validEmail, validPNR, validMobile, validSeatNo, validFrom, validTo,
            validCardNo,
            internetShortMsg, internetLongMsg, badServerRequest, badServerResponse;

    private Get_API_EXE_RouteTimeXML_ViewModel routeTimeXMLViewModel;
    private Get_API_EXE_RouteTimeALLXML_ViewModel routeTimeAllXMLViewModel;
    private Get_ReportType_ViewModel reportTypeViewModel;
    private List<Api_List_Status> currentApiListStatus;
    private int totalConsecutiveApiCall = 0;
    private File downloadFile;
    private BroadcastReceiver onCompleteDownload;
    private String TAG = "ExeBookingActivity";

    // Extra Fare Agent
    private int CAM_OtherChargeType, CAM_IsOnline;
    private double CAM_OtherCharge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        oops = getResources().getString(R.string.dialog_error_title);
        success = getResources().getString(R.string.dialog_success_title);
        validFrom = getResources().getString(R.string.valid_from);
        validTo = getResources().getString(R.string.valid_to);
        validRoute = getResources().getString(R.string.valid_route);
        validEmail = getResources().getString(R.string.valid_email);
        validPNR = getResources().getString(R.string.valid_pnr);
        validMobile = getResources().getString(R.string.valid_mobile);
        validSeatNo = getResources().getString(R.string.validSeat);
        validCardNo = getResources().getString(R.string.valid_card_no);
        internetShortMsg = getResources().getString(R.string.no_internet_short_msg);
        internetLongMsg = getResources().getString(R.string.no_internet_long_msg);
        badServerRequest = getResources().getString(R.string.bad_server_request);
        badServerResponse = getResources().getString(R.string.bad_server_response);

        binding = ActivityExeBookingV2Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        binding.txtCmpMarquee.setSelected(true);
        binding.txtItsMarquee.setSelected(true);


        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this, TAG));

        PH_IPAddress = getIpAddress();

        cd = new ConnectionDetector(ExeBookingActivity_V2.this);
        getPref = new ITSExeSharedPref(ExeBookingActivity_V2.this);
        selSeats = new LinkedList<>();

        routeTimeXMLListParent = new ArrayList<>();
        routeTimeXMLListChild = new ArrayList<>();


        bookingTypeList = new ArrayList<>();
        extraRouteTimeList = new ArrayList<>();
        busTypeColorCodeList = new ArrayList<>();
        itsGetAllAgentByCompanyIDS = new ArrayList<>();
        itsGetAllAgentCityByCompanyIDS = new ArrayList<>();
        itsGetGuestTypeByCompanyIDModelList = new ArrayList<>();
        itsBranchMasterSelectXMLModelList = new ArrayList<>();
        itsGetBranchUserByCompanyIDModelList = new ArrayList<>();
        itsOnlineWalletModelList = new ArrayList<>();
        itsGetDisplayRouteBeforeMinModelsList = new ArrayList<>();
        itsFetchChartForeColorModelList = new ArrayList<>();
        itsGetCompanyMarqueeModelList = new ArrayList<>();
        itsGetITSMarqueeModelList = new ArrayList<>();
        itsGetBookingRightsByUsers = new ArrayList<>();
        itsDoNotAllowFareChangeRouteList = new ArrayList<>();
        otherCompanyBookingRightsList = new ArrayList<>();
        itsRouteTimeExtraFareGetList = new ArrayList<>();
        itsGetIdProofList = new ArrayList<>();
        itsStateList = new ArrayList<>();
        itsCancelremarkList = new ArrayList<>();
        itsRemarkModifyList = new ArrayList<>();
        itsRemarkRePrintList = new ArrayList<>();


        loginCmpId = getPref.getCM_CompanyID();
        JM_ITSAdminID = getPref.getAdminUserId();
        sel_CompanyName = "";
        sel_BranchName = getPref.getBM_BranchName();
        sel_BranchCode = getPref.getBM_BranchCode();
        loginBranchCityId = getPref.getCM_CityID();
        cityId = getPref.getCM_CityID();
        sel_BranchCityName = "";
        pickUpId = 0;
        branchId = getPref.getBM_BranchID();
        branchUserId = getPref.getBUM_BranchUserID();

        today = new Date();
        sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
        sdf_search_pnr = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", java.util.Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        date_today = sdf_full.format(today);
        str_journeyDate = date_today;

        try {
            reqDate = sdf_full.parse(str_journeyDate);
        } catch (Exception ignored) {
        }



        binding.actvSourceCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

                if (TRANSACTION_TYPE != APP_CONSTANTS.MODIFY) {
                    if (fromCityList != null && fromCityList.size() > 0) {
                        for (int i = 0; i < fromCityList.size(); i++) {
                            if (fromCityList.get(i).equals(arg0.toString())) {
                                sel_SourceCityName = fromCityList.get(i);

                                // showProgressDialog("Loading....");

                                Executors.newSingleThreadExecutor().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        sourceCityId = Integer.parseInt(appDatabase.getApiExeRouteTimeXMLDao().getFromCityId(sel_SourceCityName));
                                        toCityList = appDatabase.getApiExeRouteTimeXMLDao().getToCity(sel_SourceCityName);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                //disMissDialog();
                                                if (toCityList != null && toCityList.size() > 0) {
                                                    toCityArrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.autocomplete_items, toCityList);
                                                    binding.actvDestCity.setAdapter(toCityArrayAdapter);
                                                    binding.actvDestCity.requestFocus();

                                                    if (swapCounter == 1 && toCityList.contains(tempDestinationCity)) {
                                                        sel_DestinationCityName = tempDestinationCity;
                                                        binding.actvDestCity.setText(sel_DestinationCityName);
                                                        binding.actvDestCity.setSelection(tempDestinationCity.length());

                                                        /*if (!TextUtils.isEmpty(sel_SourceCityName) && !TextUtils.isEmpty(sel_DestinationCityName) && reqDate != null) {
                                                            fetchRouteFromDataBase();
                                                        }*/

                                                    } else if (swapCounter == 0 && toCityList.contains(sel_DestinationCityName)) {
                                                        binding.actvDestCity.setSelection(sel_DestinationCityName.length());
                                                        if (!TextUtils.isEmpty(sel_SourceCityName) && !TextUtils.isEmpty(sel_DestinationCityName) && reqDate != null) {
                                                            fetchRouteFromDataBase();
                                                        }
                                                    } else {
                                                        binding.actvDestCity.setText("");
                                                        sel_DestinationCityName = "";
                                                        destCityId = 0;
                                                        clearTextFields();
                                                        binding.actvDestCity.requestFocus();
                                                        if (currentApiListStatus != null && currentApiListStatus.size() == 0) {
                                                            showKeyBoard(binding.actvDestCity);
                                                        }
                                                    }
                                                } else {
                                                    showToast("Try another route,  no destinations found");
                                                    binding.actvDestCity.setAdapter(null);
                                                    binding.actvDestCity.setText("");
                                                    sel_DestinationCityName = "";
                                                    destCityId = 0;
                                                }
                                            }
                                        });


                                    }
                                });

                                break;
                            } else {
                                sel_SourceCityName = "";
                                sourceCityId = 0;
                                clearTextFields();
                            }
                        }

                    }
                }


            }
        });

        binding.actvDestCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (TRANSACTION_TYPE != APP_CONSTANTS.MODIFY) {
                    if (toCityList != null && toCityList.size() > 0) {
                        for (int j = 0; j < toCityList.size(); j++) {
                            if (toCityList.get(j).equals(arg0.toString())) {
                                if (binding.actvSourceCity.getText().toString().equals(arg0.toString())) {
                                    showToast("From City and To City can't be same !!!");
                                    binding.actvDestCity.setText("");
                                    clearTextFields();
                                } else {
                                    sel_DestinationCityName = toCityList.get(j);

                                    binding.actvDestCity.setSelection(binding.actvDestCity.getText().length());
                                    binding.actvDestCity.dismissDropDown();

                                    swapCounter = 0;

                                /*if (swapCounter == 0) {
                                    rl_calender.performClick();
                                } else {
                                    swapCounter = 0;
                                }*/

                                    try {
                                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                                Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(binding.actvDestCity.getWindowToken(), 0);
                                    } catch (Exception ex) {
                                    }


                                    Executors.newSingleThreadExecutor().execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            destCityId = Integer.parseInt(appDatabase.getApiExeRouteTimeXMLDao().getToCityId(sel_DestinationCityName));
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (!TextUtils.isEmpty(sel_SourceCityName) && !TextUtils.isEmpty(sel_DestinationCityName) && reqDate != null) {
                                                        fetchRouteFromDataBase();
                                                    } else {
                                                        showToast("Try another route or date , no data found");
                                                    }
                                                }
                                            });
                                        }
                                    });


                                }

                                break;
                            } else {
                                destCityId = 0;
                                sel_DestinationCityName = "";
                                clearTextFields();
                            }
                        }

                    }
                }

            }
        });

        //=====================================================================================
        // Swap button Click Event
//=====================================================================================
        binding.imgSwap.setOnClickListener(v -> {
            if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
                showAlreadyLoadTransactionsDialog();
            } else {
                if (TextUtils.isEmpty(sel_SourceCityName)) {
                    showToast(validFrom);
                } else if (TextUtils.isEmpty(sel_DestinationCityName)) {
                    showToast(validTo);
                } else {
                    String destinationCity = sel_SourceCityName;
                    binding.actvSourceCity.setText(sel_DestinationCityName);
                    swapCounter = 1;

                    //  actv_DestinationCity.setText("");
                    tempDestinationCity = destinationCity;

                    sel_SourceCityName = binding.actvSourceCity.getText().toString();

                }
            }

        });


        binding.imgCalendar.setOnClickListener(arg0 -> openDateDialog());

        binding.spnDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    reqDate = sdf_full.parse(binding.spnDate.getSelectedItem().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING && GA_AllowMultyDateAgentQuota_Exe == APP_CONSTANTS.RIGHTS_ALLOW) {
                    try {
                        if (sdf_full.parse(binding.txtAgentquotaDate.getText().toString()).getTime() < reqDate.getTime()) {
                            binding.txtAgentquotaDate.setText(binding.spnDate.getSelectedItem().toString());
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(sel_SourceCityName) && !TextUtils.isEmpty(sel_DestinationCityName) && reqDate != null) {
                    fetchRouteFromDataBase();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("nothing", "selected");
            }
        });


        binding.spnRouteTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                clearCouponCode();

                binding.txtRouteCounter.setText((binding.spnRouteTime.getSelectedItemPosition() + 1) + "/" + routeTimeXMLListParent.size());

                binding.txtRouteName.setText("  " + routeTimeXMLListParent.get(position).getRTTime() + " - " + routeTimeXMLListParent.get(position).getRMRouteNameDisplay());

                Get_API_EXE_RouteTimeXML_Response.RouteTimeXML data = routeTimeXMLListParent.get(position);


                routeId = Integer.parseInt(data.getRMRouteID());
                routeTimeId = Integer.parseInt(data.getRTRouteTimeID());
                bookedByCmpId = Integer.parseInt(data.getCMCompanyID());

                try {
                    str_journeyDate = binding.spnDate.getSelectedItem().toString();
                } catch (Exception ignored) {
                }

                RPCM_Hours = data.getRPCMHours();
                RPCM_Min = data.getRPCMMin();

                jStartTime = data.getRTTime();
                jCityTime = data.getStartTime();
                IsSameDay = data.getPSIIsSameDay();


                // selSeats=new LinkedList<>();
                // seatSelectedViewModel.addSeats(selSeats);


                if (sourceCityId == Integer.parseInt(data.getRMFromCityID()) && destCityId == Integer.parseInt(data.getRMToCityID())
                        && sourceCityId == Integer.parseInt(data.getRTMFromCityID()) && destCityId == Integer.parseInt(data.getRTMToCityID())) {
                    IsSubRoute = 0;
                } else {
                    IsSubRoute = 1;
                }

                totalConsecutiveApiCall = 2;

                disableActivityScreen();
                showProgressDialog(getResources().getString(R.string.loading));
                createSeatArrangementRequest();
                pickUpDropRequest();

                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<ITSGetCompanyMarquee_Model> listTemp = appDatabase.getApiExeRouteTimeALLXMLDao().getITSGetCompanyMarquee(reqDate, routeId, routeTimeId);
                        if (listTemp.size() > 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    itsGetCompanyMarqueeModelList = listTemp;
                                    setMarquee(1);
                                }
                            });
                        }
                    }
                });

                if(bookedByCmpId>0 && loginCmpId!=bookedByCmpId){
                    fetchOtherCompanyRights();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        binding.btnCouponCode.setOnClickListener(v -> {
            if ((isLoadTransactions() != 1) || (modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING || modifyBookingTypeId == APP_CONSTANTS.WAITING_BOOKING || modifyBookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING || modifyBookingTypeId == APP_CONSTANTS.AGENT_PHONE_BOOKING)) {
                if (TextUtils.isEmpty(binding.txtCouponCode.getText().toString().trim())) {
                    showSnackBar(oops, "Enter valid coupon code", Toast.LENGTH_LONG, 1);
                } else if (sourceCityId == 0) {
                    showSnackBar(oops, "Select valid from city", Toast.LENGTH_LONG, 1);
                } else if (destCityId == 0) {
                    showSnackBar(oops, "Select valid to city", Toast.LENGTH_LONG, 1);
                } else if (routeId == 0 || routeTimeId == 0) {
                    showSnackBar(oops, validRoute, Toast.LENGTH_LONG, 1);
                } else if (totalPax > OS_CouponCodeMaxSeatBook) {
                    showSnackBar(oops, "You are not permitted to more than " + OS_CouponCodeMaxSeatBook + " Pax in [Coupon Code]", Toast.LENGTH_LONG, 1);
                } else {
                    hideKeyBoard();
                    CouponCodeDetailsApiCall();
                }
            }

        });

        binding.spnBookingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // releaseBookingTypeIds();

                bookingTypeId = bookingTypeList.get(position).getBookingTypeId();
                reqBookingTypeName = bookingTypeList.get(position).getDefaultName();
                reqBookingTypeColor = bookingTypeList.get(position).getColor();

                binding.txtAgentBal.setVisibility(View.GONE);
                binding.llConfirmBooking.setVisibility(View.GONE);
                binding.llBlank.setVisibility(View.GONE);
                binding.llBranchBooking.setVisibility(View.GONE);
                binding.llAgentBooking.setVisibility(View.GONE);
                binding.llAgentQuota.setVisibility(View.GONE);
                binding.llGuestBooking.setVisibility(View.GONE);
                binding.llBankCardBooking.setVisibility(View.GONE);
                binding.llCompanyCardBooking.setVisibility(View.GONE);
                binding.llItsplWallet.setVisibility(View.GONE);

                clearCompanyCardDetails();


                calTotalSeat();

                switch (bookingTypeId) {
                    case APP_CONSTANTS.CONFIRM_BOOKING:
                        if (OS_AllowCoupanCode == 1) {
                            binding.llConfirmBooking.setVisibility(View.VISIBLE);

                            LinearLayout.LayoutParams paramBookingType = new LinearLayout.LayoutParams(
                                    /*width*/ 0,
                                    /*height*/ ViewGroup.LayoutParams.WRAP_CONTENT,
                                    /*weight*/ 1.0f
                            );

                            LinearLayout.LayoutParams paramConfirmBooking = new LinearLayout.LayoutParams(
                                    /*width*/ 0,
                                    /*height*/ ViewGroup.LayoutParams.WRAP_CONTENT,
                                    /*weight*/ 2.0f
                            );

                            binding.llBookingType.setLayoutParams(paramBookingType);
                            binding.llConfirmBooking.setLayoutParams(paramConfirmBooking);
                        }
                        break;
                    case APP_CONSTANTS.PHONE_BOOKING:
                        binding.llBlank.setVisibility(View.INVISIBLE);
                        break;
                    case APP_CONSTANTS.AGENT_BOOKING:
                        binding.llAgentBooking.setVisibility(View.VISIBLE);
                        binding.llAgentRateNet.setVisibility(View.VISIBLE);
                        binding.llAgentTicketNo.setVisibility(View.VISIBLE);
                        bindAgentBookingTypeData();
                        break;
                    case APP_CONSTANTS.BRANCH_BOOKING:
                        binding.llBranchBooking.setVisibility(View.VISIBLE);
                        bindBranchMasterBookingTypeData();
                        break;
                    case APP_CONSTANTS.GUEST_BOOKING:
                        binding.llGuestBooking.setVisibility(View.VISIBLE);
                        bindGuestBookingTypeData();
                        break;
                    case APP_CONSTANTS.BANK_CARD_BOOKING:
                        binding.llBankCardBooking.setVisibility(View.VISIBLE);
                        bindBankCardBookingType();
                        break;
                    case APP_CONSTANTS.CMP_CARD_BOOKING:
                        if (selSeats.size() > OS_MaxCompanyCardBooking) {
                            showSnackBar(oops, "You can't book seat more than " + OS_MaxCompanyCardBooking + " in " + reqBookingTypeName, Toast.LENGTH_LONG, 1);
                        } else {
                            binding.llCompanyCardBooking.setVisibility(View.VISIBLE);

                            binding.llCmpCardDetails.setVisibility(View.GONE);
                            binding.txtCmpDiscount.setVisibility(View.INVISIBLE);
                            binding.etCmpCardDisc.setVisibility(View.GONE);

                            bindCompanyBookingType();
                        }
                        break;
                    case APP_CONSTANTS.AGENT_QUOTA_BOOKING:
                        binding.llAgentBooking.setVisibility(View.VISIBLE);
                        binding.llAgentQuota.setVisibility(View.VISIBLE);
                        bindAgentBookingTypeData();
                        binding.llAgentRateNet.setVisibility(View.GONE);
                        binding.llAgentTicketNo.setVisibility(View.GONE);
                        binding.txtAgentquotaDate.setText(sdf_full.format(reqDate));
                        break;
                    case APP_CONSTANTS.AGENT_PHONE_BOOKING:
                        binding.llAgentBooking.setVisibility(View.VISIBLE);
                        binding.llAgentRateNet.setVisibility(View.VISIBLE);
                        binding.llAgentTicketNo.setVisibility(View.VISIBLE);
                        bindAgentBookingTypeData();
                        break;
                    case APP_CONSTANTS.API_PHONE_BOOKING:
                        binding.llAgentBooking.setVisibility(View.VISIBLE);
                        binding.llAgentRateNet.setVisibility(View.VISIBLE);
                        binding.llAgentTicketNo.setVisibility(View.VISIBLE);
                        bindAgentBookingTypeData();
                        break;
                    case APP_CONSTANTS.ITSPL_WALLET_BOOKING:
                        binding.llItsplWallet.setVisibility(View.VISIBLE);
                        bindITSPLWalletBookingType();
                        break;
                    default:
                        binding.llBlank.setVisibility(View.INVISIBLE);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        binding.txtReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                } catch (Exception ignored) {
                }

                if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
                    showAlreadyLoadTransactionsDialog();
                } else {
                    if (routeId > 0 && routeTimeId > 0) {
                        viewReportActivity(reportTypeList.get(binding.spnReport.getSelectedItemPosition()).getReportID());
                    } else {
                        showSnackBar(oops, getResources().getString(R.string.valid_route), Toast.LENGTH_LONG, 1);
                    }

                }

            }
        });


        appDatabase = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase();


        currentApiListStatus = new ArrayList<>();

        disableActivityScreen();

        showProgressDialog(getResources().getString(R.string.loading));
        totalConsecutiveApiCall = 3;

        addApiList(APP_CONSTANTS.Get_ReportType, API_RESPONSE_STATUS.RUNNING);
        addApiList(APP_CONSTANTS.Get_API_EXE_RouteTimeXML, API_RESPONSE_STATUS.RUNNING);
        addApiList(APP_CONSTANTS.Get_API_EXE_RouteTimeALLXML, API_RESPONSE_STATUS.RUNNING);

        // Report Type

        reportTypeViewModel = new ViewModelProvider(ExeBookingActivity_V2.this).get(Get_ReportType_ViewModel.class);
        getReportTypeRequest();
        reportTypeViewModel.listMutableLiveData.observe(ExeBookingActivity_V2.this, response -> {
            totalConsecutiveApiCall--;
            if (response != null) {
                reportTypeList = response;
                getReportTypeResponse();
                addApiList(APP_CONSTANTS.Get_ReportType, API_RESPONSE_STATUS.SUCCESS);
            } else {
                addApiList(APP_CONSTANTS.Get_ReportType, API_RESPONSE_STATUS.FAILED);
            }
        });

        // Route Time XML
        routeTimeXMLViewModel = new ViewModelProvider(ExeBookingActivity_V2.this).get(Get_API_EXE_RouteTimeXML_ViewModel.class);
        routeXmlApiRequest();
        routeTimeXMLViewModel.routeXmlMutableLiveData.observe(ExeBookingActivity_V2.this, response -> {
            totalConsecutiveApiCall--;
            if (response != null) {
                routeTimeXmlApiResponse(response);
                addApiList(APP_CONSTANTS.Get_API_EXE_RouteTimeXML, API_RESPONSE_STATUS.SUCCESS);
            } else {
                addApiList(APP_CONSTANTS.Get_API_EXE_RouteTimeXML, API_RESPONSE_STATUS.FAILED);
            }
        });


        // Route Time All XML
        routeTimeAllXMLViewModel = new ViewModelProvider(ExeBookingActivity_V2.this).get(Get_API_EXE_RouteTimeALLXML_ViewModel.class);
        routeTimeAllXmlRequest();
        routeTimeAllXMLViewModel.xmlMutableLiveData.observe(ExeBookingActivity_V2.this, response -> {
            totalConsecutiveApiCall--;
            if (response != null) {
                routeTimeAllXmlApiResponse(response);
                addApiList(APP_CONSTANTS.Get_API_EXE_RouteTimeALLXML, API_RESPONSE_STATUS.SUCCESS);
            } else {
                addApiList(APP_CONSTANTS.Get_API_EXE_RouteTimeALLXML, API_RESPONSE_STATUS.FAILED);
            }
        });

        binding.spnBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reqBranchMasterBranchId = itsBranchMasterSelectXMLModelList.get(position).getBM_BranchID();
                bindBranchUserByBranchMaster();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spnAgentCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reqAgentCityId = itsGetAllAgentCityByCompanyIDS.get(position).getCM_CityId();
                bindAgentNameByAgentCity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.etBankNotes.setOnTouchListener((view18, event) -> {
            if (view18.getId() == R.id.et_bank_notes) {
                view18.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view18.getParent().requestDisallowInterceptTouchEvent(
                                false);
                        break;
                }
            }
            return false;
        });

        binding.imgSearchPnr.setOnClickListener(view1 -> {
            if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
            } else {
                Get_SearchingPNRNOApiCall(7, binding.etSearchPnr.getText().toString().trim());
            }
        });

        binding.imgLoadTicket.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }

            if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
                showAlreadyLoadTransactionsDialog();
            } else {
                int pos = binding.spnSearch.getSelectedItemPosition();
                switch (pos) {
                    case APP_CONSTANTS.SEARCHING_PNR:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {
                            Get_SearchingPNRNOApiCall(2, binding.etSearchPnr.getText().toString().trim());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_SEAT:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, "Enter valid seat no", Toast.LENGTH_LONG, 1);
                        } else {
                            Get_SearchingSeatNoApiCall();
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_MOBILE:
                        if (binding.etSearchPnr.getText().length() <= 9) {
                            showSnackBar(oops, validMobile, Toast.LENGTH_LONG, 1);
                        } else {
                            Get_SearchingMobileNoApiCall();
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_PHONE_TKT_COUNT:
                        if (binding.etSearchPnr.getText().length() <= 0) {
                            showSnackBar(oops, "Enter valid no of seats to open phone booking", Toast.LENGTH_LONG, 1);
                        } else {
                            PhoneTicketCountApiCall(binding.etSearchPnr.getText().toString());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_PRINT_CANCEL_TKT:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {

                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_GET_SCHEDULE_BUSNO:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, "Enter valid bus no", Toast.LENGTH_LONG, 1);
                        } else {
                            Get_Searching_BusSchedule_By_BusNoApiCall(binding.etSearchPnr.getText().toString());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_PAST_BOOK_MOBILE:
                        if (binding.etSearchPnr.getText().length() <= 9) {
                            showSnackBar(oops, validMobile, Toast.LENGTH_LONG, 1);
                        } else {
                            Get_Searching_PastBookingDataByPhoneApiCall(binding.etSearchPnr.getText().toString());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_GPS_LOCATION_BUS_NO:
                        if (binding.etSearchPnr.getText().length() <= 9) {
                            showSnackBar(oops, "Enter valid bus no", Toast.LENGTH_LONG, 1);
                        } else {

                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_CHANGE_PNR_GST:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {

                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_PRINT_INVOICE:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {
                            Get_SearchingPNRNOApiCall(6, binding.etSearchPnr.getText().toString());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_ESEND_INVOICE:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {
                            Get_SearchingPNRNOApiCall(5, binding.etSearchPnr.getText().toString());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_PNR_LOG:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {
                            PNRBookingLogApiCall(binding.etSearchPnr.getText().toString().trim());
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_PNR_FEEDBACK:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {
                            PNRFeedBackApiCall();
                        }
                        break;
                    case APP_CONSTANTS.SEARCHING_INVOICE_PDF:
                        if (TextUtils.isEmpty(binding.etSearchPnr.getText())) {
                            showSnackBar(oops, validPNR, Toast.LENGTH_LONG, 1);
                        } else {
                            Get_SearchingPNRNOApiCall(6, binding.etSearchPnr.getText().toString());
                        }
                        break;

                }

            }
        });

        // Network Detection Observer
        //  networkDetection();

        // Seat Arrangement Observer

        seatArrangementViewModel = new ViewModelProvider(ExeBookingActivity_V2.this).get(SeatArrangement_ViewModel.class);
        seatArrangementViewModel.mutableSeatChartLiveData.observe(ExeBookingActivity_V2.this, new Observer<List<SeatArrangement_Response.Datum>>() {
            @Override
            public void onChanged(List<SeatArrangement_Response.Datum> seatResponse) {
                totalConsecutiveApiCall--;
                if (seatResponse != null && seatResponse.size() > 0) {
                    addApiList(APP_CONSTANTS.SeatArrangement, API_RESPONSE_STATUS.SUCCESS);
                    listSeatDetails.clear();
                    binding.hsSeat.setVisibility(View.VISIBLE);
                    binding.llSeatFare.setVisibility(View.VISIBLE);

                    listSeatDetails.addAll(seatResponse);

                    IsStopBooking = listSeatDetails.get(0).getIsStopBooking();

                    setSeats();
                    setFareChangeListener();
                    bindBookingTypeRecyclerView();
                } else {
                    addApiList(APP_CONSTANTS.SeatArrangement, API_RESPONSE_STATUS.FAILED);

                    binding.hsSeat.setVisibility(View.GONE);
                    binding.llSeatFare.setVisibility(View.GONE);
                    if (OS_AllowToFetchAllPaxDetail == 1) {
                        binding.llPassengerDetails.setVisibility(View.GONE);
                    }
                    listSeatDetails.clear();

                }
            }
        });

        pickupDropViewModel = new ViewModelProvider(ExeBookingActivity_V2.this).get(PickupDrop_ViewModel.class);
        pickupDropViewModel.pickUpDropLiveData.observe(ExeBookingActivity_V2.this, new Observer<List<PickupDrop_Response.Datum>>() {
            @Override
            public void onChanged(List<PickupDrop_Response.Datum> listPickUpResponse) {
                listPickUp = new ArrayList<>();
                listDropUp = new ArrayList<>();
                totalConsecutiveApiCall--;
                if (listPickUpResponse != null) {
                    addApiList(APP_CONSTANTS.PickupDrop, API_RESPONSE_STATUS.SUCCESS);
                    if (listPickUpResponse.size() > 0) {
                        for (PickupDrop_Response.Datum data : listPickUpResponse) {
                            if (data.getPickupDrop().equalsIgnoreCase("Pickup")) {
                                listPickUp.add(data);
                            } else if (data.getPickupDrop().equalsIgnoreCase("Drop")) {
                                listDropUp.add(data);
                            }
                        }
                    }
                    setPickupDropDefault();
                } else {
                    addApiList(APP_CONSTANTS.PickupDrop, API_RESPONSE_STATUS.FAILED);
                    setPickupDropDefault();
                }
            }
        });


        // Seat Selection Observer
        seatSelectedViewModel = new ViewModelProvider(ExeBookingActivity_V2.this).get(SeatSelected_ViewModel.class);
        seatSelectedViewModel.mutableLiveData.observe(ExeBookingActivity_V2.this, new Observer<List<SelectedSeatDetailsBean>>() {
            @Override
            public void onChanged(List<SelectedSeatDetailsBean> data) {
                //passengerListAdapter.notifyDataSetChanged();

                if (!binding.rvPassengerList.isComputingLayout()) {
                    binding.rvPassengerList.post(new Runnable() {
                        @Override
                        public void run() {
                            passengerListAdapter.notifyDataSetChanged();
                        }
                    });
                }

                if (data != null && data.size() > 0) {
                    binding.llMultipax.setVisibility(View.VISIBLE);
                } else {
                    binding.llMultipax.setVisibility(View.GONE);
                }
                calTotalSeat();
            }
        });


        passengerListAdapter = new BookTicketPassengerListAdapter(ExeBookingActivity_V2.this, selSeats, OS_IsAskIDProof, itsGetIdProofList, new BookTicketPassengerListAdapter.OnTextChangeValidator() {
            @Override
            public void onTextSeatNoChange(List<SelectedSeatDetailsBean> list) {

            }
        });
        binding.rvPassengerList.setAdapter(passengerListAdapter);

        binding.btnClear.setOnClickListener(view17 -> {
            binding.cvBookingType.setVisibility(View.GONE);
            binding.hsSeat.setVisibility(View.GONE);
            clearAfterInsertBooking();
        });

        checkMultipleBooking();


        binding.actvSourceCity.setOnTouchListener(this);
        binding.actvDestCity.setOnTouchListener(this);
        binding.spnBookingType.setOnTouchListener(this);
        binding.btnCouponCode.setOnTouchListener(this);
        binding.btnCompanyCardRedeem.setOnTouchListener(this);
        binding.chkMultipleBooking.setOnTouchListener(this);
        binding.chkGST.setOnTouchListener(this);
        binding.etNoOfSeat.setOnTouchListener(this);

        binding.spnGuest.setOnTouchListener(this);
        //binding.spnBankcard.setOnTouchListener(this);
        binding.spnItsplWallet.setOnTouchListener(this);
        binding.spnSearch.setOnTouchListener(this);

        // Agent Booking
        binding.spnAgentName.setOnTouchListener(this);
        binding.spnAgentCity.setOnTouchListener(this);
        binding.spnAgentRate.setOnTouchListener(this);

        // Branch Booking
        binding.spnBranch.setOnTouchListener(this);
        binding.spnBranchUser.setOnTouchListener(this);

        // Company Card Booking
        binding.btnCompanyCardRedeem.setOnTouchListener(this);
        binding.etCmpBkgCardno.setOnTouchListener(this);


        binding.btnBookTicket.setOnClickListener(view16 -> {
            try {
                getWindow().getDecorView().clearFocus();
            } catch (Exception ignored) { }


            binding.btnBookTicket.requestFocus();
            binding.btnBookTicket.setEnabled(false);
            int backDateFlag = isAllowBackDateBooking();
            if (backDateFlag == 2) {
                showBackDatePromptDialog(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(@NonNull Message message) {
                        if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                            insertBookingOperations(message.what);
                        } else {
                            binding.btnBookTicket.setEnabled(true);
                        }
                        return false;
                    }
                });
            } else {
                insertBookingOperations(backDateFlag);
            }
        });

        binding.spnSearch.setAdapter(new PNRSearchAdapter(ExeBookingActivity_V2.this, searchPNR));

        binding.etCmpCardDisc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable.toString())) {
                    calTotalSeat();
                } else {
                    if (Double.parseDouble(editable.toString()) > cmpCardMaxDiscount) {
                        hideKeyBoard();
                        showSnackBar(oops, "You can redeem max discount " + cmpCardMaxDiscount, Toast.LENGTH_LONG, 1);
                        if (cmpCardMaxDiscount > 0) {
                            binding.etCmpCardDisc.setText(String.valueOf((int) cmpCardMaxDiscount));
                            binding.etCmpCardDisc.setSelection(binding.etCmpCardDisc.getText().toString().length());
                        }
                    } else {
                        calTotalSeat();
                    }
                }

            }
        });

        // Waiting

        binding.txtWaiting.setOnClickListener(view15 -> {
            if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
                showAlreadyLoadTransactionsDialog();
            } else {
                if(routeId!=0 && routeTimeId!=0){
                    waitingListReportApiCall();
                }

            }
        });

        binding.txtStopBooking.setOnClickListener(view12 -> {
            if (bookedByCmpId != 0) {
                if (IsStopBooking == 1) {
                    releaseStopBookingApiCall();
                } else {
                    openStopBookingDialog();
                }
            } else {
                showSnackBar(oops, validRoute, Toast.LENGTH_LONG, 1);
            }
        });

        binding.txtSms.setOnClickListener(view13 -> {
            if (routeId != 0) {
                openChartSMSDialog();
            } else {
                showSnackBar(oops, validRoute, Toast.LENGTH_LONG, 1);
            }
        });

        binding.imgSeatPermanentHold.setOnClickListener(view14 -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }

            if (routeId != 0) {
                openSeatPermanentHoldDialog();
            }
        });

        binding.imgPrevChart.setOnClickListener(view1 -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }
            prevChart();
        });

        binding.imgNextChart.setOnClickListener(view1 -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }
            nextChart();
        });

        binding.txtDriverPickup.setOnClickListener(view1 -> {
            if (listSeatDetails != null && listSeatDetails.size() > 0) {
                openDriverAndPickupManDetailsDialog();
            }
        });

        binding.imgReload.setOnClickListener(view1 -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }
            resetSeatArrangement();
        });

        binding.spnAgentName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ITSGetAllAgentByCompanyID_Model data = itsGetAllAgentByCompanyIDS.get(binding.spnAgentName.getSelectedItemPosition());
                AM_AgentID = data.getAM_AgentID() != null ? data.getAM_AgentID() : 0;
                CAM_OtherChargeType = data.getCAM_OtherChargeType() != null ? data.getCAM_OtherChargeType() : 0;
                CAM_OtherCharge = data.getCAM_OtherCharge() != null ? data.getCAM_OtherCharge() : 0;
                CAM_IsOnline = data.getCAM_IsOnline() != null ? data.getCAM_IsOnline() : 0;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void insertBookingOperations(int backDateFlag) {
        if (haveRightsForHomeCompany() && (backDateFlag == -1 || backDateFlag == 1)) {
            if (checkBookingTypeValidation()) {
                if (checkSeatValidation()) {
                    if (isValidSeatNo() != 1) {
                        binding.btnBookTicket.setEnabled(true);
                        showSnackBar(oops, validSeatNo, Toast.LENGTH_LONG, 1);
                    } else if (Double.parseDouble(binding.etTotalAmount.getText().toString()) <= 0) {
                        binding.btnBookTicket.setEnabled(true);
                        showSnackBar(oops, "Ticket amount must be greater than 0, please set seat fare", Toast.LENGTH_LONG, 1);
                    } else {
                        calTotalSeat();
                        ticketConfirmationDialog();
                    }
                } else {
                    binding.btnBookTicket.setEnabled(true);
                }
            } else {
                binding.btnBookTicket.setEnabled(true);
            }
        } else {
            binding.btnBookTicket.setEnabled(true);
        }
    }

    private void showBackDatePromptDialog(final Handler.Callback callback) {
        if (!isFinishing()) {
            final Message message = new Message();
            try {
                dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
                dialogLoadTransactions.show();
                dialogLoadTransactions.setTitleText("Back Date Seat Booking");
                dialogLoadTransactions.setContentText("Are You Sure , You Want To Book In Back Date?");
                dialogLoadTransactions.setCancelable(false);
                dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
                dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));

                dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                    message.what = APP_CONSTANTS.STATUS_SUCCESS;
                    callback.handleMessage(message);
                });

                dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                    message.what = APP_CONSTANTS.STATUS_FAILED;
                    callback.handleMessage(message);

                });
            } catch (Exception ex) {
                binding.actvSourceCity.setEnabled(true);
            }
        }
    }

    private void releaseBookingTypeIds() {
        GTM_GuestTypeID = 0;
        AM_AgentID = 0;
        ABI_TicketNo = "";
        ABI_NeteRate = 0;
        ABI_TotalSeat = 0;
        ABI_AgentCityID = 0;
        BBI_TicketNo = "";
        BBI_BM_BranchID = 0;
        BBI_UM_UserID = 0;
        bankCardId = 0;
        bankCardRemarks = "";
        PH_TransStatus = 0;
        PH_CardHolderName = "";
        reqPhoneBookingHours = "0";
        reqPhoneBookingMins = "0";
        INSERT_UPDATE_BOOKING_STATUS = 0;

    }

    private void setFareChangeListener() {

        if (AllowFareChnage == 1 && !verifyDoNotAllowFareChangeRoute()) {

            binding.etAcSeat.setEnabled(true);
            binding.etAcSleeper.setEnabled(true);
            binding.etAcSlumber.setEnabled(true);
            binding.etNonacSeat.setEnabled(true);
            binding.etNonacSleeper.setEnabled(true);
            binding.etNonacSlumber.setEnabled(true);

            setFocusChangeListener();

            binding.etAcSeat.addTextChangedListener(getTextWatcher(0, binding.etAcSeat));
            binding.etAcSleeper.addTextChangedListener(getTextWatcher(1, binding.etAcSleeper));
            binding.etAcSlumber.addTextChangedListener(getTextWatcher(2, binding.etAcSlumber));
            binding.etNonacSeat.addTextChangedListener(getTextWatcher(3, binding.etNonacSeat));
            binding.etNonacSleeper.addTextChangedListener(getTextWatcher(4, binding.etNonacSleeper));
            binding.etNonacSlumber.addTextChangedListener(getTextWatcher(5, binding.etNonacSlumber));
        } else {

            binding.etAcSeat.setEnabled(false);
            binding.etAcSleeper.setEnabled(false);
            binding.etAcSlumber.setEnabled(false);
            binding.etNonacSeat.setEnabled(false);
            binding.etNonacSleeper.setEnabled(false);
            binding.etNonacSlumber.setEnabled(false);

            /*binding.etAcSeat.setFocusable(false);
            binding.etAcSleeper.setFocusable(false);
            binding.etAcSlumber.setFocusable(false);
            binding.etNonacSeat.setFocusable(false);
            binding.etNonacSleeper.setFocusable(false);
            binding.etNonacSlumber.setFocusable(false);

            binding.etAcSeat.setClickable(false);
            binding.etAcSleeper.setClickable(false);
            binding.etAcSlumber.setClickable(false);
            binding.etNonacSeat.setClickable(false);
            binding.etNonacSleeper.setClickable(false);
            binding.etNonacSlumber.setClickable(false);

            binding.etAcSeat.setLongClickable(false);
            binding.etAcSleeper.setLongClickable(false);
            binding.etAcSlumber.setLongClickable(false);
            binding.etNonacSeat.setLongClickable(false);
            binding.etNonacSleeper.setLongClickable(false);
            binding.etNonacSlumber.setLongClickable(false);*/

        }
    }

    private boolean verifyDoNotAllowFareChangeRoute() {
        if (itsDoNotAllowFareChangeRouteList != null && itsDoNotAllowFareChangeRouteList.size() > 0) {
            for (ITSDoNotAllowFareChangeInBelowRouteTime_Model data : itsDoNotAllowFareChangeRouteList) {
                if (data.getCM_CompanyID() == bookedByCmpId && data.getRM_RouteID() == routeId && data.getRT_RouteTimeID() == routeTimeId) {
                    return true;
                }
            }
        }
        return false;
    }

    /*private boolean verifyFareChangeOtherCompanyRoute() {
        if (otherCompanyBookingRightsList != null && otherCompanyBookingRightsList.size() > 0) {
            for (ITSGetOtherCompanyBookingRightsExe_Model data : otherCompanyBookingRightsList) {
                if(data.getCM_CompanyID()==cmCmpId && data.getRM_RouteID()==routeId && data.getRT_RouteTimeID()==routeTimeId){
                    return true;
                }
            }
        }
        return false;
    }*/


    private TextWatcher getTextWatcher(final int seat_type, final EditText editText) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String str = editable.toString();
                if (!machine_changed_edittext) {
                    machine_changed_edittext = true;
                    setSeatFarePosition(seat_type, str.length() <= 0 ? 0 : Integer.parseInt(str));
                    editText.setSelection(editText.getText().toString().length());
                    machine_changed_edittext = false;
                }

            }
        };
    }

    private void setSeatFarePosition(int seat_type, int newSeatRate) {
        switch (seat_type) {
            case 0:
                newAcSeatRate = newSeatRate;
                break;
            case 1:
                newAcSlpRate = newSeatRate;
                break;
            case 2:
                newAcSlmbRate = newSeatRate;
                break;
            case 3:
                newNonAcSeatRate = newSeatRate;
                break;
            case 4:
                newNonAcSlpRate = newSeatRate;
                break;
            case 5:
                newNonAcSlmbRate = newSeatRate;
                break;
        }
        calTotalSeat();
    }


    private void networkDetection() {
        NetworkLiveData networkLiveData = new NetworkLiveData(ExeBookingActivity_V2.this);
        networkLiveData.observe(ExeBookingActivity_V2.this, new Observer<NetworkModel>() {
            @Override
            public void onChanged(NetworkModel networkModel) {
                switch (networkModel.getIsConnected()) {
                    case CONNECTED:
                        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
                            bottomSheetDialog.dismiss();
                        }
                        break;
                    case CONNECTING:
                        internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                        internetConnectionBinding.loading.setVisibility(View.VISIBLE);
                        break;
                    case DISCONNECTED:
                        displayNetworkDialog();
                        break;
                    case DISCONNECTING:
                        break;
                    case SUSPENDED:
                        break;
                    case UNKNOWN:
                        break;
                }
            }
        });
    }

    private void displayNetworkDialog() {
        if (!isFinishing() && bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
            bottomSheetDialog.dismiss();
        }

        internetConnectionBinding = LayoutBottomSheetInternetConnectionBinding.inflate(getLayoutInflater());
        // final View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, false);
        final View view = internetConnectionBinding.getRoot();
        internetConnectionBinding.btnCheckNet.setOnClickListener(view1 -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }

            internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
            internetConnectionBinding.loading.setVisibility(View.VISIBLE);

            Handler mHand = new Handler(Looper.getMainLooper());
            mHand.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (!cd.isConnectingToInternet()) {
                        internetConnectionBinding.btnCheckNet.setVisibility(View.VISIBLE);
                        internetConnectionBinding.loading.setVisibility(View.GONE);
                        showSnackBar(oops, internetShortMsg, Toast.LENGTH_LONG, 1);
                    } else {
                        internetConnectionBinding.btnCheckNet.setVisibility(View.GONE);
                        internetConnectionBinding.loading.setVisibility(View.VISIBLE);
                    }
                }
            }, 2000);

        });

        //View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, null);
        bottomSheetDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);


    }

    private void bindPassengerList() {
        passengerListAdapter = new BookTicketPassengerListAdapter(ExeBookingActivity_V2.this, selSeats, OS_IsAskIDProof, itsGetIdProofList, list -> {
            for (SeatArrangement_Response.Datum data : listSeatDetails) {
                if (data.getSeatButton() != null) {
                    setSeatBackground(data.getSeatButton(), data.getBookingTypeColor(), -1);
                    data.getSeatButton().setSelected(false);
                }
                for (int i = 0; i < selSeats.size(); i++) {
                    if (!TextUtils.isEmpty(data.getBADSeatNo()) && !TextUtils.isEmpty(selSeats.get(i).getBADSeatNo()) && data.getBADSeatNo().equalsIgnoreCase(selSeats.get(i).getBADSeatNo())) {

                        selSeats.get(i).setSeatRate((int) data.getSeatRate());
                        selSeats.get(i).setIsIncludeTax(data.getIsIncludeTax());
                        selSeats.get(i).setCADBusType(data.getCADBusType());
                        selSeats.get(i).setCADSeatType(data.getCADSeatType());
                        selSeats.get(i).setSeatButton(data.getSeatButton());

                        if (data.getSeatButton() != null) {
                            setSeatBackground(data.getSeatButton(), APP_CONSTANTS.SELECTED_SEAT_COLOR, -1);
                            data.getSeatButton().setSelected(true);
                        }
                        break;
                    }
                }
            }
            calTotalSeat();
        });
        binding.rvPassengerList.setAdapter(passengerListAdapter);

    }

    private void CouponCodeDetailsApiCall() {
        discountAmount = 0.0;
        listDiscountCopon = new ArrayList<>();

        binding.txtDiscount.setText("");
        binding.txtMinBookAmount.setText("");

        showProgressDialog("Loading....");


        CouponCodeDetails_Request request = new CouponCodeDetails_Request(
                bookedByCmpId,
                branchId,
                branchUserId,
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                bookingTypeId,
                str_journeyDate,
                binding.txtCouponCode.getText().toString().trim(),
                1,
                "",
                AllowMultipleBooking,
                arrangementId
        );

       /* CouponCodeDetails_Request request = new CouponCodeDetails_Request(
                getPref.getCM_CompanyID(),
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                bookingTypeId,
                str_journeyDate,
                binding.txtCouponCode.getText().toString().trim(),
                1,
                "",
                1,
                510
        );*/


        Call<CouponCodeDetails_Response> call = apiService.CouponCodeDetails(request);
        call.enqueue(new Callback<CouponCodeDetails_Response>() {
            @Override
            public void onResponse(@NotNull Call<CouponCodeDetails_Response> call, @NotNull Response<CouponCodeDetails_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() != 0 && response.body().getData() != null && response.body().getData().size() > 0) {
                        listDiscountCopon = response.body().getData();
                    } else {
                        binding.txtMinBookAmount.setVisibility(View.VISIBLE);
                        binding.txtMinBookAmount.setTextColor(getResources().getColor(R.color.bg_button_light));
                        binding.txtMinBookAmount.setText("Invalid coupon code");
                    }
                    calTotalSeat();
                } else {
                    showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "CouponCodeDetails", oops, getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<CouponCodeDetails_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "CouponCodeDetails", oops, getResources().getString(R.string.bad_server_response));
            }
        });

    }

    private void calDiscount() {
        double maxDiscount = 0.0;
        discountAmount = 0;
        discountSeatString = "";
        double minimumBookingAmount = 0;
        binding.txtMinBookAmount.setVisibility(View.GONE);
        binding.txtMinBookAmount.setTextColor(getResources().getColor(R.color.color_blue));
        binding.txtDiscount.setVisibility(View.INVISIBLE);
        if (!TextUtils.isEmpty(binding.txtCouponCode.getText().toString().trim())) {
            for (CouponCodeDetails_Response.Datum data : listDiscountCopon) {
                if (!TextUtils.isEmpty(data.getCCMaxDisCount())) {
                    maxDiscount = Double.parseDouble(data.getCCMaxDisCount());
                }
                if (data.getCCCouponCode().equalsIgnoreCase(binding.txtCouponCode.getText().toString().trim())) {
                    minimumBookingAmount = data.getCCMinimumAmount();
                    if (data.getISSpecialDiscountScheme() == 0) {
                        // Regular Case
                        if (data.getCCDiscountType() == 1) {
                            // Percentage
                            int tillSeatDiscount = data.getTillSeatDiscount();
                            if (tillSeatDiscount > 0 && totalBaseFare > data.getCCMinimumAmount() && totalPax > 0 && totalPax <= data.getMaxSeat()) {
                                for (int i = 0; i < selSeats.size(); i++) {
                                    if (tillSeatDiscount > i) {
                                        discountAmount += ((selSeats.get(i).getSeatRate() * data.getDisAmtPer()) / 100);
                                    }
                                }
                            }
                        } else {
                            // Rs
                            if (totalBaseFare >= data.getCCMinimumAmount() && totalPax <= data.getMaxSeat()) {
                                discountAmount += data.getCCAmount();
                            }
                        }
                    } else {
                        // Special Case
                        if (data.getISSpecialDiscountScheme() == 4) {
                            discountSeatString = data.getJMSeatString();
                        }
                        if (data.getCCDisType() == 1) {
                            int tillSeatDiscount = data.getTillSeatDiscount();
                            if (tillSeatDiscount > 0 && totalBaseFare > data.getCCMinimumAmount() && totalPax > 0 && totalPax <= data.getMaxSeat()) {
                                for (int i = 0; i < selSeats.size(); i++) {
                                    if (tillSeatDiscount > i) {
                                        discountAmount += ((selSeats.get(i).getSeatRate() * data.getDisAmtPer()) / 100);
                                    }
                                }
                            }
                        } else {
                            if (totalBaseFare >= data.getCCMinimumAmount() && totalPax <= data.getMaxSeat()) {
                                discountAmount += data.getCCAmount();
                            }
                        }

                    }

                }
            }

        }

        if (maxDiscount > 0 && discountAmount > maxDiscount) {
            discountAmount = maxDiscount;
        }

        if (discountAmount > 0) {
            binding.txtDiscount.setVisibility(View.VISIBLE);
            binding.txtDiscount.setText("Disc. Rs: " + String.format(Locale.getDefault(), "%.2f", discountAmount));
        } else {
            binding.txtDiscount.setVisibility(View.INVISIBLE);
            binding.txtDiscount.setText("Disc. Rs: " + String.format(Locale.getDefault(), "%.2f", discountAmount));
        }

        if (minimumBookingAmount > 0) {
            binding.txtMinBookAmount.setVisibility(View.VISIBLE);
            binding.txtMinBookAmount.setText("Minimum Booking Amount: " + getResources().getString(R.string.rs) + minimumBookingAmount);
        }

    }

    private void clearCouponCode() {
        listDiscountCopon = new ArrayList<>();
        binding.txtCouponCode.setText("");
        discountAmount = 0;
        binding.txtDiscount.setText("Disc. Rs: " + String.format(Locale.getDefault(), "%.2f", discountAmount));
        binding.txtMinBookAmount.setText("");
        calDiscount();
    }

    private void routeTimeXmlApiResponse(List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> response) {
        try {
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    appDatabase.getApiExeRouteTimeXMLDao()
                            .insertAllGet_API_EXE_RouteTimeXML(response);
                    fromCityList = appDatabase.getApiExeRouteTimeXMLDao().getFromCity();
                    sel_SourceCityName = appDatabase.getApiExeRouteTimeXMLDao().getCityName(String.valueOf(cityId));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fromCityBinding();
                            isLoadApi(2);
                        }
                    });
                }
            });
        } catch (Exception ex) {
            disMissDialog();
        }
    }

    private void routeXmlApiRequest() {
        Get_API_EXE_RouteTimeXML_Request request = new Get_API_EXE_RouteTimeXML_Request(
                getPref.getCM_CompanyID(),
                getPref.getBM_BranchID()
        );
        routeTimeXMLViewModel.onCreateRequest(request);
    }

    private void routeTimeAllXmlRequest() {
        final Get_API_EXE_RouteTimeALLXML_Request request = new Get_API_EXE_RouteTimeALLXML_Request(
                getPref.getCM_CompanyID(),
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                getPref.getCM_CityID(),
                getPref.getVersionName(),
                getPref.getTBRM_ID(),
                "",
                0
        );
        routeTimeAllXMLViewModel.onCreateRequest(request);
    }

    private void routeTimeAllXmlApiResponse(Response<Get_API_EXE_RouteTimeALLXML_Response> response) {
        if (response.body().getStatus() != null && response.body().getStatus() != 0) {

            if (response.body().getData().getITSGetBookingTypeByCompanyIDXML() != null && response.body().getData().getITSGetBookingTypeByCompanyIDXML().size() > 0) {
                bookingTypeList = response.body().getData().getITSGetBookingTypeByCompanyIDXML();
            }

            if (response.body().getData().getITSGetExtraRouteTime() != null && response.body().getData().getITSGetExtraRouteTime().size() > 0) {
                extraRouteTimeList = response.body().getData().getITSGetExtraRouteTime();
            }

            if (response.body().getData().getITSBusTypeColorCode() != null && response.body().getData().getITSBusTypeColorCode().size() > 0) {
                busTypeColorCodeList = response.body().getData().getITSBusTypeColorCode();
            }

            final List<ITSGetAllAgentByCompanyID_Model> itsGetAllAgentByCompanyIDModelList = new ArrayList<>();
            if (response.body().getData().getITSGetAllAgentByCompanyID() != null && response.body().getData().getITSGetAllAgentByCompanyID().size() > 0) {

                for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetAllAgentByCompanyID data : response.body().getData().getITSGetAllAgentByCompanyID()) {
                    ITSGetAllAgentByCompanyID_Model dataAgent = new ITSGetAllAgentByCompanyID_Model();

                    dataAgent.setAM_AgentID(data.getAMAgentID());
                    dataAgent.setAM_AgentName(data.getAMAgentName());
                    dataAgent.setAM_PhoneNo1(data.getAMPhoneNo1());
                    dataAgent.setCAM_AgentCode(data.getCAMAgentCode());
                    dataAgent.setCAM_IsOnline(data.getCAMIsOnline());
                    dataAgent.setCAM_OtherCharge(data.getCAMOtherCharge());
                    dataAgent.setCAM_OtherChargeType(data.getCAMOtherChargeType());
                    dataAgent.setCM_CityId(data.getCMCityId());
                    dataAgent.setCM_CityName(data.getCMCityName());
                    dataAgent.setIS_DisplayOnAgentList(data.getISDisplayOnAgentList());
                    dataAgent.setIsAgentCredit(data.getIsAgentCredit());


                    itsGetAllAgentByCompanyIDModelList.add(dataAgent);
                }
            }

            // Guest Type
            final List<ITSGetGuestTypeByCompanyID_Model> listGuestMaster = new ArrayList<>();
            if (response.body().getData().getITSGetGuestTypeByCompanyID() != null && response.body().getData().getITSGetGuestTypeByCompanyID().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetGuestTypeByCompanyID data : response.body().getData().getITSGetGuestTypeByCompanyID()) {
                    ITSGetGuestTypeByCompanyID_Model dataGuest = new ITSGetGuestTypeByCompanyID_Model();
                    dataGuest.setCM_CompanyID(data.getCMCompanyID());
                    dataGuest.setGTM_GuestTypeID(data.getGTMGuestTypeID());
                    dataGuest.setGTM_GuestName(data.getGTMGuestName());
                    listGuestMaster.add(dataGuest);
                }
            }

            // Branch Master

            final List<ITSBranchMasterSelectXML_Model> itsBranchMaster = new ArrayList<>();
            if (response.body().getData().getITSBranchMasterSelectXML() != null && response.body().getData().getITSBranchMasterSelectXML().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSBranchMasterSelectXML data : response.body().getData().getITSBranchMasterSelectXML()) {
                    ITSBranchMasterSelectXML_Model dataBranch = new ITSBranchMasterSelectXML_Model();

                    dataBranch.setBM_BranchID(data.getBMBranchID());
                    dataBranch.setBM_BranchName(data.getBMBranchName());
                    dataBranch.setBM_BranchCode(data.getBMBranchCode());
                    dataBranch.setBM_UserName(data.getBMUserName());
                    dataBranch.setBM_Password(data.getBMPassword());
                    dataBranch.setBM_BranchManager(data.getBMBranchManager());
                    dataBranch.setBM_ContactNo(data.getBMContactNo());
                    dataBranch.setBM_ContactNo1(data.getBMContactNo1());
                    dataBranch.setCM_CountryID(data.getCMCountryID());
                    dataBranch.setSM_StateID(data.getSMStateID());
                    dataBranch.setCM_CityID(data.getCMCityID());
                    dataBranch.setBM_Address(data.getBMAddress());
                    dataBranch.setPM_PickupID(data.getPMPickupID());
                    dataBranch.setPM_PickupName(data.getPMPickupName());
                    dataBranch.setBM_PhoneNo(data.getBMPhoneNo());
                    dataBranch.setBM_CommissionAmt(data.getBMCommissionAmt());
                    dataBranch.setBM_CommissionPer(data.getBMCommissionPer());
                    dataBranch.setBM_NoOfUsers(data.getBMNoOfUsers());
                    dataBranch.setCM_CompanyID(data.getCMCompanyID());
                    dataBranch.setBM_CreatedDate(data.getBMCreatedDate());
                    dataBranch.setBM_IsActive(data.getBMIsActive());
                    dataBranch.setBM_AutoDeletePhoneHour(data.getBMAutoDeletePhoneHour());
                    dataBranch.setBM_AutoDeletePhoneMin(data.getBMAutoDeletePhoneMin());
                    dataBranch.setBM_IsPrepaid(data.getBMIsPrepaid());
                    dataBranch.setBM_AutoAvailability(data.getBMAutoAvailability());

                    itsBranchMaster.add(dataBranch);

                    if (dataBranch != null && branchId == dataBranch.getBM_BranchID()) {
                        BM_IsPrepaid = data.getBMIsPrepaid();
                    }


                }
            }


            // Branch User Id

            final List<ITSGetBranchUserByCompanyID_Model> itsBranchUser = new ArrayList<>();
            if (response.body().getData().getITSGetBranchUserByCompanyID() != null && response.body().getData().getITSGetBranchUserByCompanyID().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetBranchUserByCompanyID data : response.body().getData().getITSGetBranchUserByCompanyID()) {
                    ITSGetBranchUserByCompanyID_Model dataBranchUser = new ITSGetBranchUserByCompanyID_Model();

                    dataBranchUser.setBUM_BranchUserID(data.getBUMBranchUserID());
                    dataBranchUser.setBM_BranchID(data.getBMBranchID());
                    dataBranchUser.setBUM_UserName(data.getBUMUserName());
                    dataBranchUser.setBUM_Password(data.getBUMPassword());
                    dataBranchUser.setBUM_Position(data.getBUMPosition());
                    dataBranchUser.setBUM_FullName(data.getBUMFullName());
                    dataBranchUser.setBUM_Address(data.getBUMAddress());
                    dataBranchUser.setBUM_ContactNo(data.getBUMContactNo());
                    dataBranchUser.setBUM_Photo(data.getBUMPhoto());
                    dataBranchUser.setSM_StateID(data.getSMStateID());
                    dataBranchUser.setCM_CityID(data.getCMCityID());
                    dataBranchUser.setBUM_ReferenceBy(data.getBUMReferenceBy());
                    dataBranchUser.setBUM_ContactNoOfReference(data.getBUMContactNoOfReference());
                    dataBranchUser.setBUM_Remarks(data.getBUMRemarks());
                    dataBranchUser.setBUM_BirthDate(data.getBUMBirthDate());
                    dataBranchUser.setBUM_BranchCode(data.getBUMBranchCode());
                    dataBranchUser.setBUM_CreatedDate(data.getBUMCreatedDate());
                    dataBranchUser.setBUM_IsActive(data.getBUMIsActive());
                    dataBranchUser.setBUM_Agent_Number(data.getBUMAgentNumber());

                    if (dataBranchUser.getBM_BranchID() != null && data.getBUMBranchUserID() != null &&
                            branchId == dataBranchUser.getBM_BranchID() && branchUserId == data.getBUMBranchUserID()) {
                        BUM_Agent_Number = data.getBUMAgentNumber();
                        Log.e("BUM_Agent_Number", "" + BUM_Agent_Number);
                    }

                    itsBranchUser.add(dataBranchUser);

                }
            }

            // ITSPl Wallet

            final List<ITSOnlineWallet_Model> itsOnlineWalletModelList = new ArrayList<>();
            if (response.body().getData().getITSOnlineWallet() != null && response.body().getData().getITSOnlineWallet().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSOnlineWallet data : response.body().getData().getITSOnlineWallet()) {
                    ITSOnlineWallet_Model dataWallet = new ITSOnlineWallet_Model();
                    dataWallet.setWCM_ID(data.getWCMID());
                    dataWallet.setWCM_CompanyName(data.getWCMCompanyName());
                    dataWallet.setWCD_TDRCharges(data.getWCDTDRCharges());
                    dataWallet.setWCD_WalletKey(data.getWCDWalletKey());
                    dataWallet.setWCM_IsOTP(data.getWCMIsOTP());
                    itsOnlineWalletModelList.add(dataWallet);
                }
            }

            // ITS_Get_BookingRights_ByUser

            itsGetBookingRightsByUsers = new ArrayList<>();

            if (response.body().getData().getITSGetBookingRightsByUser() != null && response.body().getData().getITSGetBookingRightsByUser().size() > 0) {
                itsGetBookingRightsByUsers = response.body().getData().getITSGetBookingRightsByUser();

                bookingRightsByUserData = itsGetBookingRightsByUsers.get(0);

                Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingRightsByUser data = itsGetBookingRightsByUsers.get(0);

                IsMultipleChecked = data.getGAAllowMultipleBooking();
                AllowFareChnage = data.getGAAllowFareChange();
                AllowLessFareChange = data.getGAAllowLessFareChange();
                AllowFareChangeInPhoneModify = data.getGAAllowFareChangeInPhoneModify();
                GA_AllowBackDateBooking = data.getGAAllowBackDateBooking();
                GA_ShowBackDate = data.getGAShowBackDate();
                GA_AllowBackDateCancellation = data.getGAAllowBackDateCancellation();
                GA_IsPromptBackDateBooking = data.getGAIsPromptBackDateBooking();
                GA_AllowToSetLessPhoneCancelTime = data.getGAAllowToSetLessPhoneCancelTime();
                GA_AllowChangeRefundCharges = data.getGAAllowChangeRefundCharges();
                GA_AllowLessRefundCharges = data.getGAAllowLessRefundCharges();
                GA_AllowReturnPhoneBooking = data.getGAAllowReturnPhoneBooking();
                BR_AskChargeOnModify = data.getBRAskChargeOnModify();
                GA_AllowMultyDateAgentQuota_Exe = data.getGAAllowMultyDateAgentQuotaExe();
                GA_AllowPhoneOnHoldSeat = data.getGAAllowPhoneOnHoldSeat();

                GA_AllowReprint = data.getGAAllowReprint();
                IsBookingLog = data.getIsBookingLog();
                GA_IsShowFeedBack = data.getGAIsShowFeedBack();
                GA_AllowConfirmToOpen = data.getGAAllowConfirmToOpen();

            }

            // ITSGetDisplayRouteBeforeMin

            //final List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetDisplayRouteBeforeMin> itsRouteBeforeMinModelList = new ArrayList<>();
            if (response.body().getData().getITSGetDisplayRouteBeforeMin() != null && response.body().getData().getITSGetDisplayRouteBeforeMin().size() > 0) {
                itsGetDisplayRouteBeforeMinModelsList = response.body().getData().getITSGetDisplayRouteBeforeMin();
                itsGetDisplayRouteBeforeMin = itsGetDisplayRouteBeforeMinModelsList.get(0);

                getDateList(itsGetDisplayRouteBeforeMin.getCDHBackDateShow(), itsGetDisplayRouteBeforeMin.getCDHAdvanceDateShow());

                CDH_BackDateShow = itsGetDisplayRouteBeforeMin.getCDHBackDateShow();
                CDH_AdvanceDateShow = itsGetDisplayRouteBeforeMin.getCDHAdvanceDateShow();
                OS_MaxCompanyCardBooking = itsGetDisplayRouteBeforeMin.getOSMaxCompanyCardBooking();
                OS_AllowCoupanCode = itsGetDisplayRouteBeforeMin.getOSAllowCoupanCode();
                OS_CouponCodeMaxSeatBook = itsGetDisplayRouteBeforeMin.getOSCouponCodeMaxSeatBook();
                OS_AllowToFetchAllPaxDetail = itsGetDisplayRouteBeforeMin.getOSAllowToFetchAllPaxDetail();
                OSIsPendingAmount = itsGetDisplayRouteBeforeMin.getOSIsPendingAmount();
                ISCountTableInBooked = itsGetDisplayRouteBeforeMin.getISCountTableInBooked();
                OS_DisplayBookSeatFareOnChart = itsGetDisplayRouteBeforeMin.getOSDisplayBookSeatFareOnChart();
                OS_DisplayTotalFareOnChart = itsGetDisplayRouteBeforeMin.getOSDisplayTotalFareOnChart();
                OS_GetExtraValueOnFare = itsGetDisplayRouteBeforeMin.getOSGetExtraValueOnFare();
                OS_IsAskIDProof = itsGetDisplayRouteBeforeMin.getOSIsAskIDProof();
                OS_IsIDProofMandatory = itsGetDisplayRouteBeforeMin.getOSIsIDProofMandatory();
                OS_IsCrossStateIDProofMandatory = itsGetDisplayRouteBeforeMin.getOSIsCrossStateIDProofMandatory();
                CDH_ShowStopBooking = itsGetDisplayRouteBeforeMin.getCDHShowStopBooking();


                CM_IsACServiceTax = itsGetDisplayRouteBeforeMin.getCMIsACServiceTax();
                CM_IsNonACServiceTax = itsGetDisplayRouteBeforeMin.getCMIsNonACServiceTax();
                CM_ACFareIncludeTax = itsGetDisplayRouteBeforeMin.getCMACFareIncludeTax();
                CM_NonACFareIncludeTax = itsGetDisplayRouteBeforeMin.getCMNonACFareIncludeTax();

                CM_ACServiceTax = itsGetDisplayRouteBeforeMin.getCMACServiceTax();
                CM_NonACServiceTax = itsGetDisplayRouteBeforeMin.getCMNonACServiceTax();
                CM_ServiceTaxRoundUp = itsGetDisplayRouteBeforeMin.getCMServiceTaxRoundUp();

                CM_ACPackageServiceTax = itsGetDisplayRouteBeforeMin.getCMACPackageServiceTax();
                CM_NonACPackageServiceTax = itsGetDisplayRouteBeforeMin.getCMNonACPackageServiceTax();

                CM_ACOwnScheduleServiceTax = itsGetDisplayRouteBeforeMin.getCMACOwnScheduleServiceTax();
                CM_NonACOwnScheduleServiceTax = itsGetDisplayRouteBeforeMin.getCMNonACOwnScheduleServiceTax();

                CM_ACOtherOwnScheduleServiceTax = itsGetDisplayRouteBeforeMin.getCMACOtherOwnScheduleServiceTax();
                CM_NonACOtherOwnScheduleServiceTax = itsGetDisplayRouteBeforeMin.getCMNonACOtherOwnScheduleServiceTax();

                CDH_AutoDeletePhoneTime = itsGetDisplayRouteBeforeMin.getCDHAutoDeletePhoneTime();
                CDH_AutoDeleteHour = itsGetDisplayRouteBeforeMin.getCDHAutoDeleteHour();
                CDH_AutoDeleteMinute = itsGetDisplayRouteBeforeMin.getCDHAutoDeleteMinute();
                CDH_AutoDeletePhoneBookMinutes = itsGetDisplayRouteBeforeMin.getCDHAutoDeletePhoneBookMinutes();
                OS_PhoneBookingToRemotePayment = itsGetDisplayRouteBeforeMin.getOSPhoneBookingToRemotePayment();


                OS_DisplayNetRate = itsGetDisplayRouteBeforeMin.getOSDisplayNetRate();

                OS_AskOTPOnModifyBooking = itsGetDisplayRouteBeforeMin.getOSAskOTPOnModifyBooking();
                OS_AskOTPOnPhoneBooking = itsGetDisplayRouteBeforeMin.getOSAskOTPOnPhoneBooking();
                OS_AskOTPOnBankCardBooking = itsGetDisplayRouteBeforeMin.getOSAskOTPOnBankCardBooking();

                OS_HoldGeneralAgent = itsGetDisplayRouteBeforeMin.getOSHoldGeneralAgent();
                OS_HoldGuest = itsGetDisplayRouteBeforeMin.getOSHoldGuest();
                OS_HoldCompanyCard = itsGetDisplayRouteBeforeMin.getOSHoldCompanyCard();
                OS_HoldBankCard = itsGetDisplayRouteBeforeMin.getOSHoldBankCard();

                OS_ReprintGeneralAgent = itsGetDisplayRouteBeforeMin.getOSReprintGeneralAgent();
                OS_EmailConfirm = itsGetDisplayRouteBeforeMin.getOSEmailConfirm();
                OS_EmailPhone = itsGetDisplayRouteBeforeMin.getOSEmailPhone();
                OS_EmailAgent = itsGetDisplayRouteBeforeMin.getOSEmailAgent();
                OS_EmailBranch = itsGetDisplayRouteBeforeMin.getOSEmailBranch();
                OS_EmailGuest = itsGetDisplayRouteBeforeMin.getOSEmailGuest();
                OS_EmailBankCard = itsGetDisplayRouteBeforeMin.getOSEmailBankCard();
                OS_EmailCompanyCard = itsGetDisplayRouteBeforeMin.getOSEmailCompanyCard();
                OS_EmailWaiting = itsGetDisplayRouteBeforeMin.getOSEmailWaiting();
                OS_EmailAgentQuata = itsGetDisplayRouteBeforeMin.getOSEmailAgentQuata();
                OS_EmailOpen = itsGetDisplayRouteBeforeMin.getOSEmailOpen();
                OS_EmailGeneralAgent = itsGetDisplayRouteBeforeMin.getOSEmailGeneralAgent();
                OS_EmailOnlineAgent = itsGetDisplayRouteBeforeMin.getOSEmailOnlineAgent();
                OS_EmailAPIAgent = itsGetDisplayRouteBeforeMin.getOSEmailAPIAgent();
                OS_EmailB2CAgent = itsGetDisplayRouteBeforeMin.getOSEmailB2CAgent();
                OS_EmailOnlineWallet = itsGetDisplayRouteBeforeMin.getOSEmailOnlineWallet();
                CM_IsSMSActive = itsGetDisplayRouteBeforeMin.getCMIsSMSActive();

                OS_PDFConfirm = itsGetDisplayRouteBeforeMin.getOSPDFConfirm();
                OS_PDFPhone = itsGetDisplayRouteBeforeMin.getOSPDFPhone();
                OS_PDFGeneralAgent = itsGetDisplayRouteBeforeMin.getOSPDFGeneralAgent();
                OS_PDFOnlineAgent = itsGetDisplayRouteBeforeMin.getOSPDFOnlineAgent();
                OS_PDFAPIAgent = itsGetDisplayRouteBeforeMin.getOSPDFAPIAgent();
                OS_PDFB2CAgent = itsGetDisplayRouteBeforeMin.getOSPDFB2CAgent();
                OS_PDFBranch = itsGetDisplayRouteBeforeMin.getOSPDFBranch();
                OS_PDFGuest = itsGetDisplayRouteBeforeMin.getOSPDFGuest();
                OS_PDFBankCard = itsGetDisplayRouteBeforeMin.getOSPDFBankCard();
                OS_PDFCompanyCard = itsGetDisplayRouteBeforeMin.getOSPDFCompanyCard();
                OS_PDFAgentQuata = itsGetDisplayRouteBeforeMin.getOSPDFAgentQuata();
                OS_PDFOnlineWallet = itsGetDisplayRouteBeforeMin.getOSPDFOnlineWallet();

                CDH_IsCallCenterAPI = itsGetDisplayRouteBeforeMin.getCDHIsCallCenterAPI();

            }

            // ITS_FetchChartForeColor

            itsFetchChartForeColorModelList = new ArrayList<>();
            if (response.body().getData().getITSFetchChartForeColor() != null && response.body().getData().getITSFetchChartForeColor().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSFetchChartForeColor data : response.body().getData().getITSFetchChartForeColor()) {
                    ITSFetchChartForeColor_Model colorModel = new ITSFetchChartForeColor_Model();

                    colorModel.setES_ConfColor(data.getESConfColor());
                    colorModel.setES_PhColor(data.getESPhColor());
                    colorModel.setES_AgColor(data.getESAgColor());
                    colorModel.setES_AgQuotaColor(data.getESAgQuotaColor());
                    colorModel.setES_GuestColor(data.getESGuestColor());
                    colorModel.setES_BankC(data.getESBankC());
                    colorModel.setES_CompC(data.getESCompC());
                    colorModel.setES_BranchColor(data.getESBranchColor());
                    colorModel.setES_OnlineAgentColor(data.getESOnlineAgentColor());
                    colorModel.setES_B2CColor(data.getESB2CColor());
                    colorModel.setES_APiColor(data.getESAPiColor());
                    colorModel.setES_OnlineAgentPhoneColor(data.getESOnlineAgentPhoneColor());
                    colorModel.setES_OnlineAgentCardColor(data.getESOnlineAgentCardColor());
                    colorModel.setES_OnlineWallet(data.getESOnlineWallet());
                    colorModel.setES_RemotePayment(data.getESRemotePayment());

                    itsFetchChartForeColorModelList.add(colorModel);
                }
            }

            // Company Marquee

            itsGetCompanyMarqueeModelList = new ArrayList<>();

            if (response.body().getData().getITSGetCompanyMarquee() != null && response.body().getData().getITSGetCompanyMarquee().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetCompanyMarquee data : response.body().getData().getITSGetCompanyMarquee()) {
                    ITSGetCompanyMarquee_Model marqueeModel = new ITSGetCompanyMarquee_Model();
                    marqueeModel.setRM_RouteID(data.getRMRouteID());
                    marqueeModel.setRT_RouteTimeID(data.getRTRouteTimeID());
                    marqueeModel.setMm_message(data.getMmMessage());
                    marqueeModel.setJM_FromDateint(data.getJMFromDateint());
                    marqueeModel.setJM_ToDateint(data.getJMToDateint());

                    itsGetCompanyMarqueeModelList.add(marqueeModel);

                }
            }

            // ITS Marquee

            itsGetITSMarqueeModelList = new ArrayList<>();

            if (response.body().getData().getITSGetITSMarquee() != null && response.body().getData().getITSGetITSMarquee().size() > 0) {
                itsGetITSMarqueeModelList = response.body().getData().getITSGetITSMarquee();
            }


            // ITS_DoNotAllowFareChangeInBelowRouteTime

            itsDoNotAllowFareChangeRouteList = new ArrayList<>();

            if (response.body().getData().getITSDoNotAllowFareChangeInBelowRouteTime() != null && response.body().getData().getITSDoNotAllowFareChangeInBelowRouteTime().size() > 0) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSDoNotAllowFareChangeInBelowRouteTime data : response.body().getData().getITSDoNotAllowFareChangeInBelowRouteTime()) {
                    ITSDoNotAllowFareChangeInBelowRouteTime_Model dataDonotAllowed = new ITSDoNotAllowFareChangeInBelowRouteTime_Model();
                    dataDonotAllowed.setCM_CompanyID(data.getCMCompanyID());
                    dataDonotAllowed.setRM_RouteID(data.getRMRouteID());
                    dataDonotAllowed.setRT_RouteTimeID(data.getRTRouteTimeID());

                    itsDoNotAllowFareChangeRouteList.add(dataDonotAllowed);

                }
            }

            // ITS_GetOtherCompanyBookingRights_Exe

            otherCompanyBookingRightsList = new ArrayList<>();

            if (response.body().getData().getITSGetOtherCompanyBookingRightsExe() != null && response.body().getData().getITSGetOtherCompanyBookingRightsExe().size() > 0) {
                otherCompanyBookingRightsList = response.body().getData().getITSGetOtherCompanyBookingRightsExe();
            }

            // ITS_RouteTimeExtraFareGet


            if (response.body().getData().getITSRouteTimeExtraFareGet() != null && response.body().getData().getITSRouteTimeExtraFareGet().size() > 0) {
                itsRouteTimeExtraFareGetList = response.body().getData().getITSRouteTimeExtraFareGet();
            }
            if (response.body().getData().getITSGetIdProof() != null && response.body().getData().getITSGetIdProof().size() > 0) {
                itsGetIdProofList = response.body().getData().getITSGetIdProof();
            }
            if (response.body().getData().getITSState() != null && response.body().getData().getITSState().size() > 0) {
                itsStateList = response.body().getData().getITSState();
            }

            if (response.body().getData().getITSCancelremarks() != null && response.body().getData().getITSCancelremarks().size() > 0) {
                itsCancelremarkList = response.body().getData().getITSCancelremarks();
            }

            if (response.body().getData().getITSRemarkModify() != null && response.body().getData().getITSRemarkModify().size() > 0) {
                itsRemarkModifyList = response.body().getData().getITSRemarkModify();
            }

            if (response.body().getData().getITSRemarkRePrint() != null && response.body().getData().getITSRemarkRePrint().size() > 0) {
                itsRemarkRePrintList = response.body().getData().getITSRemarkRePrint();
            }


            try {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        // Agent Type
                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetAllAgentByCompanyID(itsGetAllAgentByCompanyIDModelList);

                        // Booking Type
                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetBookingTypeByCompanyIDXML(bookingTypeList);

                        //bookingTypeList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSGetBookingTypeByCompanyIDXML();

                        // Branch Type

                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSBranchMasterSelectXML(itsBranchMaster);

                        appDatabase.getApiExeRouteTimeALLXMLDao()
                                .insertITSGetBranchUserByCompanyID(itsBranchUser);

                        // Guest Type

                        appDatabase.getApiExeRouteTimeALLXMLDao()
                                .insertITSGetGuestTypeByCompanyID(listGuestMaster);

                        // ITSPL Wallet Type

                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSOnlineWallet(itsOnlineWalletModelList);

                        // ITSGetDisplayRouteBeforeMin

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetDisplayRouteBeforeMin(itsRouteBeforeMinModelList);

                        // ITSFetchChartForeColor
                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSFetchChartForeColor(itsFetchChartForeColorModelList);

                        // Company Marquee - ITS Marquee

                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetCompanyMarquee(itsGetCompanyMarqueeModelList);

                        //appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetITSMarquee(itsGetITSMarqueeModelList);

                        itsGetCompanyMarqueeModelList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSGetCompanyMarquee(reqDate, routeId, routeTimeId);

                        // itsFetchChartForeColorModelList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSFetchChartForeColor();


                        // ItsGetBookingRightsByUsers

                        //  appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetBookingRightsByUser(itsGetBookingRightsByUsers);

                        // ITSDoNotAllowFareChangeInBelowRouteTime

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSDoNotAllowFareChangeInBelowRouteTime(itsDoNotAllowFareChangeRouteList);

                        // ITS_GetOtherCompanyBookingRights_Exe

                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetOtherCompanyBookingRightsExe(otherCompanyBookingRightsList);

                        // ITS_RouteTimeExtraFareGet

                        appDatabase.getApiExeRouteTimeALLXMLDao().insertITSRouteTimeExtraFareGet(itsRouteTimeExtraFareGetList);

                        // ITS_GetIdProof

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSGetIdProof(itsGetIdProofList);

                        // ITS_State

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSState(itsStateList);

                        // ITSCancelremark

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSCancelremark(itsCancelremarkList);


                        // ITS_RemarkModify

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSRemarkModify(itsRemarkModifyList);


                        // ITSRemarkRePrint

                        // appDatabase.getApiExeRouteTimeALLXMLDao().insertITSRemarkRePrint(itsRemarkRePrintList);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (bookingTypeList != null && bookingTypeList.size() > 0) {
                                    binding.spnBookingType.setAdapter(new BookingTypeAdapter(ExeBookingActivity_V2.this, bookingTypeList));

                                    if (IsMultipleChecked == 1) {
                                        binding.chkMultipleBooking.setVisibility(View.VISIBLE);
                                    }

                                    if (CDH_ShowStopBooking == 1) {
                                        binding.txtStopBooking.setVisibility(View.VISIBLE);
                                    } else {
                                        binding.txtStopBooking.setVisibility(View.GONE);
                                    }

                                    if (OS_AllowToFetchAllPaxDetail == 1) {
                                        bindPassengerList();
                                    }

                                    if (OS_IsAskIDProof == 1 && OS_AllowToFetchAllPaxDetail == 0) {
                                        bindIdProof();
                                    }

                                    setMarquee(0);

                                    isLoadApi(2);


                                } else {
                                    binding.spnBookingType.setAdapter(null);
                                }

                            }
                        });


                    }
                });
            } catch (Exception ignored) {
                disMissDialog();
            }

        }
    }

    private void getReportTypeRequest() {
        Get_ReportType_Request request = new Get_ReportType_Request(
                getPref.getCM_CompanyID(),
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID()
        );
        reportTypeViewModel.onCreateRequest(request);
    }

    private void getReportTypeResponse() {
        reportTypeListAdapter = new ReportTypeListAdapter(ExeBookingActivity_V2.this, reportTypeList);
        binding.spnReport.setAdapter(reportTypeListAdapter);
    }

    private void fromCityBinding() {
        if (fromCityList != null && fromCityList.size() > 0) {
            fromCityArrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.autocomplete_items, fromCityList);
            binding.actvSourceCity.setAdapter(fromCityArrayAdapter);

            if (cityId > 0 && !TextUtils.isEmpty(sel_SourceCityName)) {
                binding.actvSourceCity.setText(sel_SourceCityName);
            }
        }
    }

    private void SeatArrangementObserver() {

    }


    private void createSeatArrangementRequest() {
        addApiList(APP_CONSTANTS.SeatArrangement, API_RESPONSE_STATUS.RUNNING);


        if (selSeats != null && selSeats.size() > 0) {
            selSeats.clear();
            seatSelectedViewModel.addSeats(selSeats);
        }
        calTotalSeat();

        SeatArrangement_Request request = new SeatArrangement_Request(
                bookedByCmpId,
                str_journeyDate,
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                branchId,
                branchUserId,
                Config.VERIFY_CALL,
                IsSameDay
        );

        seatArrangementViewModel.init(request);

    }

    @Override
    public void onClick(View arg0) {
        if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
            showAlreadyLoadTransactionsDialog();
        } else if (listSeatDetails.size() > 0 && listSeatDetails.get(arg0.getId()).getJPGender().equalsIgnoreCase("MF")) {
            AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(ExeBookingActivity_V2.this);
            alertDialog3.setTitle("Hold Seat Details");

            String HoldString = listSeatDetails.get(arg0.getId()).getSeatHoldBy() + "\n\n"
                    + "Passenger Name : " + listSeatDetails.get(arg0.getId()).getJMPassengerName() + "\n"
                    + "Phone No : " + listSeatDetails.get(arg0.getId()).getJMPhone1() + "\n"
                    + "Email ID : " + listSeatDetails.get(arg0.getId()).getEmailID() + "\n"
                    + "Route : " + listSeatDetails.get(arg0.getId()).getFromCity() + " To " + listSeatDetails.get(arg0.getId()).getToCity() + "\n"
                    + "Release Time : " + listSeatDetails.get(arg0.getId()).getHoldReleaseDateTime() + "\n"
                    + "Pickup : " + listSeatDetails.get(arg0.getId()).getPickUPTime() + "\n";

            alertDialog3.setMessage(HoldString);

            alertDialog3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            if (!isFinishing()) {
                alertDialog3.show();
            }
        } else if (IsStopBooking == 1) {

            final AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(ExeBookingActivity_V2.this);
            alertDialog3.setTitle("Route Stop Booking");
            alertDialog3.setMessage("This Route Is Stop Booking");
            alertDialog3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog3.setNegativeButton("Release", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    releaseStopBookingApiCall();
                }
            });
            alertDialog3.show();


        } else if (listSeatDetails.size() > 0 && listSeatDetails.get(arg0.getId()).getIsAllowProcess() == 0) {
            if (arg0.isSelected()) {

                if (OS_AllowToFetchAllPaxDetail == 1) {
                    for (int i = 0; i < selSeats.size(); i++) {
                        if (listSeatDetails.get(arg0.getId()).getBADSeatNo().equalsIgnoreCase(selSeats.get(i).getBADSeatNo())) {
                            setSeatBackground((Button) arg0, listSeatDetails.get(arg0.getId()).getBookingTypeColor(), -1);
                            selSeats.remove(i);
                            arg0.setSelected(false);
                            break;
                        }
                    }
                    seatSelectedViewModel.addSeats(selSeats);
                } else {
                    showSeatSelectionDialog(arg0, listSeatDetails.get(arg0.getId()));
                }


            } else {

                if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING && selSeats.size() >= OS_MaxCompanyCardBooking) {
                    showSnackBar(oops, "You can book maximum " + OS_MaxCompanyCardBooking + " seats in " + reqBookingTypeName, Toast.LENGTH_LONG, 1);
                } else if (OS_AllowToFetchAllPaxDetail == 1 && selSeats.size() > 7) {
                    showSnackBar(oops, "You can book maximum 8 seats", Toast.LENGTH_LONG, 1);
                } else if (OS_AllowToFetchAllPaxDetail == 0 && selSeats.size() > 11) {
                    showSnackBar(oops, "You can book maximum 12 seats", Toast.LENGTH_LONG, 1);
                } else {
                    if (OS_AllowToFetchAllPaxDetail == 1) {

                        setSeatBackground((Button) arg0, APP_CONSTANTS.SELECTED_SEAT_COLOR, -1);
                        arg0.setSelected(true);
                        SelectedSeatDetails(arg0.getId(), "");

                        seatSelectedViewModel.addSeats(selSeats);
                    } else {
                        showSeatSelectionDialog(arg0, listSeatDetails.get(arg0.getId()));
                    }
                }

            }
        } else {
            btnSeat = arg0;
            Get_SearchingPNRNOApiCall(4, String.valueOf(listSeatDetails.get(arg0.getId()).getJMPNRNO()));
        }


    }

    public void setSeats() {


        linkedHashMapBookedSeat.clear();
        bookingTypeSeatCounterList.clear();
        clearSeatRate();
        resetSeats();

        if (OSIsPendingAmount == 1) {
            binding.llPendingAmt.setVisibility(View.VISIBLE);
        } else {
            binding.llPendingAmt.setVisibility(View.GONE);
        }

        setAmenities();

        if (OS_AllowToFetchAllPaxDetail == 1) {
            binding.llPassengerDetails.setVisibility(View.GONE);
        } else {
            binding.llPassengerDetails.setVisibility(View.VISIBLE);
        }

        if (IsStopBooking == 1) {
            binding.txtStopBooking.setText("Release Route");
        } else {
            binding.txtStopBooking.setText("Stop Booking");
        }


        upperSeatCounter = 0;

        binding.llUpLower.removeAllViews();
        binding.llArrangement.removeAllViews();

        IsIncludeTax = 0;

        for (SeatArrangement_Response.Datum dataSeat : listSeatDetails) {


            if (dataSeat.getCADUDType() != null && dataSeat.getCADUDType() == 0) {
                if (max_column_length < dataSeat.getCADCell()) {
                    max_column_length = dataSeat.getCADCell();
                }
                upperSeatCounter++;
                //break;
            }

            if (dataSeat.getCADBlockType() != null && dataSeat.getCADBlockType() == 3 || dataSeat.getIsAllowProcess() == 1) {
                continue;
            }

            /*if ((!TextUtils.isEmpty(dataSeat.getIsIncludeTax())) && (dataSeat.getIsIncludeTax().equalsIgnoreCase("0"))) {
                double tempServiceTax = 0;
                if (CM_IsACServiceTax == 1) {
                    tempServiceTax = ((dataSeat.getSeatRate() * CM_ACServiceTax) / 100);
                } else if (CM_IsNonACServiceTax == 1) {
                    tempServiceTax = ((dataSeat.getSeatRate() * CM_NonACServiceTax) / 100);
                }
                double serviceTaxRoundUp = 0;
                if (CM_ServiceTaxRoundUp > 0) {
                    serviceTaxRoundUp = getServiceTaxRoundUpLocal(tempServiceTax, CM_ServiceTaxRoundUp);
                } else {
                    serviceTaxRoundUp = tempServiceTax;
                }
                dataSeat.setServiceTax(serviceTaxRoundUp);
                dataSeat.setServiceTaxRoundUp(serviceTaxRoundUp - tempServiceTax);
            }*/

            if (dataSeat.getSeatRate() > 0) {
                if (dataSeat.getCADBusType() == 0) {
                    if (dataSeat.getCADSeatType() == 0) {
                        oldAcSeatRate = (int) dataSeat.getSeatRate();
                        newAcSeatRate = oldAcSeatRate;

                    } else if (dataSeat.getCADSeatType() == 1) {
                        oldAcSlpRate = (int) dataSeat.getSeatRate();
                        newAcSlpRate = oldAcSlpRate;

                    } else if (dataSeat.getCADSeatType() == 2) {
                        oldAcSlmbRate = (int) dataSeat.getSeatRate();
                        newAcSlmbRate = oldAcSlmbRate;

                    }
                } else if (dataSeat.getCADBusType() == 1) {

                    if (dataSeat.getCADSeatType() == 0) {
                        oldNonAcSeatRate = (int) dataSeat.getSeatRate();
                        newNonAcSeatRate = oldNonAcSeatRate;

                    } else if (dataSeat.getCADSeatType() == 1) {
                        oldNonAcSlpRate = (int) dataSeat.getSeatRate();
                        newNonAcSlpRate = oldNonAcSlpRate;

                    } else if (dataSeat.getCADSeatType() == 2) {
                        oldNonAcSlmbRate = (int) dataSeat.getSeatRate();
                        newNonAcSlmbRate = oldNonAcSlmbRate;
                    }
                }
            }

        }


        if (isLoadTransactions() != 1) {
            binding.etAcSeat.setText(String.valueOf(newAcSeatRate));
            binding.etAcSleeper.setText(String.valueOf(newAcSlpRate));
            binding.etAcSlumber.setText(String.valueOf(newAcSlmbRate));
            binding.etNonacSeat.setText(String.valueOf(newNonAcSeatRate));
            binding.etNonacSleeper.setText(String.valueOf(newNonAcSlpRate));
            binding.etNonacSlumber.setText(String.valueOf(newNonAcSlmbRate));
        }


        totalCapacity = 0;
        totalBooked = 0;
        totalAvailable = 0;
        totalBookedTable = 0;

        totalOpacity = 0;

        SeatArrangement_Response.Datum dataInitial = listSeatDetails.get(0);

        bookedByCmpId = dataInitial.getRouteCompanyID();
        arrangementId = dataInitial.getArrangementID();


        totalCapacity = dataInitial.getTotalSeat();
        totalBooked = dataInitial.getTotalBookedSeat();
        totalAvailable = dataInitial.getTotalSeat() - dataInitial.getTotalBookedSeat();
        routeBranchId = dataInitial.getBMBranchID() != null ? dataInitial.getBMBranchID() : 0;


        if (!TextUtils.isEmpty(dataInitial.getBusNo())) {
            reqBusNo = dataInitial.getBusNo();
            binding.txtBusNo.setText(reqBusNo);
        }


        RelativeLayout.LayoutParams params;
        float textSize = getResources().getDimension(R.dimen.text_size);
        int width = displaymetrics.widthPixels;

        int buttonWidth;
        int buttonHeight;
        int buttonSpace;
        int seatnamesize;

        width = width / 3;

        if (max_column_length > 9) {
            buttonWidth = ((width) / 5);
            buttonHeight = width / 10;
            buttonSpace = (width / 9) - 1;
            seatnamesize = ((width / 6) / 4) - 10;
        } else {
            buttonWidth = (width / 6);
            buttonHeight = width / 12;
            buttonSpace = (width / 12) + 1;
            seatnamesize = ((width / 6) / 4) - 5;
        }

        for (int i = 0; i < listSeatDetails.size(); i++) {

            SeatArrangement_Response.Datum data = listSeatDetails.get(i);


            if (data.getCADBlockType() != null && data.getCADBlockType() == 3) {
                if (ISCountTableInBooked == 0 && data.getIsAllowProcess() == 1) {
                    totalBookedTable++;
                }
                if (data.getIsAllowProcess() == 0) {
                    continue;
                }
            }

            if (TextUtils.isEmpty(data.getBADSeatNo())) {

                int bookingTypeIndex = findBookingTypeIndex(data.getBookingTypeID());
                if (bookingTypeIndex >= 0) {

                    data.setBookingTypeName(bookingTypeList.get(bookingTypeIndex).getDefaultName());


                    int indexBookingTypeCount = findBookingTypeTotalSeat(data.getBookingTypeID());
                    if (indexBookingTypeCount != -1) {
                        bookingTypeSeatCounterList.get(indexBookingTypeCount).setTotalSeatbook(bookingTypeSeatCounterList.get(indexBookingTypeCount).getTotalSeatbook() + 1);
                        bookingTypeSeatCounterList.get(indexBookingTypeCount).setJPGender("");
                    } else {
                        Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML dataModel = bookingTypeList.get(bookingTypeIndex);
                        BookingTypeSeatCounterBean dataBookingType = new BookingTypeSeatCounterBean();
                        dataBookingType.setTotalSeatbook(1);
                        dataBookingType.setBookingTypeId(dataModel.getBookingTypeId());
                        dataBookingType.setDefaultName(dataModel.getDefaultName());
                        dataBookingType.setJPGender("");
                        bookingTypeSeatCounterList.add(dataBookingType);
                    }

                }

                continue;
            }

            final Button btn_Seat = new Button(ExeBookingActivity_V2.this);
            btn_Seat.setPadding(-4, -4, -4, -4);

            if (seatnamesize > 39) {
                seatnamesize = 39;
            }


            if (data.getCADColspan() != null && data.getCADColspan() > 0) {
                params = new RelativeLayout.LayoutParams(buttonWidth, buttonHeight);
            } else if (data.getCADRowSpan() != null && data.getCADRowSpan() > 0) {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonWidth);
            } else {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonHeight);
            }

            if (max_column_length >= 8) {
                params.setMargins(((data.getCADCell()) * buttonSpace), ((data.getCADRow() - 1) * buttonSpace), 0, 0);
                if (data.getCADRow() == 1) {
                    if (data.getCADCell() == 1) {
                        params.setMargins(((data.getCADCell()) * buttonSpace), 0, 0, 0);
                    }
                }
            } else {
                params.setMargins(((data.getCADCell() - 1) * buttonSpace), ((data.getCADRow() - 1) * buttonSpace), 0, 0);
                if (data.getCADRow() == 1) {
                    if (data.getCADCell() == 1) {
                        params.setMargins(((data.getCADCell() - 1) * buttonSpace), 0, 0, 0);
                    }
                }
            }


            btn_Seat.setId(i);
            btn_Seat.setLayoutParams(params);
            btn_Seat.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) textSize);
            btn_Seat.setTypeface(null, Typeface.BOLD);
            btn_Seat.setText(data.getBADSeatNo());
            btn_Seat.setOnClickListener(this);

            // Store Id of seat button
            data.setSeatId(i);
            data.setSeatButton(btn_Seat);

            if (data.getIsAllowProcess() == 0) {
                if (OS_DisplayTotalFareOnChart == 1) {
                    String s = btn_Seat.getText().toString().trim() + "\n" + listSeatDetails.get(i).getSeatRate();
                    SpannableString ss1 = new SpannableString(s);
                    ss1.setSpan(new RelativeSizeSpan(0.8f), btn_Seat.getText().toString().trim().length(), s.length(), 0); // set size// set color
                    btn_Seat.setText(ss1);
                }
            } else {

                String seatRemarks = TextUtils.isEmpty(data.getRemarks()) ? btn_Seat.getText().toString().trim() : btn_Seat.getText().toString().trim() + "*";
                if (data.getJPGender().equalsIgnoreCase("F")) {
                    seatRemarks = seatRemarks + "\n [" + data.getJPGender() + "]";
                }
                SpannableString ss1 = new SpannableString(seatRemarks);
                ss1.setSpan(new RelativeSizeSpan(0.8f), btn_Seat.getText().toString().trim().length(), seatRemarks.length(), 0); // set size// set color
                btn_Seat.setText(ss1);
            }

            btn_Seat.clearAnimation();


            int bookingTypeIndex = findBookingTypeIndex(data.getBookingTypeID());
            if (bookingTypeIndex >= 0) {

                data.setBookingTypeName(bookingTypeList.get(bookingTypeIndex).getDefaultName());
                if (data.getJPGender().equalsIgnoreCase("MF")) {
                    setSeatBackground(btn_Seat, "#6e61a5", data.getJPChartFinishStatus());
                    data.setBookingTypeColor("#6e61a5");
                } else {
                    setSeatBackground(btn_Seat, bookingTypeList.get(bookingTypeIndex).getColor(), data.getJPChartFinishStatus());
                    data.setBookingTypeColor(bookingTypeList.get(bookingTypeIndex).getColor());
                }


                int indexBookingTypeCount = findBookingTypeTotalSeat(data.getBookingTypeID());
                if (indexBookingTypeCount != -1) {
                    bookingTypeSeatCounterList.get(indexBookingTypeCount).setTotalSeatbook(bookingTypeSeatCounterList.get(indexBookingTypeCount).getTotalSeatbook() + 1);
                    bookingTypeSeatCounterList.get(indexBookingTypeCount).setJPGender(data.getJPGender());
                } else {
                    Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML dataModel = bookingTypeList.get(bookingTypeIndex);
                    BookingTypeSeatCounterBean dataBookingType = new BookingTypeSeatCounterBean();
                    dataBookingType.setTotalSeatbook(1);
                    dataBookingType.setBookingTypeId(dataModel.getBookingTypeId());
                    dataBookingType.setDefaultName(dataModel.getDefaultName());
                    dataBookingType.setJPGender(data.getJPGender());
                    bookingTypeSeatCounterList.add(dataBookingType);
                }

            }

            if (data.getIsAllowProcess() == 1 && !data.getJPGender().equalsIgnoreCase("MF")) {

                if (!linkedHashMapBookedSeat.containsKey(String.valueOf(data.getJMPNRNO()))) {

                    SelectedSeatDetailsBean dataBookedSeat = new SelectedSeatDetailsBean();
                    dataBookedSeat.setJMPassengerName(data.getJMPassengerName());
                    dataBookedSeat.setJMPhone1(data.getJMPhone1());
                    dataBookedSeat.setBADSeatNo(data.getBADSeatNo());
                    dataBookedSeat.setRouteName(data.getRouteName());
                    dataBookedSeat.setRemarks(data.getRemarks());
                    dataBookedSeat.setBookingTypeID(data.getBookingTypeID());
                    dataBookedSeat.setBookingTypeName(data.getBookingTypeName());
                    dataBookedSeat.setJMPNRNO(data.getJMPNRNO());
                    dataBookedSeat.setFromCity(data.getFromCity());
                    dataBookedSeat.setToCity(data.getToCity());

                    linkedHashMapBookedSeat.put(String.valueOf(data.getJMPNRNO()), dataBookedSeat);
                } else {
                    SelectedSeatDetailsBean dataBookedSeat = linkedHashMapBookedSeat.get(String.valueOf(data.getJMPNRNO()));
                    dataBookedSeat.setBADSeatNo(dataBookedSeat.getBADSeatNo() + "," + data.getBADSeatNo());
                    linkedHashMapBookedSeat.put(String.valueOf(data.getJMPNRNO()), dataBookedSeat);
                }
            }


            if (data.getCADRow() == 1 && upperSeatCounter > 0) {
                Button btn_UpLow = new Button(ExeBookingActivity_V2.this);
                btn_UpLow.setBackgroundResource(android.R.color.transparent);

                params = new RelativeLayout.LayoutParams(buttonHeight, buttonHeight);

                if (max_column_length >= 8) {
                    params.leftMargin = ((data.getCADCell()) * buttonSpace) + 10;

                    if (data.getCADRow() == 1) {
                        if (data.getCADCell() == 1) {
                            params.leftMargin = data.getCADCell() * buttonSpace;
                        }
                    }

                } else {
                    params.leftMargin = ((data.getCADCell() - 1) * buttonSpace);
                    if (data.getCADRow() == 1) {
                        if (data.getCADCell() == 1) {
                            params.leftMargin = ((data.getCADCell() - 1) * buttonSpace);
                        }
                    }
                }

                params.topMargin = 0;
                params.rightMargin = 0;
                params.bottomMargin = 0;


                if (data.getCADUDType() == 0) {
                    btn_UpLow.setBackgroundResource(R.drawable.arr_ub);
                } else {
                    btn_UpLow.setBackgroundResource(R.drawable.arr_lb);
                }
                btn_UpLow.setLayoutParams(params);


                if (upperSeatCounter > 0) {
                    binding.llUpLower.addView(btn_UpLow);
                    binding.llUpLower.setVisibility(View.VISIBLE);
                } else {
                    binding.llUpLower.setVisibility(View.GONE);
                }
            }
            binding.llArrangement.addView(btn_Seat);
        }

        //  binding.llMultipax.setVisibility(View.VISIBLE);

        prevNextChartValidation();

        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
            seatModificationOperations();
        }


    }

    private void resetSeatSelection() {
        if (selSeats != null && selSeats.size() > 0) {
            for (SelectedSeatDetailsBean data : selSeats) {
                if (data.getSeatButton() != null) {
                    data.getSeatButton().setSelected(false);
                    setSeatBackground(data.getSeatButton(), data.getBookingTypeColor(), -1);
                }
            }
        }
    }

    private int findBookingTypeIndex(int bookingTypeId) {
        int index = -1;
        for (int i = 0; i < bookingTypeList.size(); i++) {
            if (bookingTypeList.get(i).getBookingTypeId() == bookingTypeId) {
                index = i;
                break;
            }
        }
        return index;
    }

    private int findBookingTypeTotalSeat(int bookingTypeId) {
        int index = -1;
        for (int i = 0; i < bookingTypeSeatCounterList.size(); i++) {
            if (bookingTypeSeatCounterList.get(i).getBookingTypeId() == bookingTypeId) {
                index = i;
                break;
            }
        }
        return index;
    }

    private void ticketConfirmationDialog() {

        if (bottomBookTicketDialog != null && bottomBookTicketDialog.isShowing()) {
            bottomBookTicketDialog.dismiss();
        }

        fareDisplayBinding = LayoutFareDisplayBinding.inflate(getLayoutInflater());
        // final View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, false);
        final View view = fareDisplayBinding.getRoot();

        fareDisplayBinding.txtBookingType.setText(reqBookingTypeName);
        fareDisplayBinding.txtFromCity.setText(sel_SourceCityName);
        fareDisplayBinding.txtToCity.setText(sel_DestinationCityName);
        fareDisplayBinding.txtJourneyDate.setText(binding.spnDate.getSelectedItem().toString());
        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {

            fareDisplayBinding.llModifyCharge.setVisibility(View.VISIBLE);

            //modifyTotalGST = TextUtils.isEmpty(binding.etTotGst.getText().toString()) ? 0.0 : Double.parseDouble(binding.etTotGst.getText().toString());
            modifyTotalBaseFare = TextUtils.isEmpty(binding.etTotalAmount.getText().toString()) ? 0.0 : Double.parseDouble(binding.etTotalAmount.getText().toString()) - Double.parseDouble(binding.etTotGst.getText().toString());

            modifyAcSeatRate = Integer.parseInt(binding.etAcSeat.getText().toString());
            modifyAcSlpRate = Integer.parseInt(binding.etAcSleeper.getText().toString());
            modifyAcSlmbRate = Integer.parseInt(binding.etAcSlumber.getText().toString());
            modifyNonAcSeatRate = Integer.parseInt(binding.etNonacSeat.getText().toString());
            modifyNonAcSlpRate = Integer.parseInt(binding.etNonacSleeper.getText().toString());
            modifyNonAcSlmbRate = Integer.parseInt(binding.etNonacSlumber.getText().toString());

            fareDisplayBinding.txtBaseFare.setText(String.format(Locale.getDefault(), "%.2f", modifyTotalBaseFare));
            fareDisplayBinding.txtGst.setText(String.format(Locale.getDefault(), "%.2f", Double.parseDouble(binding.etTotGst.getText().toString())));
            fareDisplayBinding.txtTotalAmount.setText(String.format(Locale.getDefault(), "%.2f", (modifyTotalBaseFare + Double.parseDouble(binding.etTotGst.getText().toString()) - modifyDiscountAmount)));
            fareDisplayBinding.txtTotalPassenger.setText(String.valueOf(modifyTotalPax));
            fareDisplayBinding.txtSeatList.setText(modifySeats);

            if (discountAmount > 0) {
                fareDisplayBinding.llDiscountAmount.setVisibility(View.VISIBLE);
                fareDisplayBinding.viewDiscountAmount.setVisibility(View.VISIBLE);
                fareDisplayBinding.txtDiscountAmount.setText(String.format(Locale.getDefault(), "%.2f", discountAmount));
            } else {
                fareDisplayBinding.llDiscountAmount.setVisibility(View.GONE);
                fareDisplayBinding.viewDiscountAmount.setVisibility(View.GONE);
            }

            fareDisplayBinding.btnSubmit.setText("Modify Ticket");

        } else {
            fareDisplayBinding.llModifyCharge.setVisibility(View.GONE);

            fareDisplayBinding.txtTotalPassenger.setText(String.valueOf(totalPax));
            fareDisplayBinding.txtSeatList.setText(seatNameList);
            fareDisplayBinding.txtBaseFare.setText(String.format(Locale.getDefault(), "%.2f", totalBaseFare));
            fareDisplayBinding.txtGst.setText(String.format(Locale.getDefault(), "%.2f", totalGST + totalRoundUp));
            fareDisplayBinding.txtTotalAmount.setText(String.format(Locale.getDefault(), "%.2f", (totalBaseFare + totalGST + totalRoundUp) - discountAmount));

            if (discountAmount > 0) {
                fareDisplayBinding.llDiscountAmount.setVisibility(View.VISIBLE);
                fareDisplayBinding.viewDiscountAmount.setVisibility(View.VISIBLE);
                fareDisplayBinding.txtDiscountAmount.setText(String.format(Locale.getDefault(), "%.2f", discountAmount));
                if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                    fareDisplayBinding.txtDiscountTitle.setText("Wallet Redeem");
                }
            } else {
                fareDisplayBinding.llDiscountAmount.setVisibility(View.GONE);
                fareDisplayBinding.viewDiscountAmount.setVisibility(View.GONE);
            }


            fareDisplayBinding.btnSubmit.setText("Book Ticket");

        }


        fareDisplayBinding.btnCancel.setOnClickListener(view1 -> {
            binding.btnBookTicket.setEnabled(true);
            bottomBookTicketDialog.dismiss();
        });

        fareDisplayBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.btnBookTicket.setEnabled(true);
                bottomBookTicketDialog.dismiss();

                if (binding.chkGST.isChecked() && TRANSACTION_TYPE != APP_CONSTANTS.MODIFY) {
                    handleMessage(BOOK_TICKET_FLOW.OPEN_GST_DIALOG);
                } else {
                    if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING && CDH_AutoDeletePhoneTime == APP_CONSTANTS.RIGHTS_ALLOW) {
                        handleMessage(BOOK_TICKET_FLOW.PHONE_BOOKING_TIME_POPUP);
                    } else {
                        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                            JM_BranchModifyCharges = TextUtils.isEmpty(fareDisplayBinding.etModifyCharge.getText().toString()) ? 0.0 : Double.parseDouble(fareDisplayBinding.etModifyCharge.getText().toString());
                        }
                        handleMessage(BOOK_TICKET_FLOW.BOOK_TICKET_API_CALL);
                    }
                }
            }
        });


        bottomBookTicketDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookTicketDialog.setContentView(view);
        bottomBookTicketDialog.setCancelable(false);
        bottomBookTicketDialog.show();
        try {
            bottomBookTicketDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        } catch (Exception ex) {
        }
        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
            bottomBookTicketDialog.setTitle("Are You Sure, You Want To MODIFY This Transaction?");
        } else {
            bottomBookTicketDialog.setTitle("Are You Sure, You Want To PROCESS This Transaction?");
        }


        FrameLayout bottomSheet = (FrameLayout) bottomBookTicketDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

    }

    private void showAskOTPDialog(final Handler.Callback callback) {
        OTP_MAX_ATTEMPTS = 0;
        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }
        resendOTPCallBack = new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message message) {
                if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                    if (!isFinishing()) {
                        final Message message_OLd = new Message();

                        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
                            dialogLoadTransactions.dismiss();
                        }

                        try {
                            dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                            dialogLoadTransactions.setCustomImage(R.drawable.ic_resend_otp);
                            dialogLoadTransactions.show();
                            try {
                                dialogLoadTransactions.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                            } catch (Exception ignored) {
                            }
                            dialogLoadTransactions.setTitleText("Verify OTP");
                            dialogLoadTransactions.setContentText("OTP has been sent to your booking Mobile No");
                            dialogLoadTransactions.setCancelable(false);
                            dialogLoadTransactions.setConfirmText("Verify");
                            dialogLoadTransactions.setCancelText("Cancel");

                            dialogLoadTransactions.showResendOTPButton(true);
                            dialogLoadTransactions.showResendOTPEditText(true);
                            dialogLoadTransactions.setResentOTPText("ReSend OTP");


                            dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {
                                if (AUTO_DETECT_OTP.equalsIgnoreCase(dialogLoadTransactions.getResendOTPText())) {
                                    hideKeyBoard();
                                    resendOTPCallBack = null;
                                    dialogLoadTransactions.dismissWithAnimation();
                                    dialogLoadTransactions.cancel();

                                    message_OLd.what = 1;
                                    callback.handleMessage(message_OLd);
                                } else {
                                    showSnackBar(oops, "Enter Valid OTP", Toast.LENGTH_LONG, 1);
                                }
                            });

                            dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                                hideKeyBoard();
                                resendOTPCallBack = null;
                                dialogLoadTransactions.dismissWithAnimation();
                                dialogLoadTransactions.cancel();

                                message_OLd.what = 0;
                                callback.handleMessage(message_OLd);

                            });

                            dialogLoadTransactions.setResendOTPClickListener(sweetAlertDialog -> {
                                OTP_MAX_ATTEMPTS++;
                                if (OTP_MAX_ATTEMPTS >= 3) {
                                    hideKeyBoard();
                                    resendOTPCallBack = null;
                                    dialogLoadTransactions.dismissWithAnimation();
                                    dialogLoadTransactions.cancel();
                                    message_OLd.what = 0;
                                    callback.handleMessage(message_OLd);
                                    showSnackBar(oops, "You Reached Max OTP Attempts , Try After Some Times", Toast.LENGTH_LONG, 1);
                                } else {
                                    ReSendOTPApiCall(resendOTPCallBack);
                                }
                            });
                        } catch (Exception ignored) {

                        }
                    }
                }
                return false;
            }
        };

        ReSendOTPApiCall(resendOTPCallBack);

    }

    private void openWalletGenerateOTPDialog(final Handler.Callback callback) {

        OTP_MAX_ATTEMPTS = 0;
        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        if (reqWalletAskOTP == 1) {
            resendOTPCallBack = new Handler.Callback() {
                @Override
                public boolean handleMessage(@NonNull Message message) {
                    if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                        if (!isFinishing()) {
                            final Message message_OLd = new Message();
                            message_OLd.what = 0;
                            String responseMsg = "";
                            if (message.getData() != null) {
                                responseMsg = message.getData().getString("response_msg");
                            }

                            if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
                                dialogLoadTransactions.dismiss();
                            }

                            try {
                                dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                                dialogLoadTransactions.setCustomImage(R.drawable.ic_resend_otp);
                                dialogLoadTransactions.show();
                                try {
                                    dialogLoadTransactions.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                                } catch (Exception ex) {
                                }

                                dialogLoadTransactions.setTitleText("Verify OTP");
                                dialogLoadTransactions.setContentText(responseMsg);


                                dialogLoadTransactions.setCancelable(false);
                                dialogLoadTransactions.setConfirmText("Verify");
                                dialogLoadTransactions.setCancelText("Cancel");

                                dialogLoadTransactions.showResendOTPButton(true);
                                dialogLoadTransactions.showResendOTPEditText(true);
                                dialogLoadTransactions.setResentOTPText("ReSend OTP");


                                dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {
                                    if (!TextUtils.isEmpty(dialogLoadTransactions.getResendOTPText())) {
                                        reqWalletOTP = dialogLoadTransactions.getResendOTPText();
                                        hideKeyBoard();
                                        resendOTPCallBack = null;
                                        dialogLoadTransactions.dismissWithAnimation();
                                        dialogLoadTransactions.cancel();

                                        message_OLd.what = 1;
                                        callback.handleMessage(message_OLd);
                                    } else {
                                        showSnackBar(oops, "Enter Valid OTP", Toast.LENGTH_LONG, 1);
                                    }
                                });

                                dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                                    hideKeyBoard();
                                    resendOTPCallBack = null;
                                    dialogLoadTransactions.dismissWithAnimation();
                                    dialogLoadTransactions.cancel();

                                    callback.handleMessage(message_OLd);

                                });

                                dialogLoadTransactions.setResendOTPClickListener(sweetAlertDialog -> {
                                    OTP_MAX_ATTEMPTS++;
                                    if (OTP_MAX_ATTEMPTS >= 3) {
                                        hideKeyBoard();
                                        resendOTPCallBack = null;
                                        dialogLoadTransactions.dismissWithAnimation();
                                        dialogLoadTransactions.cancel();
                                        callback.handleMessage(message_OLd);
                                        showSnackBar(oops, "You Reached Max OTP Attempts , Try After Some Times", Toast.LENGTH_LONG, 1);
                                    } else {
                                        resendITSPLWalletGenerateOTPApiCall(resendOTPCallBack);
                                    }
                                });
                            } catch (Exception ignored) {
                            }
                        }
                    }
                    return false;
                }
            };

            resendITSPLWalletGenerateOTPApiCall(resendOTPCallBack);

        } else {
            resendITSPLWalletGenerateOTPApiCall(callback);
        }
    }

    private void resendITSPLWalletGenerateOTPApiCall(final Handler.Callback callback) {

        showProgressDialog("Loading....");

        reqWalletOrderNo = "";
        reqWalletState = "";

        final Message message = new Message();
        message.what = APP_CONSTANTS.STATUS_FAILED;

        String mobileNo = "", paxName = "", remarks = "", email = "", pickUpName = "", pickUpTime = "";
        int pickUpId = 0;
        if (OS_AllowToFetchAllPaxDetail == 0) {
            paxName = binding.etPaxName.getText().toString().trim();
            mobileNo = binding.etPaxPhone1.getText().toString().trim();
        } else {
            paxName = selSeats.get(0).getJMPassengerName().trim();
            mobileNo = selSeats.get(0).getJMPhone1().trim();
        }

        if (!TextUtils.isEmpty(binding.etRemarks.getText().toString().trim())) {
            remarks = binding.etRemarks.getText().toString().trim();
        }

        if (!TextUtils.isEmpty(binding.etEmail.getText().toString().trim())) {
            email = binding.etEmail.getText().toString().trim();
        }

        if (binding.spnPickup.getSelectedItemPosition() != -1) {
            pickUpId = listPickUp.get(binding.spnPickup.getSelectedItemPosition()).getPSMPickupID();
            pickUpName = listPickUp.get(binding.spnPickup.getSelectedItemPosition()).getPickUPTime();

            if (!TextUtils.isEmpty(pickUpName) && pickUpName.contains("|")) {
                String[] pickupArray = pickUpName.split("\\|");
                pickUpTime = pickupArray[0];
                pickUpName = pickupArray[1];
            }
        }


        ITSPLWalletGenerateOTP_Request request = new ITSPLWalletGenerateOTP_Request(
                loginCmpId,
                branchId,
                branchUserId,
                reqWalletType,
                reqWalletKey,
                paxName,
                mobileNo,
                totalPayableAmount,
                "",
                remarks,
                email,
                str_journeyDate,
                sourceCityId,
                destCityId,
                sel_SourceCityName,
                sel_DestinationCityName,
                bookedByCmpId,
                arrangementId,
                seatNameList,
                routeId,
                routeTimeId,
                jCityTime,
                jStartTime,
                pickUpId,
                pickUpName,
                pickUpTime
        );

        //Log.e("wallet generarate", new Gson().toJson(request));

        Call<ITSPLWalletGenerateOTP_Response> call = apiService.ITSPLWalletGenerateOTP(request);
        call.enqueue(new Callback<ITSPLWalletGenerateOTP_Response>() {
            @Override
            public void onResponse(@NotNull Call<ITSPLWalletGenerateOTP_Response> call, @NotNull Response<ITSPLWalletGenerateOTP_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        ITSPLWalletGenerateOTP_Response.Datum data = response.body().getData().get(0);
                        if (data.getStatus().equalsIgnoreCase("1")) {
                            // showSnackBar("Success", data.getMassage(), Toast.LENGTH_LONG, 2);
                            reqWalletOrderNo = data.getOrderId();
                            reqWalletState = data.getState();
                            message.what = APP_CONSTANTS.STATUS_SUCCESS;
                            Bundle bundle = new Bundle();
                            bundle.putString("response_msg", data.getMassage());
                            message.setData(bundle);
                        } else {
                            showSnackBar(oops, data.getMassage(), Toast.LENGTH_LONG, 1);
                        }
                    } else {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    }
                } else {
                    showSnackBar(oops, badServerResponse, Toast.LENGTH_LONG, 1);
                }
                callback.handleMessage(message);
            }

            @Override
            public void onFailure(@NotNull Call<ITSPLWalletGenerateOTP_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showSnackBar(oops, "Unable to send OTP , try again", Toast.LENGTH_LONG, 1);
                callback.handleMessage(message);
            }
        });

    }

    private void ReSendOTPApiCall(final Handler.Callback callback) {
        final Message message = new Message();
        message.what = APP_CONSTANTS.STATUS_FAILED;

        String mobileNo = "";
        AUTO_DETECT_OTP = "";
        if (OS_AllowToFetchAllPaxDetail == 0) {
            mobileNo = binding.etPaxPhone1.getText().toString().trim();
        } else {
            mobileNo = selSeats.get(0).getJMPhone1().trim();
        }

        SendOTP_Request request = new SendOTP_Request(
                loginCmpId,
                str_journeyDate,
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                getPref.getBUM_BranchUserID(),
                mobileNo,
                bookingTypeId,
                bookedByCmpId
        );


        Call<SendOTP_Response> call = apiService.SendOTP(request);
        call.enqueue(new Callback<SendOTP_Response>() {
            @Override
            public void onResponse(@NotNull Call<SendOTP_Response> call, @NotNull Response<SendOTP_Response> response) {

                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        AUTO_DETECT_OTP = response.body().getData().get(0).getOTP();
                        showSnackBar(success, "OTP send successfully to register mobile number", Toast.LENGTH_LONG, 2);
                        message.what = APP_CONSTANTS.STATUS_SUCCESS;
                    } else {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    }
                } else {
                    showSnackBar(oops, "Unable to send OTP , try again ", Toast.LENGTH_LONG, 1);

                }
                callback.handleMessage(message);
            }

            @Override
            public void onFailure(@NotNull Call<SendOTP_Response> call, @NotNull Throwable t) {
                showSnackBar(oops, "Unable to send OTP , try again ", Toast.LENGTH_LONG, 1);
                callback.handleMessage(message);
            }
        });

    }

    private void addApiList(String apiName, API_RESPONSE_STATUS status) {
        boolean haveApiAddToList = false;
        if (currentApiListStatus != null && currentApiListStatus.size() > 0) {
            for (int i = 0; i < currentApiListStatus.size(); ) {
                Api_List_Status data = currentApiListStatus.get(i);
                if (data.getApiName().equals(apiName)) {
                    data.setApiStatus(status);
                    haveApiAddToList = true;
                    if (totalConsecutiveApiCall != 0) {
                        break;
                    }
                }
                if (totalConsecutiveApiCall == 0 && data.getApiStatus() == API_RESPONSE_STATUS.SUCCESS || data.getApiStatus() == API_RESPONSE_STATUS.NO_DATA) {
                    currentApiListStatus.remove(data);
                    i = 0;
                } else {
                    i++;
                }
            }
        }

        if (!haveApiAddToList) {
            Api_List_Status apiListStatus = new Api_List_Status();
            apiListStatus.setApiName(apiName);
            apiListStatus.setApiStatus(status);
            currentApiListStatus.add(apiListStatus);
        }

        if (totalConsecutiveApiCall == 0) {
            enableActivityScreen();
            disMissDialog();
            if (currentApiListStatus.size() == 0) {
                resetApiList();
            } else {
                totalConsecutiveApiCall = currentApiListStatus.size();
                showInternetConnectionDialog_V2(new Handler.Callback() {
                                                    @Override
                                                    public boolean handleMessage(@NonNull Message message) {
                                                        disableActivityScreen();
                                                        showProgressDialog(getResources().getString(R.string.loading));
                                                        if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                                                            for (Api_List_Status data : currentApiListStatus) {
                                                                switch (data.getApiName()) {
                                                                    case APP_CONSTANTS.Get_ReportType:
                                                                        getReportTypeRequest();
                                                                        break;
                                                                    case APP_CONSTANTS.Get_API_EXE_RouteTimeXML:
                                                                        routeXmlApiRequest();
                                                                        break;
                                                                    case APP_CONSTANTS.Get_API_EXE_RouteTimeALLXML:
                                                                        routeTimeAllXmlRequest();
                                                                        break;
                                                                    case APP_CONSTANTS.PickupDrop:
                                                                        pickUpDropRequest();
                                                                        break;
                                                                    case APP_CONSTANTS.SeatArrangement:
                                                                        createSeatArrangementRequest();
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        return false;
                                                    }
                                                },
                        oops, getResources().getString(R.string.bad_server_response));
            }
        }

    }

    private void resetApiList() {
        currentApiListStatus.clear();
    }


    private void showInternetConnectionDialog(final int dialogType, final String method, String title, String message) {
        disMissDialog();

        if (!isFinishing()) {

            if (dialogType == APP_CONSTANTS.DIALOG_TYPE_CUSTOM_IMAGE) {
                dialogSuccess = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                dialogSuccess.setCustomImage(R.drawable.ic_network);
            } else if (dialogType == APP_CONSTANTS.DIALOG_TYPE_ERROR) {
                dialogSuccess = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.ERROR_TYPE);
            } else if (dialogType == APP_CONSTANTS.DIALOG_TYPE_SUCCESS) {
                dialogSuccess = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.SUCCESS_TYPE);
            } else if (dialogType == APP_CONSTANTS.DIALOG_TYPE_WARNING) {
                dialogSuccess = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            }

            dialogSuccess.show();


            dialogSuccess.setTitleText(title);
            dialogSuccess.setContentText(message);




       /* TextView text = dialogSuccess.findViewById(R.id.content_text);
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setText(message);
        text.setLines(5);*/

            dialogSuccess.setCancelable(false);

            if (dialogType == -1) {
                dialogSuccess.setConfirmText(getResources().getString(R.string.retry));
            } else {
                dialogSuccess.setConfirmText(getResources().getString(R.string.ok));
            }

            dialogSuccess.setConfirmClickListener(sweetAlertDialog -> {
                dialogSuccess.dismissWithAnimation();
                dialogSuccess.cancel();


            });

        }
    }

    private void showInternetConnectionDialog_V2(final Handler.Callback callback, String title, String desc) {

        if (!isFinishing()) {

            dialogSuccess = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.ERROR_TYPE);

            dialogSuccess.show();

            dialogSuccess.setTitleText(title);
            dialogSuccess.setContentText(desc);
       /* TextView text = dialogSuccess.findViewById(R.id.content_text);
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setText(message);
        text.setLines(5);*/

            dialogSuccess.setCancelable(false);
            dialogSuccess.setConfirmText(getResources().getString(R.string.ok));

            dialogSuccess.setConfirmClickListener(sweetAlertDialog -> {
                try {
                    if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                } catch (Exception ignored) {
                }
                dialogSuccess.dismissWithAnimation();
                dialogSuccess.cancel();
                if (callback != null) {
                    Message message = new Message();
                    message.what = 1;
                    callback.handleMessage(message);
                }
            });
        }
    }

    private void disMissDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }

        try {
            dialogProgress = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.PROGRESS_TYPE);
            dialogProgress.setTitleText(LoadingMessage);
            dialogProgress.setCancelable(false);
            dialogProgress.show();
        } catch (Exception ignored) {
        }
    }

    private void hideKeyBoard() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void showKeyBoard(View view) {
        try {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception ex) {
        }
    }

    private void showKeyBoardForced(View view) {
        try {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ex) {
        }
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            try {
                Snackbar bar = Snackbar.make(binding.llMain, getResources().getString(R.string.click_back_again), Snackbar.LENGTH_LONG);
                bar.show();
                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
            } catch (Exception ignored) {
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }


    private void Get_SearchingPNRNOApiCall(final int isForCancelDetails, String searchPNRNo) {
        // 0-> Cancel Details
        // 1 -> Seat Details
        // 2-> Load PNR
        // 3-> Modify PNR
        // 4-> Seat Modification PopUp
        // 5 -> SEARCHING_ESEND_INVOICE
        // 6 -> SEARCHING_INVOICE_PRINT
        // 7 -> SEARCH_PNR
        IsSearchOperation = 0;
        listFetchDetailsPNR = new ArrayList<>();

        if (isForCancelDetails == 2) {
            searchRouteTimeId = 0;
            IsSearchOperation = 0;
            if (TextUtils.isEmpty(searchPNRNo)) {
                searchPNRNo = binding.etSearchPnr.getText().toString().trim();
            }
        }

        showProgressDialog("Loading....");

        FetchDetailsByPNRNO_Request request = new FetchDetailsByPNRNO_Request(
                searchPNRNo,
                bookedByCmpId,
                branchId,
                branchUserId
        );

        Call<FetchDetailsByPNRNO_Response> call = apiService.FetchDetailsByPNRNO(request);
        String finalSearchPNRNo = searchPNRNo;
        call.enqueue(new Callback<FetchDetailsByPNRNO_Response>() {
            @Override
            public void onResponse(@NotNull Call<FetchDetailsByPNRNO_Response> call, @NotNull Response<FetchDetailsByPNRNO_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listFetchDetailsPNR = response.body().getData();
                            if (isForCancelDetails == 0) {
                                cancelDetailsApiCall();
                            } else if (isForCancelDetails == 1) {
                                showSeatDetailsDialog();
                            } else if (isForCancelDetails == 3) {
                                if (listFetchDetailsPNR.get(0).getIsAllowModify() == 1) {
                                    seatModificationOperations();
                                } else {
                                    showSnackBar(oops, getResources().getString(R.string.modify_pnr_not_allow) + finalSearchPNRNo, Toast.LENGTH_LONG, 1);
                                }
                            } else if (isForCancelDetails == 4) {
                                showSeatModificationPopUp();
                            } else if (isForCancelDetails == 5) {
                                openETicketDialog(APP_CONSTANTS.SEND_INVOICE);
                            } else if (isForCancelDetails == 6) {
                                openETicketDialog(APP_CONSTANTS.DOWNLOAD_INVOICE);
                            } else if (isForCancelDetails == 7) {
                                showPNRDetailsDialog();
                            } else {

                                IsSearchOperation = 1;
                                FetchDetailsByPNRNO_Response.Datum data = listFetchDetailsPNR.get(0);

                                try {
                                    reqDate = sdf_search_pnr.parse(data.getJMJourneyStartDate());
                                    Date initialDate = sdf_full.parse(listDates.get(0));
                                    if (reqDate.compareTo(initialDate) < 0) {
                                        showSnackBar(oops, getResources().getString(R.string.date_transaction_not_permit) + sdf_full.format(initialDate), Toast.LENGTH_LONG, 1);
                                    } else {
                                        TRANSACTION_TYPE = APP_CONSTANTS.MODIFY;
                                        sourceCityId = data.getJMJourneyFrom();
                                        destCityId = data.getJMJourneyTo();
                                        sel_SourceCityName = data.getFromCity();
                                        sel_DestinationCityName = data.getToCity();
                                        routeId = data.getRMRouteID();
                                        routeTimeId = data.getRTTime();

                                        binding.actvSourceCity.setText(data.getFromCity());
                                        binding.actvDestCity.setText(data.getToCity());

                                        if (binding.spnDate.getSelectedItem().toString().equals(sdf_full.format(reqDate))) {
                                            fetchRouteFromDataBase();
                                        } else {
                                            binding.spnDate.setSelection(listDates.indexOf(sdf_full.format(reqDate)));
                                        }
                                    }
                                } catch (Exception ignored) {
                                }
                            }

                        } else {
                            showSnackBar(oops, getResources().getString(R.string.no_data_pnr), Toast.LENGTH_LONG, 1);
                        }
                    } else {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    }
                } else {
                    showSnackBar(oops, getResources().getString(R.string.no_internet_long_msg), Toast.LENGTH_LONG, 1);
                }
            }

            @Override
            public void onFailure(@NotNull Call<FetchDetailsByPNRNO_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "FetchDetailsByPNRNO", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void Get_SearchingSeatNoApiCall() {

        showProgressDialog("Loading....");

        Get_SearchingSeatNo_Request request = new Get_SearchingSeatNo_Request(
                sourceCityId,
                destCityId,
                routeTimeId,
                jStartTime,
                str_journeyDate,
                binding.etSearchPnr.getText().toString().trim(),
                routeId,
                loginCmpId,
                arrangementId,
                bookedByCmpId,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID()
        );


        Call<Get_SearchingSeatNo_Response> call = apiService.Get_SearchingSeatNo(request);
        call.enqueue(new Callback<Get_SearchingSeatNo_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_SearchingSeatNo_Response> call, @NotNull Response<Get_SearchingSeatNo_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getSearchingSeatNo() != null && response.body().getData().getSearchingSeatNo().size() > 0) {
                            listSearchSeatNo = response.body().getData().getSearchingSeatNo();
                        }

                        if (response.body().getData().getTable1() != null && response.body().getData().getTable1().size() > 0) {
                            // listSearchSeatTable1 = response.body().getData().getTable1();

                            Get_SearchingSeatNo_Response.Table1 data = response.body().getData().getTable1().get(0);
                            if (data.getJMPNRNO() != null && data.getJMPNRNO() > 0) {
                                Get_SearchingPNRNOApiCall(2, String.valueOf(data.getJMPNRNO()));
                            }

                           /* try {
                                reqDate = sdf_search_pnr.parse(data.getJMJourneyStartDate());
                                Date initialDate = sdf_full.parse(listDates.get(0));
                                if (reqDate.compareTo(initialDate) < 0) {
                                    showInternetConnectionDialog(0, "", getResources().getString(R.string.sorder_oops), "Sorry , You Are Not Permitted To Load Transaction Before Date : " + sdf_full.format(initialDate));
                                } else {
                                    SEAT_MODIFICATION_FLAG = 1;
                                    sourceCityId = data.getJMJourneyFrom();
                                    destCityId = data.getJMJourneyTo();
                                    sel_SourceCityName = data.getFromCity();
                                    sel_DestinationCityName = data.getToCity();
                                    routeId = data.getRMRouteID();
                                    routeTimeId = data.getRTTime();

                                    binding.actvSourceCity.setText(data.getFromCity());
                                    binding.actvDestCity.setText(data.getToCity());

                                    if (binding.spnDate.getSelectedItem().toString().equals(sdf_full.format(reqDate))) {
                                        fetchRouteFromDataBase();
                                    } else {
                                        binding.spnDate.setSelection(listDates.indexOf(sdf_full.format(reqDate)));
                                    }
                                }
                            } catch (Exception ex) {
                            }*/

                           /* IsSearchOperation = 1;

                            listSearchSeatTable1 = response.body().getData().getTable1();
                            Get_SearchingSeatNo_Response.Table1 data = listSearchSeatTable1.get(0);

                            sourceCityId = data.getJMJourneyFrom();
                            destCityId = data.getJMJourneyTo();

                            sel_SourceCityName = data.getFromCity();
                            sel_DestinationCityName = data.getToCity();

                            binding.actvSourceCity.setText(data.getFromCity());
                            binding.actvDestCity.setText(data.getToCity());

                            try {
                                reqDate = sdf_search_pnr.parse(data.getJMJourneyStartDate());
                                binding.spnDate.setSelection(listDates.indexOf(sdf_full.format(reqDate)));
                            } catch (Exception ex) {
                            }

                            if (bookingTypeList != null && bookingTypeList.size() > 0) {
                                binding.spnBookingType.setSelection(bookingTypeList.indexOf(data.getBTMBookingTypeID()));
                            }

                            binding.etAcSeat.setText(String.valueOf(data.getRMACSeatRate()));
                            binding.etAcSleeper.setText(String.valueOf(data.getRMACSleeperRate()));
                            binding.etAcSlumber.setText(String.valueOf(data.getRMACSlumberRate()));

                            binding.etNonacSeat.setText(String.valueOf(data.getRMNonACSeatRate()));
                            binding.etNonacSleeper.setText(String.valueOf(data.getRMNonACSleeperRate()));
                            binding.etNonacSlumber.setText(String.valueOf(data.getRMNonAcSlumberRate()));

                            binding.etTotAcSeat.setText(String.valueOf(data.getRMACSeatQuantity()));
                            binding.etTotAcSleeper.setText(String.valueOf(data.getRMACSleeperQuantity()));
                            binding.etTotAcSlumber.setText(String.valueOf(data.getRMACSlumberQuantity()));
                            binding.etTotGst.setText(String.valueOf(data.getServiceTaxAmt()));

                            binding.etTotNonacSeat.setText(String.valueOf(data.getRMNonACSeatQuantity()));
                            binding.etTotNonacSleeper.setText(String.valueOf(data.getRMNonACSleeperQuantity()));
                            binding.etTotNonacSlumber.setText(String.valueOf(data.getRMNonACSlumberQuantity()));
                            binding.etTotalAmount.setText(String.valueOf(data.getJMPayableAmount()));

                            binding.etRemarks.setText(data.getJMRemarks());
                            binding.etEmail.setText(data.getJMEmailID());

                            binding.etNoOfSeat.setText(String.valueOf(data.getJMTotalPassengers()));

                            bindPasengerList(1);*/


                        } else {
                            showInternetConnectionDialog(0, "", oops, "Sorry , No Data Found For Requested Seat No \n Please Select Valid Route And J-Date  ");
                        }
                    } else {
                        showToast(response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "Get_SearchingSeatNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_SearchingSeatNo_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "Get_SearchingSeatNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void Get_SearchingMobileNoApiCall() {
        showProgressDialog("Loading....");

        Get_SearchingMobileNo_Request request = new Get_SearchingMobileNo_Request(
                binding.etSearchPnr.getText().toString().trim(),
                bookedByCmpId,
                getPref.getBM_BranchID()
        );

        Call<Get_SearchingMobileNo_Response> call = apiService.Get_SearchingMobileNo(request);
        call.enqueue(new Callback<Get_SearchingMobileNo_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_SearchingMobileNo_Response> call, @NotNull Response<Get_SearchingMobileNo_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getSearchingMobileNo() != null && response.body().getData().getSearchingMobileNo().size() > 0) {
                            listSearchMobileNo = response.body().getData().getSearchingMobileNo();
                            openSearchMobileNoDialog();
                        }
                    } else {
                        showToast(response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "Get_SearchingMobileNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_SearchingMobileNo_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "Get_SearchingMobileNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void PNRFeedBackApiCall() {
        showProgressDialog("Loading....");

        PNRFeedBack_Request request = new PNRFeedBack_Request(
                bookedByCmpId,
                binding.etSearchPnr.getText().toString().trim(),
                getPref.getBUM_BranchUserID()
        );

        Call<PNRFeedBack_Response> call = apiService.PNRFeedBack(request);
        call.enqueue(new Callback<PNRFeedBack_Response>() {
            @Override
            public void onResponse(@NotNull Call<PNRFeedBack_Response> call, @NotNull Response<PNRFeedBack_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listPNRFeedBack = response.body().getData();
                            openPNRFeedBackDialog();
                        } else {
                            showInternetConnectionDialog(0, "PNRFeedBack", oops, response.body().getMessage());
                        }
                    } else {
                        showInternetConnectionDialog(0, "PNRFeedBack", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "PNRFeedBack", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PNRFeedBack_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "PNRFeedBack", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void openPNRFeedBackDialog() {
        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }


        final DialogPnrBookingLogBinding dialogBinding = DialogPnrBookingLogBinding.inflate(getLayoutInflater());
        View view = dialogBinding.getRoot();

        dialogBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
        });

        dialogBinding.rvMobileSearchDetails.setAdapter(new PNRFeedBackAdapter(ExeBookingActivity_V2.this, listPNRFeedBack));

        dialogBinding.rvMobileSearchDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();
    }

    private void PNRBookingLogApiCall(String pnrNo) {
        showProgressDialog("Loading....");

        PNRBookingLog_Request request = new PNRBookingLog_Request(
                pnrNo,
                getPref.getBUM_BranchUserID(),
                bookedByCmpId
        );

        Call<PNRBookingLog_Response> call = apiService.PNRBookingLog(request);
        call.enqueue(new Callback<PNRBookingLog_Response>() {
            @Override
            public void onResponse(@NotNull Call<PNRBookingLog_Response> call, @NotNull Response<PNRBookingLog_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listPNRBookingLog = response.body().getData();
                            openPNRBookingLogDialog();
                        } else {
                            showInternetConnectionDialog(0, "PNRBookingLog", oops, response.body().getMessage());
                        }
                    } else {
                        showInternetConnectionDialog(0, "PNRBookingLog", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "PNRBookingLog", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PNRBookingLog_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "PNRBookingLog", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void openPNRBookingLogDialog() {
        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }


        final DialogPnrBookingLogBinding dialogBinding = DialogPnrBookingLogBinding.inflate(getLayoutInflater());
        View view = dialogBinding.getRoot();

        dialogBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
        });

        dialogBinding.txtBookingType.setText("PNR Booking Log");

        dialogBinding.rvMobileSearchDetails.setAdapter(new PNRBookingLogAdapter(ExeBookingActivity_V2.this, listPNRBookingLog));

        dialogBinding.rvMobileSearchDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();
    }

    private void openSearchMobileNoDialog() {

        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }


        final DialogSearchingMobileNoBinding dialogBinding = DialogSearchingMobileNoBinding.inflate(getLayoutInflater());
        View view = dialogBinding.getRoot();

        dialogBinding.imgClose.setOnClickListener(view1 -> bottomBookingTypeSeatDetailsDialog.dismiss());


        dialogBinding.rvMobileSearchDetails.setAdapter(new SearchMobileSeatDetailsAdapter(ExeBookingActivity_V2.this, listSearchMobileNo, new SearchMobileSeatDetailsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Get_SearchingMobileNo_Response.SearchingMobileNo item, int position, int flag) {
                if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
                    bottomBookingTypeSeatDetailsDialog.dismiss();
                }
                switch (flag) {
                    case 0:
                        Get_SearchingPNRNOApiCall(2, String.valueOf(item.getPNRNO()));
                        break;
                    case 1:
                        try {
                            if (GA_AllowBackDateCancellation == 0 && sdf_search_pnr.parse(item.getJourneyDate()).compareTo(sdf_full.parse(date_today)) < 0) {
                                showSnackBar(oops, "You can't cancel back date seat", Toast.LENGTH_LONG, 1);
                            } else {
                                if (cancelBookingValidation()) {
                                    showWarningDialog(flag, String.valueOf(item.getPNRNO()));
                                }

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        break;
                    case 2:
                        break;
                }
            }
        }));


        dialogBinding.rvMobileSearchDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();


    }

    private void openPhoneTicketCountDialog() {

        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }


        final DialogPhoneTicketCountBinding dialogBinding = DialogPhoneTicketCountBinding.inflate(getLayoutInflater());
        View view = dialogBinding.getRoot();

        dialogBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
        });


        dialogBinding.rvMobileSearchDetails.setAdapter(new PhoneTicketCountAdapter(ExeBookingActivity_V2.this, listPhoneTicketCount, new PhoneTicketCountAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(PhoneTicketCount_Response.Datum item, int position, int flag) {
                if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
                    bottomBookingTypeSeatDetailsDialog.dismiss();
                }
                switch (flag) {
                    case 0:
                        Get_SearchingPNRNOApiCall(2, String.valueOf(item.getPnrno()));
                        break;
                    case 1:
                        try {
                            if (GA_AllowBackDateCancellation == 0 && sdf_search_pnr.parse(item.getJourneyDate()).compareTo(sdf_full.parse(date_today)) < 0) {
                                showSnackBar(oops, "You can't cancel back date seat", Toast.LENGTH_LONG, 1);
                            } else {
                                if (cancelBookingValidation()) {
                                    showWarningDialog(flag, String.valueOf(item.getPnrno()));
                                }

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        break;
                    case 2:
                        break;
                }
            }
        }));


        dialogBinding.rvMobileSearchDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();


    }

    private void PhoneTicketCountApiCall(String countTotalSeat) {

        listPhoneTicketCount = new ArrayList<>();

        showProgressDialog("Loading....");

        PhoneTicketCount_Request request = new PhoneTicketCount_Request(
                str_journeyDate,
                countTotalSeat,
                bookedByCmpId,
                loginCmpId,
                getPref.getBM_BranchID()
        );

        // Log.e("phone ticket req",new Gson().toJson(request));

        Call<PhoneTicketCount_Response> call = apiService.PhoneTicketCount(request);
        call.enqueue(new Callback<PhoneTicketCount_Response>() {
            @Override
            public void onResponse(@NotNull Call<PhoneTicketCount_Response> call, @NotNull Response<PhoneTicketCount_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            listPhoneTicketCount = response.body().getData();
                            openPhoneTicketCountDialog();
                        } else {
                            showInternetConnectionDialog(0, "PhoneTicketCount", oops, response.body().getMessage());
                        }
                    } else {
                        showInternetConnectionDialog(0, "PhoneTicketCount", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "PhoneTicketCount", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<PhoneTicketCount_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "PhoneTicketCount", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void Get_SearchingCancelTkTPrintApiCall(String searchPNRCancel) {
        showProgressDialog("Loading....");

        Get_SearchingCancelTkTPrint_Request request = new Get_SearchingCancelTkTPrint_Request(
                searchPNRCancel,
                getPref.getCM_CompanyID()
        );

        Call<Get_SearchingCancelTkTPrint_Response> call = apiService.Get_SearchingCancelTkTPrint(request);
        call.enqueue(new Callback<Get_SearchingCancelTkTPrint_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_SearchingCancelTkTPrint_Response> call, @NotNull Response<Get_SearchingCancelTkTPrint_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getSearchingCancelTkTPrint() != null && response.body().getData().getSearchingCancelTkTPrint().size() > 0) {
                            listCancelPNR = response.body().getData().getSearchingCancelTkTPrint();
                        }
                    } else {
                        showToast(response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "Get_SearchingCancelTkTPrint", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_SearchingCancelTkTPrint_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "Get_SearchingCancelTkTPrint", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void Get_Searching_PastBookingDataByPhoneApiCall(String phoneNo) {
        showProgressDialog("Loading....");

        Get_Searching_PastBookingDataByPhone_Request request = new Get_Searching_PastBookingDataByPhone_Request(
                getPref.getCM_CompanyID(),
                phoneNo
        );

        Call<Get_Searching_PastBookingDataByPhone_Response> call = apiService.Get_Searching_PastBookingDataByPhone(request);
        call.enqueue(new Callback<Get_Searching_PastBookingDataByPhone_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Searching_PastBookingDataByPhone_Response> call, @NotNull Response<Get_Searching_PastBookingDataByPhone_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getSearchingPastBookingDataByPhone() != null && response.body().getData().getSearchingPastBookingDataByPhone().size() > 0) {
                            listPassBookingPhone = response.body().getData().getSearchingPastBookingDataByPhone();
                            openPastBookingByMobileNoDialog();
                        } else {
                            showInternetConnectionDialog(0, "Get_Searching_PastBookingDataByPhone", oops, response.body().getMessage());
                        }
                    } else {
                        showInternetConnectionDialog(0, "Get_Searching_PastBookingDataByPhone", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "Get_Searching_PastBookingDataByPhone", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Searching_PastBookingDataByPhone_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "Get_Searching_PastBookingDataByPhone", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void openPastBookingByMobileNoDialog() {

        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }


        final DialogPastBookingByMobileNoBinding dialogBinding = DialogPastBookingByMobileNoBinding.inflate(getLayoutInflater());
        View view = dialogBinding.getRoot();

        dialogBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
        });

        dialogBinding.rvPastBooking.setAdapter(new PastBookingByMobileAdapter(ExeBookingActivity_V2.this, listPassBookingPhone));

        dialogBinding.rvPastBooking.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();


    }

    private void Get_Searching_GPSLastLocationByBusNoApiCall() {
        showProgressDialog("Loading....");

        Get_Searching_GPSLastLocationByBusNo_Request request = new Get_Searching_GPSLastLocationByBusNo_Request(
                searchBusGpsLastLocation,
                getPref.getCM_CompanyID()
        );

        Call<Get_Searching_GPSLastLocationByBusNo_Response> call = apiService.Get_Searching_GPSLastLocationByBusNo(request);
        call.enqueue(new Callback<Get_Searching_GPSLastLocationByBusNo_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Searching_GPSLastLocationByBusNo_Response> call, @NotNull Response<Get_Searching_GPSLastLocationByBusNo_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getSearchingGPSLastLocationByBusNo() != null && response.body().getData().getSearchingGPSLastLocationByBusNo().size() > 0) {
                            listGPSLastLocationByBusNo = response.body().getData().getSearchingGPSLastLocationByBusNo();
                        }
                    } else {
                        showToast(response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "Get_Searching_GPSLastLocationByBusNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Searching_GPSLastLocationByBusNo_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "Get_Searching_GPSLastLocationByBusNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void Get_Searching_BusSchedule_By_BusNoApiCall(String busNo) {

        showProgressDialog("Loading....");

        Get_Searching_BusSchedule_By_BusNo_Request request = new Get_Searching_BusSchedule_By_BusNo_Request(
                getPref.getCM_CompanyID(),
                str_journeyDate,
                busNo
        );
        Call<Get_Searching_BusSchedule_By_BusNo_Response> call = apiService.Get_Searching_BusSchedule_By_BusNo(request);
        call.enqueue(new Callback<Get_Searching_BusSchedule_By_BusNo_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Searching_BusSchedule_By_BusNo_Response> call, @NotNull Response<Get_Searching_BusSchedule_By_BusNo_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getSearchingBusScheduleByBusNo() != null && response.body().getData().getSearchingBusScheduleByBusNo().size() > 0) {
                            listBusScheduleBusNo = response.body().getData().getSearchingBusScheduleByBusNo();
                            openBusScheduleBusNoDialog();
                        }
                    } else {
                        showToast(response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "Get_Searching_BusSchedule_By_BusNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Searching_BusSchedule_By_BusNo_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "Get_Searching_BusSchedule_By_BusNo", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });
    }

    private void openStopBookingDialog() {
        if (bottomBookTicketDialog != null && bottomBookTicketDialog.isShowing()) {
            bottomBookTicketDialog.dismiss();
        }

        DialogStopBookingBinding fareDisplayBinding = DialogStopBookingBinding.inflate(getLayoutInflater());
        // final View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_internet_connection, false);
        final View view = fareDisplayBinding.getRoot();


        fareDisplayBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookTicketDialog.dismiss();
            }
        });

        fareDisplayBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookTicketDialog.dismiss();
                stopBookingApiCall(fareDisplayBinding.etRemarks.getText().toString().trim(), str_journeyDate);
            }
        });

        bottomBookTicketDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookTicketDialog.setContentView(view);
        bottomBookTicketDialog.setCancelable(false);
        bottomBookTicketDialog.show();

        FrameLayout bottomSheet = (FrameLayout) bottomBookTicketDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        try {
            bottomBookTicketDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        } catch (Exception ex) {
        }


    }

    private void stopBookingApiCall(String remarks, String date) {
        showProgressDialog("Loading....");

        StopBooking_Request request = new StopBooking_Request(
                bookedByCmpId,
                routeId,
                routeTimeId,
                remarks,
                getPref.getBUM_BranchUserID(),
                date,
                sourceCityId
        );


        Call<StopBooking_Response> call = apiService.StopBooking(request);
        call.enqueue(new Callback<StopBooking_Response>() {
            @Override
            public void onResponse(@NotNull Call<StopBooking_Response> call, @NotNull Response<StopBooking_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        showSnackBar(success, "For Selected Route Stop Booking Successfully Done", Snackbar.LENGTH_LONG, 2);
                        resetSeatArrangement();
                    } else {
                        showInternetConnectionDialog(0, "StopBooking", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "StopBooking", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                }
            }

            @Override
            public void onFailure(@NotNull Call<StopBooking_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "StopBooking", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
            }
        });

    }

    private void releaseStopBookingApiCall() {

        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        try {
            dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            dialogLoadTransactions.show();
            dialogLoadTransactions.setTitleText("Release Stop Booking");
            dialogLoadTransactions.setContentText("Are You Sure, You Want To PROCESS Release Booking For Selected Route?");
            dialogLoadTransactions.setCancelable(false);
            dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
            dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));

            //dialogLoadTransactions.showRemarksEditText(true);
            // dialogLoadTransactions.setResentOTPText("Remarks");

            dialogLoadTransactions.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                    showProgressDialog("Loading....");

                    ReleaseStopBooking_Request request = new ReleaseStopBooking_Request(
                            str_journeyDate,
                            bookedByCmpId,
                            routeId,
                            routeTimeId,
                            branchId,
                            branchUserId,
                            0
                    );

                    Call<ReleaseStopBooking_Response> call = apiService.ReleaseStopBooking(request);
                    call.enqueue(new Callback<ReleaseStopBooking_Response>() {
                        @Override
                        public void onResponse(@NotNull Call<ReleaseStopBooking_Response> call, @NotNull Response<ReleaseStopBooking_Response> response) {
                            disMissDialog();
                            if (response.isSuccessful() && response.body() != null) {
                                if (response.body().getStatus() == 1) {
                                    showSnackBar(success, response.body().getData(), Snackbar.LENGTH_LONG, 2);
                                    resetSeatArrangement();
                                } else {
                                    showInternetConnectionDialog(0, "ReleaseStopBooking", oops, response.body().getMessage());
                                }
                            } else {
                                showInternetConnectionDialog(0, "ReleaseStopBooking", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                            }
                        }

                        @Override
                        public void onFailure(@NotNull Call<ReleaseStopBooking_Response> call, @NotNull Throwable t) {
                            disMissDialog();
                            showInternetConnectionDialog(0, "ReleaseStopBooking", getResources().getString(R.string.no_internet_short_msg), getResources().getString(R.string.no_internet_long_msg));
                        }
                    });


                }
            });

            dialogLoadTransactions.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                }
            });
        } catch (Exception ignored) {
        }

    }

    private void openBusScheduleBusNoDialog() {

        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }


        final DialogBusScheduleByBusNoBinding dialogBinding = DialogBusScheduleByBusNoBinding.inflate(getLayoutInflater());
        View view = dialogBinding.getRoot();

        dialogBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
        });

        dialogBinding.rvScheduleBusno.setAdapter(new SearchBusScheduleBusNoAdapter(ExeBookingActivity_V2.this, listBusScheduleBusNo));

        dialogBinding.rvScheduleBusno.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();


    }

    private void ticketPrintPDFApiCall(String reqPNRNo, int fromCity, int routeId, int routeTimeId, int arrangementId, int pickUpId, int timeFormat, int reqCompanyId) {
        showProgressDialog("Loading....");

        TicketPrintPDF_Request request = new TicketPrintPDF_Request(
                reqPNRNo,
                branchUserId,
                loginCmpId,
                fromCity,
                routeId,
                routeTimeId,
                arrangementId,
                branchId,
                pickUpId,
                timeFormat,
                reqCompanyId
        );

        Call<TicketPrintPDF_Response> call = apiService.TicketPrintPDF(request);
        call.enqueue(new Callback<TicketPrintPDF_Response>() {
            @Override
            public void onResponse(@NotNull Call<TicketPrintPDF_Response> call, @NotNull Response<TicketPrintPDF_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == APP_CONSTANTS.STATUS_SUCCESS
                        && response.body().getData().size() > 0 && !TextUtils.isEmpty(response.body().getData().get(0).getUrl())) {
                    resetSearchModule();
                    downloadPDf(reqPNRNo, response.body().getData().get(0).getUrl(), 1);
                } else {
                    if (response.body() != null) {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    } else {
                        showSnackBar(oops, badServerResponse, Toast.LENGTH_LONG, 1);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<TicketPrintPDF_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showSnackBar(oops, badServerResponse, Toast.LENGTH_LONG, 1);
            }
        });

    }

    private void printGSTInvoiceApiCall(String reqPNRNo, int reqCompanyId) {
        showProgressDialog("Loading....");

        Get_Searching_PrintGSTInvoice_Request request = new Get_Searching_PrintGSTInvoice_Request(
                reqPNRNo,
                reqCompanyId
        );

        Call<Get_Searching_PrintGSTInvoice_Response> call = apiService.Get_Searching_PrintGSTInvoice(request);
        call.enqueue(new Callback<Get_Searching_PrintGSTInvoice_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_Searching_PrintGSTInvoice_Response> call, @NotNull Response<Get_Searching_PrintGSTInvoice_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == APP_CONSTANTS.STATUS_SUCCESS
                        && response.body().getData() != null && response.body().getData().getPDFUrl().size() > 0 && !TextUtils.isEmpty(response.body().getData().getPDFUrl().get(0).getUrl())) {
                    resetSearchModule();
                    downloadPDf(reqPNRNo, response.body().getData().getPDFUrl().get(0).getUrl(), 2);
                } else {
                    if (response.body() != null) {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    } else {
                        showSnackBar(oops, badServerResponse, Toast.LENGTH_LONG, 1);
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_Searching_PrintGSTInvoice_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showSnackBar(oops, badServerResponse, Toast.LENGTH_LONG, 1);
            }
        });
    }

    private void viewReportActivity(int reportId) {

        Intent intent;
        Class cls = null;


        String reportTitle = reportTypeListAdapter.getItem(binding.spnReport.getSelectedItemPosition()).getReportName().trim();

        Bundle data = new Bundle();

        //data.putString("ReportTitle", reportTitle);
        data.putString("ReportTitle", Pattern.compile("[0-9]").matcher(reportTitle).find() ? reportTitle.substring(binding.spnReport.getSelectedItemPosition() > 9 ? 3 : 2).trim() : reportTitle);
        data.putInt("Company_Id", loginCmpId);
        data.putInt("BookedByCompanyId", bookedByCmpId);
        data.putString("FromDate", str_journeyDate);
        data.putString("JourneyStartDate", str_journeyDate);
        data.putInt("RouteId", routeId);
        data.putInt("RouteTimeId", routeTimeId);
        data.putInt("FromCity", sourceCityId);
        data.putInt("ToCity", destCityId);
        data.putString("JourneyStartTime", jStartTime);
        data.putInt("ArrangementId", arrangementId);
        data.putInt("CityId", cityId);
        data.putString("JourneyCityTime", jCityTime);
        data.putString("IsSameDay", IsSameDay);
        data.putInt("IsPendingAmount", OSIsPendingAmount);
        data.putInt("CheckListFor", 0);
        data.putInt("IsSubRoute", IsSubRoute);
        data.putString("RouteName", sel_SourceCityName + " To " + sel_DestinationCityName);


        switch (reportId) {
            case 1:
                cls = SeatWiseRouteDepartureActivity.class;
                //cls = RouteTimeWisePickupChartActivity.class;
                break;
            case 2:
                cls = SubRouteWiseChartActivity.class;
                break;
            case 3:
                cls = BranchUserWiseCollectionRefundChartActivity.class;
                break;
            case 4:
                cls = SeatNumberWiseChartActivity.class;
                break;
            case 5:
                //All Day Branch Wise Return Onwards Memo
                cls = AllDayBranchWiseReturnOnwardMemoActivity.class;
                break;
            case 6:
                data.putInt("CheckListFor", 1);
                cls = MainRouteWiseChartActivity.class;
                break;
            case 7:
                cls = SeatAllocationChartActivity.class;
                break;
            case 8:
                cls = TimeWiseBranchMemoReportActivity.class;
                break;
            case 9:
                cls = TimeWiseAllBranchMemoReportActivity.class;
                break;
            case 10:
                cls = AllDayMemoReportActivity.class;
                break;
            case 11:
                cls = AllDayMemoReportActivity.class;
                break;
            case 12:
                cls = CancelReportByRouteTimeActivity.class;
                break;
            case 13:
                //Seat Arrangement Report
                cls = SeatArrangementReportActivity.class;
                break;
            case 14:
                cls = SubRouteWiseTicketCountReportActivity.class;
                break;
            case 15:
                cls = CityDropUpWiseChartActivity.class;
                break;
            case 16:
                cls = RouteDepartureReportActivity.class;
                break;
            case 17:
                //Route Time Memo Report
                cls = RouteTimeMemoReportActivity.class;
                break;
            case 18:
                cls = PickupWiseInquiryReportActivity.class;
                break;
            case 19:
                cls = MainRouteWisePassengerDetailActivity.class;
                break;
            case 20:
                cls = RouteTimeBookingMemoReportActivity.class;
                break;
            case 21:
                //Bus Income Receipt
                cls = BusIncomeReceiptReportActivity.class;
                break;
            case 22:
                // Bulk Chart Ticket Print
                cls = RouteTimeBookingMemoReportActivity.class;
                break;
            case 23:
                cls = RoutePickUpDropChartActivity.class;
                break;
            case 24:
                // City Pickup DropWise Chart
                cls = RouteTimeBookingMemoReportActivity.class;
                break;
            case 25:
                cls = DailyRouteTimeWiseMemoReportActivity.class;
                break;
            case 26:
                // Allow City Wise Seat Chart Report
                cls = DailyRouteTimeWiseMemoReportActivity.class;
                break;
            case 27:
                // Future Booking Graph
                cls = DailyRouteTimeWiseMemoReportActivity.class;
                break;
            case 28:
                // Pickup Drop Report
                cls = DailyRouteTimeWiseMemoReportActivity.class;
                break;
           /* case 25:
                cls = CityWiseSeatTypeChartActivity.class;
                break;*/


        }

        if (cls != null) {
            intent = new Intent(ExeBookingActivity_V2.this, cls);
            intent.putExtras(data);
            startActivity(intent);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    private void isLoadApi(int isLoad) {
        if (bookingTypeList != null && bookingTypeList.size() > 0 && fromCityList != null && fromCityList.size() > 0) {
            //disMissDialog();
        }
    }

    private void getDateList(int minDays, int maxDays) {
        //CDH_BackDateShow   CDH_AdvanceDateShow

        Date todaysDate = new Date();
        Date previousDate = null;
        if (GA_ShowBackDate == 1) {
            previousDate = new Date(todaysDate.getTime() - (MILLIS_IN_A_DAY * minDays));
        } else {
            previousDate = todaysDate;
        }

        Date anotherDate = new Date(todaysDate.getTime() + (MILLIS_IN_A_DAY * (maxDays + 1)));

        List<Date> list = getDaysBetweenDates(previousDate, anotherDate);

        listDates = new ArrayList<>();
        //61
        for (Date date : list) {
            listDates.add(new SimpleDateFormat("dd-MM-yyyy").format(date));
        }

        if (listDates != null && listDates.size() > 0) {
            binding.spnDate.setAdapter(new DateSelectionAdapter(ExeBookingActivity_V2.this, listDates));
            binding.spnDate.setSelection(listDates.indexOf(sdf_full.format(reqDate)));
        }

    }

    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate)) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void fetchRouteFromDataBase() {
        if (sourceCityId != 0 && destCityId != 0) {
            try {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {

                        routeTimeXMLListChild = appDatabase.getApiExeRouteTimeXMLDao().fetchRouteList(reqDate, String.valueOf(sourceCityId), String.valueOf(destCityId));
                        Log.e("routeTimeXMLListChild",""+routeTimeXMLListChild.size());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(routeTimeXMLListChild!=null && routeTimeXMLListChild.size()>0){
                                    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listIsSameDay_0 = new ArrayList<>();
                                    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listIsSameDay_1 = new ArrayList<>();
                                    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listIsSameDay_2 = new ArrayList<>();
                                    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listIsSameDay_3 = new ArrayList<>();
                                    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listIsSameDay_4 = new ArrayList<>();

                                    List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listExtraRemove = new ArrayList<>();

                                    for (Get_API_EXE_RouteTimeXML_Response.RouteTimeXML data : routeTimeXMLListChild) {

                                        if (!data.getTimeCategory().equals("0")) {

                                            boolean isAddRoute = isExtraRoute(data.getRMRouteID(), data.getRTRouteTimeID(), data.getPSIIsSameDay());

                                            if (isAddRoute) {

                                                if (data.getPSIIsSameDay().equals("1")) {
                                                    listIsSameDay_1.add(data);
                                                }

                                           /* switch (data.getPSI_IsSameDay()) {
                                                case "0":
                                                    listIsSameDay_0.add(data);
                                                    break;
                                                case "1":
                                                    listIsSameDay_1.add(data);
                                                    break;
                                                case "2":
                                                    listIsSameDay_2.add(data);
                                                    break;
                                                case "3":
                                                    listIsSameDay_3.add(data);
                                                    break;
                                                case "4":
                                                    listIsSameDay_4.add(data);
                                                    break;
                                            }*/

                                            /*if (data.getPSI_IsSameDay().equals("1")) {
                                                listIsSameDay.add(data);
                                            }*/
                                            } else {
                                                listExtraRemove.add(data);
                                            }

                                        } else {

                                            if (data.getPSIIsSameDay().equals("1")) {
                                                listIsSameDay_1.add(data);
                                            }

                                        /*switch (data.getPSI_IsSameDay()) {
                                            case "0":
                                                listIsSameDay_0.add(data);
                                                break;
                                            case "1":
                                                listIsSameDay_1.add(data);
                                                break;
                                            case "2":
                                                listIsSameDay_2.add(data);
                                                break;
                                            case "3":
                                                listIsSameDay_3.add(data);
                                                break;
                                            case "4":
                                                listIsSameDay_4.add(data);
                                                break;
                                        }*/


                                        }

                                    }

                                    routeTimeXMLListChild.removeAll(listExtraRemove);
                                    routeTimeXMLListChild.removeAll(listIsSameDay_1);


                                    // routeTimeXMLListChild.removeAll(listExtraRemove);


                                    sortRouteListIsSameDayWise(routeTimeXMLListChild);
                                    sortRouteListIsSameDayWise(listIsSameDay_1);

//                                sortRouteListIsSameDayWise(listIsSameDay_2);
//                                sortRouteListIsSameDayWise(listIsSameDay_3);
//                                sortRouteListIsSameDayWise(listIsSameDay_4);


                                    routeTimeXMLListParent = new ArrayList<>();
                                    routeTimeXMLListParent.addAll(routeTimeXMLListChild);
                                    routeTimeXMLListParent.addAll(listIsSameDay_1);

                                    binding.spnRouteTime.setAdapter(new AvailableRoutesAdapter(getApplicationContext(), R.layout.autocomplete_items, routeTimeXMLListParent, busTypeColorCodeList));

                                    if(routeTimeXMLListParent.size()<=0){
                                            showSnackBar(oops,"No Route Avaialble For Selected City",Toast.LENGTH_LONG,1);
                                            binding.llPanelFirst.setVisibility(View.GONE);
                                    }else{
                                        binding.llPanelFirst.setVisibility(View.VISIBLE);
                                    }

                                    // Search PNR
                                    if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                                        int routeIndex = -1;
                                        for (int j = 0; j < routeTimeXMLListParent.size(); j++) {
                                            if (routeTimeXMLListParent.get(j).getRTRouteTimeID().equals(String.valueOf(routeTimeId))) {
                                                routeIndex = j;
                                                break;
                                            }
                                        }
                                        if (routeIndex != -1) {
                                            binding.spnRouteTime.setSelection(routeIndex);
                                        }
                                    } else {

                                        flag_date_selection = false;
                                        busposition = 0;

                                        for (int i = 0; i < routeTimeXMLListParent.size(); i++) {

                                            try {
                                                String APiDAte;
                                                final DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                                                Date date1 = new Date();
                                                APiDAte = sdf.format(date1) + " " + routeTimeXMLListParent.get(i).getStartTime();
                                                Date date = new Date();
                                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.getDefault());
                                                dateFormat.format(date);

                                                if (dateFormat.parse(dateFormat.format(date)).before(dateFormat.parse(APiDAte))) {
                                                    if (!flag_date_selection) {
                                                        busposition = i;
                                                        flag_date_selection = true;
                                                        break;
                                                    }
                                                }

                                            } catch (Exception e) {
                                                //
                                            }

                                        }

                                        binding.spnRouteTime.setSelection(busposition);

                                    }
                                }else{
                                    showSnackBar(oops,"No Route Avaialble For Selected City",Toast.LENGTH_LONG,1);
                                    binding.llPanelFirst.setVisibility(View.GONE);
                                }
                            }
                        });

                    }
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }


           /* Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    // final List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> mainList = appDatabase.getApiExeRouteTimeXMLDao().getGet_API_EXE_RouteTimeXML(String.valueOf(sourceCityId), String.valueOf(destCityId));
                    final List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> list = appDatabase.getApiExeRouteTimeXMLDao().fetchUserBetweenDate(reqDate, String.valueOf(sourceCityId), String.valueOf(destCityId));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.txtRouteCounter.setText((binding.spnRouteTime.getSelectedItemPosition() + 1) + "/" + list.size());

                            binding.spnRouteTime.setAdapter(new AvailableRoutesAdapter(getApplicationContext(), R.layout.autocomplete_items, list));
                        }
                    });
                }
            });*/
        } else {
            clearTextFields();
        }

    }

    private boolean isExtraRoute(String routeId, String routeTimeId, String IsSameDay) {


        Calendar c = Calendar.getInstance();

        boolean flag = false;
        for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetExtraRouteTime data : extraRouteTimeList) {

            if (data.getRMRouteID().equals(routeId)
                    && data.getRTRouteTimeID().equals(routeTimeId)) {


                Date date = null;
                /*try {
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(data.getDATE());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                c.setTime(date);*/

                c.setTime(reqDate);

                String tempServerDate = "";

                String internalDate = data.getDATE();


                switch (IsSameDay) {
                    case "0":


                        c.add(Calendar.DATE, -1);
                        tempServerDate = sdf_full.format(c.getTime());


                        if (tempServerDate.equals(internalDate)) {
                            flag = true;
                        }

                      /*  if (sdf_server.format(tempServerDate).equals(internalDate)) {
                            flag = true;
                        }*/

                        break;
                    case "1":

                        tempServerDate = sdf_full.format(c.getTime());


                        if (tempServerDate.equals(internalDate)) {
                            flag = true;
                        }

                       /* internalDate = sdf_server.format(c.getTime());
                        if (sdf_server.format(serverDate).equals(internalDate)) {
                            flag = true;
                        }*/
                        break;
                    case "2":

                        tempServerDate = sdf_full.format(c.getTime());


                        if (tempServerDate.equals(internalDate)) {
                            flag = true;
                        }

                       /* internalDate = sdf_server.format(c.getTime());
                        if (sdf_server.format(serverDate).equals(internalDate)) {
                            flag = true;
                        }*/
                        break;
                    case "3":

                        c.add(Calendar.DATE, -2);
                        tempServerDate = sdf_full.format(c.getTime());


                        if (tempServerDate.equals(internalDate)) {
                            flag = true;
                        }

                       /* c.add(Calendar.DATE, -2);
                        internalDate = sdf_server.format(c.getTime());

                        if (sdf_server.format(serverDate).equals(internalDate)) {
                            flag = true;
                        }*/

                        break;
                    case "4":

                        c.add(Calendar.DATE, -3);
                        tempServerDate = sdf_full.format(c.getTime());

                        if (tempServerDate.equals(internalDate)) {
                            flag = true;
                        }

                      /*  c.add(Calendar.DATE, -3);
                        internalDate = sdf_server.format(c.getTime());

                        if (sdf_server.format(serverDate).equals(internalDate)) {
                            flag = true;
                        }*/

                        break;
                    default:
                        flag = false;
                        break;
                }

                if (flag) {
                    break;
                }

            }


        }
        return flag;
    }

    private void sortRouteListIsSameDayWise(List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> listIsSameDay) {
        Collections.sort(listIsSameDay, new Comparator<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML>() {

            @Override
            public int compare(Get_API_EXE_RouteTimeXML_Response.RouteTimeXML o1, Get_API_EXE_RouteTimeXML_Response.RouteTimeXML o2) {
                try {
                    return new SimpleDateFormat("hh:mm a", Locale.ENGLISH).parse(o1.getStartTime()).compareTo(new SimpleDateFormat("hh:mm a").parse(o2.getStartTime()));
                } catch (ParseException e) {
                    return 0;
                }
            }
        });
    }

    private void bindAgentBookingTypeData() {

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                itsGetAllAgentCityByCompanyIDS = appDatabase.getApiExeRouteTimeALLXMLDao().getITSAllAgentCityNameByCompanyId();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (itsGetAllAgentCityByCompanyIDS != null && itsGetAllAgentCityByCompanyIDS.size() > 0) {
                            binding.spnAgentCity.setAdapter(new AgentBookingTypeCityAdapter(ExeBookingActivity_V2.this, itsGetAllAgentCityByCompanyIDS, 1));
                            if (OS_DisplayNetRate == 1) {
                                binding.spnAgentRate.setAdapter(new Ret_Net_Adapter(ExeBookingActivity_V2.this, new String[]{"Rate", "Net"}));
                            } else {
                                binding.llAgentRateNet.setVisibility(View.GONE);
                                //binding.spnAgentRate.setAdapter(new Ret_Net_Adapter(ExeBookingActivity_V2.this, new String[]{"Rate", "Net"}));
                            }

                            if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                                if (itsGetAllAgentCityByCompanyIDS.size() > 0 && ABI_AgentCityID != 0) {
                                    for (int i = 0; i < itsGetAllAgentCityByCompanyIDS.size(); i++) {
                                        if (itsGetAllAgentCityByCompanyIDS.get(i).getCM_CityId() == ABI_AgentCityID) {
                                            binding.spnAgentCity.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                                binding.spnAgentRate.setSelection(ABI_NeteRate);
                            }


                        } else {
                            showSnackBar(oops, "Agent city unavailable", Toast.LENGTH_LONG, 1);
                        }
                    }
                });
            }
        });


        if (GA_AllowMultyDateAgentQuota_Exe == 1) {
            binding.txtAgentquotaDate.setOnClickListener(view -> openAgentQuotaDateDialog());
        }
    }

    private void bindAgentNameByAgentCity() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                itsGetAllAgentByCompanyIDS = appDatabase.getApiExeRouteTimeALLXMLDao().getITSAllAgentByCompanyId(reqAgentCityId);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (itsGetAllAgentByCompanyIDS != null && itsGetAllAgentByCompanyIDS.size() > 0) {
                            binding.spnAgentName.setAdapter(new AgentBookingTypeCityAdapter(ExeBookingActivity_V2.this, itsGetAllAgentByCompanyIDS, 0));

                            if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY && AM_AgentID != 0) {
                                if (itsGetAllAgentByCompanyIDS.size() > 0 && AM_AgentID != 0) {
                                    for (int i = 0; i < itsGetAllAgentByCompanyIDS.size(); i++) {
                                        if (itsGetAllAgentByCompanyIDS.get(i).getAM_AgentID() == AM_AgentID) {
                                            binding.spnAgentName.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                            }

                        } else {
                            binding.spnAgentName.setAdapter(null);
                            showSnackBar(oops, "Agent data unavailable", Toast.LENGTH_LONG, 1);
                        }
                    }
                });
            }
        });
    }


    private void bindGuestBookingTypeData() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                itsGetGuestTypeByCompanyIDModelList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSGetGuestTypeByCompanyID();
                if (itsGetGuestTypeByCompanyIDModelList != null && itsGetGuestTypeByCompanyIDModelList.size() > 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.spnGuest.setAdapter(new GuestBookingTypeAdapter(ExeBookingActivity_V2.this, itsGetGuestTypeByCompanyIDModelList));
                            if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY && GTM_GuestTypeID != 0) {
                                for (int i = 0; i < itsGetGuestTypeByCompanyIDModelList.size(); i++) {
                                    if (itsGetGuestTypeByCompanyIDModelList.get(i).getGTM_GuestTypeID() == GTM_GuestTypeID) {
                                        binding.spnGuest.setSelection(i);
                                        break;
                                    }

                                }

                            }
                        }
                    });
                }
            }
        });

    }

    private void bindBranchMasterBookingTypeData() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                itsBranchMasterSelectXMLModelList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSBranchMasterSelectXML();
                if (itsBranchMasterSelectXMLModelList != null && itsBranchMasterSelectXMLModelList.size() > 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.spnBranch.setAdapter(new BranchNameBookingTypeAdapter(ExeBookingActivity_V2.this, itsBranchMasterSelectXMLModelList));
                            binding.txtAgentBal.setVisibility(View.VISIBLE);
                            binding.txtAgentBal.setText("BAL " + getResources().getString(R.string.rs) + "0.0");

                            if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                                for (int i = 0; i < itsBranchMasterSelectXMLModelList.size(); i++) {
                                    if (itsBranchMasterSelectXMLModelList.get(i).getBM_BranchID() == BBI_BM_BranchID) {
                                        binding.spnBranch.setSelection(i);
                                        break;
                                    }
                                }
                            } else {
                                for (int i = 0; i < itsBranchMasterSelectXMLModelList.size(); i++) {
                                    if (itsBranchMasterSelectXMLModelList.get(i).getBM_BranchID() == branchId) {
                                        binding.spnBranch.setSelection(i);
                                        break;
                                    }
                                }
                            }

                        }
                    });
                }

            }
        });
    }

    private void bindBranchUserByBranchMaster() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                itsGetBranchUserByCompanyIDModelList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSGetBranchUserByCompanyID(reqBranchMasterBranchId);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (itsGetBranchUserByCompanyIDModelList != null && itsGetBranchUserByCompanyIDModelList.size() > 0) {
                            binding.spnBranchUser.setAdapter(new BranchUserByBranchMasterBookingTypeAdapter(ExeBookingActivity_V2.this, itsGetBranchUserByCompanyIDModelList));
                            if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                                binding.etBranchTicketNo.setText(BBI_TicketNo);
                                for (int i = 0; i < itsGetBranchUserByCompanyIDModelList.size(); i++) {
                                    if (itsGetBranchUserByCompanyIDModelList.get(i).getBUM_BranchUserID() == BBI_UM_UserID) {
                                        binding.spnBranchUser.setSelection(i);
                                        break;
                                    }
                                }
                            } else {
                                for (int i = 0; i < itsGetBranchUserByCompanyIDModelList.size(); i++) {
                                    if (itsGetBranchUserByCompanyIDModelList.get(i).getBUM_BranchUserID() == branchUserId) {
                                        binding.spnBranchUser.setSelection(i);
                                        break;
                                    }
                                }
                            }
                        } else {
                            binding.spnBranchUser.setAdapter(null);
                        }

                    }
                });

            }
        });
    }

    private void bindBankCardBookingType() {
        binding.spnBankcard.setAdapter(new BankCreditDebitAdapter(ExeBookingActivity_V2.this, bankCard));
        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
            binding.spnBankcard.setSelection(bankCardId);
            binding.etBankNotes.setText(bankCardRemarks);
        }
    }

    private void bindCompanyBookingType() {

        binding.btnCompanyCardRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.etCmpBkgCardno.getText().toString().trim())) {
                    showToast(validCardNo);
                } else if (sourceCityId == 0) {
                    showToast(validFrom);
                } else if (destCityId == 0) {
                    showToast(validTo);
                } else if (routeId == 0 || routeTimeId == 0) {
                    showToast(validRoute);
                } else if (totalPax > OS_MaxCompanyCardBooking) {
                    showInternetConnectionDialog(0, "", oops, "You are not permitted to more than " + OS_MaxCompanyCardBooking + " Pax in [Company Card]");
                } else if (totalPax <= 0) {
                    showToast(getResources().getString(R.string.select_one_seat));
                } else {
                    hideKeyBoard();
                    companyCardDetailsApiCall();
                }
            }
        });
    }

    private void companyCardDetailsApiCall() {

        cmpCardNo = "";
        cmpCardBalance = 0;
        cmpCardMaxDiscount = 0;
        cmpCardOTP = "";
        cmpCardId = "";

        if (animCompanyCardDetails != null) {
            animCompanyCardDetails.cancel();
            binding.txtCmpCardDetails.clearAnimation();
            animCompanyCardDetails = null;
        }

        binding.llCmpCardDetails.setVisibility(View.GONE);
        binding.txtCmpDiscount.setVisibility(View.INVISIBLE);
        binding.etCmpCardDisc.setVisibility(View.GONE);

        showProgressDialog("Loading....");
        //binding.txtCmpCardDetails.setText("");

        CompanyCardDetails_Request request = new CompanyCardDetails_Request(
                loginCmpId,
                binding.etCmpBkgCardno.getText().toString().trim(),
                str_journeyDate,
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                bookedByCmpId
        );


        Call<CompanyCardDetails_Response> call = apiService.CompanyCardDetails(request);
        call.enqueue(new Callback<CompanyCardDetails_Response>() {
            @Override
            public void onResponse(@NotNull Call<CompanyCardDetails_Response> call, @NotNull Response<CompanyCardDetails_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() != 0 && response.body().getData() != null && response.body().getData().size() > 0) {
                        listCompanyCard = response.body().getData();

                        if (listCompanyCard != null && listCompanyCard.size() > 0) {
                            binding.llCmpCardDetails.setVisibility(View.VISIBLE);
                            binding.txtCmpDiscount.setVisibility(View.VISIBLE);
                            binding.etCmpCardDisc.setVisibility(View.VISIBLE);
                            if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                                calCompanyCardDetails(totalPayableAmount);
                            } else {
                                calTotalSeat();
                            }

                        }

                    } else {
                        showInternetConnectionDialog(0, "CompanyCardDetails", oops, "Invalid Card No");
                    }
                } else {
                    showInternetConnectionDialog(0, "CompanyCardDetails", oops, getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<CompanyCardDetails_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "CompanyCardDetails", oops, getResources().getString(R.string.bad_server_response));
            }
        });
    }


    private void bindITSPLWalletBookingType() {

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                itsOnlineWalletModelList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSOnlineWallet();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (itsOnlineWalletModelList != null && itsOnlineWalletModelList.size() > 0) {
                            binding.spnItsplWallet.setAdapter(new ITSPLWalletBindingAdapter(ExeBookingActivity_V2.this, itsOnlineWalletModelList));
                        } else {
                            binding.spnItsplWallet.setAdapter(null);
                        }

                    }
                });

            }
        });

        binding.spnItsplWallet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                reqWalletType = itsOnlineWalletModelList.get(position).getWCM_ID();
                reqWalletKey = itsOnlineWalletModelList.get(position).getWCD_WalletKey();
                if (itsOnlineWalletModelList.get(position).getWCM_IsOTP() != null) {
                    reqWalletAskOTP = itsOnlineWalletModelList.get(position).getWCM_IsOTP();
                }
                if (itsOnlineWalletModelList.get(position).getWCD_TDRCharges() != null) {
                    reqWalletTDRCharge = itsOnlineWalletModelList.get(position).getWCD_TDRCharges();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void clearTextFields() {
        binding.spnRouteTime.setAdapter(null);
        binding.txtRouteCounter.setText("");
        binding.txtRouteName.setText("");
    }


    private void bindBookingTypeRecyclerView() {
        if (bookingTypeList != null && bookingTypeList.size() > 0) {

            List<Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML> listTempBookingType = new ArrayList<>();
            for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML data : bookingTypeList) {
                if (data.getBookingTypeId() == APP_CONSTANTS.CONFIRM_BOOKING || data.getBookingTypeId() == APP_CONSTANTS.PHONE_BOOKING ||
                        data.getBookingTypeId() == APP_CONSTANTS.AGENT_BOOKING || data.getBookingTypeId() == APP_CONSTANTS.BRANCH_BOOKING ||
                        data.getBookingTypeId() == APP_CONSTANTS.GUEST_BOOKING || data.getBookingTypeId() == APP_CONSTANTS.BANK_CARD_BOOKING ||
                        data.getBookingTypeId() == APP_CONSTANTS.CMP_CARD_BOOKING || data.getBookingTypeId() == APP_CONSTANTS.WAITING_BOOKING ||
                        data.getBookingTypeId() == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                    listTempBookingType.add(data);
                }
            }


            binding.cvBookingType.setVisibility(View.VISIBLE);

            binding.txtCapacity.setText("Capacity: " + totalCapacity);


            if (ISCountTableInBooked == 0) {
                totalBooked = totalBooked - totalBookedTable;
                totalAvailable = totalCapacity - totalBooked;
            }

            binding.txtBooked.setText("Booked: " + totalBooked);
            binding.txtAvailable.setText("Available: " + totalAvailable);
            binding.txtOccupacy.setText("Occu: " + ((totalBooked * 100) / totalCapacity) + "%");


            GridLayoutManager gridBookingTypeManager = new GridLayoutManager(ExeBookingActivity_V2.this, 3);
            binding.rvBookingType.setLayoutManager(gridBookingTypeManager);
            SeatChartBookingTypeAdapter seatChartBookingTypeAdapter = new SeatChartBookingTypeAdapter(ExeBookingActivity_V2.this, listTempBookingType, itsFetchChartForeColorModelList, bookingTypeSeatCounterList, new SeatChartBookingTypeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Get_API_EXE_RouteTimeALLXML_Response.ITSGetBookingTypeByCompanyIDXML item, int pos, int bookingTypeTotalSeat) {
                    if (linkedHashMapBookedSeat.size() > 0) {
                        if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
                            showAlreadyLoadTransactionsDialog();
                        } else {
                            if (item.getBookingTypeId() == APP_CONSTANTS.WAITING_BOOKING) {
                                waitingListReportApiCall();
                            } else {
                                showBookingTypeWiseSeatDetails(item.getBookingTypeId(), item.getDefaultName());
                            }
                        }
                    }
                }
            });

            binding.rvBookingType.setAdapter(seatChartBookingTypeAdapter);
            //binding.rvBookingType.addItemDecoration(new SpacesItemDecoration(5));

        } else {
            binding.cvBookingType.setVisibility(View.VISIBLE);
        }

    }


    private void setMarquee(int updateMarquee) {

        if (itsGetCompanyMarqueeModelList != null && itsGetCompanyMarqueeModelList.size() > 0 && !TextUtils.isEmpty(itsGetCompanyMarqueeModelList.get(0).getMm_message())) {


            binding.relCmpMarquee.setVisibility(View.VISIBLE);

            binding.txtCmpMarqueeHint.setHint(getPref.getLoginCompanyName() + " [" + getPref.getCM_CompanyID() + "]");

            String marQueeText = "";
            for (ITSGetCompanyMarquee_Model data : itsGetCompanyMarqueeModelList) {
                if (!TextUtils.isEmpty(data.getMm_message())) {
                    marQueeText += data.getMm_message() + "\t\t\t";
                }
            }

            try{
                binding.txtCmpMarquee.getSettings().setJavaScriptEnabled(true);
                String summary = "<html><body><MARQUEE><font color='526993'>"+marQueeText+"</font></MARQUEE></body></html>";
                binding.txtCmpMarquee.loadData(summary, "text/html", "utf-8");
                binding.txtCmpMarquee.setBackgroundColor(Color.TRANSPARENT);

                final WebSettings webSettings = binding.txtCmpMarquee.getSettings();
                webSettings.setDefaultFontSize((int)getResources().getDimension(R.dimen.marquee_text_size));
            }catch (Exception ex){}

        }

        if (updateMarquee == 0 && itsGetITSMarqueeModelList != null && itsGetITSMarqueeModelList.size() > 0 && !TextUtils.isEmpty(itsGetITSMarqueeModelList.get(0).getMMMarqueeMessage())) {

            binding.relItsMarquee.setVisibility(View.VISIBLE);
            String marQueeText = "";
            for (Get_API_EXE_RouteTimeALLXML_Response.ITSGetITSMarquee data : itsGetITSMarqueeModelList) {
                if (!TextUtils.isEmpty(data.getMMMarqueeMessage())) {
                    marQueeText += data.getMMMarqueeMessage() + "\t\t\t";
                }
            }

            try{
                binding.txtItsMarquee.getSettings().setJavaScriptEnabled(true);
                String summary = "<html><body><MARQUEE><font color='526993'>"+marQueeText+"</font></MARQUEE></body></html>";
                binding.txtItsMarquee.loadData(summary, "text/html", "utf-8");
                binding.txtItsMarquee.setBackgroundColor(Color.TRANSPARENT);

                final WebSettings webSettings = binding.txtItsMarquee.getSettings();
                webSettings.setDefaultFontSize((int)getResources().getDimension(R.dimen.marquee_text_size));
            }catch (Exception ignored){}


        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animITSMarquee != null) {
            animITSMarquee.cancel();
            binding.txtItsMarquee.clearAnimation();
        }
        if (animCmpMarquee != null) {
            animCmpMarquee.cancel();
            binding.txtCmpMarquee.clearAnimation();
        }
        if (animInProgressTransaction != null) {
            animInProgressTransaction.cancel();
            binding.txtTransactionHighlight.clearAnimation();
        }

        if (animCompanyCardDetails != null) {
            animCompanyCardDetails.cancel();
            binding.txtCmpCardDetails.clearAnimation();
            animCompanyCardDetails = null;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animITSMarquee != null) {
            binding.txtItsMarquee.startAnimation(animITSMarquee);
        }
        if (animCmpMarquee != null) {
            binding.txtCmpMarquee.startAnimation(animCmpMarquee);
        }

    }

    public void setSeatBackground(Button btn_Seat, String bookingTypeColor,
                                  int ChartFinishStatus) {
        int startColor;
        if (bookingTypeColor.contains("#")) {
            startColor = Color.parseColor(bookingTypeColor);
        } else {
            startColor = Color.parseColor("#" + bookingTypeColor);
        }

        int[] colors = {startColor, startColor};
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);


        gradientDrawable.setStroke(1, borderColor);
        btn_Seat.setBackground(gradientDrawable);


        btn_Seat.setTextColor(getComplementaryColor(startColor));
        btn_Seat.setTag(startColor);

        if (ChartFinishStatus == 1) {
            /*try {
                btn_Seat.setAnimation(AnimationUtils.loadAnimation(ExeBookingActivity_V2.this, R.anim.seat_blink));
            } catch (Exception ex) {
            }*/
        }
    }

    private int getComplementaryColor(int color) {
        int R = color & 255;
        int G = (color >> 8) & 255;
        int B = (color >> 16) & 255;
        int A = (color >> 24) & 255;
        R = 255 - R;
        G = 255 - G;
        B = 255 - B;
        return R + (G << 8) + (B << 16) + (A << 24);
    }

    void SelectedSeatDetails(int pos, String gender) {
        SeatArrangement_Response.Datum addSeat = listSeatDetails.get(pos);
        SelectedSeatDetailsBean data = new SelectedSeatDetailsBean();

        data.setBADSeatNo(addSeat.getBADSeatNo());
        data.setJPGender(addSeat.getJPGender());
        data.setSeatStatus(addSeat.getSeatStatus());
        data.setCADSeatType(addSeat.getCADSeatType());
        data.setCADRow(addSeat.getCADRow());
        data.setCADCell(addSeat.getCADCell());
        data.setCADUDType(addSeat.getCADUDType());
        data.setCADBlockType(addSeat.getCADBlockType());
        data.setCADRowSpan(addSeat.getCADRowSpan());
        data.setCADColspan(addSeat.getCADColspan());
        data.setJMPNRNO(addSeat.getJMPNRNO());
        data.setJMJourneyFrom(addSeat.getJMJourneyFrom());
        data.setFromCity(addSeat.getFromCity());
        data.setJMJourneyTo(addSeat.getJMJourneyTo());
        data.setToCity(addSeat.getToCity());
        data.setJMPassengerName(addSeat.getJMPassengerName());
        data.setJMPhone1(addSeat.getJMPhone1());
        data.setPMPickupID(addSeat.getPMPickupID());
        data.setPickUPTime(addSeat.getPickUPTime());
        data.setJPChartFinishStatus(addSeat.getJPChartFinishStatus());
        data.setIsAllowProcess(addSeat.getIsAllowProcess());
        data.setCADBusType(addSeat.getCADBusType());
        data.setCADFareType(addSeat.getCADFareType());
        data.setSeatRate((int) addSeat.getSeatRate());
        data.setBookingTypeID(addSeat.getBookingTypeID());
        data.setPSIDepartTime(addSeat.getPSIDepartTime());
        data.setOrderBy(addSeat.getOrderBy());
        data.setFromSrNo(addSeat.getFromSrNo());
        data.setPSIIsSameDay(addSeat.getPSIIsSameDay());
        data.setTotalSeat(addSeat.getTotalSeat());
        data.setTotalBookedSeat(addSeat.getTotalBookedSeat());
        data.setBookingTypeName(addSeat.getBookingTypeName());
        data.setBookingUser(addSeat.getBookingUser());
        data.setTotalAmount(addSeat.getTotalAmount());
        data.setPickupManName(addSeat.getPickupManName());
        data.setIsStopBooking(addSeat.getIsStopBooking());
        data.setReported(addSeat.getReported());
        data.setNonReported(addSeat.getNonReported());
        data.setPending(addSeat.getPending());
        data.setIsIncludeTax(addSeat.getIsIncludeTax());
        data.setServiceTax(addSeat.getServiceTax());
        data.setArrangementID(addSeat.getArrangementID());
        data.setBusID(addSeat.getBusID());
        data.setRemarks(addSeat.getRemarks());
        data.setJMPhoneOnHold(addSeat.getJMPhoneOnHold());
        data.setTHIsSeatBlockWeb(addSeat.getTHIsSeatBlockWeb());
        data.setSeatHoldBy(addSeat.getSeatHoldBy());
        data.setBookingDateTime(addSeat.getBookingDateTime());
        data.setBMBranchID(addSeat.getBMBranchID());
        data.setUMUserID(addSeat.getUMUserID());
        data.setRouteName(addSeat.getRouteName());
        data.setRouteTime(addSeat.getRouteTime());
        data.setBusNo(addSeat.getBusNo());
        data.setCurrentKM(addSeat.getCurrentKM());
        data.setRouteCompanyID(addSeat.getRouteCompanyID());
        data.setJourneyDate(addSeat.getJourneyDate());
        data.setEmailID(addSeat.getEmailID());
        data.setHoldReleaseDateTime(addSeat.getHoldReleaseDateTime());
        data.setCAMIsOnline(addSeat.getCAMIsOnline());
        data.setB2COrderNo(addSeat.getB2COrderNo());
        data.setWalletTypeID(addSeat.getWalletTypeID());
        data.setCDHSocialDistanceTransactionType(addSeat.getCDHSocialDistanceTransactionType());
        data.setCDHSocialDistancePercentage(addSeat.getCDHSocialDistancePercentage());
        data.setDMDriver1(addSeat.getDMDriver1());
        data.setDMPhoneNo(addSeat.getDMPhoneNo());
        data.setPaxAge(addSeat.getPaxAge());
        data.setIdProofNumber(addSeat.getIdProofNumber());
        data.setIdProofName(addSeat.getIdProofName());
        data.setIdProofId(addSeat.getIdProofId());
        data.setRadioButton(addSeat.getRadioButton());
        data.setRouteId(addSeat.getRouteId());
        data.setRouteTimeId(addSeat.getRouteTimeId());
        data.setServiceTaxRoundUp(addSeat.getServiceTaxRoundUp());
        data.setSeatButton(addSeat.getSeatButton());
        data.setBookingTypeColor(addSeat.getBookingTypeColor());

        if (OS_AllowToFetchAllPaxDetail == 0) {
            data.setJPGender(gender);
        }
        selSeats.add(data);


        totalPax = selSeats.size();

    }

    private void bindIdProof() {
        List<String> idProofSubList = new ArrayList<>();
        if (itsGetIdProofList != null && itsGetIdProofList.size() > 0) {
            for (int i = 0; i < itsGetIdProofList.size(); i++) {
                idProofSubList.add(itsGetIdProofList.get(i).getPMProofName());
            }
        }
        BookTicketPassengerListAdapter.IdProofArrayAdapter idProofArrayAdapter = new BookTicketPassengerListAdapter.IdProofArrayAdapter(ExeBookingActivity_V2.this, idProofSubList);
        binding.spnIdProof.setAdapter(idProofArrayAdapter);
    }


    private boolean checkBookingTypeValidation() {
        boolean bookingTypeFlag = false;
        switch (bookingTypeId) {

            case APP_CONSTANTS.CONFIRM_BOOKING:
                if (discountAmount > 0 && TextUtils.isEmpty(binding.txtCouponCode.getText().toString())) {
                    showSnackBar(oops, "Enter valid coupon code", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;
            case APP_CONSTANTS.PHONE_BOOKING:
                if ((GA_AllowReturnPhoneBooking == 1) || (GA_AllowReturnPhoneBooking == 0 && sourceCityId == loginBranchCityId)) {
                    bookingTypeFlag = true;
                } else {
                    showSnackBar(oops, "You Can Not Allow To Book Return Phone Booking", Toast.LENGTH_LONG, 1);
                    bookingTypeFlag = false;
                }
                break;
            case APP_CONSTANTS.AGENT_BOOKING:
                // Agent
                if (binding.spnAgentCity.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent city", Toast.LENGTH_LONG, 1);
                } else if (binding.spnAgentName.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent name", Toast.LENGTH_LONG, 1);
                } else if (TextUtils.isEmpty(binding.etAgentTicketNo.getText().toString())) {
                    showSnackBar(oops, "Enter valid agent ticket no", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;
            case APP_CONSTANTS.BRANCH_BOOKING:
                // Branch
                if (binding.spnBranch.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid branch name", Toast.LENGTH_LONG, 1);
                } else if (binding.spnBranchUser.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid branch user name", Toast.LENGTH_LONG, 1);
                } else if (TextUtils.isEmpty(binding.etBranchTicketNo.getText().toString())) {
                    showSnackBar(oops, "Enter valid branch ticket no", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;
            case APP_CONSTANTS.CMP_CARD_BOOKING:
                // Company Card

                if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (TextUtils.isEmpty(binding.etCmpBkgCardno.getText().toString())) {
                        showSnackBar(oops, "Enter valid company card no", Toast.LENGTH_LONG, 1);
                    } else if (!cmpCardNo.equalsIgnoreCase(binding.etCmpBkgCardno.getText().toString())) {
                        showSnackBar(oops, "Enter valid company card no", Toast.LENGTH_LONG, 1);
                    } else {
                        bookingTypeFlag = true;
                    }
                } else {
                    if (TextUtils.isEmpty(binding.etCmpBkgCardno.getText().toString())) {
                        showSnackBar(oops, "Enter valid company card no", Toast.LENGTH_LONG, 1);
                    } else if (TextUtils.isEmpty(binding.etCmpCardOtp.getText().toString())) {
                        showSnackBar(oops, "Enter valid company card OTP", Toast.LENGTH_LONG, 1);
                    } else if (!cmpCardNo.equalsIgnoreCase(binding.etCmpBkgCardno.getText().toString())) {
                        showSnackBar(oops, "Enter valid company card no", Toast.LENGTH_LONG, 1);
                    } else if (!cmpCardOTP.equalsIgnoreCase(binding.etCmpCardOtp.getText().toString())) {
                        showSnackBar(oops, "Enter valid company card OTP", Toast.LENGTH_LONG, 1);
                    } else if (selSeats.size() > OS_MaxCompanyCardBooking) {
                        showSnackBar(oops, "You can't book seat more than " + OS_MaxCompanyCardBooking + " in " + reqBookingTypeName, Toast.LENGTH_LONG, 1);
                    } else if (TextUtils.isEmpty(binding.etCmpCardDisc.getText().toString().trim())) {
                        showSnackBar(oops, "Enter valid discount amount", Toast.LENGTH_LONG, 1);
                    } else if (Double.parseDouble(binding.etCmpCardDisc.getText().toString().trim()) <= 0) {
                        if (cmpCardMaxDiscount > 0) {
                            binding.etCmpCardDisc.setText(String.format(Locale.getDefault(), "%.2f", cmpCardMaxDiscount));
                            showSnackBar(oops, "Max discount amount is " + String.format(Locale.getDefault(), "%.2f", cmpCardMaxDiscount), Toast.LENGTH_LONG, 1);
                        } else {
                            showSnackBar(oops, "Enter valid discount amount", Toast.LENGTH_LONG, 1);
                        }
                    } else if (Double.parseDouble(binding.etCmpCardDisc.getText().toString().trim()) > cmpCardBalance) {
                        showSnackBar(oops, "Discount amount can't be greater than wallet balance", Toast.LENGTH_LONG, 1);
                    } else if (Double.parseDouble(binding.etCmpCardDisc.getText().toString().trim()) > cmpCardMaxDiscount) {
                        showSnackBar(oops, "You can redeem max discount " + cmpCardMaxDiscount, Toast.LENGTH_LONG, 1);
                    } else {
                        bookingTypeFlag = true;
                    }
                }

                break;
            case APP_CONSTANTS.AGENT_QUOTA_BOOKING:
                // Agent Quota
                if (binding.spnAgentCity.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent city", Toast.LENGTH_LONG, 1);
                } else if (binding.spnAgentName.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent name", Toast.LENGTH_LONG, 1);
                } else {
                    try {
                        if (sdf_full.parse(binding.txtAgentquotaDate.getText().toString()).getTime() >= reqDate.getTime()) {
                            bookingTypeFlag = true;
                        } else {
                            showSnackBar(oops, "Select valid agent quota to date", Toast.LENGTH_LONG, 1);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case APP_CONSTANTS.AGENT_PHONE_BOOKING:
                // Agent Phone
                if (binding.spnAgentCity.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent city", Toast.LENGTH_LONG, 1);
                } else if (binding.spnAgentName.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent name", Toast.LENGTH_LONG, 1);
                } else if (TextUtils.isEmpty(binding.etAgentTicketNo.getText().toString())) {
                    showSnackBar(oops, "Enter valid agent ticket no", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;
            case APP_CONSTANTS.API_PHONE_BOOKING:
                // API Phone
                if (binding.spnAgentCity.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent city", Toast.LENGTH_LONG, 1);
                } else if (binding.spnAgentName.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid agent name", Toast.LENGTH_LONG, 1);
                } else if (TextUtils.isEmpty(binding.etAgentTicketNo.getText().toString())) {
                    showSnackBar(oops, "Enter valid agent ticket no", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;
            case APP_CONSTANTS.ITSPL_WALLET_BOOKING:
                // ITSPL Wallets
                if (binding.spnItsplWallet.getSelectedItemPosition() == -1) {
                    showSnackBar(oops, "Select valid ITSPL wallet", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;
            case APP_CONSTANTS.REMOTE_PAYMENT_BOOKING:
                // Remote Payment
                if (TextUtils.isEmpty(binding.etEmail.getText().toString())) {
                    showSnackBar(oops, validEmail, Toast.LENGTH_LONG, 1);
                    binding.etEmail.requestFocus();
                } else if (!binding.etEmail.getText().toString().trim().matches("[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.+[a-z.A-Z0-9]+")) {
                    showSnackBar(oops, validEmail, Toast.LENGTH_LONG, 1);
                    binding.etEmail.requestFocus();
                } else if (OS_PhoneBookingToRemotePayment == 0 && modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                    showSnackBar(oops, "You Can Not Allow Phone To Remote Payment Booking", Toast.LENGTH_LONG, 1);
                } else {
                    bookingTypeFlag = true;
                }
                break;

            default:
                bookingTypeFlag = true;
                break;
        }
        return bookingTypeFlag;
    }

    private boolean checkSeatValidation() {
        boolean seatFlag = false;
        if (selSeats != null && selSeats.size() > 0) {
            if (OS_AllowToFetchAllPaxDetail == 1) {
                for (SelectedSeatDetailsBean data : selSeats) {
                    if (TextUtils.isEmpty(data.getJMPhone1()) || data.getJMPhone1().length() <= 9) {
                        seatFlag = false;
                        showSnackBar(oops, validMobile, Toast.LENGTH_LONG, 1);
                        break;
                    } else if (TextUtils.isEmpty(data.getJMPassengerName())) {
                        seatFlag = false;
                        showSnackBar(oops, "Enter valid passenger name", Toast.LENGTH_LONG, 1);
                        break;
                    } else if (TextUtils.isEmpty(data.getPaxAge()) || Integer.parseInt(data.getPaxAge()) <= 0) {
                        seatFlag = false;
                        showSnackBar(oops, "Enter valid passenger age", Toast.LENGTH_LONG, 1);
                        break;
                    } else if (OS_IsIDProofMandatory == 1 && TextUtils.isEmpty(data.getIdProofNumber())) {
                        seatFlag = false;
                        showSnackBar(oops, "Enter valid id proof number", Toast.LENGTH_LONG, 1);
                        break;
                    } else if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT && data.getSeatRate() <= 0) {
                        seatFlag = false;
                        showSnackBar(oops, "Enter valid seat no or rate", Toast.LENGTH_LONG, 1);
                        break;
                    } else {
                        seatFlag = true;
                    }
                }
            } else {
                if (TextUtils.isEmpty(binding.etPaxName.getText())) {
                    showSnackBar(oops, "Enter passenger name", Toast.LENGTH_LONG, 1);
                } else if (TextUtils.isEmpty(binding.etPaxPhone1.getText())) {
                    showSnackBar(oops, "Enter passenger phone no", Toast.LENGTH_LONG, 1);
                } else {
                    seatFlag = true;
                }
            }

        } else {
            showSnackBar(oops, "Please select atleast 1 seat", Toast.LENGTH_LONG, 1);
        }
        return seatFlag;
    }

    private int isValidSeatNo() {
        modifySeats = "";
        //data.getIsAllowProcess() == 0
        int currentRoute = 0;
        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
            currentRoute = routeId == selSeats.get(0).getRouteId() && routeTimeId == selSeats.get(0).getRouteTimeId() ? 1 : 0;
        }
        //(data.getIsAllowProcess() == 0 || currentRoute == 1)

        int loopCounter = 0;
        LinkedHashMap<String, List<SelectedSeatDetailsBean>> linkedHashMap = new LinkedHashMap<>();

        for (SelectedSeatDetailsBean dataSelectSeats : selSeats) {

            /*if (dataSelectSeats.getCADUDType() != null && !linkedHashMap.containsKey(dataSelectSeats.getBADSeatNo() + dataSelectSeats.getCADUDType())) {
                List<SelectedSeatDetailsBean> list = new ArrayList<>();
                list.add(dataSelectSeats);
                linkedHashMap.put(dataSelectSeats.getBADSeatNo() + dataSelectSeats.getCADUDType(), list);
            } else if (dataSelectSeats.getCADUDType() != null) {
                List<SelectedSeatDetailsBean> list = linkedHashMap.get(dataSelectSeats.getBADSeatNo() + dataSelectSeats.getCADUDType());
                list.add(dataSelectSeats);
                linkedHashMap.put(dataSelectSeats.getBADSeatNo() + dataSelectSeats.getCADUDType(), list);
            }*/

            if (!linkedHashMap.containsKey(dataSelectSeats.getBADSeatNo())) {
                List<SelectedSeatDetailsBean> list = new ArrayList<>();
                list.add(dataSelectSeats);
                linkedHashMap.put(dataSelectSeats.getBADSeatNo(), list);
            } else {
                List<SelectedSeatDetailsBean> list = linkedHashMap.get(dataSelectSeats.getBADSeatNo());
                list.add(dataSelectSeats);
                linkedHashMap.put(dataSelectSeats.getBADSeatNo(), list);
            }

            if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT && dataSelectSeats.getSeatRate() <= 0) {
                loopCounter = 0;
                break;
            }

            for (SeatArrangement_Response.Datum data : listSeatDetails) {
                if ((!TextUtils.isEmpty(data.getBADSeatNo())) && (!TextUtils.isEmpty(dataSelectSeats.getBADSeatNo())) && data.getBADSeatNo().equalsIgnoreCase(dataSelectSeats.getBADSeatNo()) && (data.getIsAllowProcess() == 0 || currentRoute == 1)) {
                    int seatRate = 0;
                    if (currentRoute == 1 && data.getIsAllowProcess() == 1 && TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                        if (modifySeatsTemp.contains(data.getBADSeatNo())) {
                            if (data.getCADBusType() == 0) {
                                if (data.getCADSeatType() == 0) {
                                    seatRate = Integer.parseInt(binding.etAcSeat.getText().toString().trim()) == 0 ? oldAcSeatRate : Integer.parseInt(binding.etAcSeat.getText().toString().trim());
                                } else if (data.getCADSeatType() == 1) {
                                    seatRate = Integer.parseInt(binding.etAcSleeper.getText().toString().trim()) == 0 ? oldAcSlpRate : Integer.parseInt(binding.etAcSleeper.getText().toString().trim());
                                } else if (data.getCADSeatType() == 2) {
                                    seatRate = Integer.parseInt(binding.etAcSlumber.getText().toString().trim()) == 0 ? oldAcSlmbRate : Integer.parseInt(binding.etAcSlumber.getText().toString().trim());
                                }
                            } else if (data.getCADBusType() == 1) {
                                if (data.getCADSeatType() == 0) {
                                    seatRate = Integer.parseInt(binding.etNonacSeat.getText().toString().trim()) == 0 ? oldNonAcSeatRate : Integer.parseInt(binding.etNonacSeat.getText().toString().trim());
                                } else if (data.getCADSeatType() == 1) {
                                    seatRate = Integer.parseInt(binding.etNonacSleeper.getText().toString().trim()) == 0 ? oldNonAcSlpRate : Integer.parseInt(binding.etNonacSleeper.getText().toString().trim());
                                } else if (data.getCADSeatType() == 2) {
                                    seatRate = Integer.parseInt(binding.etNonacSlumber.getText().toString().trim()) == 0 ? oldNonAcSlmbRate : Integer.parseInt(binding.etNonacSlumber.getText().toString().trim());
                                }
                            }
                            dataSelectSeats.setSeatRate(seatRate);
                            modifySeats = TextUtils.isEmpty(modifySeats) ? data.getBADSeatNo() : modifySeats + "," + data.getBADSeatNo();
                            loopCounter++;
                            break;
                        }
                    } else {
                        if (data.getCADBusType() == 0) {
                            if (data.getCADSeatType() == 0) {
                                seatRate = Integer.parseInt(binding.etAcSeat.getText().toString().trim()) == 0 ? oldAcSeatRate : Integer.parseInt(binding.etAcSeat.getText().toString().trim());
                            } else if (data.getCADSeatType() == 1) {
                                seatRate = Integer.parseInt(binding.etAcSleeper.getText().toString().trim()) == 0 ? oldAcSlpRate : Integer.parseInt(binding.etAcSleeper.getText().toString().trim());
                            } else if (data.getCADSeatType() == 2) {
                                seatRate = Integer.parseInt(binding.etAcSlumber.getText().toString().trim()) == 0 ? oldAcSlmbRate : Integer.parseInt(binding.etAcSlumber.getText().toString().trim());
                            }
                        } else if (data.getCADBusType() == 1) {
                            if (data.getCADSeatType() == 0) {
                                seatRate = Integer.parseInt(binding.etNonacSeat.getText().toString().trim()) == 0 ? oldNonAcSeatRate : Integer.parseInt(binding.etNonacSeat.getText().toString().trim());
                            } else if (data.getCADSeatType() == 1) {
                                seatRate = Integer.parseInt(binding.etNonacSleeper.getText().toString().trim()) == 0 ? oldNonAcSlpRate : Integer.parseInt(binding.etNonacSleeper.getText().toString().trim());
                            } else if (data.getCADSeatType() == 2) {
                                seatRate = Integer.parseInt(binding.etNonacSlumber.getText().toString().trim()) == 0 ? oldNonAcSlmbRate : Integer.parseInt(binding.etNonacSlumber.getText().toString().trim());
                            }
                        }
                        dataSelectSeats.setSeatRate(seatRate);
                        modifySeats = TextUtils.isEmpty(modifySeats) ? data.getBADSeatNo() : modifySeats + "," + data.getBADSeatNo();
                        loopCounter++;
                        break;
                    }

                }
            }
        }

        List<String> keyList = new ArrayList<>();
        if (linkedHashMap != null && linkedHashMap.size() > 0) {
            keyList = new ArrayList<>(linkedHashMap.keySet());
        }

        if (loopCounter == keyList.size()) {
            loopCounter = 1;
        } else {
            loopCounter = 0;
        }

        return loopCounter;
    }

    private void calTotalSeat() {

        if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS && modifyBookingTypeId != APP_CONSTANTS.WAITING_BOOKING) {
            return;
        }

        acSeatTotal = 0;
        acSlpTotal = 0;
        acSlmbTotal = 0;
        nonAcSeatTotal = 0;
        nonAcSlpTotal = 0;
        nonAcSlmbTotal = 0;

        JM_OriginalTotalPayable = 0;
        totalRoundUp = 0.0;

        seatNameList = "";

        totalBaseFare = 0.0;
        totalGST = 0.0;
        totalPayableAmount = 0.0;


        if (selSeats != null && selSeats.size() > 0) {

            int IsIncludeTax = 0, isAcBus = 0;

            for (SelectedSeatDetailsBean data : selSeats) {

                if (!TextUtils.isEmpty(data.getIsIncludeTax())) {
                    IsIncludeTax = Integer.parseInt(data.getIsIncludeTax());
                }

                seatNameList = TextUtils.isEmpty(seatNameList) ? data.getBADSeatNo() : seatNameList + "," + data.getBADSeatNo();

                if (data.getCADBusType() != null && data.getCADBusType() == 0) {
                    isAcBus = 0;
                    if (data.getCADSeatType() == 0) {
                        newAcSeatRate = newAcSeatRate <= 0 ? oldAcSeatRate : newAcSeatRate;
                        acSeatTotal += 1;
                        JM_OriginalTotalPayable += oldAcSeatRate;
                        totalBaseFare += newAcSeatRate;
                    } else if (data.getCADSeatType() == 1 && IsIncludeTax == 0) {
                        newAcSlpRate = newAcSlpRate <= 0 ? oldAcSlpRate : newAcSlpRate;
                        acSlpTotal += 1;
                        JM_OriginalTotalPayable += oldAcSlpRate;
                        totalBaseFare += newAcSlpRate;
                    } else if (data.getCADSeatType() == 2 && IsIncludeTax == 0) {
                        newAcSlmbRate = newAcSlmbRate <= 0 ? oldAcSlmbRate : newAcSlmbRate;
                        acSlmbTotal += 1;
                        JM_OriginalTotalPayable += oldAcSlmbRate;
                        totalBaseFare += newAcSlmbRate;
                    }
                } else if (data.getCADBusType() != null && data.getCADBusType() == 1) {
                    isAcBus = 1;
                    if (data.getCADSeatType() == 0) {
                        newNonAcSeatRate = newNonAcSeatRate <= 0 ? oldNonAcSeatRate : newNonAcSeatRate;
                        nonAcSeatTotal += 1;
                        JM_OriginalTotalPayable += oldNonAcSeatRate;
                        totalBaseFare += newNonAcSeatRate;
                    } else if (data.getCADSeatType() == 1) {
                        newNonAcSlpRate = newNonAcSlpRate <= 0 ? oldNonAcSlpRate : newNonAcSlpRate;
                        nonAcSlpTotal += 1;
                        JM_OriginalTotalPayable += oldNonAcSlpRate;
                        totalBaseFare += newNonAcSlpRate;
                    } else if (data.getCADSeatType() == 2) {
                        newNonAcSlmbRate = newNonAcSlmbRate <= 0 ? oldNonAcSlmbRate : newNonAcSlmbRate;
                        nonAcSlmbTotal += 1;
                        JM_OriginalTotalPayable += oldNonAcSlmbRate;
                        totalBaseFare += newNonAcSlmbRate;
                    }

                }

            }


            totalBaseFare += fetchExtraValueOnRoute(totalBaseFare);
            if (bookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {
                totalBaseFare += fetchAgentWiseExtraCharge(totalBaseFare);
            }

            if (IsIncludeTax == 0) {
                if (isAcBus == 0 && CM_IsACServiceTax == 1 && CM_ACServiceTax > 0) {
                    // AC
                    totalGST = calServiceTax(totalBaseFare, CM_ACServiceTax);
                } else if (CM_IsNonACServiceTax == 1 && CM_NonACServiceTax > 0) {
                    // Non AC
                    totalGST = calServiceTax(totalBaseFare, CM_NonACServiceTax);
                }
            }

            if (CM_ServiceTaxRoundUp > 0 && totalGST > 0 && totalGST % CM_ServiceTaxRoundUp != 0) {
                double tempTotalRoundUp = getServiceTaxRoundUpLocal(totalGST, CM_ServiceTaxRoundUp);
                totalRoundUp = tempTotalRoundUp - totalGST;
            }

            totalPayableAmount = totalBaseFare;
            if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING && listDiscountCopon != null && listDiscountCopon.size() > 0) {
                calDiscount();
            } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING && listCompanyCard != null && listCompanyCard.size() > 0) {
                cmpCardMaxDiscount = calCompanyCardDetails(totalPayableAmount);
                discountAmount = 0;
                if (cmpCardMaxDiscount > 0) {
                    if (!TextUtils.isEmpty(binding.etCmpCardDisc.getText().toString())) {
                        discountAmount = Double.parseDouble(binding.etCmpCardDisc.getText().toString());
                        if (discountAmount > cmpCardMaxDiscount) {
                            discountAmount = cmpCardMaxDiscount;
                            binding.etCmpCardDisc.setText(String.valueOf((int) discountAmount));
                            binding.etCmpCardDisc.setSelection(binding.etCmpCardDisc.getText().toString().length());
                        }
                    }
                }
            } else {
                discountAmount = 0;
            }


            binding.etTotAcSeat.setText(String.valueOf(acSeatTotal));
            binding.etTotAcSleeper.setText(String.valueOf(acSlpTotal));
            binding.etTotAcSlumber.setText(String.valueOf(acSlmbTotal));

            binding.etTotNonacSeat.setText(String.valueOf(nonAcSeatTotal));
            binding.etTotNonacSleeper.setText(String.valueOf(nonAcSlpTotal));
            binding.etTotNonacSlumber.setText(String.valueOf(nonAcSlmbTotal));

            binding.etTotGst.setText(String.format(Locale.getDefault(), "%.2f", totalGST + totalRoundUp));
            binding.etTotalAmount.setText(String.format(Locale.getDefault(), "%.2f", ((totalPayableAmount + totalGST + totalRoundUp) - discountAmount)));
            totalPax = selSeats.size();
            binding.etNoOfSeat.setText(String.valueOf(totalPax));
        } else {
            resetSeats();
            if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING && listDiscountCopon != null && listDiscountCopon.size() > 0) {
                calDiscount();
            } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING && listCompanyCard != null && listCompanyCard.size() > 0) {
                cmpCardMaxDiscount = calCompanyCardDetails(totalPayableAmount);
                discountAmount = 0;
            }
        }

    }

    private void clearCompanyCardDetails() {
        cmpCardNo = "";
        cmpCardBalance = 0;
        cmpCardMaxDiscount = 0;
        cmpCardOTP = "";
        cmpCardId = "";
        if (listCompanyCard != null && listCompanyCard.size() > 0) {
            listCompanyCard.clear();
        }
    }

    private double calServiceTax(double seatRate,
                                 double serviceTax) {

        return ((seatRate * serviceTax) / 100);
       /* double serviceTaxRoundUp;
        if (CM_ServiceTaxRoundUp > 0) {
            serviceTaxRoundUp = getServiceTaxRoundUpLocal(tempServiceTax, CM_ServiceTaxRoundUp);
        } else {
            serviceTaxRoundUp = tempServiceTax;
        }

        if(serviceTaxRoundUp==0){
            serviceTaxRoundUp=tempServiceTax;
        }

        double roundUp=serviceTaxRoundUp - tempServiceTax;*/

        //dataSeat.setServiceTaxRoundUp(roundUp);


    }

    private void resetSeats() {
        totalPax = 0;

        binding.etTotAcSeat.setText("0");
        binding.etTotAcSleeper.setText("0");
        binding.etTotAcSlumber.setText("0");
        binding.etTotNonacSeat.setText("0");
        binding.etTotNonacSleeper.setText("0");
        binding.etTotNonacSlumber.setText("0");

        binding.etTotGst.setText("0");
        binding.etTotalAmount.setText("0");

        binding.etNoOfSeat.setText("0");

    }

    private void showSeatSelectionDialog(final View arg0, SeatArrangement_Response.Datum data) {
        if (alertSeatSelect != null && alertSeatSelect.isShowing()) {
            alertSeatSelect.dismiss();
        }

        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.seatselection_dialog_v2, viewGroup, false);

        TextView txt_seat_no, txt_base_fare, txt_gst, txt_total;
        final TextView btn_cancel, btn_male, btn_female;

        txt_seat_no = dialogView.findViewById(R.id.txt_seat_no);
        txt_base_fare = dialogView.findViewById(R.id.txt_base_fare);
        txt_gst = dialogView.findViewById(R.id.txt_gst);
        txt_total = dialogView.findViewById(R.id.txt_total);

        btn_cancel = dialogView.findViewById(R.id.btn_cancel);
        btn_male = dialogView.findViewById(R.id.btn_male);
        btn_female = dialogView.findViewById(R.id.btn_female);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        alertSeatSelect = builder.create();
        if (!isFinishing())
            alertSeatSelect.show();

        if (arg0.isSelected()) {
            btn_cancel.setText("Deselect");
        }

        txt_seat_no.setText(data.getBADSeatNo());
        txt_base_fare.setText(getResources().getString(R.string.rs) + String.format(Locale.getDefault(), "%.2f", (data.getSeatRate() - data.getServiceTax())));
        txt_gst.setText(getResources().getString(R.string.rs) + String.format(Locale.getDefault(), "%.2f", (data.getServiceTax())));
        txt_total.setText(getResources().getString(R.string.rs) + String.format(Locale.getDefault(), "%.2f", (data.getSeatRate())));


        final AlertDialog finalAlertDialog = alertSeatSelect;
        btn_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!arg0.isSelected()) {
                    setSeatBackground((Button) arg0, APP_CONSTANTS.SELECTED_SEAT_COLOR, -1);
                    arg0.setSelected(true);
                    SelectedSeatDetails(arg0.getId(), "M");

                    seatSelectedViewModel.addSeats(selSeats);
                }
                if (finalAlertDialog != null && finalAlertDialog.isShowing()) {
                    finalAlertDialog.dismiss();
                }

            }
        });


        btn_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!arg0.isSelected()) {
                    setSeatBackground((Button) arg0, APP_CONSTANTS.SELECTED_SEAT_COLOR, -1);
                    arg0.setSelected(true);
                    SelectedSeatDetails(arg0.getId(), "F");

                    seatSelectedViewModel.addSeats(selSeats);
                }
                if (finalAlertDialog != null && finalAlertDialog.isShowing()) {
                    finalAlertDialog.dismiss();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalAlertDialog != null && finalAlertDialog.isShowing()) {
                    finalAlertDialog.dismiss();
                }
                if (arg0.isSelected()) {
                    if (selSeats != null && selSeats.size() > 0) {
                        /*for (int i = 0; i < bookingTypeList.size(); i++) {
                            if (bookingTypeList.get(i).getBookingTypeId() == listSeatDetails.get(arg0.getId()).getBookingTypeID()) {
                                setSeatBackground((Button) arg0, bookingTypeList.get(i).getColor(), -1);
                                arg0.setSelected(false);
                                break;
                            }
                        }*/
                        for (int i = 0; i < selSeats.size(); i++) {
                            if (listSeatDetails.get(arg0.getId()).getBADSeatNo().equals(selSeats.get(i).getBADSeatNo())) {
                                setSeatBackground((Button) arg0, listSeatDetails.get(arg0.getId()).getBookingTypeColor(), -1);
                                arg0.setSelected(false);
                                selSeats.remove(i);
                                break;
                            }
                        }
                        seatSelectedViewModel.addSeats(selSeats);
                    }
                }
            }
        });

        //Now we need an AlertDialog.Builder object

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (AllowFareChnage == 1 && !verifyDoNotAllowFareChangeRoute()) {

        }

        try {
            if (getCurrentFocus() != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception ignored) {
        }

        return super.dispatchTouchEvent(ev);
    }

    private View.OnFocusChangeListener getFocusChange(final int seatRate) {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (!focus) {
                    EditText editText = (EditText) view;
                    String str = editText.getText().toString();
                    //AllowLessFareChange = 0;
                    if (AllowLessFareChange == 1) {
                        if (TextUtils.isEmpty(editText.getText())) {
                            editText.setText(String.valueOf(seatRate));
                            showSnackBar(oops, "You can't set empty seat price", Snackbar.LENGTH_LONG, 1);
                        }
                    } else {
                        if (!TextUtils.isEmpty(str)) {
                            if (seatRate < Double.parseDouble(str)) {
                                editText.setText(String.valueOf(seatRate));
                                showSnackBar(oops, "You can't set less price then given seat price", Snackbar.LENGTH_LONG, 1);
                            }
                        } else {
                            editText.setText(String.valueOf(seatRate));
                            showSnackBar(oops, "You can't set less price then given seat price", Snackbar.LENGTH_LONG, 1);
                        }

                    }
                }
            }
        };
    }

    private void setFocusChangeListener() {
        binding.etAcSeat.setOnFocusChangeListener(getFocusChange(oldAcSeatRate));
        binding.etAcSleeper.setOnFocusChangeListener(getFocusChange(oldAcSlpRate));
        binding.etAcSlumber.setOnFocusChangeListener(getFocusChange(oldAcSlmbRate));
        binding.etNonacSeat.setOnFocusChangeListener(getFocusChange(oldNonAcSeatRate));
        binding.etNonacSleeper.setOnFocusChangeListener(getFocusChange(oldNonAcSlpRate));
        binding.etNonacSlumber.setOnFocusChangeListener(getFocusChange(oldNonAcSlmbRate));
    }

    private void showSnackBar(String title, String desc, int duration, int is_error_img) {

        if (!isFinishing()) {
            int is_top = 0;

           /* Rect r = new Rect();
            binding.llMain.getWindowVisibleDisplayFrame(r);
            int screenHeight = binding.llMain.getRootView().getHeight();
            int keypadHeight = screenHeight - r.bottom;
            if (keypadHeight > screenHeight * 0.15) {
                is_top = 1;
            } else {
                is_top = 0;
            }*/
            if (TextUtils.isEmpty(desc)) {
                desc = badServerResponse;
            }

            final Snackbar snackbar = CustomSnackBar.showSnackbar(ExeBookingActivity_V2.this, binding.llMain, title, desc, duration, is_top);
            snackbar.show();

            if (is_error_img == 1) {
                // Error
                ImageView imgError = snackbar.getView().findViewById(R.id.img_error);
                imgError.clearAnimation();
                imgError.setVisibility(View.VISIBLE);

                imgError.setAnimation(OptAnimationLoader.loadAnimation(ExeBookingActivity_V2.this, R.anim.error_x_in));
            } else if (is_error_img == 2) {
                // Success

                FrameLayout frameLayout = snackbar.getView().findViewById(R.id.success_frame);
                frameLayout.clearAnimation();
                frameLayout.setVisibility(View.VISIBLE);

                frameLayout.setAnimation(OptAnimationLoader.loadAnimation(ExeBookingActivity_V2.this, R.anim.success_mask_layout));
            }


        }

    }

    private void clearSeatRate() {
        oldAcSeatRate = 0;
        oldAcSlpRate = 0;
        oldAcSlmbRate = 0;
        oldNonAcSeatRate = 0;
        oldNonAcSlpRate = 0;
        oldNonAcSlmbRate = 0;
        newAcSeatRate = 0;
        newAcSlpRate = 0;
        newAcSlmbRate = 0;
        newNonAcSeatRate = 0;
        newNonAcSlpRate = 0;
        newNonAcSlmbRate = 0;
    }

    private double calCompanyCardDetails(double totalPayableAmount) {

        double totalDiscountAmount = 0.0;
        double minimumAmountForDiscount = 0;
        StringBuilder cardDetails = new StringBuilder();


        for (CompanyCardDetails_Response.Datum data : listCompanyCard) {
            int IsDiscountRechargeWise = 0, IsDiscountPercentage = 0, IsDiscountAmount = 0;
            double CCM_Balance = 0, CNM_MinRedemption = 0, CNMMaxRedemption = 0, CNMMaxRedemptionOnTotalAmount = 0;

            if (TextUtils.isEmpty(cardDetails.toString())) {

                cmpCardNo = data.getCCMCardNo();
                cmpCardOTP = data.getCCMOTP();
                cmpCardId = data.getCNMCardNameID();

                if (!TextUtils.isEmpty(data.getCCMBalance())) {
                    cmpCardBalance = Double.parseDouble(data.getCCMBalance());
                }

                if (!TextUtils.isEmpty(data.getCardType())) {
                    cardDetails.append("\nCard Type : " + data.getCardType() + "\n");
                }

                if (!TextUtils.isEmpty(data.getCCMName())) {
                    cardDetails.append("Owner Name : " + data.getCCMName() + "\n");
                }

                if (!TextUtils.isEmpty(data.getCCMPhoneNo1())) {
                    cardDetails.append("Mobile No : " + data.getCCMPhoneNo1() + "\n");
                }

                if (!TextUtils.isEmpty(data.getCCMAddress())) {
                    cardDetails.append("Address : " + data.getCCMAddress() + "\n");
                }

                if (!TextUtils.isEmpty(data.getCNMMaxRedemptionOnTotalAmount())) {
                    cardDetails.append("Maximum Redeemtion On Total Amount " + data.getCNMMaxRedemptionOnTotalAmount() + " %" + "\n");
                }


                binding.txtCardType.setText("Card type : " + data.getCardType());
                binding.txtCardBalance.setText("Balance :  " + data.getCCMBalance());
                //binding.txtCmpCardDetails.setMovementMethod(new ScrollingMovementMethod());
            }

            if (totalPayableAmount > 0) {
                if (!TextUtils.isEmpty(data.getCPMDicountRechargeWise())) {
                    IsDiscountRechargeWise = Integer.parseInt(data.getCPMDicountRechargeWise());
                }

                if (!TextUtils.isEmpty(data.getCPMPercentage())) {
                    IsDiscountPercentage = Integer.parseInt(data.getCPMPercentage());
                }

                if (!TextUtils.isEmpty(data.getCPMAmount())) {
                    IsDiscountAmount = Integer.parseInt(data.getCPMAmount());
                }

                if (!TextUtils.isEmpty(data.getCNMMaxRedemptionOnTotalAmount())) {
                    CNMMaxRedemptionOnTotalAmount = Double.parseDouble(data.getCNMMaxRedemptionOnTotalAmount());
                }


                if (!TextUtils.isEmpty(data.getCNMMaxRedemption())) {
                    CNMMaxRedemption = Double.parseDouble(data.getCNMMaxRedemption());
                }

                if (!TextUtils.isEmpty(data.getCCMBalance())) {
                    CCM_Balance = Double.parseDouble(data.getCCMBalance());
                }

                if (!TextUtils.isEmpty(data.getCNMMinRedemption())) {
                    CNM_MinRedemption = Double.parseDouble(data.getCNMMinRedemption());
                }


                if (data.getCNMISWallet() == 0) {
                    if (!TextUtils.isEmpty(data.getCPRMinimumAmount())) {
                        minimumAmountForDiscount = Double.parseDouble(data.getCPRMinimumAmount());
                    }
                    if (IsDiscountRechargeWise == 1) {
                        double fromAmount = 0, toAmount = 0, discPer = 0, discRecharge = 0;
                        if (!TextUtils.isEmpty(data.getCPRFromAmount())) {
                            fromAmount = Double.parseDouble(data.getCPRFromAmount());
                        }
                        if (!TextUtils.isEmpty(data.getCPRToAmount())) {
                            toAmount = Double.parseDouble(data.getCPRToAmount());
                        }
                        if (totalPayableAmount >= fromAmount && totalPayableAmount <= toAmount) {
                            if (!TextUtils.isEmpty(data.getCPRPercentage())) {
                                discPer = Double.parseDouble(data.getCPRPercentage());
                            }
                            if (!TextUtils.isEmpty(data.getCPRDiscountRechrgeWise())) {
                                discRecharge = Double.parseDouble(data.getCPRDiscountRechrgeWise());
                            }
                            if (discPer != 0) {
                                totalDiscountAmount = totalPayableAmount * (discPer / 100);
                                break;
                            } else {
                                totalDiscountAmount = discRecharge;
                                break;
                            }
                        }
                    } else {
                        if (totalPayableAmount >= minimumAmountForDiscount) {
                            double fromAmount = 0, discPer = 0;

                            if (!TextUtils.isEmpty(data.getCPRFromAmount())) {
                                fromAmount = Double.parseDouble(data.getCPRFromAmount());
                            }

                            if (!TextUtils.isEmpty(data.getCPRPercentage())) {
                                discPer = Double.parseDouble(data.getCPRPercentage());
                            }

                            if (IsDiscountPercentage == 1) {
                                totalDiscountAmount = (totalPayableAmount * discPer) / 100;
                                break;
                            } else if (IsDiscountAmount == 1) {
                                totalDiscountAmount = fromAmount;
                                break;
                            }

                        }
                    }
                } else if (data.getCNMISWallet() == 1) {
                    if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT ||
                            (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY && modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING) &&
                                    bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                        if (CNMMaxRedemptionOnTotalAmount > 0) {
                            totalDiscountAmount = (totalPayableAmount * CNMMaxRedemptionOnTotalAmount) / 100;
                            totalDiscountAmount = CCM_Balance > totalDiscountAmount ? totalDiscountAmount : CCM_Balance;
                            break;
                        } else if (CNMMaxRedemption > 0) {
                            totalDiscountAmount = (CCM_Balance * CNMMaxRedemption) / 100;
                            break;
                        } else if (CNMMaxRedemption <= 0) {
                            totalDiscountAmount = (CCM_Balance * CNM_MinRedemption) / 100;
                            break;
                        }
                    }
                }
            } else {
                break;
            }


        }

        if (!TextUtils.isEmpty(cardDetails.toString())) {

            if (totalDiscountAmount > 0) {
                cardDetails.append("Maximum Discount : " + String.format(Locale.getDefault(), "%.2f", totalDiscountAmount));
            }

            binding.txtCmpCardDetails.setText(cardDetails.toString());

            if (animCompanyCardDetails == null) {
                try {
                    animCompanyCardDetails = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.anim_company_card);
                    binding.txtCmpCardDetails.startAnimation(animCompanyCardDetails);
                } catch (Exception ignored) {
                }
            }

        }

        return totalDiscountAmount;

    }

    private double fetchAgentWiseExtraCharge(double totalBaseFare) {
        double extraFare = 0;
        if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT ||
                (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY && modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING) &&
                        bookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {
            if (itsGetAllAgentByCompanyIDS != null && itsGetAllAgentByCompanyIDS.size() > 0) {
                if (CAM_OtherChargeType == 0 && CAM_OtherCharge > 0) {
                    extraFare = CAM_OtherCharge;
                } else if (CAM_OtherCharge > 0) {
                    extraFare = (totalBaseFare * CAM_OtherCharge) / 100;
                }
            }
        }

        return extraFare;
    }

    private double fetchExtraValueOnRoute(double totalBaseFare) {
        double extraFare = 0;

        if (OS_GetExtraValueOnFare == APP_CONSTANTS.RIGHTS_ALLOW && itsRouteTimeExtraFareGetList != null && itsRouteTimeExtraFareGetList.size() > 0) {
            if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT || (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY && modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING)) {
                for (Get_API_EXE_RouteTimeALLXML_Response.ITSRouteTimeExtraFareGet data : itsRouteTimeExtraFareGetList) {
                    if (data.getRMRouteID() == routeId && data.getRTRouteTimeID() == routeTimeId) {
                        if (data.getMinPax() != null && data.getMinPax() >= totalPax || binding.chkMultipleBooking.isChecked()) {
                            if (data.getValueType() == 0) {
                                // Rs
                                extraFare = data.getExtraValue();
                            } else if (data.getValueType() == 1) {
                                // Percentage
                                if (data.getExtraValue() > 0) {
                                    extraFare = (totalBaseFare * data.getExtraValue()) / 100;
                                }
                            }
                        }
                        break;
                    }
                }
            }

        }
        return extraFare;
    }

    private int isAllowBackDateBooking() {
        int flag = -1;
        //Flag==> -1 -> Continue Booking ,  0-> Don't Allow , 1-> Allow , 2-> Allow And Prompt

        if (TRANSACTION_TYPE != APP_CONSTANTS.MODIFY) {
            try {
                if (sdf_full.parse(str_journeyDate).compareTo(sdf_full.parse(date_today)) < 0) {
                    if (GA_AllowBackDateBooking == 0) {
                        flag = 0;
                        showToast("You can't book back date seat");
                    } else {
                        flag = 1;
                        if (GA_IsPromptBackDateBooking == 1) {
                            flag = 2;
                        }
                    }
                }
            } catch (Exception ignored) {
            }
        }

        return flag;
    }

    private void checkMultipleBooking() {
        if (binding.chkMultipleBooking.isChecked()) {
            AllowMultipleBooking = 1;
        } else if (OS_AllowToFetchAllPaxDetail == 1 && !binding.chkMultipleBooking.isChecked()) {
            AllowMultipleBooking = 2;
        } else {
            AllowMultipleBooking = 0;
        }
    }

    private void saveLastTransactionDetails(String seatName, String paxPhone, String
            resStatus, String transStatus) {
        showLastTransactionDetails(
                binding.txtRouteName.getText().toString().trim(),
                sel_SourceCityName + " To " + sel_DestinationCityName,
                seatName,
                paxPhone,
                str_journeyDate,
                jStartTime,
                resStatus,
                transStatus,
                reqBookingTypeName,
                binding.etTotalAmount.getText().toString()
        );
    }

    private void insertBookingApiCall() {
        releaseBookingTypeIds();

        // Journey Status = 1 When Yes And Print OtherWise 0

        showProgressDialog("Loading...");

        /*if (bookingTypeId == 1 && listDiscountCopon != null && listDiscountCopon.size() > 0) {
            calDiscount();
        }*/

        checkMultipleBooking();

        String paxName, paxPhone1, paxPhone2 = "", paxEmail, paxRemarks, seatDetailsParameters = "", seatNameList = "";

        if (OS_AllowToFetchAllPaxDetail == 0) {
            paxName = binding.etPaxName.getText().toString().trim();
            paxPhone1 = binding.etPaxPhone1.getText().toString().trim();
            paxPhone2 = binding.etPaxPhone2.getText().toString().trim();
        } else {
            paxName = selSeats.get(0).getJMPassengerName().trim();
            paxPhone1 = selSeats.get(0).getJMPhone1().trim();
        }
        paxEmail = binding.etEmail.getText().toString().trim();
        paxRemarks = binding.etRemarks.getText().toString().trim();
        PH_CardHolderName = paxName;

        IsGstChecked = binding.chkGST.isChecked() ? 1 : 0;

        pickUpId = listPickUp.get(binding.spnPickup.getSelectedItemPosition()).getPSMPickupID();
        dropId = listDropUp.get(binding.spnDrop.getSelectedItemPosition()).getPSMPickupID();


        String seatParams = "";
        for (SelectedSeatDetailsBean data : selSeats) {

            seatNameList = TextUtils.isEmpty(seatNameList) ? data.getBADSeatNo() : seatNameList + "," + data.getBADSeatNo();

            if (AllowMultipleBooking == 0) {
                if (OS_AllowToFetchAllPaxDetail == 1) {
                    seatParams = data.getBADSeatNo() + "," + data.getJPGender() + "," + data.getSeatRate() + "," + data.getCADBusType() + "," + data.getCADSeatType() + discountAmount + ", " + data.getPaxAge() + "," + data.getIdProofId() + "," + data.getIdProofNumber();
                } else {
                    seatParams = data.getBADSeatNo() + "," + data.getJPGender() + "," + data.getSeatRate() + "," + data.getCADBusType() + "," + data.getCADSeatType();
                }

            } else if (AllowMultipleBooking == 1) {
                if (OS_AllowToFetchAllPaxDetail == 1) {
                    seatParams = data.getBADSeatNo() + "," + "0" + "," + data.getJPGender() + ", " + data.getJMPhone1() + ", " + data.getJMPassengerName() + ", " +
                            data.getSeatRate() + "," + data.getCADBusType() + ", " + data.getCADSeatType() + ", " + discountAmount + ", " + data.getPaxAge();
                } else {
                    seatParams = data.getBADSeatNo() + "," + "0" + "," + data.getJPGender() + ", " + paxPhone1 + ", " + paxName + ", " +
                            data.getSeatRate() + "," + data.getCADBusType() + ", " + data.getCADSeatType() + ", " + discountAmount + ", " + data.getPaxAge();
                }

            } else if (AllowMultipleBooking == 2) {
                seatParams = data.getBADSeatNo() + "," + "0" + "," + data.getJPGender() + ", " + data.getJMPhone1() + ", " + data.getJMPassengerName() + ", " +
                        data.getSeatRate() + "," + data.getCADBusType() + ", " + data.getCADSeatType() + ", " + discountAmount + ", " + data.getPaxAge()
                        + "," + data.getIdProofId() + "," + data.getIdProofNumber();
            }

            seatDetailsParameters = TextUtils.isEmpty(seatDetailsParameters) ? seatParams : seatDetailsParameters + "*" + seatParams;
        }

        if (bookingTypeId == APP_CONSTANTS.AGENT_BOOKING || bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING || bookingTypeId == APP_CONSTANTS.AGENT_PHONE_BOOKING || bookingTypeId == APP_CONSTANTS.API_PHONE_BOOKING) {
            AM_AgentID = itsGetAllAgentByCompanyIDS.get(binding.spnAgentName.getSelectedItemPosition()).getAM_AgentID();
            ABI_TicketNo = binding.etAgentTicketNo.getText().toString().trim();
            ABI_NeteRate = binding.spnAgentRate.getSelectedItemPosition() == 0 ? 0 : 1;
            ABI_TotalSeat = totalPax;
            ABI_AgentCityID = itsGetAllAgentCityByCompanyIDS.get(binding.spnAgentCity.getSelectedItemPosition()).getCM_CityId();
        } else if (bookingTypeId == APP_CONSTANTS.BRANCH_BOOKING) {
            BBI_TicketNo = binding.etBranchTicketNo.getText().toString().trim();
            BBI_BM_BranchID = itsBranchMasterSelectXMLModelList.get(binding.spnBranch.getSelectedItemPosition()).getBM_BranchID();
            BBI_UM_UserID = itsGetBranchUserByCompanyIDModelList.get(binding.spnBranchUser.getSelectedItemPosition()).getBUM_BranchUserID();
        } else if (bookingTypeId == APP_CONSTANTS.GUEST_BOOKING) {
            GTM_GuestTypeID = itsGetGuestTypeByCompanyIDModelList.get(binding.spnGuest.getSelectedItemPosition()).getGTM_GuestTypeID();
        } else if (bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING) {
            PH_TransStatus = binding.spnBankcard.getSelectedItemPosition();
            PH_CardHolderName = TextUtils.isEmpty(binding.etBankNotes.getText().toString().trim()) ? "" : binding.etBankNotes.getText().toString().trim();
        } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
            PH_CardID = cmpCardNo;
        }


        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {

            if (bookingTypeId != 18) {
                clearITSPLWalletParameters();
            }

            modifyTotalBaseFare = Double.parseDouble(binding.etTotalAmount.getText().toString());
            JM_OriginalTotalPayable = modifyTotalBaseFare;

            if (modifyTotalRoundUp > 0) {
                modifyTotalGST = modifyTotalGST - modifyTotalRoundUp;
            }

            final BookingUpdate_Request request = new BookingUpdate_Request(
                    str_journeyDate,
                    jStartTime,
                    jCityTime,
                    loginCmpId,
                    "",
                    "",
                    sourceCityId,
                    destCityId,
                    routeId,
                    routeTimeId,
                    0,
                    "",
                    "",
                    pickUpId,
                    dropId,
                    bookingTypeId,
                    branchId,
                    branchUserId,
                    1,
                    0,
                    0,
                    modifyTotalBaseFare,
                    modifyTotalPax,
                    arrangementId,
                    modifyAcSeatRate,
                    modifyAcSlpRate,
                    modifyAcSlmbRate,
                    modifyNonAcSeatRate,
                    modifyNonAcSlpRate,
                    modifyNonAcSlmbRate,
                    modifyAcSeatTotal,
                    modifyAcSlpTotal,
                    modifyAcSlmbTotal,
                    modifyNonAcSeatTotal,
                    modifyNonAcSlpTotal,
                    modifyNonAcSlmbTotal,
                    paxName,
                    paxPhone1,
                    paxPhone2,
                    paxEmail,
                    paxRemarks,
                    PH_CardHolderName,
                    PH_CardID,
                    PH_CardProvidorID,
                    PH_TransStatus,
                    PH_DateTime,
                    PH_IPAddress,
                    PH_Amount,
                    GTM_GuestTypeID,
                    AM_AgentID,
                    ABI_TicketNo,
                    ABI_NeteRate,
                    ABI_TotalSeat,
                    ABI_AgentCityID,
                    BBI_BM_BranchID,
                    BBI_UM_UserID,
                    BBI_TicketNo,
                    bookedByCmpId,
                    seatDetailsParameters,
                    AllowMultipleBooking,
                    ModiFyPNRNo,
                    JM_OriginalTotalPayable,
                    JM_PCRegistrationID,
                    modifyTotalGST,
                    String.format(Locale.getDefault(), "%.2f", modifyTotalRoundUp),
                    JM_B2C_OrderNo,
                    JM_TDRCharge,
                    JM_IsMagicBox,
                    Modify_RemarkID,
                    JM_GSTState,
                    JM_GSTCompanyName,
                    JM_GSTRegNo,
                    JM_ITSAdminID,
                    JM_BranchModifyCharges,
                    Integer.parseInt(IsSameDay),
                    PM_IDProofString
            );

            final String finalSeatNameList = seatNameList;
            final String finalPaxPhone = paxPhone1;
            Call<BookingUpdate_Response> call = apiService.BookingUpdate(request);
            call.enqueue(new Callback<BookingUpdate_Response>() {
                @Override
                public void onResponse(@NotNull Call<BookingUpdate_Response> call, @NotNull Response<BookingUpdate_Response> response) {
                    disMissDialog();
                    if (response.isSuccessful() && response.body() != null && response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            if (response.body().getStatus() == 1 && response.body().getData().get(0).getBookingStatus() == 1) {
                                saveLastTransactionDetails(finalSeatNameList, finalPaxPhone, response.body().getData().get(0).getStatus(), "MODIFY");
                                showSnackBar(success, "Seat successfully updated", Snackbar.LENGTH_LONG, 2);
                                resetSeatArrangement();
                            } else {
                                saveLastTransactionDetails(finalSeatNameList, finalPaxPhone, " NOT MODIFY", "MODIFY");
                                showInternetConnectionDialog(0, "BookingUpdate", oops, response.body().getData().get(0).getStatus());
                            }

                        } else {
                            saveLastTransactionDetails(finalSeatNameList, finalPaxPhone, " NOT MODIFY", "MODIFY");
                            showInternetConnectionDialog(0, "BookingUpdate", oops, response.body().getMessage());
                        }
                    } else {
                        showInternetConnectionDialog(0, "BookingUpdate", oops, getResources().getString(R.string.bad_server_response));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<BookingUpdate_Response> call, @NotNull Throwable t) {
                    disMissDialog();
                    showInternetConnectionDialog(0, "BookingUpdate", oops, getResources().getString(R.string.bad_server_response));
                }
            });

        } else {

            if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                JM_B2C_OrderNo = reqWalletOrderNo;
            } else {
                clearITSPLWalletParameters();
            }

            InsertBooking_Request request = new InsertBooking_Request(
                    str_journeyDate,
                    jStartTime,
                    jCityTime,
                    loginCmpId,
                    "",
                    "",
                    sourceCityId,
                    destCityId,
                    routeId,
                    routeTimeId,
                    0,
                    "",
                    "",
                    pickUpId,
                    dropId,
                    bookingTypeId,
                    branchId,
                    branchUserId,
                    1,
                    0,
                    0,
                    totalBaseFare,
                    totalPax,
                    arrangementId,
                    newAcSeatRate,
                    newAcSlpRate,
                    newAcSlmbRate,
                    newNonAcSeatRate,
                    newNonAcSlpRate,
                    newNonAcSlmbRate,
                    acSeatTotal,
                    acSlpTotal,
                    acSlmbTotal,
                    nonAcSeatTotal,
                    nonAcSlpTotal,
                    nonAcSlmbTotal,
                    paxName,
                    paxPhone1,
                    paxPhone2,
                    paxEmail,
                    paxRemarks,
                    getPref.getBUM_BranchUserID(),
                    PH_CardHolderName,
                    PH_CardID,
                    PH_CardProvidorID,
                    PH_TransStatus,
                    PH_DateTime,
                    PH_IPAddress,
                    PH_Amount,
                    GTM_GuestTypeID,
                    AM_AgentID,
                    ABI_TicketNo,
                    ABI_NeteRate,
                    ABI_TotalSeat,
                    ABI_AgentCityID,
                    BBI_BM_BranchID,
                    BBI_UM_UserID,
                    BBI_TicketNo,
                    bookedByCmpId,
                    seatDetailsParameters,
                    AllowMultipleBooking,
                    JM_OriginalTotalPayable,
                    JM_PCRegistrationID,
                    totalGST,
                    String.format(Locale.getDefault(), "%.2f", totalRoundUp),
                    JM_B2C_OrderNo,
                    JM_TDRCharge,
                    JM_IsMagicBox,
                    Modify_RemarkID,
                    JM_GSTState,
                    JM_GSTCompanyName,
                    JM_GSTRegNo,
                    JM_ITSAdminID,
                    "",
                    discountAmount,
                    Integer.parseInt(IsSameDay),
                    PM_IDProofString,
                    reqPhoneBookingHours,
                    reqPhoneBookingMins,
                    reqWalletKey,
                    reqWalletType,
                    reqWalletOTP,
                    reqWalletState
            );

            // Log.e("request", new Gson().toJson(request));

            Call<InsertBooking_Response> call = apiService.InsertBooking(request);
            final String finalSeatNameList = seatNameList;
            final String finalPaxPhone = paxPhone1;
            call.enqueue(new Callback<InsertBooking_Response>() {
                @Override
                public void onResponse(@NotNull Call<InsertBooking_Response> call, @NotNull Response<InsertBooking_Response> response) {
                    disMissDialog();
                    if (response.isSuccessful() && response.body() != null && response.body().getStatus() == 1) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            int bookingStatus = 0;
                            String bookingStatusMessage;
                            if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                                bookingStatusMessage = response.body().getData().get(0).getMessage();
                                if (!TextUtils.isEmpty(response.body().getData().get(0).getStatus()) && response.body().getData().get(0).getStatus().equals("1")) {
                                    bookingStatus = 1;
                                }
                            } else {
                                bookingStatusMessage = response.body().getData().get(0).getstatus();
                                if (response.body().getData().get(0).getBookingStatus() == 1) {
                                    bookingStatus = 1;
                                }
                            }

                            if (bookingStatus == 1) {
                                saveLastTransactionDetails(finalSeatNameList, finalPaxPhone, response.body().getData().get(0).getStatus(), "INSERT");
                                resetSeatArrangement();
                                showSnackBar(success, "Seat successfully booked", Snackbar.LENGTH_LONG, 2);
                            } else {
                                saveLastTransactionDetails(finalSeatNameList, finalPaxPhone, "NOT BOOKED", "INSERT");
                                showInternetConnectionDialog(0, "InsertBooking", oops, bookingStatusMessage);
                            }

                        } else {
                            saveLastTransactionDetails(finalSeatNameList, finalPaxPhone, "NOT BOOKED", "INSERT");
                            showInternetConnectionDialog(0, "InsertBooking", oops, response.body().getMessage());
                        }
                    } else {
                        showInternetConnectionDialog(0, "InsertBooking", oops, getResources().getString(R.string.bad_server_response));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<InsertBooking_Response> call, @NotNull Throwable t) {
                    disMissDialog();
                    showInternetConnectionDialog(0, "InsertBooking", oops, getResources().getString(R.string.bad_server_response));
                }
            });

        }


    }

    private String getIpAddress() {
        try {
            for (@SuppressWarnings("rawtypes")
                 Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (@SuppressWarnings("rawtypes")
                     Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ignored) {
        }
        return "";
    }

    private void openGSTDialog() {
        try {
            final Dialog dialogGST = new Dialog(ExeBookingActivity_V2.this);
            dialogGST.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogGST.setCancelable(false);

            final LayoutDialogGstBinding dialogBinding = LayoutDialogGstBinding.inflate(getLayoutInflater());
            View view = dialogBinding.getRoot();
            dialogGST.setContentView(view);

            dialogBinding.spnState.setAdapter(new GSTStateAdapter(ExeBookingActivity_V2.this, itsStateList));


            dialogBinding.btnSubmit.setOnClickListener(view1 -> {
                if (TextUtils.isEmpty(dialogBinding.etGstno.getText().toString()) || !isValidGSTNo(dialogBinding.etGstno.getText().toString())) {
                    dialogBinding.etGstno.setError("Enter Valid GST No");
                } else if (TextUtils.isEmpty(dialogBinding.etGstregName.getText().toString())) {
                    dialogBinding.etGstno.setError("Enter Valid Name");
                } else {
                    hideKeyBoard();
                    dialogGST.dismiss();
                    JM_GSTState = itsStateList.get(dialogBinding.spnState.getSelectedItemPosition()).getSMStateID();
                    JM_GSTRegNo = dialogBinding.etGstno.getText().toString().trim();
                    JM_GSTCompanyName = dialogBinding.etGstregName.getText().toString().trim();

                    if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING && CDH_AutoDeletePhoneTime == 1) {
                        handleMessage(BOOK_TICKET_FLOW.PHONE_BOOKING_TIME_POPUP);
                    } else {
                        handleMessage(BOOK_TICKET_FLOW.BOOK_TICKET_API_CALL);
                    }
                }
            });

            dialogBinding.btnCancel.setOnClickListener(view12 -> dialogGST.dismiss());

            if (!isFinishing()) {
                dialogGST.show();
            }

        } catch (Exception ignored) {
        }

    }

    private boolean isValidGSTNo(String str) {
        String regex = "^[0-9]{2}[A-Z]{5}[0-9]{4}"
                + "[A-Z]{1}[1-9A-Z]{1}"
                + "Z[0-9A-Z]{1}$";
        Pattern p = Pattern.compile(regex);
        if (str == null) {
            return false;
        }
        Matcher m = p.matcher(str);
        return m.matches();
    }

    private void showPhoneBookingCancelTimePopUp() {
        // phoneBookingMins -> Before Pickup && phoneBookingMinutes -> After Pickup
        int phoneBookingMins = 0, phoneBookingMinutes = 0, IsAfaterPickupTimeSet = 0;

        if (RPCM_Hours > 0 || RPCM_Min > 0) {
            phoneBookingMins = RPCM_Hours > 0 ? RPCM_Hours * 60 : 0;
            phoneBookingMins += RPCM_Min;
        } else if (BM_AutoDeletePhoneHour > 0 || BM_AutoDeletePhoneMin > 0) {
            phoneBookingMins = BM_AutoDeletePhoneHour > 0 ? BM_AutoDeletePhoneHour * 60 : 0;
            phoneBookingMins += BM_AutoDeletePhoneMin;
        } else if (CDH_AutoDeleteHour > 0 || CDH_AutoDeleteMinute > 0) {
            phoneBookingMins = CDH_AutoDeleteHour > 0 ? CDH_AutoDeleteHour * 60 : 0;
            phoneBookingMins += CDH_AutoDeleteMinute;
        } else {
            phoneBookingMinutes = CDH_AutoDeletePhoneBookMinutes;
        }


        if (phoneBookingMins > 0 || phoneBookingMinutes > 0) {

            final String sel_JourneyDate = binding.spnDate.getSelectedItem().toString();
            final String SelectedPickUpTime = listPickUp.get(binding.spnPickup.getSelectedItemPosition()).getPSIDepartTime();

            try {
                final Dialog dialogSetPhoneBookingTime = new Dialog(ExeBookingActivity_V2.this);
                dialogSetPhoneBookingTime.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogSetPhoneBookingTime.setCancelable(false);

                final DialogPhoneBookingCancelTimeBinding dialogBinding = DialogPhoneBookingCancelTimeBinding.inflate(getLayoutInflater());
                View view = dialogBinding.getRoot();
                dialogSetPhoneBookingTime.setContentView(view);

                dialogBinding.imgHourPlus.setOnClickListener(v -> {
                    if (!TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                        if (Integer.parseInt(dialogBinding.edtHour.getText().toString().trim()) == 0) {
                            dialogBinding.edtHour.setText("1");
                        } else {
                            dialogBinding.edtHour.setText((Integer.parseInt(dialogBinding.edtHour.getText().toString().trim()) + 1) + "");
                        }
                    } else {
                        dialogBinding.edtHour.setText("1");
                    }
                });

                dialogBinding.imgHourMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                            if (Integer.parseInt(dialogBinding.edtHour.getText().toString().trim()) != 0) {
                                dialogBinding.edtHour.setText((Integer.parseInt(dialogBinding.edtHour.getText().toString().trim()) - 1) + "");
                            }
                        } else {
                            dialogBinding.edtHour.setText("0");
                        }
                    }
                });

                dialogBinding.imgMinutePlus.setOnClickListener(v -> {
                    if (!TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                        if (Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim()) == 0) {
                            dialogBinding.edtMinute.setText("1");
                        } else {
                            dialogBinding.edtMinute.setText((Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim()) + 1) + "");
                        }
                    } else {
                        dialogBinding.edtMinute.setText("1");
                    }
                });

                dialogBinding.imgMinuteMinus.setOnClickListener(v -> {
                    if (!TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                        if (Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim()) == 0) {
                            //dialogBinding.edtHour.setText("1");
                        } else {
                            dialogBinding.edtMinute.setText((Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim()) - 1) + "");
                        }
                    } else {
                        dialogBinding.edtMinute.setText("0");
                    }
                });

                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
                final SimpleDateFormat simpleDateFormatDisplay = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.getDefault());
                calendarForPhoneBookingTime = Calendar.getInstance();

                Date date = simpleDateFormat.parse(sel_JourneyDate + " " + SelectedPickUpTime);
                assert date != null;
                calendarForPhoneBookingTime.setTime(date);

                if (phoneBookingMins > 0) {
                    IsAfaterPickupTimeSet = 0;
                    calendarForPhoneBookingTime.add(Calendar.HOUR_OF_DAY, -(phoneBookingMins / 60));
                    calendarForPhoneBookingTime.add(Calendar.MINUTE, -(phoneBookingMins % 60));

                    dialogBinding.edtHour.setText(phoneBookingMins / 60 + "");
                    dialogBinding.edtMinute.setText(phoneBookingMins % 60 + "");

                } else if (phoneBookingMinutes > 0) {
                    IsAfaterPickupTimeSet = 1;
                    calendarForPhoneBookingTime.add(Calendar.HOUR_OF_DAY, (phoneBookingMinutes / 60));
                    calendarForPhoneBookingTime.add(Calendar.MINUTE, (phoneBookingMinutes % 60));

                    dialogBinding.edtHour.setText(phoneBookingMinutes / 60 + "");
                    dialogBinding.edtMinute.setText(phoneBookingMinutes % 60 + "");
                }

                dialogBinding.txtCancelDateTime.setText(simpleDateFormatDisplay.format(calendarForPhoneBookingTime.getTime()));

                final int finalPhoneBookingMins1 = phoneBookingMins;
                final int finalPhoneBookingMinutes1 = phoneBookingMinutes;

                dialogBinding.edtMinute.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        int Hour = 0;
                        int Minute = 0;
                        dialogBinding.txtError.setVisibility(View.GONE);

                        if (!TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                            Hour = Integer.parseInt(dialogBinding.edtHour.getText().toString().trim());
                        }

                        if (!TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                            Minute = Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim());
                            if (Minute > 59) {
                                dialogBinding.edtMinute.setText("0");
                            }
                        }


                        Date date;
                        if (finalPhoneBookingMins1 > 0) {
                            try {
                                date = simpleDateFormat.parse(sel_JourneyDate + " " + SelectedPickUpTime);
                                assert date != null;
                                calendarForPhoneBookingTime.setTime(date);
                                calendarForPhoneBookingTime.add(Calendar.MINUTE, -Minute);
                                calendarForPhoneBookingTime.add(Calendar.HOUR_OF_DAY, -Hour);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else if (finalPhoneBookingMinutes1 > 0) {
                            try {
                                date = simpleDateFormat.parse(sel_JourneyDate + " " + SelectedPickUpTime);
                                assert date != null;
                                calendarForPhoneBookingTime.setTime(date);
                                calendarForPhoneBookingTime.add(Calendar.MINUTE, Minute);
                                calendarForPhoneBookingTime.add(Calendar.HOUR_OF_DAY, Hour);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        dialogBinding.txtCancelDateTime.setText(simpleDateFormatDisplay.format(calendarForPhoneBookingTime.getTime()));

                    }
                });


                dialogBinding.edtHour.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        int Hour = 0;
                        int Minute = 0;
                        dialogBinding.txtError.setVisibility(View.GONE);
                        if (!TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                            Hour = Integer.parseInt(dialogBinding.edtHour.getText().toString().trim());
                        }
                        if (!TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                            Minute = Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim());
                        }

                        Date date = null;

                        if (finalPhoneBookingMins1 > 0) {
                            try {
                                date = simpleDateFormat.parse(sel_JourneyDate + " " + SelectedPickUpTime);
                                assert date != null;
                                calendarForPhoneBookingTime.setTime(date);
                                calendarForPhoneBookingTime.add(Calendar.HOUR_OF_DAY, -Hour);
                                calendarForPhoneBookingTime.add(Calendar.MINUTE, -Minute);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        } else if (finalPhoneBookingMinutes1 > 0) {
                            try {
                                date = simpleDateFormat.parse(sel_JourneyDate + " " + SelectedPickUpTime);
                                calendarForPhoneBookingTime.setTime(date);
                                calendarForPhoneBookingTime.add(Calendar.HOUR_OF_DAY, Hour);
                                calendarForPhoneBookingTime.add(Calendar.MINUTE, Minute);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        dialogBinding.txtCancelDateTime.setText(simpleDateFormatDisplay.format(calendarForPhoneBookingTime.getTime()));
                    }
                });

                final int finalIsAfaterPickupTimeSet = IsAfaterPickupTimeSet;
                final int finalPhoneBookingMinutes = phoneBookingMinutes;
                final int finalPhoneBookingMins = phoneBookingMins;
                dialogBinding.btnSetTime.setOnClickListener(v -> {

                    int TotalMinute = 0;
                    if (!TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                        TotalMinute = Integer.parseInt(dialogBinding.edtHour.getText().toString().trim()) * 60;
                    }
                    if (!TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                        TotalMinute = TotalMinute + Integer.parseInt(dialogBinding.edtMinute.getText().toString().trim());
                    }
                    if (finalIsAfaterPickupTimeSet == 1) {
                        if (TotalMinute > finalPhoneBookingMinutes) {
                            dialogBinding.txtError.setVisibility(View.VISIBLE);
                            dialogBinding.txtError.setText("Sorry, You can not set more minutes set by company \n Maximum Time Required " + finalPhoneBookingMinutes + " Minutes");
                        } else {
                            dialogSetPhoneBookingTime.dismiss();
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


                            if (TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                                reqPhoneBookingMins = "0";
                            } else {
                                reqPhoneBookingMins = dialogBinding.edtMinute.getText().toString().trim();
                            }

                            if (TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                                reqPhoneBookingHours = "0";
                            } else {
                                reqPhoneBookingHours = dialogBinding.edtHour.getText().toString().trim();
                            }

                            handleMessage(BOOK_TICKET_FLOW.BOOK_TICKET_API_CALL);
                        }
                    } else {
                        if (TotalMinute < finalPhoneBookingMins && GA_AllowToSetLessPhoneCancelTime == 0) {
                            dialogBinding.txtError.setVisibility(View.VISIBLE);
                            dialogBinding.txtError.setText("Sorry, You can not set less minutes set by company \n Minimum Time Required " + finalPhoneBookingMins + " Minutes");
                        } else {
                            dialogSetPhoneBookingTime.dismiss();
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


                            if (TextUtils.isEmpty(dialogBinding.edtMinute.getText().toString().trim())) {
                                reqPhoneBookingMins = "0";
                            } else {
                                reqPhoneBookingMins = dialogBinding.edtMinute.getText().toString().trim();
                            }

                            if (TextUtils.isEmpty(dialogBinding.edtHour.getText().toString().trim())) {
                                reqPhoneBookingHours = "0";
                            } else {
                                reqPhoneBookingHours = dialogBinding.edtHour.getText().toString().trim();
                            }

                            handleMessage(BOOK_TICKET_FLOW.BOOK_TICKET_API_CALL);

                        }
                    }


                });

                dialogBinding.btnCancel.setOnClickListener(v -> dialogSetPhoneBookingTime.dismiss());


                dialogBinding.txtCancelDateTime.setAnimation(AnimationUtils.loadAnimation(ExeBookingActivity_V2.this, R.anim.flash_leave_now));

                if (dialogSetPhoneBookingTime.getWindow() != null) {
                    dialogSetPhoneBookingTime.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    dialogSetPhoneBookingTime.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }

                if (!isFinishing()) {
                    dialogSetPhoneBookingTime.show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            showSnackBar(oops, "Phone Booking Time Isn't Set !!!", Toast.LENGTH_LONG, 1);
        }


    }

    private void clearITSPLWalletParameters() {

        // ITSPL Wallet
        reqWalletOrderNo = "";
        reqWalletAskOTP = 0;
        reqWalletType = 0;
        reqWalletOTP = "";
        reqWalletKey = "";
        reqWalletState = "";
        reqWalletTDRCharge = 0.0;
        JM_B2C_OrderNo = "";
    }

    private void clearAfterInsertBooking() {

        cmpCardNo = "";
        cmpCardBalance = 0;
        cmpCardMaxDiscount = 0;
        cmpCardOTP = "";
        cmpCardId = "";

        if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
            if (animCompanyCardDetails != null) {
                animCompanyCardDetails.cancel();
                binding.txtCmpCardDetails.clearAnimation();
                animCompanyCardDetails = null;
            }
        }

        binding.chkAutoInsertQuota.setChecked(false);

        linkedHashMapBookedSeat.clear();
        TRANSACTION_TYPE = APP_CONSTANTS.INSERT;
        selSeats.clear();
        seatSelectedViewModel.addSeats(selSeats);
        modifyTotalBaseFare = 0;
        modifyTotalGST = 0;

        JM_GSTState = 0;
        JM_GSTRegNo = "";
        JM_GSTCompanyName = "";
        binding.chkGST.setChecked(false);
        binding.chkMultipleBooking.setChecked(false);

        AM_AgentID = 0;
        ABI_TicketNo = "";
        ABI_NeteRate = 0;
        ABI_TotalSeat = 0;
        ABI_AgentCityID = 0;

        BBI_BM_BranchID = 0;
        BBI_UM_UserID = 0;
        BBI_TicketNo = "";

        JM_B2C_OrderNo = "";
        reqWalletState = "";
        reqWalletKey = "";
        reqWalletOTP = "";
        reqWalletType = 0;


        binding.etSearchPnr.setText("");
        binding.txtCouponCode.setText("");
        binding.etAgentTicketNo.setText("");
        binding.etBranchTicketNo.setText("");
        binding.etBankNotes.setText("");
        binding.etCmpBkgCardno.setText("");
        binding.etCmpCardDisc.setText("");
        binding.etCmpCardOtp.setText("");
        binding.etNoOfSeat.setText("0");

        binding.etAcSeat.setText("0");
        binding.etAcSleeper.setText("0");
        binding.etAcSlumber.setText("0");
        binding.etNonacSeat.setText("0");
        binding.etNonacSleeper.setText("0");
        binding.etNonacSlumber.setText("0");


        binding.etTotAcSeat.setText("0");
        binding.etTotAcSleeper.setText("0");
        binding.etTotAcSlumber.setText("0");
        binding.etTotNonacSeat.setText("0");
        binding.etTotNonacSleeper.setText("0");
        binding.etTotNonacSlumber.setText("0");


        binding.etTotGst.setText("0");
        binding.etTotalAmount.setText("0");


        binding.etPaxPhone1.setText("");
        binding.etPaxPhone2.setText("");

        binding.etPaxName.setText("");
        binding.etIdNo.setText("");

        binding.etRemarks.setText("");
        binding.etEmail.setText("");


    }

    private void pickUpDropRequest() {
        addApiList(APP_CONSTANTS.PickupDrop, API_RESPONSE_STATUS.RUNNING);
        listPickUp = new ArrayList<>();
        listDropUp = new ArrayList<>();

        binding.spnPickup.setAdapter(null);
        binding.spnDrop.setAdapter(null);

        PickupDrop_Request request = new PickupDrop_Request(
                loginCmpId,
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                str_journeyDate,
                bookedByCmpId,
                IsSameDay
        );

        pickupDropViewModel.createPickupDropRequest(request);

    }

    private void setPickupDropDefault() {
        if (listPickUp.size() <= 0) {
            PickupDrop_Response.Datum dataPickUp = new PickupDrop_Response.Datum();
            dataPickUp.setPickUPTime("Not Defined...");
            dataPickUp.setPSMPickupID(0);
            dataPickUp.setPSIDepartTime("0");
            listPickUp.add(dataPickUp);
        }
        if (listDropUp.size() <= 0) {
            PickupDrop_Response.Datum dataPickUp = new PickupDrop_Response.Datum();
            dataPickUp.setPickUPTime("Not Defined...");
            dataPickUp.setPSMPickupID(0);
            dataPickUp.setPSIDepartTime("0");
            listDropUp.add(dataPickUp);
        }

        binding.spnPickup.setAdapter(new PickUpAdapter(ExeBookingActivity_V2.this, listPickUp));
        binding.spnDrop.setAdapter(new PickUpAdapter(ExeBookingActivity_V2.this, listDropUp));
    }

    private void cancelDetailsApiCall() {
        listCancelDetails = new ArrayList<>();

        String JM_SeatList = "", JM_Cancel_Remarks = "";
        double JM_CancleAmount = 0.0, JM_RefundCharges = 0.0, GST = 0.0;
        int JM_PNRNO = 0, JM_BookedBy_CM_CompanyID = 0, BranchId = 0, BranchUserId = 0, BTM_BookingTypeID = 0, JM_CancelBy = 0,
                JM_CancelBy_BranchID = 0, CM_CompanyID = 0, JM_IsBlackList = 0, JM_PCRegistrationID = 0, AgentRefundType = 0, IsWalletCancellation = 0,
                Cancel_RemarkID = 0, JM_CancelFromUserID = 0;
        for (FetchDetailsByPNRNO_Response.Datum data : listFetchDetailsPNR) {
            JM_PNRNO = data.getJMPNRNO();
            GST = data.getServiceTaxAmt();
            JM_BookedBy_CM_CompanyID = data.getJMBookedByCMCompanyID();
            BranchId = data.getBMBranchID();
            BranchUserId = getPref.getBUM_BranchUserID();
            JM_CancleAmount = data.getJMPayableAmount();
            JM_SeatList = TextUtils.isEmpty(JM_SeatList) ? data.getBADSeatNo() : JM_SeatList + "," + data.getBADSeatNo();
            BTM_BookingTypeID = data.getBTMBookingTypeID();
        }


        GetCancellationDetails_ConfirmCancellation_Request request = new GetCancellationDetails_ConfirmCancellation_Request(
                JM_PNRNO,
                JM_BookedBy_CM_CompanyID,
                BranchId,
                BranchUserId,
                String.format(Locale.getDefault(), "%.2f", JM_CancleAmount),
                JM_SeatList,
                BTM_BookingTypeID,
                JM_CancelBy,
                String.format(Locale.getDefault(), "%.2f", JM_CancleAmount),
                JM_CancelBy_BranchID,
                CM_CompanyID,
                JM_RefundCharges,
                JM_Cancel_Remarks,
                JM_IsBlackList,
                JM_PCRegistrationID,
                AgentRefundType,
                IsWalletCancellation,
                "",
                0,
                0,
                0,
                "",
                "",
                "",
                "",
                ""
        );


        Call<GetCancellationDetails_ConfirmCancellation_Response> call = apiService.GetCancellationDetails_ConfirmCancellation(request);
        final int finalJM_PNRNO = JM_PNRNO;
        final String finalJM_SeatList = JM_SeatList;
        final double finalJM_CancleAmount = JM_CancleAmount;
        final double finalGST = GST;
        int finalBTM_BookingTypeID = BTM_BookingTypeID;
        call.enqueue(new Callback<GetCancellationDetails_ConfirmCancellation_Response>() {
            @Override
            public void onResponse(@NotNull Call<GetCancellationDetails_ConfirmCancellation_Response> call, @NotNull Response<GetCancellationDetails_ConfirmCancellation_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == 1) {
                    if (response.body().getData() != null && response.body().getData().getGetCancellationDetail() != null && response.body().getData().getGetCancellationDetail().size() > 0) {
                        if (response.body().getData().getGetCancellationDetail().get(0).getStatus() == 1) {
                            listCancelDetails = response.body().getData().getGetCancellationDetail();
                            showCancelDetailsDialog(finalJM_PNRNO, finalJM_SeatList, finalJM_CancleAmount, finalGST, finalBTM_BookingTypeID);
                        } else {
                            showInternetConnectionDialog(0, "GetCancellationDetails_ConfirmCancellation", oops, response.body().getData().getGetCancellationDetail().get(0).getStatusMsg());
                        }

                    } else {
                        showInternetConnectionDialog(0, "GetCancellationDetails_ConfirmCancellation", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(0, "GetCancellationDetails_ConfirmCancellation", oops, getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<GetCancellationDetails_ConfirmCancellation_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0, "GetCancellationDetails_ConfirmCancellation", oops, getResources().getString(R.string.bad_server_response));
            }
        });

    }

    private void showCancelDetailsDialog(int JM_PNRNO, final String JM_SeatList,
                                         final double JM_CancleAmount, double GST, int bookingTypeId) {
        int blackListStatus = 0;
        final Dialog dialogCancelDetails = new Dialog(ExeBookingActivity_V2.this);

        dialogCancellationDetailsBinding = DialogCancellationDetailsBinding.inflate(getLayoutInflater());
        final View view = dialogCancellationDetailsBinding.getRoot();

        final GetCancellationDetails_ConfirmCancellation_Response.GetCancellationDetail data = listCancelDetails.get(0);
        dialogCancellationDetailsBinding.txtPnr.setText(dialogCancellationDetailsBinding.txtPnr.getText() + String.valueOf(JM_PNRNO));
        dialogCancellationDetailsBinding.txtSeatBooked.setText(JM_SeatList);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getResources().getString(R.string.rs));
        stringBuilder.append(String.format(Locale.getDefault(), "%.2f", JM_CancleAmount - GST));
        stringBuilder.append(" + ");
        stringBuilder.append(getResources().getString(R.string.rs));
        stringBuilder.append(String.format(Locale.getDefault(), "%.2f", GST));
        stringBuilder.append(" = ");
        stringBuilder.append(String.format(Locale.getDefault(), "%.2f", JM_CancleAmount));

        dialogCancellationDetailsBinding.txtTotalAmount.setText(stringBuilder.toString());

        StringBuilder strRefundCharges = new StringBuilder();
        strRefundCharges.append(getResources().getString(R.string.rs));
        strRefundCharges.append(String.format(Locale.getDefault(), "%.2f", data.getJMRefundCharges()) + " [" + data.getDeductPer() + "%] , ");
        strRefundCharges.append("GST ");
        strRefundCharges.append(getResources().getString(R.string.rs));
        strRefundCharges.append("0.0 ");
        strRefundCharges.append(",Refund Amount ");
        strRefundCharges.append(getResources().getString(R.string.rs));
        strRefundCharges.append(String.format(Locale.getDefault(), "%.2f", JM_CancleAmount - data.getJMRefundCharges()));

        dialogCancellationDetailsBinding.txtRefundCahrges.setText(strRefundCharges.toString());


        if (itsCancelremarkList != null && itsCancelremarkList.size() > 0) {
            dialogCancellationDetailsBinding.spnRemarksReason.setAdapter(new CancelRemarksAdapter(ExeBookingActivity_V2.this, itsCancelremarkList));
        }

        if (GA_AllowChangeRefundCharges == 1) {
            dialogCancellationDetailsBinding.etRefundCharges.setEnabled(true);
            dialogCancellationDetailsBinding.etRefundCharges.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String str = editable.toString();
                    if (!machine_changed_edittext) {
                        machine_changed_edittext = true;
                        double refundChargesFare = str.trim().length() <= 0 ? 0 : Double.parseDouble(str);
                        dialogCancellationDetailsBinding.etRefundAmount.setText(String.format(Locale.getDefault(), "%.2f", JM_CancleAmount - refundChargesFare));
                        if (Config.roundTwoDecimals(refundChargesFare) > Config.roundTwoDecimals(JM_CancleAmount)) {
                            dialogCancellationDetailsBinding.etRefundAmount.setText(String.format(Locale.getDefault(), "%.2f", JM_CancleAmount - data.getJMRefundCharges()));
                        }
                        machine_changed_edittext = false;
                    }
                }
            });
        } else {
            dialogCancellationDetailsBinding.etRefundCharges.setEnabled(false);
        }

        dialogCancellationDetailsBinding.etRefundCharges.setText(String.format(Locale.getDefault(), "%.2f", data.getJMRefundCharges()));
        dialogCancellationDetailsBinding.etRefundAmount.setText(String.format(Locale.getDefault(), "%.2f", JM_CancleAmount - data.getJMRefundCharges()));


        dialogCancellationDetailsBinding.btnCancel.setOnClickListener(view1 -> dialogCancelDetails.dismiss());

        if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
            blackListStatus = APP_CONSTANTS.RIGHTS_ALLOW;
        } else {
            if ((bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING || bookingTypeId == APP_CONSTANTS.AGENT_BOOKING ||
                    bookingTypeId == APP_CONSTANTS.BRANCH_BOOKING || bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING ||
                    bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING || bookingTypeId == APP_CONSTANTS.AGENT_CC_CARD_BOOKING ||
                    bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING || bookingTypeId == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) && GA_AllowChangeRefundCharges == APP_CONSTANTS.STATUS_SUCCESS) {
                if (!TextUtils.isEmpty(data.getCancellationCharge()) && Integer.parseInt(data.getCancellationCharge()) > 0) {
                    blackListStatus = bookingRightsByUserData.getGAAskCancelBranchUser();
                }
            } else {
                blackListStatus = bookingRightsByUserData.getGAAskCancelBranchUser();
                // As per exe by default allow
                blackListStatus = APP_CONSTANTS.RIGHTS_ALLOW;
            }
        }

        if (blackListStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            dialogCancellationDetailsBinding.chkIsBlackList.setVisibility(View.VISIBLE);
            dialogCancellationDetailsBinding.txtRemarksTitle.setText(getResources().getString(R.string.reason));
        }

        final int finalBlackListStatus = blackListStatus;
        dialogCancellationDetailsBinding.btnSubmit.setOnClickListener(view12 -> {
            String cancelRemarks = TextUtils.isEmpty(dialogCancellationDetailsBinding.etRemarks.getText().toString().trim()) ? "" : dialogCancellationDetailsBinding.etRemarks.getText().toString().trim();
            int cancelRemarksId = 0;
            if (itsCancelremarkList != null && itsCancelremarkList.size() > 0) {
                cancelRemarksId = itsCancelremarkList.get(dialogCancellationDetailsBinding.spnRemarksReason.getSelectedItemPosition()).getRMRemarkID();
            }
            dialogCancellationDetailsBinding.etRefundCharges.setError(null);
            boolean is_continue = true;
            double refundChargesFare = dialogCancellationDetailsBinding.etRefundCharges.getText().toString().trim().length() <= 0 ? 0.0 : Double.parseDouble(dialogCancellationDetailsBinding.etRefundCharges.getText().toString());
            if (Config.roundTwoDecimals(refundChargesFare) > Config.roundTwoDecimals(JM_CancleAmount)) {
                is_continue = false;
                dialogCancellationDetailsBinding.etRefundCharges.setText(String.format(Locale.getDefault(), "%.2f", data.getJMRefundCharges()));
                showToast("You can't set refund charge greater than " + String.format(Locale.getDefault(), "%.2f", JM_CancleAmount));
                dialogCancellationDetailsBinding.etRefundCharges.setSelection(dialogCancellationDetailsBinding.etRefundCharges.getText().toString().length());
            } else if (GA_AllowChangeRefundCharges == 1) {
                if (GA_AllowLessRefundCharges == 0) {
                    if (Config.roundTwoDecimals(data.getJMRefundCharges()) > Config.roundTwoDecimals(refundChargesFare)) {
                        is_continue = false;
                        dialogCancellationDetailsBinding.etRefundCharges.setText(String.format(Locale.getDefault(), "%.2f", data.getJMRefundCharges()));
                        dialogCancellationDetailsBinding.etRefundCharges.setError("You can't set refund charge less than " + String.format(Locale.getDefault(), "%.2f", data.getJMRefundCharges()));
                        showToast("You can't set refund charge less than " + String.format(Locale.getDefault(), "%.2f", data.getJMRefundCharges()));
                        dialogCancellationDetailsBinding.etRefundCharges.setSelection(dialogCancellationDetailsBinding.etRefundCharges.getText().toString().length());
                    }
                }
            }

            if (is_continue) {
                dialogCancelDetails.dismiss();
                if (listFetchDetailsPNR != null && listFetchDetailsPNR.size() > 0 && listFetchDetailsPNR.get(0).getBTMBookingTypeID() == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                    InsertAgentQuotaBookingCancleApiCall(0);
                } else {
                    int blackListStatusTemp = 0;
                    if (finalBlackListStatus == 1) {
                        blackListStatusTemp = dialogCancellationDetailsBinding.chkIsBlackList.isChecked() ? APP_CONSTANTS.RIGHTS_ALLOW : APP_CONSTANTS.RIGHTS_DENY;
                    }
                    confirmCancelApiCall(cancelRemarks, String.format(Locale.getDefault(), "%.2f", refundChargesFare), cancelRemarksId, blackListStatusTemp);
                }
            }
        });

        dialogCancelDetails.setContentView(view);
        dialogCancelDetails.setCancelable(false);
        if (!isFinishing())
            dialogCancelDetails.show();
        try {
            dialogCancelDetails.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        } catch (Exception ignored) {
        }




        /*FrameLayout bottomSheet = (FrameLayout) bottomBookTicketDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);*/
    }


    private void confirmCancelApiCall(String cancelRemarks, String refundCharges,
                                      int cancelRemarksId, int JM_IsBlackList) {
        String JM_SeatList = "", JM_Cancel_Remarks = "", JM_RefundCharges = "0.0";
        double JM_CancleAmount = 0.0;
        int JM_PNRNO = 0, JM_BookedBy_CM_CompanyID = 0, BranchId = 0, BranchUserId = 0, BTM_BookingTypeID = 0, JM_CancelBy = 0,
                JM_CancelBy_BranchID = 0, CM_CompanyID = 0, JM_PCRegistrationID = 0, AgentRefundType = 0, IsWalletCancellation = 0,
                Cancel_RemarkID = 0, JM_CancelFromUserID = 0;

        for (FetchDetailsByPNRNO_Response.Datum data : listFetchDetailsPNR) {
            JM_BookedBy_CM_CompanyID = data.getJMBookedByCMCompanyID();
            BranchId = data.getBMBranchID();
            BranchUserId = getPref.getBUM_BranchUserID();

            JM_PNRNO = data.getJMPNRNO();
            BTM_BookingTypeID = data.getBTMBookingTypeID();
            JM_CancelBy = data.getUMUserID();
            JM_CancleAmount = data.getJMPayableAmount();
            JM_CancelBy_BranchID = getPref.getBM_BranchID();
            CM_CompanyID = data.getCMCompanyID();
            JM_RefundCharges = refundCharges;
            JM_Cancel_Remarks = cancelRemarks;
            JM_SeatList = TextUtils.isEmpty(JM_SeatList) ? data.getBADSeatNo() : JM_SeatList + "," + data.getBADSeatNo();
            JM_ITSAdminID = getPref.getAdminUserId();
            JM_CancelFromUserID = getPref.getBUM_BranchUserID();
            Cancel_RemarkID = cancelRemarksId;
        }

        GetCancellationDetails_ConfirmCancellation_Request request = new GetCancellationDetails_ConfirmCancellation_Request(
                JM_PNRNO,
                JM_BookedBy_CM_CompanyID,
                BranchId,
                BranchUserId,
                String.format(Locale.getDefault(), "%.2f", JM_CancleAmount),
                JM_SeatList,
                BTM_BookingTypeID,
                JM_CancelBy,
                String.format(Locale.getDefault(), "%.2f", JM_CancleAmount),
                JM_CancelBy_BranchID,
                CM_CompanyID,
                Double.parseDouble(JM_RefundCharges),
                JM_Cancel_Remarks,
                JM_IsBlackList,
                JM_PCRegistrationID,
                AgentRefundType,
                IsWalletCancellation,
                JM_SeatList,
                JM_ITSAdminID,
                Cancel_RemarkID,
                JM_CancelFromUserID,
                "",
                "",
                "",
                "",
                ""
        );

        // Log.e("confirm cancel", new Gson().toJson(request));

        Call<GetCancellationDetails_ConfirmCancellation_Response> call = apiService.GetCancellationDetails_ConfirmCancellation(request);
        call.enqueue(new Callback<GetCancellationDetails_ConfirmCancellation_Response>() {
            @Override
            public void onResponse(@NotNull Call<GetCancellationDetails_ConfirmCancellation_Response> call, @NotNull Response<GetCancellationDetails_ConfirmCancellation_Response> response) {
                disMissDialog();

                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == 1) {
                    if (response.body().getData() != null && response.body().getData().getGetCancellationDetail() != null && response.body().getData().getGetCancellationDetail().size() > 0) {
                        if (response.body().getData().getFinalCancel().get(0).getCancelStatus() == 1) {
                            resetSeatArrangement();
                        } else {
                            showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "GetCancellationDetails_ConfirmCancellation", oops, response.body().getData().getGetCancellationDetail().get(0).getStatusMsg());
                        }
                    } else {
                        showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "GetCancellationDetails_ConfirmCancellation", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "GetCancellationDetails_ConfirmCancellation", oops, getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<GetCancellationDetails_ConfirmCancellation_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "GetCancellationDetails_ConfirmCancellation", oops, getResources().getString(R.string.bad_server_response));
            }
        });

    }

    private void showSeatDetailsDialog() {
        // final Dialog dialogSeatDetails = new Dialog(ExeBookingActivity_V2.this);

        dialogSeatDetailsModificationPoupBinding = DialogSeatDetailsModificationPoupBinding.inflate(getLayoutInflater());
        final View view = dialogSeatDetailsModificationPoupBinding.getRoot();

        bottomSheetModificationDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomSheetModificationDialog.setContentView(view);
        bottomSheetModificationDialog.setCancelable(true);
        bottomSheetModificationDialog.show();
        bottomSheetModificationDialog.setTitle("Are You Sure, You Want To PROCESS This Transaction?");

        FrameLayout bottomSheet = (FrameLayout) bottomSheetModificationDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);


       /* dialogSeatDetails.setContentView(view);
        dialogSeatDetails.setCancelable(true);
        if (!isFinishing())
            dialogSeatDetails.show();*/
    }

    private void showAlreadyLoadTransactionsDialog() {
        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {

            if (!isFinishing()) {

                try {
                    dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
                    dialogLoadTransactions.show();
                    dialogLoadTransactions.setTitleText("You Have Already Loaded One Transactions !");
                    dialogLoadTransactions.setContentText("Do You Want To Process It?");


                    dialogLoadTransactions.setCancelable(false);
                    dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
                    dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));

                    dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {
                        dialogLoadTransactions.dismissWithAnimation();
                        dialogLoadTransactions.cancel();
                    });

                    dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                        dialogLoadTransactions.dismissWithAnimation();
                        dialogLoadTransactions.cancel();

                        clearModificationSeats();

                    });


                } catch (Exception ex) {
                    binding.actvSourceCity.setEnabled(true);
                }


            }
        }
    }

    private void clearModificationSeats() {


        selSeats.clear();
        seatSelectedViewModel.addSeats(selSeats);

        IsSearchOperation = 0;
        TRANSACTION_TYPE = APP_CONSTANTS.INSERT;
        modifyBookingTypeId = 0;

        bankCardId = 0;
        bankCardRemarks = "";

        modifyTotalPax = 0;
        modifyTotalRoundUp = 0;
        modifySeats = "";
        modifySeatsTemp = "";

        ABI_TotalSeat = 0;
        AM_AgentID = 0;
        ABI_AgentCityID = 0;
        ABI_TicketNo = "";
        ABI_NeteRate = 0;

        JM_GSTRegNo = "";
        JM_GSTState = 0;
        JM_GSTCompanyName = "";

        binding.etCmpCardDisc.setText("");
        binding.etCmpBkgCardno.setText("");
        binding.etCmpCardOtp.setText("");
        binding.txtCouponCode.setText("");
        binding.etBankNotes.setText("");
        binding.etBranchTicketNo.setText("");
        binding.etAgentTicketNo.setText("");
        binding.spnBookingType.setSelection(0);
        binding.chkGST.setChecked(false);
        binding.chkMultipleBooking.setChecked(false);
        binding.etRemarks.setText("");
        binding.etEmail.setText("");


    }

    private int isLoadTransactions() {
        if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
            binding.btnBookTicket.setText(getResources().getString(R.string.edit));
            return 1;
        } else {
            binding.btnBookTicket.setText(getResources().getString(R.string.book));
            if (binding.txtTransactionHighlight.getVisibility() == View.VISIBLE) {
                releaseTransactionHighLight();
            }
            return 0;
        }
    }

    private void seatModificationOperations() {
        clearModificationSeats();
        TRANSACTION_TYPE = APP_CONSTANTS.MODIFY;
        int loopCounter = 0;

        // This only for waiting booking type
        if (listFetchDetailsPNR.get(0).getBTMBookingTypeID() != null && listFetchDetailsPNR.get(0).getBTMBookingTypeID() == 8 && listFetchDetailsPNR.get(0).getJMTotalPassengers() != null && listFetchDetailsPNR.get(0).getJMTotalPassengers() > listFetchDetailsPNR.size()) {
            for (int i = 0; i < (listFetchDetailsPNR.get(0).getJMTotalPassengers() - 1); i++) {
                listFetchDetailsPNR.add(listFetchDetailsPNR.get(0));
            }
        }

        for (FetchDetailsByPNRNO_Response.Datum data : listFetchDetailsPNR) {
            SelectedSeatDetailsBean dataSeats = new SelectedSeatDetailsBean();

            if (loopCounter == 0) {
                sourceCityId = data.getJMJourneyFrom();
                destCityId = data.getJMJourneyTo();
                sel_SourceCityName = data.getFromCity();
                sel_DestinationCityName = data.getToCity();

                binding.actvSourceCity.setText(data.getFromCity());
                binding.actvDestCity.setText(data.getToCity());
                binding.actvDestCity.dismissDropDown();
                binding.actvDestCity.clearFocus();

                modifyTotalPax = data.getJMTotalPassengers();
                totalPax = modifyTotalPax;

                binding.etTotGst.setText(String.format(Locale.getDefault(), "%.2f", data.getServiceTaxAmt()));
                binding.etTotalAmount.setText(String.format(Locale.getDefault(), "%.2f", data.getJMPayableAmount()));

                binding.etRemarks.setText(data.getJMRemarks());
                binding.etEmail.setText(data.getJMEmailID());

                binding.etNoOfSeat.setText(String.valueOf(modifyTotalPax));

                JM_B2C_OrderNo = String.valueOf(data.getJMB2COrderNo());
                ModiFyPNRNo = String.valueOf(data.getJMPNRNO());

                if (!TextUtils.isEmpty(data.getJMGSTRegNo())) {
                    JM_GSTRegNo = data.getJMGSTRegNo();
                    JM_GSTState = data.getJMGSTState();
                    JM_GSTCompanyName = data.getJMGSTCompanyName();

                    binding.chkGST.setChecked(true);

                }

                int pickUpIndex = -1;
                if (listPickUp != null && listPickUp.size() > 0) {
                    for (int j = 0; j < listPickUp.size(); j++) {
                        if (listPickUp.get(j).getPSMPickupID().equals(data.getPMPickupID())) {
                            pickUpIndex = j;
                            break;
                        }
                    }
                    if (pickUpIndex != -1) {
                        binding.spnPickup.setSelection(pickUpIndex);
                    }
                }


                int dropUpIndex = -1;
                if (listDropUp != null && listDropUp.size() > 0) {
                    for (int j = 0; j < listDropUp.size(); j++) {
                        if (listDropUp.get(j).getPSMPickupID().equals(data.getDMDropID())) {
                            dropUpIndex = j;
                            break;
                        }
                    }
                    if (dropUpIndex != -1) {
                        binding.spnDrop.setSelection(dropUpIndex);
                    }
                }


                int bookingTypeIndex = 0;
                if (bookingTypeList != null && bookingTypeList.size() > 0) {
                    for (int j = 0; j < bookingTypeList.size(); j++) {
                        if (bookingTypeList.get(j).getBookingTypeId() == data.getBTMBookingTypeID()) {
                            bookingTypeIndex = j;
                            modifyBookingTypeId = data.getBTMBookingTypeID();
                            if (data.getBTMBookingTypeID() == APP_CONSTANTS.AGENT_BOOKING || data.getBTMBookingTypeID() == APP_CONSTANTS.AGENT_PHONE_BOOKING || data.getBTMBookingTypeID() == APP_CONSTANTS.API_PHONE_BOOKING) {
                                AM_AgentID = data.getAMAgentID();
                                ABI_TicketNo = data.getABIAgentTicketNo();
                                ABI_NeteRate = data.getABINetRate();
                                ABI_TotalSeat = modifyTotalPax;
                                ABI_AgentCityID = data.getABIAgentCityID();
                                binding.etAgentTicketNo.setText(ABI_TicketNo);
                            } else if (data.getBTMBookingTypeID() == APP_CONSTANTS.BRANCH_BOOKING) {
                                BBI_TicketNo = data.getBBI_TicketNo();
                                BBI_UM_UserID = data.getUM_UserID2();
                                BBI_BM_BranchID = data.getBM_BranchID2();
                            } else if (data.getBTMBookingTypeID() == APP_CONSTANTS.GUEST_BOOKING) {
                                GTM_GuestTypeID = data.getgTMGuestTypeID();
                            } else if (data.getBTMBookingTypeID() == APP_CONSTANTS.BANK_CARD_BOOKING) {
                                bankCardId = data.getPHTransStatus();
                                bankCardRemarks = data.getPHCardHolderName();
                            } else if (data.getBTMBookingTypeID() == APP_CONSTANTS.CMP_CARD_BOOKING) {
                                cmpCardNo = data.getPHCardID();
                                binding.etCmpBkgCardno.setText(cmpCardNo);
                                if (!TextUtils.isEmpty(cmpCardNo)) {
                                    binding.etCmpBkgCardno.setSelection(cmpCardNo.length());
                                }
                            } else if (data.getBTMBookingTypeID() == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                                AM_AgentID = data.getAMAgentID();
                                ABI_TotalSeat = modifyTotalPax;
                                ABI_AgentCityID = data.getABIAgentCityID();
                            } else if (data.getBTMBookingTypeID() == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {

                            }
                            break;
                        }
                    }

                    if (bookingTypeIndex != -1) {
                        binding.spnBookingType.setSelection(bookingTypeIndex);
                    }
                }

            }


            if (data.getCADBusType() != null && data.getCADBusType() == 0) {
                if (data.getCADSeatType() == 0) {
                    dataSeats.setSeatRate(data.getRMACSeatRate());
                } else if (data.getCADSeatType() == 1) {
                    dataSeats.setSeatRate(data.getRMACSleeperRate());
                } else if (data.getCADSeatType() == 2) {
                    dataSeats.setSeatRate(data.getRMACSlumberRate());
                }
            } else if (data.getCADBusType() != null && data.getCADBusType() == 1) {
                if (data.getCADSeatType() == 0) {
                    dataSeats.setSeatRate(data.getRMNonACSeatRate());
                } else if (data.getCADSeatType() == 1) {
                    dataSeats.setSeatRate(data.getRMNonACSleeperRate());
                } else if (data.getCADSeatType() == 2) {
                    dataSeats.setSeatRate(data.getRMNonAcSlumberRate());
                }
            }

            dataSeats.setIdProofId(data.getJPIDProof());
            dataSeats.setIdProofNumber(data.getJPIDProofNumber());

            binding.etAcSeat.setText(String.valueOf(data.getRMACSeatRate()));
            binding.etAcSleeper.setText(String.valueOf(data.getRMACSleeperRate()));
            binding.etAcSlumber.setText(String.valueOf(data.getRMACSlumberRate()));

            binding.etNonacSeat.setText(String.valueOf(data.getRMNonACSeatRate()));
            binding.etNonacSleeper.setText(String.valueOf(data.getRMNonACSleeperRate()));
            binding.etNonacSlumber.setText(String.valueOf(data.getRMNonAcSlumberRate()));

            modifyAcSeatTotal = data.getRMACSeatQuantity();
            modifyAcSlpTotal = data.getRMACSleeperQuantity();
            modifyAcSlmbTotal = data.getRMACSlumberQuantity();
            modifyNonAcSeatTotal = data.getRMNonACSeatQuantity();
            modifyNonAcSlpTotal = data.getRMNonACSleeperQuantity();
            modifyNonAcSlmbTotal = data.getRMNonACSlumberQuantity();

            binding.etTotAcSeat.setText(String.valueOf(data.getRMACSeatQuantity()));
            binding.etTotAcSleeper.setText(String.valueOf(data.getRMACSleeperQuantity()));
            binding.etTotAcSlumber.setText(String.valueOf(data.getRMACSlumberQuantity()));


            binding.etTotNonacSeat.setText(String.valueOf(data.getRMNonACSeatQuantity()));
            binding.etTotNonacSleeper.setText(String.valueOf(data.getRMNonACSleeperQuantity()));
            binding.etTotNonacSlumber.setText(String.valueOf(data.getRMNonACSlumberQuantity()));

            JM_GSTState = data.getJMGSTState();
            JM_GSTCompanyName = data.getJMGSTCompanyName();
            JM_GSTRegNo = data.getJMGSTRegNo();


            dataSeats.setRouteId(data.getRMRouteID());
            dataSeats.setRouteTimeId(data.getRTTime());
            dataSeats.setCADBusType(data.getCADBusType());
            dataSeats.setCADSeatType(data.getCADSeatType());
            dataSeats.setBADSeatNo(data.getBADSeatNo());

            if (data.getBTMBookingTypeID() != null && data.getBTMBookingTypeID() == 8 && loopCounter != 0) {
                dataSeats.setJMPassengerName("");
                dataSeats.setPaxAge("");
                dataSeats.setJPGender("");
                dataSeats.setJMPhone1("");
            } else {
                if (!TextUtils.isEmpty(data.getJPPassengerName())) {
                    dataSeats.setJMPassengerName(data.getJPPassengerName());
                } else if (!TextUtils.isEmpty(data.getJMPassengerName())) {
                    dataSeats.setJMPassengerName(data.getJMPassengerName());
                }

                if (data.getJPAge() != null && data.getJPAge() > 0) {
                    dataSeats.setPaxAge(String.valueOf(data.getJPAge()));
                } else {
                    dataSeats.setPaxAge("");
                }

                if (!TextUtils.isEmpty(data.getJPGender())) {
                    dataSeats.setJPGender(data.getJPGender());
                }

                if (!TextUtils.isEmpty(data.getJPPhoneNo())) {
                    dataSeats.setJMPhone1(data.getJPPhoneNo());
                } else if (!TextUtils.isEmpty(data.getJMPhone1())) {
                    dataSeats.setJMPhone1(data.getJMPhone1());
                }

            }

            modifyTotalRoundUp += data.getJPRoundUP();
            modifyTotalGST += data.getJPServiceTax();

            modifySeatsTemp = TextUtils.isEmpty(modifySeatsTemp) ? data.getBADSeatNo() : modifySeatsTemp + "," + data.getBADSeatNo();

            selSeats.add(dataSeats);

            loopCounter++;
        }

        if (selSeats != null && selSeats.size() > 0) {
            seatSelectedViewModel.addSeats(selSeats);
        }

        applyTransactionHighLight();

    }

    private int applyValidationModificationPopUp() {
        //  isApiB2CAgent => Agent Type like as general,api,b2c,etc
        int phoneBookingToRemotePayment = itsGetDisplayRouteBeforeMin.getOSPhoneBookingToRemotePayment() != null ? itsGetDisplayRouteBeforeMin.getOSPhoneBookingToRemotePayment() : 0;
        int JM_IsPrepaidCard = listFetchDetailsPNR.get(0).getJMIsPrepaidCard();
        int CNM_ISWallet = listFetchDetailsPNR.get(0).getCNMIsWallet();
        int bookingType = listFetchDetailsPNR.get(0).getBTMBookingTypeID();
        int routeBranchId = listFetchDetailsPNR.get(0).getBMBranchID();
        int isApiB2CAgent = listFetchDetailsPNR.get(0).getCAMIsOnline();
        int pnrCompanyId = listFetchDetailsPNR.get(0).getJMBookedByCMCompanyID();
        int AllowPhoneOnHoldSeat = listFetchDetailsPNR.get(0).getJMPhoneOnHold();
        int totalPax = listFetchDetailsPNR.size();

        int holdStatus = 0, rePrintStatus = 0, emailStatus = 0, smsStatus = 0, logStatus = 0, feedBackStatus = 0,
                pdfStatus = 0, openStatus = 0, callStatus = 0, callVisible = 0, haveRightsForAll = 1, cancelStatus = 0, modifyStatus = 0,
                checkBoxStatus = 0, removeAndBookStatus = 0, haveRightsForKeep = 1, confirmStatus = 0,
                reportedStatus = 0, nonReportedStatus = 0, paymentLinkStatus = 0;


        if (bookingRightsByUserData == null || itsGetDisplayRouteBeforeMin == null) {
            return checkBoxStatus;
        }

        if (loginCmpId == pnrCompanyId) {
            if (!TextUtils.isEmpty(BUM_Agent_Number)) {
                callStatus = CDH_IsCallCenterAPI;
            }
        }

        if ((bookingType == APP_CONSTANTS.AGENT_BOOKING && isApiB2CAgent == APP_CONSTANTS.GENERAL_AGENT) ||
                bookingType == APP_CONSTANTS.AGENT_QUOTA_BOOKING ||
                bookingType == APP_CONSTANTS.BRANCH_BOOKING) {

            smsStatus = itsGetDisplayRouteBeforeMin.getCMIsSMSActive();
            logStatus = bookingRightsByUserData.getIsBookingLog();
            if (bookingType == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                callStatus = APP_CONSTANTS.RIGHTS_DENY;
                holdStatus = itsGetDisplayRouteBeforeMin.getOSHoldAgentQuota();
                emailStatus = itsGetDisplayRouteBeforeMin.getOSEmailAgentQuata();
                pdfStatus = itsGetDisplayRouteBeforeMin.getOSPDFAgentQuata();
                cancelStatus = bookingRightsByUserData.getBRAgentQuataCancel();
                modifyStatus = bookingRightsByUserData.getBRAgentQuataModify();
            } else if (bookingType == APP_CONSTANTS.BRANCH_BOOKING) {
                holdStatus = itsGetDisplayRouteBeforeMin.getOSHoldBranch();
                emailStatus = itsGetDisplayRouteBeforeMin.getOSEmailBranch();
                pdfStatus = itsGetDisplayRouteBeforeMin.getOSPDFBranch();
                cancelStatus = bookingRightsByUserData.getBRBranchCancel();
                modifyStatus = bookingRightsByUserData.getBRBranchModify();
            } else if (bookingType == APP_CONSTANTS.AGENT_BOOKING) {
                holdStatus = OS_HoldGeneralAgent;
                rePrintStatus = OS_ReprintGeneralAgent;
                emailStatus = OS_EmailGeneralAgent;
                pdfStatus = OS_PDFGeneralAgent;
                cancelStatus = bookingRightsByUserData.getBRAgentCancel();
                modifyStatus = bookingRightsByUserData.getBRAgentModify();

            }

            haveRightsForKeep = cancelStatus;
            if (IsReportedPNR == APP_CONSTANTS.RIGHTS_ALLOW && (bookingType == APP_CONSTANTS.BRANCH_BOOKING || bookingType == APP_CONSTANTS.AGENT_BOOKING)) {
                cancelStatus = APP_CONSTANTS.RIGHTS_DENY;
                modifyStatus = cancelStatus;
            }

            if (bookingRightsByUserData.getGAAllowNonReported() == APP_CONSTANTS.RIGHTS_ALLOW) {
                checkBoxStatus = APP_CONSTANTS.RIGHTS_ALLOW;
            } else {
                checkBoxStatus = haveRightsForKeep;
                reportedStatus = haveRightsForKeep;
                nonReportedStatus = haveRightsForKeep;
            }

            if (totalPax > 1) {
                //removeAndBookStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                removeAndBookStatus = haveRightsForKeep;
            }


        } else if (bookingType == APP_CONSTANTS.CONFIRM_BOOKING ||
                bookingType == APP_CONSTANTS.GUEST_BOOKING ||
                bookingType == APP_CONSTANTS.BANK_CARD_BOOKING ||
                bookingType == APP_CONSTANTS.CMP_CARD_BOOKING ||
                bookingType == APP_CONSTANTS.AGENT_CC_CARD_BOOKING ||
                bookingType == APP_CONSTANTS.AGENT_BOOKING ||
                bookingType == APP_CONSTANTS.ITSPL_WALLET_BOOKING ||
                bookingType == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {

            smsStatus = CM_IsSMSActive;
            logStatus = IsBookingLog;
            feedBackStatus = GA_IsShowFeedBack;


            if (GA_AllowReprint == APP_CONSTANTS.RIGHTS_ALLOW) {
                rePrintStatus = APP_CONSTANTS.RIGHTS_ALLOW;
            }

            if (bookingType == APP_CONSTANTS.CONFIRM_BOOKING) {
                emailStatus = OS_EmailConfirm;
                pdfStatus = OS_PDFConfirm;
                if (GA_AllowConfirmToOpen == APP_CONSTANTS.RIGHTS_ALLOW && loginCmpId == pnrCompanyId && BM_IsPrepaid != 1) {
                    openStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                }

                cancelStatus = bookingRightsByUserData.getBRConfirmCancel();
                modifyStatus = bookingRightsByUserData.getBRConfirmModify();


            } else if (bookingType == APP_CONSTANTS.AGENT_BOOKING) {
                if (isApiB2CAgent == APP_CONSTANTS.GENERAL_AGENT) {
                    holdStatus = OS_HoldGeneralAgent;
                    rePrintStatus = OS_ReprintGeneralAgent;
                    emailStatus = OS_EmailGeneralAgent;
                    pdfStatus = OS_PDFGeneralAgent;

                } else if (isApiB2CAgent == APP_CONSTANTS.ONLINE_AGENT) {
                    emailStatus = OS_EmailOnlineAgent;
                    pdfStatus = OS_PDFOnlineWallet;
                    if (branchId == routeBranchId) {
                        modifyStatus = bookingRightsByUserData.getBROnlineAgentModify();
                        cancelStatus = bookingRightsByUserData.getBROnlineAgentCancel();
                    }
                } else if (isApiB2CAgent == APP_CONSTANTS.API_AGENT) {
                    emailStatus = OS_EmailAPIAgent;
                    pdfStatus = OS_PDFAPIAgent;
                    if (branchId == routeBranchId) {
                        modifyStatus = bookingRightsByUserData.getBRAPIModify();
                        cancelStatus = bookingRightsByUserData.getBRAPICancel();
                    }
                } else if (isApiB2CAgent == APP_CONSTANTS.B2C_AGENT) {
                    emailStatus = OS_EmailB2CAgent;
                    pdfStatus = OS_PDFB2CAgent;
                    if (branchId == routeBranchId) {
                        modifyStatus = bookingRightsByUserData.getBRB2CModify();
                        cancelStatus = bookingRightsByUserData.getBRB2CCancel();
                    }
                }

            } else if (bookingType == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                emailStatus = OS_EmailOnlineWallet;
                pdfStatus = OS_PDFOnlineWallet;
                cancelStatus = bookingRightsByUserData.getBROnlineWalletCancel();
                modifyStatus = bookingRightsByUserData.getBROnlineWalletModify();
            } else if (bookingType == APP_CONSTANTS.AGENT_CC_CARD_BOOKING) {
                emailStatus = OS_EmailOnlineAgent;
                pdfStatus = OS_PDFOnlineAgent;
                if (branchId == routeBranchId) {
                    modifyStatus = bookingRightsByUserData.getBROnlineAgentCardModify();
                    cancelStatus = bookingRightsByUserData.getBROnlineAgentCardCancel();
                }
            } else if (bookingType == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {
                emailStatus = OS_EmailConfirm;
                pdfStatus = OS_PDFConfirm;
                callStatus = APP_CONSTANTS.RIGHTS_DENY;
                cancelStatus = bookingRightsByUserData.getBRRemotePaymentCancel();
                modifyStatus = bookingRightsByUserData.getBRRemotePaymentModify();
                if (loginCmpId == pnrCompanyId && phoneBookingToRemotePayment == APP_CONSTANTS.RIGHTS_ALLOW) {
                    paymentLinkStatus = itsGetDisplayRouteBeforeMin.getCMIsSMSActive();
                }
            } else if (bookingType == APP_CONSTANTS.GUEST_BOOKING) {
                holdStatus = OS_HoldGuest;
                emailStatus = OS_EmailGuest;
                pdfStatus = OS_PDFGuest;
                cancelStatus = bookingRightsByUserData.getBRGuestCancel();
                modifyStatus = bookingRightsByUserData.getBRGuestModify();
            } else if (bookingType == APP_CONSTANTS.BANK_CARD_BOOKING) {
                holdStatus = OS_HoldBankCard;
                emailStatus = OS_EmailBankCard;
                pdfStatus = OS_PDFBankCard;
                cancelStatus = bookingRightsByUserData.getBRBankCardCancel();
                modifyStatus = bookingRightsByUserData.getBRBankCardModify();
            } else if (bookingType == APP_CONSTANTS.CMP_CARD_BOOKING) {
                holdStatus = OS_HoldCompanyCard;
                emailStatus = OS_EmailCompanyCard;
                pdfStatus = OS_PDFCompanyCard;
                cancelStatus = bookingRightsByUserData.getBRCompanyCardCancel();
                modifyStatus = bookingRightsByUserData.getBRCompanyCardModify();
                if (branchId == routeBranchId) {
                    // self Branch
                    if (JM_IsPrepaidCard == 2) {
                        modifyStatus = APP_CONSTANTS.RIGHTS_DENY;
                    }
                }

            }

            haveRightsForAll = cancelStatus;

            if (CNM_ISWallet == APP_CONSTANTS.RIGHTS_DENY) {
                // Remove And New
                if (totalPax > 1) {
                    removeAndBookStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                    if (bookingType == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING || bookingType == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                        removeAndBookStatus = APP_CONSTANTS.RIGHTS_DENY;
                    } /*else if (haveRightsForAll == APP_CONSTANTS.RIGHTS_ALLOW && IsReportedPNR == APP_CONSTANTS.RIGHTS_DENY) {
                        removeAndBookStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                    }*/
                } else {
                    removeAndBookStatus = APP_CONSTANTS.RIGHTS_DENY;
                }

                // Check Box
                if (bookingType == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING || bookingType == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                    checkBoxStatus = APP_CONSTANTS.RIGHTS_DENY;
                } else if (bookingRightsByUserData.getGAAllowNonReported() == APP_CONSTANTS.RIGHTS_ALLOW) {
                    checkBoxStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                } else {
                    checkBoxStatus = haveRightsForAll;
                }

            }

            if (IsReportedPNR == APP_CONSTANTS.RIGHTS_ALLOW) {
                cancelStatus = APP_CONSTANTS.RIGHTS_DENY;
                modifyStatus = cancelStatus;
            }

        } else if (bookingType == APP_CONSTANTS.PHONE_BOOKING ||
                bookingType == APP_CONSTANTS.AGENT_PHONE_BOOKING ||
                bookingType == APP_CONSTANTS.API_PHONE_BOOKING) {

            holdStatus = APP_CONSTANTS.RIGHTS_ALLOW;
            emailStatus = itsGetDisplayRouteBeforeMin.getOSEmailPhone();
            smsStatus = itsGetDisplayRouteBeforeMin.getCMIsSMSActive();
            pdfStatus = itsGetDisplayRouteBeforeMin.getOSPDFPhone();
            logStatus = bookingRightsByUserData.getIsBookingLog();


            if (bookingType == APP_CONSTANTS.PHONE_BOOKING) {
                holdStatus = GA_AllowPhoneOnHoldSeat;
                cancelStatus = bookingRightsByUserData.getBRPhoneCancel();
                modifyStatus = bookingRightsByUserData.getBRPhoneModify();
                if (branchId != routeBranchId) {
                    if (bookingRightsByUserData.getBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                        confirmStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                    }
                } else {
                    if (bookingRightsByUserData.getBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                        confirmStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                    }
                }

                if (loginCmpId == pnrCompanyId && phoneBookingToRemotePayment == APP_CONSTANTS.RIGHTS_ALLOW) {
                    paymentLinkStatus = itsGetDisplayRouteBeforeMin.getCMIsSMSActive();
                }

            } else if (bookingType == APP_CONSTANTS.API_PHONE_BOOKING) {
                callStatus = APP_CONSTANTS.RIGHTS_DENY;
                cancelStatus = bookingRightsByUserData.getBRAPIPhoneCancel();
                modifyStatus = bookingRightsByUserData.getBRAPIPhoneModify();
            } else if (bookingType == APP_CONSTANTS.AGENT_PHONE_BOOKING) {
                callStatus = APP_CONSTANTS.RIGHTS_DENY;
                cancelStatus = bookingRightsByUserData.getBROnlineAgentPhoneCancel();
                modifyStatus = bookingRightsByUserData.getBROnlineAgentPhoneModify();
            }
            haveRightsForKeep = cancelStatus;

            if (bookingRightsByUserData.getGAAllowNonReported() == APP_CONSTANTS.RIGHTS_ALLOW) {
                checkBoxStatus = APP_CONSTANTS.RIGHTS_ALLOW;
            } else {
                checkBoxStatus = haveRightsForKeep;
            }

            reportedStatus = haveRightsForKeep;
            nonReportedStatus = haveRightsForKeep;


            if (totalPax > 1) {
                //removeAndBookStatus = APP_CONSTANTS.RIGHTS_ALLOW;
                removeAndBookStatus = haveRightsForKeep;
            }


        }

        if (bookingRightsByUserData.getGAAllowNonReported() == APP_CONSTANTS.RIGHTS_ALLOW) {
            reportedStatus = APP_CONSTANTS.RIGHTS_ALLOW;
            nonReportedStatus = APP_CONSTANTS.RIGHTS_ALLOW;
        }

        if (bookingType == APP_CONSTANTS.OPEN_BOOKING || bookingType == APP_CONSTANTS.WAITING_BOOKING) {
            callStatus = APP_CONSTANTS.RIGHTS_DENY;
        }

        if (holdStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtHold.setVisibility(View.VISIBLE);
            if (GA_AllowPhoneOnHoldSeat == APP_CONSTANTS.RIGHTS_ALLOW) {
                if (AllowPhoneOnHoldSeat == APP_CONSTANTS.RIGHTS_ALLOW) {
                    seatModifyBinding.txtHold.setText(getString(R.string.unhold));
                } else {
                    seatModifyBinding.txtHold.setText(getString(R.string.hold));
                }
            }
        }

        if (rePrintStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtReprint.setVisibility(View.VISIBLE);
        }

        if (emailStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtEticket.setText("E-Ticket");
            seatModifyBinding.txtEticket.setVisibility(View.VISIBLE);
        }

        if (smsStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtSendSms.setVisibility(View.VISIBLE);
        }

        if (logStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtLog.setVisibility(View.VISIBLE);
        }

        if (feedBackStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtFeedback.setVisibility(View.VISIBLE);
        }

        if (pdfStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtPdf.setVisibility(View.VISIBLE);
        }

        if (openStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtOpen.setVisibility(View.VISIBLE);
        }

        if (callStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtCall.setVisibility(View.VISIBLE);
            seatModifyBinding.txtCall.setEnabled(false);
            seatModifyBinding.txtCall.setTextColor(getResources().getColor(R.color.black));
        }

        if (cancelStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtCancel.setVisibility(View.VISIBLE);
        }

        if (modifyStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtModify.setVisibility(View.VISIBLE);
        }

        if (removeAndBookStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtRemoveAndBook.setVisibility(View.VISIBLE);
        }

        if (confirmStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtConfirm.setVisibility(View.VISIBLE);
        }

        if (reportedStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtReported.setVisibility(View.VISIBLE);
        }

        if (nonReportedStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtNonReported.setVisibility(View.VISIBLE);
        }

        if (paymentLinkStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
            seatModifyBinding.txtPaymentLinkSms.setVisibility(View.VISIBLE);
            seatModifyBinding.txtPaymentLinkSms.setPaintFlags(seatModifyBinding.txtPaymentLinkSms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        return checkBoxStatus;

    }

    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }

        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    private int getNavigationBarHeight() {
        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private void showSeatModificationPopUp() {
        IsReportedPNR = 0;
        int totalPax, pnrNo;
        String emailId, phoneNo, seatNo;

        totalPax = listFetchDetailsPNR.size();
        pnrNo = listFetchDetailsPNR.get(0).getJMPNRNO() != null ? listFetchDetailsPNR.get(0).getJMPNRNO() : 0;
        emailId = listFetchDetailsPNR.get(0).getJMEmailID();
        phoneNo = listFetchDetailsPNR.get(0).getJPPhoneNo();
        seatNo = listFetchDetailsPNR.get(0).getBADSeatNo();

        for (FetchDetailsByPNRNO_Response.Datum data : listFetchDetailsPNR) {
            if (IsReportedPNR == 0) {
                IsReportedPNR = data.getJPChartFinishStatus() != null ? data.getJPChartFinishStatus() : 0;
            }
        }

        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
            popupModificationDialog.dismiss();
        }

        popupModificationDialog = new PopupWindow();
        popupModificationDialog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        if (displaymetrics != null) {
            popupModificationDialog.setWidth(displaymetrics.widthPixels / 4);
        } else {
            popupModificationDialog.setWidth(250);
        }

        popupModificationDialog.setOutsideTouchable(true);
        popupModificationDialog.setFocusable(true);

        seatModifyBinding = LayoutSeatModificationPopupV2Binding.inflate(getLayoutInflater());
        View view = seatModifyBinding.getRoot();

        popupModificationDialog.setContentView(view);


        popupModificationDialog.showAsDropDown(btnSeat);

        int checkBoxStatus = applyValidationModificationPopUp();

        int finalPnrNo = pnrNo;
        String finalEmailId = emailId;
        int finalTotalPax = totalPax;
        String selectedPNR = "";
        final String[] selectedSeats = {""};

        if (!TextUtils.isEmpty(String.valueOf(finalPnrNo))) {

            seatModifyBinding.txtLog.setOnClickListener(view12 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                PNRBookingLogApiCall(String.valueOf(finalPnrNo));
            });

            seatModifyBinding.txtEticket.setOnClickListener(view13 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                openETicketDialog(APP_CONSTANTS.SEND_TICKET);
            });

            seatModifyBinding.txtPdf.setOnClickListener(view14 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                openETicketDialog(APP_CONSTANTS.DOWNLOAD_TICKET);
            });

            seatModifyBinding.txtSeatDetails.setOnClickListener(view15 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                showSeatDetailsDialog();
            });

            seatModifyBinding.txtSendSms.setOnClickListener(view16 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                sendSmsToPNRDialog(1, phoneNo, String.valueOf(finalPnrNo));
            });

            seatModifyBinding.txtCancel.setOnClickListener(view17 -> {
                if (cancelBookingValidation()) {
                    if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                        popupModificationDialog.dismiss();
                    }
                    cancelDetailsApiCall();
                }
            });

            seatModifyBinding.txtModify.setOnClickListener(view18 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                showWarningDialog(2, String.valueOf(finalPnrNo));
            });

            seatModifyBinding.txtOpen.setOnClickListener(view19 -> {
                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }
                openToConfirmDialog(String.valueOf(finalPnrNo));
            });

            final String[] finalSeatNo = {seatNo};
            seatModifyBinding.txtReported.setOnClickListener(view110 -> {
                finalSeatNo[0] = finalTotalPax > 1 ? selectedSeats[0] : finalSeatNo[0];
                if (finalTotalPax > 1 && TextUtils.isEmpty(finalSeatNo[0])) {
                    showSnackBar(oops, validSeatNo, Toast.LENGTH_LONG, 1);
                } else {
                    if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                        popupModificationDialog.dismiss();
                    }
                    reportedApiCall(selectedPNR, finalSeatNo[0]);
                }
            });

            seatModifyBinding.txtNonReported.setOnClickListener(view1 -> {
                finalSeatNo[0] = finalTotalPax > 1 ? selectedSeats[0] : finalSeatNo[0];
                listReqParameters.clear();
                if (finalTotalPax > 1 && TextUtils.isEmpty(finalSeatNo[0])) {
                    showSnackBar(oops, validSeatNo, Toast.LENGTH_LONG, 1);
                } else {
                    if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                        popupModificationDialog.dismiss();
                    }

                    listReqParameters.add(selectedPNR);
                    listReqParameters.add(selectedSeats[0]);
                    String dialogTitle = "Seat Non Reported";
                    String dialogDesc = "Are You Sure , You Want To Set Seats [ " + selectedSeats[0] + " ] As a Non Reported?";
                    openCommonWarningDilaog(APP_CONSTANTS.NON_REPORTED, dialogTitle, dialogDesc);
                }
            });

            seatModifyBinding.txtHold.setOnClickListener(arg1 -> {
                finalSeatNo[0] = finalTotalPax > 1 ? selectedSeats[0] : finalSeatNo[0];
                listReqParameters.clear();
                // Add Transaction Status into listReqParameters
                listReqParameters.add("1");
                if (finalTotalPax > 1 && TextUtils.isEmpty(finalSeatNo[0])) {
                    showSnackBar(oops, validSeatNo, Toast.LENGTH_LONG, 1);
                } else {
                    listReqParameters.add(finalSeatNo[0]);
                    if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                        popupModificationDialog.dismiss();
                    }
                    String dialogTitle = "Seat Block";
                    String dialogDesc = "Are You Sure , You Want To Seat Block ?";
                    openCommonWarningDilaog(APP_CONSTANTS.SEAT_BLOCK_UNBLOCK, dialogTitle, dialogDesc);
                }
            });

            if (totalPax > 1 && checkBoxStatus == APP_CONSTANTS.RIGHTS_ALLOW) {
                seatModifyBinding.rvSeatNo.setLayoutManager(new GridLayoutManager(ExeBookingActivity_V2.this, 2, GridLayoutManager.VERTICAL, false));

                SeatModificationListAdapter adapter = new SeatModificationListAdapter(ExeBookingActivity_V2.this, listFetchDetailsPNR, new SeatModificationListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(FetchDetailsByPNRNO_Response.Datum item, int position) {
                        selectedSeats[0] = "";
                        for (FetchDetailsByPNRNO_Response.Datum dataSeat : listFetchDetailsPNR) {
                            if (dataSeat.getRadioButton() == 1) {
                                seatModifyBinding.txtCancel.setTextColor(getResources().getColor(R.color.white));
                                seatModifyBinding.txtCancel.setEnabled(true);
                                selectedSeats[0] = TextUtils.isEmpty(selectedSeats[0]) ? dataSeat.getBADSeatNo() : selectedSeats[0] + "," + dataSeat.getBADSeatNo();
                            }
                        }
                        if (selectedSeats[0].length() != totalPax) {
                            seatModifyBinding.txtCancel.setTextColor(getResources().getColor(R.color.color_top_header_dark));
                            seatModifyBinding.txtCancel.setEnabled(false);
                        } else {
                            try {
                                if (sdf_full.parse(str_journeyDate).compareTo(sdf_full.parse(date_today)) < 0) {
                                    if (GA_AllowBackDateCancellation == 0) {
                                        seatModifyBinding.txtCancel.setEnabled(false);
                                        seatModifyBinding.txtCancel.setTextColor(getResources().getColor(R.color.black));
                                    }
                                }
                            } catch (ParseException ignored) {
                            }
                        }
                    }
                });
                seatModifyBinding.rvSeatNo.setAdapter(adapter);
            }

        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:

                if (view.getId() == binding.actvSourceCity.getId()) {
                    binding.actvSourceCity.showDropDown();
                } else if (view.getId() == binding.actvDestCity.getId()) {
                    binding.actvDestCity.showDropDown();
                } else {
                    if (isLoadTransactions() == APP_CONSTANTS.STATUS_SUCCESS) {
                        if (modifyBookingTypeId != 2 && modifyBookingTypeId != 8 && modifyBookingTypeId != 9 && modifyBookingTypeId != 15 && modifyBookingTypeId != 15) {
                            showAlreadyLoadTransactionsDialog();
                            return true;
                        }
                    }
                }

                return false;
        }
        return false;
    }

    private void showLastTransactionDetails(String route, String subroute, String seats, String mobile,
                                            String jdate, String jtime, String pnr, String status, String bookingtype, String
                                                    totalamount) {
        binding.cvLastTransaction.setVisibility(View.VISIBLE);
        binding.txtLastTransCity.setText("JOURNEY # " + subroute + " | ");
        binding.txtLastTransSeats.setText("Seats " + seats + " | ");
        binding.txtLastTransPnr.setText("PNR No # " + pnr + " | ");
        binding.txtLastTransStatus.setText("Status # " + status);

        try {
            int startColor;
            if (reqBookingTypeColor.contains("#")) {
                startColor = Color.parseColor(reqBookingTypeColor);
            } else {
                startColor = Color.parseColor("#" + reqBookingTypeColor);
            }

            // int[] colors = {startColor, startColor};
            //GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
            //gradientDrawable.setStroke(1, borderColor);
            //binding.txtLastTransBookingType.setBackground(gradientDrawable);
//            binding.txtLastTransBookingType.setTextColor(getComplementaryColor(startColor));
            //binding.txtLastTransBookingType.setTextColor(startColor);
        } catch (Exception ignored) {
        }

        binding.txtLastTransBookingType.setText("Booking Type # " + bookingtype + " | ");
    }

    private int findBookingTypeIdInBookingTypeDialog(List<BookingTypeSeatCounterBean> bookingTypeTempList, int bookingTypeId) {
        int index = -1;
        for (int i = 0; i < bookingTypeTempList.size(); i++) {
            if (bookingTypeTempList.get(i).getBookingTypeId() == bookingTypeId) {
                index = i;
                break;
            }
        }
        return index;
    }


    private void showBookingTypeWiseSeatDetails(final int bookingTypeId, final String bookingTypeName) {
        int gridCount = 9;
        final List<BookingTypeSeatCounterBean> bookingTypeTempList = new ArrayList<>();


        final List<String> KeySet = new ArrayList<>(linkedHashMapBookedSeat.keySet());
        final LinkedHashMap<String, SelectedSeatDetailsBean> tempLinkedHashMapBookedSeat = new LinkedHashMap<>();

        for (String keys : KeySet) {
            SelectedSeatDetailsBean dataSeat = linkedHashMapBookedSeat.get(keys);

            int index = findBookingTypeIdInBookingTypeDialog(bookingTypeTempList, dataSeat.getBookingTypeID());
            if (index == -1) {
                BookingTypeSeatCounterBean dataSeatCounter = new BookingTypeSeatCounterBean();
                dataSeatCounter.setBookingTypeId(dataSeat.getBookingTypeID());
                dataSeatCounter.setDefaultName(dataSeat.getBookingTypeName());
                if (bookingTypeId == dataSeat.getBookingTypeID()) {
                    dataSeatCounter.setIsClick(1);
                    dataSeatCounter.setTextColor(R.color.bg_button_light);
                } else {
                    dataSeatCounter.setIsClick(0);
                    dataSeatCounter.setTextColor(R.color.black);
                }
                bookingTypeTempList.add(dataSeatCounter);
            }

            if (dataSeat.getBookingTypeID() == bookingTypeId) {
                tempLinkedHashMapBookedSeat.put(String.valueOf(dataSeat.getJMPNRNO()), dataSeat);
            }
        }

        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }

        final DialogBookingTypeWiseSeatDetailsBinding seatDetailsBinding = DialogBookingTypeWiseSeatDetailsBinding.inflate(getLayoutInflater());
        View view = seatDetailsBinding.getRoot();

        seatDetailsBinding.imgClose.setOnClickListener(view1 -> bottomBookingTypeSeatDetailsDialog.dismiss());

        GridLayoutManager gridBookingTypeManager = new GridLayoutManager(ExeBookingActivity_V2.this, gridCount);
        seatDetailsBinding.rvBookingType.setLayoutManager(gridBookingTypeManager);

        bookingTypeDialogAdapter = new BookingTypeDialog_Adapter(ExeBookingActivity_V2.this, bookingTypeTempList, (item, pos) -> {
            for (BookingTypeSeatCounterBean data : bookingTypeTempList) {
                if (item.getBookingTypeId() == data.getBookingTypeId()) {
                    data.setIsClick(1);
                    data.setTextColor(R.color.bg_button_light);
                } else {
                    data.setIsClick(0);
                    data.setTextColor(R.color.black);
                }
            }

            bookingTypeDialogAdapter.notifyDataSetChanged();

            LinkedHashMap<String, SelectedSeatDetailsBean> tempLinkedHashMap = new LinkedHashMap<>();

            for (String data : KeySet) {
                SelectedSeatDetailsBean dataSeat = linkedHashMapBookedSeat.get(data);
                if (dataSeat.getBookingTypeID() == item.getBookingTypeId()) {
                    tempLinkedHashMap.put(String.valueOf(dataSeat.getJMPNRNO()), dataSeat);
                }
            }
            bookSeatAdapter.modifyAdapter(tempLinkedHashMap);
            seatDetailsBinding.txtBookingType.setText("Book Ticket Details - " + item.getDefaultName());

        });

        seatDetailsBinding.rvBookingType.setAdapter(bookingTypeDialogAdapter);

        bookSeatAdapter = new BookingTypeSeatDetailsDialogAdapter(ExeBookingActivity_V2.this, tempLinkedHashMapBookedSeat, (item, position, flag) -> {
            switch (flag) {
                case 0:
                    break;
                case 1:
                    try {
                        if (GA_AllowBackDateCancellation == 0 && sdf_full.parse(str_journeyDate).compareTo(sdf_full.parse(date_today)) < 0) {
                            showToast("You can't cancel back date seat");
                        } else {
                            if (cancelBookingValidation()) {
                                showWarningDialog(flag, String.valueOf(item.getJMPNRNO()));
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    showWarningDialog(flag, String.valueOf(item.getJMPNRNO()));
                    break;
            }
        });
        seatDetailsBinding.rvSeatDetails.setAdapter(bookSeatAdapter);


        seatDetailsBinding.rvSeatDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                v.onTouchEvent(event);
                return true;
            }
        });

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);

        seatDetailsBinding.txtBookingType.setText(String.format(Locale.getDefault(), "Book Ticket Details - %s", bookingTypeName));

        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();

    }

    private void showWarningDialog(final int trans_type, final String reqParameters) {
        // 1-> cancel , 2-> modify
        String title = "", description = "";
        if (trans_type == 1) {
            title = "Cancel Ticket";
            description = "Are You Sure, You Want to PROCESS Cancel Seat Transaction?";
        } else if (trans_type == 2) {
            title = "Modify Ticket";
            description = "Are You Sure, You Want to PROCESS Modify Seat Transaction?";
        }
        if (dialogWarning != null && dialogWarning.isShowing()) {
            dialogWarning.dismiss();
        }
        dialogWarning = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
        if (!isFinishing())
            dialogWarning.show();

        dialogWarning.setTitleText(title);
        dialogWarning.setContentText(description);


        dialogWarning.setCancelable(false);

        dialogWarning.setConfirmText(getResources().getString(R.string.yes));
        dialogWarning.setCancelText(getResources().getString(R.string.no));


        dialogWarning.setConfirmClickListener(sweetAlertDialog -> {

            if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }

            dialogWarning.dismissWithAnimation();
            dialogWarning.cancel();

            switch (trans_type) {
                case 0:
                    break;
                case 1:
                    Get_SearchingPNRNOApiCall(0, reqParameters);
                    break;
                case 2:
                    resetSeatSelection();
                    Get_SearchingPNRNOApiCall(3, reqParameters);
                    break;
            }

        });

        dialogWarning.setCancelClickListener(sweetAlertDialog -> {
            dialogWarning.dismissWithAnimation();
            dialogWarning.cancel();
        });
    }

    private void releaseTransactionHighLight() {
        if (animInProgressTransaction != null) {
            try {
                animInProgressTransaction.cancel();
            } catch (Exception ignored) {
            }
        }
        binding.txtTransactionHighlight.setText("");
        binding.txtTransactionHighlight.clearAnimation();
        binding.txtTransactionHighlight.setVisibility(View.GONE);
    }

    private void applyTransactionHighLight() {
        binding.txtTransactionHighlight.setVisibility(View.VISIBLE);
        final String text = "You Are Modifying PNR No. : " + ModiFyPNRNo + " || Please Process Transaction";
        binding.txtTransactionHighlight.setText(text);
        try {
            animInProgressTransaction = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.zoom_in);
            binding.txtTransactionHighlight.startAnimation(animInProgressTransaction);
        } catch (Exception ignored) {
        }
    }

    private void waitingListReportApiCall() {
        showProgressDialog("Loading....");

        WaitingReport_Request request = new WaitingReport_Request(
                loginCmpId,
                str_journeyDate,
                jStartTime,
                jCityTime,
                sourceCityId,
                destCityId,
                routeId,
                routeTimeId,
                arrangementId,
                bookedByCmpId,
                IsSameDay
        );

        Call<WaitingReport_Response> call = apiService.WaitingReport(request);
        call.enqueue(new Callback<WaitingReport_Response>() {
            @Override
            public void onResponse(@NotNull Call<WaitingReport_Response> call, @NotNull Response<WaitingReport_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body().getStatus() == 1) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        openWaitingListReportDialog(response.body().getData());
                    } else {
                        showSnackBar(oops, "Waiting data unavailable", Toast.LENGTH_LONG, 1);
                    }
                } else {
                    showToast(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NotNull Call<WaitingReport_Response> call, @NotNull Throwable t) {
                disMissDialog();
            }
        });

    }

    private void openWaitingListReportDialog(List<WaitingReport_Response.Datum> list) {
        if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
            bottomBookingTypeSeatDetailsDialog.dismiss();
        }

        final DialogWaitingListReportBinding dialogWaiting = DialogWaitingListReportBinding.inflate(getLayoutInflater());
        View view = dialogWaiting.getRoot();

        dialogWaiting.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
        });

        dialogWaiting.rvPastBooking.setAdapter(new WaitingListReportAdapter(ExeBookingActivity_V2.this, list, (flag, item) -> {
            if (bottomBookingTypeSeatDetailsDialog != null && bottomBookingTypeSeatDetailsDialog.isShowing()) {
                bottomBookingTypeSeatDetailsDialog.dismiss();
            }
            switch (flag) {
                case 0:
                    Get_SearchingPNRNOApiCall(2, String.valueOf(item.getPNRNO()));
                    passengerListAdapter.setWaitingBookingState(1);
                    break;
                case 1:
                    try {
                        if (GA_AllowBackDateCancellation == 0 && sdf_search_pnr.parse(item.getJourneyDate()).compareTo(sdf_full.parse(date_today)) < 0) {
                            showToast("You can't cancel back date seat");
                        } else {
                            showWarningDialog(flag, String.valueOf(item.getPNRNO()));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }));

        bottomBookingTypeSeatDetailsDialog = new BottomSheetDialog(ExeBookingActivity_V2.this);
        bottomBookingTypeSeatDetailsDialog.setContentView(view);
        bottomBookingTypeSeatDetailsDialog.setCancelable(true);


        FrameLayout bottomSheet = (FrameLayout) bottomBookingTypeSeatDetailsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        if (!isFinishing())
            bottomBookingTypeSeatDetailsDialog.show();


    }

    private void openDateDialog() {

        final Calendar c = Calendar.getInstance();

        Date minDate = null, maxDate = null;

        try {
            c.add(Calendar.DATE, (CDH_AdvanceDateShow));
            maxDate = c.getTime();

            // Subtract 6 days from Calendar updated date
            c.add(Calendar.DATE, -((CDH_AdvanceDateShow + CDH_BackDateShow)));

            // Set the Calendar new date as minimum date of date picker
            minDate = c.getTime();

            maxDate = sdf_full.parse(sdf_full.format(maxDate));
            minDate = sdf_full.parse(sdf_full.format(minDate));

        } catch (Exception ignored) {
        }


        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
            popupModificationDialog.dismiss();
        }

        popupModificationDialog = new PopupWindow();
        popupModificationDialog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        if (displaymetrics != null) {
            popupModificationDialog.setWidth(displaymetrics.widthPixels / 4);
        } else {
            popupModificationDialog.setWidth(250);
        }


        // popup.setWidth(200);
        popupModificationDialog.setOutsideTouchable(true);
        popupModificationDialog.setFocusable(true);
        // popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        final DialogCustomCalendarBinding calendarBinding = DialogCustomCalendarBinding.inflate(getLayoutInflater());
        View view = calendarBinding.getRoot();


        popupModificationDialog.setContentView(view);
        //popupModificationDialog.showAsDropDown(btnSeat,0,0, Gravity.BOTTOM);
        popupModificationDialog.showAsDropDown(binding.imgCalendar);


        Date finalMaxDate = maxDate;
        Date finalMinDate = minDate;
        calendarBinding.robotoCalendarPicker.setRobotoCalendarListener(new CustomCalendarView.RobotoCalendarListener() {
            @Override
            public void onDayClick(Date date) {

                String valid_until = sdf_full.format(date);
                Date strDate = null;
                try {
                    strDate = sdf_full.parse(valid_until);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (strDate != null) {
                    if (finalMinDate != null && strDate.getTime() >= finalMinDate.getTime() && strDate.getTime() <= finalMaxDate.getTime()) {
                        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                            popupModificationDialog.dismiss();
                        }
                        reqDate = strDate;
                        binding.spnDate.setSelection(listDates.indexOf(valid_until));
                    } else if (finalMaxDate != null && finalMaxDate.getTime() >= strDate.getTime()) {
                        showToast("You don't have rights to select this date");
                    }
                }

            }

            @Override
            public void onDayLongClick(Date date) {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onLeftButtonClick() {

            }
        });
        calendarBinding.robotoCalendarPicker.setShortWeekDays(false);
        calendarBinding.robotoCalendarPicker.showDateTitle(true);

        if (maxDate != null) {
            try {
                calendarBinding.robotoCalendarPicker.setEndDate(maxDate);
            } catch (Exception ignored) {
            }
        }

        if (minDate != null) {
            try {
                calendarBinding.robotoCalendarPicker.setDate(minDate, sdf_full.parse(date_today));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


    }

    private void openAgentQuotaDateDialog() {

        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
            popupModificationDialog.dismiss();
        }

        popupModificationDialog = new PopupWindow();
        popupModificationDialog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        if (displaymetrics != null) {
            popupModificationDialog.setWidth(displaymetrics.widthPixels / 4);
        } else {
            popupModificationDialog.setWidth(250);
        }


        // popup.setWidth(200);
        popupModificationDialog.setOutsideTouchable(true);
        popupModificationDialog.setFocusable(true);
        // popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        final DialogCustomCalendarBinding calendarBinding = DialogCustomCalendarBinding.inflate(getLayoutInflater());
        View view = calendarBinding.getRoot();


        popupModificationDialog.setContentView(view);
        //popupModificationDialog.showAsDropDown(btnSeat,0,0, Gravity.BOTTOM);
        popupModificationDialog.showAsDropDown(binding.relAgentQuota);


        calendarBinding.robotoCalendarPicker.setRobotoCalendarListener(new CustomCalendarView.RobotoCalendarListener() {
            @Override
            public void onDayClick(Date date) {


                String valid_until = sdf_full.format(date);
                Date strDate = null;
                try {
                    strDate = sdf_full.parse(valid_until);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (reqDate.getTime() > strDate.getTime()) {
                    String SettleDateString = sdf_full.format(reqDate);
                    showToast("Can't Select Less Than " + SettleDateString);
                } else {
                    if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                        popupModificationDialog.dismiss();
                    }
                    String FormatDate = sdf_full.format(date);
                    binding.txtAgentquotaDate.setText(FormatDate);
                }
            }

            @Override
            public void onDayLongClick(Date date) {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onLeftButtonClick() {

            }
        });
        calendarBinding.robotoCalendarPicker.setShortWeekDays(false);
        calendarBinding.robotoCalendarPicker.showDateTitle(true);
        try {
            calendarBinding.robotoCalendarPicker.setEndDate(sdf_full.parse(listDates.get(listDates.size() - 1)));
        } catch (Exception ignored) {
        }

        calendarBinding.robotoCalendarPicker.setDate(reqDate, reqDate);

    }

    private void InsertAgentQuotaBookingCancleApiCall(int trans_type) {

        String paxName;

        if (trans_type == 1) {
            if (OS_AllowToFetchAllPaxDetail == 0) {
                paxName = binding.etPaxName.getText().toString().trim();
            } else {
                paxName = selSeats.get(0).getJMPassengerName().trim();
            }

            pickUpId = listPickUp.get(binding.spnPickup.getSelectedItemPosition()).getPSMPickupID();
            AM_AgentID = itsGetAllAgentByCompanyIDS.get(binding.spnAgentName.getSelectedItemPosition()).getAM_AgentID();
            ABI_AgentCityID = itsGetAllAgentCityByCompanyIDS.get(binding.spnAgentCity.getSelectedItemPosition()).getCM_CityId();
        } else {
            FetchDetailsByPNRNO_Response.Datum data = listFetchDetailsPNR.get(0);
            paxName = TextUtils.isEmpty(data.getJMPassengerName()) ? data.getJPPassengerName() : data.getJMPassengerName();
            pickUpId = data.getPMPickupID() == null ? 0 : data.getPMPickupID();
            AM_AgentID = data.getAMAgentID() == null ? 0 : data.getAMAgentID();
            ABI_AgentCityID = data.getABIAgentCityID();
        }


        showProgressDialog("Loading...");

        InsertAgentQuotaBookingCancle_Request request = new InsertAgentQuotaBookingCancle_Request(
                bookedByCmpId,
                branchId,
                getPref.getBUM_BranchUserID(),
                routeId,
                routeTimeId,
                str_journeyDate,
                binding.txtAgentquotaDate.getText().toString(),
                ABI_AgentCityID,
                AM_AgentID,
                seatNameList,
                sourceCityId,
                destCityId,
                paxName,
                trans_type,
                binding.chkAutoInsertQuota.isChecked() ? 1 : 0,
                pickUpId,
                getPref.getTBRM_ID()
        );

        //Log.e("req agent quo",new Gson().toJson(request));

        Call<InsertAgentQuotaBookingCancle_Response> call = apiService.InsertAgentQuotaBookingCancle(request);
        call.enqueue(new Callback<InsertAgentQuotaBookingCancle_Response>() {
            @Override
            public void onResponse(@NotNull Call<InsertAgentQuotaBookingCancle_Response> call, @NotNull Response<InsertAgentQuotaBookingCancle_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body().getStatus() == 1) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        if (response.body().getData().get(0).getBookingStatus().equals("1")) {
                            if (trans_type == 1) {
                                if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                                    saveLastTransactionDetails(seatNameList, "", "", "MODIFY");
                                    showSnackBar(success, "Seat successfully updated", Snackbar.LENGTH_LONG, 2);
                                } else {
                                    saveLastTransactionDetails(seatNameList, "", "", "INSERT");
                                    showSnackBar(success, "Seat successfully booked", Snackbar.LENGTH_LONG, 2);
                                }
                            } else {
                                saveLastTransactionDetails(seatNameList, "", "", "CANCEL");
                                showSnackBar(success, "Seat successfully cancelled", Snackbar.LENGTH_LONG, 2);
                            }
                            resetSeatArrangement();
                        } else {
                            showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "InsertAgentQuotaBookingCancle", oops, response.body().getData().get(0).getStatus());
                        }

                    } else {
                        showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "InsertAgentQuotaBookingCancle", oops, response.body().getMessage());
                    }
                } else {
                    showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "InsertAgentQuotaBookingCancle", oops, getResources().getString(R.string.bad_server_response));
                }

            }

            @Override
            public void onFailure(@NotNull Call<InsertAgentQuotaBookingCancle_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "InsertAgentQuotaBookingCancle", oops, getResources().getString(R.string.bad_server_response));
            }
        });
    }

    private void nonReportedApiCall() {

        String pnrNo = listReqParameters.size() > 0 ? listReqParameters.get(0) : "";
        String seatList = listReqParameters.size() > 1 ? listReqParameters.get(1) : "";

        showProgressDialog("Loading...");

        NonReported_Request request = new NonReported_Request(
                loginCmpId,
                bookedByCmpId,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                pnrNo,
                str_journeyDate,
                routeId,
                routeTimeId,
                seatList,
                JM_PCRegistrationID,
                getPref.getAdminUserId(),
                dialogLoadTransactions.getResendOTPText()
        );

        Call<NonReported_Response> call = apiService.NonReported(request);
        call.enqueue(new Callback<NonReported_Response>() {
            @Override
            public void onResponse(@NotNull Call<NonReported_Response> call, @NotNull Response<NonReported_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() == 1 && response.body().getData() != null && response.body().getData().size() > 0) {
                    showSnackBar(success, response.body().getData().get(0).getStatus(), Snackbar.LENGTH_LONG, 2);
                    resetSeatArrangement();
                } else {
                    assert response.body() != null;
                    showSnackBar(oops, response.body().getMessage(), Snackbar.LENGTH_LONG, 1);
                }
            }

            @Override
            public void onFailure(@NotNull Call<NonReported_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "NonReported", oops, getResources().getString(R.string.bad_server_response));
            }
        });
    }

    private void reportedApiCall(String PNRNo, String seats) {

        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        try {
            dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            dialogLoadTransactions.show();
            dialogLoadTransactions.setTitleText("Seat Reported");
            dialogLoadTransactions.setContentText("Are You Sure , You Want To Set Seats [ " + seats + " ] As a Reported?");
            dialogLoadTransactions.setCancelable(false);
            dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
            dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));


            dialogLoadTransactions.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                    showProgressDialog("Loading...");

                    Reported_Request request = new Reported_Request(
                            loginCmpId,
                            bookedByCmpId,
                            getPref.getBM_BranchID(),
                            getPref.getBUM_BranchUserID(),
                            PNRNo,
                            str_journeyDate,
                            routeId,
                            routeTimeId,
                            seats,
                            JM_PCRegistrationID,
                            getPref.getAdminUserId()
                    );

                    Call<Reported_Response> call = apiService.Reported(request);
                    call.enqueue(new Callback<Reported_Response>() {
                        @Override
                        public void onResponse(@NotNull Call<Reported_Response> call, @NotNull Response<Reported_Response> response) {
                            disMissDialog();
                            if (response.isSuccessful() && response.body().getStatus() == 1) {
                                showSnackBar(success, "Seat successfully reported", Snackbar.LENGTH_LONG, 2);
                                resetSeatArrangement();
                            } else {
                                showSnackBar(oops, response.body().getMessage(), Snackbar.LENGTH_LONG, 1);
                            }
                        }

                        @Override
                        public void onFailure(@NotNull Call<Reported_Response> call, @NotNull Throwable t) {
                            disMissDialog();
                            showInternetConnectionDialog(APP_CONSTANTS.DIALOG_TYPE_ERROR, "Reported", oops, getResources().getString(R.string.bad_server_response));
                        }
                    });

                }
            });

            dialogLoadTransactions.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                }
            });
        } catch (Exception ignored) {
        }
    }

    private void sendSmsToPNRDialog(int smsType, String mobileNo, String pnrNo) {
        // smsType-1 ,

        String dilaogTitle = "Send SMS", dialogDescription;
        if (smsType == 1) {
            dialogDescription = "Are You Sure , You Want To Send Bus Schedule And PickUp Man SMS?";
        } else {
            dialogDescription = "Are You Sure , You Want To Send Payment Link SMS?";
        }

        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        try {
            dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            dialogLoadTransactions.show();
            try {
                dialogLoadTransactions.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            } catch (Exception ignored) {
            }
            dialogLoadTransactions.setTitleText(dilaogTitle);
            dialogLoadTransactions.setContentText(dialogDescription);
            dialogLoadTransactions.setCancelable(false);
            dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
            dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));

            dialogLoadTransactions.showMobileNoEditText(true, mobileNo);

            dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {
                dialogLoadTransactions.dismissWithAnimation();
                dialogLoadTransactions.cancel();

                if (smsType == 1) {
                    showProgressDialog("Loading...");

                    SendSMS_Request request = new SendSMS_Request(
                            bookedByCmpId,
                            branchId,
                            branchUserId,
                            pnrNo,
                            1
                    );

                    Call<SendSMS_Response> call = apiService.SendSMS(request);
                    call.enqueue(new Callback<SendSMS_Response>() {
                        @Override
                        public void onResponse(@NotNull Call<SendSMS_Response> call, @NotNull Response<SendSMS_Response> response) {
                            disMissDialog();
                            if (response.isSuccessful() && response.body().getStatus() == 1) {
                                if (response.body().getData() != null && response.body().getData().size() > 0) {
                                    showSnackBar(success, getResources().getString(R.string.send_sms_success), Snackbar.LENGTH_LONG, 2);
                                } else {
                                    showSnackBar(oops, APP_CONSTANTS.NO_DATA, Snackbar.LENGTH_LONG, 1);
                                }
                            } else {
                                showSnackBar(oops, getResources().getString(R.string.bad_server_response), Snackbar.LENGTH_LONG, 1);
                            }
                        }

                        @Override
                        public void onFailure(@NotNull Call<SendSMS_Response> call, @NotNull Throwable t) {
                            disMissDialog();
                            showSnackBar(oops, getResources().getString(R.string.bad_server_request), Snackbar.LENGTH_LONG, 1);
                        }
                    });
                } else {

                }
            });

            dialogLoadTransactions.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                }
            });
        } catch (Exception ignored) {
        }
    }

    private void openChartSMSDialog() {

        String[] arrayHours, arrayMins, arrayAmPm;

        if (dialogChartSms != null && dialogChartSms.isShowing()) {
            dialogChartSms.dismiss();
        }

        dialogChartSms = new Dialog(ExeBookingActivity_V2.this);

        DialogChartSmsBinding chartSmsBinding = DialogChartSmsBinding.inflate(getLayoutInflater());
        final View view = chartSmsBinding.getRoot();

        chartSmsBinding.txtRoute.setText(binding.txtRouteName.getText().toString());

        chartSmsBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogChartSms != null && dialogChartSms.isShowing()) {
                    dialogChartSms.dismiss();
                }
            }
        });

        chartSmsBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogChartSms != null && dialogChartSms.isShowing()) {
                    dialogChartSms.dismiss();
                }
            }
        });

        chartSmsBinding.rgRouteLate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                dispSmsRouteChartDetails(chartSmsBinding);

                if (!isChartSmsRadioChecked && chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() != -1) {
                    isChartSmsRadioChecked = true;
                    chartSmsBinding.rgPickupChange.clearCheck();
                    isChartSmsRadioChecked = false;
                }
            }
        });

        chartSmsBinding.rgPickupChange.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (!isChartSmsRadioChecked && chartSmsBinding.rgPickupChange.getCheckedRadioButtonId() != -1) {
                    isChartSmsRadioChecked = true;
                    chartSmsBinding.rgRouteLate.clearCheck();
                    isChartSmsRadioChecked = false;
                    if (chartSmsBinding.rgPickupChange.getCheckedRadioButtonId() != chartSmsBinding.rbAddressSms.getId()) {
                        chartSmsBinding.llSelectedCity.setVisibility(View.VISIBLE);
                    } else {
                        chartSmsBinding.llSelectedCity.setVisibility(View.GONE);
                    }
                    chartSmsBinding.txtSmsInfo.setVisibility(View.GONE);
                    if (chartSmsBinding.rgPickupChange.getCheckedRadioButtonId() == chartSmsBinding.rbPickupChange.getId()) {
                        chartSmsBinding.llOldPickup.setVisibility(View.VISIBLE);
                        chartSmsBinding.llNewPickup.setVisibility(View.VISIBLE);
                        chartSmsBinding.rlAmPm.setVisibility(View.VISIBLE);
                        chartSmsBinding.chkSendMail.setVisibility(View.GONE);
                        chartSmsBinding.llRemarks.setVisibility(View.GONE);
                    } else {
                        chartSmsBinding.llOldPickup.setVisibility(View.GONE);
                        chartSmsBinding.llNewPickup.setVisibility(View.GONE);
                        chartSmsBinding.rlAmPm.setVisibility(View.GONE);
                        chartSmsBinding.llSelectedCity.setVisibility(View.GONE);

                        chartSmsBinding.chkSendMail.setVisibility(View.VISIBLE);
                        chartSmsBinding.llRemarks.setVisibility(View.VISIBLE);
                    }

                    if (chartSmsBinding.rgPickupChange.getCheckedRadioButtonId() == chartSmsBinding.rbBustypeChange.getId()) {
                        chartSmsBinding.llBusType.setVisibility(View.VISIBLE);
                        chartSmsBinding.llBusCategory.setVisibility(View.VISIBLE);

                        chartSmsBinding.llTime.setVisibility(View.GONE);
                        chartSmsBinding.llRemarks.setVisibility(View.GONE);
                        chartSmsBinding.chkSendMail.setVisibility(View.GONE);
                    } else {
                        chartSmsBinding.llTime.setVisibility(View.VISIBLE);
                        chartSmsBinding.llBusType.setVisibility(View.GONE);
                        chartSmsBinding.llBusCategory.setVisibility(View.GONE);
                    }

                }
            }
        });

        chartSmsBinding.spnHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if (chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() != -1 && pos > 0) {
                    dispSmsRouteChartDetails(chartSmsBinding);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        chartSmsBinding.spnMin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if (chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() != -1 && pos > 0) {
                    dispSmsRouteChartDetails(chartSmsBinding);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        // Hour Spinner Binding
        arrayHours = new String[13];
        for (int i = 0; i <= 12; i++) {
            if (i < 10) {
                arrayHours[i] = "0" + i;
            } else {
                arrayHours[i] = String.valueOf(i);
            }
        }
        chartSmsBinding.spnHour.setAdapter(new ArrayAdapter<String>(ExeBookingActivity_V2.this, R.layout.support_simple_spinner_dropdown_item, arrayHours));


        // Minute Spinner Binding
        arrayMins = new String[60];
        for (int j = 0; j < 60; j++) {
            if (j < 10) {
                arrayMins[j] = "0" + j;
            } else {
                arrayMins[j] = String.valueOf(j);
            }
        }
        chartSmsBinding.spnMin.setAdapter(new ArrayAdapter<String>(ExeBookingActivity_V2.this, R.layout.support_simple_spinner_dropdown_item, arrayMins));

        // AM PM Spinner Binding
        arrayAmPm = new String[]{"AM", "PM"};
        chartSmsBinding.spnAmPm.setAdapter(new ArrayAdapter<String>(ExeBookingActivity_V2.this, R.layout.support_simple_spinner_dropdown_item, arrayAmPm));


        /*try{
            Animation animation =
                    AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.seat_blink);
            chartSmsBinding.txtSmsInfo.startAnimation(animation);
        }catch (Exception ex){}*/


        dialogChartSms.setContentView(view);
        dialogChartSms.setCancelable(false);
        if (!isFinishing())
            dialogChartSms.show();

        try {
            dialogChartSms.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        } catch (Exception ex) {
        }


        /*FrameLayout bottomSheet = (FrameLayout) bottomBookTicketDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);*/
    }

    private void dispSmsRouteChartDetails(DialogChartSmsBinding chartSmsBinding) {
        chartSmsBinding.llBusType.setVisibility(View.GONE);
        chartSmsBinding.llBusCategory.setVisibility(View.GONE);

        chartSmsBinding.llTime.setVisibility(View.VISIBLE);
        chartSmsBinding.chkSendMail.setVisibility(View.VISIBLE);
        chartSmsBinding.llRemarks.setVisibility(View.VISIBLE);
        chartSmsBinding.llOldPickup.setVisibility(View.GONE);
        chartSmsBinding.llNewPickup.setVisibility(View.GONE);

        StringBuilder routeDetails = new StringBuilder();
        routeDetails.append(sel_SourceCityName);
        routeDetails.append(" To ");
        routeDetails.append(sel_DestinationCityName);

        if (chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() == chartSmsBinding.rbRouteLateBy.getId()) {
            routeDetails.append(" Is Late By ");
        } else if (chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() == chartSmsBinding.rbRouteEarlyBy.getId()) {
            routeDetails.append(" Is Early By ");
        } else if (chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() == chartSmsBinding.rbRouteCancelled.getId()) {
            routeDetails.append(" Is Cancelled");
        }

        if (chartSmsBinding.rgRouteLate.getCheckedRadioButtonId() != chartSmsBinding.rbRouteCancelled.getId()) {
            if (chartSmsBinding.spnHour.getSelectedItemPosition() > 0) {
                routeDetails.append(chartSmsBinding.spnHour.getSelectedItem().toString() + " Hours");
                if (chartSmsBinding.spnMin.getSelectedItemPosition() > 0) {
                    routeDetails.append(" And " + chartSmsBinding.spnMin.getSelectedItem().toString() + " Minutes");
                }
            } else {
                if (chartSmsBinding.spnMin.getSelectedItemPosition() > 0) {
                    routeDetails.append(chartSmsBinding.spnMin.getSelectedItem().toString() + " Minutes");
                }
            }
        }
        chartSmsBinding.llSelectedCity.setVisibility(View.VISIBLE);
        chartSmsBinding.txtSmsInfo.setVisibility(View.VISIBLE);
        chartSmsBinding.rlAmPm.setVisibility(View.GONE);
        chartSmsBinding.txtSmsInfo.setText(routeDetails.toString());

    }

    private void openToConfirmDialog(String pnrNo) {
        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        try {
            dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            dialogLoadTransactions.show();
            dialogLoadTransactions.setTitleText("Open To Confirm");
            dialogLoadTransactions.setContentText("Are You Sure , You Want To Make PNR No # " + pnrNo + " To OPEN?");
            dialogLoadTransactions.setCancelable(false);
            dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
            dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));


            dialogLoadTransactions.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();
                }
            });

            dialogLoadTransactions.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();

                }
            });
        } catch (Exception ignored) {
        }
    }

    private void openSeatPermanentHoldDialog() {
        StringBuilder routeBuilder = new StringBuilder();
        if (!TextUtils.isEmpty(reqBusNo)) {
            routeBuilder.append(reqBusNo);
            routeBuilder.append(" || ");
        }
        routeBuilder.append(binding.txtRouteName.getText().toString());
        routeBuilder.append(" || ");
        routeBuilder.append(str_journeyDate);

        if (bookingTypeList != null && bookingTypeList.size() > 0) {
            for (int i = 0; i < bookingTypeList.size(); i++) {
                if (bookingTypeList.get(i).getBookingTypeId() == APP_CONSTANTS.EMPTY_SEAT_BOOKING) {
                    emptySeatColor = bookingTypeList.get(i).getColor();
                    break;
                }
            }
        }

        Intent intent = new Intent(ExeBookingActivity_V2.this, SeatHoldActivity.class);

        intent.putExtra("CompanyId", loginCmpId);
        intent.putExtra("RouteId", routeId);
        intent.putExtra("RouteTimeId", routeTimeId);
        intent.putExtra("FromCityId", sourceCityId);
        intent.putExtra("ToCityId", destCityId);
        intent.putExtra("StartTime", jStartTime);
        intent.putExtra("CityTime", jCityTime);
        intent.putExtra("JourneyDate", str_journeyDate);
        intent.putExtra("IsSameDay", IsSameDay);
        intent.putExtra("EmptySeatColor", emptySeatColor);
        intent.putExtra("RouteDetails", routeBuilder.toString());
        startActivityForResult(intent, 2);

    }


    private void resetSeatArrangement() {
        if (sourceCityId > 0 && destCityId > 0 && routeId > 0 && routeTimeId > 0) {

            clearAfterInsertBooking();
            clearModificationSeats();

            totalConsecutiveApiCall = 2;

            showProgressDialog(getResources().getString(R.string.loading));
            Handler mHand = new Handler(Looper.getMainLooper());

            mHand.postDelayed(new Runnable() {
                @Override
                public void run() {
                    createSeatArrangementRequest();
                    pickUpDropRequest();
                }
            }, 500);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == 1) {
            resetSeatArrangement();
        } else if (requestCode == REQUEST_CODE_PERMISSION) {
            Message message = new Message();
            if (Environment.isExternalStorageManager()) {
                message.what = APP_CONSTANTS.STATUS_SUCCESS;
            } else {
                message.what = APP_CONSTANTS.STATUS_FAILED;
            }
            downloadInvoiceCallback.handleMessage(message);
        }
    }

    private void openETicketDialog(int eTicketFlag) {
        if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }
        String reqPNRNo, reqSeatNo, reqCustEmail, reqCompanyName;
        int reqCompanyId;

        FetchDetailsByPNRNO_Response.Datum data = listFetchDetailsPNR.get(0);
        reqPNRNo = data.getJMPNRNO() != null ? String.valueOf(data.getJMPNRNO()) : "";
        reqCompanyId = data.getJMBookedByCMCompanyID() != null ? data.getJMBookedByCMCompanyID() : 0;
        reqCompanyName = "";
        reqCustEmail = data.getJMEmailID();

        String title = "";
        StringBuilder description = new StringBuilder().append("Are You Sure , You Want To ");
        if (eTicketFlag == APP_CONSTANTS.SEND_TICKET) {
            title = getString(R.string.e_send_ticket);
        } else if (eTicketFlag == APP_CONSTANTS.SEND_INVOICE) {
            title = getString(R.string.e_send_gst_invoice);
        } else if (eTicketFlag == APP_CONSTANTS.DOWNLOAD_TICKET) {
            title = getString(R.string.download_e_ticket);
        } else if (eTicketFlag == APP_CONSTANTS.DOWNLOAD_INVOICE) {
            title = getString(R.string.download_gst_invoice);
        }
        description.append(title);
        description.append("?");

        try {
            dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            dialogLoadTransactions.show();

            dialogLoadTransactions.setTitleText(title);
            dialogLoadTransactions.setContentText(description.toString());
            dialogLoadTransactions.setCancelable(false);
            dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
            dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));


            if (eTicketFlag == APP_CONSTANTS.SEND_TICKET || eTicketFlag == APP_CONSTANTS.SEND_INVOICE) {
                dialogLoadTransactions.showEmailEditText(true, reqCustEmail);
            }

            try {
                dialogLoadTransactions.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            } catch (Exception ex) {
            }

            dialogLoadTransactions.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    String reqCustEmail = dialogLoadTransactions.getResendOTPText();
                    if (eTicketFlag == APP_CONSTANTS.SEND_TICKET || eTicketFlag == APP_CONSTANTS.SEND_INVOICE) {
                        boolean isValid = false;
                        if (TextUtils.isEmpty(reqCustEmail)) {
                            CustomToast.showToastMessage(ExeBookingActivity_V2.this, validEmail);
                        } else if (!reqCustEmail.matches("[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.+[a-z.A-Z0-9]+")) {
                            CustomToast.showToastMessage(ExeBookingActivity_V2.this, validEmail);
                        } else {
                            try {
                                dialogLoadTransactions.dismissWithAnimation();
                                dialogLoadTransactions.cancel();
                            } catch (Exception ignored) {
                            }
                            if (eTicketFlag == APP_CONSTANTS.SEND_TICKET) {
                                eSendTicketApiCall(reqPNRNo, reqCompanyId, reqCustEmail);
                            } else {
                                eSendGSTInvoiceApiCall(reqPNRNo, reqCompanyId, "", reqCustEmail);
                            }
                        }
                    } else {
                        try {
                            dialogLoadTransactions.dismissWithAnimation();
                            dialogLoadTransactions.cancel();
                        } catch (Exception ignored) {
                        }
                        downloadInvoiceCallback = new Handler.Callback() {
                            @Override
                            public boolean handleMessage(@NonNull Message message) {
                                if (message.what == APP_CONSTANTS.STATUS_SUCCESS) {
                                    if (eTicketFlag == APP_CONSTANTS.DOWNLOAD_TICKET) {
                                        ticketPrintPDFApiCall(
                                                reqPNRNo,
                                                data.getJMJourneyFrom(),
                                                data.getRMRouteID(),
                                                data.getRTTime(),
                                                data.getBAMArrangementID(),
                                                data.getPMPickupID(),
                                                12,
                                                data.getJMBookedByCMCompanyID()
                                        );
                                    } else if (eTicketFlag == APP_CONSTANTS.DOWNLOAD_INVOICE) {
                                        printGSTInvoiceApiCall(reqPNRNo, reqCompanyId);
                                    }
                                } else {
                                    permissionAlert();
                                }
                                return false;
                            }
                        };
                        checkPermissions();
                    }
                }
            });

            dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                dialogLoadTransactions.dismissWithAnimation();
                dialogLoadTransactions.cancel();

            });
        } catch (Exception ignored) {
        }
    }


    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasPermissions(ExeBookingActivity_V2.this, RunTimePerMissions)) {
            ActivityCompat.requestPermissions(ExeBookingActivity_V2.this, RunTimePerMissions, REQUEST_CODE_PERMISSION);
            /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                try {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                    intent.addCategory("android.intent.category.DEFAULT");
                    intent.setData(Uri.parse(String.format("package:%s", getApplicationContext().getPackageName())));
                    startActivityForResult(intent, REQUEST_CODE_PERMISSION);
                } catch (Exception e) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    startActivityForResult(intent, REQUEST_CODE_PERMISSION);
                }
            } else {
                ActivityCompat.requestPermissions(ExeBookingActivity_V2.this, RunTimePerMissions, REQUEST_CODE_PERMISSION);
            }*/
        } else {
            Message message = new Message();
            message.what = APP_CONSTANTS.STATUS_SUCCESS;
            downloadInvoiceCallback.handleMessage(message);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION) {
            Message message = new Message();
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                message.what = APP_CONSTANTS.STATUS_SUCCESS;
            } else {
                message.what = APP_CONSTANTS.STATUS_FAILED;
            }
            downloadInvoiceCallback.handleMessage(message);
        }
    }

    private void permissionAlert() {
        if (!isFinishing()) {
            new AlertDialog.Builder(ExeBookingActivity_V2.this)
                    .setTitle("Storage Permission Required")
                    .setMessage("You Need To Grant Permission For Download Ticket / Invoice")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                    }).show();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && context != null && permissions != null) {
            return Environment.isExternalStorageManager();
        } else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }*/

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void downloadPDf(final String FileName, String pdfURL, int flag) {
        // 1-> Ticket , 2-> Invoice
        downloadFile = null;
        String title = flag == 1 ? "Ticket" : "GST Invoice";
        try {
            StringBuilder invoicePathBuilder = new StringBuilder();
            invoicePathBuilder.append("/");
            invoicePathBuilder.append("ITSExe");
            invoicePathBuilder.append("/");
            invoicePathBuilder.append(title);
            invoicePathBuilder.append("/");
            invoicePathBuilder.append(FileName);
            invoicePathBuilder.append(".pdf");


            final File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + invoicePathBuilder.toString());

//            final File file = new File(MediaStore.Downloads.EXTERNAL_CONTENT_URI + invoicePathBuilder.toString());

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(pdfURL));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationUri(Uri.fromFile(file));
            request.setDescription("Downloading..." + title);

            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            if (dm != null) {
                dm.enqueue(request);
            }

            if (file != null) {
                downloadFile = file;
                onCompleteDownload = new BroadcastReceiver() {
                    public void onReceive(Context ctxt, Intent intent) {
                        if (ctxt != null) {
                            openCommonWarningDilaog(APP_CONSTANTS.OPEN_DOWNLOAD_FILE, getResources().getString(R.string.dialog_success_title), title + " Download Complete");
                        }

                    }
                };
                registerReceiver(onCompleteDownload, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                showSnackBar(title, getResources().getString(R.string.downloading), Toast.LENGTH_LONG, 0);
            }

        } catch (Exception ignored) {
            showSnackBar(oops, getResources().getString(R.string.unable_download) + title, Toast.LENGTH_LONG, 1);
        }
    }

    private void prevChart() {
        if (routeId > 0 && binding.spnRouteTime.getCount() > 0 && binding.spnRouteTime.getSelectedItemPosition() > 0) {
            binding.spnRouteTime.setSelection(binding.spnRouteTime.getSelectedItemPosition() - 1);
        }
    }

    private void nextChart() {
        if (routeId > 0 && binding.spnRouteTime.getCount() > 0 && binding.spnRouteTime.getSelectedItemPosition() >= 0 && binding.spnRouteTime.getSelectedItemPosition() != (binding.spnRouteTime.getCount() - 1)) {
            binding.spnRouteTime.setSelection(binding.spnRouteTime.getSelectedItemPosition() + 1);
        }
    }

    private void prevNextChartValidation() {
        binding.imgMiddleCityChart.setVisibility(View.VISIBLE);
        if (binding.spnRouteTime.getCount() > 0 && binding.spnRouteTime.getSelectedItemPosition() > 0) {
            binding.imgPrevChart.setVisibility(View.VISIBLE);
        } else {
            binding.imgPrevChart.setVisibility(View.GONE);
        }

        if (binding.spnRouteTime.getCount() > 0 && binding.spnRouteTime.getSelectedItemPosition() != (binding.spnRouteTime.getCount() - 1)) {
            binding.rlAmenities.setVisibility(View.VISIBLE);
            binding.imgNextChart.setVisibility(View.VISIBLE);
        } else {
            if (binding.txtAmenities.getVisibility() == View.INVISIBLE && binding.imgAmenitiesClose.getVisibility() == View.INVISIBLE) {
                binding.rlAmenities.setVisibility(View.GONE);
            }
            binding.imgNextChart.setVisibility(View.INVISIBLE);
        }
    }

    private void seatBlockUnblockApiCall() {

        String tranStatus = listReqParameters.size() > 0 ? listReqParameters.get(0) : "";
        String seatList = listReqParameters.size() > 1 ? listReqParameters.get(1) : "";

        SeatHold_Request request = new SeatHold_Request(
                bookedByCmpId,
                str_journeyDate,
                routeId,
                routeTimeId,
                sourceCityId,
                destCityId,
                jStartTime,
                jCityTime,
                branchId,
                branchUserId,
                "2",
                str_journeyDate,
                str_journeyDate,
                "",
                tranStatus,
                seatList,
                ""
        );

        Call<SeatHold_Response> call = apiService.SeatHold(request);
        call.enqueue(new Callback<SeatHold_Response>() {
            @Override
            public void onResponse(@NonNull Call<SeatHold_Response> call, @NonNull Response<SeatHold_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1 && response.body().getData() != null && response.body().getData().size() > 0 && response.body().getData().get(0).getStatus() != null && response.body().getData().get(0).getStatus() == 1) {
                        showSnackBar(success, response.body().getData().get(0).getStatusMsg(), Toast.LENGTH_LONG, 2);
                        resetSeatArrangement();
                    } else {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            showSnackBar(oops, response.body().getData().get(0).getStatusMsg(), Toast.LENGTH_LONG, 1);
                        } else {
                            showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                        }
                    }
                } else {
                    showSnackBar(oops, getResources().getString(R.string.bad_server_request), Toast.LENGTH_LONG, 1);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SeatHold_Response> call, @NonNull Throwable t) {
                disMissDialog();
                showSnackBar(oops, getResources().getString(R.string.bad_server_response), Toast.LENGTH_LONG, 1);
            }
        });
    }


    private void setAmenities() {
        binding.rlAmenities.setVisibility(View.VISIBLE);
        //binding.imgAmenitiesClose.setVisibility(View.VISIBLE);
        binding.txtAmenities.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(binding.txtAmenities.getText().toString())) {
            int amenitiesLen = (int) binding.txtAmenities.getPaint().measureText(binding.txtAmenities.getText().toString());
            int deviceLength = (displaymetrics.widthPixels / 3) + 20;
            String amenitiesMarqueeData = binding.txtAmenities.getText().toString(), leftSpace = "", rightSpace = "";
            if (amenitiesLen < deviceLength) {
                for (int i = 0; i < (deviceLength - (int) amenitiesLen); i++) {
                    leftSpace += getResources().getString(R.string.white_space_text);
                    rightSpace += getResources().getString(R.string.white_space_text);
                    amenitiesLen = (int) binding.txtAmenities.getPaint().measureText(leftSpace + amenitiesMarqueeData + rightSpace);
                }
            }
            amenitiesMarqueeData = leftSpace + amenitiesMarqueeData + rightSpace;
            binding.txtAmenities.setText(amenitiesMarqueeData);
            binding.txtAmenities.setSelected(true);
        }
        binding.imgAmenitiesClose.setOnClickListener(view1 ->
                {
                    binding.imgAmenitiesClose.setVisibility(View.INVISIBLE);
                    binding.txtAmenities.setVisibility(View.INVISIBLE);
                    if (binding.imgNextChart.getVisibility() == View.INVISIBLE) {
                        binding.rlAmenities.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void openDriverAndPickupManDetailsDialog() {
        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
            popupModificationDialog.dismiss();
        }

        popupModificationDialog = new PopupWindow();
        popupModificationDialog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        if (displaymetrics != null) {
            popupModificationDialog.setWidth(displaymetrics.widthPixels / 4);
        } else {
            popupModificationDialog.setWidth(250);
        }

        // popup.setWidth(200);
        popupModificationDialog.setOutsideTouchable(true);
        popupModificationDialog.setFocusable(true);
        // popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        final DialogDriverPickupmanDetailsBinding driverDetailsBinding = DialogDriverPickupmanDetailsBinding.inflate(getLayoutInflater());
        View view = driverDetailsBinding.getRoot();

        if (listSeatDetails != null && listSeatDetails.size() > 0) {

            driverDetailsBinding.txtDriverName.setText(listSeatDetails.get(0).getDMDriver1());
            driverDetailsBinding.txtDriverPhone.setText(listSeatDetails.get(0).getDMPhoneNo());

            driverDetailsBinding.txtDriverPhone.setOnClickListener(view1 -> {
                if (!TextUtils.isEmpty(driverDetailsBinding.txtDriverPhone.getText().toString())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + driverDetailsBinding.txtDriverPhone.getText().toString()));
                    startActivity(intent);
                }
            });

            driverDetailsBinding.txtPickupMan.setText(listSeatDetails.get(0).getPickupManName());
//            driverDetailsBinding.txtPickupManPhone.setText(listSeatDetails.get(0).getph());

            driverDetailsBinding.txtPickupManPhone.setOnClickListener(view1 -> {
                if (!TextUtils.isEmpty(driverDetailsBinding.txtPickupManPhone.getText().toString())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + driverDetailsBinding.txtPickupManPhone.getText().toString()));
                    startActivity(intent);
                }
            });

        }


        popupModificationDialog.setContentView(view);
        //popupModificationDialog.showAsDropDown(btnSeat,0,0, Gravity.BOTTOM);
        popupModificationDialog.showAsDropDown(binding.txtDriverPickup);
    }

    private void openCommonWarningDilaog(int trnsType, String dialogTitle, String dialogDesc) {
        // Trans Type => 1-> Seat Block , 2 -> Non Reported , 3 -> Open Download File


        if (!isFinishing() && dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {
            dialogLoadTransactions.dismiss();
        }

        try {
            if (trnsType == APP_CONSTANTS.OPEN_DOWNLOAD_FILE) {
                dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.SUCCESS_TYPE);
            } else {
                dialogLoadTransactions = new SweetAlertDialog(ExeBookingActivity_V2.this, SweetAlertDialog.WARNING_TYPE);
            }
            dialogLoadTransactions.show();
            dialogLoadTransactions.setTitleText(dialogTitle);
            dialogLoadTransactions.setContentText(dialogDesc);
            dialogLoadTransactions.setCancelable(false);
            if (trnsType == APP_CONSTANTS.OPEN_DOWNLOAD_FILE) {
                dialogLoadTransactions.setConfirmText("View");
                dialogLoadTransactions.setCancelText(getResources().getString(R.string.cancel));

                dialogLoadTransactions.setResentOTPText("Share");

            } else {
                dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
                dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));
            }


            dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {
                try {
                    if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                } catch (Exception ignored) {
                }

                dialogLoadTransactions.dismissWithAnimation();
                dialogLoadTransactions.cancel();
                switch (trnsType) {
                    case APP_CONSTANTS.SEAT_BLOCK_UNBLOCK:
                        seatBlockUnblockApiCall();
                        break;
                    case APP_CONSTANTS.NON_REPORTED:
                        nonReportedApiCall();
                        break;
                    case APP_CONSTANTS.OPEN_DOWNLOAD_FILE:
                        openDownloadFile(APP_CONSTANTS.FILE_VIEW, downloadFile);
                        break;
                }
            });

            dialogLoadTransactions.setResendOTPClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < APP_CONSTANTS.TIME_PREVENT_DOUBLE_CLICK) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                    } catch (Exception ignored) {
                    }

                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();
                    openDownloadFile(APP_CONSTANTS.FILE_SHARE, downloadFile);
                }
            });

            dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                dialogLoadTransactions.dismissWithAnimation();
                dialogLoadTransactions.cancel();
            });
        } catch (Exception ignored) {
        }
    }

    private void eSendTicketApiCall(String pnrNo, int cmpId, String custEmail) {
        showProgressDialog(getString(R.string.loading));

        ESendTicketRequest request = new ESendTicketRequest(
                pnrNo,
                cmpId,
                custEmail
        );

        Call<ESendTicketResponse> call = apiService.ESendTicket(request);
        call.enqueue(new Callback<ESendTicketResponse>() {
            @Override
            public void onResponse(@NotNull Call<ESendTicketResponse> call, @NotNull Response<ESendTicketResponse> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1 && !TextUtils.isEmpty(response.body().getData())) {
                        showSnackBar(success, getResources().getString(R.string.ticket_send_success) + custEmail, Toast.LENGTH_LONG, 2);
                        resetSearchModule();
                    } else {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    }
                } else {
                    showSnackBar(oops, getResources().getString(R.string.bad_server_request), Toast.LENGTH_LONG, 1);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ESendTicketResponse> call, @NotNull Throwable t) {
                disMissDialog();
                showSnackBar(oops, getResources().getString(R.string.bad_server_response), Toast.LENGTH_LONG, 1);
            }
        });
    }

    private void eSendGSTInvoiceApiCall(String pnrNo, int cmpId, String cmpName, String custEmail) {
        showProgressDialog(getString(R.string.loading));

        ESendGSTInvoice_Request request = new ESendGSTInvoice_Request(
                pnrNo,
                cmpId,
                cmpName,
                custEmail
        );

        Call<ESendGSTInvoice_Response> call = apiService.ESendGSTInvoice(request);
        call.enqueue(new Callback<ESendGSTInvoice_Response>() {
            @Override
            public void onResponse(@NotNull Call<ESendGSTInvoice_Response> call, @NotNull Response<ESendGSTInvoice_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() != null && response.body().getStatus() == 1) {
                        showSnackBar(success, getResources().getString(R.string.invoice_send_success) + custEmail, Toast.LENGTH_LONG, 2);
                        resetSearchModule();
                    } else {
                        showSnackBar(oops, response.body().getMessage(), Toast.LENGTH_LONG, 1);
                    }
                } else {
                    showSnackBar(oops, getResources().getString(R.string.bad_server_request), Toast.LENGTH_LONG, 1);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ESendGSTInvoice_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showSnackBar(oops, getResources().getString(R.string.bad_server_response), Toast.LENGTH_LONG, 1);
            }
        });
    }

    private void resetSearchModule() {
        binding.spnSearch.setSelection(0);
        binding.etSearchPnr.setText("");
    }

    private void showPNRDetailsDialog() {

        Dialog pnrDetailsDialog = new Dialog(ExeBookingActivity_V2.this);

        DialogPnrDetailsBinding pnrDetailsBinding = DialogPnrDetailsBinding.inflate(getLayoutInflater());
        final View view = pnrDetailsBinding.getRoot();

        pnrDetailsBinding.btnCancel.setOnClickListener(view1 -> pnrDetailsDialog.dismiss());


        pnrDetailsDialog.setCancelable(false);
        pnrDetailsDialog.setContentView(view);
        pnrDetailsDialog.setCancelable(false);
        if (!isFinishing())
            pnrDetailsDialog.show();
        try {
            pnrDetailsDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        } catch (Exception ignored) {
        }
    }

    private void openDownloadFile(int flag, File file) {

        if (!isFinishing() && onCompleteDownload != null) {
            unregisterReceiver(onCompleteDownload);
        }

        Uri fileUri = null;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fileUri = getUriForFile(ExeBookingActivity_V2.this, BuildConfig.APPLICATION_ID, file);
        } else {
            fileUri = Uri.fromFile(file);
        }

        if (flag == APP_CONSTANTS.FILE_VIEW) {
            if (fileUri != null) {
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setDataAndType(fileUri, getContentResolver().getType(fileUri));
                install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent intent = Intent.createChooser(install, "Open File");
                try {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showSnackBar(oops, "No Any Application Available In Your Device For View PDF Ticket / Invoice", Toast.LENGTH_LONG, 1);
                } catch (Exception ex) {
                    showSnackBar(oops, "Unable To Open File", Toast.LENGTH_LONG, 1);
                }
            }
        } else if (flag == APP_CONSTANTS.FILE_SHARE) {
            if (fileUri != null) {
                Intent install = new Intent(Intent.ACTION_SEND);
                install.setDataAndType(fileUri, getContentResolver().getType(fileUri));
                install.putExtra(Intent.EXTRA_TITLE, "Share File");
                install.putExtra(Intent.EXTRA_STREAM, fileUri);
                install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent intent = Intent.createChooser(install, "Share File");
                try {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showSnackBar(oops, "No Any Application Available In Your Device For Share PDF Ticket / Invoice", Toast.LENGTH_LONG, 1);
                } catch (Exception ignored) {
                    showSnackBar(oops, "Unable To Share File", Toast.LENGTH_LONG, 1);
                }
            }
        }

    }

    private void disableActivityScreen() {
        try {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (Exception ignored) {
        }
    }

    private void enableActivityScreen() {
        try {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (Exception ignored) {
        }
    }

    private void showToast(String msgDesc) {
        if (!isFinishing()) {
            showSnackBar(oops, msgDesc, Toast.LENGTH_LONG, 1);
        }
    }

    private boolean cancelBookingValidation() {
        boolean cancelBooking = false;
        switch (bookingTypeId) {
            case APP_CONSTANTS.CONFIRM_BOOKING:
                // Confirm

                break;
            case APP_CONSTANTS.PHONE_BOOKING:
                // Phone
                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRPhoneCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAPhoneCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRPhoneCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }
                break;
            case APP_CONSTANTS.AGENT_BOOKING:
                // Agent
                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRAgentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAAgentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRAgentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }

                break;
            case APP_CONSTANTS.BRANCH_BOOKING:
                // Branch
                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRBranchCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOABranchCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRBranchCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }

                break;

            case APP_CONSTANTS.GUEST_BOOKING:
                // Guest

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRGuestCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAGuestCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRGuestCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }
                break;

            case APP_CONSTANTS.BANK_CARD_BOOKING:
                // BankCard
                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRBankCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOABankCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRBankCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }

                break;

            case APP_CONSTANTS.CMP_CARD_BOOKING:
                // CompanyCard

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRCompanyCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOACompanyCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRCompanyCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }
                break;

            case APP_CONSTANTS.WAITING_BOOKING:
                // Waiting

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRWaitingCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAWaitingCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRWaitingCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }

                break;

            case APP_CONSTANTS.AGENT_QUOTA_BOOKING:
                // AgentQuota Booking

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRAgentQuataCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAAgentQuataCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRAgentQuotaCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }

                break;

            case APP_CONSTANTS.OPEN_BOOKING:
                // Open Booking

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBROpenCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAOpenCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        break;
                }
                break;

            case APP_CONSTANTS.AGENT_PHONE_BOOKING:
                // Agent Phone

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBROnlineAgentPhoneCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        break;
                    case 2:
                        // Other Company
                        break;
                }

                break;

            case APP_CONSTANTS.API_PHONE_BOOKING:
                // API Phone

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRAPIPhoneCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        break;
                    case 2:
                        // Other Company
                        break;
                }

                break;

            case APP_CONSTANTS.AGENT_CC_CARD_BOOKING:
                // Agent CCard

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBROnlineAgentCardCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        break;
                    case 2:
                        // Other Company
                        break;
                }

                break;

            case APP_CONSTANTS.ITSPL_WALLET_BOOKING:
                // ITSPL Wallet

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBROnlineWalletCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOAOnlineWalletCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRITSPLWalletCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }
                break;

            case APP_CONSTANTS.REMOTE_PAYMENT_BOOKING:
                // Remote Payment

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRRemotePaymentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        if (bookingRightsByUserData.getBOARemotePaymentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRRemotePaymentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }
                break;

            case APP_CONSTANTS.API_BOOKING:
                // API
                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRAPICancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRAPIAgentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }

                break;
            case APP_CONSTANTS.B2C_BOOKING:
                // B2C

                switch (IsLoginBranchRoute) {
                    case 0:
                        // Own Branch
                        if (bookingRightsByUserData.getBRB2CCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                    case 1:
                        // Other Branch
                        break;
                    case 2:
                        // Other Company
                        if (bookingRightsByUserData.getFBRB2CAgentCancel() == 1) {
                            cancelBooking = true;
                        }
                        break;
                }
                break;
        }

        if (!cancelBooking) {
            showSnackBar(oops, "You Can Not Allow To Cancel " + reqBookingTypeName + " Booking", Toast.LENGTH_LONG, 1);
        }

        return cancelBooking;
    }

    private boolean haveRightsForHomeCompany() {
        int haveRights = 1;
        switch (bookingTypeId) {
            case APP_CONSTANTS.CONFIRM_BOOKING:
                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRConfirmCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRConfirmModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAConfirmModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRConfirmCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAConfirmCancel();
                    }
                }
                break;

            case APP_CONSTANTS.PHONE_BOOKING:
                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRPhoneCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRPhoneModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAPhoneModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRPhoneCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAPhoneCancel();
                    }
                }
                break;

            case APP_CONSTANTS.AGENT_BOOKING:
                // Agent Booking

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAgentCreate();
                        } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                            haveRights = bookingRightsByUserData.getBROnlineAgentCreate();
                        } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAPICreate();
                        } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                            haveRights = bookingRightsByUserData.getBRB2CCreate();
                        }
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAgentModify();
                        } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                            haveRights = bookingRightsByUserData.getBROnlineAgentModify();
                        } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAPIModify();
                        } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                            haveRights = bookingRightsByUserData.getBRB2CModify();
                        }
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                            haveRights = bookingRightsByUserData.getBOAAgentModify();
                        } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                            haveRights = bookingRightsByUserData.getBROnlineAgentModify();
                        } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAPIModify();
                        } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                            haveRights = bookingRightsByUserData.getBRB2CModify();
                        }
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAgentCancel();
                        } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                            haveRights = bookingRightsByUserData.getBROnlineAgentCancel();
                        } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAPICancel();
                        } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                            haveRights = bookingRightsByUserData.getBRB2CCancel();
                        }
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                            haveRights = bookingRightsByUserData.getBOAAgentCancel();
                        } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                            haveRights = bookingRightsByUserData.getBROnlineAgentCancel();
                        } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                            haveRights = bookingRightsByUserData.getBRAPICancel();
                        } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                            haveRights = bookingRightsByUserData.getBRB2CCancel();
                        }
                    }
                }
                break;
            case APP_CONSTANTS.BRANCH_BOOKING:
                // Branch Booking

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRBranchCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRBranchModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOABranchModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRBranchCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOABranchCancel();
                    }
                }
                break;

            case APP_CONSTANTS.GUEST_BOOKING:
                // Guest Booking

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRGuestCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRGuestModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAGuestModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRGuestCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAGuestCancel();
                    }
                }

                break;

            case APP_CONSTANTS.BANK_CARD_BOOKING:
                // BankCard Booking

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRBankCardCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRBankCardModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOABankCardModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRBankCardCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOABankCardCancel();
                    }
                }
                break;

            case APP_CONSTANTS.CMP_CARD_BOOKING:
                // CompanyCard Booking
                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRCompanyCardCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRCompanyCardModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOACompanyCardModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRCompanyCardCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOACompanyCardCancel();
                    }
                }
                break;

            case APP_CONSTANTS.WAITING_BOOKING:
                // Waiting Booking

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRWaitingCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRWaitingModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAWaitingModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRWaitingCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAWaitingCancel();
                    }
                }
                break;

            case APP_CONSTANTS.AGENT_QUOTA_BOOKING:
                // AgentQuota Booking
                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRAgentQuataCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRAgentQuataModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAAgentQuataModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRAgentQuataCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAAgentQuataCancel();
                    }
                }
                break;

            case APP_CONSTANTS.OPEN_BOOKING:
                // Open Booking

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROpenCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROpenModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAOpenModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROpenCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAOpenCancel();
                    }
                }
                break;

            case APP_CONSTANTS.AGENT_PHONE_BOOKING:
                // Agent Phone

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE && CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentPhoneCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE && CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentPhoneModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE && CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentPhoneModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE && CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentPhoneCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE && CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentPhoneCancel();
                    }
                }
                break;

            case APP_CONSTANTS.API_PHONE_BOOKING:
                // API Phone

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE && CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                        haveRights = bookingRightsByUserData.getBRAPIPhoneCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE && CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                        haveRights = bookingRightsByUserData.getBRAPIPhoneModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE && CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                        haveRights = bookingRightsByUserData.getBRAPIPhoneModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE && CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                        haveRights = bookingRightsByUserData.getBRAPIPhoneCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE && CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                        haveRights = bookingRightsByUserData.getBRAPIPhoneCancel();
                    }
                }
                break;

            case APP_CONSTANTS.AGENT_CC_CARD_BOOKING:
                // Agent CCard

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentCardCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentCardModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentCardModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentCardCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineAgentCardCancel();
                    }
                }

                break;

            case APP_CONSTANTS.ITSPL_WALLET_BOOKING:
                // ITSPL Wallet

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineWalletCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineWalletModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAOnlineWalletModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBROnlineWalletCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOAOnlineWalletCancel();
                    }
                }
                break;

            case APP_CONSTANTS.REMOTE_PAYMENT_BOOKING:
                // Remote Payment

                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRRemotePaymentCreate();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRRemotePaymentModify();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOARemotePaymentModify();
                    }
                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    if (IsLoginBranchRoute == APP_CONSTANTS.OWN_ROUTE) {
                        haveRights = bookingRightsByUserData.getBRRemotePaymentCancel();
                    } else if (IsLoginBranchRoute == APP_CONSTANTS.OTHER_BRANCH_ROUTE) {
                        haveRights = bookingRightsByUserData.getBOARemotePaymentCancel();
                    }
                }
                break;
        }

        if (haveRights == APP_CONSTANTS.RIGHTS_DENY) {
            String transType = "";
            if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                transType = "Book Ticket In ";
            } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                transType = "Modify Ticket In ";
            } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                transType = "Cancel Ticket In ";
            }
            showSnackBar(oops, "You Can Not Allow To  " + transType + reqBookingTypeName + " Booking", Toast.LENGTH_LONG, 1);
        }


        return haveRights == 1 ? true : false;
    }

    private void fetchOtherCompanyRights(){
        try {
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    otherCompanyBookingRightsList = appDatabase.getApiExeRouteTimeALLXMLDao().getITSGetOtherCompanyBookingRightsExe(loginCmpId, bookedByCmpId);
                }
            });
        } catch (Exception ignored) { }
    }

    private int haveRightsForOtherCompany() {
        int haveRights = 0;
        int AllowModifyCharge = 0;

        if (otherCompanyBookingRightsList.size() > 0) {
            Get_API_EXE_RouteTimeALLXML_Response.ITSGetOtherCompanyBookingRightsExe data = otherCompanyBookingRightsList.get(0);
            String validationMsg = "";
            try {
                if (TRANSACTION_TYPE == APP_CONSTANTS.INSERT) {
                    validationMsg = "To Insert - " + reqBookingTypeName;
                    if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING) {
                        validationMsg = "To INSERT CONFIRM";
                        haveRights = data.getOCBRConfirm();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For CONFIRM Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                        validationMsg = "To INSERT PHONE";
                        haveRights = data.getOCBRPhone();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRPhoneCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRPhoneCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For PHONE Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {
                        haveRights = data.getOCBRAgent();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT && data.getOCBRAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To INSERT GENERAL AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT && data.getOCBROnlineAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBROnlineAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To INSERT ONLINE AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT && data.getOCBRAPIAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAPIAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To INSERT API AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT && data.getOCBRB2CAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRB2CAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To INSERT B2C AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            }
                        } else {
                            validationMsg = "For AGENT Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.BRANCH_BOOKING) {
                        validationMsg = "To INSERT BRANCH";
                        haveRights = data.getOCBRBranch();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBranchCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBranchCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For BRANCH Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.GUEST_BOOKING) {
                        validationMsg = "To INSERT GUEST";
                        haveRights = data.getOCBRGuest();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRGuestCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRGuestCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For GUEST Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING) {
                        validationMsg = "To INSERT BANK-CARD";
                        haveRights = data.getOCBRBankCard();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBankCardCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBankCardCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For BANK-CARD Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                        validationMsg = "To INSERT COMPANY-CARD";
                        haveRights = data.getOCBRCompanyCard();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = data.getOCBRCompanyCardCreate();
                        } else {
                            validationMsg = "For COMPANY-CARD Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.WAITING_BOOKING) {
                        validationMsg = "To INSERT WAITING";
                        haveRights = data.getOCBRWaiting();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWaitingCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRWaitingCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For WAITING Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                        validationMsg = "To INSERT AGENT-QUOTA";
                        haveRights = data.getOCBRAgentQuota();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRAgentQuotaCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentQuotaCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For AGENT-QUOTA Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.OPEN_BOOKING) {
                        validationMsg = "To INSERT OPEN";
                        haveRights = data.getOCBROpen();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = data.getOCBROpenCreate();
                        } else {
                            validationMsg = "For OPEN Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                        validationMsg = "To INSERT Online Wallet";
                        haveRights = data.getOCBRWallet();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWalletCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRITSPLWalletCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For Online Wallet Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {
                        validationMsg = "To INSERT Remote Payment";
                        haveRights = data.getOCBRRemotePayment();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRRemotePaymentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRRemotePaymentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For Remote Payment Type";
                        }
                    }


                } else if (TRANSACTION_TYPE == APP_CONSTANTS.MODIFY) {
                    validationMsg = "To Modify - " + reqBookingTypeName;
                    AllowModifyCharge = data.getOSAllowModifyCharges();


                    if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING && modifyBookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING) {
                        //type = "To UPDATE CONFIRM";
                        haveRights = data.getOCBRConfirm();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRConfirmModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For CONFIRM Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING && modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                        //type = "To UPDATE PHONE";
                        haveRights = data.getOCBRPhone();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For PHONE Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING && modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                        // new Condition For Phone to Confirm.
                        haveRights = data.getOCBRConfirm();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For CONFIRM Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.AGENT_BOOKING && modifyBookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {

                        haveRights = data.getOCBRAgent();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            //if ((int)paraSAVE.CAM_IsOnline > 1)
                            //{
                            //    validationMsg =  "To INSERT " + (paraSAVE.CAM_IsOnline.ToString()).ToUpper() + "-AGENT";
                            //    haveRights = false;
                            //}
                            //else
                            //{
                            //    haveRights = OCBR_Agent_Modify && bookingRightsByUserData.OtherCompanyBookingRights.FBR_AgentModify;
                            //}
                            if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT && data.getOCBRAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To UPDATE GENERAL AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT && data.getOCBROnlineAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBROnlineAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To UPDATE ONLINE AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT && data.getOCBRAPIAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAPIAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To UPDATE API AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT && data.getOCBRB2CAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRB2CAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                validationMsg = "To UPDATE B2C AGENT";
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            }
                        } else {
                            validationMsg = "For AGENT Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.BRANCH_BOOKING && modifyBookingTypeId == APP_CONSTANTS.BRANCH_BOOKING) {
                        validationMsg = "To UPDATE BRANCH";
                        haveRights = data.getOCBRBranch();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBranchModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBranchModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For BRANCH Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.GUEST_BOOKING && modifyBookingTypeId == APP_CONSTANTS.GUEST_BOOKING) {
                        validationMsg = "To UPDATE GUEST";
                        haveRights = data.getOCBRGuest();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRGuestModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRGuestModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For GUEST Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING && modifyBookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING) {
                        validationMsg = "To UPDATE BANK-CARD";
                        haveRights = data.getOCBRBankCard();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBankCardModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBankCardModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For BANK-CARD Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING && modifyBookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                        validationMsg = "To UPDATE COMPANY-CARD";
                        haveRights = data.getOCBRCompanyCard();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = data.getOCBRCompanyCardModify();
                        } else {
                            validationMsg = "For COMPANY-CARD Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.WAITING_BOOKING && modifyBookingTypeId == APP_CONSTANTS.WAITING_BOOKING) {
                        validationMsg = "To UPDATE WAITING";
                        haveRights = data.getOCBRWaiting();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWaitingModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For WAITING Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING && modifyBookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                        validationMsg = "To UPDATE AGENT-QUOTA";
                        haveRights = data.getOCBRAgentQuota();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRAgentQuotaModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentQuotaModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For AGENT-QUOTA Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.OPEN_BOOKING && modifyBookingTypeId == APP_CONSTANTS.OPEN_BOOKING) {
                        validationMsg = "To UPDATE OPEN";
                        haveRights = data.getOCBROpen();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = data.getOCBROpenModify();
                        } else {
                            validationMsg = "For OPEN Booking Type";
                        }
                    } else if (modifyBookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                        validationMsg = "To UPDATE OnlineWallet";
                        haveRights = data.getOCBRWallet();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWalletModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRITSPLWalletModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For OnlineWallet Booking Type";
                        }
                    } else if (modifyBookingTypeId == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {
                        validationMsg = "To UPDATE OnlineWallet";
                        haveRights = data.getOCBRRemotePayment();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRRemotePaymentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRRemotePaymentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For OnlineWallet Booking Type";
                        }
                    } else if (bookingTypeId != modifyBookingTypeId) {
                        if (modifyBookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING) {
                            validationMsg = "To UPDATE CONFIRM";
                            haveRights = data.getOCBRConfirm();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRConfirmModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For CONFIRM Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                            validationMsg = "To UPDATE PHONE";
                            haveRights = data.getOCBRPhone();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRPhoneModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For PHONE Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {
                            haveRights = data.getOCBRAgent();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                                if (CAM_IsOnline > 1) {
                                    if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                                        validationMsg = "To INSERT API AGENT";
                                    } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                                        validationMsg = "To INSERT B2C AGENT";
                                    }
                                    haveRights = APP_CONSTANTS.RIGHTS_DENY;
                                } else {
                                    if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                                        validationMsg = "To UPDATE GENERAL AGENT";
                                    } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                                        validationMsg = "To UPDATE ONLINE AGENT";
                                    }
                                    if (data.getOCBRAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                        haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                    }
                                }
                            } else {
                                validationMsg = "For AGENT Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.BRANCH_BOOKING) {
                            validationMsg = "To UPDATE BRANCH";
                            haveRights = data.getOCBRBranch();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBranchModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBranchModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For BRANCH Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.GUEST_BOOKING) {
                            validationMsg = "To UPDATE GUEST";
                            haveRights = data.getOCBRGuest();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRGuestModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRGuestModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For GUEST Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING) {
                            validationMsg = "To UPDATE BANK-CARD";
                            haveRights = data.getOCBRBankCard();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBankCardModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBankCardModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For BANK-CARD Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                            validationMsg = "To UPDATE COMPANY-CARD";
                            haveRights = data.getOCBRCompanyCard();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = data.getOCBRCompanyCardModify();
                            } else {
                                validationMsg = "For COMPANY-CARD Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.WAITING_BOOKING) {
                            validationMsg = "To UPDATE WAITING";
                            haveRights = data.getOCBRWaiting();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWaitingModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRWaitingModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For WAITING Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                            validationMsg = "To UPDATE AGENT-QUOTA";
                            haveRights = data.getOCBRAgentQuota();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRAgentQuotaModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentQuotaModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For AGENT-QUOTA Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.OPEN_BOOKING) {
                            validationMsg = "To UPDATE OPEN";
                            haveRights = data.getOCBROpen();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = data.getOCBROpenModify();
                            } else {
                                validationMsg = "For OPEN Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                            validationMsg = "To UPDATE OnlineWallet";
                            haveRights = data.getOCBRWallet();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWalletModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRITSPLWalletModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For OnlineWallet Booking Type";
                            }
                        } else if (modifyBookingTypeId == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {
                            validationMsg = "To UPDATE Remote Payment";
                            haveRights = data.getOCBRRemotePayment();
                            if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRRemotePaymentModify() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRRemotePaymentModify() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                            } else {
                                validationMsg = "For Remote Payment Booking Type";
                            }
                        }
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING) {
                                validationMsg = "To INSERT CONFIRM";
                                haveRights = data.getOCBRConfirm();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For CONFIRM Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                                validationMsg = "To INSERT PHONE";
                                haveRights = data.getOCBRPhone();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRPhoneCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRPhoneCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For PHONE Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {
                                haveRights = data.getOCBRAgent();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {

                                    if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT) {
                                        validationMsg = "To INSERT GENERAL AGENT";
                                    } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT) {
                                        validationMsg = "To INSERT ONLINE AGENT";
                                    } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT) {
                                        validationMsg = "To INSERT API AGENT";
                                    } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT) {
                                        validationMsg = "To INSERT B2C AGENT";
                                    }

                                    if (CAM_IsOnline > 1) {
                                        haveRights = APP_CONSTANTS.RIGHTS_DENY;
                                    } else {
                                        if (data.getOCBRAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                        }
                                    }
                                } else {
                                    validationMsg = "For AGENT Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.BRANCH_BOOKING) {
                                validationMsg = "To INSERT BRANCH";
                                haveRights = data.getOCBRBranch();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBranchCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBranchCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For BRANCH Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.GUEST_BOOKING) {
                                validationMsg = "To INSERT GUEST";
                                haveRights = data.getOCBRGuest();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRGuestCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRGuestCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For GUEST Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING) {
                                validationMsg = "To INSERT BANK-CARD";
                                haveRights = data.getOCBRBankCard();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBankCardCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBankCardCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For BANK-CARD Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                                validationMsg = "To INSERT COMPANY-CARD";
                                haveRights = data.getOCBRCompanyCard();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = data.getOCBRCompanyCardCreate();
                                } else {
                                    validationMsg = "For COMPANY-CARD Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.WAITING_BOOKING) {
                                validationMsg = "To INSERT WAITING";
                                haveRights = data.getOCBRWaiting();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWaitingCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRWaitingCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For WAITING Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                                validationMsg = "To INSERT AGENT-QUOTA";
                                haveRights = data.getOCBRAgentQuota();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRAgentQuotaCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentQuotaCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For AGENT-QUOTA Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.OPEN_BOOKING) {
                                validationMsg = "To INSERT OPEN";
                                haveRights = data.getOCBROpen();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = data.getOCBROpenCreate();
                                } else {
                                    validationMsg = "For OPEN Booking Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                                validationMsg = "To INSERT OnlineWallet";
                                haveRights = data.getOCBRWallet();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWalletCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRITSPLWalletCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For Online Wallet Type";
                                }
                            } else if (bookingTypeId == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {
                                validationMsg = "To INSERT Remote Payment";
                                haveRights = data.getOCBRRemotePayment();
                                if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRRemotePaymentCreate() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRRemotePaymentCreate() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                    haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                } else {
                                    validationMsg = "For Remote Payment Type";
                                }
                            }
                        }
                    }

                } else if (TRANSACTION_TYPE == APP_CONSTANTS.CANCEL) {
                    // Cancel
                    validationMsg = "To Cancel - " + reqBookingTypeName;

                    if (bookingTypeId == APP_CONSTANTS.CONFIRM_BOOKING) {
                        validationMsg = "To CANCEL CONFIRM";
                        haveRights = data.getOCBRConfirm();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRConfirmCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRConfirmCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For CONFIRM Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.PHONE_BOOKING) {
                        validationMsg = "To CANCEL PHONE";
                        haveRights = data.getOCBRPhone();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRPhoneCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRPhoneCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For PHONE Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.AGENT_BOOKING) {

                        haveRights = data.getOCBRAgent();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            //if ((int)paraSAVE.CAM_IsOnline > 0)
                            //{
                            //    haveRights = false;
                            //}
                            //else
                            //{
                            //    haveRights = OCBR_Agent_Cancel && bookingRightsByUserData.FBR_AgentCancel;
                            //}
                            if (CAM_IsOnline == APP_CONSTANTS.GENERAL_AGENT && data.getOCBRAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                validationMsg = "To CANCEL GENERAL AGENT";
                            } else if (CAM_IsOnline == APP_CONSTANTS.ONLINE_AGENT && data.getOCBROnlineAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBROnlineAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                validationMsg = "To CANCEL ONLINE_AGENT";
                            } else if (CAM_IsOnline == APP_CONSTANTS.API_AGENT && data.getOCBRAPIAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAPIAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                validationMsg = "To CANCEL API_AGENT";
                            } else if (CAM_IsOnline == APP_CONSTANTS.B2C_AGENT && data.getOCBRB2CAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRB2CAgentCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                                haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                                validationMsg = "To CANCEL B2C AGENT";
                            }
                        } else {
                            validationMsg = "For AGENT Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.BRANCH_BOOKING) {
                        validationMsg = "To CANCEL BRANCH";
                        haveRights = data.getOCBRBranch();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBranchCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBranchCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For BRANCH Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.GUEST_BOOKING) {
                        validationMsg = "To CANCEL GUEST";
                        haveRights = data.getOCBRGuest();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRGuestCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRGuestCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For GUEST Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.BANK_CARD_BOOKING) {
                        validationMsg = "To CANCEL BANK-CARD";
                        haveRights = data.getOCBRBankCard();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRBankCardCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRBankCardCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For BANK-CARD Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.CMP_CARD_BOOKING) {
                        validationMsg = "To CANCEL COMPANY-CARD";
                        haveRights = data.getOCBRCompanyCard();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRCompanyCardCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRCompanyCardCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For COMPANY-CARD Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.WAITING_BOOKING) {
                        validationMsg = "To CANCEL WAITING";
                        haveRights = data.getOCBRWaiting();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWaitingCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRWaitingCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For WAITING Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.AGENT_QUOTA_BOOKING) {
                        validationMsg = "To CANCEL AGENT-QUOTA";
                        haveRights = data.getOCBRAgentQuota();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRAgentQuotaCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRAgentQuotaCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For AGENT-QUOTA Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.OPEN_BOOKING) {
                        validationMsg = "To CANCEL OPEN";
                        haveRights = data.getOCBROpen();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = data.getOCBROpenCancel();
                        } else {
                            validationMsg = "For OPEN Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.ITSPL_WALLET_BOOKING) {
                        validationMsg = "To CANCEL OnlineWallet";
                        haveRights = data.getOCBRWallet();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRWalletCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRITSPLWalletCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For OnlineWallet Booking Type";
                        }
                    } else if (bookingTypeId == APP_CONSTANTS.REMOTE_PAYMENT_BOOKING) {
                        validationMsg = "To CANCEL Remote Payment";
                        haveRights = data.getOCBRRemotePayment();
                        if (haveRights == APP_CONSTANTS.RIGHTS_ALLOW && data.getOCBRRemotePaymentCancel() == APP_CONSTANTS.RIGHTS_ALLOW && bookingRightsByUserData.getFBRRemotePaymentCancel() == APP_CONSTANTS.RIGHTS_ALLOW) {
                            haveRights = APP_CONSTANTS.RIGHTS_ALLOW;
                        } else {
                            validationMsg = "For Remote Payment Booking Type";
                        }
                    }


                }

                if (haveRights == APP_CONSTANTS.RIGHTS_DENY) {
                    validationMsg = "Sorry, You Are Officially Not Allowed" + validationMsg + " For OTHER COMPANY.";
                    showSnackBar(oops, validationMsg, Toast.LENGTH_LONG, 1);
                }
            } catch (Exception ex) {
            }
        }
        return haveRights;

    }

}

