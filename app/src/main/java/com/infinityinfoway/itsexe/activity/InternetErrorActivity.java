package com.infinityinfoway.itsexe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;


public class InternetErrorActivity extends AppCompatActivity {

    private TextView txt_text_wallet;
    private Button btntry_again;
    private ConnectionDetector cd;
    private String caller = "";
    private Bundle getPutData;
    private TextView txt_enable_settings;
    private RelativeLayout relative_main;
    private ITSExeSharedPref getPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet_error);


        txt_text_wallet = findViewById(R.id.txt_text_wallet);
        btntry_again = findViewById(R.id.btntry_again);
        txt_enable_settings = findViewById(R.id.txt_enable_settings);
        relative_main = findViewById(R.id.relative_main);
        getPutData = getIntent().getExtras();
        if (getPutData != null) {
            caller = getPutData.getString("caller");
        } else {
            caller = "other";
        }

        getPref = new ITSExeSharedPref(InternetErrorActivity.this);


        btntry_again.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(InternetErrorActivity.this);
                if (!cd.isConnectingToInternet()) {
                    // Toast.makeText(InternetErrorActivity.this, "You Still Do Not Have Internet Connection", Toast.LENGTH_LONG).show();

                    Snackbar snackbar;
                    snackbar = Snackbar.make(relative_main, "You Still Do Not Have Internet Connection", Snackbar.LENGTH_LONG);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getResources().getColor(R.color.red));
                    TextView textView = snackBarView.findViewById(R.id.snackbar_text);
                    textView.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();

                } else {
                    if (caller.equals("SplashScreen")) {
                        Intent i = new Intent(InternetErrorActivity.this, SplashScreen.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    } else if (caller.equals("Error")) {
                        Intent i = new Intent(InternetErrorActivity.this, SplashScreen.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    } else {
                        finish();
                    }
                }
            }
        });

        txt_enable_settings.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });


    }

    public void onBackPressed() {
        if (caller.equals("SplashScreen")) {
            Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
