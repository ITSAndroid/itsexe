package com.infinityinfoway.itsexe.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_InsertTabletBookingRegistration_Request;
import com.infinityinfoway.itsexe.api.response.Get_InsertTabletBookingRegistration_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.custom_dialog.SweetAlertDialog;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityDeviceRegistrationBinding;
import com.infinityinfoway.itsexe.utils.CustomSnackBar;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeviceRegistrationActivity extends AppCompatActivity {
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ActivityDeviceRegistrationBinding binding;
    private String str_versionName, str_deviceUser, str_deviceID, str_imei, str_deviceModel, str_deviceMake, str_deviceProduct, str_UserName,   str_CompanyName;
    private int str_osVersion, str_versionCode, str_CompanyId;
    private ITSExeSharedPref getPref;
    private long lastClickTime = 0;
    private SweetAlertDialog dialogSuccess, dialogProgress;
    private ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDeviceRegistrationBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        cd = new ConnectionDetector(DeviceRegistrationActivity.this);
        getPref = new ITSExeSharedPref(DeviceRegistrationActivity.this);

        str_versionName = getPref.getVersionName();
        str_versionCode = getPref.getVersionCode();
        str_deviceID = getPref.getdeviceID();
        str_osVersion = getPref.getosVersion();
        str_imei = getPref.getimei();
        str_deviceModel = getPref.getdeviceModel();
        str_deviceMake = getPref.getdeviceMake();
        str_deviceProduct = getPref.getdeviceProduct();
        str_deviceUser = getPref.getdeviceUser();


        binding.btnRegister.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) { }

            hideKeyBoard();

            if (TextUtils.isEmpty(binding.etCmpName.getText().toString())) {
                showSnackBar(getResources().getString(R.string.dialog_error_title),getResources().getString(R.string.cmp_name_valid));
            } else if (TextUtils.isEmpty(binding.etBranchName.getText().toString())) {
                showSnackBar(getResources().getString(R.string.dialog_error_title),getResources().getString( R.string.branch_name_valid));
            } else if (TextUtils.isEmpty(binding.etUserName.getText().toString())) {
                showSnackBar(getResources().getString(R.string.dialog_error_title),getResources().getString( R.string.user_name_valid));
            } else {

                if (cd.isConnectingToInternet()) {


                    str_UserName = binding.etUserName.getText().toString().replaceAll("&", "&amp;");
                    str_CompanyName = binding.etCmpName.getText().toString().replaceAll("&", "&amp;");

                    if (!TextUtils.isEmpty(binding.etCompanyId.getText().toString())) {
                        str_CompanyId = Integer.parseInt(binding.etCompanyId.getText().toString().trim());
                    } else {
                        str_CompanyId = 0;
                    }

                    deviceRegistrationApiCall();
                } else {
                    showInternetConnectionDialog(-1, getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.no_internet_long_msg));
                }
            }
        });
    }

    private void deviceRegistrationApiCall() {

        showProgressDialog();

        String tbrm_Id = "0";
        Get_InsertTabletBookingRegistration_Request request = new Get_InsertTabletBookingRegistration_Request(
                tbrm_Id,
                str_deviceID,
                String.valueOf(str_osVersion),
                str_imei,
                str_deviceModel,
                str_deviceMake,
                str_deviceProduct,
                str_deviceUser,
                "",
                str_CompanyName,
                "",
                "",
                str_UserName,
                1,
                0,
                0,
                str_CompanyId,
                str_versionCode,
                "",
                "",
                str_versionName,
                binding.etBranchName.getText().toString().replaceAll("&", "&amp;")
        );

        Call<Get_InsertTabletBookingRegistration_Response> call = apiService.Get_InsertTabletBookingRegistration(request);


        call.enqueue(new Callback<Get_InsertTabletBookingRegistration_Response>() {
            @Override
            public void onResponse(@NotNull Call<Get_InsertTabletBookingRegistration_Response> call, @NotNull Response<Get_InsertTabletBookingRegistration_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getData().getInsertTabletBookingRegistration() != null && response.body().getData().getInsertTabletBookingRegistration().size() > 0) {
                        Get_InsertTabletBookingRegistration_Response.InsertTabletBookingRegistration data = response.body().getData().getInsertTabletBookingRegistration().get(0);
                        String responseMessage=data.getStatusMsg();
                        if (response.body().getStatus() != 0) {
                            getPref.setTBRM_ID(data.getTDRMID());
                            showInternetConnectionDialog(response.body().getStatus(),  getResources().getString(R.string.dialog_success_title), responseMessage + " \t\t Request Authorization Id: " + getPref.getTBRM_ID());
                        } else {
                            showInternetConnectionDialog(response.body().getStatus(),  getResources().getString(R.string.dialog_error_title), responseMessage);
                        }
                    }
                } else {
                    showInternetConnectionDialog(0,  getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Get_InsertTabletBookingRegistration_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showInternetConnectionDialog(0,  getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
            }
        });

    }

    private void showInternetConnectionDialog(final int dialogType,  String title, String message) {

        if (dialogType > 0) {
            dialogSuccess = new SweetAlertDialog(DeviceRegistrationActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        } else {
            dialogSuccess = new SweetAlertDialog(DeviceRegistrationActivity.this, SweetAlertDialog.ERROR_TYPE);
        }

        dialogSuccess.setTitleText(title);

        dialogSuccess.setContentText(message);

        try {
//            TextView text = dialogSuccess.findViewById(R.id.content_text);
//            text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//            text.setText(message);
            // text.setLines(3);
            dialogSuccess.setCancelable(false);
            dialogSuccess.show();
        } catch (Exception ignored) {
        }

        dialogSuccess.setConfirmText(getResources().getString(R.string.ok));

        dialogSuccess.setConfirmClickListener(sweetAlertDialog -> {
            dialogSuccess.dismissWithAnimation();
            dialogSuccess.cancel();

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }

            if (dialogType > 0) {
                finish();
            }
        });

    }

    private void disMissDialog() {
        if (dialogProgress != null && !isFinishing() && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

    private void showProgressDialog() {
        try {
            dialogProgress = new SweetAlertDialog(DeviceRegistrationActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            dialogProgress.setTitleText(getResources().getString(R.string.loading));
            dialogProgress.setCancelable(false);
            dialogProgress.show();
        } catch (Exception ignored) {
        }
    }

    private void hideKeyBoard() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void showSnackBar(String title, String desc) {
        if (!isFinishing()) {
            if (TextUtils.isEmpty(desc)) {
                desc = getResources().getString(R.string.bad_server_response);
            }
            final Snackbar snackbar = CustomSnackBar.showSnackbar(DeviceRegistrationActivity.this, binding.llMain, title, desc, Toast.LENGTH_LONG, 0);
            snackbar.show();
        }
    }

}