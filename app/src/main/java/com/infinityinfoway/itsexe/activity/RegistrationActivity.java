package com.infinityinfoway.itsexe.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.adapters.BranchListAdapter;
import com.infinityinfoway.itsexe.adapters.CompanyListAdapter;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.RxAPICallHelper;
import com.infinityinfoway.itsexe.api.request.AdminLogin_Request;
import com.infinityinfoway.itsexe.api.request.Get_BranchList_Request;
import com.infinityinfoway.itsexe.api.request.Get_CompanyList_Request;
import com.infinityinfoway.itsexe.api.request.Get_TabVerifyLogin_Request;
import com.infinityinfoway.itsexe.api.response.AdminLogin_Response;
import com.infinityinfoway.itsexe.api.response.Get_BranchList_Response;
import com.infinityinfoway.itsexe.api.response.Get_CompanyList_Response;
import com.infinityinfoway.itsexe.api.response.Get_TabVerifyLogin_Response;
import com.infinityinfoway.itsexe.bean.Api_List_Status;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.custom_dialog.SweetAlertDialog;
import com.infinityinfoway.itsexe.database.AppDatabase;
import com.infinityinfoway.itsexe.database.AppExecutors;
import com.infinityinfoway.itsexe.database.DatabaseClient;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivityRegistrationBinding;
import com.infinityinfoway.itsexe.utils.API_RESPONSE_STATUS;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;
import com.infinityinfoway.itsexe.utils.ExceptionHandler;
import com.infinityinfoway.itsexe.utils.NETWORK_STATUS;
import com.infinityinfoway.itsexe.utils.NetworkLiveData;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.utils.RxAPICallback;
import java.util.List;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
public class RegistrationActivity extends AppCompatActivity {

    private ITSExeSharedPref getPref;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ConnectionDetector cd;
    private long lastClickTime = 0;
    private List<Get_TabVerifyLogin_Response.LoginDetail> loginDetailList;
    private List<Get_CompanyList_Response.CityDetail> listCompanyDetails;
    private List<Get_BranchList_Response.BranchDetail> listBranchDetails;
    private ActivityRegistrationBinding binding;
    private int reqCompanyId, reqBranchId, reqStateId, loginCompanyId, loginBranchId;
    private String reqCompanyName;
    private SweetAlertDialog dialogProgress;
    private AppDatabase appDatabase;
    private String AdminUserOTP = "";
    private Dialog dialogAdminLogin;
    private Dialog dialogSubmitOTP;
    private final MutableLiveData<Api_List_Status> mutableApiStatus = new MutableLiveData<>();
    private final MutableLiveData<PROGRESS_STATUS> mutableProgressStatus = new MutableLiveData<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private String badNetwork, badServerResponse;
    private String responseMsg;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegistrationBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        cd = new ConnectionDetector(RegistrationActivity.this);
        networkDetection();
        String TAG = "RegistrationActivity";
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this, TAG));
        getPref = new ITSExeSharedPref(RegistrationActivity.this);
        handler = new Handler();

        badNetwork = getResources().getString(R.string.no_internet_long_msg);
        badServerResponse = getResources().getString(R.string.bad_server_response);

        String userName = getPref.getBUM_UserName();
        String password = getPref.getBUM_Password();
        loginCompanyId = getPref.getCM_CompanyID();
        loginBranchId = getPref.getBM_BranchID();

        if (password != null && password.length() > 0 && getPref.getIsChkRememberMe() == 1) {
            if (getPref.IsLogin()) {
                binding.rgEtPassword.setText(password);
                binding.chkRememberMe.setChecked(true);
            }
        }

        if (userName != null && userName.length() > 0 && getPref.getIsChkRememberMe() == 1) {
            if (getPref.IsLogin()) {
                binding.rgEtUsername.setText(userName);
                binding.rgEtUsername.setSelection(binding.rgEtUsername.getText().length());
            }
        }


        //================================================================================================
        // Registration Button ClickListener
        //================================================================================================
        binding.btnLogin.setOnClickListener(arg0 -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) { }

            hideKeyBoard();

            if (binding.spnCompany.getSelectedItemPosition() == -1) {
                showSnackBar("", getResources().getString(R.string.valid_cmp), false);
            } else if (binding.spnBranch.getSelectedItemPosition() == -1) {
                showSnackBar("", getResources().getString(R.string.valid_branch), false);
            } else if (binding.rgEtUsername.getText().toString().length() <= 0) {
                showSnackBar("", getResources().getString(R.string.valid_user), false);
            } else if (binding.rgEtPassword.getText().toString().length() <= 0) {
                showSnackBar("", getResources().getString(R.string.valid_password), false);
            } else {
                Get_TabVerifyLoginApiCall();

            }
        });

        binding.spnCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (listCompanyDetails != null && listCompanyDetails.size() > 0) {
                    reqCompanyName = listCompanyDetails.get(position).getCMCompanyName();
                    reqCompanyId = listCompanyDetails.get(position).getCMCompanyID();
                    reqStateId = listCompanyDetails.get(position).getSMStateID();
                    getBranchListApiCall();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spnBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reqBranchId = listBranchDetails.get(position).getBMBranchID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        appDatabase = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase();

        AppExecutors.getInstance().diskIO().execute(() -> {
            try {
                appDatabase.clearAllTables();
            } catch (Exception ignored) {
            }
        });

        mutableApiStatus.observe(RegistrationActivity.this, apiListStatus -> {
            switch (apiListStatus.getApiStatus()) {
                case SUCCESS:
                case NO_DATA:
                    showSnackBar(apiListStatus.getApiName(), apiListStatus.getApiResponseMsg(), false);
                    break;
                case FAILED:
                    showSnackBar(apiListStatus.getApiName(), apiListStatus.getApiResponseMsg(), true);
                    break;
            }
        });

        mutableProgressStatus.observe(RegistrationActivity.this, progress_status -> {
            switch (progress_status) {
                case SHOW:
                    showProgressDialog(getResources().getString(R.string.loading));
                    break;
                case DISMISS:
                    disMissDialog();
                    break;
            }
        });

        mutableProgressStatus.setValue(PROGRESS_STATUS.SHOW);
        getCompanyListApiCall();

    }

    private Observable<Get_BranchList_Response> getBranchListObservable() {
        Get_BranchList_Request request = new Get_BranchList_Request(
                reqCompanyId,
                getPref.getTBRM_ID()
        );
        return apiService.Get_BranchList(request);
    }

    private void getBranchListApiCall() {
        if (cd != null && cd.isConnectingToInternet()) {
            compositeDisposable.add(RxAPICallHelper.call(getBranchListObservable(), new RxAPICallback<Get_BranchList_Response>() {
                @Override
                public void onSuccess(Get_BranchList_Response response) {
                    mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                    if (response.getStatus() != null && response.getStatus() == 1 && response.getData().getBranchDetails() != null && response.getData().getBranchDetails().size() > 0) {
                        listBranchDetails = response.getData().getBranchDetails();
                        binding.spnBranch.setAdapter(new BranchListAdapter(RegistrationActivity.this, listBranchDetails));
                        if (loginBranchId > 0) {
                            for (int i = 0; i < listBranchDetails.size(); i++) {
                                if (listBranchDetails.get(i).getBMBranchID() == loginBranchId) {
                                    binding.spnBranch.setSelection(i);
                                    break;
                                }
                            }
                            loginBranchId = 0;
                        }

                    } else {
                        responseMsg = TextUtils.isEmpty(response.getMessage()) ? badServerResponse : response.getMessage();
                        addApiStatus(APP_CONSTANTS.Get_CompanyList, API_RESPONSE_STATUS.FAILED, responseMsg);
                    }
                }

                @Override
                public void onFailed(Throwable throwable) {
                    mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                    addApiStatus(APP_CONSTANTS.Get_BranchList, API_RESPONSE_STATUS.FAILED, badServerResponse);
                }
            }));
        } else {
            addApiStatus(APP_CONSTANTS.Get_BranchList, API_RESPONSE_STATUS.NO_DATA, badNetwork);
        }

    }

    private Observable<Get_CompanyList_Response> getCompanyListObservable() {
        Get_CompanyList_Request request = new Get_CompanyList_Request(
                getPref.getOSDeviceID(),
                getPref.getTBRM_ID()
        );
        return apiService.Get_CompanyList(request);
    }

    private void getCompanyListApiCall() {
        if (cd != null && cd.isConnectingToInternet()) {
            compositeDisposable.add(RxAPICallHelper.call(getCompanyListObservable(), new RxAPICallback<Get_CompanyList_Response>() {
                @Override
                public void onSuccess(Get_CompanyList_Response response) {
                    if (response.getStatus() != null && response.getStatus() == 1 && response.getData().getCityDetails() != null && response.getData().getCityDetails().size() > 0) {
                        listCompanyDetails = response.getData().getCityDetails();
                        binding.spnCompany.setAdapter(new CompanyListAdapter(RegistrationActivity.this, listCompanyDetails));
                        for (int i = 0; i < listCompanyDetails.size(); i++) {
                            if (listCompanyDetails.get(i).getCMCompanyID() == loginCompanyId) {
                                binding.spnCompany.setSelection(i);
                                break;
                            }
                        }
                    } else {
                        responseMsg = TextUtils.isEmpty(response.getMessage()) ? badServerResponse : response.getMessage();
                        addApiStatus(APP_CONSTANTS.Get_CompanyList, API_RESPONSE_STATUS.FAILED, responseMsg);
                    }
                }

                @Override
                public void onFailed(Throwable throwable) {
                    mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                    addApiStatus(APP_CONSTANTS.Get_CompanyList, API_RESPONSE_STATUS.FAILED, badServerResponse);
                }
            }));
        } else {
            addApiStatus(APP_CONSTANTS.Get_CompanyList, API_RESPONSE_STATUS.FAILED, badNetwork);
        }
                /*compositeDisposable.add(getCompanyListObservable().subscribeOn(Schedulers.io()).flatMap(new Function<Get_CompanyList_Response, ObservableSource<Get_BranchList_Response>>() {
                    @Override
                    public ObservableSource<Get_BranchList_Response> apply(Get_CompanyList_Response response) {
                        if (response.getStatus() != null && response.getStatus() == 1 && response.getData().getCityDetails() != null && response.getData().getCityDetails().size() > 0) {
                            listCompanyDetails = response.getData().getCityDetails();
                            addApiList(APP_CONSTANTS.Get_CompanyList, API_RESPONSE_STATUS.SUCCESS);
                            return getBranchListObservable();
                        } else {
                            addApiList(APP_CONSTANTS.Get_CompanyList, API_RESPONSE_STATUS.FAILED);
                            return null;
                        }
                    }
                }).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Get_BranchList_Response>() {
                    @Override
                    public void onNext(@NonNull Get_BranchList_Response response) {
                        if (listCompanyDetails != null && listCompanyDetails.size() > 0) {
                            binding.spnCompany.setAdapter(new CompanyListAdapter(RegistrationActivity.this, listCompanyDetails));
                        }
                        if (response.getStatus() != null && response.getStatus() == 1 && response.getData().getBranchDetails() != null && response.getData().getBranchDetails().size() > 0) {
                            listBranchDetails = response.getData().getBranchDetails();
                            binding.spnBranch.setAdapter(new BranchListAdapter(RegistrationActivity.this, listBranchDetails));
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("error", "error2" + e.getMessage());
                        mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                        showInternetConnectionDialog(0, "", "oops", "no internet");
                    }

                    @Override
                    public void onComplete() {
                        mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                    }
                }));*/
    }


    private Observable<Get_TabVerifyLogin_Response> getTabVerifyLoginObservable() {
        Get_TabVerifyLogin_Request request = new Get_TabVerifyLogin_Request(
                binding.rgEtUsername.getText().toString().trim(),
                binding.rgEtPassword.getText().toString(),
                reqBranchId,
                reqCompanyId,
                getPref.getTBRM_ID(),
                getPref.getOSDeviceID()
        );
        return apiService.Get_TabVerifyLogin(request);
    }

    private void Get_TabVerifyLoginApiCall() {
        if (cd != null && cd.isConnectingToInternet()) {
            mutableProgressStatus.setValue(PROGRESS_STATUS.SHOW);
            compositeDisposable.add(RxAPICallHelper.call(getTabVerifyLoginObservable(), new RxAPICallback<Get_TabVerifyLogin_Response>() {
                @Override
                public void onSuccess(Get_TabVerifyLogin_Response response) {
                    mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                    if (response.getStatus() != null && response.getStatus() == 1 && response.getData().getLoginDetails() != null && response.getData().getLoginDetails().size() > 0) {
                        loginDetailList = response.getData().getLoginDetails();
                        getPref.setLoginCompanyName(reqCompanyName);
                        getPref.setLoginDetails(loginDetailList.get(0));
                        getPref.setLoginStateId(reqStateId);
                        if (binding.chkRememberMe.isChecked()) {
                            getPref.setChkRememberMe(1);
                        } else {
                            getPref.setChkRememberMe(0);
                        }
                        if (loginDetailList.get(0).getIsAdminlogin() == 1) {
                            showAdminLoginPopUp();
                        } else {
                            getPref.setAdminUserId(0);
                            redirectInHomeScreen();
                        }
                    } else {
                        responseMsg = TextUtils.isEmpty(response.getMessage()) ? badServerResponse : getResources().getString(R.string.invalid_user_name);
                        addApiStatus(APP_CONSTANTS.Get_TabVerifyLogin, API_RESPONSE_STATUS.NO_DATA, responseMsg);
                    }
                }

                @Override
                public void onFailed(Throwable throwable) {
                    mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                    addApiStatus(APP_CONSTANTS.Get_TabVerifyLogin, API_RESPONSE_STATUS.FAILED, badServerResponse);
                }
            }));
        } else {
            addApiStatus(APP_CONSTANTS.Get_TabVerifyLogin, API_RESPONSE_STATUS.NO_DATA, badNetwork);
        }

    }


    private void addApiStatus(String apiName, API_RESPONSE_STATUS apiStatus, String resMsg) {
        Api_List_Status apiListStatus = new Api_List_Status();
        apiListStatus.setApiName(apiName);
        apiListStatus.setApiStatus(apiStatus);
        apiListStatus.setApiResponseMsg(resMsg);
        mutableApiStatus.setValue(apiListStatus);
    }

    public void showAdminLoginPopUp() {
        try {
            dialogAdminLogin = new Dialog(RegistrationActivity.this);
            dialogAdminLogin.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogAdminLogin.setCancelable(false);
            dialogAdminLogin.setContentView(R.layout.dialog_admin_login);
            final EditText edt_user_name = dialogAdminLogin.findViewById(R.id.edt_user_name);
            final EditText edt_password = dialogAdminLogin.findViewById(R.id.edt_password);
            ImageView img_close = dialogAdminLogin.findViewById(R.id.img_close);
            Button btn_admin_login = dialogAdminLogin.findViewById(R.id.btn_admin_login);
            final CheckBox chk_remember_me_admin = dialogAdminLogin.findViewById(R.id.chk_remember_me_admin);
            final ProgressBar progressbar_login = dialogAdminLogin.findViewById(R.id.progressbar_login);

            img_close.setOnClickListener(view -> dialogAdminLogin.dismiss());

            btn_admin_login.setOnClickListener(v -> {
                try {
                    if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                } catch (Exception ignored) {
                }

                if (TextUtils.isEmpty(edt_user_name.getText().toString().trim())) {
                    edt_user_name.setError("Enter Username");
                    edt_user_name.requestFocus();
                } else if (TextUtils.isEmpty(edt_password.getText().toString().trim())) {
                    edt_password.setError("Enter Password");
                    edt_password.requestFocus();
                } else {

                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edt_user_name.getWindowToken(), 0);
                        imm.hideSoftInputFromWindow(edt_password.getWindowToken(), 0);
                    } catch (Exception ignored) {
                    }


                    if (chk_remember_me_admin.isChecked()) {
                        getPref.setAdminUserNamePassword(
                                edt_user_name.getText().toString().trim(),
                                edt_password.getText().toString().trim());
                        getPref.setAdminRememberMe(true);
                    } else {
                        getPref.setAdminUserNamePassword(
                                "",
                                "");
                        getPref.setAdminRememberMe(false);
                    }

                    hideKeyBoard();

                    adminLoginApiCall(edt_user_name.getText().toString().trim(),
                            edt_password.getText().toString().trim(), progressbar_login);


                }
            });

            if (getPref.getAdminRememberMe()) {
                chk_remember_me_admin.setChecked(true);
                edt_password.setText(getPref.getAdminUserPassword());
                edt_user_name.setText(getPref.getAdminUserName());
                edt_user_name.setSelection(getPref.getAdminUserName().length());
            }

            if (dialogAdminLogin.getWindow() != null) {
                dialogAdminLogin.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                dialogAdminLogin.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            dialogAdminLogin.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showOTPPopUp(String OTPNotes, String instruction) {

        try {
            dialogSubmitOTP = new Dialog(RegistrationActivity.this);
            dialogSubmitOTP.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogSubmitOTP.setCancelable(false);
            dialogSubmitOTP.setContentView(R.layout.dialog_admin_login_otp);
            final EditText edt_otp = dialogSubmitOTP.findViewById(R.id.edt_otp);
            ImageView img_close = dialogSubmitOTP.findViewById(R.id.img_close);
            TextView txt_otp_notes = dialogSubmitOTP.findViewById(R.id.txt_otp_notes);
            TextView txt_instruction = dialogSubmitOTP.findViewById(R.id.txt_instruction);
            Button btn_submit_otp = dialogSubmitOTP.findViewById(R.id.btn_submit_otp);

            txt_otp_notes.setText(OTPNotes);
            txt_instruction.setText(instruction);

            img_close.setOnClickListener(view -> dialogSubmitOTP.dismiss());

            btn_submit_otp.setOnClickListener(v -> {

                if (TextUtils.isEmpty(edt_otp.getText().toString().trim())) {
                    edt_otp.setError("Enter OTP");
                    edt_otp.requestFocus();
                } else if (!TextUtils.isEmpty(AdminUserOTP) && !edt_otp.getText().toString().trim().equals(AdminUserOTP)) {
                    edt_otp.setError("Enter Valid OTP");
                    edt_otp.requestFocus();
                } else {
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edt_otp.getWindowToken(), 0);
                    } catch (Exception ignored) {
                    }
                    dialogSubmitOTP.dismiss();
                    redirectInHomeScreen();
                }
            });

            if (dialogSubmitOTP.getWindow() != null) {
                dialogSubmitOTP.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                dialogSubmitOTP.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            dialogSubmitOTP.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Observable<AdminLogin_Response> getAdminLoginApiObservable(String UserName, String Password) {
        AdminLogin_Request request = new AdminLogin_Request(
                UserName,
                Password,
                Config.VERIFY_CALL,
                getPref.getdeviceID(),
                Config.APP_NAME,
                getPref.getimei()
        );
        return apiService.AdminLogin(request);
    }

    public void adminLoginApiCall(String UserName, String Password, final ProgressBar progressbar_login) {
        if (cd != null && cd.isConnectingToInternet()) {
            progressbar_login.setVisibility(View.VISIBLE);
            compositeDisposable.add(RxAPICallHelper.call(getAdminLoginApiObservable(UserName, Password), new RxAPICallback<AdminLogin_Response>() {
                @Override
                public void onSuccess(AdminLogin_Response response) {
                    progressbar_login.setVisibility(View.GONE);
                    if (response.getStatus() != null && response.getStatus() == 1 && response.getData().getAdminLoginDetails() != null && response.getData().getAdminLoginDetails().size() > 0) {

                        AdminLogin_Response.AdminLoginDetail data = response.getData().getAdminLoginDetails().get(0);

                        if (data.getStatus() != null && data.getStatus() == 1) {
                            if (dialogAdminLogin != null) {
                                dialogAdminLogin.dismiss();
                            }
                        }

                        if (data.getUMMarketingUser() == 1) {
                            AdminUserOTP = data.getOTP();
                            showOTPPopUp(data.getOTPNotes(),
                                    data.getOTPMessage());
                            getPref.setAdminUserId(data.getUMUserID());
                        } else if (data.getUMMarketingUser() == 0
                                && data.getStatus() == 1) {
                            getPref.setAdminUserId(data.getUMUserID());
                            redirectInHomeScreen();
                        } else {
                            responseMsg = TextUtils.isEmpty(data.getLoginStatus()) ? badServerResponse : data.getLoginStatus();
                            addApiStatus(APP_CONSTANTS.AdminLogin, API_RESPONSE_STATUS.NO_DATA, responseMsg);
                        }
                    } else {
                        responseMsg = TextUtils.isEmpty(response.getMessage()) ? badServerResponse : response.getMessage();
                        addApiStatus(APP_CONSTANTS.AdminLogin, API_RESPONSE_STATUS.NO_DATA, responseMsg);
                    }
                }

                @Override
                public void onFailed(Throwable throwable) {
                    progressbar_login.setVisibility(View.GONE);
                    addApiStatus(APP_CONSTANTS.AdminLogin, API_RESPONSE_STATUS.FAILED, badServerResponse);
                }
            }));

        } else {
            addApiStatus(APP_CONSTANTS.AdminLogin, API_RESPONSE_STATUS.NO_DATA, badNetwork);
        }
    }


    private void hideKeyBoard() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        try {
            dialogProgress = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            dialogProgress.setTitleText(LoadingMessage);
            dialogProgress.setCancelable(false);
            dialogProgress.show();
        } catch (Exception ignored) {
        }
    }

    private void disMissDialog() {
        if (dialogProgress != null && !isFinishing() && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
        alertDialogBuilder
                .setMessage("Are You Sure Want To Exit?")
                .setCancelable(false)
                .setPositiveButton("YES", (dialog, id) -> {
                    dialog.cancel();
                    finish();
                    System.exit(0);
                });
        alertDialogBuilder.setNegativeButton("NO", (dialog, id) -> dialog.cancel());
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void networkDetection() {
        NetworkLiveData networkLiveData = new NetworkLiveData(RegistrationActivity.this);
        networkLiveData.observe(RegistrationActivity.this, networkModel -> {
            if (networkModel.getIsConnected() == NETWORK_STATUS.DISCONNECTED) {
                showSnackBar("", badNetwork, false);
            }
        });
    }

    public void redirectInHomeScreen() {
        addApiStatus(APP_CONSTANTS.Get_TabVerifyLogin, API_RESPONSE_STATUS.SUCCESS, getResources().getString(R.string.login_success));
        getPref.setIsLogin(true);
        handler.postDelayed(() -> {
            Intent intent = new Intent(RegistrationActivity.this, ExeBookingActivity_V2.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }, 500);

    }

    private void clearDisposable() {
        try {
            if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
                compositeDisposable.clear();
                compositeDisposable.dispose();
            }
        } catch (Exception ignored) {
        }
    }

    private void showSnackBar(final String method, String desc, boolean isRetry) {
        if (!isFinishing()) {
            if (TextUtils.isEmpty(desc)) {
                desc = badServerResponse;
            }
            int snackBarDuration = method.equals(APP_CONSTANTS.Get_CompanyList) ? Snackbar.LENGTH_INDEFINITE : Snackbar.LENGTH_LONG;
            Snackbar mSnackBar = Snackbar.make(binding.mainLayout, desc, snackBarDuration);
            if (isRetry) {
                mSnackBar.setAction(getResources().getString(R.string.retry), view -> {
                    switch (method) {
                        case APP_CONSTANTS.Get_CompanyList:
                            getCompanyListApiCall();
                            break;
                        case APP_CONSTANTS.Get_BranchList:
                            getBranchListApiCall();
                            break;
                        case APP_CONSTANTS.Get_TabVerifyLogin:
                            Get_TabVerifyLoginApiCall();
                            break;
                    }
                }).setActionTextColor(getResources().getColor(R.color.white));
            }
            mSnackBar.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearDisposable();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

}