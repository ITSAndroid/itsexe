package com.infinityinfoway.itsexe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class ExceptionHandlerActivity extends Activity {

    TextView tv_ErrorMsg;
    ConnectionDetector cd;
    String caller = "";
    Bundle getPutData;
    private String dnt_report;
    private String stacktrace_report;
    private String deviceinfo_report;
    private String activity_name;
    private String versionname;
    private String ipaddress;
    private String android_id;
    private ITSExeSharedPref getPref;
    private String username;
    private ImageButton btn_retry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exception_handler);
        tv_ErrorMsg = findViewById(R.id.tv_ErrorMsg);
        btn_retry = findViewById(R.id.btn_Retry);
        getPref = new ITSExeSharedPref(ExceptionHandlerActivity.this);
        cd = new ConnectionDetector(ExceptionHandlerActivity.this);
        getPutData = getIntent().getExtras();
        if (getPutData != null) {
            tv_ErrorMsg.setText("Error Occurred, Please try again");
            dnt_report = getPutData.getString("DNT_REPORT");
            stacktrace_report = getPutData.getString("STACKTRACE_REPORT");
            deviceinfo_report = getPutData.getString("DEVICEINFO_REPORT");
            activity_name = getPutData.getString("ACTIVITY_NAME");
            versionname = getPutData.getString("VERSIONNAME_REPORT");
            ipaddress = getIpAddress();
            username = getPref.getBUM_UserName();


            android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);



        }
        btn_retry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(ExceptionHandlerActivity.this, SplashScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
    }

    public void onBackPressed() {
        Intent i = new Intent(ExceptionHandlerActivity.this, SplashScreen.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public static String getIpAddress() {
        try {
            for (@SuppressWarnings("rawtypes")
                 Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (@SuppressWarnings("rawtypes")
                     Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ignored) {
        }
        return "";
    }


}
