package com.infinityinfoway.itsexe.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.SeatArrangement_Request;
import com.infinityinfoway.itsexe.api.request.SeatHold_Request;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.api.response.SeatHold_Response;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.custom_dialog.OptAnimationLoader;
import com.infinityinfoway.itsexe.custom_dialog.SweetAlertDialog;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySeatHoldBinding;
import com.infinityinfoway.itsexe.databinding.DialogCustomCalendarBinding;
import com.infinityinfoway.itsexe.utils.APP_CONSTANTS;
import com.infinityinfoway.itsexe.utils.CustomCalendarView;
import com.infinityinfoway.itsexe.utils.CustomSnackBar;
import com.infinityinfoway.itsexe.utils.ExceptionHandler;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatHoldActivity extends AppCompatActivity implements View.OnClickListener {

    private ITSExeSharedPref getPref;
    private final int borderColor = Color.parseColor("#FF0000");
    private int max_column_length;
    private String PassFromDate, PassToDate;
    private Date fromDate, toDate;
    private List<String> list_selected_day;
    private String PassSeatBlockType, PassDaysCSV, PassSeatList;
    private List<String> listSelectedSeatList;
    private ApiInterface apiService;
    private ActivitySeatHoldBinding binding;
    private List<SeatArrangement_Response.Datum> listSeatDetails = new ArrayList<>();
    private String jStartTime, jCityTime, sel_JourneyDate, isSameDay, emptySeatColor, routeDetails;
    private int sel_CompanyID, sel_RouteID, sel_TimeID, sel_FromCityID, sel_ToCityID;
    private final SimpleDateFormat sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
    private long lastClickTime = 0;
    private PopupWindow popupModificationDialog;
    private SweetAlertDialog dialogSuccess, dialogLoadTransactions, dialogProgress;
    private DisplayMetrics displaymetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySeatHoldBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        String TAG="SeatHoldActivity";
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this, TAG));

        displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        if (getIntent().hasExtra("RouteId")) {
            jStartTime = getIntent().getStringExtra("StartTime");
            jCityTime = getIntent().getStringExtra("CityTime");
            sel_CompanyID = getIntent().getIntExtra("CompanyId", 0);
            sel_RouteID = getIntent().getIntExtra("RouteId", 0);
            sel_TimeID = getIntent().getIntExtra("RouteTimeId", 0);
            sel_FromCityID = getIntent().getIntExtra("FromCityId", 0);
            sel_ToCityID = getIntent().getIntExtra("ToCityId", 0);
            sel_JourneyDate = getIntent().getStringExtra("JourneyDate");
            isSameDay = getIntent().getStringExtra("IsSameDay");
            emptySeatColor = getIntent().getStringExtra("EmptySeatColor");
            routeDetails = getIntent().getStringExtra("RouteDetails");
        }


        binding.layoutBase.backBtn.setVisibility(View.VISIBLE);
        binding.layoutBase.backBtn.setOnClickListener(view1 -> setResultData(0));

        binding.layoutBase.txtTitle.setText(getResources().getString(R.string.block_seat));

        try {
            fromDate = sdf_full.parse(sel_JourneyDate);
            toDate = fromDate;
            assert fromDate != null;
            PassFromDate = sdf_full.format(fromDate);
            PassToDate = sdf_full.format(toDate);
            binding.txtFromDate.setText(sdf_full.format(fromDate));
            binding.txtToDate.setText(sdf_full.format(toDate));
        } catch (Exception ignored) { }

        apiService = ApiClient.getClient().create(ApiInterface.class);

        getPref = new ITSExeSharedPref(SeatHoldActivity.this);

        binding.tvBusDetail.setText(routeDetails);


        binding.radioGrpBlockSeat.setOnCheckedChangeListener((radioGroup, i) -> {
            binding.llCustomDateSelection.setVisibility(View.GONE);
            binding.flexSeatDetailContainer.setVisibility(View.GONE);

            if (radioGroup.getCheckedRadioButtonId() == R.id.rb_custom) {
                binding.llCustomDateSelection.setVisibility(View.VISIBLE);
                binding.flexSeatDetailContainer.setVisibility(View.VISIBLE);
            } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_permanent) {
                binding.flexSeatDetailContainer.setVisibility(View.VISIBLE);
            }
        });

        binding.rbCurrentChart.setChecked(true);

        listSelectedSeatList = new ArrayList<>();
        binding.txtFromDate.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }
            openDateDialog(binding.txtFromDate, 0);
        });

        binding.txtToDate.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();
            } catch (Exception ignored) {
            }
            openDateDialog(binding.txtToDate, 1);
        });

        binding.btnSeatBlock.setOnClickListener(v -> {
            binding.btnSeatBlock.setEnabled(false);
            seatHoldApiCall("1");
        });

        binding.btnUnblockSeat.setOnClickListener(v -> {
            binding.btnUnblockSeat.setEnabled(false);
            seatHoldApiCall("0");
        });

        binding.btnCancel.setOnClickListener(v -> {
            listSelectedSeatList.clear();
            binding.txtSelectedSeats.setText("");
            if (listSeatDetails != null && listSeatDetails.size() > 0) {
                setSeats(0);
            }
        });

        binding.chkSelectUnselectAllSeats.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (listSeatDetails != null && listSeatDetails.size() > 0) {
                listSelectedSeatList.clear();
                setSeats(isChecked ? 1 : 0);
            }
        });

        SeatArrangementApiCall();
    }

    private void releaseButtonFlag() {
        binding.btnSeatBlock.setEnabled(true);
        binding.btnUnblockSeat.setEnabled(true);
    }

    private void seatHoldApiCall(String tranStatus) {
        int selected_ID = binding.radioGrpBlockSeat.getCheckedRadioButtonId();
        RadioButton radioButton_selected = findViewById(selected_ID);
        if (radioButton_selected == null) {
            showSnackBar("Oops", "Select Block Type", Snackbar.LENGTH_LONG);
            releaseButtonFlag();
        } else {
            if (radioButton_selected.getText().toString().equalsIgnoreCase("Custom")) {
                PassSeatBlockType = "0";
            } else if (radioButton_selected.getText().toString().equalsIgnoreCase("Permanent")) {
                PassSeatBlockType = "1";
            } else if (radioButton_selected.getText().toString().equalsIgnoreCase("Current Chart")) {
                PassSeatBlockType = "2";
            }
        }

        if (PassSeatBlockType == null || PassSeatBlockType.equals("")) {
            showSnackBar("Oops", "Select Block Type", Snackbar.LENGTH_LONG);
            releaseButtonFlag();
        } else if (listSelectedSeatList == null || listSelectedSeatList.size() <= 0) {
            showSnackBar("Oops", "Select Seat", Snackbar.LENGTH_LONG);
            releaseButtonFlag();
        } else if (PassSeatBlockType.equals("0") && (PassFromDate == null || PassFromDate.equals(""))) {
            showSnackBar("Oops", "Select From Journey Date", Snackbar.LENGTH_LONG);
            releaseButtonFlag();
        } else if (PassSeatBlockType.equals("0") && (PassToDate == null || PassToDate.equals(""))) {
            showSnackBar("Oops", "Select To Journey Date", Snackbar.LENGTH_LONG);
            releaseButtonFlag();
        } else {

            String dialogTitle = tranStatus.equals("0") ? "Seat UnBlock" : "Seat Block";
            try {
                dialogLoadTransactions = new SweetAlertDialog(SeatHoldActivity.this, SweetAlertDialog.WARNING_TYPE);
                dialogLoadTransactions.show();
                dialogLoadTransactions.setTitleText(dialogTitle);
                dialogLoadTransactions.setContentText("Are You Sure , You Want To " + dialogTitle + " ?");
                dialogLoadTransactions.setCancelable(false);
                dialogLoadTransactions.setConfirmText(getResources().getString(R.string.yes));
                dialogLoadTransactions.setCancelText(getResources().getString(R.string.no));


                dialogLoadTransactions.setConfirmClickListener(sweetAlertDialog -> {

                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                    } catch (Exception ignored) {
                    }

                    if (dialogLoadTransactions != null && dialogLoadTransactions.isShowing()) {

                        dialogLoadTransactions.dismissWithAnimation();
                        dialogLoadTransactions.cancel();

                        if (PassSeatBlockType.equals("1") || PassSeatBlockType.equals("2")) {
                            PassFromDate = sel_JourneyDate;
                            PassToDate = sel_JourneyDate;

                        } else {
                            PassFromDate = binding.txtFromDate.getText().toString();
                            PassToDate = binding.txtToDate.getText().toString();
                        }

                        if (PassSeatBlockType.equals("2")) {
                            PassDaysCSV = "";
                        } else {
                            fetchDays();
                        }

                        if (list_selected_day != null && list_selected_day.size() > 0 && !PassSeatBlockType.equals("2")) {
                            PassDaysCSV = TextUtils.join(",", list_selected_day);
                        } else {
                            PassDaysCSV = "";
                        }

                        PassSeatList = TextUtils.join(",", listSelectedSeatList);

                        showProgressDialog("Loading...");

                        SeatHold_Request request = new SeatHold_Request(
                                sel_CompanyID,
                                sel_JourneyDate,
                                sel_RouteID,
                                sel_TimeID,
                                sel_FromCityID,
                                sel_ToCityID,
                                jStartTime,
                                jCityTime,
                                getPref.getBM_BranchID(),
                                getPref.getBUM_BranchUserID(),
                                PassSeatBlockType,
                                PassFromDate,
                                PassToDate,
                                PassDaysCSV,
                                PassSeatList,
                                tranStatus,
                                binding.etRemarks.getText().toString().trim()
                        );

                        Call<SeatHold_Response> call = apiService.SeatHold(request);
                        call.enqueue(new Callback<SeatHold_Response>() {
                            @Override
                            public void onResponse(@NonNull Call<SeatHold_Response> call, @NonNull Response<SeatHold_Response> response) {
                                disMissDialog();
                                releaseButtonFlag();
                                if (response.isSuccessful() && response.body() != null) {
                                    if (response.body().getStatus() == 1 && response.body().getData() != null && response.body().getData().size() > 0 && response.body().getData().get(0).getStatus() != null && response.body().getData().get(0).getStatus() == 1) {
                                        dialogSuccess = new SweetAlertDialog(SeatHoldActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                                        if (!isFinishing())
                                            dialogSuccess.show();

                                        dialogSuccess.setTitleText(dialogTitle);
                                        dialogSuccess.setContentText(response.body().getData().get(0).getStatusMsg());
                                        dialogSuccess.setCancelable(false);

                                        dialogSuccess.setConfirmClickListener(sweetAlertDialog1 -> {
                                            dialogSuccess.dismissWithAnimation();
                                            dialogSuccess.cancel();
                                            setResultData(1);
                                        });
                                    } else {
                                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                                            showSnackBar(getResources().getString(R.string.dialog_error_title), response.body().getData().get(0).getStatusMsg(), Toast.LENGTH_LONG);
                                        } else {
                                            showSnackBar(getResources().getString(R.string.dialog_error_title), response.body().getMessage(), Toast.LENGTH_LONG);
                                        }
                                    }
                                } else {
                                    showSnackBar(getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_request), Toast.LENGTH_LONG);
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<SeatHold_Response> call, @NonNull Throwable t) {
                                disMissDialog();
                                showSnackBar(getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response), Toast.LENGTH_LONG);
                                releaseButtonFlag();
                            }
                        });
                    }
                });

                dialogLoadTransactions.setCancelClickListener(sweetAlertDialog -> {
                    dialogLoadTransactions.dismissWithAnimation();
                    dialogLoadTransactions.cancel();
                    releaseButtonFlag();
                });
            } catch (Exception ignored) {
                releaseButtonFlag();
            }
        }
    }

    public void setSeats(int flagSelectAllSeats) {
        int upperSeatCounter = 0;

        if (binding.llUpLower.getChildCount() > 0) {
            binding.llUpLower.removeAllViews();
        }

        binding.llArrangement.removeAllViews();

        for (SeatArrangement_Response.Datum dataSeat : listSeatDetails) {


            if (dataSeat.getCADUDType() != null && dataSeat.getCADUDType() == 0) {
                if (max_column_length < dataSeat.getCADCell()) {
                    max_column_length = dataSeat.getCADCell();
                }
                upperSeatCounter++;
            }

        }

        RelativeLayout.LayoutParams params;
        float textSize = getResources().getDimension(R.dimen.text_size);
        int width = displaymetrics.widthPixels;

        int buttonWidth;
        int buttonHeight;
        int buttonSpace;

        width = width / 3;


        for (int i = 0; i < listSeatDetails.size(); i++) {

            SeatArrangement_Response.Datum data = listSeatDetails.get(i);

            if (data.getCADBlockType() != null && data.getCADBlockType() == 3) {
                if (data.getIsAllowProcess() == 0) {
                    continue;
                }
            }

            if (TextUtils.isEmpty(data.getBADSeatNo())) {
                continue;
            }

            final Button btn_Seat = new Button(SeatHoldActivity.this);

           /* if (max_column_length > 9) {
                buttonWidth = ((width) / 5);
                buttonHeight = width / 10;
                buttonSpace = (width / 9) - 1;
            } else {
                    buttonWidth = (width / 4);
                    buttonHeight = width / 9;
                buttonSpace = (width / 8) - 1;
            }*/

            if (max_column_length > 9) {
                buttonWidth = ((width) / 5);
                buttonHeight = width / 10;
                buttonSpace = (width / 9) - 1;
            } else {
                buttonWidth = (width / 6);
                buttonHeight = width / 12;
                buttonSpace = (width / 12) + 1;
            }

            btn_Seat.setPadding(-4, -4, -4, -4);


            btn_Seat.setPadding(-4, -4, -4, -4);


            if (data.getCADColspan() != null && data.getCADColspan() > 0) {
                params = new RelativeLayout.LayoutParams(buttonWidth, buttonHeight);
            } else if (data.getCADRowSpan() != null && data.getCADRowSpan() > 0) {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonWidth);
            } else {
                params = new RelativeLayout.LayoutParams(buttonHeight, buttonHeight);
            }

            if (max_column_length >= 8) {
                params.setMargins(((data.getCADCell()) * buttonSpace), ((data.getCADRow() - 1) * buttonSpace), 0, 0);

                if (data.getCADRow() == 1) {
                    if (data.getCADCell() == 1) {
                        params.setMargins(((data.getCADCell()) * buttonSpace), 0, 0, 0);
                    }
                }

            } else {
                params.setMargins(((data.getCADCell() - 1) * buttonSpace), ((data.getCADRow() - 1) * buttonSpace), 0, 0);

                if (data.getCADRow() == 1) {
                    if (data.getCADCell() == 1) {
                        params.setMargins(((data.getCADCell() - 1) * buttonSpace), 0, 0, 0);
                    }
                }

            }

            btn_Seat.setId(i);
            btn_Seat.setLayoutParams(params);
            btn_Seat.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            btn_Seat.setTypeface(null, Typeface.BOLD);
            btn_Seat.setText(data.getBADSeatNo());
            btn_Seat.setOnClickListener(this);

            if (flagSelectAllSeats == 1) {
                setSeatBackground(btn_Seat, APP_CONSTANTS.SELECTED_SEAT_COLOR);
                listSelectedSeatList.add(listSeatDetails.get(i).getBADSeatNo());
                btn_Seat.setSelected(true);
            } else {
                setSeatBackground(btn_Seat, emptySeatColor);
                btn_Seat.setSelected(false);
            }

            if (data.getCADRow() == 1 && upperSeatCounter > 0) {
                Button btn_UpLow = new Button(SeatHoldActivity.this);
                btn_UpLow.setBackgroundResource(android.R.color.transparent);

                params = new RelativeLayout.LayoutParams(buttonHeight, buttonHeight);
                if (max_column_length >= 8) {
                    params.leftMargin = ((data.getCADCell()) * buttonSpace) + 10;

                    if (data.getCADRow() == 1) {
                        if (data.getCADCell() == 1) {
                            params.leftMargin = data.getCADCell() * buttonSpace;
                        }
                    }

                } else {
                    params.leftMargin = ((data.getCADCell() - 1) * buttonSpace);
                    if (data.getCADRow() == 1) {
                        if (data.getCADCell() == 1) {
                            params.leftMargin = ((data.getCADCell() - 1) * buttonSpace);
                        }
                    }
                }

                params.topMargin = 0;
                params.rightMargin = 0;
                params.bottomMargin = 0;

                if (data.getCADUDType() == 0) {
                    btn_UpLow.setBackgroundResource(R.drawable.arr_ub);
                } else {
                    btn_UpLow.setBackgroundResource(R.drawable.arr_lb);
                }
                btn_UpLow.setLayoutParams(params);

                binding.llUpLower.addView(btn_UpLow);
                binding.llUpLower.setVisibility(View.VISIBLE);

            }
            binding.llArrangement.addView(btn_Seat);
        }
        binding.txtSelectedSeats.setText(android.text.TextUtils.join(",", listSelectedSeatList));
    }


    @Override
    public void onClick(View arg0) {
        if (listSeatDetails.size() > 0) {
            if (arg0.isSelected()) {
                setSeatBackground((Button) arg0, emptySeatColor);
                arg0.setSelected(false);
                listSelectedSeatList.remove(listSeatDetails.get(arg0.getId()).getBADSeatNo());
            } else {
                setSeatBackground((Button) arg0, APP_CONSTANTS.SELECTED_SEAT_COLOR);
                arg0.setSelected(true);
                listSelectedSeatList.add(listSeatDetails.get(arg0.getId()).getBADSeatNo());
            }
            binding.txtSelectedSeats.setText(android.text.TextUtils.join(",", listSelectedSeatList));
        }
    }

    public void setSeatBackground(Button btn_Seat, String bookingTypeColor) {
        int startColor;
        if (bookingTypeColor.contains("#")) {
            startColor = Color.parseColor(bookingTypeColor);
        } else {
            startColor = Color.parseColor("#" + bookingTypeColor);
        }

        int[] colors = {startColor, startColor};
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);


        gradientDrawable.setStroke(1, borderColor);
        btn_Seat.setBackground(gradientDrawable);
        btn_Seat.setTextColor(getComplementaryColor(startColor));
        btn_Seat.setTag(startColor);

    }

    private int getComplementaryColor(int color) {
        int R = color & 255;
        int G = (color >> 8) & 255;
        int B = (color >> 16) & 255;
        int A = (color >> 24) & 255;
        R = 255 - R;
        G = 255 - G;
        B = 255 - B;
        return R + (G << 8) + (B << 16) + (A << 24);
    }

    private void SeatArrangementApiCall() {

        showProgressDialog("Loading");

        SeatArrangement_Request request = new SeatArrangement_Request(
                sel_CompanyID,
                sel_JourneyDate,
                sel_RouteID,
                sel_TimeID,
                sel_FromCityID,
                sel_ToCityID,
                getPref.getBM_BranchID(),
                getPref.getBUM_BranchUserID(),
                Config.VERIFY_CALL,
                isSameDay
        );

        Call<SeatArrangement_Response> call = apiService.SeatArrangement(request);
        call.enqueue(new Callback<SeatArrangement_Response>() {
            @Override
            public void onResponse(@NotNull Call<SeatArrangement_Response> call, @NotNull Response<SeatArrangement_Response> response) {
                disMissDialog();
                if (response.isSuccessful() && response.body() != null && response.body().getData().getData() != null && response.body().getData().getData().size() > 0) {
                    listSeatDetails = response.body().getData().getData();
                    setSeats(0);
                } else {
                    showSnackBar("Oops", "Try Again , Unable To Fetch Server Response", Snackbar.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(@NotNull Call<SeatArrangement_Response> call, @NotNull Throwable t) {
                disMissDialog();
                showSnackBar("Oops", "Try Again , Unable To Fetch Server Response", Snackbar.LENGTH_LONG);
            }
        });

    }

    private void disMissDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

    private void showProgressDialog(String LoadingMessage) {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }

        try {
            dialogProgress = new SweetAlertDialog(SeatHoldActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            dialogProgress.setTitleText(LoadingMessage);
            dialogProgress.setCancelable(false);
            dialogProgress.show();
        } catch (Exception ignored) {
        }
    }

    private void showSnackBar(String title, String desc, int duration) {

        if (!isFinishing()) {
            int is_top = 0;
            final Snackbar snackbar = CustomSnackBar.showSnackbar(SeatHoldActivity.this, binding.llMain, title, desc, duration, is_top);
            snackbar.show();

            ImageView imgError = snackbar.getView().findViewById(R.id.img_error);
            imgError.clearAnimation();
            imgError.setVisibility(View.VISIBLE);

            imgError.setAnimation(OptAnimationLoader.loadAnimation(SeatHoldActivity.this, R.anim.error_x_in));

        }

    }

    private void openDateDialog(TextView txtDate, int flag) {

        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
            popupModificationDialog.dismiss();
        }

        Date minDate = null, maxDate;
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 5);
        maxDate = cal.getTime();

        if (flag == 0) {
            try {
                minDate = sdf_full.parse(sel_JourneyDate);
                maxDate = sdf_full.parse(sdf_full.format(maxDate));
            } catch (Exception ignored) {
            }
        } else {
            try {
                if (!TextUtils.isEmpty(PassFromDate)) {
                    minDate = sdf_full.parse(PassFromDate);
                } else {
                    minDate = sdf_full.parse(sel_JourneyDate);
                }
                maxDate = sdf_full.parse(sdf_full.format(maxDate));

            } catch (Exception ignored) {
            }
        }

        popupModificationDialog = new PopupWindow();
        popupModificationDialog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        if (displaymetrics != null) {
            popupModificationDialog.setWidth(displaymetrics.widthPixels / 4);
        } else {
            popupModificationDialog.setWidth(250);
        }

        // popup.setWidth(200);
        popupModificationDialog.setOutsideTouchable(true);
        popupModificationDialog.setFocusable(true);
        // popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        final DialogCustomCalendarBinding calendarBinding = DialogCustomCalendarBinding.inflate(getLayoutInflater());
        View view = calendarBinding.getRoot();


        popupModificationDialog.setContentView(view);
        //popupModificationDialog.showAsDropDown(btnSeat,0,0, Gravity.BOTTOM);
        popupModificationDialog.showAsDropDown(txtDate);

        Date finalMaxDate = maxDate;
        Date finalMinDate = minDate;
        calendarBinding.robotoCalendarPicker.setRobotoCalendarListener(new CustomCalendarView.RobotoCalendarListener() {
            @Override
            public void onDayClick(Date date) {

                if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                    popupModificationDialog.dismiss();
                }

                String valid_until = sdf_full.format(date);
                Date strDate = null;
                try {
                    strDate = sdf_full.parse(valid_until);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (strDate != null) {
                    if (finalMinDate != null && finalMaxDate != null && strDate.getTime() >= finalMinDate.getTime() && strDate.getTime() <= finalMaxDate.getTime()) {
                        if (popupModificationDialog != null && popupModificationDialog.isShowing()) {
                            popupModificationDialog.dismiss();
                        }
                        if (flag == 0) {
                            fromDate = date;
                            PassFromDate = sdf_full.format(date);
                            binding.txtFromDate.setText(PassFromDate);
                            if (fromDate.getTime() > toDate.getTime()) {
                                toDate = date;
                                PassToDate = PassFromDate;
                                binding.txtToDate.setText(PassToDate);
                            }
                        } else {
                            toDate = date;
                            PassToDate = sdf_full.format(date);
                            binding.txtToDate.setText(PassToDate);
                        }
                    } else {
                        Toast.makeText(SeatHoldActivity.this, "You can't select this date", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onDayLongClick(Date date) {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onLeftButtonClick() {

            }
        });
        calendarBinding.robotoCalendarPicker.setShortWeekDays(false);
        calendarBinding.robotoCalendarPicker.showDateTitle(true);


        if (maxDate != null) {
            try {
                calendarBinding.robotoCalendarPicker.setEndDate(maxDate);
            } catch (Exception ignored) {
            }
        }

        if (minDate != null) {
            calendarBinding.robotoCalendarPicker.setDate(minDate, minDate);
        }

    }

    private void fetchDays() {
        list_selected_day = new ArrayList<>();
        if (binding.chkSunday.isChecked()) {
            list_selected_day.add("0");
        }
        if (binding.chkMonday.isChecked()) {
            list_selected_day.add("1");
        }
        if (binding.chkTuesday.isChecked()) {
            list_selected_day.add("2");
        }
        if (binding.chkWednesday.isChecked()) {
            list_selected_day.add("3");
        }
        if (binding.chkThursday.isChecked()) {
            list_selected_day.add("4");
        }
        if (binding.chkFriday.isChecked()) {
            list_selected_day.add("5");
        }
        if (binding.chkSaturday.isChecked()) {
            list_selected_day.add("6");
        }
    }

    public void onBackPressed() {
        setResultData(0);
    }

    private void setResultData(int resultCode) {
        Intent intent = new Intent();
        setResult(resultCode, intent);
        finish();
    }

}
