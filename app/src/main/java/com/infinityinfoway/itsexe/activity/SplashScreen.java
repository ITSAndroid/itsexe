package com.infinityinfoway.itsexe.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.infinityinfoway.itsexe.R;
import com.infinityinfoway.itsexe.api.ApiClient;
import com.infinityinfoway.itsexe.api.ApiInterface;
import com.infinityinfoway.itsexe.api.request.Get_TabCheckRegistration_Request;
import com.infinityinfoway.itsexe.api.request.checkVersionInfo_Request;
import com.infinityinfoway.itsexe.api.response.Get_TabCheckRegistration_Response;
import com.infinityinfoway.itsexe.api.response.checkVersionInfo_Response;
import com.infinityinfoway.itsexe.bean.ConnectionDetector;
import com.infinityinfoway.itsexe.config.Config;
import com.infinityinfoway.itsexe.custom_dialog.SweetAlertDialog;
import com.infinityinfoway.itsexe.database.ITSExeSharedPref;
import com.infinityinfoway.itsexe.databinding.ActivitySplashBinding;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NewApi")
public class SplashScreen extends AppCompatActivity {
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    public static String AndroidID;
    int currentApiVersion = 0;
    String IMEI = "";
    String deviceName = "";
    String DeviceModel = "";
    String DeviceProduct = "";
    String DeviceUser = "";
    ConnectionDetector cd;
    public String PACKAGE_NAME;
    public String versionName, android_id, appName;
    public int versioncode;
    private ITSExeSharedPref getPref;
    private SweetAlertDialog dialogSuccess, dialogProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySplashBinding binding = ActivitySplashBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        cd = new ConnectionDetector(SplashScreen.this);

        currentApiVersion = android.os.Build.VERSION.SDK_INT;
        AndroidID = Secure.getString(getContentResolver(), Secure.ANDROID_ID);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (Exception ignored) {
        }

        if (pInfo != null) {
            versionName = pInfo.versionName;
            versioncode = pInfo.versionCode;
        }


        getPref = new ITSExeSharedPref(SplashScreen.this);
        getPref.saveVersionInfo(versioncode, versionName);
        PACKAGE_NAME = getApplicationContext().getPackageName();
        getPref.setOSDeviceID(AndroidID);

        IMEI = UUID.randomUUID().toString();
        deviceName = Build.MANUFACTURER;
        DeviceModel = Build.MODEL;
        DeviceProduct = Build.PRODUCT;
        DeviceUser = AndroidID;

        getPref.savePref(
                DeviceUser,
                AndroidID,
                currentApiVersion,
                IMEI,
                DeviceModel,
                deviceName,
                DeviceProduct);

        checkVersionInfoApiCall();
        // get_TabCheckRegistrationApiCall();


    }

    private void checkVersionInfoApiCall() {
        if (cd.isConnectingToInternet()) {
            showProgressDialog();

            checkVersionInfo_Request request = new checkVersionInfo_Request(
                    versioncode,
                    0,
                    AndroidID,
                    versionName
            );

            Call<checkVersionInfo_Response> call = apiService.checkVersionInfo(request);
            call.enqueue(new Callback<checkVersionInfo_Response>() {
                @Override
                public void onResponse(@NotNull Call<checkVersionInfo_Response> call, @NotNull Response<checkVersionInfo_Response> response) {
                    disMissDialog();
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            checkVersionInfo_Response.Datum data = response.body().getData().get(0);
                            if (versioncode < data.getVersionCode() && data.getUpdateSeverity() != 0) {
                                showVersionUpdateDialog(data.getUpdateSeverity());
                            } else {
                                get_TabCheckRegistrationApiCall();
                            }
                        } else {
                            showInternetConnectionDialog(0, "checkVersionInfo", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
                        }
                    } else {
                        showInternetConnectionDialog(0, "checkVersionInfo", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<checkVersionInfo_Response> call, @NotNull Throwable t) {
                    disMissDialog();
                    showInternetConnectionDialog(0, "checkVersionInfo", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
                }
            });

        } else {
            showInternetConnectionDialog(-1, "checkVersionInfo", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.no_internet_long_msg));
        }
    }

    private void get_TabCheckRegistrationApiCall() {

        if (cd.isConnectingToInternet()) {
            showProgressDialog();

            Get_TabCheckRegistration_Request request = new Get_TabCheckRegistration_Request(
                    getPref.getOSDeviceID(),
                    getPref.getTBRM_ID()
            );

            Call<Get_TabCheckRegistration_Response> call = apiService.Get_TabCheckRegistration(request);


            call.enqueue(new Callback<Get_TabCheckRegistration_Response>() {
                @Override
                public void onResponse(@NotNull Call<Get_TabCheckRegistration_Response> call, @NotNull Response<Get_TabCheckRegistration_Response> response) {
                    disMissDialog();
                    if (response.isSuccessful() && response.body() != null) {
                        int registrationStatus = response.body().getData().getRegistrationStatus();

                        if (registrationStatus != 0) {

                            if (response.body().getData().getRegistration() != null && response.body().getData().getRegistration().size() > 0) {
                                getPref.setTBRM_ID(String.valueOf(response.body().getData().getRegistration().get(0).getTBRMID()));
                            }

                            if (registrationStatus == 1) {
                                // Active (Register and Approved)

                                Intent intent = new Intent(SplashScreen.this, RegistrationActivity.class);
                                startActivity(intent);
                                finish();


                            } else if (registrationStatus == 2) {
                                // Delete,Pending,etc
                                showInternetConnectionDialog(2, "", getResources().getString(R.string.dialog_error_title), response.body().getData().getRegistrationMessage());
                            }

                        } else {
                            // Fresh Registration
                            Intent intent = new Intent(SplashScreen.this, DeviceRegistrationActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        showInternetConnectionDialog(0, "Get_TabCheckRegistration", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<Get_TabCheckRegistration_Response> call, @NotNull Throwable t) {
                    disMissDialog();
                    showInternetConnectionDialog(0, "Get_TabCheckRegistration", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.bad_server_response));
                }
            });
        } else {
            showInternetConnectionDialog(-1, "Get_TabCheckRegistration", getResources().getString(R.string.dialog_error_title), getResources().getString(R.string.no_internet_long_msg));
        }


    }

    private void showInternetConnectionDialog(final int dialogType, final String method, String title, String message) {
        if (!isFinishing()) {
            if (dialogType == -1) {
                dialogSuccess = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                dialogSuccess.setCustomImage(R.drawable.ic_network);
            } else if (dialogType == 0) {
                dialogSuccess = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.ERROR_TYPE);
            } else if (dialogType == 1) {
                dialogSuccess = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.SUCCESS_TYPE);
            } else if (dialogType == 2) {
                dialogSuccess = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.WARNING_TYPE);
            }

            dialogSuccess.show();

            dialogSuccess.setTitleText(title);
            dialogSuccess.setContentText(message);



       /* TextView text = dialogSuccess.findViewById(R.id.content_text);
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setText(message);
        text.setLines(5);*/

            dialogSuccess.setCancelable(false);


            if (dialogType == -1) {
                dialogSuccess.setConfirmText(getResources().getString(R.string.retry));
            } else {
                dialogSuccess.setConfirmText(getResources().getString(R.string.ok));
            }

            dialogSuccess.setConfirmClickListener(sweetAlertDialog -> {
                dialogSuccess.dismissWithAnimation();
                dialogSuccess.cancel();

                if (dialogType == 2) {
                    finish();
                } else {
                    switch (method) {
                        case "Get_TabCheckRegistration":
                            get_TabCheckRegistrationApiCall();
                            break;
                        case "checkVersionInfo":
                            checkVersionInfoApiCall();
                            break;

                    }
                }

            });

        }

    }

    private void disMissDialog() {
        if (dialogProgress != null && !isFinishing() && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

    private void showProgressDialog() {
        try {
            dialogProgress = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.PROGRESS_TYPE);
            dialogProgress.setTitleText(getResources().getString(R.string.loading));
            dialogProgress.setCancelable(false);
            dialogProgress.show();
        } catch (Exception ignored) {
        }
    }

    private void showVersionUpdateDialog(int updateSeverity) {
        dialogSuccess = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.SUCCESS_TYPE);
        dialogSuccess.show();

        dialogSuccess.setCancelable(false);
        dialogSuccess.setTitleText(getResources().getString(R.string.new_update));
        dialogSuccess.setContentText(getResources().getString(R.string.new_version));

        dialogSuccess.setConfirmText(getResources().getString(R.string.update_now));

        dialogSuccess.setConfirmClickListener(sweetAlertDialog -> {
            dialogSuccess.dismissWithAnimation();
            dialogSuccess.cancel();

            try {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("market://details?id=" + Config.PACKAGE_NAME));
                i.setPackage("com.android.vending");
                startActivity(i);
                finish();
            } catch (ActivityNotFoundException ex) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(
                        "https://play.google.com/store/apps/details?id=" + Config.PACKAGE_NAME));
                intent.setPackage("com.android.vending");
                startActivity(intent);
                finish();
            }

        });

        if (updateSeverity == 1) {
            dialogSuccess.setCancelText(getResources().getString(R.string.not_now));
            dialogSuccess.showCancelButton(true);
            dialogSuccess.setCancelClickListener(sweetAlertDialog -> {
                dialogSuccess.dismissWithAnimation();
                dialogSuccess.cancel();
                get_TabCheckRegistrationApiCall();

            });
        }


    }


}