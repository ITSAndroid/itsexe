package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.bean.SelectedSeatDetailsBean;

import java.util.List;

public class SeatSelected_ViewModel extends ViewModel {
    public MutableLiveData<List<SelectedSeatDetailsBean>> mutableLiveData = new MutableLiveData<>();

    public void addSeats(List<SelectedSeatDetailsBean> seats) {
        mutableLiveData.setValue(seats);
    }

    public MutableLiveData<List<SelectedSeatDetailsBean>> getSelectedSeats() {
        return mutableLiveData;
    }

}
