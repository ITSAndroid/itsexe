package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeXML_Request;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeXML_Response;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_repository.Get_API_EXE_RouteTimeXML_ViewRepository;

import java.util.List;

public class Get_API_EXE_RouteTimeXML_ViewModel extends ViewModel {
    public MutableLiveData<List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML>> routeXmlMutableLiveData = new MutableLiveData<>();
    private Get_API_EXE_RouteTimeXML_ViewRepository repository;

    private void createRepository() {
        if (repository == null) {
            repository = new Get_API_EXE_RouteTimeXML_ViewRepository();
        }
    }

    public void onCreateRequest(Get_API_EXE_RouteTimeXML_Request request) {
        createRepository();
        repository.Get_API_EXE_RouteTimeXML(request, new Get_API_EXE_RouteTimeXML_ViewRepository.OnResponseHandler() {
            @Override
            public void onSuccessResponse(List<Get_API_EXE_RouteTimeXML_Response.RouteTimeXML> list) {
                if (list != null) {
                    routeXmlMutableLiveData.setValue(list);
                }
            }

            @Override
            public void onFailureResponse() {
                routeXmlMutableLiveData.setValue(null);
            }
        });
    }

}
