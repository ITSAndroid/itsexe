package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.SeatArrangement_Request;
import com.infinityinfoway.itsexe.api.response.SeatArrangement_Response;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_repository.SeatArrangement_ViewRepository;

import java.util.List;

public class SeatArrangement_ViewModel extends ViewModel {
    public MutableLiveData<List<SeatArrangement_Response.Datum>> mutableSeatChartLiveData = new MutableLiveData<>();
    private SeatArrangement_ViewRepository repository;

    public void init(SeatArrangement_Request request) {
        if (repository == null) {
            repository = new SeatArrangement_ViewRepository();
        }

        repository.SeatArrangement(request, new SeatArrangement_ViewRepository.SeatArrangement_OnResponseHandler() {
            @Override
            public void onSucessHandler(List<SeatArrangement_Response.Datum> list, boolean status) {
                mutableSeatChartLiveData.setValue(list);
            }

            @Override
            public void onFailedHandler() {
                mutableSeatChartLiveData.setValue(null);
            }
        });
    }

}
