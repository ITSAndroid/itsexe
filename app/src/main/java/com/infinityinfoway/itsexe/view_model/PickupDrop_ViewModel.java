package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.PickupDrop_Request;
import com.infinityinfoway.itsexe.api.response.PickupDrop_Response;
import com.infinityinfoway.itsexe.view_repository.PickupDrop_ViewRepository;

import java.util.List;

public class PickupDrop_ViewModel extends ViewModel {
    public MutableLiveData<List<PickupDrop_Response.Datum>> pickUpDropLiveData=new MutableLiveData<>();
    private PickupDrop_ViewRepository repository;

    private void createRepository(){
        if(repository==null){
            repository=PickupDrop_ViewRepository.getInstance();
        }
    }

    public void createPickupDropRequest(PickupDrop_Request request){
        createRepository();
        repository.PickupDrop(request, new PickupDrop_ViewRepository.OnResponseHandler() {
            @Override
            public void onSuccessResponse(List<PickupDrop_Response.Datum> list) {
                if(list!=null){
                    pickUpDropLiveData.setValue(list);
                }else{
                    pickUpDropLiveData.setValue(null);
                }
            }

            @Override
            public void onFailureResponse() {
                pickUpDropLiveData.setValue(null);
            }
        });
    }

}
