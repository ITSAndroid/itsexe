package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.Get_ReportType_Request;
import com.infinityinfoway.itsexe.api.response.Get_ReportType_Response;
import com.infinityinfoway.itsexe.view_repository.Get_ReportType_ViewRepository;

import java.util.List;

public class Get_ReportType_ViewModel extends ViewModel {
    public MutableLiveData<List<Get_ReportType_Response.ReportType>> listMutableLiveData=new MutableLiveData<>();
    private Get_ReportType_ViewRepository repository;

    private void createRepository(){
        if(repository==null){
            repository=Get_ReportType_ViewRepository.getInstance();
        }
    }

    public void onCreateRequest(Get_ReportType_Request request){
        createRepository();
        repository.Get_ReportType(request, new Get_ReportType_ViewRepository.OnResponseHandler() {
            @Override
            public void onSuccessResponse(List<Get_ReportType_Response.ReportType> list) {
                listMutableLiveData.setValue(list);
            }

            @Override
            public void onFailureResponse() {
                listMutableLiveData.setValue(null);
            }
        });
    }

}
