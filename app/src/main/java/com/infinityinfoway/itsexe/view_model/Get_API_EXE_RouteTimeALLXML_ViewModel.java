package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.Get_API_EXE_RouteTimeALLXML_Request;
import com.infinityinfoway.itsexe.api.response.Get_API_EXE_RouteTimeALLXML_Response;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_repository.Get_API_EXE_RouteTimeALLXML_ViewRepository;

import retrofit2.Response;

public class Get_API_EXE_RouteTimeALLXML_ViewModel extends ViewModel {
    public MutableLiveData<Response<Get_API_EXE_RouteTimeALLXML_Response>> xmlMutableLiveData = new MutableLiveData<>();
    private Get_API_EXE_RouteTimeALLXML_ViewRepository repository;

    private void createRepository() {
        if (repository == null) {
            repository = Get_API_EXE_RouteTimeALLXML_ViewRepository.getInstance();
        }
    }

    public void onCreateRequest(Get_API_EXE_RouteTimeALLXML_Request request) {
        createRepository();
        repository.Get_API_EXE_RouteTimeALLXML(request, new Get_API_EXE_RouteTimeALLXML_ViewRepository.OnResponseHandler() {
            @Override
            public void onSuccessResponse(Response<Get_API_EXE_RouteTimeALLXML_Response> response) {
                xmlMutableLiveData.setValue(response);
            }

            @Override
            public void onFailureResponse() {
                xmlMutableLiveData.setValue(null);
            }
        });
    }

}
