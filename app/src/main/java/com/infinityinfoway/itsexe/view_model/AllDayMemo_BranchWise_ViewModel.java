package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.Get_Report_AllDayMemo_BranchWise_Request;
import com.infinityinfoway.itsexe.api.response.Get_Report_AllDayMemo_BranchWise_Response;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_repository.AllDayMemo_BranchWise_ViewRepository;

import java.util.LinkedHashMap;
import java.util.List;

public class AllDayMemo_BranchWise_ViewModel extends ViewModel {
    public MutableLiveData<LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>>> mutableLiveData = new MutableLiveData<>();
    private AllDayMemo_BranchWise_ViewRepository repository;
    public MutableLiveData<PROGRESS_STATUS> mutableProgressStatus = new MutableLiveData<>();

    public void init(Get_Report_AllDayMemo_BranchWise_Request request) {
        if (repository == null) {
            repository = AllDayMemo_BranchWise_ViewRepository.getInstance();
        }

        mutableProgressStatus.setValue(PROGRESS_STATUS.SHOW);

        repository.Get_Report_AllDayMemo_BranchWise(request, new AllDayMemo_BranchWise_ViewRepository.AllDayMemo_BranchWise_OnResponseHandler() {
            @Override
            public void onSucessHandler(LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>> data, boolean status) {
                mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                mutableLiveData.setValue(data);
                // getAllDayMemoBranchWiseList();
            }

            @Override
            public void onFailedHandler() {
                mutableProgressStatus.setValue(PROGRESS_STATUS.BAD_RESPONSE);
                mutableLiveData.setValue(null);
            }
        });
    }

    public MutableLiveData<LinkedHashMap<String, LinkedHashMap<String, List<Get_Report_AllDayMemo_BranchWise_Response.AllDayMemo>>>> getAllDayMemoBranchWiseList() {
        return mutableLiveData;
    }


}
