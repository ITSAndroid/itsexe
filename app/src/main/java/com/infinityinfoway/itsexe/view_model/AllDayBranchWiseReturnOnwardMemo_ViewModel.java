package com.infinityinfoway.itsexe.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infinityinfoway.itsexe.api.request.AllDayBranchWiseReturnOnwardMemo_Request;
import com.infinityinfoway.itsexe.api.response.AllDayBranchWiseReturnOnwardMemo_Response;
import com.infinityinfoway.itsexe.utils.PROGRESS_STATUS;
import com.infinityinfoway.itsexe.view_repository.AllDayBranchWiseReturnOnwardMemo_ViewRepository;

import java.util.LinkedHashMap;
import java.util.List;

public class AllDayBranchWiseReturnOnwardMemo_ViewModel extends ViewModel {
    public MutableLiveData<LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>>> mutableLiveData = new MutableLiveData<>();
    private AllDayBranchWiseReturnOnwardMemo_ViewRepository repository;
    public MutableLiveData<PROGRESS_STATUS> mutableProgressStatus = new MutableLiveData<>();

    public void init(AllDayBranchWiseReturnOnwardMemo_Request request) {
        if (repository == null) {
            repository = AllDayBranchWiseReturnOnwardMemo_ViewRepository.getInstance();
        }

        mutableProgressStatus.setValue(PROGRESS_STATUS.SHOW);

        repository.AllDayBranchWiseReturnOnwardMemo(request, new AllDayBranchWiseReturnOnwardMemo_ViewRepository.OnResponseHandler() {
            @Override
            public void onSucessHandler(LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>> data, boolean status) {
                mutableProgressStatus.setValue(PROGRESS_STATUS.DISMISS);
                mutableLiveData.setValue(data);
                // getAllDayMemoBranchWiseList();
            }

            @Override
            public void onFailedHandler() {
                mutableProgressStatus.setValue(PROGRESS_STATUS.BAD_RESPONSE);
                mutableLiveData.setValue(null);
            }
        });
    }

    public MutableLiveData<LinkedHashMap<String, LinkedHashMap<String, List<AllDayBranchWiseReturnOnwardMemo_Response.Datum>>>> getAllDayBranchWiseReturnOnwardMemo() {
        return mutableLiveData;
    }


}

